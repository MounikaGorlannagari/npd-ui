import { __decorate, __read, __spread, __values } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { extendMoment } from 'moment-range';
import * as Moment from 'moment';
import { Observable } from 'rxjs';
import { FilterHelperService } from '../../shared/services/filter-helper.service';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import { SearchConfigConstants } from '../../mpm-utils/objects/SearchConfigConstants';
import { LoaderService } from '../../loader/loader.service';
import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { IndexerService } from '../../shared/services/indexer/indexer.service';
import { TaskConstants } from '../../project/tasks/task.constants';
import * as acronui from './../../mpm-utils/auth/utility';
import { SearchDataService } from '../../search/services/search-data.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import * as $ from 'jquery';
import { DeliverableService } from '../../project/tasks/deliverable/deliverable.service';
var moment = extendMoment(Moment);
var SynchronizeTaskFiltersComponent = /** @class */ (function () {
    function SynchronizeTaskFiltersComponent(sharingService, appService, entityAppDefService, utilService, fieldConfigService, filterHelperService, loaderService, viewConfigService, indexerService, deliverableService, otmmService, searchDataService) {
        this.sharingService = sharingService;
        this.appService = appService;
        this.entityAppDefService = entityAppDefService;
        this.utilService = utilService;
        this.fieldConfigService = fieldConfigService;
        this.filterHelperService = filterHelperService;
        this.loaderService = loaderService;
        this.viewConfigService = viewConfigService;
        this.indexerService = indexerService;
        this.deliverableService = deliverableService;
        this.otmmService = otmmService;
        this.searchDataService = searchDataService;
        this.deliverableData = new EventEmitter();
        this.selectedUser = new EventEmitter();
        this.selectedFilters = new EventEmitter();
        this.teams = [];
        this.roles = [];
        this.users = [];
        this.momentPtr = moment;
        this.viewTypeCellSize = 35;
        this.userId = [];
        this.selectedItems = [];
        this.allUsers = [];
        this.userIds = [];
        this.OwnerArray = []; // {}
        this.searchConditions = [];
        this.emailSearchCondition = [];
        this.convertedDate = [];
        this.storeSearchconditionObject = null;
        this.subscriptions = [];
        this.teamInitial = '';
        this.roleInitial = '';
        this.userInitial = '';
        this.previousUserId = [];
    }
    SynchronizeTaskFiltersComponent.prototype.GetRoleDNByRoleId = function (roleId) {
        var allRoles = this.sharingService.getAllRoles();
        return allRoles.find(function (role) { return role['MPM_APP_Roles-id'].Id === roleId; });
    };
    SynchronizeTaskFiltersComponent.prototype.getRolesAndUser = function (team) {
        var _this = this;
        this.selectedTeam = team;
        if (this.selectedItems.indexOf(team) !== -1) {
            console.log("Value exists!");
        }
        else {
            this.selectedItems.push(team);
        }
        /* this.selectedItems.push(team); */
        this.selectedFilters.emit(this.selectedItems);
        console.log(team);
        var allTeamRoles = this.sharingService.getAllTeamRoles();
        console.log(allTeamRoles);
        this.allRoles = [];
        this.users = [];
        allTeamRoles.forEach(function (teamRole) {
            if (teamRole && teamRole.R_PO_TEAM !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"] !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"].Id !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"].Id === team['MPM_Teams-id'].Id) {
                var teamRoles = teamRole; //this.GetRoleDNByRoleId(teamRole['MPM_Team_Role_Mapping-id'].Id); // this.utilService.GetRoleDNByRoleId(teamRole['MPM_Team_Role_Mapping-id'].Id);
                console.log(teamRoles);
                if (teamRoles !== undefined) {
                    _this.allRoles.push(teamRoles);
                }
            }
        });
        this.getUsersForTeam(team).subscribe(function (response) {
            console.log(response);
        });
        /*  const getUserByRoleRequest = {
           allocationType: 'ROLE',
           roleDN: team.ROLE_DN,
           teamID: team['MPM_Teams-id'].Id,
           isApproveTask: false,
           isUploadTask: true
         };
         this.appService.getUsersForTeam(getUserByRoleRequest)
           .subscribe(allUsers => {
             if (allUsers && allUsers.users && allUsers.users.user) {
               allUsers.users.user.forEach(user => {
                 this.users.push(user);
               });
             }
           }); */
    };
    SynchronizeTaskFiltersComponent.prototype.getUsersForTeam = function (team) {
        var _this = this;
        return new Observable(function (observer) {
            var getUserByRoleRequest = {
                allocationType: 'ROLE',
                roleDN: team.ROLE_DN,
                teamID: team['MPM_Teams-id'].Id,
                isApproveTask: false,
                isUploadTask: true
            };
            _this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(function (allUsers) {
                if (allUsers && allUsers.users && allUsers.users.user) {
                    allUsers.users.user.forEach(function (user) {
                        _this.users.push(user);
                    });
                }
                observer.next(true);
                observer.complete();
            });
        });
    };
    SynchronizeTaskFiltersComponent.prototype.getTeamsAndUser = function (role) {
        this.selectedRole = role;
        //  role.ROLE_NAME = role.ROLE_NAME.replace(/_/g, ' ');
        if (this.selectedItems.indexOf(role) !== -1) {
            console.log("Value exists!");
        }
        else {
            this.selectedItems.push(role);
        }
        /* this.selectedItems.push(role); */
        this.selectedFilters.emit(this.selectedItems);
        console.log(role);
        this.getUsersByRole(role, this.selectedTeam).subscribe(function (response) {
        });
    };
    SynchronizeTaskFiltersComponent.prototype.getUsersByRole = function (role, selectedTeam) {
        var _this = this;
        return new Observable(function (observer) {
            var getUserByRoleRequest = {};
            _this.users = [];
            getUserByRoleRequest = {
                allocationType: TaskConstants.ALLOCATION_TYPE_ROLE,
                roleDN: role.ROLE_DN,
                teamID: selectedTeam ? selectedTeam['MPM_Teams-id'].Id : '',
                isApproveTask: false,
                isUploadTask: true
            };
            _this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(function (response) {
                if (response && response.users && response.users.user) {
                    var userList = acronui.findObjectsByProp(response, 'user');
                    var users_1 = [];
                    if (userList && userList.length && userList.length > 0) {
                        userList.forEach(function (user) {
                            var fieldObj = users_1.find(function (e) { return e.value === user.dn; });
                            if (!fieldObj) {
                                users_1.push({
                                    name: user.name,
                                    value: user.cn,
                                    displayName: user.name,
                                    cn: user.cn
                                });
                            }
                        });
                    }
                    _this.users = users_1;
                }
                else {
                    _this.users = [];
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, function () {
                observer.next(false);
                observer.complete();
            });
        });
    };
    SynchronizeTaskFiltersComponent.prototype.allocationTask = function (user) {
        var capacity = 8;
        var taskAllocation = 1;
        return taskAllocation + '/' + capacity;
    };
    SynchronizeTaskFiltersComponent.prototype.resourceTaskAllocation = function (user, currentViewStartDates, currentViewEndDates) {
        var firstDate = moment(currentViewStartDates);
        var secondDate = moment(currentViewEndDates);
        var diffInDays = Math.abs(firstDate.diff(secondDate, 'days'));
        var allocation = [];
        for (var i = 0; i <= diffInDays; i++) {
            allocation[i] = this.allocationTask(user); // '0/8';
        }
        return allocation;
    };
    /* getDeliverable(user, removingFilterValue) {
      console.log(user);
      this.loaderService.show();
      this.userId = [];
      if (removingFilterValue) {
        let userid, itemid;
        this.allUsers.forEach((item, index) => {
          userid = user && user[0] && user[0]['Identity-id'] !== undefined && user[0]['Identity-id'].Id ? user[0]['Identity-id'].Id : user && user[0] && user[0].cn ? user[0].cn : '';
          itemid = item && item['Identity-id'] !== undefined && item['Identity-id'].Id ? item['Identity-id'].Id : item && item.cn ? item.cn : '';
          if (itemid === userid) {
            this.allUsers.splice(index, 1);
  
          }
        });
        this.userId.forEach((id, idx) => {
          const ids = id && id[0] && id[0]['Identity-id'] !== undefined && id[0]['Identity-id'].Id ? id[0]['Identity-id'].Id : id && id[0] && id[0].cn ? id[0].cn : ''
          if (ids === userid) {
            this.userId.splice(idx, 1);
          }
        });
      } else {
        this.allUsers.push(user);
        this.selectedItems.push(user);
        this.selectedFilters.emit(this.selectedItems);
        this.selectedUser.emit(user);
      }
  
      let viewConfig;
      this.allUsers.forEach(element => {
        if (element.cn) {
          this.userId.push(this.allUser.filter(users => users.UserId === element.cn));
        }
      });
     
      let UserId = [];
      if (this.userId.length > 0) {
        this.userId.forEach(id => {
          if (UserId.indexOf(id[0]['Identity-id'].Id) === -1) {
            UserId.push(id[0]['Identity-id'].Id);
          }
  
        })
      } else {
        this.allUsers.forEach(users => {
          UserId.push(users['Identity-id'].Id);
        });
      }
  
      const dates = this.momentPtr.utc(this.currentViewStartDates).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDates).format('YYYY-MM-DDT00:00:00.000') + 'Z';
  
      const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.levels, dates, UserId);// user['Identity-id'].Id);
      const otmmMPMDataTypes = OTMMMPMDataTypes.DELIVERABLE;
  
      const condition = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, otmmMPMDataTypes);
      condition ? conditionObject.DELIVERABLE_CONDTION.push(condition) : console.log('');
  
  
  
  
      if (this.viewConfigs) {
        viewConfig = this.viewConfigs;
      } else {
        viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
      }
      this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails: ViewConfig) => {
        const searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
          ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
        const parameters: SearchRequest = {
          search_config_id: searchConfig ? searchConfig : null,
          keyword: '',
          search_condition_list: {
            search_condition: conditionObject.DELIVERABLE_CONDTION
          },
          facet_condition_list: {
            facet_condition: []
          },
          sorting_list: {
            sort: []
          },
          cursor: {
            page_index: 0,
            page_size: 100
          }
        };
  
        this.loaderService.show();
        if (UserId.length > 0) {
          this.subscriptions.push(
            this.indexerService.search(parameters).subscribe((data: SearchResponse) => {
              console.log(data);
              this.resource = [];
              this.OwnerArray = [];
              if (data.data.length > 0) {
                const dFormat = 'YYYY-MM-DD';
                data.data.forEach((item, index) => {
                  let styles = {};
                  const eventStartDate = this.momentPtr(item.DELIVERABLE_START_DATE, dFormat);
                  const eventEndDate = this.momentPtr(item.DELIVERABLE_DUE_DATE, dFormat);
                  const diff = (eventEndDate.diff(eventStartDate)) / (1000 * 60 * 60 * 24);
                  const extraPixel = ((diff + 1) % 2) === 0 ? 0 : 2;
                  const width = ((diff + 1) * (this.viewTypeCellSize + extraPixel)); // ((diff + 1) * (this.viewTypeCellSize - 1));
  
                  let OwnerName = item.DELIVERABLE_ACTIVE_USER_NAME;
                  if (!this.OwnerArray.hasOwnProperty(OwnerName)) {
                    this.OwnerArray[OwnerName] = [];
                    //  this.resource['allocation'] = [0 / 8];
                    this.resourceTaskAllocation(OwnerName, this.currentViewStartDates, this.currentViewEndDates);
                    //   this.OwnerArray = { [OwnerName]: [] };
                  }
                  let top = 30 * this.OwnerArray[OwnerName].length;
                  styles['width'] = width + 'px';
                  styles['top'] = top + 'px';
  
                  const deliverableDetail = {
                    data: item,
                    style: styles
                  };
                  let k: any, v: any;
                  for ([k, v] of Object.entries(this.OwnerArray)) {
                    if (v.length === 0) {
                      const date = this.datee
                      this.OwnerArray[OwnerName]['date'] = date;// this.datess;
                    }
                  }
                  this.OwnerArray[OwnerName].push({ deliverableDetail });
  
  
                  console.log(this.OwnerArray);
  
                  //  this.resource.push(this.OwnerArray);
                  console.log(this.resource);
                });
                //  this.resource.height = 35 * data.data.length + 'px';
                this.OwnerArray.height = 35 * data.data.length + 'px';
              }
              // this.deliverableData.emit(this.resource);
              this.deliverableData.emit(this.OwnerArray);
              this.resourceAllocation.emit(this.resource);
            })
          );
        } else {
          this.OwnerArray = [];
          this.deliverableData.emit(this.OwnerArray);
        }
  
        this.loaderService.hide();
      });
  
    } */
    SynchronizeTaskFiltersComponent.prototype.getProperty = function (deliverableData, mapperName) {
        // const level = this.isReviewer ? MPM_LEVELS.DELIVERABLE_REVIEW : MPM_LEVELS.DELIVERABLE;
        var displayColumn = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.DELIVERABLE);
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, deliverableData);
    };
    /**
       * @since MPMV3-946
       * @param deliverableType
       */
    SynchronizeTaskFiltersComponent.prototype.getDeliverableTypeIcon = function (deliverableType) {
        return this.deliverableService.getDeliverableTypeIcon(deliverableType);
    };
    SynchronizeTaskFiltersComponent.prototype.reload = function (user) {
        console.log("reload");
        console.log(this.selectedItems);
        console.log(user);
        // this.getDeliverable(user, false)
    };
    SynchronizeTaskFiltersComponent.prototype.getDeliverableList = function (UserId) {
        var _this = this;
        var viewConfig;
        var dates = this.momentPtr.utc(this.currentViewStartDates).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDates).format('YYYY-MM-DDT00:00:00.000') + 'Z';
        //MPM-2224
        /*  const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, MPM_LEVELS.TASK, dates, UserId); // this.levels// user['Identity-id'].Id);
       */
        var conditionObject = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, MPM_LEVELS.TASK, this.momentPtr.utc(this.currentViewStartDates).format('YYYY-MM-DDT00:00:00.000') + 'Z', this.momentPtr.utc(this.currentViewEndDates).format('YYYY-MM-DDT00:00:00.000') + 'Z', UserId, true); // this.levels// user['Identity-id'].Id);
        this.storeSearchconditionObject = Object.assign({}, conditionObject);
        var otmmMPMDataTypes = OTMMMPMDataTypes.DELIVERABLE;
        //MPM-2224
        /* const conditionObjects: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.levels, dates, UserId); // this.levels// user['Identity-id'].Id);
        */ var conditionObjects = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.levels, this.momentPtr.utc(this.currentViewStartDates).format('YYYY-MM-DDT00:00:00.000') + 'Z', this.momentPtr.utc(this.currentViewEndDates).format('YYYY-MM-DDT00:00:00.000') + 'Z', UserId, true); // this.levels// user['Identity-id'].Id);
        //  this.storeSearchconditionObject = Object.assign({}, conditionObject);
        var otmmMPMDataTypess = OTMMMPMDataTypes.DELIVERABLE;
        var conditions = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, otmmMPMDataTypes);
        conditions ? conditionObjects.DELIVERABLE_CONDTION.push(conditions) : console.log('');
        if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
            //keyWord = this.searchConditions[0].keyword;
        }
        else if (this.searchConditions && this.searchConditions.length > 0) {
            conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(this.searchConditions);
        }
        this.emailSearchCondition = this.emailSearchCondition && Array.isArray(this.emailSearchCondition) ? this.emailSearchCondition : [];
        if (this.viewConfigs) {
            viewConfig = this.viewConfigs;
        }
        else {
            viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
        }
        this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe(function (viewDetails) {
            var searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
            var parameters = {
                search_config_id: searchConfig ? searchConfig : null,
                keyword: '',
                grouping: {
                    group_by: 'TASK',
                    group_from_type: otmmMPMDataTypes,
                    search_condition_list: {
                        search_condition: __spread(conditionObject.DELIVERABLE_CONDTION, _this.emailSearchCondition)
                    }
                },
                search_condition_list: {
                    search_condition: conditionObject.TASK_CONDITION
                },
                facet_condition_list: {
                    facet_condition: []
                },
                sorting_list: {
                    sort: []
                },
                cursor: {
                    page_index: 0,
                    page_size: 1000
                }
            };
            var parameterss = {
                search_config_id: searchConfig ? searchConfig : null,
                keyword: '',
                search_condition_list: {
                    search_condition: conditionObjects.DELIVERABLE_CONDTION
                },
                facet_condition_list: {
                    facet_condition: []
                },
                sorting_list: {
                    sort: []
                },
                cursor: {
                    page_index: 0,
                    page_size: 100
                }
            };
            _this.loaderService.show();
            if (UserId.length > 0) {
                _this.subscriptions.push(_this.indexerService.search(parameters).subscribe(function (data) {
                    _this.indexerService.search(parameterss).subscribe(function (deliverableData) {
                        console.log(data);
                        console.log(deliverableData);
                        _this.resource = [];
                        _this.OwnerArray = [];
                        if (data.data.length > 0) {
                            var dFormat_1 = 'YYYY-MM-DD';
                            data.data.forEach(function (item, itemIndex) {
                                //checking -deliverableData to deliverable
                                deliverableData.data.forEach(function (deliverable, itemIndex) {
                                    var e_1, _a, _b;
                                    if (item.ID === deliverable.TASK_ITEM_ID.split('.')[1]) { //deliverableData.data[itemIndex].TASK_ITEM_ID.split('.')[1]) {
                                        var styles = {};
                                        /*   const eventStartDate = this.momentPtr(deliverableData.data[itemIndex].DELIVERABLE_START_DATE, dFormat);
                                          const eventEndDate = this.momentPtr(deliverableData.data[itemIndex].DELIVERABLE_DUE_DATE, dFormat); */
                                        var eventStartDate = _this.momentPtr(deliverable.DELIVERABLE_START_DATE, dFormat_1);
                                        var eventEndDate = _this.momentPtr(deliverable.DELIVERABLE_DUE_DATE, dFormat_1);
                                        var diff = (eventEndDate.diff(eventStartDate)) / (1000 * 60 * 60 * 24);
                                        var extraPixel = ((diff + 1) % 2) === 0 ? 0 : 1; // 0 : 2;
                                        /*   const extraPixel = ((diff + 1) % 2) === 0 ? 0 : 2; */
                                        var width = ((diff + 1) * (_this.viewTypeCellSize + extraPixel)); // ((diff + 1) * (this.viewTypeCellSize - 1));
                                        /* let OwnerName = deliverableData.data[itemIndex].DELIVERABLE_ACTIVE_USER_NAME; */
                                        var OwnerName = deliverable.DELIVERABLE_ACTIVE_USER_NAME;
                                        if (!_this.OwnerArray.hasOwnProperty(OwnerName)) {
                                            _this.OwnerArray[OwnerName] = [];
                                            _this.resourceTaskAllocation(OwnerName, _this.currentViewStartDates, _this.currentViewEndDates);
                                        }
                                        var top_1 = 44 * _this.OwnerArray[OwnerName].length; // 30
                                        //    styles['width'] = (diff !== 1 ? (((diff + 1) % 2) === 0) ? ((width) + 'px') : (width + 4) + 'px' : (width) + 'px');
                                        styles['top'] = top_1 + 'px';
                                        var deliverableDetail = {
                                            data: deliverable,
                                            name: (_this.levels.includes('TASK')) ? item.TASK_NAME : item.PROJECT_NAME,
                                            /* icon: this.getDeliverableTypeIcon(this.getProperty(deliverableData.data[itemIndex], 'DELIVERABLE_TYPE')).ICON,
                                             */
                                            icon: _this.getDeliverableTypeIcon(_this.getProperty(deliverable, 'DELIVERABLE_TYPE')).ICON,
                                            style: styles
                                        };
                                        var k = void 0, v = void 0;
                                        try {
                                            for (var _c = __values(Object.entries(_this.OwnerArray)), _d = _c.next(); !_d.done; _d = _c.next()) {
                                                _b = __read(_d.value, 2), k = _b[0], v = _b[1];
                                                if (v.length === 0) {
                                                    var date = $.extend(true, [], _this.convertedDate); // this.convertedDate;//JSON.parse(JSON.stringify(this.datee))//$.extend(true,[],this.datee)
                                                    _this.OwnerArray[OwnerName]['date'] = date;
                                                    _this.OwnerArray[OwnerName]['allocation'] = _this.resourceTaskAllocation(OwnerName, _this.currentViewStartDates, _this.currentViewEndDates);
                                                }
                                            }
                                        }
                                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                                        finally {
                                            try {
                                                if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                                            }
                                            finally { if (e_1) throw e_1.error; }
                                        }
                                        _this.OwnerArray[OwnerName].push({ deliverableDetail: deliverableDetail });
                                    }
                                });
                            });
                            _this.OwnerArray.height = 50 * data.data.length + 'px'; // 35 //50
                        }
                        _this.loaderService.hide();
                        _this.deliverableData.emit(_this.OwnerArray);
                        //   this.resourceAllocation.emit(this.resource);
                    });
                }));
            }
            else {
                _this.loaderService.hide();
                _this.OwnerArray = [];
                _this.deliverableData.emit(_this.OwnerArray);
            }
        });
    };
    // search by task-grouping and map the name to deliverable search
    SynchronizeTaskFiltersComponent.prototype.getDeliverable = function (user, removingFilterValue, isParent) {
        var _this = this;
        console.log(user);
        this.loaderService.show();
        var UserId = [];
        if (isParent) {
            var userid = this.allUser.find(function (users) { return users.UserId === user; });
            // if (this.previousUserId.includes(userid['Identity-id'].Id)) {
            if ((userid && userid['Identity-id'] && userid['Identity-id'].Id !== undefined && this.previousUserId.includes(userid['Identity-id'].Id)) ||
                (userid === undefined)) {
                UserId = this.previousUserId;
                this.getDeliverableList(UserId);
            }
            else {
                this.loaderService.hide();
            }
        }
        else {
            this.userId = [];
            if (removingFilterValue) {
                var userid_1, itemid_1;
                this.allUsers.forEach(function (item, index) {
                    userid_1 = user && user[0] && user[0]['Identity-id'] !== undefined && user[0]['Identity-id'].Id ? user[0]['Identity-id'].Id : user && user[0] && user[0].cn ? user[0].cn : '';
                    itemid_1 = item && item['Identity-id'] !== undefined && item['Identity-id'].Id ? item['Identity-id'].Id : item && item.cn ? item.cn : '';
                    if (itemid_1 === userid_1) {
                        _this.allUsers.splice(index, 1);
                    }
                });
                this.userId.forEach(function (id, idx) {
                    var ids = id && id[0] && id[0]['Identity-id'] !== undefined && id[0]['Identity-id'].Id ? id[0]['Identity-id'].Id : id && id[0] && id[0].cn ? id[0].cn : '';
                    if (ids === userid_1) {
                        _this.userId.splice(idx, 1);
                    }
                });
            }
            else {
                this.allUsers.push(user);
                if (this.selectedItems.indexOf(user) !== -1) {
                    console.log("Value exists!");
                }
                else {
                    this.selectedItems.push(user);
                }
                /* this.selectedItems.push(user); */
                this.selectedFilters.emit(this.selectedItems);
                this.selectedUser.emit(user);
            }
            this.allUsers.forEach(function (element) {
                if (element.cn) {
                    _this.userId.push(_this.allUser.filter(function (users) { return users.UserId === element.cn; }));
                    //   this.userIds.push(this.userId);
                }
            });
            /* let UserId = []; */
            if (this.userId.length > 0) {
                this.userId.forEach(function (id) {
                    if (UserId.indexOf(id[0]['Identity-id'].Id) === -1) {
                        UserId.push(id[0]['Identity-id'].Id);
                    }
                });
            }
            else {
                this.allUsers.forEach(function (users) {
                    UserId.push(users['Identity-id'].Id);
                });
            }
            this.previousUserId = UserId;
            this.getDeliverableList(UserId);
        }
    };
    SynchronizeTaskFiltersComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        console.log(changes);
        var currentStartDate = this.currentViewStartDates && this.currentViewStartDates._i !== undefined ? this.currentViewStartDates._i : this.currentViewStartDates;
        if (changes && changes.datee && changes.datee.currentValue && changes.datee.currentValue[0] && changes.datee.currentValue[0]._i &&
            !changes.datee.firstChange) {
            this.convertedDate = [];
            changes.datee.currentValue.forEach(function (element) {
                var x = element.format('YYYY-MM-DD');
                var date = {
                    date: x,
                    element: element.format('dd DD'),
                    isOverlap: 'false'
                };
                _this.convertedDate.push(date);
            });
            console.log(this.convertedDate);
        }
        this.getDeliverable(this.removedFilter, true);
        // To clear the selected value
        if (this.removedFilter && this.removedFilter.length > 0 && this.removedFilter[0] === 'TEAM') {
            this.teamInitial = '';
            this.selectedTeam = '';
            if (this.selectedRole) {
            }
            else {
                this.roleInitial = '';
                this.userInitial = '';
                if (this.inputName && this.inputName.nativeElement && this.inputName.nativeElement.value) {
                    this.inputName.nativeElement.value = 'Users';
                }
                // added back all roles and users
                this.allRoles = this.sharingService.getAllTeamRoles(); //this.sharingService.getAllRoles();
                this.appService.getAllUsers().subscribe(function (allUsers) {
                    if (allUsers && allUsers.User) {
                        _this.allUser = allUsers.User;
                        allUsers.User.forEach(function (user) {
                            if (user && user.FullName && typeof (user.FullName) !== 'object') {
                                _this.users.push(user);
                            }
                        });
                    }
                });
            }
        }
        else if (this.removedFilter && this.removedFilter.length > 0 && this.removedFilter[0] === 'ROLE') {
            this.roleInitial = '';
            this.selectedRole = '';
            if (this.selectedTeam) {
                /*  if (this.inputName && this.inputName.nativeElement && this.inputName.nativeElement.value) {
                   this.inputName.nativeElement.value = 'Users';
                 } */
                this.getUsersForTeam(this.selectedTeam).subscribe(function (response) {
                    console.log(response);
                });
            }
            else {
                this.userInitial = '';
                if (this.inputName && this.inputName.nativeElement && this.inputName.nativeElement.value) {
                    this.inputName.nativeElement.value = 'Users';
                }
                this.appService.getAllUsers().subscribe(function (allUsers) {
                    if (allUsers && allUsers.User) {
                        _this.allUser = allUsers.User;
                        allUsers.User.forEach(function (user) {
                            if (user && user.FullName && typeof (user.FullName) !== 'object') {
                                _this.users.push(user);
                            }
                        });
                    }
                });
            }
            /*  this.getUsersByRole(this.selectedRole, this.selectedTeam).subscribe(response => {
       
             }); */
        }
        else if (this.removedFilter && this.removedFilter.length > 0 && (this.removedFilter[0].name !== undefined || this.removedFilter[0].FullName !== undefined)) {
            if (this.inputName && this.inputName.nativeElement && this.inputName.nativeElement.value) {
                this.inputName.nativeElement.value = 'Users';
            }
            this.userInitial = '';
        }
        this.removedFilter = [];
    };
    SynchronizeTaskFiltersComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.level = this.groupBy && this.groupBy.value && this.groupBy.value.split('_')[2] ? this.groupBy.value.split('_')[2] : '';
        this.allTeams = this.sharingService.getAllTeams();
        /* this.allTeams.forEach(team => {
          this.teams.push(team);
        }); */
        this.allRoles = this.sharingService.getAllTeamRoles(); //this.sharingService.getAllRoles();
        /*  this.allRoles.forEach(role => {
           role.ROLE_NAME = role.ROLE_NAME.replace(/_/g, ' ');
         }); */
        this.appService.getAllUsers().subscribe(function (allUsers) {
            if (allUsers && allUsers.User) {
                _this.allUser = allUsers.User;
                allUsers.User.forEach(function (user) {
                    if (user && user.FullName && typeof (user.FullName) !== 'object') {
                        _this.users.push(user);
                    }
                });
            }
        });
        this.datee.forEach(function (element) {
            var x = element.format('YYYY-MM-DD');
            var date = {
                date: x,
                element: element.format('dd DD'),
                isOverlap: 'false'
            };
            _this.convertedDate.push(date);
        });
        console.log(this.convertedDate);
        // to change search
        this.subscriptions.push(this.searchDataService.searchData.subscribe(function (data) {
            _this.searchConditions = (data && Array.isArray(data) ? _this.otmmService.addRelationalOperator(data) : []);
        }));
    };
    SynchronizeTaskFiltersComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (subscription) { return subscription.unsubscribe(); });
    };
    SynchronizeTaskFiltersComponent.ctorParameters = function () { return [
        { type: SharingService },
        { type: AppService },
        { type: EntityAppDefService },
        { type: UtilService },
        { type: FieldConfigService },
        { type: FilterHelperService },
        { type: LoaderService },
        { type: ViewConfigService },
        { type: IndexerService },
        { type: DeliverableService },
        { type: OTMMService },
        { type: SearchDataService }
    ]; };
    __decorate([
        Input()
    ], SynchronizeTaskFiltersComponent.prototype, "groupBy", void 0);
    __decorate([
        Input()
    ], SynchronizeTaskFiltersComponent.prototype, "selectedMonth", void 0);
    __decorate([
        Input()
    ], SynchronizeTaskFiltersComponent.prototype, "currentViewStartDates", void 0);
    __decorate([
        Input()
    ], SynchronizeTaskFiltersComponent.prototype, "currentViewEndDates", void 0);
    __decorate([
        Input()
    ], SynchronizeTaskFiltersComponent.prototype, "levels", void 0);
    __decorate([
        Input()
    ], SynchronizeTaskFiltersComponent.prototype, "viewConfigs", void 0);
    __decorate([
        Input()
    ], SynchronizeTaskFiltersComponent.prototype, "removedFilter", void 0);
    __decorate([
        Input()
    ], SynchronizeTaskFiltersComponent.prototype, "datess", void 0);
    __decorate([
        Input()
    ], SynchronizeTaskFiltersComponent.prototype, "datee", void 0);
    __decorate([
        Output()
    ], SynchronizeTaskFiltersComponent.prototype, "deliverableData", void 0);
    __decorate([
        Output()
    ], SynchronizeTaskFiltersComponent.prototype, "selectedUser", void 0);
    __decorate([
        Output()
    ], SynchronizeTaskFiltersComponent.prototype, "selectedFilters", void 0);
    __decorate([
        ViewChild('name')
    ], SynchronizeTaskFiltersComponent.prototype, "inputName", void 0);
    SynchronizeTaskFiltersComponent = __decorate([
        Component({
            selector: 'mpm-synchronize-task-filters',
            template: "<p>synchronize-task-filters works!</p>\r\n",
            styles: [""]
        })
    ], SynchronizeTaskFiltersComponent);
    return SynchronizeTaskFiltersComponent;
}());
export { SynchronizeTaskFiltersComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3luY2hyb25pemUtdGFzay1maWx0ZXJzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Jlc291cmNlLW1hbmFnZW1lbnQvc3luY2hyb25pemUtdGFzay1maWx0ZXJzL3N5bmNocm9uaXplLXRhc2stZmlsdGVycy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxZQUFZLEVBQWlCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6RyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDbEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNoRixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDaEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBRTNELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUMsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsT0FBTyxFQUFFLFVBQVUsRUFBZ0IsTUFBTSxNQUFNLENBQUM7QUFFaEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDbEYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scUVBQXFFLENBQUM7QUFDekcsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0seUVBQXlFLENBQUM7QUFDakgsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFHdEYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzVELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBRTVFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUMvRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDbkUsT0FBTyxLQUFLLE9BQU8sTUFBTSxnQ0FBZ0MsQ0FBQztBQUMxRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFFekYsSUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBT3BDO0lBc0RFLHlDQUNTLGNBQThCLEVBQzlCLFVBQXNCLEVBQ3RCLG1CQUF3QyxFQUN4QyxXQUF3QixFQUN4QixrQkFBc0MsRUFDdEMsbUJBQXdDLEVBQ3hDLGFBQTRCLEVBQzVCLGlCQUFvQyxFQUNwQyxjQUE4QixFQUM5QixrQkFBc0MsRUFDdEMsV0FBd0IsRUFDeEIsaUJBQW9DO1FBWHBDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQXREbkMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2QyxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFLcEQsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxVQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ1gsY0FBUyxHQUFHLE1BQU0sQ0FBQztRQUluQixxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFFdEIsV0FBTSxHQUFHLEVBQUUsQ0FBQztRQUdaLGtCQUFhLEdBQVEsRUFBRSxDQUFDO1FBRXhCLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFDZCxZQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2IsZUFBVSxHQUFRLEVBQUUsQ0FBQyxDQUFDLEtBQUs7UUFFM0IscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLHlCQUFvQixHQUFHLEVBQUUsQ0FBQztRQUUxQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUVuQiwrQkFBMEIsR0FBeUIsSUFBSSxDQUFDO1FBRWhELGtCQUFhLEdBQXdCLEVBQUUsQ0FBQztRQUloRCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUVqQixtQkFBYyxHQUFHLEVBQUUsQ0FBQztJQWVoQixDQUFDO0lBRUwsMkRBQWlCLEdBQWpCLFVBQWtCLE1BQU07UUFDdEIsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuRCxPQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTSxFQUF0QyxDQUFzQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELHlEQUFlLEdBQWYsVUFBZ0IsSUFBSTtRQUFwQixpQkF5Q0M7UUF4Q0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFBO1NBQzdCO2FBQU07WUFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMvQjtRQUNELG9DQUFvQztRQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQixJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQzNELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7WUFDM0IsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFNBQVMsS0FBSyxTQUFTLElBQUksUUFBUSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxTQUFTLElBQUksUUFBUSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLEtBQUssU0FBUyxJQUFJLFFBQVEsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2hPLElBQU0sU0FBUyxHQUFHLFFBQVEsQ0FBQSxDQUFBLGtKQUFrSjtnQkFDNUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQTtnQkFDdEIsSUFBSSxTQUFTLEtBQUssU0FBUyxFQUFFO29CQUMzQixLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDL0I7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDSDs7Ozs7Ozs7Ozs7Ozs7aUJBY1M7SUFDWCxDQUFDO0lBRUQseURBQWUsR0FBZixVQUFnQixJQUFJO1FBQXBCLGlCQW9CQztRQW5CQyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixJQUFNLG9CQUFvQixHQUFHO2dCQUMzQixjQUFjLEVBQUUsTUFBTTtnQkFDdEIsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPO2dCQUNwQixNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUU7Z0JBQy9CLGFBQWEsRUFBRSxLQUFLO2dCQUNwQixZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUM7aUJBQ2xELFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7b0JBQ3JELFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7d0JBQzlCLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN4QixDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCx5REFBZSxHQUFmLFVBQWdCLElBQUk7UUFDbEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsdURBQXVEO1FBQ3ZELElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDM0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQTtTQUM3QjthQUFNO1lBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDL0I7UUFDRCxvQ0FBb0M7UUFDcEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFbEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7UUFFL0QsQ0FBQyxDQUFDLENBQUM7SUFHTCxDQUFDO0lBRUQsd0RBQWMsR0FBZCxVQUFlLElBQUksRUFBRSxZQUFZO1FBQWpDLGlCQTBDQztRQXpDQyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixJQUFJLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztZQUM5QixLQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUNoQixvQkFBb0IsR0FBRztnQkFDckIsY0FBYyxFQUFFLGFBQWEsQ0FBQyxvQkFBb0I7Z0JBQ2xELE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTztnQkFDcEIsTUFBTSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDM0QsYUFBYSxFQUFFLEtBQUs7Z0JBQ3BCLFlBQVksRUFBRSxJQUFJO2FBQ25CLENBQUM7WUFFRixLQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDbEQsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDakIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRTtvQkFDckQsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDN0QsSUFBTSxPQUFLLEdBQUcsRUFBRSxDQUFDO29CQUNqQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUN0RCxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTs0QkFDbkIsSUFBTSxRQUFRLEdBQUcsT0FBSyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEVBQUUsRUFBbkIsQ0FBbUIsQ0FBQyxDQUFDOzRCQUN0RCxJQUFJLENBQUMsUUFBUSxFQUFFO2dDQUNiLE9BQUssQ0FBQyxJQUFJLENBQUM7b0NBQ1QsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO29DQUNmLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtvQ0FDZCxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUk7b0NBQ3RCLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTtpQ0FDWixDQUFDLENBQUM7NkJBQ0o7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7b0JBQ0QsS0FBSSxDQUFDLEtBQUssR0FBRyxPQUFLLENBQUM7aUJBQ3BCO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2lCQUNqQjtnQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHdEQUFjLEdBQWQsVUFBZSxJQUFJO1FBQ2pCLElBQU0sUUFBUSxHQUFHLENBQUMsQ0FBQztRQUNuQixJQUFNLGNBQWMsR0FBRyxDQUFDLENBQUM7UUFDekIsT0FBTyxjQUFjLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQztJQUN6QyxDQUFDO0lBRUQsZ0VBQXNCLEdBQXRCLFVBQXVCLElBQUksRUFBRSxxQkFBcUIsRUFBRSxtQkFBbUI7UUFDckUsSUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDaEQsSUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDL0MsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ2hFLElBQU0sVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUN0QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3BDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsU0FBUztTQUNwRDtRQUNELE9BQU8sVUFBVSxDQUFDO0lBQ3BCLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQW9KSTtJQUVKLHFEQUFXLEdBQVgsVUFBWSxlQUFlLEVBQUUsVUFBVTtRQUNyQywwRkFBMEY7UUFDMUYsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMvSCxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxhQUFhLEVBQUUsZUFBZSxDQUFDLENBQUM7SUFDOUYsQ0FBQztJQUVEOzs7U0FHSztJQUNMLGdFQUFzQixHQUF0QixVQUF1QixlQUFlO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCxnREFBTSxHQUFOLFVBQU8sSUFBSTtRQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQixtQ0FBbUM7SUFDckMsQ0FBQztJQUVELDREQUFrQixHQUFsQixVQUFtQixNQUFNO1FBQXpCLGlCQTBKQztRQXpKQyxJQUFJLFVBQVUsQ0FBQztRQUNmLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsR0FBRyxDQUFDO1FBQ3RNLFVBQVU7UUFDVjtTQUNDO1FBQ0QsSUFBTSxlQUFlLEdBQXlCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLDJCQUEyQixFQUFFLFVBQVUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyx5Q0FBeUM7UUFDL1ksSUFBSSxDQUFDLDBCQUEwQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQ3JFLElBQU0sZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUMsV0FBVyxDQUFDO1FBRTFELFVBQVU7UUFDTjtVQUNFLENBQUEsSUFBTSxnQkFBZ0IsR0FBeUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLHlDQUF5QztRQUU5WSx5RUFBeUU7UUFDekUsSUFBTSxpQkFBaUIsR0FBRyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUM7UUFFdkQsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxFQUFFLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzdLLFVBQVUsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRXRGLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsS0FBSyxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFO1lBQ3JKLDZDQUE2QztTQUM5QzthQUFNLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BFLGVBQWUsQ0FBQyxjQUFjLEdBQUcsZUFBZSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDL0Y7UUFFRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRW5JLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNwQixVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztTQUMvQjthQUFNO1lBQ0wsVUFBVSxHQUFHLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7U0FDckQ7UUFDRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsK0JBQStCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsV0FBdUI7WUFDbkcsSUFBTSxZQUFZLEdBQUcsV0FBVyxDQUFDLDJCQUEyQixJQUFJLE9BQU8sV0FBVyxDQUFDLDJCQUEyQixLQUFLLFFBQVE7Z0JBQ3pILENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLDJCQUEyQixFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDakUsSUFBTSxVQUFVLEdBQWtCO2dCQUNoQyxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSTtnQkFDcEQsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsUUFBUSxFQUFFO29CQUNSLFFBQVEsRUFBRSxNQUFNO29CQUNoQixlQUFlLEVBQUUsZ0JBQWdCO29CQUNqQyxxQkFBcUIsRUFBRTt3QkFDckIsZ0JBQWdCLFdBQU0sZUFBZSxDQUFDLG9CQUFvQixFQUFLLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQztxQkFDMUY7aUJBQ0Y7Z0JBQ0QscUJBQXFCLEVBQUU7b0JBQ3JCLGdCQUFnQixFQUFFLGVBQWUsQ0FBQyxjQUFjO2lCQUNqRDtnQkFDRCxvQkFBb0IsRUFBRTtvQkFDcEIsZUFBZSxFQUFFLEVBQUU7aUJBQ3BCO2dCQUNELFlBQVksRUFBRTtvQkFDWixJQUFJLEVBQUUsRUFBRTtpQkFDVDtnQkFDRCxNQUFNLEVBQUU7b0JBQ04sVUFBVSxFQUFFLENBQUM7b0JBQ2IsU0FBUyxFQUFFLElBQUk7aUJBQ2hCO2FBQ0YsQ0FBQztZQUVGLElBQU0sV0FBVyxHQUFrQjtnQkFDakMsZ0JBQWdCLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUk7Z0JBQ3BELE9BQU8sRUFBRSxFQUFFO2dCQUNYLHFCQUFxQixFQUFFO29CQUNyQixnQkFBZ0IsRUFBRSxnQkFBZ0IsQ0FBQyxvQkFBb0I7aUJBQ3hEO2dCQUNELG9CQUFvQixFQUFFO29CQUNwQixlQUFlLEVBQUUsRUFBRTtpQkFDcEI7Z0JBQ0QsWUFBWSxFQUFFO29CQUNaLElBQUksRUFBRSxFQUFFO2lCQUNUO2dCQUNELE1BQU0sRUFBRTtvQkFDTixVQUFVLEVBQUUsQ0FBQztvQkFDYixTQUFTLEVBQUUsR0FBRztpQkFDZjthQUNGLENBQUM7WUFFRixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3JCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNyQixLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFvQjtvQkFDcEUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsZUFBK0I7d0JBQ2hGLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7d0JBQzdCLEtBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO3dCQUNuQixLQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQzt3QkFDckIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ3hCLElBQU0sU0FBTyxHQUFHLFlBQVksQ0FBQzs0QkFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsU0FBUztnQ0FDaEMsMENBQTBDO2dDQUMxQyxlQUFlLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFdBQVcsRUFBRSxTQUFTOztvQ0FDbEQsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLFdBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUMsK0RBQStEO3dDQUN0SCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7d0NBQ2hCO2dKQUN3Rzt3Q0FDeEcsSUFBTSxjQUFjLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsc0JBQXNCLEVBQUUsU0FBTyxDQUFDLENBQUM7d0NBQ25GLElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLG9CQUFvQixFQUFFLFNBQU8sQ0FBQyxDQUFDO3dDQUMvRSxJQUFNLElBQUksR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dDQUN6RSxJQUFNLFVBQVUsR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQSxTQUFTO3dDQUMzRCwwREFBMEQ7d0NBQzFELElBQU0sS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLDhDQUE4Qzt3Q0FFakgsbUZBQW1GO3dDQUNuRixJQUFJLFNBQVMsR0FBRyxXQUFXLENBQUMsNEJBQTRCLENBQUM7d0NBQ3pELElBQUksQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsRUFBRTs0Q0FDOUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUM7NENBQ2hDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsS0FBSSxDQUFDLHFCQUFxQixFQUFFLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO3lDQUU5Rjt3Q0FFRCxJQUFJLEtBQUcsR0FBRyxFQUFFLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQSxLQUFLO3dDQUN0RCx5SEFBeUg7d0NBRXpILE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFHLEdBQUcsSUFBSSxDQUFDO3dDQUUzQixJQUFNLGlCQUFpQixHQUFHOzRDQUN4QixJQUFJLEVBQUUsV0FBVzs0Q0FDakIsSUFBSSxFQUFFLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVk7NENBQ3pFOytDQUNHOzRDQUNILElBQUksRUFBRSxLQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUk7NENBRXpGLEtBQUssRUFBRSxNQUFNO3lDQUNkLENBQUM7d0NBQ0YsSUFBSSxDQUFDLFNBQUssRUFBRSxDQUFDLFNBQUssQ0FBQzs7NENBQ25CLEtBQWUsSUFBQSxLQUFBLFNBQUEsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUEsZ0JBQUEsNEJBQUU7MEVBQTFDLFNBQUMsRUFBRSxTQUFDO2dEQUNSLElBQUksQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0RBRWxCLElBQU0sSUFBSSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUEsQ0FBQSw0RkFBNEY7b0RBQy9JLEtBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDO29EQUMxQyxLQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsS0FBSSxDQUFDLHFCQUFxQixFQUFFLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2lEQUN6STs2Q0FDRjs7Ozs7Ozs7O3dDQUNELEtBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsaUJBQWlCLG1CQUFBLEVBQUUsQ0FBQyxDQUFDO3FDQUN4RDtnQ0FDSCxDQUFDLENBQUMsQ0FBQTs0QkFFSixDQUFDLENBQUMsQ0FBQzs0QkFDSCxLQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUMsVUFBVTt5QkFDbEU7d0JBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3dCQUMzQyxpREFBaUQ7b0JBQ25ELENBQUMsQ0FBQyxDQUFBO2dCQUNKLENBQUMsQ0FBQyxDQUNILENBQUM7YUFDSDtpQkFBTTtnQkFDTCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixLQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztnQkFDckIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQzVDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUVBQWlFO0lBQ2pFLHdEQUFjLEdBQWQsVUFBZSxJQUFJLEVBQUUsbUJBQW1CLEVBQUUsUUFBUztRQUFuRCxpQkF5RUM7UUF4RUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRTFCLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUNoQixJQUFJLFFBQVEsRUFBRTtZQUNaLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLE1BQU0sS0FBSyxJQUFJLEVBQXJCLENBQXFCLENBQUMsQ0FBQztZQUNqRSxnRUFBZ0U7WUFDaEUsSUFBSSxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN2SSxDQUFDLE1BQU0sS0FBSyxTQUFTLENBQUMsRUFBRTtnQkFDeEIsTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNqQztpQkFBTTtnQkFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQzNCO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBQ2pCLElBQUksbUJBQW1CLEVBQUU7Z0JBQ3ZCLElBQUksUUFBTSxFQUFFLFFBQU0sQ0FBQztnQkFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSztvQkFDaEMsUUFBTSxHQUFHLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDNUssUUFBTSxHQUFHLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssU0FBUyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ3ZJLElBQUksUUFBTSxLQUFLLFFBQU0sRUFBRTt3QkFDckIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO3FCQUVoQztnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEVBQUUsRUFBRSxHQUFHO29CQUMxQixJQUFNLEdBQUcsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsS0FBSyxTQUFTLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7b0JBQzVKLElBQUksR0FBRyxLQUFLLFFBQU0sRUFBRTt3QkFDbEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO3FCQUM1QjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6QixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFBO2lCQUM3QjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDL0I7Z0JBQ0Qsb0NBQW9DO2dCQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzlCO1lBR0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO2dCQUMzQixJQUFJLE9BQU8sQ0FBQyxFQUFFLEVBQUU7b0JBQ2QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsTUFBTSxLQUFLLE9BQU8sQ0FBQyxFQUFFLEVBQTNCLENBQTJCLENBQUMsQ0FBQyxDQUFDO29CQUM1RSxvQ0FBb0M7aUJBQ3JDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFFSCxzQkFBc0I7WUFDdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsRUFBRTtvQkFDcEIsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDbEQsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7cUJBQ3RDO2dCQUVILENBQUMsQ0FBQyxDQUFBO2FBQ0g7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO29CQUN6QixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDdkMsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDO1lBRTdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUVqQztJQUVILENBQUM7SUFFRCxxREFBVyxHQUFYLFVBQVksT0FBc0I7UUFBbEMsaUJBaUZDO1FBaEZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDckIsSUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztRQUNoSyxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsS0FBSyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDN0gsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRTtZQUM1QixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztZQUN4QixPQUFPLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO2dCQUN4QyxJQUFNLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN2QyxJQUFNLElBQUksR0FBRztvQkFDWCxJQUFJLEVBQUUsQ0FBQztvQkFDUCxPQUFPLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7b0JBQ2hDLFNBQVMsRUFBRSxPQUFPO2lCQUNuQixDQUFBO2dCQUNELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDakM7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsOEJBQThCO1FBQzlCLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsS0FBSyxNQUFNLEVBQUU7WUFDM0YsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDdkIsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO2FBRXRCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2dCQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztnQkFDdEIsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRTtvQkFDeEYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQztpQkFDOUM7Z0JBQ0QsaUNBQWlDO2dCQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQSxvQ0FBb0M7Z0JBQzFGLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDOUMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTt3QkFDN0IsS0FBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO3dCQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7NEJBQ3hCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxRQUFRLEVBQUU7Z0NBQ2hFLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUN2Qjt3QkFDSCxDQUFDLENBQUMsQ0FBQztxQkFDSjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1NBRUY7YUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEtBQUssTUFBTSxFQUFFO1lBQ2xHLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1lBQ3ZCLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDckI7O3FCQUVLO2dCQUNMLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ3hELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7Z0JBQ3RCLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUU7b0JBQ3hGLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUM7aUJBQzlDO2dCQUNELElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDOUMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTt3QkFDN0IsS0FBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO3dCQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7NEJBQ3hCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxRQUFRLEVBQUU7Z0NBQ2hFLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUN2Qjt3QkFDSCxDQUFDLENBQUMsQ0FBQztxQkFDSjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1lBQ0Q7O21CQUVPO1NBRVI7YUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssU0FBUyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxLQUFLLFNBQVMsQ0FBQyxFQUFFO1lBQzVKLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUU7Z0JBQ3hGLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUM7YUFDOUM7WUFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztTQUN2QjtRQUNELElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxrREFBUSxHQUFSO1FBQUEsaUJBc0NDO1FBckNDLCtIQUErSDtRQUMvSCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbEQ7O2NBRU07UUFDTixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQSxvQ0FBb0M7UUFDMUY7O2VBRU87UUFDUCxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDOUMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTtnQkFDN0IsS0FBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQ3hCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxRQUFRLEVBQUU7d0JBQ2hFLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUN2QjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDeEIsSUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN2QyxJQUFNLElBQUksR0FBRztnQkFDWCxJQUFJLEVBQUUsQ0FBQztnQkFDUCxPQUFPLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7Z0JBQ2hDLFNBQVMsRUFBRSxPQUFPO2FBQ25CLENBQUE7WUFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2hDLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FDckIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJO1lBQzlDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUU1RyxDQUFDLENBQUMsQ0FDSCxDQUFDO0lBRUosQ0FBQztJQUVELHFEQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLFlBQVksSUFBSSxPQUFBLFlBQVksQ0FBQyxXQUFXLEVBQUUsRUFBMUIsQ0FBMEIsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7O2dCQXByQndCLGNBQWM7Z0JBQ2xCLFVBQVU7Z0JBQ0QsbUJBQW1CO2dCQUMzQixXQUFXO2dCQUNKLGtCQUFrQjtnQkFDakIsbUJBQW1CO2dCQUN6QixhQUFhO2dCQUNULGlCQUFpQjtnQkFDcEIsY0FBYztnQkFDVixrQkFBa0I7Z0JBQ3pCLFdBQVc7Z0JBQ0wsaUJBQWlCOztJQWhFcEM7UUFBUixLQUFLLEVBQUU7b0VBQVM7SUFDUjtRQUFSLEtBQUssRUFBRTswRUFBZTtJQUNkO1FBQVIsS0FBSyxFQUFFO2tGQUF1QjtJQUN0QjtRQUFSLEtBQUssRUFBRTtnRkFBcUI7SUFDcEI7UUFBUixLQUFLLEVBQUU7bUVBQVE7SUFDUDtRQUFSLEtBQUssRUFBRTt3RUFBYTtJQUNaO1FBQVIsS0FBSyxFQUFFOzBFQUFlO0lBQ2Q7UUFBUixLQUFLLEVBQUU7bUVBQVE7SUFDUDtRQUFSLEtBQUssRUFBRTtrRUFBTztJQUVMO1FBQVQsTUFBTSxFQUFFOzRFQUEyQztJQUMxQztRQUFULE1BQU0sRUFBRTt5RUFBd0M7SUFDdkM7UUFBVCxNQUFNLEVBQUU7NEVBQTJDO0lBZ0NqQztRQUFsQixTQUFTLENBQUMsTUFBTSxDQUFDO3NFQUFXO0lBOUNsQiwrQkFBK0I7UUFMM0MsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLDhCQUE4QjtZQUN4QyxzREFBd0Q7O1NBRXpELENBQUM7T0FDVywrQkFBK0IsQ0E0dUIzQztJQUFELHNDQUFDO0NBQUEsQUE1dUJELElBNHVCQztTQTV1QlksK0JBQStCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgU2ltcGxlQ2hhbmdlcywgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBleHRlbmRNb21lbnQgfSBmcm9tICdtb21lbnQtcmFuZ2UnO1xyXG5pbXBvcnQgKiBhcyBNb21lbnQgZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRhc2tTZWFyY2hDb25kaXRpb25zIH0gZnJvbSAnLi4vb2JqZWN0L1Jlc291cmNlTWFuYWdlbWVudENvbXBvbmVudFBhcmFtcyc7XHJcbmltcG9ydCB7IEZpbHRlckhlbHBlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmlsdGVyLWhlbHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvY29uc3RhbnRzL01QTVNlYXJjaE9wZXJhdG9yLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3JOYW1lLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1NlYXJjaENvbmZpZ0NvbnN0YW50cyc7XHJcbmltcG9ydCB7IFNlYXJjaFJlcXVlc3QgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9vYmplY3RzL1NlYXJjaFJlcXVlc3QnO1xyXG5pbXBvcnQgeyBTZWFyY2hSZXNwb25zZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL29iamVjdHMvU2VhcmNoUmVzcG9uc2UnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTU1QTURhdGFUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL09UTU1NUE1EYXRhVHlwZXMnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IFZpZXdDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3ZpZXctY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJbmRleGVyU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2luZGV4ZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3Rhc2tzL3Rhc2suY29uc3RhbnRzJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBTZWFyY2hEYXRhU2VydmljZSB9IGZyb20gJy4uLy4uL3NlYXJjaC9zZXJ2aWNlcy9zZWFyY2gtZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgJCBmcm9tICdqcXVlcnknO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLnNlcnZpY2UnO1xyXG5cclxuY29uc3QgbW9tZW50ID0gZXh0ZW5kTW9tZW50KE1vbWVudCk7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1zeW5jaHJvbml6ZS10YXNrLWZpbHRlcnMnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9zeW5jaHJvbml6ZS10YXNrLWZpbHRlcnMuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3N5bmNocm9uaXplLXRhc2stZmlsdGVycy5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFN5bmNocm9uaXplVGFza0ZpbHRlcnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBASW5wdXQoKSBncm91cEJ5O1xyXG4gIEBJbnB1dCgpIHNlbGVjdGVkTW9udGg7XHJcbiAgQElucHV0KCkgY3VycmVudFZpZXdTdGFydERhdGVzO1xyXG4gIEBJbnB1dCgpIGN1cnJlbnRWaWV3RW5kRGF0ZXM7XHJcbiAgQElucHV0KCkgbGV2ZWxzO1xyXG4gIEBJbnB1dCgpIHZpZXdDb25maWdzO1xyXG4gIEBJbnB1dCgpIHJlbW92ZWRGaWx0ZXI7XHJcbiAgQElucHV0KCkgZGF0ZXNzO1xyXG4gIEBJbnB1dCgpIGRhdGVlO1xyXG5cclxuICBAT3V0cHV0KCkgZGVsaXZlcmFibGVEYXRhID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIHNlbGVjdGVkVXNlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBzZWxlY3RlZEZpbHRlcnMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAvLyBAT3V0cHV0KCkgcmVzb3VyY2VBbGxvY2F0aW9uID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gIGFsbFRlYW1zO1xyXG4gIGFsbFJvbGVzO1xyXG4gIHRlYW1zID0gW107XHJcbiAgcm9sZXMgPSBbXTtcclxuICB1c2VycyA9IFtdO1xyXG4gIG1vbWVudFB0ciA9IG1vbWVudDtcclxuICByZXNvdXJjZTtcclxuICBzZWxlY3RlZExpc3RGaWx0ZXI7XHJcbiAgc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyO1xyXG4gIHZpZXdUeXBlQ2VsbFNpemUgPSAzNTtcclxuICBhbGxVc2VyO1xyXG4gIHVzZXJJZCA9IFtdO1xyXG4gIHNlbGVjdGVkVGVhbTtcclxuICBzZWxlY3RlZFJvbGU7XHJcbiAgc2VsZWN0ZWRJdGVtczogYW55ID0gW107XHJcblxyXG4gIGFsbFVzZXJzID0gW107XHJcbiAgdXNlcklkcyA9IFtdO1xyXG4gIE93bmVyQXJyYXk6IGFueSA9IFtdOyAvLyB7fVxyXG5cclxuICBzZWFyY2hDb25kaXRpb25zID0gW107XHJcbiAgZW1haWxTZWFyY2hDb25kaXRpb24gPSBbXTtcclxuXHJcbiAgY29udmVydGVkRGF0ZSA9IFtdO1xyXG5cclxuICBzdG9yZVNlYXJjaGNvbmRpdGlvbk9iamVjdDogVGFza1NlYXJjaENvbmRpdGlvbnMgPSBudWxsO1xyXG5cclxuICBwcml2YXRlIHN1YnNjcmlwdGlvbnM6IEFycmF5PFN1YnNjcmlwdGlvbj4gPSBbXTtcclxuXHJcbiAgQFZpZXdDaGlsZCgnbmFtZScpIGlucHV0TmFtZTtcclxuXHJcbiAgdGVhbUluaXRpYWwgPSAnJztcclxuICByb2xlSW5pdGlhbCA9ICcnO1xyXG4gIHVzZXJJbml0aWFsID0gJyc7XHJcblxyXG4gIHByZXZpb3VzVXNlcklkID0gW107XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGZpbHRlckhlbHBlclNlcnZpY2U6IEZpbHRlckhlbHBlclNlcnZpY2UsXHJcbiAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIHB1YmxpYyB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgaW5kZXhlclNlcnZpY2U6IEluZGV4ZXJTZXJ2aWNlLFxyXG4gICAgcHVibGljIGRlbGl2ZXJhYmxlU2VydmljZTogRGVsaXZlcmFibGVTZXJ2aWNlLFxyXG4gICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgIHB1YmxpYyBzZWFyY2hEYXRhU2VydmljZTogU2VhcmNoRGF0YVNlcnZpY2UsXHJcbiAgKSB7IH1cclxuXHJcbiAgR2V0Um9sZUROQnlSb2xlSWQocm9sZUlkKSB7XHJcbiAgICBjb25zdCBhbGxSb2xlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsUm9sZXMoKTtcclxuICAgIHJldHVybiBhbGxSb2xlcy5maW5kKHJvbGUgPT4gcm9sZVsnTVBNX0FQUF9Sb2xlcy1pZCddLklkID09PSByb2xlSWQpO1xyXG4gIH1cclxuXHJcbiAgZ2V0Um9sZXNBbmRVc2VyKHRlYW0pIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRUZWFtID0gdGVhbTtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkSXRlbXMuaW5kZXhPZih0ZWFtKSAhPT0gLTEpIHtcclxuICAgICAgY29uc29sZS5sb2coXCJWYWx1ZSBleGlzdHMhXCIpXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSXRlbXMucHVzaCh0ZWFtKTtcclxuICAgIH1cclxuICAgIC8qIHRoaXMuc2VsZWN0ZWRJdGVtcy5wdXNoKHRlYW0pOyAqL1xyXG4gICAgdGhpcy5zZWxlY3RlZEZpbHRlcnMuZW1pdCh0aGlzLnNlbGVjdGVkSXRlbXMpO1xyXG4gICAgY29uc29sZS5sb2codGVhbSk7XHJcbiAgICBjb25zdCBhbGxUZWFtUm9sZXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbFRlYW1Sb2xlcygpO1xyXG4gICAgY29uc29sZS5sb2coYWxsVGVhbVJvbGVzKTtcclxuICAgIHRoaXMuYWxsUm9sZXMgPSBbXTtcclxuICAgIHRoaXMudXNlcnMgPSBbXTtcclxuICAgIGFsbFRlYW1Sb2xlcy5mb3JFYWNoKHRlYW1Sb2xlID0+IHtcclxuICAgICAgaWYgKHRlYW1Sb2xlICYmIHRlYW1Sb2xlLlJfUE9fVEVBTSAhPT0gdW5kZWZpbmVkICYmIHRlYW1Sb2xlLlJfUE9fVEVBTVtcIk1QTV9UZWFtcy1pZFwiXSAhPT0gdW5kZWZpbmVkICYmIHRlYW1Sb2xlLlJfUE9fVEVBTVtcIk1QTV9UZWFtcy1pZFwiXS5JZCAhPT0gdW5kZWZpbmVkICYmIHRlYW1Sb2xlLlJfUE9fVEVBTVtcIk1QTV9UZWFtcy1pZFwiXS5JZCA9PT0gdGVhbVsnTVBNX1RlYW1zLWlkJ10uSWQpIHtcclxuICAgICAgICBjb25zdCB0ZWFtUm9sZXMgPSB0ZWFtUm9sZS8vdGhpcy5HZXRSb2xlRE5CeVJvbGVJZCh0ZWFtUm9sZVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQpOyAvLyB0aGlzLnV0aWxTZXJ2aWNlLkdldFJvbGVETkJ5Um9sZUlkKHRlYW1Sb2xlWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCk7XHJcbiAgICAgICAgY29uc29sZS5sb2codGVhbVJvbGVzKVxyXG4gICAgICAgIGlmICh0ZWFtUm9sZXMgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgdGhpcy5hbGxSb2xlcy5wdXNoKHRlYW1Sb2xlcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMuZ2V0VXNlcnNGb3JUZWFtKHRlYW0pLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcclxuICAgIH0pO1xyXG4gICAgLyogIGNvbnN0IGdldFVzZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgYWxsb2NhdGlvblR5cGU6ICdST0xFJyxcclxuICAgICAgIHJvbGVETjogdGVhbS5ST0xFX0ROLFxyXG4gICAgICAgdGVhbUlEOiB0ZWFtWydNUE1fVGVhbXMtaWQnXS5JZCxcclxuICAgICAgIGlzQXBwcm92ZVRhc2s6IGZhbHNlLFxyXG4gICAgICAgaXNVcGxvYWRUYXNrOiB0cnVlXHJcbiAgICAgfTtcclxuICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0VXNlcnNGb3JUZWFtKGdldFVzZXJCeVJvbGVSZXF1ZXN0KVxyXG4gICAgICAgLnN1YnNjcmliZShhbGxVc2VycyA9PiB7XHJcbiAgICAgICAgIGlmIChhbGxVc2VycyAmJiBhbGxVc2Vycy51c2VycyAmJiBhbGxVc2Vycy51c2Vycy51c2VyKSB7XHJcbiAgICAgICAgICAgYWxsVXNlcnMudXNlcnMudXNlci5mb3JFYWNoKHVzZXIgPT4ge1xyXG4gICAgICAgICAgICAgdGhpcy51c2Vycy5wdXNoKHVzZXIpO1xyXG4gICAgICAgICAgIH0pO1xyXG4gICAgICAgICB9XHJcbiAgICAgICB9KTsgKi9cclxuICB9XHJcblxyXG4gIGdldFVzZXJzRm9yVGVhbSh0ZWFtKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGNvbnN0IGdldFVzZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgIGFsbG9jYXRpb25UeXBlOiAnUk9MRScsXHJcbiAgICAgICAgcm9sZUROOiB0ZWFtLlJPTEVfRE4sXHJcbiAgICAgICAgdGVhbUlEOiB0ZWFtWydNUE1fVGVhbXMtaWQnXS5JZCxcclxuICAgICAgICBpc0FwcHJvdmVUYXNrOiBmYWxzZSxcclxuICAgICAgICBpc1VwbG9hZFRhc2s6IHRydWVcclxuICAgICAgfTtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRVc2VyQnlSb2xlUmVxdWVzdClcclxuICAgICAgICAuc3Vic2NyaWJlKGFsbFVzZXJzID0+IHtcclxuICAgICAgICAgIGlmIChhbGxVc2VycyAmJiBhbGxVc2Vycy51c2VycyAmJiBhbGxVc2Vycy51c2Vycy51c2VyKSB7XHJcbiAgICAgICAgICAgIGFsbFVzZXJzLnVzZXJzLnVzZXIuZm9yRWFjaCh1c2VyID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLnVzZXJzLnB1c2godXNlcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KVxyXG4gIH1cclxuXHJcbiAgZ2V0VGVhbXNBbmRVc2VyKHJvbGUpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRSb2xlID0gcm9sZTtcclxuICAgIC8vICByb2xlLlJPTEVfTkFNRSA9IHJvbGUuUk9MRV9OQU1FLnJlcGxhY2UoL18vZywgJyAnKTtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkSXRlbXMuaW5kZXhPZihyb2xlKSAhPT0gLTEpIHtcclxuICAgICAgY29uc29sZS5sb2coXCJWYWx1ZSBleGlzdHMhXCIpXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSXRlbXMucHVzaChyb2xlKTtcclxuICAgIH1cclxuICAgIC8qIHRoaXMuc2VsZWN0ZWRJdGVtcy5wdXNoKHJvbGUpOyAqL1xyXG4gICAgdGhpcy5zZWxlY3RlZEZpbHRlcnMuZW1pdCh0aGlzLnNlbGVjdGVkSXRlbXMpO1xyXG4gICAgY29uc29sZS5sb2cocm9sZSk7XHJcblxyXG4gICAgdGhpcy5nZXRVc2Vyc0J5Um9sZShyb2xlLCB0aGlzLnNlbGVjdGVkVGVhbSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuXHJcbiAgICB9KTtcclxuXHJcblxyXG4gIH1cclxuXHJcbiAgZ2V0VXNlcnNCeVJvbGUocm9sZSwgc2VsZWN0ZWRUZWFtKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGxldCBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHt9O1xyXG4gICAgICB0aGlzLnVzZXJzID0gW107XHJcbiAgICAgIGdldFVzZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgIGFsbG9jYXRpb25UeXBlOiBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFLFxyXG4gICAgICAgIHJvbGVETjogcm9sZS5ST0xFX0ROLFxyXG4gICAgICAgIHRlYW1JRDogc2VsZWN0ZWRUZWFtID8gc2VsZWN0ZWRUZWFtWydNUE1fVGVhbXMtaWQnXS5JZCA6ICcnLFxyXG4gICAgICAgIGlzQXBwcm92ZVRhc2s6IGZhbHNlLFxyXG4gICAgICAgIGlzVXBsb2FkVGFzazogdHJ1ZVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRVc2VyQnlSb2xlUmVxdWVzdClcclxuICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS51c2VycyAmJiByZXNwb25zZS51c2Vycy51c2VyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJMaXN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ3VzZXInKTtcclxuICAgICAgICAgICAgY29uc3QgdXNlcnMgPSBbXTtcclxuICAgICAgICAgICAgaWYgKHVzZXJMaXN0ICYmIHVzZXJMaXN0Lmxlbmd0aCAmJiB1c2VyTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgdXNlckxpc3QuZm9yRWFjaCh1c2VyID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkT2JqID0gdXNlcnMuZmluZChlID0+IGUudmFsdWUgPT09IHVzZXIuZG4pO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgICB1c2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiB1c2VyLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHVzZXIuY24sXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHVzZXIubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICBjbjogdXNlci5jblxyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnVzZXJzID0gdXNlcnM7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJzID0gW107XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBhbGxvY2F0aW9uVGFzayh1c2VyKSB7XHJcbiAgICBjb25zdCBjYXBhY2l0eSA9IDg7XHJcbiAgICBjb25zdCB0YXNrQWxsb2NhdGlvbiA9IDE7XHJcbiAgICByZXR1cm4gdGFza0FsbG9jYXRpb24gKyAnLycgKyBjYXBhY2l0eTtcclxuICB9XHJcblxyXG4gIHJlc291cmNlVGFza0FsbG9jYXRpb24odXNlciwgY3VycmVudFZpZXdTdGFydERhdGVzLCBjdXJyZW50Vmlld0VuZERhdGVzKSB7XHJcbiAgICBjb25zdCBmaXJzdERhdGUgPSBtb21lbnQoY3VycmVudFZpZXdTdGFydERhdGVzKTtcclxuICAgIGNvbnN0IHNlY29uZERhdGUgPSBtb21lbnQoY3VycmVudFZpZXdFbmREYXRlcyk7XHJcbiAgICBjb25zdCBkaWZmSW5EYXlzID0gTWF0aC5hYnMoZmlyc3REYXRlLmRpZmYoc2Vjb25kRGF0ZSwgJ2RheXMnKSk7XHJcbiAgICBjb25zdCBhbGxvY2F0aW9uID0gW107XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8PSBkaWZmSW5EYXlzOyBpKyspIHtcclxuICAgICAgYWxsb2NhdGlvbltpXSA9IHRoaXMuYWxsb2NhdGlvblRhc2sodXNlcik7Ly8gJzAvOCc7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gYWxsb2NhdGlvbjtcclxuICB9XHJcblxyXG4gIC8qIGdldERlbGl2ZXJhYmxlKHVzZXIsIHJlbW92aW5nRmlsdGVyVmFsdWUpIHtcclxuICAgIGNvbnNvbGUubG9nKHVzZXIpO1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgIHRoaXMudXNlcklkID0gW107XHJcbiAgICBpZiAocmVtb3ZpbmdGaWx0ZXJWYWx1ZSkge1xyXG4gICAgICBsZXQgdXNlcmlkLCBpdGVtaWQ7XHJcbiAgICAgIHRoaXMuYWxsVXNlcnMuZm9yRWFjaCgoaXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgICB1c2VyaWQgPSB1c2VyICYmIHVzZXJbMF0gJiYgdXNlclswXVsnSWRlbnRpdHktaWQnXSAhPT0gdW5kZWZpbmVkICYmIHVzZXJbMF1bJ0lkZW50aXR5LWlkJ10uSWQgPyB1c2VyWzBdWydJZGVudGl0eS1pZCddLklkIDogdXNlciAmJiB1c2VyWzBdICYmIHVzZXJbMF0uY24gPyB1c2VyWzBdLmNuIDogJyc7XHJcbiAgICAgICAgaXRlbWlkID0gaXRlbSAmJiBpdGVtWydJZGVudGl0eS1pZCddICE9PSB1bmRlZmluZWQgJiYgaXRlbVsnSWRlbnRpdHktaWQnXS5JZCA/IGl0ZW1bJ0lkZW50aXR5LWlkJ10uSWQgOiBpdGVtICYmIGl0ZW0uY24gPyBpdGVtLmNuIDogJyc7XHJcbiAgICAgICAgaWYgKGl0ZW1pZCA9PT0gdXNlcmlkKSB7XHJcbiAgICAgICAgICB0aGlzLmFsbFVzZXJzLnNwbGljZShpbmRleCwgMSk7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMudXNlcklkLmZvckVhY2goKGlkLCBpZHgpID0+IHtcclxuICAgICAgICBjb25zdCBpZHMgPSBpZCAmJiBpZFswXSAmJiBpZFswXVsnSWRlbnRpdHktaWQnXSAhPT0gdW5kZWZpbmVkICYmIGlkWzBdWydJZGVudGl0eS1pZCddLklkID8gaWRbMF1bJ0lkZW50aXR5LWlkJ10uSWQgOiBpZCAmJiBpZFswXSAmJiBpZFswXS5jbiA/IGlkWzBdLmNuIDogJydcclxuICAgICAgICBpZiAoaWRzID09PSB1c2VyaWQpIHtcclxuICAgICAgICAgIHRoaXMudXNlcklkLnNwbGljZShpZHgsIDEpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmFsbFVzZXJzLnB1c2godXNlcik7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRJdGVtcy5wdXNoKHVzZXIpO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRmlsdGVycy5lbWl0KHRoaXMuc2VsZWN0ZWRJdGVtcyk7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRVc2VyLmVtaXQodXNlcik7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IHZpZXdDb25maWc7XHJcbiAgICB0aGlzLmFsbFVzZXJzLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgIGlmIChlbGVtZW50LmNuKSB7XHJcbiAgICAgICAgdGhpcy51c2VySWQucHVzaCh0aGlzLmFsbFVzZXIuZmlsdGVyKHVzZXJzID0+IHVzZXJzLlVzZXJJZCA9PT0gZWxlbWVudC5jbikpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgXHJcbiAgICBsZXQgVXNlcklkID0gW107XHJcbiAgICBpZiAodGhpcy51c2VySWQubGVuZ3RoID4gMCkge1xyXG4gICAgICB0aGlzLnVzZXJJZC5mb3JFYWNoKGlkID0+IHtcclxuICAgICAgICBpZiAoVXNlcklkLmluZGV4T2YoaWRbMF1bJ0lkZW50aXR5LWlkJ10uSWQpID09PSAtMSkge1xyXG4gICAgICAgICAgVXNlcklkLnB1c2goaWRbMF1bJ0lkZW50aXR5LWlkJ10uSWQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgIH0pXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmFsbFVzZXJzLmZvckVhY2godXNlcnMgPT4ge1xyXG4gICAgICAgIFVzZXJJZC5wdXNoKHVzZXJzWydJZGVudGl0eS1pZCddLklkKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgZGF0ZXMgPSB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZXMpLmZvcm1hdCgnWVlZWS1NTS1ERFQwMDowMDowMC4wMDAnKSArICdaJyArICcgYW5kICcgKyB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld0VuZERhdGVzKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWic7XHJcblxyXG4gICAgY29uc3QgY29uZGl0aW9uT2JqZWN0OiBUYXNrU2VhcmNoQ29uZGl0aW9ucyA9IHRoaXMuZmlsdGVySGVscGVyU2VydmljZS5nZXRTZWFyY2hDb25kaXRpb25OZXcodGhpcy5zZWxlY3RlZExpc3RGaWx0ZXIsIHRoaXMuc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyLCB0aGlzLmxldmVscywgZGF0ZXMsIFVzZXJJZCk7Ly8gdXNlclsnSWRlbnRpdHktaWQnXS5JZCk7XHJcbiAgICBjb25zdCBvdG1tTVBNRGF0YVR5cGVzID0gT1RNTU1QTURhdGFUeXBlcy5ERUxJVkVSQUJMRTtcclxuXHJcbiAgICBjb25zdCBjb25kaXRpb24gPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5jcmVhdGVDb25kaXRpb25PYmplY3QoJ0NPTlRFTlRfVFlQRScsIE1QTVNlYXJjaE9wZXJhdG9ycy5JUywgTVBNU2VhcmNoT3BlcmF0b3JOYW1lcy5JUywgTVBNU2VhcmNoT3BlcmF0b3JzLkFORCwgb3RtbU1QTURhdGFUeXBlcyk7XHJcbiAgICBjb25kaXRpb24gPyBjb25kaXRpb25PYmplY3QuREVMSVZFUkFCTEVfQ09ORFRJT04ucHVzaChjb25kaXRpb24pIDogY29uc29sZS5sb2coJycpO1xyXG5cclxuXHJcblxyXG5cclxuICAgIGlmICh0aGlzLnZpZXdDb25maWdzKSB7XHJcbiAgICAgIHZpZXdDb25maWcgPSB0aGlzLnZpZXdDb25maWdzO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdmlld0NvbmZpZyA9IFNlYXJjaENvbmZpZ0NvbnN0YW50cy5TRUFSQ0hfTkFNRS5UQVNLO1xyXG4gICAgfVxyXG4gICAgdGhpcy52aWV3Q29uZmlnU2VydmljZS5nZXRBbGxEaXNwbGF5RmVpbGRGb3JWaWV3Q29uZmlnKHZpZXdDb25maWcpLnN1YnNjcmliZSgodmlld0RldGFpbHM6IFZpZXdDb25maWcpID0+IHtcclxuICAgICAgY29uc3Qgc2VhcmNoQ29uZmlnID0gdmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHICYmIHR5cGVvZiB2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcgPT09ICdzdHJpbmcnXHJcbiAgICAgICAgPyBwYXJzZUludCh2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcsIDEwKSA6IG51bGw7XHJcbiAgICAgIGNvbnN0IHBhcmFtZXRlcnM6IFNlYXJjaFJlcXVlc3QgPSB7XHJcbiAgICAgICAgc2VhcmNoX2NvbmZpZ19pZDogc2VhcmNoQ29uZmlnID8gc2VhcmNoQ29uZmlnIDogbnVsbCxcclxuICAgICAgICBrZXl3b3JkOiAnJyxcclxuICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IGNvbmRpdGlvbk9iamVjdC5ERUxJVkVSQUJMRV9DT05EVElPTlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZmFjZXRfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbjogW11cclxuICAgICAgICB9LFxyXG4gICAgICAgIHNvcnRpbmdfbGlzdDoge1xyXG4gICAgICAgICAgc29ydDogW11cclxuICAgICAgICB9LFxyXG4gICAgICAgIGN1cnNvcjoge1xyXG4gICAgICAgICAgcGFnZV9pbmRleDogMCxcclxuICAgICAgICAgIHBhZ2Vfc2l6ZTogMTAwXHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgaWYgKFVzZXJJZC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb25zLnB1c2goXHJcbiAgICAgICAgICB0aGlzLmluZGV4ZXJTZXJ2aWNlLnNlYXJjaChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoKGRhdGE6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLnJlc291cmNlID0gW107XHJcbiAgICAgICAgICAgIHRoaXMuT3duZXJBcnJheSA9IFtdO1xyXG4gICAgICAgICAgICBpZiAoZGF0YS5kYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICBjb25zdCBkRm9ybWF0ID0gJ1lZWVktTU0tREQnO1xyXG4gICAgICAgICAgICAgIGRhdGEuZGF0YS5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IHN0eWxlcyA9IHt9O1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXZlbnRTdGFydERhdGUgPSB0aGlzLm1vbWVudFB0cihpdGVtLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUsIGRGb3JtYXQpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXZlbnRFbmREYXRlID0gdGhpcy5tb21lbnRQdHIoaXRlbS5ERUxJVkVSQUJMRV9EVUVfREFURSwgZEZvcm1hdCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkaWZmID0gKGV2ZW50RW5kRGF0ZS5kaWZmKGV2ZW50U3RhcnREYXRlKSkgLyAoMTAwMCAqIDYwICogNjAgKiAyNCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBleHRyYVBpeGVsID0gKChkaWZmICsgMSkgJSAyKSA9PT0gMCA/IDAgOiAyO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgd2lkdGggPSAoKGRpZmYgKyAxKSAqICh0aGlzLnZpZXdUeXBlQ2VsbFNpemUgKyBleHRyYVBpeGVsKSk7IC8vICgoZGlmZiArIDEpICogKHRoaXMudmlld1R5cGVDZWxsU2l6ZSAtIDEpKTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgT3duZXJOYW1lID0gaXRlbS5ERUxJVkVSQUJMRV9BQ1RJVkVfVVNFUl9OQU1FO1xyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLk93bmVyQXJyYXkuaGFzT3duUHJvcGVydHkoT3duZXJOYW1lKSkge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLk93bmVyQXJyYXlbT3duZXJOYW1lXSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAvLyAgdGhpcy5yZXNvdXJjZVsnYWxsb2NhdGlvbiddID0gWzAgLyA4XTtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5yZXNvdXJjZVRhc2tBbGxvY2F0aW9uKE93bmVyTmFtZSwgdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZXMsIHRoaXMuY3VycmVudFZpZXdFbmREYXRlcyk7XHJcbiAgICAgICAgICAgICAgICAgIC8vICAgdGhpcy5Pd25lckFycmF5ID0geyBbT3duZXJOYW1lXTogW10gfTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGxldCB0b3AgPSAzMCAqIHRoaXMuT3duZXJBcnJheVtPd25lck5hbWVdLmxlbmd0aDtcclxuICAgICAgICAgICAgICAgIHN0eWxlc1snd2lkdGgnXSA9IHdpZHRoICsgJ3B4JztcclxuICAgICAgICAgICAgICAgIHN0eWxlc1sndG9wJ10gPSB0b3AgKyAncHgnO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlRGV0YWlsID0ge1xyXG4gICAgICAgICAgICAgICAgICBkYXRhOiBpdGVtLFxyXG4gICAgICAgICAgICAgICAgICBzdHlsZTogc3R5bGVzXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgbGV0IGs6IGFueSwgdjogYW55O1xyXG4gICAgICAgICAgICAgICAgZm9yIChbaywgdl0gb2YgT2JqZWN0LmVudHJpZXModGhpcy5Pd25lckFycmF5KSkge1xyXG4gICAgICAgICAgICAgICAgICBpZiAodi5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkYXRlID0gdGhpcy5kYXRlZSBcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLk93bmVyQXJyYXlbT3duZXJOYW1lXVsnZGF0ZSddID0gZGF0ZTsvLyB0aGlzLmRhdGVzcztcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5W093bmVyTmFtZV0ucHVzaCh7IGRlbGl2ZXJhYmxlRGV0YWlsIH0pO1xyXG5cclxuXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLk93bmVyQXJyYXkpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vICB0aGlzLnJlc291cmNlLnB1c2godGhpcy5Pd25lckFycmF5KTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMucmVzb3VyY2UpO1xyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIC8vICB0aGlzLnJlc291cmNlLmhlaWdodCA9IDM1ICogZGF0YS5kYXRhLmxlbmd0aCArICdweCc7XHJcbiAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5LmhlaWdodCA9IDM1ICogZGF0YS5kYXRhLmxlbmd0aCArICdweCc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gdGhpcy5kZWxpdmVyYWJsZURhdGEuZW1pdCh0aGlzLnJlc291cmNlKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZURhdGEuZW1pdCh0aGlzLk93bmVyQXJyYXkpO1xyXG4gICAgICAgICAgICB0aGlzLnJlc291cmNlQWxsb2NhdGlvbi5lbWl0KHRoaXMucmVzb3VyY2UpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuT3duZXJBcnJheSA9IFtdO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVEYXRhLmVtaXQodGhpcy5Pd25lckFycmF5KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgIH0pO1xyXG5cclxuICB9ICovXHJcblxyXG4gIGdldFByb3BlcnR5KGRlbGl2ZXJhYmxlRGF0YSwgbWFwcGVyTmFtZSkge1xyXG4gICAgLy8gY29uc3QgbGV2ZWwgPSB0aGlzLmlzUmV2aWV3ZXIgPyBNUE1fTEVWRUxTLkRFTElWRVJBQkxFX1JFVklFVyA6IE1QTV9MRVZFTFMuREVMSVZFUkFCTEU7XHJcbiAgICBjb25zdCBkaXNwbGF5Q29sdW1uID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgbWFwcGVyTmFtZSwgTVBNX0xFVkVMUy5ERUxJVkVSQUJMRSk7XHJcbiAgICByZXR1cm4gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5RGlzcGxheUNvbHVtbihkaXNwbGF5Q29sdW1uLCBkZWxpdmVyYWJsZURhdGEpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICAgKiBAc2luY2UgTVBNVjMtOTQ2XHJcbiAgICAgKiBAcGFyYW0gZGVsaXZlcmFibGVUeXBlXHJcbiAgICAgKi9cclxuICBnZXREZWxpdmVyYWJsZVR5cGVJY29uKGRlbGl2ZXJhYmxlVHlwZSkge1xyXG4gICAgcmV0dXJuIHRoaXMuZGVsaXZlcmFibGVTZXJ2aWNlLmdldERlbGl2ZXJhYmxlVHlwZUljb24oZGVsaXZlcmFibGVUeXBlKTtcclxuICB9XHJcblxyXG4gIHJlbG9hZCh1c2VyKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcInJlbG9hZFwiKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRJdGVtcylcclxuICAgIGNvbnNvbGUubG9nKHVzZXIpO1xyXG4gICAgLy8gdGhpcy5nZXREZWxpdmVyYWJsZSh1c2VyLCBmYWxzZSlcclxuICB9XHJcblxyXG4gIGdldERlbGl2ZXJhYmxlTGlzdChVc2VySWQpIHtcclxuICAgIGxldCB2aWV3Q29uZmlnO1xyXG4gICAgY29uc3QgZGF0ZXMgPSB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZXMpLmZvcm1hdCgnWVlZWS1NTS1ERFQwMDowMDowMC4wMDAnKSArICdaJyArICcgYW5kICcgKyB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld0VuZERhdGVzKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWic7XHJcbiAgICAvL01QTS0yMjI0XHJcbiAgICAvKiAgY29uc3QgY29uZGl0aW9uT2JqZWN0OiBUYXNrU2VhcmNoQ29uZGl0aW9ucyA9IHRoaXMuZmlsdGVySGVscGVyU2VydmljZS5nZXRTZWFyY2hDb25kaXRpb25OZXcodGhpcy5zZWxlY3RlZExpc3RGaWx0ZXIsIHRoaXMuc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyLCBNUE1fTEVWRUxTLlRBU0ssIGRhdGVzLCBVc2VySWQpOyAvLyB0aGlzLmxldmVscy8vIHVzZXJbJ0lkZW50aXR5LWlkJ10uSWQpO1xyXG4gICAqL1xyXG4gICAgY29uc3QgY29uZGl0aW9uT2JqZWN0OiBUYXNrU2VhcmNoQ29uZGl0aW9ucyA9IHRoaXMuZmlsdGVySGVscGVyU2VydmljZS5nZXRTZWFyY2hDb25kaXRpb25OZXcodGhpcy5zZWxlY3RlZExpc3RGaWx0ZXIsIHRoaXMuc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyLCBNUE1fTEVWRUxTLlRBU0ssIHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlcykuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onLCB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld0VuZERhdGVzKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWicsIFVzZXJJZCwgdHJ1ZSk7IC8vIHRoaXMubGV2ZWxzLy8gdXNlclsnSWRlbnRpdHktaWQnXS5JZCk7XHJcbiAgICB0aGlzLnN0b3JlU2VhcmNoY29uZGl0aW9uT2JqZWN0ID0gT2JqZWN0LmFzc2lnbih7fSwgY29uZGl0aW9uT2JqZWN0KTtcclxuICAgIGNvbnN0IG90bW1NUE1EYXRhVHlwZXMgPSBPVE1NTVBNRGF0YVR5cGVzLkRFTElWRVJBQkxFO1xyXG5cclxuLy9NUE0tMjIyNFxyXG4gICAgLyogY29uc3QgY29uZGl0aW9uT2JqZWN0czogVGFza1NlYXJjaENvbmRpdGlvbnMgPSB0aGlzLmZpbHRlckhlbHBlclNlcnZpY2UuZ2V0U2VhcmNoQ29uZGl0aW9uTmV3KHRoaXMuc2VsZWN0ZWRMaXN0RmlsdGVyLCB0aGlzLnNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlciwgdGhpcy5sZXZlbHMsIGRhdGVzLCBVc2VySWQpOyAvLyB0aGlzLmxldmVscy8vIHVzZXJbJ0lkZW50aXR5LWlkJ10uSWQpO1xyXG4gICAgKi9jb25zdCBjb25kaXRpb25PYmplY3RzOiBUYXNrU2VhcmNoQ29uZGl0aW9ucyA9IHRoaXMuZmlsdGVySGVscGVyU2VydmljZS5nZXRTZWFyY2hDb25kaXRpb25OZXcodGhpcy5zZWxlY3RlZExpc3RGaWx0ZXIsIHRoaXMuc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyLCB0aGlzLmxldmVscywgdGhpcy5tb21lbnRQdHIudXRjKHRoaXMuY3VycmVudFZpZXdTdGFydERhdGVzKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWicsIHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZXMpLmZvcm1hdCgnWVlZWS1NTS1ERFQwMDowMDowMC4wMDAnKSArICdaJywgVXNlcklkLCB0cnVlKTsgLy8gdGhpcy5sZXZlbHMvLyB1c2VyWydJZGVudGl0eS1pZCddLklkKTtcclxuXHJcbiAgICAvLyAgdGhpcy5zdG9yZVNlYXJjaGNvbmRpdGlvbk9iamVjdCA9IE9iamVjdC5hc3NpZ24oe30sIGNvbmRpdGlvbk9iamVjdCk7XHJcbiAgICBjb25zdCBvdG1tTVBNRGF0YVR5cGVzcyA9IE9UTU1NUE1EYXRhVHlwZXMuREVMSVZFUkFCTEU7XHJcblxyXG4gICAgY29uc3QgY29uZGl0aW9ucyA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmNyZWF0ZUNvbmRpdGlvbk9iamVjdCgnQ09OVEVOVF9UWVBFJywgTVBNU2VhcmNoT3BlcmF0b3JzLklTLCBNUE1TZWFyY2hPcGVyYXRvck5hbWVzLklTLCBNUE1TZWFyY2hPcGVyYXRvcnMuQU5ELCBvdG1tTVBNRGF0YVR5cGVzKTtcclxuICAgIGNvbmRpdGlvbnMgPyBjb25kaXRpb25PYmplY3RzLkRFTElWRVJBQkxFX0NPTkRUSU9OLnB1c2goY29uZGl0aW9ucykgOiBjb25zb2xlLmxvZygnJyk7XHJcblxyXG4gICAgaWYgKHRoaXMuc2VhcmNoQ29uZGl0aW9ucyAmJiB0aGlzLnNlYXJjaENvbmRpdGlvbnMubGVuZ3RoID4gMCAmJiB0aGlzLnNlYXJjaENvbmRpdGlvbnNbMF0ubWV0YWRhdGFfZmllbGRfaWQgPT09IHRoaXMuc2VhcmNoRGF0YVNlcnZpY2UuS0VZV09SRF9NQVBQRVIpIHtcclxuICAgICAgLy9rZXlXb3JkID0gdGhpcy5zZWFyY2hDb25kaXRpb25zWzBdLmtleXdvcmQ7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuc2VhcmNoQ29uZGl0aW9ucyAmJiB0aGlzLnNlYXJjaENvbmRpdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICBjb25kaXRpb25PYmplY3QuVEFTS19DT05ESVRJT04gPSBjb25kaXRpb25PYmplY3QuVEFTS19DT05ESVRJT04uY29uY2F0KHRoaXMuc2VhcmNoQ29uZGl0aW9ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5lbWFpbFNlYXJjaENvbmRpdGlvbiA9IHRoaXMuZW1haWxTZWFyY2hDb25kaXRpb24gJiYgQXJyYXkuaXNBcnJheSh0aGlzLmVtYWlsU2VhcmNoQ29uZGl0aW9uKSA/IHRoaXMuZW1haWxTZWFyY2hDb25kaXRpb24gOiBbXTtcclxuXHJcbiAgICBpZiAodGhpcy52aWV3Q29uZmlncykge1xyXG4gICAgICB2aWV3Q29uZmlnID0gdGhpcy52aWV3Q29uZmlncztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHZpZXdDb25maWcgPSBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuVEFTSztcclxuICAgIH1cclxuICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3Q29uZmlnKS5zdWJzY3JpYmUoKHZpZXdEZXRhaWxzOiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgIGNvbnN0IHNlYXJjaENvbmZpZyA9IHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyAmJiB0eXBlb2Ygdmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHID09PSAnc3RyaW5nJ1xyXG4gICAgICAgID8gcGFyc2VJbnQodmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHLCAxMCkgOiBudWxsO1xyXG4gICAgICBjb25zdCBwYXJhbWV0ZXJzOiBTZWFyY2hSZXF1ZXN0ID0ge1xyXG4gICAgICAgIHNlYXJjaF9jb25maWdfaWQ6IHNlYXJjaENvbmZpZyA/IHNlYXJjaENvbmZpZyA6IG51bGwsXHJcbiAgICAgICAga2V5d29yZDogJycsXHJcbiAgICAgICAgZ3JvdXBpbmc6IHtcclxuICAgICAgICAgIGdyb3VwX2J5OiAnVEFTSycsXHJcbiAgICAgICAgICBncm91cF9mcm9tX3R5cGU6IG90bW1NUE1EYXRhVHlwZXMsXHJcbiAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogWy4uLmNvbmRpdGlvbk9iamVjdC5ERUxJVkVSQUJMRV9DT05EVElPTiwgLi4udGhpcy5lbWFpbFNlYXJjaENvbmRpdGlvbl1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogY29uZGl0aW9uT2JqZWN0LlRBU0tfQ09ORElUSU9OXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmYWNldF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgZmFjZXRfY29uZGl0aW9uOiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc29ydGluZ19saXN0OiB7XHJcbiAgICAgICAgICBzb3J0OiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY3Vyc29yOiB7XHJcbiAgICAgICAgICBwYWdlX2luZGV4OiAwLFxyXG4gICAgICAgICAgcGFnZV9zaXplOiAxMDAwXHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgY29uc3QgcGFyYW1ldGVyc3M6IFNlYXJjaFJlcXVlc3QgPSB7XHJcbiAgICAgICAgc2VhcmNoX2NvbmZpZ19pZDogc2VhcmNoQ29uZmlnID8gc2VhcmNoQ29uZmlnIDogbnVsbCxcclxuICAgICAgICBrZXl3b3JkOiAnJyxcclxuICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IGNvbmRpdGlvbk9iamVjdHMuREVMSVZFUkFCTEVfQ09ORFRJT05cclxuICAgICAgICB9LFxyXG4gICAgICAgIGZhY2V0X2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICBmYWNldF9jb25kaXRpb246IFtdXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzb3J0aW5nX2xpc3Q6IHtcclxuICAgICAgICAgIHNvcnQ6IFtdXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjdXJzb3I6IHtcclxuICAgICAgICAgIHBhZ2VfaW5kZXg6IDAsXHJcbiAgICAgICAgICBwYWdlX3NpemU6IDEwMFxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgIGlmIChVc2VySWQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKFxyXG4gICAgICAgICAgdGhpcy5pbmRleGVyU2VydmljZS5zZWFyY2gocGFyYW1ldGVycykuc3Vic2NyaWJlKChkYXRhOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmluZGV4ZXJTZXJ2aWNlLnNlYXJjaChwYXJhbWV0ZXJzcykuc3Vic2NyaWJlKChkZWxpdmVyYWJsZURhdGE6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coZGVsaXZlcmFibGVEYXRhKTtcclxuICAgICAgICAgICAgICB0aGlzLnJlc291cmNlID0gW107XHJcbiAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5ID0gW107XHJcbiAgICAgICAgICAgICAgaWYgKGRhdGEuZGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkRm9ybWF0ID0gJ1lZWVktTU0tREQnO1xyXG4gICAgICAgICAgICAgICAgZGF0YS5kYXRhLmZvckVhY2goKGl0ZW0sIGl0ZW1JbmRleCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAvL2NoZWNraW5nIC1kZWxpdmVyYWJsZURhdGEgdG8gZGVsaXZlcmFibGVcclxuICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVEYXRhLmRhdGEuZm9yRWFjaCgoZGVsaXZlcmFibGUsIGl0ZW1JbmRleCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpdGVtLklEID09PSBkZWxpdmVyYWJsZS5UQVNLX0lURU1fSUQuc3BsaXQoJy4nKVsxXSkgey8vZGVsaXZlcmFibGVEYXRhLmRhdGFbaXRlbUluZGV4XS5UQVNLX0lURU1fSUQuc3BsaXQoJy4nKVsxXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgbGV0IHN0eWxlcyA9IHt9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgLyogICBjb25zdCBldmVudFN0YXJ0RGF0ZSA9IHRoaXMubW9tZW50UHRyKGRlbGl2ZXJhYmxlRGF0YS5kYXRhW2l0ZW1JbmRleF0uREVMSVZFUkFCTEVfU1RBUlRfREFURSwgZEZvcm1hdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RW5kRGF0ZSA9IHRoaXMubW9tZW50UHRyKGRlbGl2ZXJhYmxlRGF0YS5kYXRhW2l0ZW1JbmRleF0uREVMSVZFUkFCTEVfRFVFX0RBVEUsIGRGb3JtYXQpOyAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnRTdGFydERhdGUgPSB0aGlzLm1vbWVudFB0cihkZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9TVEFSVF9EQVRFLCBkRm9ybWF0KTtcclxuICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RW5kRGF0ZSA9IHRoaXMubW9tZW50UHRyKGRlbGl2ZXJhYmxlLkRFTElWRVJBQkxFX0RVRV9EQVRFLCBkRm9ybWF0KTtcclxuICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRpZmYgPSAoZXZlbnRFbmREYXRlLmRpZmYoZXZlbnRTdGFydERhdGUpKSAvICgxMDAwICogNjAgKiA2MCAqIDI0KTtcclxuICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV4dHJhUGl4ZWwgPSAoKGRpZmYgKyAxKSAlIDIpID09PSAwID8gMCA6IDE7Ly8gMCA6IDI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAvKiAgIGNvbnN0IGV4dHJhUGl4ZWwgPSAoKGRpZmYgKyAxKSAlIDIpID09PSAwID8gMCA6IDI7ICovXHJcbiAgICAgICAgICAgICAgICAgICAgICBjb25zdCB3aWR0aCA9ICgoZGlmZiArIDEpICogKHRoaXMudmlld1R5cGVDZWxsU2l6ZSArIGV4dHJhUGl4ZWwpKTsgLy8gKChkaWZmICsgMSkgKiAodGhpcy52aWV3VHlwZUNlbGxTaXplIC0gMSkpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgIC8qIGxldCBPd25lck5hbWUgPSBkZWxpdmVyYWJsZURhdGEuZGF0YVtpdGVtSW5kZXhdLkRFTElWRVJBQkxFX0FDVElWRV9VU0VSX05BTUU7ICovXHJcbiAgICAgICAgICAgICAgICAgICAgICBsZXQgT3duZXJOYW1lID0gZGVsaXZlcmFibGUuREVMSVZFUkFCTEVfQUNUSVZFX1VTRVJfTkFNRTtcclxuICAgICAgICAgICAgICAgICAgICAgIGlmICghdGhpcy5Pd25lckFycmF5Lmhhc093blByb3BlcnR5KE93bmVyTmFtZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5W093bmVyTmFtZV0gPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXNvdXJjZVRhc2tBbGxvY2F0aW9uKE93bmVyTmFtZSwgdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZXMsIHRoaXMuY3VycmVudFZpZXdFbmREYXRlcyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgIGxldCB0b3AgPSA0NCAqIHRoaXMuT3duZXJBcnJheVtPd25lck5hbWVdLmxlbmd0aDsvLyAzMFxyXG4gICAgICAgICAgICAgICAgICAgICAgLy8gICAgc3R5bGVzWyd3aWR0aCddID0gKGRpZmYgIT09IDEgPyAoKChkaWZmICsgMSkgJSAyKSA9PT0gMCkgPyAoKHdpZHRoKSArICdweCcpIDogKHdpZHRoICsgNCkgKyAncHgnIDogKHdpZHRoKSArICdweCcpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgIHN0eWxlc1sndG9wJ10gPSB0b3AgKyAncHgnO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlRGV0YWlsID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBkZWxpdmVyYWJsZSwgLy8gZGVsaXZlcmFibGVEYXRhLmRhdGFbaXRlbUluZGV4XSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogKHRoaXMubGV2ZWxzLmluY2x1ZGVzKCdUQVNLJykpID8gaXRlbS5UQVNLX05BTUUgOiBpdGVtLlBST0pFQ1RfTkFNRSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgLyogaWNvbjogdGhpcy5nZXREZWxpdmVyYWJsZVR5cGVJY29uKHRoaXMuZ2V0UHJvcGVydHkoZGVsaXZlcmFibGVEYXRhLmRhdGFbaXRlbUluZGV4XSwgJ0RFTElWRVJBQkxFX1RZUEUnKSkuSUNPTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb246IHRoaXMuZ2V0RGVsaXZlcmFibGVUeXBlSWNvbih0aGlzLmdldFByb3BlcnR5KGRlbGl2ZXJhYmxlLCAnREVMSVZFUkFCTEVfVFlQRScpKS5JQ09OLFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU6IHN0eWxlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgIGxldCBrOiBhbnksIHY6IGFueTtcclxuICAgICAgICAgICAgICAgICAgICAgIGZvciAoW2ssIHZdIG9mIE9iamVjdC5lbnRyaWVzKHRoaXMuT3duZXJBcnJheSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHYubGVuZ3RoID09PSAwKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRhdGUgPSAkLmV4dGVuZCh0cnVlLCBbXSwgdGhpcy5jb252ZXJ0ZWREYXRlKS8vIHRoaXMuY29udmVydGVkRGF0ZTsvL0pTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5kYXRlZSkpLy8kLmV4dGVuZCh0cnVlLFtdLHRoaXMuZGF0ZWUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5W093bmVyTmFtZV1bJ2RhdGUnXSA9IGRhdGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5W093bmVyTmFtZV1bJ2FsbG9jYXRpb24nXSA9IHRoaXMucmVzb3VyY2VUYXNrQWxsb2NhdGlvbihPd25lck5hbWUsIHRoaXMuY3VycmVudFZpZXdTdGFydERhdGVzLCB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLk93bmVyQXJyYXlbT3duZXJOYW1lXS5wdXNoKHsgZGVsaXZlcmFibGVEZXRhaWwgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5LmhlaWdodCA9IDUwICogZGF0YS5kYXRhLmxlbmd0aCArICdweCc7IC8vIDM1IC8vNTBcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRGF0YS5lbWl0KHRoaXMuT3duZXJBcnJheSk7XHJcbiAgICAgICAgICAgICAgLy8gICB0aGlzLnJlc291cmNlQWxsb2NhdGlvbi5lbWl0KHRoaXMucmVzb3VyY2UpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgdGhpcy5Pd25lckFycmF5ID0gW107XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZURhdGEuZW1pdCh0aGlzLk93bmVyQXJyYXkpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8vIHNlYXJjaCBieSB0YXNrLWdyb3VwaW5nIGFuZCBtYXAgdGhlIG5hbWUgdG8gZGVsaXZlcmFibGUgc2VhcmNoXHJcbiAgZ2V0RGVsaXZlcmFibGUodXNlciwgcmVtb3ZpbmdGaWx0ZXJWYWx1ZSwgaXNQYXJlbnQ/KSB7XHJcbiAgICBjb25zb2xlLmxvZyh1c2VyKTtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcblxyXG4gICAgbGV0IFVzZXJJZCA9IFtdO1xyXG4gICAgaWYgKGlzUGFyZW50KSB7XHJcbiAgICAgIGNvbnN0IHVzZXJpZCA9IHRoaXMuYWxsVXNlci5maW5kKHVzZXJzID0+IHVzZXJzLlVzZXJJZCA9PT0gdXNlcik7XHJcbiAgICAgIC8vIGlmICh0aGlzLnByZXZpb3VzVXNlcklkLmluY2x1ZGVzKHVzZXJpZFsnSWRlbnRpdHktaWQnXS5JZCkpIHtcclxuICAgICAgaWYgKCh1c2VyaWQgJiYgdXNlcmlkWydJZGVudGl0eS1pZCddICYmIHVzZXJpZFsnSWRlbnRpdHktaWQnXS5JZCAhPT0gdW5kZWZpbmVkICYmIHRoaXMucHJldmlvdXNVc2VySWQuaW5jbHVkZXModXNlcmlkWydJZGVudGl0eS1pZCddLklkKSkgfHxcclxuICAgICAgICAodXNlcmlkID09PSB1bmRlZmluZWQpKSB7XHJcbiAgICAgICAgVXNlcklkID0gdGhpcy5wcmV2aW91c1VzZXJJZDtcclxuICAgICAgICB0aGlzLmdldERlbGl2ZXJhYmxlTGlzdChVc2VySWQpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudXNlcklkID0gW107XHJcbiAgICAgIGlmIChyZW1vdmluZ0ZpbHRlclZhbHVlKSB7XHJcbiAgICAgICAgbGV0IHVzZXJpZCwgaXRlbWlkO1xyXG4gICAgICAgIHRoaXMuYWxsVXNlcnMuZm9yRWFjaCgoaXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgICAgIHVzZXJpZCA9IHVzZXIgJiYgdXNlclswXSAmJiB1c2VyWzBdWydJZGVudGl0eS1pZCddICE9PSB1bmRlZmluZWQgJiYgdXNlclswXVsnSWRlbnRpdHktaWQnXS5JZCA/IHVzZXJbMF1bJ0lkZW50aXR5LWlkJ10uSWQgOiB1c2VyICYmIHVzZXJbMF0gJiYgdXNlclswXS5jbiA/IHVzZXJbMF0uY24gOiAnJztcclxuICAgICAgICAgIGl0ZW1pZCA9IGl0ZW0gJiYgaXRlbVsnSWRlbnRpdHktaWQnXSAhPT0gdW5kZWZpbmVkICYmIGl0ZW1bJ0lkZW50aXR5LWlkJ10uSWQgPyBpdGVtWydJZGVudGl0eS1pZCddLklkIDogaXRlbSAmJiBpdGVtLmNuID8gaXRlbS5jbiA6ICcnO1xyXG4gICAgICAgICAgaWYgKGl0ZW1pZCA9PT0gdXNlcmlkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWxsVXNlcnMuc3BsaWNlKGluZGV4LCAxKTtcclxuXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy51c2VySWQuZm9yRWFjaCgoaWQsIGlkeCkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgaWRzID0gaWQgJiYgaWRbMF0gJiYgaWRbMF1bJ0lkZW50aXR5LWlkJ10gIT09IHVuZGVmaW5lZCAmJiBpZFswXVsnSWRlbnRpdHktaWQnXS5JZCA/IGlkWzBdWydJZGVudGl0eS1pZCddLklkIDogaWQgJiYgaWRbMF0gJiYgaWRbMF0uY24gPyBpZFswXS5jbiA6ICcnXHJcbiAgICAgICAgICBpZiAoaWRzID09PSB1c2VyaWQpIHtcclxuICAgICAgICAgICAgdGhpcy51c2VySWQuc3BsaWNlKGlkeCwgMSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5hbGxVc2Vycy5wdXNoKHVzZXIpO1xyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkSXRlbXMuaW5kZXhPZih1c2VyKSAhPT0gLTEpIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiVmFsdWUgZXhpc3RzIVwiKVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkSXRlbXMucHVzaCh1c2VyKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLyogdGhpcy5zZWxlY3RlZEl0ZW1zLnB1c2godXNlcik7ICovXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEZpbHRlcnMuZW1pdCh0aGlzLnNlbGVjdGVkSXRlbXMpO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRVc2VyLmVtaXQodXNlcik7XHJcbiAgICAgIH1cclxuXHJcblxyXG4gICAgICB0aGlzLmFsbFVzZXJzLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgICAgaWYgKGVsZW1lbnQuY24pIHtcclxuICAgICAgICAgIHRoaXMudXNlcklkLnB1c2godGhpcy5hbGxVc2VyLmZpbHRlcih1c2VycyA9PiB1c2Vycy5Vc2VySWQgPT09IGVsZW1lbnQuY24pKTtcclxuICAgICAgICAgIC8vICAgdGhpcy51c2VySWRzLnB1c2godGhpcy51c2VySWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgICAvKiBsZXQgVXNlcklkID0gW107ICovXHJcbiAgICAgIGlmICh0aGlzLnVzZXJJZC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy51c2VySWQuZm9yRWFjaChpZCA9PiB7XHJcbiAgICAgICAgICBpZiAoVXNlcklkLmluZGV4T2YoaWRbMF1bJ0lkZW50aXR5LWlkJ10uSWQpID09PSAtMSkge1xyXG4gICAgICAgICAgICBVc2VySWQucHVzaChpZFswXVsnSWRlbnRpdHktaWQnXS5JZCk7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5hbGxVc2Vycy5mb3JFYWNoKHVzZXJzID0+IHtcclxuICAgICAgICAgIFVzZXJJZC5wdXNoKHVzZXJzWydJZGVudGl0eS1pZCddLklkKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5wcmV2aW91c1VzZXJJZCA9IFVzZXJJZDtcclxuXHJcbiAgICAgIHRoaXMuZ2V0RGVsaXZlcmFibGVMaXN0KFVzZXJJZCk7XHJcblxyXG4gICAgfVxyXG5cclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIGNvbnNvbGUubG9nKGNoYW5nZXMpO1xyXG4gICAgY29uc3QgY3VycmVudFN0YXJ0RGF0ZSA9IHRoaXMuY3VycmVudFZpZXdTdGFydERhdGVzICYmIHRoaXMuY3VycmVudFZpZXdTdGFydERhdGVzLl9pICE9PSB1bmRlZmluZWQgPyB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlcy5faSA6IHRoaXMuY3VycmVudFZpZXdTdGFydERhdGVzO1xyXG4gICAgaWYgKGNoYW5nZXMgJiYgY2hhbmdlcy5kYXRlZSAmJiBjaGFuZ2VzLmRhdGVlLmN1cnJlbnRWYWx1ZSAmJiBjaGFuZ2VzLmRhdGVlLmN1cnJlbnRWYWx1ZVswXSAmJiBjaGFuZ2VzLmRhdGVlLmN1cnJlbnRWYWx1ZVswXS5faSAmJlxyXG4gICAgICAhY2hhbmdlcy5kYXRlZS5maXJzdENoYW5nZSkge1xyXG4gICAgICB0aGlzLmNvbnZlcnRlZERhdGUgPSBbXTtcclxuICAgICAgY2hhbmdlcy5kYXRlZS5jdXJyZW50VmFsdWUuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICBjb25zdCB4ID0gZWxlbWVudC5mb3JtYXQoJ1lZWVktTU0tREQnKTtcclxuICAgICAgICBjb25zdCBkYXRlID0ge1xyXG4gICAgICAgICAgZGF0ZTogeCxcclxuICAgICAgICAgIGVsZW1lbnQ6IGVsZW1lbnQuZm9ybWF0KCdkZCBERCcpLFxyXG4gICAgICAgICAgaXNPdmVybGFwOiAnZmFsc2UnXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuY29udmVydGVkRGF0ZS5wdXNoKGRhdGUpO1xyXG4gICAgICB9KTtcclxuICAgICAgY29uc29sZS5sb2codGhpcy5jb252ZXJ0ZWREYXRlKTtcclxuICAgIH1cclxuICAgIHRoaXMuZ2V0RGVsaXZlcmFibGUodGhpcy5yZW1vdmVkRmlsdGVyLCB0cnVlKTtcclxuICAgIC8vIFRvIGNsZWFyIHRoZSBzZWxlY3RlZCB2YWx1ZVxyXG4gICAgaWYgKHRoaXMucmVtb3ZlZEZpbHRlciAmJiB0aGlzLnJlbW92ZWRGaWx0ZXIubGVuZ3RoID4gMCAmJiB0aGlzLnJlbW92ZWRGaWx0ZXJbMF0gPT09ICdURUFNJykge1xyXG4gICAgICB0aGlzLnRlYW1Jbml0aWFsID0gJyc7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRUZWFtID0gJyc7XHJcbiAgICAgIGlmICh0aGlzLnNlbGVjdGVkUm9sZSkge1xyXG5cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnJvbGVJbml0aWFsID0gJyc7XHJcbiAgICAgICAgdGhpcy51c2VySW5pdGlhbCA9ICcnO1xyXG4gICAgICAgIGlmICh0aGlzLmlucHV0TmFtZSAmJiB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50ICYmIHRoaXMuaW5wdXROYW1lLm5hdGl2ZUVsZW1lbnQudmFsdWUpIHtcclxuICAgICAgICAgIHRoaXMuaW5wdXROYW1lLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSAnVXNlcnMnO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBhZGRlZCBiYWNrIGFsbCByb2xlcyBhbmQgdXNlcnNcclxuICAgICAgICB0aGlzLmFsbFJvbGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxUZWFtUm9sZXMoKTsvL3RoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsUm9sZXMoKTtcclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsVXNlcnMoKS5zdWJzY3JpYmUoYWxsVXNlcnMgPT4ge1xyXG4gICAgICAgICAgaWYgKGFsbFVzZXJzICYmIGFsbFVzZXJzLlVzZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5hbGxVc2VyID0gYWxsVXNlcnMuVXNlcjtcclxuICAgICAgICAgICAgYWxsVXNlcnMuVXNlci5mb3JFYWNoKHVzZXIgPT4ge1xyXG4gICAgICAgICAgICAgIGlmICh1c2VyICYmIHVzZXIuRnVsbE5hbWUgJiYgdHlwZW9mICh1c2VyLkZ1bGxOYW1lKSAhPT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudXNlcnMucHVzaCh1c2VyKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgfSBlbHNlIGlmICh0aGlzLnJlbW92ZWRGaWx0ZXIgJiYgdGhpcy5yZW1vdmVkRmlsdGVyLmxlbmd0aCA+IDAgJiYgdGhpcy5yZW1vdmVkRmlsdGVyWzBdID09PSAnUk9MRScpIHtcclxuICAgICAgdGhpcy5yb2xlSW5pdGlhbCA9ICcnO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkUm9sZSA9ICcnO1xyXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZFRlYW0pIHtcclxuICAgICAgICAvKiAgaWYgKHRoaXMuaW5wdXROYW1lICYmIHRoaXMuaW5wdXROYW1lLm5hdGl2ZUVsZW1lbnQgJiYgdGhpcy5pbnB1dE5hbWUubmF0aXZlRWxlbWVudC52YWx1ZSkge1xyXG4gICAgICAgICAgIHRoaXMuaW5wdXROYW1lLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSAnVXNlcnMnO1xyXG4gICAgICAgICB9ICovXHJcbiAgICAgICAgdGhpcy5nZXRVc2Vyc0ZvclRlYW0odGhpcy5zZWxlY3RlZFRlYW0pLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy51c2VySW5pdGlhbCA9ICcnO1xyXG4gICAgICAgIGlmICh0aGlzLmlucHV0TmFtZSAmJiB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50ICYmIHRoaXMuaW5wdXROYW1lLm5hdGl2ZUVsZW1lbnQudmFsdWUpIHtcclxuICAgICAgICAgIHRoaXMuaW5wdXROYW1lLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSAnVXNlcnMnO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsVXNlcnMoKS5zdWJzY3JpYmUoYWxsVXNlcnMgPT4ge1xyXG4gICAgICAgICAgaWYgKGFsbFVzZXJzICYmIGFsbFVzZXJzLlVzZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5hbGxVc2VyID0gYWxsVXNlcnMuVXNlcjtcclxuICAgICAgICAgICAgYWxsVXNlcnMuVXNlci5mb3JFYWNoKHVzZXIgPT4ge1xyXG4gICAgICAgICAgICAgIGlmICh1c2VyICYmIHVzZXIuRnVsbE5hbWUgJiYgdHlwZW9mICh1c2VyLkZ1bGxOYW1lKSAhPT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudXNlcnMucHVzaCh1c2VyKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIC8qICB0aGlzLmdldFVzZXJzQnlSb2xlKHRoaXMuc2VsZWN0ZWRSb2xlLCB0aGlzLnNlbGVjdGVkVGVhbSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuIFxyXG4gICAgICAgfSk7ICovXHJcblxyXG4gICAgfSBlbHNlIGlmICh0aGlzLnJlbW92ZWRGaWx0ZXIgJiYgdGhpcy5yZW1vdmVkRmlsdGVyLmxlbmd0aCA+IDAgJiYgKHRoaXMucmVtb3ZlZEZpbHRlclswXS5uYW1lICE9PSB1bmRlZmluZWQgfHwgdGhpcy5yZW1vdmVkRmlsdGVyWzBdLkZ1bGxOYW1lICE9PSB1bmRlZmluZWQpKSB7XHJcbiAgICAgIGlmICh0aGlzLmlucHV0TmFtZSAmJiB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50ICYmIHRoaXMuaW5wdXROYW1lLm5hdGl2ZUVsZW1lbnQudmFsdWUpIHtcclxuICAgICAgICB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50LnZhbHVlID0gJ1VzZXJzJztcclxuICAgICAgfVxyXG4gICAgICB0aGlzLnVzZXJJbml0aWFsID0gJyc7XHJcbiAgICB9XHJcbiAgICB0aGlzLnJlbW92ZWRGaWx0ZXIgPSBbXTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgLy8gdGhpcy5sZXZlbCA9IHRoaXMuZ3JvdXBCeSAmJiB0aGlzLmdyb3VwQnkudmFsdWUgJiYgdGhpcy5ncm91cEJ5LnZhbHVlLnNwbGl0KCdfJylbMl0gPyB0aGlzLmdyb3VwQnkudmFsdWUuc3BsaXQoJ18nKVsyXSA6ICcnO1xyXG4gICAgdGhpcy5hbGxUZWFtcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsVGVhbXMoKTtcclxuICAgIC8qIHRoaXMuYWxsVGVhbXMuZm9yRWFjaCh0ZWFtID0+IHtcclxuICAgICAgdGhpcy50ZWFtcy5wdXNoKHRlYW0pO1xyXG4gICAgfSk7ICovXHJcbiAgICB0aGlzLmFsbFJvbGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxUZWFtUm9sZXMoKTsvL3RoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsUm9sZXMoKTtcclxuICAgIC8qICB0aGlzLmFsbFJvbGVzLmZvckVhY2gocm9sZSA9PiB7XHJcbiAgICAgICByb2xlLlJPTEVfTkFNRSA9IHJvbGUuUk9MRV9OQU1FLnJlcGxhY2UoL18vZywgJyAnKTtcclxuICAgICB9KTsgKi9cclxuICAgIHRoaXMuYXBwU2VydmljZS5nZXRBbGxVc2VycygpLnN1YnNjcmliZShhbGxVc2VycyA9PiB7XHJcbiAgICAgIGlmIChhbGxVc2VycyAmJiBhbGxVc2Vycy5Vc2VyKSB7XHJcbiAgICAgICAgdGhpcy5hbGxVc2VyID0gYWxsVXNlcnMuVXNlcjtcclxuICAgICAgICBhbGxVc2Vycy5Vc2VyLmZvckVhY2godXNlciA9PiB7XHJcbiAgICAgICAgICBpZiAodXNlciAmJiB1c2VyLkZ1bGxOYW1lICYmIHR5cGVvZiAodXNlci5GdWxsTmFtZSkgIT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXNlcnMucHVzaCh1c2VyKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLmRhdGVlLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgIGNvbnN0IHggPSBlbGVtZW50LmZvcm1hdCgnWVlZWS1NTS1ERCcpO1xyXG4gICAgICBjb25zdCBkYXRlID0ge1xyXG4gICAgICAgIGRhdGU6IHgsXHJcbiAgICAgICAgZWxlbWVudDogZWxlbWVudC5mb3JtYXQoJ2RkIEREJyksXHJcbiAgICAgICAgaXNPdmVybGFwOiAnZmFsc2UnXHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jb252ZXJ0ZWREYXRlLnB1c2goZGF0ZSk7XHJcbiAgICB9KTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuY29udmVydGVkRGF0ZSk7XHJcbiAgICAvLyB0byBjaGFuZ2Ugc2VhcmNoXHJcbiAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChcclxuICAgICAgdGhpcy5zZWFyY2hEYXRhU2VydmljZS5zZWFyY2hEYXRhLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICB0aGlzLnNlYXJjaENvbmRpdGlvbnMgPSAoZGF0YSAmJiBBcnJheS5pc0FycmF5KGRhdGEpID8gdGhpcy5vdG1tU2VydmljZS5hZGRSZWxhdGlvbmFsT3BlcmF0b3IoZGF0YSkgOiBbXSk7XHJcblxyXG4gICAgICB9KVxyXG4gICAgKTtcclxuXHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5mb3JFYWNoKHN1YnNjcmlwdGlvbiA9PiBzdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==