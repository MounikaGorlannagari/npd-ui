import { __decorate, __read, __spread } from "tslib";
import { Component, ViewChildren, Output, EventEmitter } from '@angular/core';
import { ChangeDetectorRef, ElementRef, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { extendMoment } from 'moment-range';
import * as Moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UtilService } from '../../mpm-utils/services/util.service';
import { FacetChangeEventService } from '../../facet/services/facet-change-event.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { SearchChangeEventService } from '../../search/services/search-change-event.service';
import { DeliverableTaskMetadataService } from '../../deliverable-task/service/deliverable.task.metadata.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { SearchDataService } from '../../search/services/search-data.service';
import { IndexerService } from '../../shared/services/indexer/indexer.service';
import { TitleCasePipe } from '@angular/common';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AppService } from '../../mpm-utils/services/app.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { TaskService } from '../../project/tasks/task.service';
import { CalendarConstants } from '../shared/constants/Calendar.constants';
import { MatSortDirections } from '../../mpm-utils/objects/MatSortDirection';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { SearchConfigConstants } from '../../mpm-utils/objects/SearchConfigConstants';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { DeliverableTypes } from '../../mpm-utils/objects/DeliverableTypes';
import { TaskConstants } from '../../project/tasks/task.constants';
import { trigger, state, style, transition, animate } from '@angular/animations';
import * as acronui from './../../mpm-utils/auth/utility';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { GROUP_BY_ALL_FILTERS, GROUP_BY_FILTERS } from '../../shared/constants/GroupByFilter';
import { GroupByFilterTypes } from '../../shared/constants/GroupByFilterTypes';
import { RouteService } from '../../shared/services/route.service';
import { FilterHelperService } from '../../shared/services/filter-helper.service';
import { DeliverableService } from '../../project/tasks/deliverable/deliverable.service';
var moment = extendMoment(Moment);
var ResourceManagementToolbarComponent = /** @class */ (function () {
    function ResourceManagementToolbarComponent(utilService, facetChangeEventService, routeService, activatedRoute, sharingService, renderer, media, changeDetectorRef, searchChangeEventService, deliverableTaskMetadataService, viewConfigService, loaderService, notificationService, filterHelperService, searchDataService, indexerService, titlecasePipe, otmmService, appService, fieldConfigService, taskService, deliverableService) {
        var _this = this;
        this.utilService = utilService;
        this.facetChangeEventService = facetChangeEventService;
        this.routeService = routeService;
        this.activatedRoute = activatedRoute;
        this.sharingService = sharingService;
        this.renderer = renderer;
        this.media = media;
        this.changeDetectorRef = changeDetectorRef;
        this.searchChangeEventService = searchChangeEventService;
        this.deliverableTaskMetadataService = deliverableTaskMetadataService;
        this.viewConfigService = viewConfigService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.filterHelperService = filterHelperService;
        this.searchDataService = searchDataService;
        this.indexerService = indexerService;
        this.titlecasePipe = titlecasePipe;
        this.otmmService = otmmService;
        this.appService = appService;
        this.fieldConfigService = fieldConfigService;
        this.taskService = taskService;
        this.deliverableService = deliverableService;
        this.componentParams = this.activatedRoute.queryParamMap.pipe(map(function (params) { return _this.resourceManagementComponentRoute(params); }));
        this.subscriptions = [];
        this.addedResourceHandler = new EventEmitter();
        this.groupByFilters = [];
        this.isReset = false;
        this.isRoute = false;
        this.totalListDataCount = 0;
        this.displayType = 'month';
        this.viewTypeCellSize = 150; // 35;
        this.momentPtr = moment;
        this.dates = [];
        this.showWeekends = true;
        this.taskAllocationOverloadStyle = {};
        this.taskAllocationStyle = {};
        this.cellStyle = {};
        /*  months = ['January', 'february', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
         */ this.facetExpansionState = true;
        this.myTaskViewConfigDetails = null;
        this.searchConfigId = -1;
        this.componentRendering = false;
        this.isPageRefresh = true;
        this.isDataRefresh = true;
        this.storeSearchconditionObject = null;
        this.sortableFields = [];
        this.searchConditions = [];
        this.emailSearchCondition = [];
        this.validateFacet = false;
        this.sidebarStyle = {};
        this.scrolled = false;
        this.isWeekView = false;
        this.previousFlag = false;
        this.nextFlag = false;
        this.allUsers = [];
        this.UNASSIGNED_TASK_SEARCH_LIMIT = 100;
        this.eventDisplay = {
            displayView: 'dayView',
            duration: { days: 30 },
            resources: []
        };
        this.allFilteredResources = [];
        this.usersUnavailabity = [];
        this.allUsersCapacity = [];
        this.projectListData = [];
        this.deliverableData = [];
        this.Math = Math;
        /** * Scroll for Fixing the dates and header */
        this.scrolling = function (el) {
            var scrollTop = el.srcElement.scrollTop;
            if (scrollTop >= 100) {
                _this.scrolled = true;
            }
            else {
                _this.scrolled = false;
            }
        };
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = function () { return _this.changeDetectorRef.detectChanges(); };
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
    }
    // PREVIOUS navigation for Calendar
    ResourceManagementToolbarComponent.prototype.calendarPrevious = function () {
        this.previousFlag = false;
        console.log(this.selectedMonth);
        for (var i = 0; i < CalendarConstants.months.length; i++) {
            if (CalendarConstants.months[i + 1] === this.selectedMonth) {
                console.log(CalendarConstants.months[i]);
                this.selectedMonth = CalendarConstants.months[i];
                this.previousFlag = true;
                break;
            }
            else if (CalendarConstants.months[0] === this.selectedMonth) {
                this.selectedMonth = CalendarConstants.months[CalendarConstants.months.length - 1];
                this.year = this.year - 1;
                break;
            }
        }
        // to have exact all date
        var noOfDays = this.getNoOfDays(this.selectedMonth);
        var days;
        if (this.displayType === 'week') {
            var startDate = this.momentPtr(this.currentViewStartDate).startOf('week').subtract(1, 'week');
            var endDate = this.momentPtr(this.currentViewStartDate).subtract(1, 'day');
            this.currentViewStartDate = startDate;
            this.currentViewEndDate = endDate;
            var range = this.momentPtr.range(startDate, endDate);
            days = Array.from(range.by('days'));
        }
        if (this.displayType === 'month') {
            var startDate = this.momentPtr(this.currentViewStartDate).subtract(noOfDays, 'days'); // 30
            var endDate = this.momentPtr(this.currentViewStartDate).subtract(1, 'day');
            this.currentViewStartDate = startDate;
            this.currentViewEndDate = endDate;
            var range = this.momentPtr.range(startDate, endDate);
            days = Array.from(range.by('days'));
        }
        if (this.showWeekends) {
            this.dates = days;
        }
        else {
            // remove sat and sun from date range
            this.dates = days.filter(function (d) { return d.isoWeekday() !== 6 && d.isoWeekday() !== 7; });
        }
        //  this.resizeCalendar();
        this.reload(false);
    };
    // NEXT navigation for Calendar
    ResourceManagementToolbarComponent.prototype.calendarNext = function () {
        this.nextFlag = false;
        console.log(this.selectedMonth);
        if (this.selectedMonth === CalendarConstants.months[CalendarConstants.months.length - 1]) {
            this.selectedMonth = CalendarConstants.months[0];
            this.year = this.year + 1;
        }
        else {
            for (var i = 0; i < CalendarConstants.months.length && this.selectedMonth !== CalendarConstants.months[CalendarConstants.months.length - 1]; i++) {
                if (CalendarConstants.months[i] === this.selectedMonth) {
                    console.log(CalendarConstants.months[i + 1]);
                    this.selectedMonth = CalendarConstants.months[i + 1];
                    this.nextFlag = true;
                    break;
                }
            }
        }
        /* if (this.selectedMonth === CalendarConstants.months[CalendarConstants.months.length - 1]) {
          this.selectedMonth = CalendarConstants.months[0];
        } */
        var noOfDays = this.getNoOfDays(this.selectedMonth);
        var days;
        if (this.displayType === 'week') {
            var startDate = this.momentPtr(this.currentViewStartDate).endOf('week').add(1, 'day');
            var endDate = this.momentPtr(startDate).add(1, 'week');
            this.currentViewStartDate = startDate;
            this.currentViewEndDate = endDate;
            var range = this.momentPtr.range(startDate, endDate);
            days = Array.from(range.by('days'));
        }
        if (this.displayType === 'month') {
            var startDate = this.momentPtr(this.currentViewEndDate).add(1, 'day');
            var endDate = this.momentPtr(this.currentViewEndDate).add(noOfDays, 'days'); // 30
            this.currentViewStartDate = startDate;
            this.currentViewEndDate = endDate;
            var range = this.momentPtr.range(startDate, endDate);
            days = Array.from(range.by('days'));
        }
        if (this.showWeekends) {
            this.dates = days;
        }
        else {
            // remove sat and sun from the date range
            this.dates = days.filter(function (d) { return d.isoWeekday() !== 6 && d.isoWeekday() !== 7; });
        }
        // this.resizeCalendar();
        this.reload(false);
    };
    ResourceManagementToolbarComponent.prototype.facetToggled = function (event) {
        this.facetExpansionState = event;
    };
    ResourceManagementToolbarComponent.prototype.selectedUserr = function (user) {
        this.selectedUsers = user;
    };
    ResourceManagementToolbarComponent.prototype.selectedFilter = function (filter) {
        this.selectedItems = filter;
    };
    ResourceManagementToolbarComponent.prototype.resourceManagementComponentRoute = function (params) {
        var groupByFilter = params.get('groupByFilter') || '';
        var deliverableTaskFilter = params.get('deliverableTaskFilter') || '';
        var listDependentFilter = params.get('listDependentFilter') || '';
        var emailLinkId = params.get('emailLinkId') || '';
        var sortBy = params.get('sortBy') || '';
        var sortOrder = params.get('sortOrder') === 'asc' ? MatSortDirections.ASC : (params.get('sortOrder') === 'desc' ? MatSortDirections.DESC : null);
        var pageNumber = params.get('pageNumber') ? Number(params.get('pageNumber')) : null;
        var pageSize = params.get('pageSize') ? Number(params.get('pageSize')) : null;
        var searchName = params.get('searchName') ? params.get('searchName') : null;
        var savedSearchName = params.get('savedSearchName') ? params.get('savedSearchName') : null;
        var advSearchData = params.get('advSearchData') ? params.get('advSearchData') : null;
        var facetData = params.get('facetData') ? params.get('facetData') : null;
        return {
            groupByFilter: groupByFilter,
            deliverableTaskFilter: deliverableTaskFilter,
            listDependentFilter: listDependentFilter,
            emailLinkId: emailLinkId,
            sortBy: sortBy,
            sortOrder: sortOrder,
            pageNumber: pageNumber,
            pageSize: pageSize,
            searchName: searchName,
            savedSearchName: savedSearchName,
            advSearchData: advSearchData,
            facetData: facetData
        };
    };
    ResourceManagementToolbarComponent.prototype.setLevel = function () {
        if (this.selectedGroupByFilter.value === GroupByFilterTypes.GROUP_BY_CAMPAIGN) {
            this.level = MPM_LEVELS.CAMPAIGN;
            this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_CAMPAIGN;
        }
        else if (this.selectedGroupByFilter.value === GroupByFilterTypes.GROUP_BY_PROJECT) {
            this.level = MPM_LEVELS.PROJECT;
            this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_PROJECT;
        }
        else if (this.selectedGroupByFilter.value === GroupByFilterTypes.GROUP_BY_TASK) {
            this.level = MPM_LEVELS.TASK;
            this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_TASK;
        }
    };
    ResourceManagementToolbarComponent.prototype.groupByFilterChange = function (option) {
        this.isReset = true;
        this.selectedGroupByFilter = option;
        this.facetRestrictionList = {};
        this.loaderService.show();
        this.facetChangeEventService.resetFacet();
        this.setLevel();
        this.getLevelDetails();
        if (!(this.searchName || this.savedSearchName || this.advSearchData || this.facetData)) {
            this.isRoute = true;
        }
        this.routeComponentView();
        this.loaderService.hide();
    };
    ResourceManagementToolbarComponent.prototype.resetPagination = function () {
        this.page = 1;
        this.skip = 0;
    };
    ResourceManagementToolbarComponent.prototype.routeComponentView = function () {
        var queryParams = {};
        if (this.selectedGroupByFilter.value) {
            queryParams['groupByFilter'] = this.selectedGroupByFilter.value;
        }
        if (this.selectedSort) {
            queryParams['sortBy'] = this.selectedSortBy ? this.selectedSortBy : null;
            queryParams['sortOrder'] = this.selectedSortOrder ? this.selectedSortOrder : null;
        }
        if (this.searchName) {
            queryParams['searchName'] = this.searchName;
        }
        if (this.savedSearchName) {
            queryParams['savedSearchName'] = this.savedSearchName;
        }
        if (this.advSearchData) {
            queryParams['advSearchData'] = this.advSearchData;
        }
        if (this.facetData) {
            queryParams['facetData'] = this.facetData;
        }
        if (this.pageSize) {
            queryParams['pageSize'] = this.pageSize;
        }
        if (this.page) {
            if (this.isReset) {
                this.resetPagination();
                this.selectedPageNumber = this.page;
                this.isReset = false;
            }
            queryParams['pageNumber'] = this.page;
        }
        this.routeService.goToResourceManagement(queryParams, this.searchName, this.savedSearchName, this.advSearchData, this.facetData);
    };
    // toggle month , week and day view
    ResourceManagementToolbarComponent.prototype.toggleView = function (viewType) {
        console.log('viewType', viewType);
        var days;
        this.displayType = viewType;
        var isReinitialize = true;
        if (this.displayType === 'week') {
            this.viewTypeCellSize = 150;
            var today = this.momentPtr({});
            var fromDate = this.momentPtr({}).startOf('week');
            var toDate = this.momentPtr({}).endOf('week');
            this.currentViewStartDate = fromDate;
            this.currentViewEndDate = toDate;
            var range = this.momentPtr.range(fromDate, toDate);
            days = Array.from(range.by('days'));
        }
        else if (this.displayType === 'day') {
            var fromDate = this.momentPtr({});
            var toDate = this.momentPtr().add(0, 'days');
            this.currentViewStartDate = fromDate;
            this.currentViewEndDate = toDate;
            var range = this.momentPtr.range(fromDate, toDate);
            days = Array.from(range.by('days'));
        }
        else {
            this.viewTypeCellSize = 35;
            var fromDate = this.momentPtr({});
            var toDate = this.momentPtr().add(29, 'days');
            this.currentViewStartDate = fromDate;
            this.currentViewEndDate = toDate;
            var range = this.momentPtr.range(fromDate, toDate);
            days = Array.from(range.by('days'));
        }
        this.dates = days;
        if (!this.showWeekends) { // do not show weekends
            // remove sat and sun from the date range
            this.dates = days.filter(function (d) { return d.isoWeekday() !== 6 && d.isoWeekday() !== 7; });
        }
        // this.resizeCalendar();
    };
    // Resizing the calendar size based on the users display and for month and week view
    ResourceManagementToolbarComponent.prototype.resizeCalendar = function () {
        var _this = this;
        this.calculateViewSize();
        this.calendarHeaders.forEach((function (elem, index) {
            _this.renderer.setStyle(elem.nativeElement, 'width', _this.viewTypeCellSize + 'px');
        }));
        this.calendarCells.forEach((function (elem, index) {
            _this.renderer.setStyle(elem.nativeElement, 'width', _this.viewTypeCellSize + 'px');
        }));
    };
    // calculate the view size for calendar container
    ResourceManagementToolbarComponent.prototype.calculateViewSize = function () {
        var windowWidth = window.innerWidth;
        // its always constant value than the window width to fit in correctly in px
        var rightMargin = 255;
        // this is going the be the calendar container width
        var calendarViewWidth = Math.floor(windowWidth - rightMargin);
        this.viewTypeCellSize = Math.floor(calendarViewWidth / this.dates.length);
        this.renderer.setStyle(this.calendarView.nativeElement, 'width', calendarViewWidth + 'px');
        this.taskAllocationOverloadStyle = {
            'background-color': 'red',
            width: (this.viewTypeCellSize) + 'px'
        };
        this.taskAllocationStyle = {
            'background-color': '#0895CE',
            width: (this.viewTypeCellSize) + 'px'
        };
        this.cellStyle = {
            width: (this.viewTypeCellSize + 1) + 'px'
        };
    };
    ResourceManagementToolbarComponent.prototype.getProperty = function (deliverableData, mapperName) {
        // const level = this.isReviewer ? MPM_LEVELS.DELIVERABLE_REVIEW : MPM_LEVELS.DELIVERABLE;
        var displayColumn = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.DELIVERABLE);
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, deliverableData);
    };
    /**
       * @since MPMV3-946
       * @param deliverableType
       */
    ResourceManagementToolbarComponent.prototype.getDeliverableTypeIcon = function (deliverableType) {
        return this.deliverableService.getDeliverableTypeIcon(deliverableType);
    };
    ResourceManagementToolbarComponent.prototype.expandRow = function (event, resource) {
        var _this = this;
        console.log(resource);
        event.stopPropagation();
        this.loaderService.show();
        var viewConfig;
        if (resource.deliverableData && Array.isArray(resource.deliverableData) && resource.deliverableData.length > 0) {
            this.loaderService.hide();
            return;
        }
        var displayField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, 
        // tslint:disable-next-line: max-line-length
        this.level === MPM_LEVELS.CAMPAIGN ? MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_CAMPAIGN_ID : this.level === MPM_LEVELS.PROJECT ? MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_PROJECT_ID : MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TASK_ID, MPM_LEVELS.DELIVERABLE);
        if (!displayField || !resource['ITEM_ID']) {
            this.loaderService.hide();
            return;
        }
        // MPM-V3-2224 - comment below one
        // const dates = this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z';
        // const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level, dates, undefined, true);
        // MPM-V3-2224
        var conditionObject = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level, this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z', this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z', undefined, false, true);
        // tslint:disable-next-line: max-line-length
        var otmmMPMDataTypes = ((this.isApprover || this.isMember) && this.selectedListFilter.value !== 'MY_NA_REVIEW_TASKS') ? OTMMMPMDataTypes.DELIVERABLE : this.isReviewer ? OTMMMPMDataTypes.DELIVERABLE_REVIEW : OTMMMPMDataTypes.DELIVERABLE;
        var condition = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, otmmMPMDataTypes);
        condition ? conditionObject.DELIVERABLE_CONDTION.push(condition) : console.log('');
        conditionObject.DELIVERABLE_CONDTION.push({
            type: displayField ? displayField.DATA_TYPE : 'string',
            field_id: displayField.INDEXER_FIELD_ID,
            relational_operator_id: MPMSearchOperators.IS,
            relational_operator_name: MPMSearchOperatorNames.IS,
            relational_operator: 'and',
            value: resource['ITEM_ID']
        });
        if (this.viewConfig) {
            viewConfig = this.viewConfig;
        }
        else {
            viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
        }
        this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe(function (viewDetails) {
            var searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
            var parameters = {
                search_config_id: searchConfig ? searchConfig : null,
                keyword: '',
                search_condition_list: {
                    search_condition: conditionObject.DELIVERABLE_CONDTION
                },
                facet_condition_list: {
                    facet_condition: []
                },
                sorting_list: {
                    sort: []
                },
                cursor: {
                    page_index: 0,
                    page_size: 100
                }
            };
            _this.loaderService.show();
            _this.subscriptions.push(_this.indexerService.search(parameters).subscribe(function (data) {
                console.log(data);
                resource.deliverableData = [];
                if (data.data.length > 0) {
                    var dFormat_1 = 'YYYY-MM-DD';
                    data.data.forEach(function (item, index) {
                        var styles = {};
                        var top = 44 * index; // 30
                        var eventStartDate = _this.momentPtr(item.DELIVERABLE_START_DATE, dFormat_1);
                        var eventEndDate = _this.momentPtr(item.DELIVERABLE_DUE_DATE, dFormat_1);
                        var diff = (eventEndDate.diff(eventStartDate)) / (1000 * 60 * 60 * 24);
                        var extraPixel = ((diff + 1) % 2) === 0 ? 2 : 0; // 0 : 2;
                        var width = ((diff + 1) * (_this.viewTypeCellSize + extraPixel)); // ((diff + 1) * (this.viewTypeCellSize - 1));
                        //  styles['width'] = (((diff + 1) % 2) === 0) ? (width + 'px') : (width + 4) + 'px';
                        styles['top'] = top + 'px';
                        var deliverableDetail = {
                            data: item,
                            icon: _this.getDeliverableTypeIcon(_this.getProperty(item, 'DELIVERABLE_TYPE')).ICON,
                            style: styles
                        };
                        resource.deliverableData.push(deliverableDetail);
                        console.log(resource.deliverableData);
                    });
                    /*  resource.height = 35 * data.data.length + 'px'; */
                    resource.height = 50 * data.data.length + 'px';
                }
            }));
            _this.loaderService.hide();
        });
    };
    ResourceManagementToolbarComponent.prototype.updateSearchConfigId = function () {
        if (this.myTaskViewConfigDetails) {
            var lastValue = this.searchConfigId;
            this.searchConfigId = parseInt(this.myTaskViewConfigDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10);
            if (this.searchConfigId > 0 && lastValue !== this.searchConfigId) {
                this.searchChangeEventService.update(this.myTaskViewConfigDetails);
            }
        }
    };
    ResourceManagementToolbarComponent.prototype.reload = function (resetPagination, value) {
        /* var width = document.getElementById('calendarCell').getBoundingClientRect().width;
        alert(width); */
        this.getlistDataIndex(resetPagination);
    };
    ResourceManagementToolbarComponent.prototype.getlistDataIndex = function (value) {
        var _this = this;
        this.loaderService.show();
        this.projectListData = [];
        this.totalListDataCount = 0;
        if (value) {
            this.previousRequest = null;
        }
        var keyWord = '';
        var dates = this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z';
        /*  const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level, dates, undefined, true); */ // this.level
        // MPM-V3-2224
        var conditionObject = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level, this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z', this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z', undefined, false, true);
        this.storeSearchconditionObject = Object.assign({}, conditionObject);
        var otmmMPMDataTypes = ((this.isApprover || this.isMember) && this.selectedListFilter.value != 'MY_NA_REVIEW_TASKS') ? OTMMMPMDataTypes.DELIVERABLE : this.isReviewer ? OTMMMPMDataTypes.DELIVERABLE_REVIEW : OTMMMPMDataTypes.DELIVERABLE;
        var sortTemp = [];
        if (this.selectedSort && this.selectedSort.active) {
            var sortOption = this.sortableFields.find(function (option) { return option.name === _this.selectedSort.active; });
            sortTemp = sortOption ? [{
                    field_id: sortOption.indexerId,
                    order: this.selectedSort.direction
                }] : [];
        }
        if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
            keyWord = this.searchConditions[0].keyword;
        }
        else if (this.searchConditions && this.searchConditions.length > 0) {
            conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(this.searchConditions);
        }
        this.emailSearchCondition = this.emailSearchCondition && Array.isArray(this.emailSearchCondition) ? this.emailSearchCondition : [];
        var parameters = {
            search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
            keyword: keyWord,
            grouping: {
                group_by: this.level,
                group_from_type: otmmMPMDataTypes,
                search_condition_list: {
                    search_condition: __spread(conditionObject.DELIVERABLE_CONDTION, this.emailSearchCondition)
                }
            },
            search_condition_list: {
                search_condition: conditionObject.TASK_CONDITION
            },
            facet_condition_list: {
                facet_condition: (this.facetRestrictionList && this.facetRestrictionList.facet_condition) ? this.facetRestrictionList.facet_condition : []
            },
            sorting_list: {
                sort: sortTemp
            },
            cursor: {
                page_index: this.skip,
                page_size: 1000
            }
        };
        this.loaderService.show();
        if (!(JSON.stringify(this.previousRequest) === JSON.stringify(parameters))) {
            this.previousRequest = parameters;
            this.indexerService.search(parameters).subscribe(function (response) {
                _this.isDataRefresh = false;
                _this.facetChangeEventService.updateFacetFilter(response.facet_field_response_list ? response.facet_field_response_list : []);
                _this.projectListData = response.data;
                _this.totalListDataCount = response.cursor.total_records;
                _this.loaderService.hide();
            }, function (error) {
                _this.notificationService.error('Failed while getting ' + _this.titlecasePipe.transform(_this.viewConfig) + ' lists');
                _this.loaderService.hide();
            });
        }
        else {
            this.loaderService.hide();
        }
    };
    ResourceManagementToolbarComponent.prototype.getLevelDetails = function (trigger) {
        var _this = this;
        if (this.viewConfig) {
            this.viewConfigService.getAllDisplayFeildForViewConfig(this.viewConfig).subscribe(function (viewDetails) {
                _this.myTaskViewConfigDetails = viewDetails;
                _this.updateSearchConfigId();
            });
            if (!(trigger === true)) {
                this.reload(false);
            }
        }
    };
    ResourceManagementToolbarComponent.prototype.refresh = function () {
        this.reload(true);
    };
    ResourceManagementToolbarComponent.prototype.initalizeComponent = function (routeData) {
        var routedGroupFilter;
        if (routeData.groupByFilter) {
            routedGroupFilter = this.enableCampaign ? GROUP_BY_ALL_FILTERS.find(function (filter) {
                return filter.value === routeData.groupByFilter;
            }) : GROUP_BY_FILTERS.find(function (filter) {
                return filter.value === routeData.groupByFilter;
            });
        }
        // tslint:disable-next-line: max-line-length
        this.selectedGroupByFilter = (routedGroupFilter === undefined) ? this.enableCampaign ? GROUP_BY_ALL_FILTERS[0] : routedGroupFilter || GROUP_BY_FILTERS[0] : routedGroupFilter || GROUP_BY_FILTERS[0]; /* this.enableCampaign ? GROUP_BY_ALL_FILTERS[0] : */
        this.setLevel();
        if (routeData.sortBy) {
            this.selectedSortBy = routeData.sortBy;
        }
        if (routeData.sortOrder) {
            this.selectedSortOrder = routeData.sortOrder;
        }
        if (routeData.pageNumber) {
            this.selectedPageNumber = routeData.pageNumber;
        }
        if (routeData.pageSize) {
            this.selectedPageSize = routeData.pageSize;
        }
        if ((routeData.searchName === null && this.searchName) || (this.searchName === null && routeData.searchName)
            || (routeData.savedSearchName === null && this.savedSearchName) || (this.savedSearchName === null && routeData.savedSearchName)
            || (routeData.advSearchData === null && this.advSearchData) || (this.advSearchData === null && routeData.advSearchData)) {
            this.selectedPageNumber = 1;
            this.searchName = routeData.searchName;
            this.savedSearchName = routeData.savedSearchName;
            this.advSearchData = routeData.advSearchData;
            this.isReset = true;
            this.routeComponentView();
        }
        else if ((routeData.facetData === null && this.facetData) || (this.facetData === null && routeData.facetData)) {
            this.selectedPageNumber = 1;
            this.facetData = routeData.facetData;
            this.isReset = true;
            this.routeComponentView();
        }
        else {
            if (routeData.searchName) {
                this.searchName = routeData.searchName;
            }
            else {
                this.searchName = null;
            }
            if (routeData.savedSearchName) {
                this.savedSearchName = routeData.savedSearchName;
            }
            else {
                this.savedSearchName = null;
            }
            if (routeData.advSearchData) {
                this.advSearchData = routeData.advSearchData;
            }
            else {
                this.advSearchData = null;
            }
            if (routeData.facetData) {
                this.facetData = routeData.facetData;
            }
            else {
                this.facetData = null;
            }
            this.componentRendering = false;
            if (routeData.emailLinkId) {
                this.getLevelDetails();
            }
            else {
                this.emailLinkId = '';
                if (this.isPageRefresh && (this.searchName || this.advSearchData || this.savedSearchName)) {
                    this.getLevelDetails(true);
                }
                else if (!(this.isPageRefresh && this.facetData)) {
                    this.getLevelDetails();
                }
            }
        }
    };
    /** CALENDAR METHODS **/
    // build calendar based on given start  and end date
    ResourceManagementToolbarComponent.prototype.buildCalendar = function (startDate, endDate) {
        this.currentViewStartDate = startDate;
        this.currentViewEndDate = endDate;
        var start = startDate; // this.momentPtr({}); //get currrent date
        var end = endDate; // this.momentPtr({}).add(30, 'days');
        var range = this.momentPtr.range(start, end);
        var days = Array.from(range.by('days'));
        this.dates = days;
    };
    ResourceManagementToolbarComponent.prototype.getDynamicWordSpacing = function () {
        var spacing = '8px';
        if (this.displayType === 'month') {
            if (!this.showWeekends) {
                spacing = '16px';
            }
            spacing = '0px';
        }
        return spacing;
    };
    ResourceManagementToolbarComponent.prototype.getNoOfDays = function (selectedMonth) {
        var noOfDays;
        if (CalendarConstants.evenMonth.includes(selectedMonth)) {
            noOfDays = 31;
        }
        else if (CalendarConstants.oddMonth.includes(selectedMonth)) {
            noOfDays = 30;
        }
        else if (CalendarConstants.leapMonth.includes(selectedMonth)) {
            /* const date = new Date(this.year, 1, 29);
            return date.getMonth() === 1; */
            noOfDays = 28;
        }
        return noOfDays;
    };
    ResourceManagementToolbarComponent.prototype.getUsersByRole = function (resource, deliverableData) {
        var _this = this;
        return new Observable(function (observer) {
            var getUserByRoleRequest = {};
            var roleId = resource.TASK_ROLE_ID ? resource.TASK_ROLE_ID : DeliverableTypes.REVIEW === deliverableData.DELIVERABLE_TYPE ? deliverableData.DELIVERABLE_APPROVER_ROLE_ID : deliverableData.DELIVERABLE_OWNER_ROLE_ID;
            var roleDn = _this.utilService.getTeamRoleDNByRoleId(roleId);
            _this.teamRoleUserOptions = [];
            getUserByRoleRequest = {
                allocationType: TaskConstants.ALLOCATION_TYPE_ROLE,
                roleDN: roleDn.ROLE_DN,
                teamID: '',
                isApproveTask: false,
                isUploadTask: true
            };
            _this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(function (response) {
                if (response && response.users && response.users.user) {
                    var userList = acronui.findObjectsByProp(response, 'user');
                    var users_1 = [];
                    if (userList && userList.length && userList.length > 0) {
                        userList.forEach(function (user) {
                            var fieldObj = users_1.find(function (e) { return e.value === user.dn; });
                            if (!fieldObj) {
                                users_1.push({
                                    name: user.dn,
                                    value: user.cn,
                                    displayName: user.name
                                });
                            }
                        });
                    }
                    _this.teamRoleUserOptions = users_1;
                }
                else {
                    _this.teamRoleUserOptions = [];
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, function () {
                observer.next(false);
                observer.complete();
            });
        });
    };
    ResourceManagementToolbarComponent.prototype.getUsers = function (resource, deliverableData) {
        console.log(deliverableData);
        this.getUsersByRole(resource, deliverableData).subscribe(function (response) {
        });
    };
    /* updateTaskDeliverable(data, userId) {
      let TaskObject;
      TaskObject = {
        'BulkOperation': false,
        'RPOOwnerUser': {
          'User': {
            'userID': userId
          }
        },
        'TaskId': {
          'Id': data.TASK_ITEM_ID.split('.')[1]
        }
      }
      const requestObj = {
        TaskObject: TaskObject,
        DeliverableReviewers: {
          DeliverableReviewer: ''
        }
      }
      this.taskService.updateTask(requestObj).subscribe(response => {
        this.loaderService.hide();
        if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
          this.reload(true);
          this.notificationService.success('Task updated successfully');
        } else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
          && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
          this.reload(true);
          this.notificationService.error(response.APIResponse.error.errorMessage);
        } else {
          this.reload(true);
          this.notificationService.error('Something went wrong on while updating task');
        }
      }, error => {
        this.reload(true);
        this.notificationService.error('Something went wrong on while updating task');
      });
    } */
    // MPM-V3-2217
    ResourceManagementToolbarComponent.prototype.updateTaskDeliverable = function (data, userId) {
        var _this = this;
        this.taskService.assignDeliverable(data.ID, userId).subscribe(function (response) {
            _this.loaderService.hide();
            if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
                _this.reload(true);
                _this.notificationService.success('Deliverable updated successfully');
                //  this.addedResourceHandler.next(userId);
                //   this.synchronizeTaskFiltersComponent.getDeliverable(userId,false);
            }
            else if (response && response.APIResponse && response.APIResponse.statusCode === '500') {
                _this.reload(true);
                _this.notificationService.error('Something went wrong on while updating deliverable');
            }
            else {
                _this.loaderService.hide();
                _this.reload(true);
                _this.notificationService.error('Something went wrong on while updating deliverable');
            }
        }, function (error) {
            _this.loaderService.hide();
            _this.reload(true);
            _this.notificationService.error('Something went wrong on while updating deliverable');
        });
    };
    ResourceManagementToolbarComponent.prototype.updateDeliverable = function (data, userId) {
        var _this = this;
        var deliverableObject;
        deliverableObject = {
            'BulkOperation': false,
            'RPODeliverableOwnerID': {
                'IdentityID': {
                    'userID': userId
                }
            },
            'DeliverableId': {
                'Id': data.ID
            }
        };
        var requestObj = {
            DeliverableObject: deliverableObject,
            DeliverableReviewers: {
                DeliverableReviewer: ''
            }
        };
        this.deliverableService.updateDeliverable(requestObj)
            .subscribe(function (response) {
            _this.loaderService.hide();
            if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
                && response.APIResponse.data.Deliverable && response.APIResponse.data.Deliverable['Deliverable-id'] && response.APIResponse.data.Deliverable['Deliverable-id'].Id) {
                _this.reload(true);
                _this.notificationService.success('Deliverable updated successfully');
                //  this.addedResourceHandler.next(userId);
                //  this.synchronizeTaskFiltersComponent.getDeliverable(userId, false);
            }
            else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                _this.reload(true);
                _this.notificationService.error(response.APIResponse.error.errorMessage);
            }
            else {
                _this.loaderService.hide();
                _this.reload(true);
                _this.notificationService.error('Something went wrong on while updating deliverable');
            }
        }, function (error) {
            _this.loaderService.hide();
            _this.reload(true);
            _this.notificationService.error('Something went wrong on while updating deliverable');
        });
    };
    ResourceManagementToolbarComponent.prototype.update = function (data, userId) {
        console.log(userId);
        this.loaderService.show();
        if (data.DELIVERABLE_TYPE === DeliverableTypes.UPLOAD) {
            this.updateDeliverable(data, userId);
        }
        else {
            this.updateTaskDeliverable(data, userId);
        }
    };
    ResourceManagementToolbarComponent.prototype.deliverableDataHandler = function (event) {
        console.log(event);
        this.deliverableDatas = event;
    };
    ResourceManagementToolbarComponent.prototype.onChickletRemoveFilter = function (filter) {
        console.log(filter);
        this.removedFilter = filter;
    };
    /* resourceAllocationHandler(allocation) {
      console.log(allocation);
      this.allocation = allocation;
    } */
    ResourceManagementToolbarComponent.prototype.getDateDifference = function (startDate, endDate) {
        var sd = (new Date(startDate));
        var ed = new Date(endDate);
        return (Math.floor(ed - sd) / (1000 * 60 * 60 * 24)) + 1;
    };
    // ngOnChanges(simpleChanges: SimpleChanges) {
    //   var angular: any;
    //   console.log("Content-width: "
    //     + (document.getElementById("calendarCell").scrollWidth)
    //     + "px<br>");
    //   console.log("Content-width1: "
    //     + angular.element(document.getElementById("calendarCell").clientHeight)
    //     + "px<br>");
    // }
    // tslint:disable-next-line: use-lifecycle-interface
    ResourceManagementToolbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.componentRendering = true;
        this.appConfig = this.sharingService.getAppConfig();
        this.enableCampaign = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CAMPAIGN]);
        this.groupByFilters = this.enableCampaign ? GROUP_BY_ALL_FILTERS : GROUP_BY_FILTERS;
        this.top = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.pageSize = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        var currentFullDate = new Date();
        var currentYear = currentFullDate.getFullYear();
        var currentMonth = currentFullDate.getMonth() + 1;
        var currentDate = currentFullDate.getDate();
        this.currentDate = currentMonth + '/' + currentDate + '/' + currentYear;
        this.selectedMonth = CalendarConstants.months[currentFullDate.getMonth()];
        this.year = currentYear;
        var noOfDays = this.getNoOfDays(this.selectedMonth);
        // this.buildCalendar(this.momentPtr({}), this.momentPtr({}).add(29, 'days'));
        /* this.buildCalendar(currentMonth + '/01/' + currentYear, this.momentPtr({}).add(29, 'days')); */
        this.buildCalendar(currentMonth + '/01/' + currentYear, currentMonth + '/' + noOfDays + '/' + currentYear);
        window.addEventListener('scroll', this.scrolling, true);
        var initialLoad = true;
        this.subscriptions.push(this.searchDataService.searchData.subscribe(function (data) {
            _this.searchConditions = (data && Array.isArray(data) ? _this.otmmService.addRelationalOperator(data) : []);
            if ((_this.searchName || _this.savedSearchName || _this.advSearchData || _this.facetData) && _this.isDataRefresh) {
                if (data && data.length > 0) {
                    if (!_this.facetData || ((_this.facetData && _this.facetRestrictionList) && (_this.advSearchData || _this.savedSearchName))) {
                        _this.getLevelDetails();
                    }
                    else {
                        _this.validateFacet = true;
                    }
                }
            }
            else if (_this.searchName === null && _this.savedSearchName === null && _this.advSearchData === null && _this.facetData === null && _this.isDataRefresh) {
                _this.getLevelDetails();
            }
            else if (!_this.isDataRefresh) {
                if (!initialLoad) {
                    _this.isReset = true;
                    _this.getLevelDetails();
                }
            }
            initialLoad = false;
        }));
        initialLoad = true;
        this.subscriptions.push(this.facetChangeEventService.onFacetCondidtionChange.subscribe(function (data) {
            _this.facetRestrictionList = data;
            if (_this.facetData && _this.isDataRefresh) {
                if (data && data.facet_condition && data.facet_condition.length > 0) {
                    if (!(_this.advSearchData || _this.savedSearchName) || _this.validateFacet) {
                        _this.getLevelDetails();
                        _this.validateFacet = false;
                    }
                }
            }
            else if (_this.facetData === null && _this.isDataRefresh) {
                _this.getLevelDetails();
            }
            else if (!_this.isDataRefresh) {
                if (!initialLoad) {
                    _this.previousRequest = null;
                    _this.isReset = true;
                    _this.getLevelDetails();
                }
            }
            initialLoad = false;
        }));
        this.subscriptions.push(this.componentParams.subscribe(function (routeData) {
            if (routeData.emailLinkId) {
                _this.emailLinkId = routeData.emailLinkId;
                _this.initalizeComponent(routeData);
            }
            else {
                _this.initalizeComponent(routeData);
            }
        }));
    };
    ResourceManagementToolbarComponent.prototype.ngOnDestroy = function () {
        this.searchChangeEventService.update(null);
        this.subscriptions.forEach(function (subscription) { return subscription.unsubscribe(); });
        window.removeEventListener('scroll', this.scrolling, true);
    };
    ResourceManagementToolbarComponent.ctorParameters = function () { return [
        { type: UtilService },
        { type: FacetChangeEventService },
        { type: RouteService },
        { type: ActivatedRoute },
        { type: SharingService },
        { type: Renderer2 },
        { type: MediaMatcher },
        { type: ChangeDetectorRef },
        { type: SearchChangeEventService },
        { type: DeliverableTaskMetadataService },
        { type: ViewConfigService },
        { type: LoaderService },
        { type: NotificationService },
        { type: FilterHelperService },
        { type: SearchDataService },
        { type: IndexerService },
        { type: TitleCasePipe },
        { type: OTMMService },
        { type: AppService },
        { type: FieldConfigService },
        { type: TaskService },
        { type: DeliverableService }
    ]; };
    __decorate([
        Output()
    ], ResourceManagementToolbarComponent.prototype, "addedResourceHandler", void 0);
    __decorate([
        ViewChildren('dateHeader')
    ], ResourceManagementToolbarComponent.prototype, "calendarHeaders", void 0);
    __decorate([
        ViewChildren('calendarCell')
    ], ResourceManagementToolbarComponent.prototype, "calendarCells", void 0);
    __decorate([
        ViewChildren('panelContainer')
    ], ResourceManagementToolbarComponent.prototype, "panelContainers", void 0);
    __decorate([
        ViewChild('calendarView')
    ], ResourceManagementToolbarComponent.prototype, "calendarView", void 0);
    ResourceManagementToolbarComponent = __decorate([
        Component({
            selector: 'mpm-resource-management-toolbar',
            template: "<mpm-synchronize-task-filters [groupBy]=\"selectedGroupByFilter\" [selectedMonth]=\"selectedMonth\"\r\n    [currentViewStartDates]=\"currentViewStartDate\" [currentViewEndDates]=\"currentViewEndDate\" [levels]=\"level\"\r\n    [viewConfigs]=\"viewConfig\" (deliverableData)=\"deliverableDataHandler($event)\" (selectedUser)=\"selectedUserr($event)\"\r\n    (selectedFilters)=\"selectedFilter($event)\" [removedFilter]=\"removedFilter\" [datess]=\"dates\" [datee]=\"dates\">\r\n</mpm-synchronize-task-filters>\r\n<mat-toolbar>\r\n    <mat-toolbar-row>\r\n        <div class=\"flex-container\">\r\n            <div><span class=\"action-item\">\r\n                    <!-- <mpm-custom-select controlLabel=\"{{'Group By: '}}\" [options]=\"groupByFilters\"\r\n                        (selectionChange)=\"groupByFilterChange($event)\" [selected]=\"selectedGroupByFilter\"\r\n                        [bulkEdit]='true'>\r\n                    </mpm-custom-select> -->\r\n                </span>\r\n                <span>\r\n                    <button mat-icon-button (click)=refresh() matTooltip=\"Refresh\">\r\n                        <mat-icon color=\"primary\" class=\"dashboard-action-btn\">refresh\r\n                        </mat-icon>\r\n                    </button>\r\n                </span>\r\n            </div>\r\n\r\n            &nbsp;&nbsp;\r\n            <div>\r\n                <span>\r\n                    <mat-button-toggle-group name=\"viewType\" aria-label=\"View Type\" [value]=\"displayType\">\r\n                        <mat-button-toggle value=\"month\" matTooltip=\"Display Month View\">\r\n\r\n                            <span class=\"toogleView\">Month</span>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle value=\"week\" [disabled]=\"true\" aria-disabled=\"true\"\r\n                            matTooltip=\"Display Week View\">\r\n\r\n                            <span class=\"toogleView\">Week</span>\r\n                        </mat-button-toggle>\r\n                        <!-- <mat-button-toggle value=\"day\" [disabled]=\"true\" aria-disabled=\"true\"\r\n                            matTooltip=\"Display day View\">\r\n\r\n                            <span class=\"toogleView\">Day</span>\r\n                        </mat-button-toggle> -->\r\n                    </mat-button-toggle-group>\r\n                </span>\r\n            </div>\r\n            <div style=\"font-size: 20px;\">\r\n                <span>\r\n                    {{ currentDate | date}}\r\n                </span>\r\n            </div>\r\n\r\n            <div class=\"calendar-container\">\r\n                <div>\r\n                    <button mat-stroked-button color=\"primary\" (click)=\"calendarPrevious()\" class=\"calendar-nav-button\"\r\n                        matTooltip=\"Previous {{displayType == 'month' ? 'Month' : 'Week'}}\">\r\n                        <mat-icon aria-hidden=\"false\" aria-label=\"previous\" class=\"calendar-nav-button-icon\">\r\n                            chevron_left\r\n                        </mat-icon>\r\n                    </button>\r\n                </div>\r\n                <div class=\"month-alignment\">\r\n                    <span>{{selectedMonth}} - {{year}}</span>\r\n                </div>\r\n                <div>\r\n                    <button mat-stroked-button color=\"primary\" (click)=\"calendarNext()\" class=\"calendar-nav-button\"\r\n                        matTooltip=\"Next {{displayType == 'month' ? 'Month' : 'Week'}}\">\r\n                        <mat-icon aria-hidden=\"false\" aria-label=\"next\" class=\"calendar-nav-button-icon\" \u00CD\u00CD>\r\n                            chevron_right\r\n                        </mat-icon>\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </mat-toolbar-row>\r\n</mat-toolbar>\r\n\r\n<mat-sidenav-container [style.marginTop.px]=\"mobileQuery.matches ? 56 : 0\" style=\"height:50%;\" autosize>\r\n\r\n    <mat-sidenav class=\"custom-sidenav\" #pmFaceNav [disableClose]=\"true\" [mode]=\"mobileQuery.matches ? 'over' : 'side'\"\r\n        [opened]=\"facetExpansionState\">\r\n        <div class=\"facet-sidenav\">\r\n            <!-- <mpm-facet [dataCount]=\"totalListDataCount\"></mpm-facet> -->\r\n        </div>\r\n        <!--  <hr />\r\n            <div class=\"sync\">\r\n                <app-synchronize-task-filters [groupBy]=\"selectedGroupByFilter\" [selectedMonth]=\"selectedMonth\"\r\n                    [currentViewStartDates]=\"currentViewStartDate\" [currentViewEndDates]=\"currentViewEndDate\"\r\n                    [levels]=\"level\" [viewConfigs]=\"viewConfig\" (deliverableData)=\"deliverableDataHandler($event)\"\r\n                    (selectedUser)=\"selectedUserr($event)\" (selectedFilters)=\"selectedFilter($event)\"\r\n                    [removedFilter]=\"removedFilter\" \r\n                    [datess]=\"dates\" [datee]=\"dates\">\r\n                </app-synchronize-task-filters>\r\n            </div> -->\r\n    </mat-sidenav>\r\n\r\n    <mat-sidenav-content class=\"mat-content\">\r\n\r\n        <div>\r\n            <div class=\"sidenav-container chart-section dateFlex-container\" fxFlex fxLayout=\"row\">\r\n                <div class=\"border-design\" [ngStyle]=\"sidebarStyle\" fxLayout=\"column\">\r\n\r\n                    <div class=\"sidenav-header text-center\">\r\n                        <span style=\"float:left;\">\r\n                            <!-- <mpm-facet-button [initalState]=\"facetExpansionState\" (facetToggled)=\"facetToggled($event)\"\r\n                                [isRMView]=\"true\">\r\n                            </mpm-facet-button> -->\r\n                        </span>\r\n\r\n                    </div>\r\n\r\n                </div>\r\n\r\n                <div class=\"sidenav-content calendar border-design\" fxLayout=\"column\" #calendarView>\r\n                    <div fxLayout=\"row\" id=\"datesHolder\" class=\"sticky-dates scroll-margin dates-container\"\r\n                        (scroll)=\"scrolling($event)\" [ngClass]=\"{'scrolled-div': scrolled}\">\r\n                        <div *ngFor=\"let date of dates\" class=\"calendar-header\"\r\n                            [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                            [ngClass]=\"{'highlightWeekEnd':date.format('dd') === 'Sa' ||date.format('dd') === 'Su'}\"\r\n                            #dateHeader>\r\n\r\n                            {{date.format('dd DD')}}\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div fxLayout=\"row\" style=\"    height: 300px;\r\n                    overflow: auto;\">\r\n                        <mat-accordion class=\"fill-layout\" multi=\"true\">\r\n                            <mat-expansion-panel hideToggle [expanded]=\"false\"\r\n                                *ngFor=\"let resource of projectListData;\">\r\n\r\n                                <mat-expansion-panel-header class=\"scroll-margin\" style=\"padding: 0px;\"\r\n                                    (click)=\"expandRow($event,resource)\">\r\n\r\n                                    <mat-panel-title fxFlex=\"100\">\r\n\r\n                                        <span class=\"resource-text-color\">\r\n                                            {{resource.TASK_NAME || resource.PROJECT_NAME|| resource.CAMPAIGN_NAME\r\n                                            }}</span>\r\n                                    </mat-panel-title>\r\n                                </mat-expansion-panel-header>\r\n                                <div>\r\n                                    <div class=\"panel-container task-content\" #panelContainer\r\n                                        [ngStyle]=\"{height: resource.height}\">\r\n                                        <div *ngFor=\"let date of dates;\" [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                                            class=\"calendar-cell\" [attr.date-filled]=\"\" #calendarCell>\r\n\r\n\r\n                                            <div *ngIf=\"resource.deliverableData && resource.deliverableData.length>0\">\r\n                                                <ng-container\r\n                                                    *ngFor=\"let event of resource.deliverableData; let idx=index\">\r\n                                                    <!-- <div *ngIf=\"date.isSame(event.data.DELIVERABLE_START_DATE, 'day')\"\r\n                                                        [id]=\"event.data.ID\" [ngStyle]=\"event.style\" class=\"rectangle\"\r\n                                                        mwlResizable matTooltip=\"{{event.data.DELIVERABLE_NAME}}\"> -->\r\n                                                    <!-- MPM-V3-2224 -->\r\n                                                    <div *ngIf=\"date.isSame(event.data.DELIVERABLE_START_DATE, 'day') ? true :(date.isSame(currentViewStartDate, 'day') && date.isAfter(event.data.DELIVERABLE_START_DATE, 'day'))\"\r\n                                                        [id]=\"event.data.ID\" [ngStyle]=\"\r\n                                                        { width: \r\n                                                        date.isSame(event.data.DELIVERABLE_START_DATE, 'day') ? \r\n                                                        ((momentPtr(event.data.DELIVERABLE_DUE_DATE).isSame(momentPtr(currentViewEndDate), 'day')\r\n                                                        || momentPtr(event.data.DELIVERABLE_DUE_DATE).isBefore( momentPtr(currentViewEndDate) ,'day') )\r\n                                                        ? (getDateDifference(event.data.DELIVERABLE_START_DATE,event.data.DELIVERABLE_DUE_DATE)*37)+ 'px' \r\n                                                        : (getDateDifference(event.data.DELIVERABLE_START_DATE,currentViewEndDate) *37) + 'px')\r\n                                                        : (date.isAfter(event.data.DELIVERABLE_START_DATE, 'day') && date.isSame(currentViewStartDate, 'day') \r\n                                                        ? ((momentPtr(event.data.DELIVERABLE_DUE_DATE).isSame(momentPtr(currentViewEndDate), 'day')\r\n                                                        || momentPtr(event.data.DELIVERABLE_DUE_DATE).isBefore(momentPtr(currentViewEndDate), 'day'))\r\n                                                        ? (getDateDifference(currentViewStartDate,event.data.DELIVERABLE_DUE_DATE)*37)+ 'px' \r\n                                                        : (getDateDifference(currentViewStartDate,currentViewEndDate)*37)+ 'px' )\t\r\n                                                        : '3px' ),top:event.style.top}\" class=\"rectangle\" mwlResizable\r\n                                                        matTooltip=\"{{event.data.DELIVERABLE_NAME}}\">\r\n                                                        <span (click)=\"getUsers(resource,event.data)\"\r\n                                                            [matMenuTriggerFor]=\"projectMenu\" class=\"event-title\">\r\n                                                            <mat-icon style=\"margin-top:12px;\">{{event.icon}}</mat-icon>\r\n                                                            {{event.data.DELIVERABLE_NAME}}\r\n                                                        </span>\r\n                                                        <mat-menu #projectMenu=\"matMenu\">\r\n                                                            <mat-option *ngFor=\"let roleName of teamRoleUserOptions\">\r\n                                                                <div class=\"check\"\r\n                                                                    (click)=\"update(event.data,roleName.value)\">\r\n                                                                    {{roleName.displayName}}</div>\r\n                                                            </mat-option>\r\n                                                        </mat-menu>\r\n\r\n                                                    </div>\r\n\r\n\r\n                                                </ng-container>\r\n                                            </div>\r\n\r\n\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </mat-expansion-panel>\r\n                            <div *ngIf=\"totalListDataCount === 0\" class=\"central-info\">\r\n                                <button color=\"accent\" disabled mat-flat-button>No data is available.</button>\r\n                            </div>\r\n                        </mat-accordion>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n            <!--    <hr /> -->\r\n            <!-- <hr />\r\n                <div>\r\n                    <app-resource-management-resource-allocation [selectedMonth]=\"selectedMonth\" [data]=\"deliverableDatas\"\r\n                        [users]=\"selectedUsers\" [selectedItems]=\"selectedItems\"\r\n                        (removed)=\"onChickletRemoveFilter($event)\" [allocation]=\"allocation\">\r\n                    </app-resource-management-resource-allocation>\r\n                </div>\r\n     -->\r\n        </div>\r\n\r\n    </mat-sidenav-content>\r\n\r\n</mat-sidenav-container>\r\n\r\n\r\n<mat-sidenav-container [style.marginTop.px]=\"mobileQuery.matches ? 56 : 0\" autosize>\r\n\r\n    <mat-sidenav class=\"custom-sidenav\" #pmFaceNav [disableClose]=\"true\" [mode]=\"mobileQuery.matches ? 'over' : 'side'\"\r\n        [opened]=\"facetExpansionState\">\r\n        <!-- <div class=\"facet-sidenav\">\r\n                <app-facet [dataCount]=\"totalListDataCount\"></app-facet>\r\n            </div> -->\r\n        <hr />\r\n        <div class=\"sync\">\r\n            <mpm-synchronize-task-filters [groupBy]=\"selectedGroupByFilter\" [selectedMonth]=\"selectedMonth\"\r\n                [currentViewStartDates]=\"currentViewStartDate\" [currentViewEndDates]=\"currentViewEndDate\"\r\n                [levels]=\"level\" [viewConfigs]=\"viewConfig\" (deliverableData)=\"deliverableDataHandler($event)\"\r\n                (selectedUser)=\"selectedUserr($event)\" (selectedFilters)=\"selectedFilter($event)\"\r\n                [removedFilter]=\"removedFilter\" [datess]=\"dates\" [datee]=\"dates\">\r\n            </mpm-synchronize-task-filters>\r\n        </div>\r\n    </mat-sidenav>\r\n\r\n    <mat-sidenav-content class=\"mat-content\" style=\"height: 35%;\">\r\n\r\n        <div>\r\n            <!-- <div class=\" sidenav-container chart-section dateFlex-container\" fxFlex fxLayout=\"row\">\r\n        <div class=\"border-design\" [ngStyle]=\"sidebarStyle\" fxLayout=\"column\">\r\n\r\n            <div class=\"sidenav-header text-center\">\r\n                <span style=\"float:left;\">\r\n                    <mpm-facet-button [initalState]=\"facetExpansionState\" (facetToggled)=\"facetToggled($event)\"\r\n                        [isRMView]=\"true\">\r\n                    </mpm-facet-button>\r\n                </span>\r\n\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"sidenav-content calendar border-design\" fxLayout=\"column\" #calendarView>\r\n            <div fxLayout=\"row\" id=\"datesHolder\" class=\"sticky-dates scroll-margin dates-container\"\r\n                (scroll)=\"scrolling($event)\" [ngClass]=\"{'scrolled-div': scrolled}\">\r\n                <div *ngFor=\"let date of dates\" class=\"calendar-header\" [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                    [ngClass]=\"{'highlightWeekEnd':date.format('dd') === 'Sa' ||date.format('dd') === 'Su'}\"\r\n                    #dateHeader>\r\n\r\n                    {{date.format('dd DD')}}\r\n                </div>\r\n            </div>\r\n\r\n            <div fxLayout=\"row\">\r\n                <mat-accordion class=\"fill-layout\" multi=\"true\">\r\n                    <mat-expansion-panel hideToggle [expanded]=\"false\" *ngFor=\"let resource of projectListData;\">\r\n\r\n                        <mat-expansion-panel-header class=\"scroll-margin\" style=\"padding: 0px;\"\r\n                            (click)=\"expandRow($event,resource)\">\r\n\r\n                            <mat-panel-title fxFlex=\"100\">\r\n\r\n                                <span class=\"resource-text-color\">\r\n                                    {{resource.TASK_NAME || resource.PROJECT_NAME|| resource.CAMPAIGN_NAME\r\n                                    }}</span>\r\n                            </mat-panel-title>\r\n                        </mat-expansion-panel-header>\r\n                        <div>\r\n                            <div class=\"panel-container task-content\" #panelContainer\r\n                                [ngStyle]=\"{height: resource.height}\">\r\n                                <div *ngFor=\"let date of dates;\" [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                                    class=\"calendar-cell\" [attr.date-filled]=\"\" #calendarCell>\r\n\r\n\r\n                                    <div *ngIf=\"resource.deliverableData && resource.deliverableData.length>0\">\r\n                                        <ng-container *ngFor=\"let event of resource.deliverableData; let idx=index\">\r\n                                            <div *ngIf=\"date.isSame(event.data.DELIVERABLE_START_DATE, 'day')\"\r\n                                                [id]=\"event.data.ID\" [ngStyle]=\"event.style\" class=\"rectangle\"\r\n                                                mwlResizable matTooltip=\"{{event.data.DELIVERABLE_NAME}}\">\r\n\r\n                                                <div>\r\n                                                    <span (click)=\"getUsers(resource,event.data)\"\r\n                                                        [matMenuTriggerFor]=\"projectMenu\"\r\n                                                        class=\"event-title\">{{event.data.DELIVERABLE_NAME}}\r\n                                                    </span>\r\n                                                    <mat-menu #projectMenu=\"matMenu\">\r\n                                                        <mat-option *ngFor=\"let roleName of teamRoleUserOptions\">\r\n                                                            <div class=\"check\"\r\n                                                                (click)=\"update(event.data,roleName.value)\">\r\n                                                                {{roleName.displayName}}</div>\r\n                                                        </mat-option>\r\n                                                    </mat-menu>\r\n                                                </div>\r\n\r\n                                            </div>\r\n\r\n\r\n                                        </ng-container>\r\n                                    </div>\r\n\r\n\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </mat-expansion-panel>\r\n                    <div *ngIf=\"totalListDataCount === 0\" class=\"central-info\">\r\n                        <button color=\"accent\" disabled mat-flat-button>No data is available.</button>\r\n                    </div>\r\n                </mat-accordion>\r\n            </div>\r\n\r\n        </div>\r\n        </div> -->\r\n            <!-- <hr /> -->\r\n            <div style=\"height: 14rem;\">\r\n                <mpm-resource-management-resource-allocation [selectedMonth]=\"selectedMonth\" [data]=\"deliverableDatas\"\r\n                    [users]=\"selectedUsers\" [selectedItems]=\"selectedItems\" (removed)=\"onChickletRemoveFilter($event)\"\r\n                    [allocation]=\"allocation\" [selectedYear]=\"year\">\r\n                </mpm-resource-management-resource-allocation>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </mat-sidenav-content>\r\n\r\n</mat-sidenav-container>\r\n\r\n\r\n\r\n\r\n\r\n<!-- <mat-toolbar>\r\n    <mat-toolbar-row>\r\n        <div class=\"flex-container\">\r\n            <div><span class=\"action-item\">\r\n                    <mpm-custom-select controlLabel=\"{{'Group By: '}}\" [options]=\"groupByFilters\"\r\n                        (selectionChange)=\"groupByFilterChange($event)\" [selected]=\"selectedGroupByFilter\"\r\n                        [bulkEdit]='true'>\r\n                    </mpm-custom-select>\r\n                </span>\r\n            </div>\r\n            &nbsp;&nbsp;\r\n            <div>\r\n                <span>\r\n                    <mat-button-toggle-group name=\"viewType\" aria-label=\"View Type\" [value]=\"displayType\">\r\n                        <mat-button-toggle value=\"month\" matTooltip=\"Display Month View\">\r\n                            \r\n                            <span class=\"toogleView\">Month</span>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle value=\"week\" [disabled]=\"true\" aria-disabled=\"true\"\r\n                            matTooltip=\"Display Week View\">\r\n                            \r\n                            <span class=\"toogleView\">Week</span>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle value=\"day\" [disabled]=\"true\" aria-disabled=\"true\"\r\n                            matTooltip=\"Display day View\">\r\n                            \r\n                            <span class=\"toogleView\">Day</span>\r\n                        </mat-button-toggle>\r\n                    </mat-button-toggle-group>\r\n                </span>\r\n            </div>\r\n            <div style=\"font-size: 20px;\">\r\n                <span>\r\n                    {{ currentDate | date}}\r\n                </span>\r\n            </div>\r\n\r\n            <div class=\"calendar-container\">\r\n                <div>\r\n                    <button mat-stroked-button color=\"primary\" (click)=\"calendarPrevious()\" class=\"calendar-nav-button\"\r\n                        matTooltip=\"Previous {{displayType == 'month' ? 'Month' : 'Week'}}\">\r\n                        <mat-icon aria-hidden=\"false\" aria-label=\"previous\" class=\"calendar-nav-button-icon\">\r\n                            chevron_left\r\n                        </mat-icon>\r\n                    </button>\r\n                </div>\r\n                <div class=\"month-alignment\">\r\n                    <span>{{selectedMonth}}</span>\r\n                </div>\r\n                <div>\r\n                    <button mat-stroked-button color=\"primary\" (click)=\"calendarNext()\" class=\"calendar-nav-button\"\r\n                        matTooltip=\"Next {{displayType == 'month' ? 'Month' : 'Week'}}\">\r\n                        <mat-icon aria-hidden=\"false\" aria-label=\"next\" class=\"calendar-nav-button-icon\" \u00CD\u00CD>\r\n                            chevron_right\r\n                        </mat-icon>\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </mat-toolbar-row>\r\n</mat-toolbar>\r\n<mat-sidenav-container [style.marginTop.px]=\"mobileQuery.matches ? 56 : 0\" autosize>\r\n    <mat-sidenav class=\"custom-sidenav\" #pmFaceNav [disableClose]=\"true\" [mode]=\"mobileQuery.matches ? 'over' : 'side'\"\r\n        [opened]=\"facetExpansionState\">\r\n        <div class=\"facet-sidenav\">\r\n            <app-facet [dataCount]=\"totalListDataCount\"></app-facet>\r\n        </div>\r\n        <hr />\r\n        <div class=\"sync\">\r\n            <app-synchronize-task-filters [groupBy]=\"selectedGroupByFilter\" [selectedMonth]=\"selectedMonth\"\r\n                [currentViewStartDates]=\"currentViewStartDate\" [currentViewEndDates]=\"currentViewEndDate\"\r\n                [levels]=\"level\" [viewConfigs]=\"viewConfig\" (deliverableData)=\"deliverableDataHandler($event)\"\r\n                (selectedUser)=\"selectedUserr($event)\" (selectedFilters)=\"selectedFilter($event)\"\r\n                [removedFilter]=\"removedFilter\" \r\n                [datess]=\"dates\" [datee]=\"dates\">\r\n            </app-synchronize-task-filters>\r\n        </div>\r\n    </mat-sidenav>\r\n\r\n    <mat-sidenav-content class=\"mat-content\">\r\n\r\n        <div>\r\n            <div class=\"sidenav-container chart-section dateFlex-container\" fxFlex fxLayout=\"row\">\r\n                <div class=\"border-design\" [ngStyle]=\"sidebarStyle\" fxLayout=\"column\">\r\n                 \r\n                    <div class=\"sidenav-header text-center\">\r\n                        <span style=\"float:left;\">\r\n                            <mpm-facet-button [initalState]=\"facetExpansionState\" (facetToggled)=\"facetToggled($event)\"\r\n                                [isRMView]=\"true\">\r\n                            </mpm-facet-button>\r\n                        </span>\r\n                       \r\n                    </div>\r\n\r\n                </div>\r\n\r\n                <div class=\"sidenav-content calendar border-design\" fxLayout=\"column\" #calendarView>\r\n                    <div fxLayout=\"row\" id=\"datesHolder\" class=\"sticky-dates scroll-margin dates-container\"\r\n                        (scroll)=\"scrolling($event)\" [ngClass]=\"{'scrolled-div': scrolled}\">\r\n                        <div *ngFor=\"let date of dates\" class=\"calendar-header\"\r\n                            [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                            [ngClass]=\"{'highlightWeekEnd':date.format('dd') === 'Sa' ||date.format('dd') === 'Su'}\"\r\n                            #dateHeader>\r\n                           \r\n                            {{date.format('dd DD')}}\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div fxLayout=\"row\">\r\n                        <mat-accordion class=\"fill-layout\" multi=\"true\">\r\n                            <mat-expansion-panel hideToggle [expanded]=\"false\"\r\n                                *ngFor=\"let resource of projectListData;\">\r\n                               \r\n                                <mat-expansion-panel-header class=\"scroll-margin\" style=\"padding: 0px;\"\r\n                                    (click)=\"expandRow($event,resource)\">\r\n                                   \r\n                                    <mat-panel-title fxFlex=\"100\">\r\n                                       \r\n                                        <span class=\"resource-text-color\">\r\n                                            {{resource.TASK_NAME || resource.PROJECT_NAME|| resource.CAMPAIGN_NAME\r\n                                            }}</span>\r\n                                    </mat-panel-title>\r\n                                </mat-expansion-panel-header>\r\n                                <div>\r\n                                    <div class=\"panel-container task-content\" #panelContainer\r\n                                        [ngStyle]=\"{height: resource.height}\">\r\n                                        <div *ngFor=\"let date of dates;\" [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                                            class=\"calendar-cell\" [attr.date-filled]=\"\" #calendarCell>\r\n\r\n                                           \r\n                                            <div *ngIf=\"resource.deliverableData && resource.deliverableData.length>0\">\r\n                                                <ng-container\r\n                                                    *ngFor=\"let event of resource.deliverableData; let idx=index\">\r\n                                                    <div *ngIf=\"date.isSame(event.data.DELIVERABLE_START_DATE, 'day')\"\r\n                                                        [id]=\"event.data.ID\" [ngStyle]=\"event.style\" class=\"rectangle\"\r\n                                                        mwlResizable matTooltip=\"{{event.data.DELIVERABLE_NAME}}\">\r\n                                                      \r\n                                                        <div>\r\n                                                            <span (click)=\"getUsers(resource,event.data)\"\r\n                                                                [matMenuTriggerFor]=\"projectMenu\"\r\n                                                                class=\"event-title\">{{event.data.DELIVERABLE_NAME}}\r\n                                                            </span>\r\n                                                            <mat-menu #projectMenu=\"matMenu\">\r\n                                                                <mat-option\r\n                                                                    *ngFor=\"let roleName of teamRoleUserOptions\">\r\n                                                                    <div class=\"check\"\r\n                                                                        (click)=\"update(event.data,roleName.value)\">\r\n                                                                        {{roleName.displayName}}</div>\r\n                                                                </mat-option>\r\n                                                            </mat-menu>\r\n                                                        </div>\r\n                                                       \r\n                                                    </div>\r\n\r\n\r\n                                                </ng-container>\r\n                                            </div>\r\n\r\n\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </mat-expansion-panel>\r\n                            <div *ngIf=\"totalListDataCount === 0\" class=\"central-info\">\r\n                                <button color=\"accent\" disabled mat-flat-button>No data is available.</button>\r\n                            </div>\r\n                        </mat-accordion>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n            <hr />\r\n            <div>\r\n                <app-resource-management-resource-allocation [selectedMonth]=\"selectedMonth\" [data]=\"deliverableDatas\"\r\n                    [users]=\"selectedUsers\" [selectedItems]=\"selectedItems\"\r\n                    (removed)=\"onChickletRemoveFilter($event)\" [allocation]=\"allocation\">\r\n                </app-resource-management-resource-allocation>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </mat-sidenav-content>\r\n</mat-sidenav-container> -->",
            animations: [
                trigger('rotatedState', [
                    state('default', style({ transform: 'rotate(0deg)' })),
                    state('rotated', style({ transform: 'rotate(180deg)' })),
                    transition('rotated => default', animate('250ms ease-out')),
                    transition('default => rotated', animate('250ms ease-in'))
                ])
            ],
            styles: [""]
        })
    ], ResourceManagementToolbarComponent);
    return ResourceManagementToolbarComponent;
}());
export { ResourceManagementToolbarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb3VyY2UtbWFuYWdlbWVudC10b29sYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Jlc291cmNlLW1hbmFnZW1lbnQvcmVzb3VyY2UtbWFuYWdlbWVudC10b29sYmFyL3Jlc291cmNlLW1hbmFnZW1lbnQtdG9vbGJhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsWUFBWSxFQUFhLE1BQU0sRUFBRSxZQUFZLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ2hILE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0YsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUM1QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUNqQyxPQUFPLEVBQUUsVUFBVSxFQUEwQixNQUFNLE1BQU0sQ0FBQztBQUMxRCxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFckMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUM3RixPQUFPLEVBQUUsOEJBQThCLEVBQUUsTUFBTSxrRUFBa0UsQ0FBQztBQUNsSCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNoRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUMzRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDM0QsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFFdEYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFFQUFxRSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBR2pILE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUNuRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2pGLE9BQU8sS0FBSyxPQUFPLE1BQU0sZ0NBQWdDLENBQUM7QUFDMUQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFFakcsT0FBTyxFQUFFLG9CQUFvQixFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDOUYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDL0UsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBR3pGLElBQU0sTUFBTSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztBQWVwQztJQXNJRSw0Q0FDUyxXQUF3QixFQUN4Qix1QkFBZ0QsRUFDaEQsWUFBMEIsRUFDMUIsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsUUFBbUIsRUFDbkIsS0FBbUIsRUFDbkIsaUJBQW9DLEVBQ3BDLHdCQUFrRCxFQUNsRCw4QkFBOEQsRUFDOUQsaUJBQW9DLEVBQ3BDLGFBQTRCLEVBQzVCLG1CQUF3QyxFQUN4QyxtQkFBd0MsRUFDeEMsaUJBQW9DLEVBQ3BDLGNBQThCLEVBQzlCLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLFVBQXNCLEVBQ3RCLGtCQUFzQyxFQUN0QyxXQUF3QixFQUN4QixrQkFBc0M7UUF0Qi9DLGlCQThCQztRQTdCUSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qiw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQXlCO1FBQ2hELGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUNuQixVQUFLLEdBQUwsS0FBSyxDQUFjO1FBQ25CLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtRQUNsRCxtQ0FBOEIsR0FBOUIsOEJBQThCLENBQWdDO1FBQzlELHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBMUp0QyxvQkFBZSxHQUFrRCxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQzlHLEdBQUcsQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLEtBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxNQUFNLENBQUMsRUFBN0MsQ0FBNkMsQ0FBQyxDQUM3RCxDQUFDO1FBQ00sa0JBQWEsR0FBd0IsRUFBRSxDQUFDO1FBRXRDLHlCQUFvQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFPekQsbUJBQWMsR0FBRyxFQUFFLENBQUM7UUFDcEIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNoQixZQUFPLEdBQUcsS0FBSyxDQUFDO1FBeUJoQix1QkFBa0IsR0FBRyxDQUFDLENBQUM7UUFFdkIsZ0JBQVcsR0FBRyxPQUFPLENBQUM7UUFDdEIscUJBQWdCLEdBQUcsR0FBRyxDQUFDLENBQUEsTUFBTTtRQUM3QixjQUFTLEdBQUcsTUFBTSxDQUFDO1FBR25CLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQWNwQixnQ0FBMkIsR0FBRyxFQUFFLENBQUM7UUFDakMsd0JBQW1CLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLGNBQVMsR0FBRyxFQUFFLENBQUM7UUFLaEI7V0FDRyxDQUFDLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUU5Qiw0QkFBdUIsR0FBZSxJQUFJLENBQUM7UUFDM0MsbUJBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUdwQix1QkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDM0Isa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFDckIsa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFHckIsK0JBQTBCLEdBQXlCLElBQUksQ0FBQztRQUV4RCxtQkFBYyxHQUFlLEVBQUUsQ0FBQztRQUVoQyxxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDdEIseUJBQW9CLEdBQUcsRUFBRSxDQUFDO1FBTTFCLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBRWxCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQixpQkFBWSxHQUFHLEtBQUssQ0FBQztRQUNyQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBSWpCLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFDZCxpQ0FBNEIsR0FBRyxHQUFHLENBQUM7UUFFbkMsaUJBQVksR0FBRztZQUNiLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUU7WUFDdEIsU0FBUyxFQUFFLEVBQUU7U0FDZCxDQUFDO1FBQ0YseUJBQW9CLEdBQUcsRUFBRSxDQUFDO1FBQzFCLHNCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUN2QixxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDdEIsb0JBQWUsR0FBRyxFQUFFLENBQUM7UUFDckIsb0JBQWUsR0FBRyxFQUFFLENBQUM7UUFpQnJCLFNBQUksR0FBRyxJQUFJLENBQUM7UUF1cEJaLCtDQUErQztRQUUvQyxjQUFTLEdBQUcsVUFBQyxFQUFFO1lBQ2IsSUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7WUFDMUMsSUFBSSxTQUFTLElBQUksR0FBRyxFQUFFO2dCQUNwQixLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN0QjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQTtRQXRvQkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxjQUFNLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxFQUF0QyxDQUFzQyxDQUFDO1FBQ3hFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7WUFDNUIsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1NBQ3hEO0lBQ0gsQ0FBQztJQUVELG1DQUFtQztJQUNuQyw2REFBZ0IsR0FBaEI7UUFFRSxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUVoQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsaUJBQWlCLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4RCxJQUFJLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDMUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGFBQWEsR0FBRyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixNQUFNO2FBQ1A7aUJBQU0sSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDN0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDbkYsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztnQkFDMUIsTUFBTTthQUNQO1NBQ0Y7UUFFRCx5QkFBeUI7UUFDekIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDcEQsSUFBSSxJQUFJLENBQUM7UUFFVCxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssTUFBTSxFQUFFO1lBQy9CLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDaEcsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBRTdFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxTQUFTLENBQUM7WUFDdEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQztZQUVsQyxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdkQsSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLE9BQU8sRUFBRTtZQUNoQyxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLO1lBQzdGLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUU3RSxJQUFJLENBQUMsb0JBQW9CLEdBQUcsU0FBUyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxPQUFPLENBQUM7WUFFbEMsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUNyQztRQUVELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUVyQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNuQjthQUFNO1lBRUwscUNBQXFDO1lBQ3JDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsRUFBNUMsQ0FBNEMsQ0FBQyxDQUFDO1NBQzdFO1FBQ0QsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELCtCQUErQjtJQUMvQix5REFBWSxHQUFaO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDaEMsSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ3hGLElBQUksQ0FBQyxhQUFhLEdBQUcsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7U0FDM0I7YUFBTTtZQUNMLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssaUJBQWlCLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2hKLElBQUksaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxhQUFhLEVBQUU7b0JBRXRELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM3QyxJQUFJLENBQUMsYUFBYSxHQUFHLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3JELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUNyQixNQUFNO2lCQUNQO2FBQ0Y7U0FDRjtRQUVEOztZQUVJO1FBRUosSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFcEQsSUFBSSxJQUFJLENBQUM7UUFFVCxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssTUFBTSxFQUFFO1lBQy9CLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDeEYsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRXpELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxTQUFTLENBQUM7WUFDdEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQztZQUVsQyxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdkQsSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLE9BQU8sRUFBRTtZQUNoQyxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDeEUsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSztZQUVwRixJQUFJLENBQUMsb0JBQW9CLEdBQUcsU0FBUyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxPQUFPLENBQUM7WUFFbEMsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUNyQztRQUVELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNuQjthQUFNO1lBRUwseUNBQXlDO1lBQ3pDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsRUFBNUMsQ0FBNEMsQ0FBQyxDQUFDO1NBQzdFO1FBQ0QseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELHlEQUFZLEdBQVosVUFBYSxLQUFLO1FBQ2hCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7SUFFbkMsQ0FBQztJQUVELDBEQUFhLEdBQWIsVUFBYyxJQUFJO1FBQ2hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFRCwyREFBYyxHQUFkLFVBQWUsTUFBTTtRQUNuQixJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztJQUM5QixDQUFDO0lBRUQsNkVBQWdDLEdBQWhDLFVBQWlDLE1BQWdCO1FBQy9DLElBQU0sYUFBYSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3hELElBQU0scUJBQXFCLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN4RSxJQUFNLG1CQUFtQixHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEUsSUFBTSxXQUFXLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDcEQsSUFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUMsSUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuSixJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDdEYsSUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ2hGLElBQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUM5RSxJQUFNLGVBQWUsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzdGLElBQU0sYUFBYSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN2RixJQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDM0UsT0FBTztZQUNMLGFBQWEsZUFBQTtZQUNiLHFCQUFxQix1QkFBQTtZQUNyQixtQkFBbUIscUJBQUE7WUFDbkIsV0FBVyxhQUFBO1lBQ1gsTUFBTSxRQUFBO1lBQ04sU0FBUyxXQUFBO1lBQ1QsVUFBVSxZQUFBO1lBQ1YsUUFBUSxVQUFBO1lBQ1IsVUFBVSxZQUFBO1lBQ1YsZUFBZSxpQkFBQTtZQUNmLGFBQWEsZUFBQTtZQUNiLFNBQVMsV0FBQTtTQUNWLENBQUM7SUFDSixDQUFDO0lBRUQscURBQVEsR0FBUjtRQUNFLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssS0FBSyxrQkFBa0IsQ0FBQyxpQkFBaUIsRUFBRTtZQUM3RSxJQUFJLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDakMsSUFBSSxDQUFDLFVBQVUsR0FBRyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUM7U0FDMUU7YUFBTSxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLEtBQUssa0JBQWtCLENBQUMsZ0JBQWdCLEVBQUU7WUFDbkYsSUFBSSxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxVQUFVLEdBQUcscUJBQXFCLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDO1NBQ3pFO2FBQU0sSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxLQUFLLGtCQUFrQixDQUFDLGFBQWEsRUFBRTtZQUNoRixJQUFJLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUM7WUFDN0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUM7U0FDdEU7SUFDSCxDQUFDO0lBRUQsZ0VBQW1CLEdBQW5CLFVBQW9CLE1BQW9CO1FBQ3RDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxNQUFNLENBQUM7UUFDcEMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUMxQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUN0RixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztTQUNyQjtRQUNELElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELDREQUFlLEdBQWY7UUFDRSxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztRQUNkLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO0lBQ2hCLENBQUM7SUFFRCwrREFBa0IsR0FBbEI7UUFDRSxJQUFNLFdBQVcsR0FBc0MsRUFBRSxDQUFDO1FBQzFELElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssRUFBRTtZQUNwQyxXQUFXLENBQUMsZUFBZSxDQUFDLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQztTQUNqRTtRQUNELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3pFLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1NBQ25GO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLFdBQVcsQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQzdDO1FBQ0QsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hCLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7U0FDdkQ7UUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdEIsV0FBVyxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7U0FDbkQ7UUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDM0M7UUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7U0FDekM7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDYixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQ3RCO1lBQ0QsV0FBVyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDdkM7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDbkksQ0FBQztJQUVELG1DQUFtQztJQUNuQyx1REFBVSxHQUFWLFVBQVcsUUFBUTtRQUVqQixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQTtRQUNqQyxJQUFJLElBQUksQ0FBQztRQUVULElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDO1FBQzVCLElBQU0sY0FBYyxHQUFHLElBQUksQ0FBQztRQUU1QixJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssTUFBTSxFQUFFO1lBRS9CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLENBQUM7WUFFNUIsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUVqQyxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwRCxJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUVoRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsUUFBUSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxNQUFNLENBQUM7WUFFakMsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRXJELElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUNyQzthQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxLQUFLLEVBQUU7WUFDckMsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNwQyxJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUUvQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsUUFBUSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxNQUFNLENBQUM7WUFFakMsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRXJELElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUVyQzthQUFNO1lBQ0wsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztZQUUzQixJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3BDLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRWhELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxRQUFRLENBQUM7WUFDckMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE1BQU0sQ0FBQztZQUVqQyxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFckQsSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBRXJDO1FBRUQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFFbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBRSx1QkFBdUI7WUFDL0MseUNBQXlDO1lBQ3pDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsRUFBNUMsQ0FBNEMsQ0FBQyxDQUFDO1NBQzdFO1FBRUQseUJBQXlCO0lBQzNCLENBQUM7SUFFRCxvRkFBb0Y7SUFDcEYsMkRBQWMsR0FBZDtRQUFBLGlCQVlDO1FBVkMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFFekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO1lBQ3hDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsT0FBTyxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUNwRixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRUosSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO1lBQ3RDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsT0FBTyxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUNwRixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRU4sQ0FBQztJQUVELGlEQUFpRDtJQUNqRCw4REFBaUIsR0FBakI7UUFFRSxJQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBRXRDLDRFQUE0RTtRQUM1RSxJQUFNLFdBQVcsR0FBRyxHQUFHLENBQUM7UUFFeEIsb0RBQW9EO1FBQ3BELElBQU0saUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDLENBQUM7UUFFaEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUUxRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFFM0YsSUFBSSxDQUFDLDJCQUEyQixHQUFHO1lBQ2pDLGtCQUFrQixFQUFFLEtBQUs7WUFDekIsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsSUFBSTtTQUN0QyxDQUFDO1FBRUYsSUFBSSxDQUFDLG1CQUFtQixHQUFHO1lBQ3pCLGtCQUFrQixFQUFFLFNBQVM7WUFDN0IsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsSUFBSTtTQUN0QyxDQUFDO1FBRUYsSUFBSSxDQUFDLFNBQVMsR0FBRztZQUNmLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJO1NBQzFDLENBQUM7SUFDSixDQUFDO0lBRUQsd0RBQVcsR0FBWCxVQUFZLGVBQWUsRUFBRSxVQUFVO1FBQ3JDLDBGQUEwRjtRQUMxRixJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9ILE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLGFBQWEsRUFBRSxlQUFlLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRUQ7OztTQUdLO0lBQ0wsbUVBQXNCLEdBQXRCLFVBQXVCLGVBQWU7UUFDcEMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUdELHNEQUFTLEdBQVQsVUFBVSxLQUFLLEVBQUUsUUFBUTtRQUF6QixpQkFnR0M7UUEvRkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN0QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLFVBQVUsQ0FBQztRQUNmLElBQUksUUFBUSxDQUFDLGVBQWUsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDOUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixPQUFPO1NBQ1I7UUFDRCxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVc7UUFDdEYsNENBQTRDO1FBQzVDLElBQUksQ0FBQyxLQUFLLEtBQUssVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixFQUFFLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN0UyxJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQ3pDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsT0FBTztTQUNSO1FBQ0Qsa0NBQWtDO1FBQ2xDLHVNQUF1TTtRQUV2TSwrTEFBK0w7UUFDL0wsY0FBYztRQUNkLElBQU0sZUFBZSxHQUF5QixJQUFJLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxHQUFHLEVBQUMsU0FBUyxFQUFDLEtBQUssRUFBQyxJQUFJLENBQUMsQ0FBQztRQUVyVyw0Q0FBNEM7UUFDNUMsSUFBTSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssS0FBSyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUM7UUFFOU8sSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxFQUFFLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzVLLFNBQVMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuRixlQUFlLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO1lBQ3hDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFFBQVE7WUFDdEQsUUFBUSxFQUFFLFlBQVksQ0FBQyxnQkFBZ0I7WUFDdkMsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtZQUM3Qyx3QkFBd0IsRUFBRSxzQkFBc0IsQ0FBQyxFQUFFO1lBQ25ELG1CQUFtQixFQUFFLEtBQUs7WUFDMUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxTQUFTLENBQUM7U0FDM0IsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQzlCO2FBQU07WUFDTCxVQUFVLEdBQUcscUJBQXFCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztTQUNyRDtRQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQywrQkFBK0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxXQUF1QjtZQUNuRyxJQUFNLFlBQVksR0FBRyxXQUFXLENBQUMsMkJBQTJCLElBQUksT0FBTyxXQUFXLENBQUMsMkJBQTJCLEtBQUssUUFBUTtnQkFDekgsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsMkJBQTJCLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNqRSxJQUFNLFVBQVUsR0FBa0I7Z0JBQ2hDLGdCQUFnQixFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJO2dCQUNwRCxPQUFPLEVBQUUsRUFBRTtnQkFDWCxxQkFBcUIsRUFBRTtvQkFDckIsZ0JBQWdCLEVBQUUsZUFBZSxDQUFDLG9CQUFvQjtpQkFDdkQ7Z0JBQ0Qsb0JBQW9CLEVBQUU7b0JBQ3BCLGVBQWUsRUFBRSxFQUFFO2lCQUNwQjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1osSUFBSSxFQUFFLEVBQUU7aUJBQ1Q7Z0JBQ0QsTUFBTSxFQUFFO29CQUNOLFVBQVUsRUFBRSxDQUFDO29CQUNiLFNBQVMsRUFBRSxHQUFHO2lCQUNmO2FBQ0YsQ0FBQztZQUVGLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFMUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQ3JCLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQW9CO2dCQUNwRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQixRQUFRLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztnQkFDOUIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3hCLElBQU0sU0FBTyxHQUFHLFlBQVksQ0FBQztvQkFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSzt3QkFDNUIsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO3dCQUNoQixJQUFJLEdBQUcsR0FBRyxFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUMsS0FBSzt3QkFDM0IsSUFBTSxjQUFjLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsU0FBTyxDQUFDLENBQUM7d0JBQzVFLElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLFNBQU8sQ0FBQyxDQUFDO3dCQUN4RSxJQUFNLElBQUksR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUN6RSxJQUFNLFVBQVUsR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQSxTQUFTO3dCQUMzRCxJQUFNLEtBQUssR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyw4Q0FBOEM7d0JBQ2pILHFGQUFxRjt3QkFDckYsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUM7d0JBQzNCLElBQU0saUJBQWlCLEdBQUc7NEJBQ3hCLElBQUksRUFBRSxJQUFJOzRCQUNWLElBQUksRUFBRSxLQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUk7NEJBQ2xGLEtBQUssRUFBRSxNQUFNO3lCQUNkLENBQUM7d0JBQ0YsUUFBUSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzt3QkFDakQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQ3hDLENBQUMsQ0FBQyxDQUFDO29CQUNILHNEQUFzRDtvQkFDdEQsUUFBUSxDQUFDLE1BQU0sR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2lCQUNoRDtZQUNILENBQUMsQ0FBQyxDQUNILENBQUM7WUFDRixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztJQUVELGlFQUFvQixHQUFwQjtRQUNFLElBQUksSUFBSSxDQUFDLHVCQUF1QixFQUFFO1lBQ2hDLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7WUFDdEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLDJCQUEyQixFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzdGLElBQUksSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksU0FBUyxLQUFLLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ2hFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7YUFDcEU7U0FDRjtJQUNILENBQUM7SUFFRCxtREFBTSxHQUFOLFVBQU8sZUFBd0IsRUFBRSxLQUFXO1FBRTFDO3dCQUNnQjtRQUNoQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUdELDZEQUFnQixHQUFoQixVQUFpQixLQUFlO1FBQWhDLGlCQXdFQztRQXZFQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7UUFDNUIsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztTQUM3QjtRQUNELElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNqQixJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxHQUFHLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLEdBQUcsQ0FBQztRQUVwTSxtTUFBbU0sQ0FBQyxhQUFhO1FBQ2pOLGNBQWM7UUFDZCxJQUFNLGVBQWUsR0FBeUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsR0FBRyxFQUFDLFNBQVMsRUFBQyxLQUFLLEVBQUMsSUFBSSxDQUFDLENBQUM7UUFFclcsSUFBSSxDQUFDLDBCQUEwQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQ3JFLElBQU0sZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLElBQUksb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDO1FBQzdPLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNsQixJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDakQsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLENBQUMsSUFBSSxLQUFLLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUF4QyxDQUF3QyxDQUFDLENBQUM7WUFDaEcsUUFBUSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDdkIsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTO29CQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTO2lCQUNuQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUNUO1FBQ0QsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixLQUFLLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUU7WUFDckosT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7U0FDNUM7YUFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNwRSxlQUFlLENBQUMsY0FBYyxHQUFHLGVBQWUsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQy9GO1FBQ0QsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxvQkFBb0IsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUVuSSxJQUFNLFVBQVUsR0FBa0I7WUFDaEMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUk7WUFDdEUsT0FBTyxFQUFFLE9BQU87WUFDaEIsUUFBUSxFQUFFO2dCQUNSLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSztnQkFDcEIsZUFBZSxFQUFFLGdCQUFnQjtnQkFDakMscUJBQXFCLEVBQUU7b0JBQ3JCLGdCQUFnQixXQUFNLGVBQWUsQ0FBQyxvQkFBb0IsRUFBSyxJQUFJLENBQUMsb0JBQW9CLENBQUM7aUJBQzFGO2FBQ0Y7WUFDRCxxQkFBcUIsRUFBRTtnQkFDckIsZ0JBQWdCLEVBQUUsZUFBZSxDQUFDLGNBQWM7YUFDakQ7WUFDRCxvQkFBb0IsRUFBRTtnQkFDcEIsZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsRUFBRTthQUMzSTtZQUNELFlBQVksRUFBRTtnQkFDWixJQUFJLEVBQUUsUUFBUTthQUNmO1lBQ0QsTUFBTSxFQUFFO2dCQUNOLFVBQVUsRUFBRSxJQUFJLENBQUMsSUFBSTtnQkFDckIsU0FBUyxFQUFFLElBQUk7YUFDaEI7U0FDRixDQUFDO1FBQ0YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUU7WUFFMUUsSUFBSSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUM7WUFDbEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsUUFBd0I7Z0JBQ3hFLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixLQUFJLENBQUMsdUJBQXVCLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM3SCxLQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7Z0JBQ3JDLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQztnQkFDeEQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM1QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNOLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsdUJBQXVCLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxDQUFDO2dCQUNuSCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDM0I7SUFDSCxDQUFDO0lBRUQsNERBQWUsR0FBZixVQUFnQixPQUFRO1FBQXhCLGlCQVdDO1FBVkMsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLElBQUksQ0FBQyxpQkFBaUIsQ0FBQywrQkFBK0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsV0FBdUI7Z0JBQ3hHLEtBQUksQ0FBQyx1QkFBdUIsR0FBRyxXQUFXLENBQUM7Z0JBQzNDLEtBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1lBQzlCLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxFQUFFO2dCQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3BCO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsb0RBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEIsQ0FBQztJQUVELCtEQUFrQixHQUFsQixVQUFtQixTQUE0QztRQUM3RCxJQUFJLGlCQUFpQixDQUFDO1FBQ3RCLElBQUksU0FBUyxDQUFDLGFBQWEsRUFBRTtZQUMzQixpQkFBaUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNO2dCQUN4RSxPQUFPLE1BQU0sQ0FBQyxLQUFLLEtBQUssU0FBUyxDQUFDLGFBQWEsQ0FBQztZQUNsRCxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTTtnQkFDL0IsT0FBTyxNQUFNLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQyxhQUFhLENBQUM7WUFDbEQsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELDRDQUE0QztRQUM1QyxJQUFJLENBQUMscUJBQXFCLEdBQUcsQ0FBQyxpQkFBaUIsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixJQUFJLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsSUFBSSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLHFEQUFxRDtRQUMzUCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEIsSUFBSSxTQUFTLENBQUMsTUFBTSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQztTQUN4QztRQUNELElBQUksU0FBUyxDQUFDLFNBQVMsRUFBRTtZQUN2QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQztTQUM5QztRQUVELElBQUksU0FBUyxDQUFDLFVBQVUsRUFBRTtZQUN4QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQztTQUNoRDtRQUNELElBQUksU0FBUyxDQUFDLFFBQVEsRUFBRTtZQUN0QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQztTQUM1QztRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLElBQUksSUFBSSxTQUFTLENBQUMsVUFBVSxDQUFDO2VBQ3ZHLENBQUMsU0FBUyxDQUFDLGVBQWUsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsS0FBSyxJQUFJLElBQUksU0FBUyxDQUFDLGVBQWUsQ0FBQztlQUM1SCxDQUFDLFNBQVMsQ0FBQyxhQUFhLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEtBQUssSUFBSSxJQUFJLFNBQVMsQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUN6SCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQztZQUN2QyxJQUFJLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQyxlQUFlLENBQUM7WUFDakQsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO1lBQzdDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBQzNCO2FBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxJQUFJLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUMvRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQztZQUNyQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNwQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUMzQjthQUFNO1lBQ0wsSUFBSSxTQUFTLENBQUMsVUFBVSxFQUFFO2dCQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUM7YUFDeEM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDeEI7WUFDRCxJQUFJLFNBQVMsQ0FBQyxlQUFlLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDLGVBQWUsQ0FBQzthQUNsRDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQzthQUM3QjtZQUNELElBQUksU0FBUyxDQUFDLGFBQWEsRUFBRTtnQkFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO2FBQzlDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2FBQzNCO1lBQ0QsSUFBSSxTQUFTLENBQUMsU0FBUyxFQUFFO2dCQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUM7YUFDdEM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7YUFDdkI7WUFDRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1lBQ2hDLElBQUksU0FBUyxDQUFDLFdBQVcsRUFBRTtnQkFDekIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2dCQUN0QixJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFO29CQUN6RixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUM1QjtxQkFBTSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRTtvQkFDbEQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2lCQUN4QjthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBYUQsd0JBQXdCO0lBRXhCLG9EQUFvRDtJQUNwRCwwREFBYSxHQUFiLFVBQWMsU0FBUyxFQUFFLE9BQU87UUFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFNBQVMsQ0FBQztRQUN0QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsT0FBTyxDQUFDO1FBQ2xDLElBQU0sS0FBSyxHQUFHLFNBQVMsQ0FBQyxDQUFDLDBDQUEwQztRQUNuRSxJQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsQ0FBQyxzQ0FBc0M7UUFDM0QsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxrRUFBcUIsR0FBckI7UUFDRSxJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLE9BQU8sRUFBRTtZQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDdEIsT0FBTyxHQUFHLE1BQU0sQ0FBQzthQUNsQjtZQUNELE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDakI7UUFDRCxPQUFPLE9BQU8sQ0FBQztJQUNqQixDQUFDO0lBRUQsd0RBQVcsR0FBWCxVQUFZLGFBQWE7UUFDdkIsSUFBSSxRQUFRLENBQUM7UUFDYixJQUFJLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDdkQsUUFBUSxHQUFHLEVBQUUsQ0FBQztTQUNmO2FBQU0sSUFBSSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxFQUFFO1lBQzdELFFBQVEsR0FBRyxFQUFFLENBQUM7U0FDZjthQUFNLElBQUksaUJBQWlCLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUM5RDs0Q0FDZ0M7WUFDaEMsUUFBUSxHQUFHLEVBQUUsQ0FBQztTQUNmO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDbEIsQ0FBQztJQUVELDJEQUFjLEdBQWQsVUFBZSxRQUFRLEVBQUUsZUFBZTtRQUF4QyxpQkEyQ0M7UUExQ0MsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsSUFBSSxvQkFBb0IsR0FBRyxFQUFFLENBQUM7WUFDOUIsSUFBTSxNQUFNLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxLQUFLLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMseUJBQXlCLENBQUM7WUFDdk4sSUFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM5RCxLQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO1lBQzlCLG9CQUFvQixHQUFHO2dCQUNyQixjQUFjLEVBQUUsYUFBYSxDQUFDLG9CQUFvQjtnQkFDbEQsTUFBTSxFQUFFLE1BQU0sQ0FBQyxPQUFPO2dCQUN0QixNQUFNLEVBQUUsRUFBRTtnQkFDVixhQUFhLEVBQUUsS0FBSztnQkFDcEIsWUFBWSxFQUFFLElBQUk7YUFDbkIsQ0FBQztZQUVGLEtBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDO2lCQUNsRCxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNqQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFO29CQUNyRCxJQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO29CQUM3RCxJQUFNLE9BQUssR0FBRyxFQUFFLENBQUM7b0JBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ3RELFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJOzRCQUNuQixJQUFNLFFBQVEsR0FBRyxPQUFLLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFuQixDQUFtQixDQUFDLENBQUM7NEJBQ3RELElBQUksQ0FBQyxRQUFRLEVBQUU7Z0NBQ2IsT0FBSyxDQUFDLElBQUksQ0FBQztvQ0FDVCxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0NBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO29DQUNkLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSTtpQ0FDdkIsQ0FBQyxDQUFDOzZCQUNKO3dCQUNILENBQUMsQ0FBQyxDQUFDO3FCQUNKO29CQUNELEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxPQUFLLENBQUM7aUJBQ2xDO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7aUJBQy9CO2dCQUNELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUU7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQscURBQVEsR0FBUixVQUFTLFFBQVEsRUFBRSxlQUFlO1FBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsZUFBZSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtRQUVqRSxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBb0NJO0lBQ0osY0FBYztJQUNkLGtFQUFxQixHQUFyQixVQUFzQixJQUFJLEVBQUUsTUFBTTtRQUFsQyxpQkFxQkM7UUFwQkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDcEUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTtnQkFDakYsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO2dCQUNyRSwyQ0FBMkM7Z0JBQzNDLHVFQUF1RTthQUN4RTtpQkFBTSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTtnQkFDeEYsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO2FBQ3RGO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQzthQUN0RjtRQUNILENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO1FBQ3ZGLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDhEQUFpQixHQUFqQixVQUFrQixJQUFJLEVBQUUsTUFBTTtRQUE5QixpQkEwQ0M7UUF6Q0MsSUFBSSxpQkFBaUIsQ0FBQztRQUN0QixpQkFBaUIsR0FBRztZQUNsQixlQUFlLEVBQUUsS0FBSztZQUN0Qix1QkFBdUIsRUFBRTtnQkFDdkIsWUFBWSxFQUFFO29CQUNaLFFBQVEsRUFBRSxNQUFNO2lCQUNqQjthQUNGO1lBQ0QsZUFBZSxFQUFFO2dCQUNmLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRTthQUNkO1NBQ0YsQ0FBQTtRQUNELElBQU0sVUFBVSxHQUFHO1lBQ2pCLGlCQUFpQixFQUFFLGlCQUFpQjtZQUNwQyxvQkFBb0IsRUFBRTtnQkFDcEIsbUJBQW1CLEVBQUUsRUFBRTthQUN4QjtTQUNGLENBQUM7UUFDRixJQUFJLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDO2FBQ2xELFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDakIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUk7bUJBQ3pHLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ25LLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsa0NBQWtDLENBQUMsQ0FBQztnQkFDckUsMkNBQTJDO2dCQUMzQyx1RUFBdUU7YUFDeEU7aUJBQU0sSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLO21CQUNqSCxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFO2dCQUNwRixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ3pFO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQzthQUN0RjtRQUNILENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO1FBQ3ZGLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG1EQUFNLEdBQU4sVUFBTyxJQUFJLEVBQUUsTUFBTTtRQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssZ0JBQWdCLENBQUMsTUFBTSxFQUFFO1lBQ3JELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDdEM7YUFBTTtZQUNMLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDMUM7SUFDSCxDQUFDO0lBRUQsbUVBQXNCLEdBQXRCLFVBQXVCLEtBQUs7UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxtRUFBc0IsR0FBdEIsVUFBdUIsTUFBTTtRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDO0lBQzlCLENBQUM7SUFFRDs7O1FBR0k7SUFFSiw4REFBaUIsR0FBakIsVUFBa0IsU0FBUyxFQUFFLE9BQU87UUFDbEMsSUFBSSxFQUFFLEdBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3BDLElBQUksRUFBRSxHQUFRLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2hDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFRCw4Q0FBOEM7SUFDOUMsc0JBQXNCO0lBQ3RCLGtDQUFrQztJQUNsQyw4REFBOEQ7SUFDOUQsbUJBQW1CO0lBQ25CLG1DQUFtQztJQUNuQyw4RUFBOEU7SUFDOUUsbUJBQW1CO0lBQ25CLElBQUk7SUFFSixvREFBb0Q7SUFDcEQscURBQVEsR0FBUjtRQUFBLGlCQWlGQztRQWhGQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDbEksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUM7UUFFcEYsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztRQUNoSSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1FBRXJJLElBQU0sZUFBZSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDbkMsSUFBTSxXQUFXLEdBQUcsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2xELElBQU0sWUFBWSxHQUFHLGVBQWUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDcEQsSUFBTSxXQUFXLEdBQUcsZUFBZSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWSxHQUFHLEdBQUcsR0FBRyxXQUFXLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQztRQUV4RSxJQUFJLENBQUMsYUFBYSxHQUFHLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQztRQUN4QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRCw4RUFBOEU7UUFDOUUsa0dBQWtHO1FBQ2xHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxHQUFHLE1BQU0sR0FBRyxXQUFXLEVBQUUsWUFBWSxHQUFHLEdBQUcsR0FBRyxRQUFRLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQyxDQUFDO1FBQzNHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUV4RCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQ3JCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSTtZQUM5QyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDMUcsSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLGVBQWUsSUFBSSxLQUFJLENBQUMsYUFBYSxJQUFJLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFJLENBQUMsYUFBYSxFQUFFO2dCQUMzRyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDM0IsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLElBQUksS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsYUFBYSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFO3dCQUN0SCxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7cUJBQ3hCO3lCQUFNO3dCQUNMLEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO3FCQUMzQjtpQkFDRjthQUNGO2lCQUFNLElBQUksS0FBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLElBQUksS0FBSSxDQUFDLGVBQWUsS0FBSyxJQUFJLElBQUksS0FBSSxDQUFDLGFBQWEsS0FBSyxJQUFJLElBQUksS0FBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLElBQUksS0FBSSxDQUFDLGFBQWEsRUFBRTtnQkFDcEosS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO2lCQUFNLElBQUksQ0FBQyxLQUFJLENBQUMsYUFBYSxFQUFFO2dCQUM5QixJQUFJLENBQUMsV0FBVyxFQUFFO29CQUNoQixLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztvQkFDcEIsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2lCQUN4QjthQUNGO1lBQ0QsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN0QixDQUFDLENBQUMsQ0FDSCxDQUFDO1FBRUYsV0FBVyxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FDckIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDakUsS0FBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQztZQUNqQyxJQUFJLEtBQUksQ0FBQyxTQUFTLElBQUksS0FBSSxDQUFDLGFBQWEsRUFBRTtnQkFDeEMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ25FLElBQUksQ0FBQyxDQUFDLEtBQUksQ0FBQyxhQUFhLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEtBQUksQ0FBQyxhQUFhLEVBQUU7d0JBQ3ZFLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzt3QkFDdkIsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7cUJBQzVCO2lCQUNGO2FBQ0Y7aUJBQU0sSUFBSSxLQUFJLENBQUMsU0FBUyxLQUFLLElBQUksSUFBSSxLQUFJLENBQUMsYUFBYSxFQUFFO2dCQUN4RCxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7YUFDeEI7aUJBQU0sSUFBSSxDQUFDLEtBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxXQUFXLEVBQUU7b0JBQ2hCLEtBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO29CQUM1QixLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztvQkFDcEIsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2lCQUN4QjthQUNGO1lBQ0QsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN0QixDQUFDLENBQUMsQ0FDSCxDQUFDO1FBQ0YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLFVBQUEsU0FBUztZQUN0QyxJQUFJLFNBQVMsQ0FBQyxXQUFXLEVBQUU7Z0JBQ3pCLEtBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLFdBQVcsQ0FBQztnQkFDekMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3BDO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNwQztRQUNILENBQUMsQ0FBQyxDQUNILENBQUM7SUFFSixDQUFDO0lBRUQsd0RBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxZQUFZLElBQUksT0FBQSxZQUFZLENBQUMsV0FBVyxFQUFFLEVBQTFCLENBQTBCLENBQUMsQ0FBQztRQUN2RSxNQUFNLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDN0QsQ0FBQzs7Z0JBbitCcUIsV0FBVztnQkFDQyx1QkFBdUI7Z0JBQ2xDLFlBQVk7Z0JBQ1YsY0FBYztnQkFDZCxjQUFjO2dCQUNwQixTQUFTO2dCQUNaLFlBQVk7Z0JBQ0EsaUJBQWlCO2dCQUNWLHdCQUF3QjtnQkFDbEIsOEJBQThCO2dCQUMzQyxpQkFBaUI7Z0JBQ3JCLGFBQWE7Z0JBQ1AsbUJBQW1CO2dCQUNuQixtQkFBbUI7Z0JBQ3JCLGlCQUFpQjtnQkFDcEIsY0FBYztnQkFDZixhQUFhO2dCQUNmLFdBQVc7Z0JBQ1osVUFBVTtnQkFDRixrQkFBa0I7Z0JBQ3pCLFdBQVc7Z0JBQ0osa0JBQWtCOztJQXJKckM7UUFBVCxNQUFNLEVBQUU7b0ZBQWdEO0lBNkN6RDtRQURDLFlBQVksQ0FBQyxZQUFZLENBQUM7K0VBQ1k7SUFHdkM7UUFEQyxZQUFZLENBQUMsY0FBYyxDQUFDOzZFQUNRO0lBR3JDO1FBREMsWUFBWSxDQUFDLGdCQUFnQixDQUFDOytFQUNRO0lBR3ZDO1FBREMsU0FBUyxDQUFDLGNBQWMsQ0FBQzs0RUFDRDtJQTdEZCxrQ0FBa0M7UUFiOUMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGlDQUFpQztZQUMzQyw0ejhCQUEyRDtZQUUzRCxVQUFVLEVBQUU7Z0JBQ1YsT0FBTyxDQUFDLGNBQWMsRUFBRTtvQkFDdEIsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsRUFBRSxTQUFTLEVBQUUsY0FBYyxFQUFFLENBQUMsQ0FBQztvQkFDdEQsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO29CQUN4RCxVQUFVLENBQUMsb0JBQW9CLEVBQUUsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUM7b0JBQzNELFVBQVUsQ0FBQyxvQkFBb0IsRUFBRSxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7aUJBQzNELENBQUM7YUFDSDs7U0FDRixDQUFDO09BQ1csa0NBQWtDLENBaW5DOUM7SUFBRCx5Q0FBQztDQUFBLEFBam5DRCxJQWluQ0M7U0FqbkNZLGtDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGRyZW4sIFF1ZXJ5TGlzdCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2hhbmdlRGV0ZWN0b3JSZWYsIEVsZW1lbnRSZWYsIE9uRGVzdHJveSwgUmVuZGVyZXIyLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgZXh0ZW5kTW9tZW50IH0gZnJvbSAnbW9tZW50LXJhbmdlJztcclxuaW1wb3J0ICogYXMgTW9tZW50IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YnNjcmlwdGlvbiwgZm9ya0pvaW4gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEZhY2V0Q2hhbmdlRXZlbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vZmFjZXQvc2VydmljZXMvZmFjZXQtY2hhbmdlLWV2ZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUGFyYW1NYXAgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNZWRpYU1hdGNoZXIgfSBmcm9tICdAYW5ndWxhci9jZGsvbGF5b3V0JztcclxuaW1wb3J0IHsgU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VhcmNoL3NlcnZpY2VzL3NlYXJjaC1jaGFuZ2UtZXZlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlVGFza01ldGFkYXRhU2VydmljZSB9IGZyb20gJy4uLy4uL2RlbGl2ZXJhYmxlLXRhc2svc2VydmljZS9kZWxpdmVyYWJsZS50YXNrLm1ldGFkYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy92aWV3LWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZWFyY2hEYXRhU2VydmljZSB9IGZyb20gJy4uLy4uL3NlYXJjaC9zZXJ2aWNlcy9zZWFyY2gtZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5kZXhlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9pbmRleGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUaXRsZUNhc2VQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC90YXNrcy90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDYWxlbmRhckNvbnN0YW50cyB9IGZyb20gJy4uL3NoYXJlZC9jb25zdGFudHMvQ2FsZW5kYXIuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTWF0U29ydERpcmVjdGlvbnMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NYXRTb3J0RGlyZWN0aW9uJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgU2VhcmNoQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU2VhcmNoQ29uZmlnQ29uc3RhbnRzJztcclxuaW1wb3J0IHsgQ3VzdG9tU2VsZWN0IH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY3VzdG9tLXNlbGVjdC9vYmplY3RzL0N1c3RvbVNlbGVjdE9iamVjdCc7XHJcbmltcG9ydCB7IE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBNUE1TZWFyY2hPcGVyYXRvcnMgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3IuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JOYW1lcyB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2NvbnN0YW50cy9NUE1TZWFyY2hPcGVyYXRvck5hbWUuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgU2VhcmNoUmVxdWVzdCB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL29iamVjdHMvU2VhcmNoUmVxdWVzdCc7XHJcbmltcG9ydCB7IFNlYXJjaFJlc3BvbnNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvb2JqZWN0cy9TZWFyY2hSZXNwb25zZSc7XHJcbmltcG9ydCB7IE9UTU1NUE1EYXRhVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9PVE1NTVBNRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0RlbGl2ZXJhYmxlVHlwZXMnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vcHJvamVjdC90YXNrcy90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IHRyaWdnZXIsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgYW5pbWF0ZSB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4vLi4vLi4vbXBtLXV0aWxzL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCB7IEFwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9hcHBsaWNhdGlvbi5jb25maWcuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgUmVzb3VyY2VNYW5hZ2VtZW50Q29tcG9uZW50UGFyYW1zLCBUYXNrU2VhcmNoQ29uZGl0aW9ucyB9IGZyb20gJy4uL29iamVjdC9SZXNvdXJjZU1hbmFnZW1lbnRDb21wb25lbnRQYXJhbXMnO1xyXG5pbXBvcnQgeyBHUk9VUF9CWV9BTExfRklMVEVSUywgR1JPVVBfQllfRklMVEVSUyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvR3JvdXBCeUZpbHRlcic7XHJcbmltcG9ydCB7IEdyb3VwQnlGaWx0ZXJUeXBlcyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvR3JvdXBCeUZpbHRlclR5cGVzJztcclxuaW1wb3J0IHsgUm91dGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3JvdXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWx0ZXJIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpbHRlci1oZWxwZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlU2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3QvdGFza3MvZGVsaXZlcmFibGUvZGVsaXZlcmFibGUuc2VydmljZSc7XHJcbmltcG9ydCB7IFN5bmNocm9uaXplVGFza0ZpbHRlcnNDb21wb25lbnQgfSBmcm9tICcuLi9zeW5jaHJvbml6ZS10YXNrLWZpbHRlcnMvc3luY2hyb25pemUtdGFzay1maWx0ZXJzLmNvbXBvbmVudCc7XHJcblxyXG5jb25zdCBtb21lbnQgPSBleHRlbmRNb21lbnQoTW9tZW50KTtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLXJlc291cmNlLW1hbmFnZW1lbnQtdG9vbGJhcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3Jlc291cmNlLW1hbmFnZW1lbnQtdG9vbGJhci5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vcmVzb3VyY2UtbWFuYWdlbWVudC10b29sYmFyLmNvbXBvbmVudC5jc3MnXSxcclxuICBhbmltYXRpb25zOiBbXHJcbiAgICB0cmlnZ2VyKCdyb3RhdGVkU3RhdGUnLCBbXHJcbiAgICAgIHN0YXRlKCdkZWZhdWx0Jywgc3R5bGUoeyB0cmFuc2Zvcm06ICdyb3RhdGUoMGRlZyknIH0pKSxcclxuICAgICAgc3RhdGUoJ3JvdGF0ZWQnLCBzdHlsZSh7IHRyYW5zZm9ybTogJ3JvdGF0ZSgxODBkZWcpJyB9KSksXHJcbiAgICAgIHRyYW5zaXRpb24oJ3JvdGF0ZWQgPT4gZGVmYXVsdCcsIGFuaW1hdGUoJzI1MG1zIGVhc2Utb3V0JykpLFxyXG4gICAgICB0cmFuc2l0aW9uKCdkZWZhdWx0ID0+IHJvdGF0ZWQnLCBhbmltYXRlKCcyNTBtcyBlYXNlLWluJykpXHJcbiAgICBdKVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFJlc291cmNlTWFuYWdlbWVudFRvb2xiYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICByZWFkb25seSBjb21wb25lbnRQYXJhbXM6IE9ic2VydmFibGU8UmVzb3VyY2VNYW5hZ2VtZW50Q29tcG9uZW50UGFyYW1zPiA9IHRoaXMuYWN0aXZhdGVkUm91dGUucXVlcnlQYXJhbU1hcC5waXBlKFxyXG4gICAgbWFwKHBhcmFtcyA9PiB0aGlzLnJlc291cmNlTWFuYWdlbWVudENvbXBvbmVudFJvdXRlKHBhcmFtcykpLFxyXG4gICk7XHJcbiAgcHJpdmF0ZSBzdWJzY3JpcHRpb25zOiBBcnJheTxTdWJzY3JpcHRpb24+ID0gW107XHJcblxyXG4gIEBPdXRwdXQoKSBhZGRlZFJlc291cmNlSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAvKiAgIEBWaWV3Q2hpbGQoU3luY2hyb25pemVUYXNrRmlsdGVyc0NvbXBvbmVudCkgc3luY2hyb25pemVUYXNrRmlsdGVyc0NvbXBvbmVudDogU3luY2hyb25pemVUYXNrRmlsdGVyc0NvbXBvbmVudDsgKi9cclxuXHJcblxyXG4gIGVuYWJsZUNhbXBhaWduO1xyXG4gIGFwcENvbmZpZzogYW55O1xyXG4gIGdyb3VwQnlGaWx0ZXJzID0gW107XHJcbiAgaXNSZXNldCA9IGZhbHNlO1xyXG4gIGlzUm91dGUgPSBmYWxzZTtcclxuICBzZWxlY3RlZEdyb3VwQnlGaWx0ZXI7XHJcbiAgZmFjZXRSZXN0cmljdGlvbkxpc3Q7XHJcbiAgbGV2ZWw7XHJcbiAgdmlld0NvbmZpZztcclxuICAvKiBtb21lbnRQdHIgPSBtb21lbnQ7XHJcbiAgdG9kYXkgPSB0aGlzLm1vbWVudFB0cigpLmZvcm1hdCgnWVlZWS1NTS1ERCcpOyAqL1xyXG4gIHNlYXJjaE5hbWU7XHJcbiAgc2F2ZWRTZWFyY2hOYW1lO1xyXG4gIGFkdlNlYXJjaERhdGE7XHJcbiAgZmFjZXREYXRhO1xyXG5cclxuICBzZWxlY3RlZExpc3RGaWx0ZXI7XHJcbiAgc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyO1xyXG4gIGVtYWlsTGlua0lkO1xyXG4gIHNlbGVjdGVkU29ydDtcclxuICBzZWxlY3RlZFNvcnRCeTtcclxuICBzZWxlY3RlZFNvcnRPcmRlcjtcclxuXHJcbiAgcGFnZVNpemU7XHJcbiAgcGFnZTtcclxuICBzZWxlY3RlZFBhZ2VOdW1iZXI7XHJcbiAgc2tpcDtcclxuICB0b3A6IG51bWJlcjtcclxuXHJcbiAgdG90YWxMaXN0RGF0YUNvdW50ID0gMDtcclxuXHJcbiAgZGlzcGxheVR5cGUgPSAnbW9udGgnO1xyXG4gIHZpZXdUeXBlQ2VsbFNpemUgPSAxNTA7Ly8gMzU7XHJcbiAgbW9tZW50UHRyID0gbW9tZW50O1xyXG4gIGN1cnJlbnRWaWV3U3RhcnREYXRlOiBtb21lbnQuTW9tZW50O1xyXG4gIGN1cnJlbnRWaWV3RW5kRGF0ZTogbW9tZW50Lk1vbWVudDtcclxuICBkYXRlcyA9IFtdO1xyXG4gIHNob3dXZWVrZW5kcyA9IHRydWU7XHJcblxyXG4gIEBWaWV3Q2hpbGRyZW4oJ2RhdGVIZWFkZXInKVxyXG4gIGNhbGVuZGFySGVhZGVyczogUXVlcnlMaXN0PEVsZW1lbnRSZWY+O1xyXG5cclxuICBAVmlld0NoaWxkcmVuKCdjYWxlbmRhckNlbGwnKVxyXG4gIGNhbGVuZGFyQ2VsbHM6IFF1ZXJ5TGlzdDxFbGVtZW50UmVmPjtcclxuXHJcbiAgQFZpZXdDaGlsZHJlbigncGFuZWxDb250YWluZXInKVxyXG4gIHBhbmVsQ29udGFpbmVyczogUXVlcnlMaXN0PEVsZW1lbnRSZWY+O1xyXG5cclxuICBAVmlld0NoaWxkKCdjYWxlbmRhclZpZXcnKVxyXG4gIGNhbGVuZGFyVmlldzogRWxlbWVudFJlZjtcclxuXHJcbiAgdGFza0FsbG9jYXRpb25PdmVybG9hZFN0eWxlID0ge307XHJcbiAgdGFza0FsbG9jYXRpb25TdHlsZSA9IHt9O1xyXG4gIGNlbGxTdHlsZSA9IHt9O1xyXG5cclxuICBjdXJyZW50RGF0ZTtcclxuXHJcbiAgc2VsZWN0ZWRNb250aDtcclxuIC8qICBtb250aHMgPSBbJ0phbnVhcnknLCAnZmVicnVhcnknLCAnTWFyY2gnLCAnQXByaWwnLCAnTWF5JywgJ0p1bmUnLCAnSnVseScsICdBdWd1c3QnLCAnU2VwdGVtYmVyJywgJ09jdG9iZXInLCAnTm92ZW1iZXInLCAnRGVjZW1iZXInXTtcclxuICAqLyBmYWNldEV4cGFuc2lvblN0YXRlID0gdHJ1ZTtcclxuXHJcbiAgbXlUYXNrVmlld0NvbmZpZ0RldGFpbHM6IFZpZXdDb25maWcgPSBudWxsO1xyXG4gIHNlYXJjaENvbmZpZ0lkID0gLTE7XHJcblxyXG4gIHNlbGVjdGVkUGFnZVNpemU7XHJcbiAgY29tcG9uZW50UmVuZGVyaW5nID0gZmFsc2U7XHJcbiAgaXNQYWdlUmVmcmVzaCA9IHRydWU7XHJcbiAgaXNEYXRhUmVmcmVzaCA9IHRydWU7XHJcbiAgcHJldmlvdXNSZXF1ZXN0O1xyXG5cclxuICBzdG9yZVNlYXJjaGNvbmRpdGlvbk9iamVjdDogVGFza1NlYXJjaENvbmRpdGlvbnMgPSBudWxsO1xyXG5cclxuICBzb3J0YWJsZUZpZWxkczogQXJyYXk8YW55PiA9IFtdO1xyXG5cclxuICBzZWFyY2hDb25kaXRpb25zID0gW107XHJcbiAgZW1haWxTZWFyY2hDb25kaXRpb24gPSBbXTtcclxuXHJcbiAgaXNBcHByb3ZlcjtcclxuICBpc01lbWJlcjtcclxuICBpc1Jldmlld2VyO1xyXG5cclxuICB2YWxpZGF0ZUZhY2V0ID0gZmFsc2U7XHJcbiAgc2lkZWJhclN0eWxlID0ge307XHJcblxyXG4gIHNjcm9sbGVkID0gZmFsc2U7XHJcbiAgaXNXZWVrVmlldyA9IGZhbHNlO1xyXG4gIHByZXZpb3VzRmxhZyA9IGZhbHNlO1xyXG4gIG5leHRGbGFnID0gZmFsc2U7XHJcblxyXG4gIG1vYmlsZVF1ZXJ5OiBNZWRpYVF1ZXJ5TGlzdDtcclxuICBwcml2YXRlIG1vYmlsZVF1ZXJ5TGlzdGVuZXI6ICgpID0+IHZvaWQ7XHJcbiAgYWxsVXNlcnMgPSBbXTtcclxuICBVTkFTU0lHTkVEX1RBU0tfU0VBUkNIX0xJTUlUID0gMTAwO1xyXG5cclxuICBldmVudERpc3BsYXkgPSB7XHJcbiAgICBkaXNwbGF5VmlldzogJ2RheVZpZXcnLFxyXG4gICAgZHVyYXRpb246IHsgZGF5czogMzAgfSxcclxuICAgIHJlc291cmNlczogW11cclxuICB9O1xyXG4gIGFsbEZpbHRlcmVkUmVzb3VyY2VzID0gW107XHJcbiAgdXNlcnNVbmF2YWlsYWJpdHkgPSBbXTtcclxuICBhbGxVc2Vyc0NhcGFjaXR5ID0gW107XHJcbiAgcHJvamVjdExpc3REYXRhID0gW107XHJcbiAgZGVsaXZlcmFibGVEYXRhID0gW107XHJcblxyXG4gIGhlaWdodDtcclxuXHJcbiAgdGVhbVJvbGVVc2VyT3B0aW9ucztcclxuXHJcbiAgZGVsaXZlcmFibGVEYXRhczogYW55O1xyXG5cclxuICBzZWxlY3RlZFVzZXJzO1xyXG4gIHNlbGVjdGVkSXRlbXM7XHJcblxyXG4gIHJlbW92ZWRGaWx0ZXI7XHJcbiAgYWxsb2NhdGlvbjtcclxuXHJcbiAgeWVhcjtcclxuXHJcblxyXG4gIE1hdGggPSBNYXRoO1xyXG4gIFxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgIHB1YmxpYyBmYWNldENoYW5nZUV2ZW50U2VydmljZTogRmFjZXRDaGFuZ2VFdmVudFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgcm91dGVTZXJ2aWNlOiBSb3V0ZVNlcnZpY2UsXHJcbiAgICBwdWJsaWMgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyByZW5kZXJlcjogUmVuZGVyZXIyLFxyXG4gICAgcHVibGljIG1lZGlhOiBNZWRpYU1hdGNoZXIsXHJcbiAgICBwdWJsaWMgY2hhbmdlRGV0ZWN0b3JSZWY6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgcHVibGljIHNlYXJjaENoYW5nZUV2ZW50U2VydmljZTogU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlLFxyXG4gICAgcHVibGljIGRlbGl2ZXJhYmxlVGFza01ldGFkYXRhU2VydmljZTogRGVsaXZlcmFibGVUYXNrTWV0YWRhdGFTZXJ2aWNlLFxyXG4gICAgcHVibGljIHZpZXdDb25maWdTZXJ2aWNlOiBWaWV3Q29uZmlnU2VydmljZSxcclxuICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZmlsdGVySGVscGVyU2VydmljZTogRmlsdGVySGVscGVyU2VydmljZSxcclxuICAgIHB1YmxpYyBzZWFyY2hEYXRhU2VydmljZTogU2VhcmNoRGF0YVNlcnZpY2UsXHJcbiAgICBwdWJsaWMgaW5kZXhlclNlcnZpY2U6IEluZGV4ZXJTZXJ2aWNlLFxyXG4gICAgcHVibGljIHRpdGxlY2FzZVBpcGU6IFRpdGxlQ2FzZVBpcGUsXHJcbiAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgdGFza1NlcnZpY2U6IFRhc2tTZXJ2aWNlLFxyXG4gICAgcHVibGljIGRlbGl2ZXJhYmxlU2VydmljZTogRGVsaXZlcmFibGVTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLm1vYmlsZVF1ZXJ5ID0gdGhpcy5tZWRpYS5tYXRjaE1lZGlhKCcobWF4LXdpZHRoOiA2MDBweCknKTtcclxuICAgIHRoaXMubW9iaWxlUXVlcnlMaXN0ZW5lciA9ICgpID0+IHRoaXMuY2hhbmdlRGV0ZWN0b3JSZWYuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgaWYgKHRoaXMubW9iaWxlUXVlcnkubWF0Y2hlcykge1xyXG4gICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IGRlcHJlY2F0aW9uXHJcbiAgICAgIHRoaXMubW9iaWxlUXVlcnkuYWRkTGlzdGVuZXIodGhpcy5tb2JpbGVRdWVyeUxpc3RlbmVyKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIFBSRVZJT1VTIG5hdmlnYXRpb24gZm9yIENhbGVuZGFyXHJcbiAgY2FsZW5kYXJQcmV2aW91cygpIHtcclxuXHJcbiAgICB0aGlzLnByZXZpb3VzRmxhZyA9IGZhbHNlO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5zZWxlY3RlZE1vbnRoKTtcclxuXHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IENhbGVuZGFyQ29uc3RhbnRzLm1vbnRocy5sZW5ndGg7IGkrKykge1xyXG4gICAgICBpZiAoQ2FsZW5kYXJDb25zdGFudHMubW9udGhzW2kgKyAxXSA9PT0gdGhpcy5zZWxlY3RlZE1vbnRoKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coQ2FsZW5kYXJDb25zdGFudHMubW9udGhzW2ldKTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkTW9udGggPSBDYWxlbmRhckNvbnN0YW50cy5tb250aHNbaV07XHJcbiAgICAgICAgdGhpcy5wcmV2aW91c0ZsYWcgPSB0cnVlO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICB9IGVsc2UgaWYgKENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1swXSA9PT0gdGhpcy5zZWxlY3RlZE1vbnRoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZE1vbnRoID0gQ2FsZW5kYXJDb25zdGFudHMubW9udGhzW0NhbGVuZGFyQ29uc3RhbnRzLm1vbnRocy5sZW5ndGggLSAxXTtcclxuICAgICAgICB0aGlzLnllYXIgPSB0aGlzLnllYXIgLSAxO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdG8gaGF2ZSBleGFjdCBhbGwgZGF0ZVxyXG4gICAgbGV0IG5vT2ZEYXlzID0gdGhpcy5nZXROb09mRGF5cyh0aGlzLnNlbGVjdGVkTW9udGgpO1xyXG4gICAgbGV0IGRheXM7XHJcblxyXG4gICAgaWYgKHRoaXMuZGlzcGxheVR5cGUgPT09ICd3ZWVrJykge1xyXG4gICAgICBjb25zdCBzdGFydERhdGUgPSB0aGlzLm1vbWVudFB0cih0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlKS5zdGFydE9mKCd3ZWVrJykuc3VidHJhY3QoMSwgJ3dlZWsnKTtcclxuICAgICAgY29uc3QgZW5kRGF0ZSA9IHRoaXMubW9tZW50UHRyKHRoaXMuY3VycmVudFZpZXdTdGFydERhdGUpLnN1YnRyYWN0KDEsICdkYXknKTtcclxuXHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdTdGFydERhdGUgPSBzdGFydERhdGU7XHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdFbmREYXRlID0gZW5kRGF0ZTtcclxuXHJcbiAgICAgIGNvbnN0IHJhbmdlID0gdGhpcy5tb21lbnRQdHIucmFuZ2Uoc3RhcnREYXRlLCBlbmREYXRlKTtcclxuICAgICAgZGF5cyA9IEFycmF5LmZyb20ocmFuZ2UuYnkoJ2RheXMnKSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuZGlzcGxheVR5cGUgPT09ICdtb250aCcpIHtcclxuICAgICAgY29uc3Qgc3RhcnREYXRlID0gdGhpcy5tb21lbnRQdHIodGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSkuc3VidHJhY3Qobm9PZkRheXMsICdkYXlzJyk7IC8vIDMwXHJcbiAgICAgIGNvbnN0IGVuZERhdGUgPSB0aGlzLm1vbWVudFB0cih0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlKS5zdWJ0cmFjdCgxLCAnZGF5Jyk7XHJcblxyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlID0gc3RhcnREYXRlO1xyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSA9IGVuZERhdGU7XHJcblxyXG4gICAgICBjb25zdCByYW5nZSA9IHRoaXMubW9tZW50UHRyLnJhbmdlKHN0YXJ0RGF0ZSwgZW5kRGF0ZSk7XHJcbiAgICAgIGRheXMgPSBBcnJheS5mcm9tKHJhbmdlLmJ5KCdkYXlzJykpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnNob3dXZWVrZW5kcykge1xyXG5cclxuICAgICAgdGhpcy5kYXRlcyA9IGRheXM7XHJcbiAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgLy8gcmVtb3ZlIHNhdCBhbmQgc3VuIGZyb20gZGF0ZSByYW5nZVxyXG4gICAgICB0aGlzLmRhdGVzID0gZGF5cy5maWx0ZXIoZCA9PiBkLmlzb1dlZWtkYXkoKSAhPT0gNiAmJiBkLmlzb1dlZWtkYXkoKSAhPT0gNyk7XHJcbiAgICB9XHJcbiAgICAvLyAgdGhpcy5yZXNpemVDYWxlbmRhcigpO1xyXG4gICAgdGhpcy5yZWxvYWQoZmFsc2UpO1xyXG4gIH1cclxuXHJcbiAgLy8gTkVYVCBuYXZpZ2F0aW9uIGZvciBDYWxlbmRhclxyXG4gIGNhbGVuZGFyTmV4dCgpIHtcclxuICAgIHRoaXMubmV4dEZsYWcgPSBmYWxzZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRNb250aCk7XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZE1vbnRoID09PSBDYWxlbmRhckNvbnN0YW50cy5tb250aHNbQ2FsZW5kYXJDb25zdGFudHMubW9udGhzLmxlbmd0aCAtIDFdKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRNb250aCA9IENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1swXTtcclxuICAgICAgdGhpcy55ZWFyID0gdGhpcy55ZWFyICsgMTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgQ2FsZW5kYXJDb25zdGFudHMubW9udGhzLmxlbmd0aCAmJiB0aGlzLnNlbGVjdGVkTW9udGggIT09IENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1tDYWxlbmRhckNvbnN0YW50cy5tb250aHMubGVuZ3RoIC0gMV07IGkrKykge1xyXG4gICAgICAgIGlmIChDYWxlbmRhckNvbnN0YW50cy5tb250aHNbaV0gPT09IHRoaXMuc2VsZWN0ZWRNb250aCkge1xyXG5cclxuICAgICAgICAgIGNvbnNvbGUubG9nKENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1tpICsgMV0pO1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZE1vbnRoID0gQ2FsZW5kYXJDb25zdGFudHMubW9udGhzW2kgKyAxXTtcclxuICAgICAgICAgIHRoaXMubmV4dEZsYWcgPSB0cnVlO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyogaWYgKHRoaXMuc2VsZWN0ZWRNb250aCA9PT0gQ2FsZW5kYXJDb25zdGFudHMubW9udGhzW0NhbGVuZGFyQ29uc3RhbnRzLm1vbnRocy5sZW5ndGggLSAxXSkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkTW9udGggPSBDYWxlbmRhckNvbnN0YW50cy5tb250aHNbMF07XHJcbiAgICB9ICovXHJcblxyXG4gICAgbGV0IG5vT2ZEYXlzID0gdGhpcy5nZXROb09mRGF5cyh0aGlzLnNlbGVjdGVkTW9udGgpO1xyXG5cclxuICAgIGxldCBkYXlzO1xyXG5cclxuICAgIGlmICh0aGlzLmRpc3BsYXlUeXBlID09PSAnd2VlaycpIHtcclxuICAgICAgY29uc3Qgc3RhcnREYXRlID0gdGhpcy5tb21lbnRQdHIodGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSkuZW5kT2YoJ3dlZWsnKS5hZGQoMSwgJ2RheScpO1xyXG4gICAgICBjb25zdCBlbmREYXRlID0gdGhpcy5tb21lbnRQdHIoc3RhcnREYXRlKS5hZGQoMSwgJ3dlZWsnKTtcclxuXHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdTdGFydERhdGUgPSBzdGFydERhdGU7XHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdFbmREYXRlID0gZW5kRGF0ZTtcclxuXHJcbiAgICAgIGNvbnN0IHJhbmdlID0gdGhpcy5tb21lbnRQdHIucmFuZ2Uoc3RhcnREYXRlLCBlbmREYXRlKTtcclxuICAgICAgZGF5cyA9IEFycmF5LmZyb20ocmFuZ2UuYnkoJ2RheXMnKSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuZGlzcGxheVR5cGUgPT09ICdtb250aCcpIHtcclxuICAgICAgY29uc3Qgc3RhcnREYXRlID0gdGhpcy5tb21lbnRQdHIodGhpcy5jdXJyZW50Vmlld0VuZERhdGUpLmFkZCgxLCAnZGF5Jyk7XHJcbiAgICAgIGNvbnN0IGVuZERhdGUgPSB0aGlzLm1vbWVudFB0cih0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSkuYWRkKG5vT2ZEYXlzLCAnZGF5cycpOyAvLyAzMFxyXG5cclxuICAgICAgdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSA9IHN0YXJ0RGF0ZTtcclxuICAgICAgdGhpcy5jdXJyZW50Vmlld0VuZERhdGUgPSBlbmREYXRlO1xyXG5cclxuICAgICAgY29uc3QgcmFuZ2UgPSB0aGlzLm1vbWVudFB0ci5yYW5nZShzdGFydERhdGUsIGVuZERhdGUpO1xyXG4gICAgICBkYXlzID0gQXJyYXkuZnJvbShyYW5nZS5ieSgnZGF5cycpKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5zaG93V2Vla2VuZHMpIHtcclxuICAgICAgdGhpcy5kYXRlcyA9IGRheXM7XHJcbiAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgLy8gcmVtb3ZlIHNhdCBhbmQgc3VuIGZyb20gdGhlIGRhdGUgcmFuZ2VcclxuICAgICAgdGhpcy5kYXRlcyA9IGRheXMuZmlsdGVyKGQgPT4gZC5pc29XZWVrZGF5KCkgIT09IDYgJiYgZC5pc29XZWVrZGF5KCkgIT09IDcpO1xyXG4gICAgfVxyXG4gICAgLy8gdGhpcy5yZXNpemVDYWxlbmRhcigpO1xyXG4gICAgdGhpcy5yZWxvYWQoZmFsc2UpO1xyXG4gIH1cclxuXHJcbiAgZmFjZXRUb2dnbGVkKGV2ZW50KSB7XHJcbiAgICB0aGlzLmZhY2V0RXhwYW5zaW9uU3RhdGUgPSBldmVudDtcclxuXHJcbiAgfVxyXG5cclxuICBzZWxlY3RlZFVzZXJyKHVzZXIpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRVc2VycyA9IHVzZXI7XHJcbiAgfVxyXG5cclxuICBzZWxlY3RlZEZpbHRlcihmaWx0ZXIpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRJdGVtcyA9IGZpbHRlcjtcclxuICB9XHJcblxyXG4gIHJlc291cmNlTWFuYWdlbWVudENvbXBvbmVudFJvdXRlKHBhcmFtczogUGFyYW1NYXApOiBSZXNvdXJjZU1hbmFnZW1lbnRDb21wb25lbnRQYXJhbXMge1xyXG4gICAgY29uc3QgZ3JvdXBCeUZpbHRlciA9IHBhcmFtcy5nZXQoJ2dyb3VwQnlGaWx0ZXInKSB8fCAnJztcclxuICAgIGNvbnN0IGRlbGl2ZXJhYmxlVGFza0ZpbHRlciA9IHBhcmFtcy5nZXQoJ2RlbGl2ZXJhYmxlVGFza0ZpbHRlcicpIHx8ICcnO1xyXG4gICAgY29uc3QgbGlzdERlcGVuZGVudEZpbHRlciA9IHBhcmFtcy5nZXQoJ2xpc3REZXBlbmRlbnRGaWx0ZXInKSB8fCAnJztcclxuICAgIGNvbnN0IGVtYWlsTGlua0lkID0gcGFyYW1zLmdldCgnZW1haWxMaW5rSWQnKSB8fCAnJztcclxuICAgIGNvbnN0IHNvcnRCeSA9IHBhcmFtcy5nZXQoJ3NvcnRCeScpIHx8ICcnO1xyXG4gICAgY29uc3Qgc29ydE9yZGVyID0gcGFyYW1zLmdldCgnc29ydE9yZGVyJykgPT09ICdhc2MnID8gTWF0U29ydERpcmVjdGlvbnMuQVNDIDogKHBhcmFtcy5nZXQoJ3NvcnRPcmRlcicpID09PSAnZGVzYycgPyBNYXRTb3J0RGlyZWN0aW9ucy5ERVNDIDogbnVsbCk7XHJcbiAgICBjb25zdCBwYWdlTnVtYmVyID0gcGFyYW1zLmdldCgncGFnZU51bWJlcicpID8gTnVtYmVyKHBhcmFtcy5nZXQoJ3BhZ2VOdW1iZXInKSkgOiBudWxsO1xyXG4gICAgY29uc3QgcGFnZVNpemUgPSBwYXJhbXMuZ2V0KCdwYWdlU2l6ZScpID8gTnVtYmVyKHBhcmFtcy5nZXQoJ3BhZ2VTaXplJykpIDogbnVsbDtcclxuICAgIGNvbnN0IHNlYXJjaE5hbWUgPSBwYXJhbXMuZ2V0KCdzZWFyY2hOYW1lJykgPyBwYXJhbXMuZ2V0KCdzZWFyY2hOYW1lJykgOiBudWxsO1xyXG4gICAgY29uc3Qgc2F2ZWRTZWFyY2hOYW1lID0gcGFyYW1zLmdldCgnc2F2ZWRTZWFyY2hOYW1lJykgPyBwYXJhbXMuZ2V0KCdzYXZlZFNlYXJjaE5hbWUnKSA6IG51bGw7XHJcbiAgICBjb25zdCBhZHZTZWFyY2hEYXRhID0gcGFyYW1zLmdldCgnYWR2U2VhcmNoRGF0YScpID8gcGFyYW1zLmdldCgnYWR2U2VhcmNoRGF0YScpIDogbnVsbDtcclxuICAgIGNvbnN0IGZhY2V0RGF0YSA9IHBhcmFtcy5nZXQoJ2ZhY2V0RGF0YScpID8gcGFyYW1zLmdldCgnZmFjZXREYXRhJykgOiBudWxsO1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZ3JvdXBCeUZpbHRlcixcclxuICAgICAgZGVsaXZlcmFibGVUYXNrRmlsdGVyLFxyXG4gICAgICBsaXN0RGVwZW5kZW50RmlsdGVyLFxyXG4gICAgICBlbWFpbExpbmtJZCxcclxuICAgICAgc29ydEJ5LFxyXG4gICAgICBzb3J0T3JkZXIsXHJcbiAgICAgIHBhZ2VOdW1iZXIsXHJcbiAgICAgIHBhZ2VTaXplLFxyXG4gICAgICBzZWFyY2hOYW1lLFxyXG4gICAgICBzYXZlZFNlYXJjaE5hbWUsXHJcbiAgICAgIGFkdlNlYXJjaERhdGEsXHJcbiAgICAgIGZhY2V0RGF0YVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHNldExldmVsKCkge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRHcm91cEJ5RmlsdGVyLnZhbHVlID09PSBHcm91cEJ5RmlsdGVyVHlwZXMuR1JPVVBfQllfQ0FNUEFJR04pIHtcclxuICAgICAgdGhpcy5sZXZlbCA9IE1QTV9MRVZFTFMuQ0FNUEFJR047XHJcbiAgICAgIHRoaXMudmlld0NvbmZpZyA9IFNlYXJjaENvbmZpZ0NvbnN0YW50cy5TRUFSQ0hfTkFNRS5STV9HUk9VUF9CWV9DQU1QQUlHTjtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5zZWxlY3RlZEdyb3VwQnlGaWx0ZXIudmFsdWUgPT09IEdyb3VwQnlGaWx0ZXJUeXBlcy5HUk9VUF9CWV9QUk9KRUNUKSB7XHJcbiAgICAgIHRoaXMubGV2ZWwgPSBNUE1fTEVWRUxTLlBST0pFQ1Q7XHJcbiAgICAgIHRoaXMudmlld0NvbmZpZyA9IFNlYXJjaENvbmZpZ0NvbnN0YW50cy5TRUFSQ0hfTkFNRS5STV9HUk9VUF9CWV9QUk9KRUNUO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLnNlbGVjdGVkR3JvdXBCeUZpbHRlci52YWx1ZSA9PT0gR3JvdXBCeUZpbHRlclR5cGVzLkdST1VQX0JZX1RBU0spIHtcclxuICAgICAgdGhpcy5sZXZlbCA9IE1QTV9MRVZFTFMuVEFTSztcclxuICAgICAgdGhpcy52aWV3Q29uZmlnID0gU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlJNX0dST1VQX0JZX1RBU0s7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBncm91cEJ5RmlsdGVyQ2hhbmdlKG9wdGlvbjogQ3VzdG9tU2VsZWN0KSB7XHJcbiAgICB0aGlzLmlzUmVzZXQgPSB0cnVlO1xyXG4gICAgdGhpcy5zZWxlY3RlZEdyb3VwQnlGaWx0ZXIgPSBvcHRpb247XHJcbiAgICB0aGlzLmZhY2V0UmVzdHJpY3Rpb25MaXN0ID0ge307XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgdGhpcy5mYWNldENoYW5nZUV2ZW50U2VydmljZS5yZXNldEZhY2V0KCk7XHJcbiAgICB0aGlzLnNldExldmVsKCk7XHJcbiAgICB0aGlzLmdldExldmVsRGV0YWlscygpO1xyXG4gICAgaWYgKCEodGhpcy5zZWFyY2hOYW1lIHx8IHRoaXMuc2F2ZWRTZWFyY2hOYW1lIHx8IHRoaXMuYWR2U2VhcmNoRGF0YSB8fCB0aGlzLmZhY2V0RGF0YSkpIHtcclxuICAgICAgdGhpcy5pc1JvdXRlID0gdHJ1ZTtcclxuICAgIH1cclxuICAgIHRoaXMucm91dGVDb21wb25lbnRWaWV3KCk7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gIH1cclxuXHJcbiAgcmVzZXRQYWdpbmF0aW9uKCkge1xyXG4gICAgdGhpcy5wYWdlID0gMTtcclxuICAgIHRoaXMuc2tpcCA9IDA7XHJcbiAgfVxyXG5cclxuICByb3V0ZUNvbXBvbmVudFZpZXcoKSB7XHJcbiAgICBjb25zdCBxdWVyeVBhcmFtczogUmVzb3VyY2VNYW5hZ2VtZW50Q29tcG9uZW50UGFyYW1zID0ge307XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZEdyb3VwQnlGaWx0ZXIudmFsdWUpIHtcclxuICAgICAgcXVlcnlQYXJhbXNbJ2dyb3VwQnlGaWx0ZXInXSA9IHRoaXMuc2VsZWN0ZWRHcm91cEJ5RmlsdGVyLnZhbHVlO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRTb3J0KSB7XHJcbiAgICAgIHF1ZXJ5UGFyYW1zWydzb3J0QnknXSA9IHRoaXMuc2VsZWN0ZWRTb3J0QnkgPyB0aGlzLnNlbGVjdGVkU29ydEJ5IDogbnVsbDtcclxuICAgICAgcXVlcnlQYXJhbXNbJ3NvcnRPcmRlciddID0gdGhpcy5zZWxlY3RlZFNvcnRPcmRlciA/IHRoaXMuc2VsZWN0ZWRTb3J0T3JkZXIgOiBudWxsO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuc2VhcmNoTmFtZSkge1xyXG4gICAgICBxdWVyeVBhcmFtc1snc2VhcmNoTmFtZSddID0gdGhpcy5zZWFyY2hOYW1lO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuc2F2ZWRTZWFyY2hOYW1lKSB7XHJcbiAgICAgIHF1ZXJ5UGFyYW1zWydzYXZlZFNlYXJjaE5hbWUnXSA9IHRoaXMuc2F2ZWRTZWFyY2hOYW1lO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuYWR2U2VhcmNoRGF0YSkge1xyXG4gICAgICBxdWVyeVBhcmFtc1snYWR2U2VhcmNoRGF0YSddID0gdGhpcy5hZHZTZWFyY2hEYXRhO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZmFjZXREYXRhKSB7XHJcbiAgICAgIHF1ZXJ5UGFyYW1zWydmYWNldERhdGEnXSA9IHRoaXMuZmFjZXREYXRhO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMucGFnZVNpemUpIHtcclxuICAgICAgcXVlcnlQYXJhbXNbJ3BhZ2VTaXplJ10gPSB0aGlzLnBhZ2VTaXplO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMucGFnZSkge1xyXG4gICAgICBpZiAodGhpcy5pc1Jlc2V0KSB7XHJcbiAgICAgICAgdGhpcy5yZXNldFBhZ2luYXRpb24oKTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkUGFnZU51bWJlciA9IHRoaXMucGFnZTtcclxuICAgICAgICB0aGlzLmlzUmVzZXQgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgICBxdWVyeVBhcmFtc1sncGFnZU51bWJlciddID0gdGhpcy5wYWdlO1xyXG4gICAgfVxyXG4gICAgdGhpcy5yb3V0ZVNlcnZpY2UuZ29Ub1Jlc291cmNlTWFuYWdlbWVudChxdWVyeVBhcmFtcywgdGhpcy5zZWFyY2hOYW1lLCB0aGlzLnNhdmVkU2VhcmNoTmFtZSwgdGhpcy5hZHZTZWFyY2hEYXRhLCB0aGlzLmZhY2V0RGF0YSk7XHJcbiAgfVxyXG5cclxuICAvLyB0b2dnbGUgbW9udGggLCB3ZWVrIGFuZCBkYXkgdmlld1xyXG4gIHRvZ2dsZVZpZXcodmlld1R5cGUpIHtcclxuXHJcbiAgICBjb25zb2xlLmxvZygndmlld1R5cGUnLCB2aWV3VHlwZSlcclxuICAgIGxldCBkYXlzO1xyXG5cclxuICAgIHRoaXMuZGlzcGxheVR5cGUgPSB2aWV3VHlwZTtcclxuICAgIGNvbnN0IGlzUmVpbml0aWFsaXplID0gdHJ1ZTtcclxuXHJcbiAgICBpZiAodGhpcy5kaXNwbGF5VHlwZSA9PT0gJ3dlZWsnKSB7XHJcblxyXG4gICAgICB0aGlzLnZpZXdUeXBlQ2VsbFNpemUgPSAxNTA7XHJcblxyXG4gICAgICBjb25zdCB0b2RheSA9IHRoaXMubW9tZW50UHRyKHt9KTtcclxuXHJcbiAgICAgIGNvbnN0IGZyb21EYXRlID0gdGhpcy5tb21lbnRQdHIoe30pLnN0YXJ0T2YoJ3dlZWsnKTtcclxuICAgICAgY29uc3QgdG9EYXRlID0gdGhpcy5tb21lbnRQdHIoe30pLmVuZE9mKCd3ZWVrJyk7XHJcblxyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlID0gZnJvbURhdGU7XHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdFbmREYXRlID0gdG9EYXRlO1xyXG5cclxuICAgICAgY29uc3QgcmFuZ2UgPSB0aGlzLm1vbWVudFB0ci5yYW5nZShmcm9tRGF0ZSwgdG9EYXRlKTtcclxuXHJcbiAgICAgIGRheXMgPSBBcnJheS5mcm9tKHJhbmdlLmJ5KCdkYXlzJykpO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmRpc3BsYXlUeXBlID09PSAnZGF5Jykge1xyXG4gICAgICBjb25zdCBmcm9tRGF0ZSA9IHRoaXMubW9tZW50UHRyKHt9KTtcclxuICAgICAgY29uc3QgdG9EYXRlID0gdGhpcy5tb21lbnRQdHIoKS5hZGQoMCwgJ2RheXMnKTtcclxuXHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdTdGFydERhdGUgPSBmcm9tRGF0ZTtcclxuICAgICAgdGhpcy5jdXJyZW50Vmlld0VuZERhdGUgPSB0b0RhdGU7XHJcblxyXG4gICAgICBjb25zdCByYW5nZSA9IHRoaXMubW9tZW50UHRyLnJhbmdlKGZyb21EYXRlLCB0b0RhdGUpO1xyXG5cclxuICAgICAgZGF5cyA9IEFycmF5LmZyb20ocmFuZ2UuYnkoJ2RheXMnKSk7XHJcblxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy52aWV3VHlwZUNlbGxTaXplID0gMzU7XHJcblxyXG4gICAgICBjb25zdCBmcm9tRGF0ZSA9IHRoaXMubW9tZW50UHRyKHt9KTtcclxuICAgICAgY29uc3QgdG9EYXRlID0gdGhpcy5tb21lbnRQdHIoKS5hZGQoMjksICdkYXlzJyk7XHJcblxyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlID0gZnJvbURhdGU7XHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdFbmREYXRlID0gdG9EYXRlO1xyXG5cclxuICAgICAgY29uc3QgcmFuZ2UgPSB0aGlzLm1vbWVudFB0ci5yYW5nZShmcm9tRGF0ZSwgdG9EYXRlKTtcclxuXHJcbiAgICAgIGRheXMgPSBBcnJheS5mcm9tKHJhbmdlLmJ5KCdkYXlzJykpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmRhdGVzID0gZGF5cztcclxuXHJcbiAgICBpZiAoIXRoaXMuc2hvd1dlZWtlbmRzKSB7IC8vIGRvIG5vdCBzaG93IHdlZWtlbmRzXHJcbiAgICAgIC8vIHJlbW92ZSBzYXQgYW5kIHN1biBmcm9tIHRoZSBkYXRlIHJhbmdlXHJcbiAgICAgIHRoaXMuZGF0ZXMgPSBkYXlzLmZpbHRlcihkID0+IGQuaXNvV2Vla2RheSgpICE9PSA2ICYmIGQuaXNvV2Vla2RheSgpICE9PSA3KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyB0aGlzLnJlc2l6ZUNhbGVuZGFyKCk7XHJcbiAgfVxyXG5cclxuICAvLyBSZXNpemluZyB0aGUgY2FsZW5kYXIgc2l6ZSBiYXNlZCBvbiB0aGUgdXNlcnMgZGlzcGxheSBhbmQgZm9yIG1vbnRoIGFuZCB3ZWVrIHZpZXdcclxuICByZXNpemVDYWxlbmRhcigpIHtcclxuXHJcbiAgICB0aGlzLmNhbGN1bGF0ZVZpZXdTaXplKCk7XHJcblxyXG4gICAgdGhpcy5jYWxlbmRhckhlYWRlcnMuZm9yRWFjaCgoKGVsZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoZWxlbS5uYXRpdmVFbGVtZW50LCAnd2lkdGgnLCB0aGlzLnZpZXdUeXBlQ2VsbFNpemUgKyAncHgnKTtcclxuICAgIH0pKTtcclxuXHJcbiAgICB0aGlzLmNhbGVuZGFyQ2VsbHMuZm9yRWFjaCgoKGVsZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoZWxlbS5uYXRpdmVFbGVtZW50LCAnd2lkdGgnLCB0aGlzLnZpZXdUeXBlQ2VsbFNpemUgKyAncHgnKTtcclxuICAgIH0pKTtcclxuXHJcbiAgfVxyXG5cclxuICAvLyBjYWxjdWxhdGUgdGhlIHZpZXcgc2l6ZSBmb3IgY2FsZW5kYXIgY29udGFpbmVyXHJcbiAgY2FsY3VsYXRlVmlld1NpemUoKSB7XHJcblxyXG4gICAgY29uc3Qgd2luZG93V2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aDtcclxuXHJcbiAgICAvLyBpdHMgYWx3YXlzIGNvbnN0YW50IHZhbHVlIHRoYW4gdGhlIHdpbmRvdyB3aWR0aCB0byBmaXQgaW4gY29ycmVjdGx5IGluIHB4XHJcbiAgICBjb25zdCByaWdodE1hcmdpbiA9IDI1NTtcclxuXHJcbiAgICAvLyB0aGlzIGlzIGdvaW5nIHRoZSBiZSB0aGUgY2FsZW5kYXIgY29udGFpbmVyIHdpZHRoXHJcbiAgICBjb25zdCBjYWxlbmRhclZpZXdXaWR0aCA9IE1hdGguZmxvb3Iod2luZG93V2lkdGggLSByaWdodE1hcmdpbik7XHJcblxyXG4gICAgdGhpcy52aWV3VHlwZUNlbGxTaXplID0gTWF0aC5mbG9vcihjYWxlbmRhclZpZXdXaWR0aCAvIHRoaXMuZGF0ZXMubGVuZ3RoKTtcclxuXHJcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuY2FsZW5kYXJWaWV3Lm5hdGl2ZUVsZW1lbnQsICd3aWR0aCcsIGNhbGVuZGFyVmlld1dpZHRoICsgJ3B4Jyk7XHJcblxyXG4gICAgdGhpcy50YXNrQWxsb2NhdGlvbk92ZXJsb2FkU3R5bGUgPSB7XHJcbiAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJ3JlZCcsXHJcbiAgICAgIHdpZHRoOiAodGhpcy52aWV3VHlwZUNlbGxTaXplKSArICdweCdcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy50YXNrQWxsb2NhdGlvblN0eWxlID0ge1xyXG4gICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjMDg5NUNFJyxcclxuICAgICAgd2lkdGg6ICh0aGlzLnZpZXdUeXBlQ2VsbFNpemUpICsgJ3B4J1xyXG4gICAgfTtcclxuXHJcbiAgICB0aGlzLmNlbGxTdHlsZSA9IHtcclxuICAgICAgd2lkdGg6ICh0aGlzLnZpZXdUeXBlQ2VsbFNpemUgKyAxKSArICdweCdcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBnZXRQcm9wZXJ0eShkZWxpdmVyYWJsZURhdGEsIG1hcHBlck5hbWUpIHtcclxuICAgIC8vIGNvbnN0IGxldmVsID0gdGhpcy5pc1Jldmlld2VyID8gTVBNX0xFVkVMUy5ERUxJVkVSQUJMRV9SRVZJRVcgOiBNUE1fTEVWRUxTLkRFTElWRVJBQkxFO1xyXG4gICAgY29uc3QgZGlzcGxheUNvbHVtbiA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIG1hcHBlck5hbWUsIE1QTV9MRVZFTFMuREVMSVZFUkFCTEUpO1xyXG4gICAgcmV0dXJuIHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4oZGlzcGxheUNvbHVtbiwgZGVsaXZlcmFibGVEYXRhKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAgICogQHNpbmNlIE1QTVYzLTk0NlxyXG4gICAgICogQHBhcmFtIGRlbGl2ZXJhYmxlVHlwZVxyXG4gICAgICovXHJcbiAgZ2V0RGVsaXZlcmFibGVUeXBlSWNvbihkZWxpdmVyYWJsZVR5cGUpIHtcclxuICAgIHJldHVybiB0aGlzLmRlbGl2ZXJhYmxlU2VydmljZS5nZXREZWxpdmVyYWJsZVR5cGVJY29uKGRlbGl2ZXJhYmxlVHlwZSk7XHJcbiAgfVxyXG5cclxuXHJcbiAgZXhwYW5kUm93KGV2ZW50LCByZXNvdXJjZSkge1xyXG4gICAgY29uc29sZS5sb2cocmVzb3VyY2UpO1xyXG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgbGV0IHZpZXdDb25maWc7XHJcbiAgICBpZiAocmVzb3VyY2UuZGVsaXZlcmFibGVEYXRhICYmIEFycmF5LmlzQXJyYXkocmVzb3VyY2UuZGVsaXZlcmFibGVEYXRhKSAmJiByZXNvdXJjZS5kZWxpdmVyYWJsZURhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBjb25zdCBkaXNwbGF5RmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLFxyXG4gICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IG1heC1saW5lLWxlbmd0aFxyXG4gICAgICB0aGlzLmxldmVsID09PSBNUE1fTEVWRUxTLkNBTVBBSUdOID8gTVBNRmllbGRDb25zdGFudHMuREVMSVZFUkFCTEVfTVBNX0ZJRUxEUy5ERUxJVkVSQUJMRV9DQU1QQUlHTl9JRCA6IHRoaXMubGV2ZWwgPT09IE1QTV9MRVZFTFMuUFJPSkVDVCA/IE1QTUZpZWxkQ29uc3RhbnRzLkRFTElWRVJBQkxFX01QTV9GSUVMRFMuREVMSVZFUkFCTEVfUFJPSkVDVF9JRCA6IE1QTUZpZWxkQ29uc3RhbnRzLkRFTElWRVJBQkxFX01QTV9GSUVMRFMuREVMSVZFUkFCTEVfVEFTS19JRCwgTVBNX0xFVkVMUy5ERUxJVkVSQUJMRSk7XHJcbiAgICBpZiAoIWRpc3BsYXlGaWVsZCB8fCAhcmVzb3VyY2VbJ0lURU1fSUQnXSkge1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICAvLyBNUE0tVjMtMjIyNCAtIGNvbW1lbnQgYmVsb3cgb25lXHJcbiAgICAvLyBjb25zdCBkYXRlcyA9IHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWicgKyAnIGFuZCAnICsgdGhpcy5tb21lbnRQdHIudXRjKHRoaXMuY3VycmVudFZpZXdFbmREYXRlKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWic7XHJcblxyXG4gICAgLy8gY29uc3QgY29uZGl0aW9uT2JqZWN0OiBUYXNrU2VhcmNoQ29uZGl0aW9ucyA9IHRoaXMuZmlsdGVySGVscGVyU2VydmljZS5nZXRTZWFyY2hDb25kaXRpb25OZXcodGhpcy5zZWxlY3RlZExpc3RGaWx0ZXIsIHRoaXMuc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyLCB0aGlzLmxldmVsLCBkYXRlcywgdW5kZWZpbmVkLCB0cnVlKTtcclxuICAgIC8vIE1QTS1WMy0yMjI0XHJcbiAgICBjb25zdCBjb25kaXRpb25PYmplY3Q6IFRhc2tTZWFyY2hDb25kaXRpb25zID0gdGhpcy5maWx0ZXJIZWxwZXJTZXJ2aWNlLmdldFNlYXJjaENvbmRpdGlvbk5ldyh0aGlzLnNlbGVjdGVkTGlzdEZpbHRlciwgdGhpcy5zZWxlY3RlZExpc3REZXBlbmRlbnRGaWx0ZXIsIHRoaXMubGV2ZWwsIHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWicsIHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSkuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onLHVuZGVmaW5lZCxmYWxzZSx0cnVlKTtcclxuXHJcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IG1heC1saW5lLWxlbmd0aFxyXG4gICAgY29uc3Qgb3RtbU1QTURhdGFUeXBlcyA9ICgodGhpcy5pc0FwcHJvdmVyIHx8IHRoaXMuaXNNZW1iZXIpICYmIHRoaXMuc2VsZWN0ZWRMaXN0RmlsdGVyLnZhbHVlICE9PSAnTVlfTkFfUkVWSUVXX1RBU0tTJykgPyBPVE1NTVBNRGF0YVR5cGVzLkRFTElWRVJBQkxFIDogdGhpcy5pc1Jldmlld2VyID8gT1RNTU1QTURhdGFUeXBlcy5ERUxJVkVSQUJMRV9SRVZJRVcgOiBPVE1NTVBNRGF0YVR5cGVzLkRFTElWRVJBQkxFO1xyXG5cclxuICAgIGNvbnN0IGNvbmRpdGlvbiA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmNyZWF0ZUNvbmRpdGlvbk9iamVjdCgnQ09OVEVOVF9UWVBFJywgTVBNU2VhcmNoT3BlcmF0b3JzLklTLCBNUE1TZWFyY2hPcGVyYXRvck5hbWVzLklTLCBNUE1TZWFyY2hPcGVyYXRvcnMuQU5ELCBvdG1tTVBNRGF0YVR5cGVzKTtcclxuICAgIGNvbmRpdGlvbiA/IGNvbmRpdGlvbk9iamVjdC5ERUxJVkVSQUJMRV9DT05EVElPTi5wdXNoKGNvbmRpdGlvbikgOiBjb25zb2xlLmxvZygnJyk7XHJcbiAgICBjb25kaXRpb25PYmplY3QuREVMSVZFUkFCTEVfQ09ORFRJT04ucHVzaCh7XHJcbiAgICAgIHR5cGU6IGRpc3BsYXlGaWVsZCA/IGRpc3BsYXlGaWVsZC5EQVRBX1RZUEUgOiAnc3RyaW5nJyxcclxuICAgICAgZmllbGRfaWQ6IGRpc3BsYXlGaWVsZC5JTkRFWEVSX0ZJRUxEX0lELFxyXG4gICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsXHJcbiAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogTVBNU2VhcmNoT3BlcmF0b3JOYW1lcy5JUyxcclxuICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogJ2FuZCcsXHJcbiAgICAgIHZhbHVlOiByZXNvdXJjZVsnSVRFTV9JRCddXHJcbiAgICB9KTtcclxuICAgIGlmICh0aGlzLnZpZXdDb25maWcpIHtcclxuICAgICAgdmlld0NvbmZpZyA9IHRoaXMudmlld0NvbmZpZztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHZpZXdDb25maWcgPSBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuVEFTSztcclxuICAgIH1cclxuICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3Q29uZmlnKS5zdWJzY3JpYmUoKHZpZXdEZXRhaWxzOiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgIGNvbnN0IHNlYXJjaENvbmZpZyA9IHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyAmJiB0eXBlb2Ygdmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHID09PSAnc3RyaW5nJ1xyXG4gICAgICAgID8gcGFyc2VJbnQodmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHLCAxMCkgOiBudWxsO1xyXG4gICAgICBjb25zdCBwYXJhbWV0ZXJzOiBTZWFyY2hSZXF1ZXN0ID0ge1xyXG4gICAgICAgIHNlYXJjaF9jb25maWdfaWQ6IHNlYXJjaENvbmZpZyA/IHNlYXJjaENvbmZpZyA6IG51bGwsXHJcbiAgICAgICAga2V5d29yZDogJycsXHJcbiAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uOiBjb25kaXRpb25PYmplY3QuREVMSVZFUkFCTEVfQ09ORFRJT05cclxuICAgICAgICB9LFxyXG4gICAgICAgIGZhY2V0X2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICBmYWNldF9jb25kaXRpb246IFtdXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzb3J0aW5nX2xpc3Q6IHtcclxuICAgICAgICAgIHNvcnQ6IFtdXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjdXJzb3I6IHtcclxuICAgICAgICAgIHBhZ2VfaW5kZXg6IDAsXHJcbiAgICAgICAgICBwYWdlX3NpemU6IDEwMFxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcblxyXG4gICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChcclxuICAgICAgICB0aGlzLmluZGV4ZXJTZXJ2aWNlLnNlYXJjaChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoKGRhdGE6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICAgIHJlc291cmNlLmRlbGl2ZXJhYmxlRGF0YSA9IFtdO1xyXG4gICAgICAgICAgaWYgKGRhdGEuZGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRGb3JtYXQgPSAnWVlZWS1NTS1ERCc7XHJcbiAgICAgICAgICAgIGRhdGEuZGF0YS5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xyXG4gICAgICAgICAgICAgIGxldCBzdHlsZXMgPSB7fTtcclxuICAgICAgICAgICAgICBsZXQgdG9wID0gNDQgKiBpbmRleDsgLy8gMzBcclxuICAgICAgICAgICAgICBjb25zdCBldmVudFN0YXJ0RGF0ZSA9IHRoaXMubW9tZW50UHRyKGl0ZW0uREVMSVZFUkFCTEVfU1RBUlRfREFURSwgZEZvcm1hdCk7XHJcbiAgICAgICAgICAgICAgY29uc3QgZXZlbnRFbmREYXRlID0gdGhpcy5tb21lbnRQdHIoaXRlbS5ERUxJVkVSQUJMRV9EVUVfREFURSwgZEZvcm1hdCk7XHJcbiAgICAgICAgICAgICAgY29uc3QgZGlmZiA9IChldmVudEVuZERhdGUuZGlmZihldmVudFN0YXJ0RGF0ZSkpIC8gKDEwMDAgKiA2MCAqIDYwICogMjQpO1xyXG4gICAgICAgICAgICAgIGNvbnN0IGV4dHJhUGl4ZWwgPSAoKGRpZmYgKyAxKSAlIDIpID09PSAwID8gMiA6IDA7Ly8gMCA6IDI7XHJcbiAgICAgICAgICAgICAgY29uc3Qgd2lkdGggPSAoKGRpZmYgKyAxKSAqICh0aGlzLnZpZXdUeXBlQ2VsbFNpemUgKyBleHRyYVBpeGVsKSk7IC8vICgoZGlmZiArIDEpICogKHRoaXMudmlld1R5cGVDZWxsU2l6ZSAtIDEpKTtcclxuICAgICAgICAgICAgICAvLyAgc3R5bGVzWyd3aWR0aCddID0gKCgoZGlmZiArIDEpICUgMikgPT09IDApID8gKHdpZHRoICsgJ3B4JykgOiAod2lkdGggKyA0KSArICdweCc7XHJcbiAgICAgICAgICAgICAgc3R5bGVzWyd0b3AnXSA9IHRvcCArICdweCc7XHJcbiAgICAgICAgICAgICAgY29uc3QgZGVsaXZlcmFibGVEZXRhaWwgPSB7XHJcbiAgICAgICAgICAgICAgICBkYXRhOiBpdGVtLFxyXG4gICAgICAgICAgICAgICAgaWNvbjogdGhpcy5nZXREZWxpdmVyYWJsZVR5cGVJY29uKHRoaXMuZ2V0UHJvcGVydHkoaXRlbSwgJ0RFTElWRVJBQkxFX1RZUEUnKSkuSUNPTixcclxuICAgICAgICAgICAgICAgIHN0eWxlOiBzdHlsZXNcclxuICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgIHJlc291cmNlLmRlbGl2ZXJhYmxlRGF0YS5wdXNoKGRlbGl2ZXJhYmxlRGV0YWlsKTtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNvdXJjZS5kZWxpdmVyYWJsZURhdGEpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgLyogIHJlc291cmNlLmhlaWdodCA9IDM1ICogZGF0YS5kYXRhLmxlbmd0aCArICdweCc7ICovXHJcbiAgICAgICAgICAgIHJlc291cmNlLmhlaWdodCA9IDUwICogZGF0YS5kYXRhLmxlbmd0aCArICdweCc7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgICAgKTtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgIH0pO1xyXG5cclxuICB9XHJcblxyXG4gIHVwZGF0ZVNlYXJjaENvbmZpZ0lkKCkge1xyXG4gICAgaWYgKHRoaXMubXlUYXNrVmlld0NvbmZpZ0RldGFpbHMpIHtcclxuICAgICAgY29uc3QgbGFzdFZhbHVlID0gdGhpcy5zZWFyY2hDb25maWdJZDtcclxuICAgICAgdGhpcy5zZWFyY2hDb25maWdJZCA9IHBhcnNlSW50KHRoaXMubXlUYXNrVmlld0NvbmZpZ0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHLCAxMCk7XHJcbiAgICAgIGlmICh0aGlzLnNlYXJjaENvbmZpZ0lkID4gMCAmJiBsYXN0VmFsdWUgIT09IHRoaXMuc2VhcmNoQ29uZmlnSWQpIHtcclxuICAgICAgICB0aGlzLnNlYXJjaENoYW5nZUV2ZW50U2VydmljZS51cGRhdGUodGhpcy5teVRhc2tWaWV3Q29uZmlnRGV0YWlscyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlbG9hZChyZXNldFBhZ2luYXRpb246IGJvb2xlYW4sIHZhbHVlPzogYW55KSB7XHJcblxyXG4gICAgLyogdmFyIHdpZHRoID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NhbGVuZGFyQ2VsbCcpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLndpZHRoO1xyXG4gICAgYWxlcnQod2lkdGgpOyAqL1xyXG4gICAgdGhpcy5nZXRsaXN0RGF0YUluZGV4KHJlc2V0UGFnaW5hdGlvbik7XHJcbiAgfVxyXG5cclxuXHJcbiAgZ2V0bGlzdERhdGFJbmRleCh2YWx1ZT86IGJvb2xlYW4pIHtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICB0aGlzLnByb2plY3RMaXN0RGF0YSA9IFtdO1xyXG4gICAgdGhpcy50b3RhbExpc3REYXRhQ291bnQgPSAwO1xyXG4gICAgaWYgKHZhbHVlKSB7XHJcbiAgICAgIHRoaXMucHJldmlvdXNSZXF1ZXN0ID0gbnVsbDtcclxuICAgIH1cclxuICAgIGxldCBrZXlXb3JkID0gJyc7XHJcbiAgICBjb25zdCBkYXRlcyA9IHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWicgKyAnIGFuZCAnICsgdGhpcy5tb21lbnRQdHIudXRjKHRoaXMuY3VycmVudFZpZXdFbmREYXRlKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWic7XHJcblxyXG4gICAgLyogIGNvbnN0IGNvbmRpdGlvbk9iamVjdDogVGFza1NlYXJjaENvbmRpdGlvbnMgPSB0aGlzLmZpbHRlckhlbHBlclNlcnZpY2UuZ2V0U2VhcmNoQ29uZGl0aW9uTmV3KHRoaXMuc2VsZWN0ZWRMaXN0RmlsdGVyLCB0aGlzLnNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlciwgdGhpcy5sZXZlbCwgZGF0ZXMsIHVuZGVmaW5lZCwgdHJ1ZSk7ICovIC8vIHRoaXMubGV2ZWxcclxuICAgIC8vIE1QTS1WMy0yMjI0XHJcbiAgICBjb25zdCBjb25kaXRpb25PYmplY3Q6IFRhc2tTZWFyY2hDb25kaXRpb25zID0gdGhpcy5maWx0ZXJIZWxwZXJTZXJ2aWNlLmdldFNlYXJjaENvbmRpdGlvbk5ldyh0aGlzLnNlbGVjdGVkTGlzdEZpbHRlciwgdGhpcy5zZWxlY3RlZExpc3REZXBlbmRlbnRGaWx0ZXIsIHRoaXMubGV2ZWwsIHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWicsIHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSkuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onLHVuZGVmaW5lZCxmYWxzZSx0cnVlKTtcclxuXHJcbiAgICB0aGlzLnN0b3JlU2VhcmNoY29uZGl0aW9uT2JqZWN0ID0gT2JqZWN0LmFzc2lnbih7fSwgY29uZGl0aW9uT2JqZWN0KTtcclxuICAgIGNvbnN0IG90bW1NUE1EYXRhVHlwZXMgPSAoKHRoaXMuaXNBcHByb3ZlciB8fCB0aGlzLmlzTWVtYmVyKSAmJiB0aGlzLnNlbGVjdGVkTGlzdEZpbHRlci52YWx1ZSAhPSAnTVlfTkFfUkVWSUVXX1RBU0tTJykgPyBPVE1NTVBNRGF0YVR5cGVzLkRFTElWRVJBQkxFIDogdGhpcy5pc1Jldmlld2VyID8gT1RNTU1QTURhdGFUeXBlcy5ERUxJVkVSQUJMRV9SRVZJRVcgOiBPVE1NTVBNRGF0YVR5cGVzLkRFTElWRVJBQkxFO1xyXG4gICAgbGV0IHNvcnRUZW1wID0gW107XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZFNvcnQgJiYgdGhpcy5zZWxlY3RlZFNvcnQuYWN0aXZlKSB7XHJcbiAgICAgIGNvbnN0IHNvcnRPcHRpb24gPSB0aGlzLnNvcnRhYmxlRmllbGRzLmZpbmQob3B0aW9uID0+IG9wdGlvbi5uYW1lID09PSB0aGlzLnNlbGVjdGVkU29ydC5hY3RpdmUpO1xyXG4gICAgICBzb3J0VGVtcCA9IHNvcnRPcHRpb24gPyBbe1xyXG4gICAgICAgIGZpZWxkX2lkOiBzb3J0T3B0aW9uLmluZGV4ZXJJZCxcclxuICAgICAgICBvcmRlcjogdGhpcy5zZWxlY3RlZFNvcnQuZGlyZWN0aW9uXHJcbiAgICAgIH1dIDogW107XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5zZWFyY2hDb25kaXRpb25zICYmIHRoaXMuc2VhcmNoQ29uZGl0aW9ucy5sZW5ndGggPiAwICYmIHRoaXMuc2VhcmNoQ29uZGl0aW9uc1swXS5tZXRhZGF0YV9maWVsZF9pZCA9PT0gdGhpcy5zZWFyY2hEYXRhU2VydmljZS5LRVlXT1JEX01BUFBFUikge1xyXG4gICAgICBrZXlXb3JkID0gdGhpcy5zZWFyY2hDb25kaXRpb25zWzBdLmtleXdvcmQ7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuc2VhcmNoQ29uZGl0aW9ucyAmJiB0aGlzLnNlYXJjaENvbmRpdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICBjb25kaXRpb25PYmplY3QuVEFTS19DT05ESVRJT04gPSBjb25kaXRpb25PYmplY3QuVEFTS19DT05ESVRJT04uY29uY2F0KHRoaXMuc2VhcmNoQ29uZGl0aW9ucyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmVtYWlsU2VhcmNoQ29uZGl0aW9uID0gdGhpcy5lbWFpbFNlYXJjaENvbmRpdGlvbiAmJiBBcnJheS5pc0FycmF5KHRoaXMuZW1haWxTZWFyY2hDb25kaXRpb24pID8gdGhpcy5lbWFpbFNlYXJjaENvbmRpdGlvbiA6IFtdO1xyXG5cclxuICAgIGNvbnN0IHBhcmFtZXRlcnM6IFNlYXJjaFJlcXVlc3QgPSB7XHJcbiAgICAgIHNlYXJjaF9jb25maWdfaWQ6IHRoaXMuc2VhcmNoQ29uZmlnSWQgPiAwID8gdGhpcy5zZWFyY2hDb25maWdJZCA6IG51bGwsXHJcbiAgICAgIGtleXdvcmQ6IGtleVdvcmQsXHJcbiAgICAgIGdyb3VwaW5nOiB7XHJcbiAgICAgICAgZ3JvdXBfYnk6IHRoaXMubGV2ZWwsXHJcbiAgICAgICAgZ3JvdXBfZnJvbV90eXBlOiBvdG1tTVBNRGF0YVR5cGVzLFxyXG4gICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogWy4uLmNvbmRpdGlvbk9iamVjdC5ERUxJVkVSQUJMRV9DT05EVElPTiwgLi4udGhpcy5lbWFpbFNlYXJjaENvbmRpdGlvbl1cclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgIHNlYXJjaF9jb25kaXRpb246IGNvbmRpdGlvbk9iamVjdC5UQVNLX0NPTkRJVElPTlxyXG4gICAgICB9LFxyXG4gICAgICBmYWNldF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgIGZhY2V0X2NvbmRpdGlvbjogKHRoaXMuZmFjZXRSZXN0cmljdGlvbkxpc3QgJiYgdGhpcy5mYWNldFJlc3RyaWN0aW9uTGlzdC5mYWNldF9jb25kaXRpb24pID8gdGhpcy5mYWNldFJlc3RyaWN0aW9uTGlzdC5mYWNldF9jb25kaXRpb24gOiBbXVxyXG4gICAgICB9LFxyXG4gICAgICBzb3J0aW5nX2xpc3Q6IHtcclxuICAgICAgICBzb3J0OiBzb3J0VGVtcFxyXG4gICAgICB9LFxyXG4gICAgICBjdXJzb3I6IHtcclxuICAgICAgICBwYWdlX2luZGV4OiB0aGlzLnNraXAsXHJcbiAgICAgICAgcGFnZV9zaXplOiAxMDAwXHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgaWYgKCEoSlNPTi5zdHJpbmdpZnkodGhpcy5wcmV2aW91c1JlcXVlc3QpID09PSBKU09OLnN0cmluZ2lmeShwYXJhbWV0ZXJzKSkpIHtcclxuXHJcbiAgICAgIHRoaXMucHJldmlvdXNSZXF1ZXN0ID0gcGFyYW1ldGVycztcclxuICAgICAgdGhpcy5pbmRleGVyU2VydmljZS5zZWFyY2gocGFyYW1ldGVycykuc3Vic2NyaWJlKChyZXNwb25zZTogU2VhcmNoUmVzcG9uc2UpID0+IHtcclxuICAgICAgICB0aGlzLmlzRGF0YVJlZnJlc2ggPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmZhY2V0Q2hhbmdlRXZlbnRTZXJ2aWNlLnVwZGF0ZUZhY2V0RmlsdGVyKHJlc3BvbnNlLmZhY2V0X2ZpZWxkX3Jlc3BvbnNlX2xpc3QgPyByZXNwb25zZS5mYWNldF9maWVsZF9yZXNwb25zZV9saXN0IDogW10pO1xyXG4gICAgICAgIHRoaXMucHJvamVjdExpc3REYXRhID0gcmVzcG9uc2UuZGF0YTtcclxuICAgICAgICB0aGlzLnRvdGFsTGlzdERhdGFDb3VudCA9IHJlc3BvbnNlLmN1cnNvci50b3RhbF9yZWNvcmRzO1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0ZhaWxlZCB3aGlsZSBnZXR0aW5nICcgKyB0aGlzLnRpdGxlY2FzZVBpcGUudHJhbnNmb3JtKHRoaXMudmlld0NvbmZpZykgKyAnIGxpc3RzJyk7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0TGV2ZWxEZXRhaWxzKHRyaWdnZXI/KSB7XHJcbiAgICBpZiAodGhpcy52aWV3Q29uZmlnKSB7XHJcbiAgICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh0aGlzLnZpZXdDb25maWcpLnN1YnNjcmliZSgodmlld0RldGFpbHM6IFZpZXdDb25maWcpID0+IHtcclxuICAgICAgICB0aGlzLm15VGFza1ZpZXdDb25maWdEZXRhaWxzID0gdmlld0RldGFpbHM7XHJcbiAgICAgICAgdGhpcy51cGRhdGVTZWFyY2hDb25maWdJZCgpO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGlmICghKHRyaWdnZXIgPT09IHRydWUpKSB7XHJcbiAgICAgICAgdGhpcy5yZWxvYWQoZmFsc2UpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKCkge1xyXG4gICAgdGhpcy5yZWxvYWQodHJ1ZSk7XHJcbiAgfVxyXG5cclxuICBpbml0YWxpemVDb21wb25lbnQocm91dGVEYXRhOiBSZXNvdXJjZU1hbmFnZW1lbnRDb21wb25lbnRQYXJhbXMpIHtcclxuICAgIGxldCByb3V0ZWRHcm91cEZpbHRlcjtcclxuICAgIGlmIChyb3V0ZURhdGEuZ3JvdXBCeUZpbHRlcikge1xyXG4gICAgICByb3V0ZWRHcm91cEZpbHRlciA9IHRoaXMuZW5hYmxlQ2FtcGFpZ24gPyBHUk9VUF9CWV9BTExfRklMVEVSUy5maW5kKGZpbHRlciA9PiB7XHJcbiAgICAgICAgcmV0dXJuIGZpbHRlci52YWx1ZSA9PT0gcm91dGVEYXRhLmdyb3VwQnlGaWx0ZXI7XHJcbiAgICAgIH0pIDogR1JPVVBfQllfRklMVEVSUy5maW5kKGZpbHRlciA9PiB7XHJcbiAgICAgICAgcmV0dXJuIGZpbHRlci52YWx1ZSA9PT0gcm91dGVEYXRhLmdyb3VwQnlGaWx0ZXI7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBtYXgtbGluZS1sZW5ndGhcclxuICAgIHRoaXMuc2VsZWN0ZWRHcm91cEJ5RmlsdGVyID0gKHJvdXRlZEdyb3VwRmlsdGVyID09PSB1bmRlZmluZWQpID8gdGhpcy5lbmFibGVDYW1wYWlnbiA/IEdST1VQX0JZX0FMTF9GSUxURVJTWzBdIDogcm91dGVkR3JvdXBGaWx0ZXIgfHwgR1JPVVBfQllfRklMVEVSU1swXSA6IHJvdXRlZEdyb3VwRmlsdGVyIHx8IEdST1VQX0JZX0ZJTFRFUlNbMF07IC8qIHRoaXMuZW5hYmxlQ2FtcGFpZ24gPyBHUk9VUF9CWV9BTExfRklMVEVSU1swXSA6ICovXHJcbiAgICB0aGlzLnNldExldmVsKCk7XHJcblxyXG4gICAgaWYgKHJvdXRlRGF0YS5zb3J0QnkpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZFNvcnRCeSA9IHJvdXRlRGF0YS5zb3J0Qnk7XHJcbiAgICB9XHJcbiAgICBpZiAocm91dGVEYXRhLnNvcnRPcmRlcikge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkU29ydE9yZGVyID0gcm91dGVEYXRhLnNvcnRPcmRlcjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAocm91dGVEYXRhLnBhZ2VOdW1iZXIpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZFBhZ2VOdW1iZXIgPSByb3V0ZURhdGEucGFnZU51bWJlcjtcclxuICAgIH1cclxuICAgIGlmIChyb3V0ZURhdGEucGFnZVNpemUpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZFBhZ2VTaXplID0gcm91dGVEYXRhLnBhZ2VTaXplO1xyXG4gICAgfVxyXG4gICAgaWYgKChyb3V0ZURhdGEuc2VhcmNoTmFtZSA9PT0gbnVsbCAmJiB0aGlzLnNlYXJjaE5hbWUpIHx8ICh0aGlzLnNlYXJjaE5hbWUgPT09IG51bGwgJiYgcm91dGVEYXRhLnNlYXJjaE5hbWUpXHJcbiAgICAgIHx8IChyb3V0ZURhdGEuc2F2ZWRTZWFyY2hOYW1lID09PSBudWxsICYmIHRoaXMuc2F2ZWRTZWFyY2hOYW1lKSB8fCAodGhpcy5zYXZlZFNlYXJjaE5hbWUgPT09IG51bGwgJiYgcm91dGVEYXRhLnNhdmVkU2VhcmNoTmFtZSlcclxuICAgICAgfHwgKHJvdXRlRGF0YS5hZHZTZWFyY2hEYXRhID09PSBudWxsICYmIHRoaXMuYWR2U2VhcmNoRGF0YSkgfHwgKHRoaXMuYWR2U2VhcmNoRGF0YSA9PT0gbnVsbCAmJiByb3V0ZURhdGEuYWR2U2VhcmNoRGF0YSkpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZFBhZ2VOdW1iZXIgPSAxO1xyXG4gICAgICB0aGlzLnNlYXJjaE5hbWUgPSByb3V0ZURhdGEuc2VhcmNoTmFtZTtcclxuICAgICAgdGhpcy5zYXZlZFNlYXJjaE5hbWUgPSByb3V0ZURhdGEuc2F2ZWRTZWFyY2hOYW1lO1xyXG4gICAgICB0aGlzLmFkdlNlYXJjaERhdGEgPSByb3V0ZURhdGEuYWR2U2VhcmNoRGF0YTtcclxuICAgICAgdGhpcy5pc1Jlc2V0ID0gdHJ1ZTtcclxuICAgICAgdGhpcy5yb3V0ZUNvbXBvbmVudFZpZXcoKTtcclxuICAgIH0gZWxzZSBpZiAoKHJvdXRlRGF0YS5mYWNldERhdGEgPT09IG51bGwgJiYgdGhpcy5mYWNldERhdGEpIHx8ICh0aGlzLmZhY2V0RGF0YSA9PT0gbnVsbCAmJiByb3V0ZURhdGEuZmFjZXREYXRhKSkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkUGFnZU51bWJlciA9IDE7XHJcbiAgICAgIHRoaXMuZmFjZXREYXRhID0gcm91dGVEYXRhLmZhY2V0RGF0YTtcclxuICAgICAgdGhpcy5pc1Jlc2V0ID0gdHJ1ZTtcclxuICAgICAgdGhpcy5yb3V0ZUNvbXBvbmVudFZpZXcoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmIChyb3V0ZURhdGEuc2VhcmNoTmFtZSkge1xyXG4gICAgICAgIHRoaXMuc2VhcmNoTmFtZSA9IHJvdXRlRGF0YS5zZWFyY2hOYW1lO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuc2VhcmNoTmFtZSA9IG51bGw7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHJvdXRlRGF0YS5zYXZlZFNlYXJjaE5hbWUpIHtcclxuICAgICAgICB0aGlzLnNhdmVkU2VhcmNoTmFtZSA9IHJvdXRlRGF0YS5zYXZlZFNlYXJjaE5hbWU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zYXZlZFNlYXJjaE5hbWUgPSBudWxsO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChyb3V0ZURhdGEuYWR2U2VhcmNoRGF0YSkge1xyXG4gICAgICAgIHRoaXMuYWR2U2VhcmNoRGF0YSA9IHJvdXRlRGF0YS5hZHZTZWFyY2hEYXRhO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuYWR2U2VhcmNoRGF0YSA9IG51bGw7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHJvdXRlRGF0YS5mYWNldERhdGEpIHtcclxuICAgICAgICB0aGlzLmZhY2V0RGF0YSA9IHJvdXRlRGF0YS5mYWNldERhdGE7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5mYWNldERhdGEgPSBudWxsO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuY29tcG9uZW50UmVuZGVyaW5nID0gZmFsc2U7XHJcbiAgICAgIGlmIChyb3V0ZURhdGEuZW1haWxMaW5rSWQpIHtcclxuICAgICAgICB0aGlzLmdldExldmVsRGV0YWlscygpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZW1haWxMaW5rSWQgPSAnJztcclxuICAgICAgICBpZiAodGhpcy5pc1BhZ2VSZWZyZXNoICYmICh0aGlzLnNlYXJjaE5hbWUgfHwgdGhpcy5hZHZTZWFyY2hEYXRhIHx8IHRoaXMuc2F2ZWRTZWFyY2hOYW1lKSkge1xyXG4gICAgICAgICAgdGhpcy5nZXRMZXZlbERldGFpbHModHJ1ZSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICghKHRoaXMuaXNQYWdlUmVmcmVzaCAmJiB0aGlzLmZhY2V0RGF0YSkpIHtcclxuICAgICAgICAgIHRoaXMuZ2V0TGV2ZWxEZXRhaWxzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKiogKiBTY3JvbGwgZm9yIEZpeGluZyB0aGUgZGF0ZXMgYW5kIGhlYWRlciAqL1xyXG5cclxuICBzY3JvbGxpbmcgPSAoZWwpOiB2b2lkID0+IHtcclxuICAgIGNvbnN0IHNjcm9sbFRvcCA9IGVsLnNyY0VsZW1lbnQuc2Nyb2xsVG9wO1xyXG4gICAgaWYgKHNjcm9sbFRvcCA+PSAxMDApIHtcclxuICAgICAgdGhpcy5zY3JvbGxlZCA9IHRydWU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNjcm9sbGVkID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKiogQ0FMRU5EQVIgTUVUSE9EUyAqKi9cclxuXHJcbiAgLy8gYnVpbGQgY2FsZW5kYXIgYmFzZWQgb24gZ2l2ZW4gc3RhcnQgIGFuZCBlbmQgZGF0ZVxyXG4gIGJ1aWxkQ2FsZW5kYXIoc3RhcnREYXRlLCBlbmREYXRlKSB7XHJcbiAgICB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlID0gc3RhcnREYXRlO1xyXG4gICAgdGhpcy5jdXJyZW50Vmlld0VuZERhdGUgPSBlbmREYXRlO1xyXG4gICAgY29uc3Qgc3RhcnQgPSBzdGFydERhdGU7IC8vIHRoaXMubW9tZW50UHRyKHt9KTsgLy9nZXQgY3VycnJlbnQgZGF0ZVxyXG4gICAgY29uc3QgZW5kID0gZW5kRGF0ZTsgLy8gdGhpcy5tb21lbnRQdHIoe30pLmFkZCgzMCwgJ2RheXMnKTtcclxuICAgIGNvbnN0IHJhbmdlID0gdGhpcy5tb21lbnRQdHIucmFuZ2Uoc3RhcnQsIGVuZCk7XHJcbiAgICBjb25zdCBkYXlzID0gQXJyYXkuZnJvbShyYW5nZS5ieSgnZGF5cycpKTtcclxuICAgIHRoaXMuZGF0ZXMgPSBkYXlzO1xyXG4gIH1cclxuXHJcbiAgZ2V0RHluYW1pY1dvcmRTcGFjaW5nKCkge1xyXG4gICAgbGV0IHNwYWNpbmcgPSAnOHB4JztcclxuICAgIGlmICh0aGlzLmRpc3BsYXlUeXBlID09PSAnbW9udGgnKSB7XHJcbiAgICAgIGlmICghdGhpcy5zaG93V2Vla2VuZHMpIHtcclxuICAgICAgICBzcGFjaW5nID0gJzE2cHgnO1xyXG4gICAgICB9XHJcbiAgICAgIHNwYWNpbmcgPSAnMHB4JztcclxuICAgIH1cclxuICAgIHJldHVybiBzcGFjaW5nO1xyXG4gIH1cclxuXHJcbiAgZ2V0Tm9PZkRheXMoc2VsZWN0ZWRNb250aCkge1xyXG4gICAgbGV0IG5vT2ZEYXlzO1xyXG4gICAgaWYgKENhbGVuZGFyQ29uc3RhbnRzLmV2ZW5Nb250aC5pbmNsdWRlcyhzZWxlY3RlZE1vbnRoKSkge1xyXG4gICAgICBub09mRGF5cyA9IDMxO1xyXG4gICAgfSBlbHNlIGlmIChDYWxlbmRhckNvbnN0YW50cy5vZGRNb250aC5pbmNsdWRlcyhzZWxlY3RlZE1vbnRoKSkge1xyXG4gICAgICBub09mRGF5cyA9IDMwO1xyXG4gICAgfSBlbHNlIGlmIChDYWxlbmRhckNvbnN0YW50cy5sZWFwTW9udGguaW5jbHVkZXMoc2VsZWN0ZWRNb250aCkpIHtcclxuICAgICAgLyogY29uc3QgZGF0ZSA9IG5ldyBEYXRlKHRoaXMueWVhciwgMSwgMjkpO1xyXG4gICAgICByZXR1cm4gZGF0ZS5nZXRNb250aCgpID09PSAxOyAqL1xyXG4gICAgICBub09mRGF5cyA9IDI4O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG5vT2ZEYXlzO1xyXG4gIH1cclxuXHJcbiAgZ2V0VXNlcnNCeVJvbGUocmVzb3VyY2UsIGRlbGl2ZXJhYmxlRGF0YSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBsZXQgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7fTtcclxuICAgICAgY29uc3Qgcm9sZUlkID0gcmVzb3VyY2UuVEFTS19ST0xFX0lEID8gcmVzb3VyY2UuVEFTS19ST0xFX0lEIDogRGVsaXZlcmFibGVUeXBlcy5SRVZJRVcgPT09IGRlbGl2ZXJhYmxlRGF0YS5ERUxJVkVSQUJMRV9UWVBFID8gZGVsaXZlcmFibGVEYXRhLkRFTElWRVJBQkxFX0FQUFJPVkVSX1JPTEVfSUQgOiBkZWxpdmVyYWJsZURhdGEuREVMSVZFUkFCTEVfT1dORVJfUk9MRV9JRDtcclxuICAgICAgY29uc3Qgcm9sZURuID0gdGhpcy51dGlsU2VydmljZS5nZXRUZWFtUm9sZUROQnlSb2xlSWQocm9sZUlkKTtcclxuICAgICAgdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zID0gW107XHJcbiAgICAgIGdldFVzZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgIGFsbG9jYXRpb25UeXBlOiBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFLFxyXG4gICAgICAgIHJvbGVETjogcm9sZURuLlJPTEVfRE4sXHJcbiAgICAgICAgdGVhbUlEOiAnJyxcclxuICAgICAgICBpc0FwcHJvdmVUYXNrOiBmYWxzZSxcclxuICAgICAgICBpc1VwbG9hZFRhc2s6IHRydWVcclxuICAgICAgfTtcclxuXHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRVc2Vyc0ZvclRlYW0oZ2V0VXNlckJ5Um9sZVJlcXVlc3QpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudXNlcnMgJiYgcmVzcG9uc2UudXNlcnMudXNlcikge1xyXG4gICAgICAgICAgICBjb25zdCB1c2VyTGlzdCA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICd1c2VyJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJzID0gW107XHJcbiAgICAgICAgICAgIGlmICh1c2VyTGlzdCAmJiB1c2VyTGlzdC5sZW5ndGggJiYgdXNlckxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgIHVzZXJMaXN0LmZvckVhY2godXNlciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBmaWVsZE9iaiA9IHVzZXJzLmZpbmQoZSA9PiBlLnZhbHVlID09PSB1c2VyLmRuKTtcclxuICAgICAgICAgICAgICAgIGlmICghZmllbGRPYmopIHtcclxuICAgICAgICAgICAgICAgICAgdXNlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogdXNlci5kbixcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdXNlci5jbixcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogdXNlci5uYW1lXHJcbiAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucyA9IHVzZXJzO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRVc2VycyhyZXNvdXJjZSwgZGVsaXZlcmFibGVEYXRhKSB7XHJcbiAgICBjb25zb2xlLmxvZyhkZWxpdmVyYWJsZURhdGEpO1xyXG4gICAgdGhpcy5nZXRVc2Vyc0J5Um9sZShyZXNvdXJjZSwgZGVsaXZlcmFibGVEYXRhKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG5cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyogdXBkYXRlVGFza0RlbGl2ZXJhYmxlKGRhdGEsIHVzZXJJZCkge1xyXG4gICAgbGV0IFRhc2tPYmplY3Q7XHJcbiAgICBUYXNrT2JqZWN0ID0ge1xyXG4gICAgICAnQnVsa09wZXJhdGlvbic6IGZhbHNlLFxyXG4gICAgICAnUlBPT3duZXJVc2VyJzoge1xyXG4gICAgICAgICdVc2VyJzoge1xyXG4gICAgICAgICAgJ3VzZXJJRCc6IHVzZXJJZFxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgJ1Rhc2tJZCc6IHtcclxuICAgICAgICAnSWQnOiBkYXRhLlRBU0tfSVRFTV9JRC5zcGxpdCgnLicpWzFdXHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGNvbnN0IHJlcXVlc3RPYmogPSB7XHJcbiAgICAgIFRhc2tPYmplY3Q6IFRhc2tPYmplY3QsXHJcbiAgICAgIERlbGl2ZXJhYmxlUmV2aWV3ZXJzOiB7XHJcbiAgICAgICAgRGVsaXZlcmFibGVSZXZpZXdlcjogJydcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgdGhpcy50YXNrU2VydmljZS51cGRhdGVUYXNrKHJlcXVlc3RPYmopLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnMjAwJykge1xyXG4gICAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdUYXNrIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzUwMCcgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3JcclxuICAgICAgICAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvckNvZGUgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JNZXNzYWdlKSB7XHJcbiAgICAgICAgdGhpcy5yZWxvYWQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yTWVzc2FnZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5yZWxvYWQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyBvbiB3aGlsZSB1cGRhdGluZyB0YXNrJyk7XHJcbiAgICAgIH1cclxuICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgdGhpcy5yZWxvYWQodHJ1ZSk7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgdXBkYXRpbmcgdGFzaycpO1xyXG4gICAgfSk7XHJcbiAgfSAqL1xyXG4gIC8vIE1QTS1WMy0yMjE3XHJcbiAgdXBkYXRlVGFza0RlbGl2ZXJhYmxlKGRhdGEsIHVzZXJJZCkge1xyXG4gICAgdGhpcy50YXNrU2VydmljZS5hc3NpZ25EZWxpdmVyYWJsZShkYXRhLklELCB1c2VySWQpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnMjAwJykge1xyXG4gICAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdEZWxpdmVyYWJsZSB1cGRhdGVkIHN1Y2Nlc3NmdWxseScpO1xyXG4gICAgICAgIC8vICB0aGlzLmFkZGVkUmVzb3VyY2VIYW5kbGVyLm5leHQodXNlcklkKTtcclxuICAgICAgICAvLyAgIHRoaXMuc3luY2hyb25pemVUYXNrRmlsdGVyc0NvbXBvbmVudC5nZXREZWxpdmVyYWJsZSh1c2VySWQsZmFsc2UpO1xyXG4gICAgICB9IGVsc2UgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgPT09ICc1MDAnKSB7XHJcbiAgICAgICAgdGhpcy5yZWxvYWQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyBvbiB3aGlsZSB1cGRhdGluZyBkZWxpdmVyYWJsZScpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgdGhpcy5yZWxvYWQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyBvbiB3aGlsZSB1cGRhdGluZyBkZWxpdmVyYWJsZScpO1xyXG4gICAgICB9XHJcbiAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIHVwZGF0aW5nIGRlbGl2ZXJhYmxlJyk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZURlbGl2ZXJhYmxlKGRhdGEsIHVzZXJJZCkge1xyXG4gICAgbGV0IGRlbGl2ZXJhYmxlT2JqZWN0O1xyXG4gICAgZGVsaXZlcmFibGVPYmplY3QgPSB7XHJcbiAgICAgICdCdWxrT3BlcmF0aW9uJzogZmFsc2UsXHJcbiAgICAgICdSUE9EZWxpdmVyYWJsZU93bmVySUQnOiB7XHJcbiAgICAgICAgJ0lkZW50aXR5SUQnOiB7XHJcbiAgICAgICAgICAndXNlcklEJzogdXNlcklkXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICAnRGVsaXZlcmFibGVJZCc6IHtcclxuICAgICAgICAnSWQnOiBkYXRhLklEXHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGNvbnN0IHJlcXVlc3RPYmogPSB7XHJcbiAgICAgIERlbGl2ZXJhYmxlT2JqZWN0OiBkZWxpdmVyYWJsZU9iamVjdCxcclxuICAgICAgRGVsaXZlcmFibGVSZXZpZXdlcnM6IHtcclxuICAgICAgICBEZWxpdmVyYWJsZVJldmlld2VyOiAnJ1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgdGhpcy5kZWxpdmVyYWJsZVNlcnZpY2UudXBkYXRlRGVsaXZlcmFibGUocmVxdWVzdE9iailcclxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzIwMCcgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YVxyXG4gICAgICAgICAgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5EZWxpdmVyYWJsZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkRlbGl2ZXJhYmxlWydEZWxpdmVyYWJsZS1pZCddICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuRGVsaXZlcmFibGVbJ0RlbGl2ZXJhYmxlLWlkJ10uSWQpIHtcclxuICAgICAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ0RlbGl2ZXJhYmxlIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgICAgICAgICAvLyAgdGhpcy5hZGRlZFJlc291cmNlSGFuZGxlci5uZXh0KHVzZXJJZCk7XHJcbiAgICAgICAgICAvLyAgdGhpcy5zeW5jaHJvbml6ZVRhc2tGaWx0ZXJzQ29tcG9uZW50LmdldERlbGl2ZXJhYmxlKHVzZXJJZCwgZmFsc2UpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzUwMCcgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3JcclxuICAgICAgICAgICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yQ29kZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvck1lc3NhZ2UpIHtcclxuICAgICAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yTWVzc2FnZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICB0aGlzLnJlbG9hZCh0cnVlKTtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgdXBkYXRpbmcgZGVsaXZlcmFibGUnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgdXBkYXRpbmcgZGVsaXZlcmFibGUnKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGUoZGF0YSwgdXNlcklkKSB7XHJcbiAgICBjb25zb2xlLmxvZyh1c2VySWQpO1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgIGlmIChkYXRhLkRFTElWRVJBQkxFX1RZUEUgPT09IERlbGl2ZXJhYmxlVHlwZXMuVVBMT0FEKSB7XHJcbiAgICAgIHRoaXMudXBkYXRlRGVsaXZlcmFibGUoZGF0YSwgdXNlcklkKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudXBkYXRlVGFza0RlbGl2ZXJhYmxlKGRhdGEsIHVzZXJJZCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBkZWxpdmVyYWJsZURhdGFIYW5kbGVyKGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCk7XHJcbiAgICB0aGlzLmRlbGl2ZXJhYmxlRGF0YXMgPSBldmVudDtcclxuICB9XHJcblxyXG4gIG9uQ2hpY2tsZXRSZW1vdmVGaWx0ZXIoZmlsdGVyKSB7XHJcbiAgICBjb25zb2xlLmxvZyhmaWx0ZXIpO1xyXG4gICAgdGhpcy5yZW1vdmVkRmlsdGVyID0gZmlsdGVyO1xyXG4gIH1cclxuXHJcbiAgLyogcmVzb3VyY2VBbGxvY2F0aW9uSGFuZGxlcihhbGxvY2F0aW9uKSB7XHJcbiAgICBjb25zb2xlLmxvZyhhbGxvY2F0aW9uKTtcclxuICAgIHRoaXMuYWxsb2NhdGlvbiA9IGFsbG9jYXRpb247XHJcbiAgfSAqL1xyXG5cclxuICBnZXREYXRlRGlmZmVyZW5jZShzdGFydERhdGUsIGVuZERhdGUpIHtcclxuICAgIGxldCBzZDogYW55ID0gKG5ldyBEYXRlKHN0YXJ0RGF0ZSkpO1xyXG4gICAgbGV0IGVkOiBhbnkgPSBuZXcgRGF0ZShlbmREYXRlKTtcclxuICAgIHJldHVybiAoTWF0aC5mbG9vcihlZCAtIHNkKSAvICgxMDAwICogNjAgKiA2MCAqIDI0KSkgKyAxO1xyXG4gIH1cclxuXHJcbiAgLy8gbmdPbkNoYW5nZXMoc2ltcGxlQ2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gIC8vICAgdmFyIGFuZ3VsYXI6IGFueTtcclxuICAvLyAgIGNvbnNvbGUubG9nKFwiQ29udGVudC13aWR0aDogXCJcclxuICAvLyAgICAgKyAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjYWxlbmRhckNlbGxcIikuc2Nyb2xsV2lkdGgpXHJcbiAgLy8gICAgICsgXCJweDxicj5cIik7XHJcbiAgLy8gICBjb25zb2xlLmxvZyhcIkNvbnRlbnQtd2lkdGgxOiBcIlxyXG4gIC8vICAgICArIGFuZ3VsYXIuZWxlbWVudChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNhbGVuZGFyQ2VsbFwiKS5jbGllbnRIZWlnaHQpXHJcbiAgLy8gICAgICsgXCJweDxicj5cIik7XHJcbiAgLy8gfVxyXG5cclxuICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IHVzZS1saWZlY3ljbGUtaW50ZXJmYWNlXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNvbXBvbmVudFJlbmRlcmluZyA9IHRydWU7XHJcbiAgICB0aGlzLmFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICB0aGlzLmVuYWJsZUNhbXBhaWduID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRU5BQkxFX0NBTVBBSUdOXSk7XHJcbiAgICB0aGlzLmdyb3VwQnlGaWx0ZXJzID0gdGhpcy5lbmFibGVDYW1wYWlnbiA/IEdST1VQX0JZX0FMTF9GSUxURVJTIDogR1JPVVBfQllfRklMVEVSUztcclxuXHJcbiAgICB0aGlzLnRvcCA9IHRoaXMudXRpbFNlcnZpY2UuY29udmVydFN0cmluZ1RvTnVtYmVyKHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfUEFHSU5BVElPTl0pO1xyXG4gICAgdGhpcy5wYWdlU2l6ZSA9IHRoaXMudXRpbFNlcnZpY2UuY29udmVydFN0cmluZ1RvTnVtYmVyKHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfUEFHSU5BVElPTl0pO1xyXG5cclxuICAgIGNvbnN0IGN1cnJlbnRGdWxsRGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICBjb25zdCBjdXJyZW50WWVhciA9IGN1cnJlbnRGdWxsRGF0ZS5nZXRGdWxsWWVhcigpO1xyXG4gICAgY29uc3QgY3VycmVudE1vbnRoID0gY3VycmVudEZ1bGxEYXRlLmdldE1vbnRoKCkgKyAxO1xyXG4gICAgY29uc3QgY3VycmVudERhdGUgPSBjdXJyZW50RnVsbERhdGUuZ2V0RGF0ZSgpO1xyXG4gICAgdGhpcy5jdXJyZW50RGF0ZSA9IGN1cnJlbnRNb250aCArICcvJyArIGN1cnJlbnREYXRlICsgJy8nICsgY3VycmVudFllYXI7XHJcblxyXG4gICAgdGhpcy5zZWxlY3RlZE1vbnRoID0gQ2FsZW5kYXJDb25zdGFudHMubW9udGhzW2N1cnJlbnRGdWxsRGF0ZS5nZXRNb250aCgpXTtcclxuICAgIHRoaXMueWVhciA9IGN1cnJlbnRZZWFyO1xyXG4gICAgbGV0IG5vT2ZEYXlzID0gdGhpcy5nZXROb09mRGF5cyh0aGlzLnNlbGVjdGVkTW9udGgpO1xyXG4gICAgLy8gdGhpcy5idWlsZENhbGVuZGFyKHRoaXMubW9tZW50UHRyKHt9KSwgdGhpcy5tb21lbnRQdHIoe30pLmFkZCgyOSwgJ2RheXMnKSk7XHJcbiAgICAvKiB0aGlzLmJ1aWxkQ2FsZW5kYXIoY3VycmVudE1vbnRoICsgJy8wMS8nICsgY3VycmVudFllYXIsIHRoaXMubW9tZW50UHRyKHt9KS5hZGQoMjksICdkYXlzJykpOyAqL1xyXG4gICAgdGhpcy5idWlsZENhbGVuZGFyKGN1cnJlbnRNb250aCArICcvMDEvJyArIGN1cnJlbnRZZWFyLCBjdXJyZW50TW9udGggKyAnLycgKyBub09mRGF5cyArICcvJyArIGN1cnJlbnRZZWFyKTtcclxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLnNjcm9sbGluZywgdHJ1ZSk7XHJcblxyXG4gICAgbGV0IGluaXRpYWxMb2FkID0gdHJ1ZTtcclxuICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKFxyXG4gICAgICB0aGlzLnNlYXJjaERhdGFTZXJ2aWNlLnNlYXJjaERhdGEuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIHRoaXMuc2VhcmNoQ29uZGl0aW9ucyA9IChkYXRhICYmIEFycmF5LmlzQXJyYXkoZGF0YSkgPyB0aGlzLm90bW1TZXJ2aWNlLmFkZFJlbGF0aW9uYWxPcGVyYXRvcihkYXRhKSA6IFtdKTtcclxuICAgICAgICBpZiAoKHRoaXMuc2VhcmNoTmFtZSB8fCB0aGlzLnNhdmVkU2VhcmNoTmFtZSB8fCB0aGlzLmFkdlNlYXJjaERhdGEgfHwgdGhpcy5mYWNldERhdGEpICYmIHRoaXMuaXNEYXRhUmVmcmVzaCkge1xyXG4gICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5mYWNldERhdGEgfHwgKCh0aGlzLmZhY2V0RGF0YSAmJiB0aGlzLmZhY2V0UmVzdHJpY3Rpb25MaXN0KSAmJiAodGhpcy5hZHZTZWFyY2hEYXRhIHx8IHRoaXMuc2F2ZWRTZWFyY2hOYW1lKSkpIHtcclxuICAgICAgICAgICAgICB0aGlzLmdldExldmVsRGV0YWlscygpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRoaXMudmFsaWRhdGVGYWNldCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc2VhcmNoTmFtZSA9PT0gbnVsbCAmJiB0aGlzLnNhdmVkU2VhcmNoTmFtZSA9PT0gbnVsbCAmJiB0aGlzLmFkdlNlYXJjaERhdGEgPT09IG51bGwgJiYgdGhpcy5mYWNldERhdGEgPT09IG51bGwgJiYgdGhpcy5pc0RhdGFSZWZyZXNoKSB7XHJcbiAgICAgICAgICB0aGlzLmdldExldmVsRGV0YWlscygpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuaXNEYXRhUmVmcmVzaCkge1xyXG4gICAgICAgICAgaWYgKCFpbml0aWFsTG9hZCkge1xyXG4gICAgICAgICAgICB0aGlzLmlzUmVzZXQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmdldExldmVsRGV0YWlscygpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpbml0aWFsTG9hZCA9IGZhbHNlO1xyXG4gICAgICB9KVxyXG4gICAgKTtcclxuXHJcbiAgICBpbml0aWFsTG9hZCA9IHRydWU7XHJcbiAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChcclxuICAgICAgdGhpcy5mYWNldENoYW5nZUV2ZW50U2VydmljZS5vbkZhY2V0Q29uZGlkdGlvbkNoYW5nZS5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgdGhpcy5mYWNldFJlc3RyaWN0aW9uTGlzdCA9IGRhdGE7XHJcbiAgICAgICAgaWYgKHRoaXMuZmFjZXREYXRhICYmIHRoaXMuaXNEYXRhUmVmcmVzaCkge1xyXG4gICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5mYWNldF9jb25kaXRpb24gJiYgZGF0YS5mYWNldF9jb25kaXRpb24ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBpZiAoISh0aGlzLmFkdlNlYXJjaERhdGEgfHwgdGhpcy5zYXZlZFNlYXJjaE5hbWUpIHx8IHRoaXMudmFsaWRhdGVGYWNldCkge1xyXG4gICAgICAgICAgICAgIHRoaXMuZ2V0TGV2ZWxEZXRhaWxzKCk7XHJcbiAgICAgICAgICAgICAgdGhpcy52YWxpZGF0ZUZhY2V0ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuZmFjZXREYXRhID09PSBudWxsICYmIHRoaXMuaXNEYXRhUmVmcmVzaCkge1xyXG4gICAgICAgICAgdGhpcy5nZXRMZXZlbERldGFpbHMoKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLmlzRGF0YVJlZnJlc2gpIHtcclxuICAgICAgICAgIGlmICghaW5pdGlhbExvYWQpIHtcclxuICAgICAgICAgICAgdGhpcy5wcmV2aW91c1JlcXVlc3QgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmlzUmVzZXQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmdldExldmVsRGV0YWlscygpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpbml0aWFsTG9hZCA9IGZhbHNlO1xyXG4gICAgICB9KVxyXG4gICAgKTtcclxuICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKFxyXG4gICAgICB0aGlzLmNvbXBvbmVudFBhcmFtcy5zdWJzY3JpYmUocm91dGVEYXRhID0+IHtcclxuICAgICAgICBpZiAocm91dGVEYXRhLmVtYWlsTGlua0lkKSB7XHJcbiAgICAgICAgICB0aGlzLmVtYWlsTGlua0lkID0gcm91dGVEYXRhLmVtYWlsTGlua0lkO1xyXG4gICAgICAgICAgdGhpcy5pbml0YWxpemVDb21wb25lbnQocm91dGVEYXRhKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5pbml0YWxpemVDb21wb25lbnQocm91dGVEYXRhKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG5cclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgdGhpcy5zZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2UudXBkYXRlKG51bGwpO1xyXG4gICAgdGhpcy5zdWJzY3JpcHRpb25zLmZvckVhY2goc3Vic2NyaXB0aW9uID0+IHN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpKTtcclxuICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLnNjcm9sbGluZywgdHJ1ZSk7XHJcbiAgfVxyXG5cclxuICAvKiBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICB2YXIgd2lkdGggPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2FsZW5kYXJDZWxsJykuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkud2lkdGg7XHJcbiAgICBhbGVydCh3aWR0aCk7XHJcbiAgfSAqL1xyXG5cclxufVxyXG4iXX0=