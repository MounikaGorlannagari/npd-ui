import { __decorate, __read, __values } from "tslib";
import { Component, OnInit, ElementRef, ViewChild, ViewChildren, QueryList, Renderer2, Input, Output, EventEmitter, SimpleChanges, ChangeDetectorRef, HostListener } from '@angular/core';
import { extendMoment } from 'moment-range';
import * as Moment from 'moment';
import { CalendarConstants } from '../shared/constants/Calendar.constants';
import { MediaMatcher } from '@angular/cdk/layout';
var moment = extendMoment(Moment);
var ResourceManagementResourceAllocationComponent = /** @class */ (function () {
    function ResourceManagementResourceAllocationComponent(renderer, media, changeDetectorRef) {
        var _this = this;
        this.renderer = renderer;
        this.media = media;
        this.changeDetectorRef = changeDetectorRef;
        this.removed = new EventEmitter();
        this.scrolled = false;
        this.dates = [];
        this.viewTypeCellSize = 150; //35;
        this.momentPtr = moment;
        this.sidebarStyle = {};
        this.Object = Object;
        this.Math = Math;
        /** * Scroll for Fixing the dates and header */
        this.scrolling = function (el) {
            var scrollTop = el.srcElement.scrollTop;
            if (scrollTop >= 100) {
                _this.scrolled = true;
            }
            else {
                _this.scrolled = false;
            }
        };
        //191
        this.getHeightForScreen();
    }
    ResourceManagementResourceAllocationComponent.prototype.onResize = function (event) {
        this.getHeightForScreen();
    };
    //191
    ResourceManagementResourceAllocationComponent.prototype.getHeightForScreen = function () {
        var width = 1536;
        var height = 26;
        var max_height = 39;
        this.screenWidth = window.innerWidth;
        var diff = Math.floor((this.screenWidth - width) / 171);
        this.height = height + (diff * 2) <= max_height ? height + (diff * 2) : 38;
    };
    // to remove the filter
    ResourceManagementResourceAllocationComponent.prototype.removeFilter = function (filter) {
        var _this = this;
        if (filter.ROLE_NAME || filter.NAME) {
            this.selectedItems.forEach(function (item, index) {
                if ((item.ROLE_NAME !== undefined && filter.ROLE_NAME !== undefined && item.ROLE_NAME === filter.ROLE_NAME) || (item.NAME !== undefined && filter.NAME !== undefined && item.NAME === filter.NAME)) {
                    _this.selectedItems.splice(index, 1);
                    _this.removed.next(filter.NAME !== undefined && filter['MPM_Teams-id'] !== undefined && filter['MPM_Teams-id'].Id !== undefined ? ['TEAM'] : ['ROLE']);
                    // this.removed.next(filter.ROLE_NAME !== undefined ? ['ROLE'] : ['TEAM']);
                }
            });
        }
        else {
            this.selectedItems.forEach(function (item, index) {
                if ((item.FullName !== undefined && filter.FullName !== undefined && item.FullName === filter.FullName) || (item.name !== undefined && filter.name !== undefined && item.name === filter.name)) {
                    _this.selectedItems.splice(index, 1);
                }
            });
        }
        if (filter.FullName || filter.name) {
            console.log("###");
            console.log(filter);
            this.removed.next([filter]);
        }
    };
    /** CALENDAR METHODS **/
    // build calendar based on given start  and end date
    ResourceManagementResourceAllocationComponent.prototype.buildCalendar = function (startDate, endDate) {
        this.currentViewStartDate = startDate;
        this.currentViewEndDate = endDate;
        var start = startDate;
        var end = endDate;
        var range = this.momentPtr.range(start, end);
        var days = Array.from(range.by('days'));
        this.dates = days;
    };
    ResourceManagementResourceAllocationComponent.prototype.getNoOfDays = function (selectedMonth) {
        var noOfDays;
        if (CalendarConstants.evenMonth.includes(selectedMonth)) {
            noOfDays = 31;
        }
        else if (CalendarConstants.oddMonth.includes(selectedMonth)) {
            noOfDays = 30;
        }
        else if (CalendarConstants.leapMonth.includes(selectedMonth)) {
            /* const date = new Date(this.year, 1, 29);
            return date.getMonth() === 1; */
            noOfDays = 28;
        }
        return noOfDays;
    };
    ResourceManagementResourceAllocationComponent.prototype.getDateDifference = function (startDate, endDate) {
        var sd = (new Date(startDate));
        var ed = new Date(endDate);
        return (Math.floor(ed - sd) / (1000 * 60 * 60 * 24)) + 1;
    };
    ResourceManagementResourceAllocationComponent.prototype.ngOnChanges = function (changes) {
        var e_1, _a, _b;
        if (changes && changes.selectedMonth && !changes.selectedMonth.firstChange) {
            for (var i = 0; i < CalendarConstants.months.length; i++) {
                if (CalendarConstants.months[i] === changes.selectedMonth.currentValue) {
                    this.buildCalendar(i + 1 + '/01/' + this.selectedYear, i + 1 + '/' + this.getNoOfDays(changes.selectedMonth.currentValue) + '/' + this.selectedYear);
                    /*  this.currentViewStartDate = i + 1 + '/01/' + this.selectedYear;
                       this.currentViewEndDate = i + 1 + '/' + this.getNoOfDays(changes.selectedMonth.currentValue) + '/' + this.selectedYear; */
                }
            }
        }
        if (changes && changes.data && changes.data.currentValue) {
            this.datas = Object.keys(changes.data.currentValue).length - 1;
            var k = void 0, v = void 0, startdate = void 0, enddate = void 0;
            try {
                // to compare the date and update the overlap of date
                for (var _c = __values(Object.entries(changes.data.currentValue)), _d = _c.next(); !_d.done; _d = _c.next()) {
                    _b = __read(_d.value, 2), k = _b[0], v = _b[1];
                    if (typeof (v) !== 'string') {
                        for (var i = 0; i < v.length; i++) {
                            for (var j = i + 1; j <= v.length - 1; j++) {
                                if (v[j] && v[j].deliverableDetail && v[j].deliverableDetail.data) {
                                    if (v[i].deliverableDetail.data.DELIVERABLE_DUE_DATE >= v[j].deliverableDetail.data.DELIVERABLE_DUE_DATE) {
                                        enddate = v[j].deliverableDetail.data.DELIVERABLE_DUE_DATE;
                                        if (v[j].deliverableDetail.data.DELIVERABLE_START_DATE <= v[i].deliverableDetail.data.DELIVERABLE_START_DATE) {
                                            startdate = v[i].deliverableDetail.data.DELIVERABLE_START_DATE;
                                        }
                                        else if (v[i].deliverableDetail.data.DELIVERABLE_START_DATE <= v[j].deliverableDetail.data.DELIVERABLE_START_DATE) {
                                            startdate = v[j].deliverableDetail.data.DELIVERABLE_START_DATE;
                                        }
                                    }
                                    else {
                                        enddate = v[i].deliverableDetail.data.DELIVERABLE_DUE_DATE;
                                        if (v[i].deliverableDetail.data.DELIVERABLE_START_DATE <= v[j].deliverableDetail.data.DELIVERABLE_START_DATE) {
                                            startdate = v[j].deliverableDetail.data.DELIVERABLE_START_DATE;
                                        }
                                        else if (v[j].deliverableDetail.data.DELIVERABLE_START_DATE <= v[i].deliverableDetail.data.DELIVERABLE_START_DATE) {
                                            startdate = v[i].deliverableDetail.data.DELIVERABLE_START_DATE;
                                        }
                                    }
                                    var sd = parseInt(startdate.split('T')[0].split('-')[2] < 10 ? startdate.split('T')[0].split('-')[2].split('0')[1] : startdate.split('T')[0].split('-')[2]);
                                    var ed = parseInt(enddate.split('T')[0].split('-')[2] < 10 ? enddate.split('T')[0].split('-')[2].split('0')[1] : enddate.split('T')[0].split('-')[2]);
                                    for (var m = sd - 1; m < ed; m++) {
                                        console.log(changes.data.currentValue[k].date[m]);
                                        changes.data.currentValue[k].date[m].isOverlap = 'true';
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
    };
    ResourceManagementResourceAllocationComponent.prototype.ngOnInit = function () {
        var currentFullDate = new Date();
        var currentYear = currentFullDate.getFullYear();
        var currentMonth = currentFullDate.getMonth() + 1;
        var currentDate = currentFullDate.getDate();
        this.currentDate = currentMonth + '/' + currentDate + '/' + currentYear;
        var noOfDays = this.getNoOfDays(this.selectedMonth);
        this.buildCalendar(currentMonth + '/01/' + currentYear, currentMonth + '/' + noOfDays + '/' + currentYear);
        window.addEventListener('scroll', this.scrolling, true);
    };
    ResourceManagementResourceAllocationComponent.ctorParameters = function () { return [
        { type: Renderer2 },
        { type: MediaMatcher },
        { type: ChangeDetectorRef }
    ]; };
    __decorate([
        Input()
    ], ResourceManagementResourceAllocationComponent.prototype, "selectedMonth", void 0);
    __decorate([
        Input()
    ], ResourceManagementResourceAllocationComponent.prototype, "data", void 0);
    __decorate([
        Input()
    ], ResourceManagementResourceAllocationComponent.prototype, "users", void 0);
    __decorate([
        Input()
    ], ResourceManagementResourceAllocationComponent.prototype, "selectedItems", void 0);
    __decorate([
        Input()
    ], ResourceManagementResourceAllocationComponent.prototype, "allocation", void 0);
    __decorate([
        Input()
    ], ResourceManagementResourceAllocationComponent.prototype, "selectedYear", void 0);
    __decorate([
        Output()
    ], ResourceManagementResourceAllocationComponent.prototype, "removed", void 0);
    __decorate([
        ViewChild('calendarView')
    ], ResourceManagementResourceAllocationComponent.prototype, "calendarView", void 0);
    __decorate([
        ViewChildren('dateHeader')
    ], ResourceManagementResourceAllocationComponent.prototype, "calendarHeaders", void 0);
    __decorate([
        ViewChildren('panelContainer')
    ], ResourceManagementResourceAllocationComponent.prototype, "panelContainers", void 0);
    __decorate([
        ViewChildren('calendarCell')
    ], ResourceManagementResourceAllocationComponent.prototype, "calendarCells", void 0);
    __decorate([
        HostListener('window:resize', ['$event'])
    ], ResourceManagementResourceAllocationComponent.prototype, "onResize", null);
    ResourceManagementResourceAllocationComponent = __decorate([
        Component({
            selector: 'mpm-resource-management-resource-allocation',
            template: "<p>resource-management-resource-allocation works!</p>\r\n",
            styles: [""]
        })
    ], ResourceManagementResourceAllocationComponent);
    return ResourceManagementResourceAllocationComponent;
}());
export { ResourceManagementResourceAllocationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb3VyY2UtbWFuYWdlbWVudC1yZXNvdXJjZS1hbGxvY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Jlc291cmNlLW1hbmFnZW1lbnQvcmVzb3VyY2UtbWFuYWdlbWVudC1yZXNvdXJjZS1hbGxvY2F0aW9uL3Jlc291cmNlLW1hbmFnZW1lbnQtcmVzb3VyY2UtYWxsb2NhdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLGlCQUFpQixFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxTCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVDLE9BQU8sS0FBSyxNQUFNLE1BQU0sUUFBUSxDQUFDO0FBQ2pDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVuRCxJQUFNLE1BQU0sR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7QUFPcEM7SUEyQ0UsdURBQ1MsUUFBbUIsRUFDbkIsS0FBbUIsRUFDbkIsaUJBQW9DO1FBSDdDLGlCQU9DO1FBTlEsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUNuQixVQUFLLEdBQUwsS0FBSyxDQUFjO1FBQ25CLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFwQ25DLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBRTlDLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLHFCQUFnQixHQUFHLEdBQUcsQ0FBQyxDQUFBLEtBQUs7UUFDNUIsY0FBUyxHQUFHLE1BQU0sQ0FBQztRQWtCbkIsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFHbEIsV0FBTSxHQUFHLE1BQU0sQ0FBQztRQUVoQixTQUFJLEdBQUcsSUFBSSxDQUFDO1FBc0RaLCtDQUErQztRQUMvQyxjQUFTLEdBQUcsVUFBQyxFQUFFO1lBQ2IsSUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7WUFDMUMsSUFBSSxTQUFTLElBQUksR0FBRyxFQUFFO2dCQUNwQixLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN0QjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQTtRQXBEQyxLQUFLO1FBQ0wsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUlELGdFQUFRLEdBQVIsVUFBUyxLQUFLO1FBQ1osSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELEtBQUs7SUFDTCwwRUFBa0IsR0FBbEI7UUFDRSxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBTSxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQU0sVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDckMsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLEdBQUcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUM3RSxDQUFDO0lBRUQsdUJBQXVCO0lBQ3ZCLG9FQUFZLEdBQVosVUFBYSxNQUFNO1FBQW5CLGlCQXFCQztRQXBCQyxJQUFJLE1BQU0sQ0FBQyxTQUFTLElBQUksTUFBTSxDQUFDLElBQUksRUFBRTtZQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO2dCQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLElBQUksTUFBTSxDQUFDLFNBQVMsS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsSUFBSSxLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbE0sS0FBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNwQyxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssU0FBUyxJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ3RKLDJFQUEyRTtpQkFDNUU7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO2dCQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxTQUFTLElBQUksTUFBTSxDQUFDLFFBQVEsS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsSUFBSSxLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDOUwsS0FBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUNyQztZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLE1BQU0sQ0FBQyxRQUFRLElBQUksTUFBTSxDQUFDLElBQUksRUFBRTtZQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1lBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQzdCO0lBQ0gsQ0FBQztJQVlELHdCQUF3QjtJQUV4QixvREFBb0Q7SUFDcEQscUVBQWEsR0FBYixVQUFjLFNBQVMsRUFBRSxPQUFPO1FBQzlCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxTQUFTLENBQUM7UUFDdEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQztRQUNsQyxJQUFNLEtBQUssR0FBRyxTQUFTLENBQUM7UUFDeEIsSUFBTSxHQUFHLEdBQUcsT0FBTyxDQUFDO1FBQ3BCLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMvQyxJQUFNLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztJQUNwQixDQUFDO0lBRUQsbUVBQVcsR0FBWCxVQUFZLGFBQWE7UUFDdkIsSUFBSSxRQUFRLENBQUM7UUFDYixJQUFJLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDdkQsUUFBUSxHQUFHLEVBQUUsQ0FBQztTQUNmO2FBQU0sSUFBSSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxFQUFFO1lBQzdELFFBQVEsR0FBRyxFQUFFLENBQUM7U0FDZjthQUFNLElBQUksaUJBQWlCLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUM5RDs0Q0FDZ0M7WUFDaEMsUUFBUSxHQUFHLEVBQUUsQ0FBQztTQUNmO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDbEIsQ0FBQztJQUVELHlFQUFpQixHQUFqQixVQUFrQixTQUFTLEVBQUUsT0FBTztRQUNsQyxJQUFJLEVBQUUsR0FBUSxDQUFDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDcEMsSUFBSSxFQUFFLEdBQVEsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDaEMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVELG1FQUFXLEdBQVgsVUFBWSxPQUFzQjs7UUFDaEMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLGFBQWEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFO1lBQzFFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN4RCxJQUFJLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxPQUFPLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRTtvQkFDdEUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUNySjtpSkFDNkg7aUJBQzlIO2FBQ0Y7U0FDRjtRQUVELElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDeEQsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsU0FBSyxFQUFFLENBQUMsU0FBSyxFQUFFLFNBQVMsU0FBQSxFQUFFLE9BQU8sU0FBQSxDQUFDOztnQkFDdkMscURBQXFEO2dCQUNyRCxLQUFlLElBQUEsS0FBQSxTQUFBLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQSxnQkFBQSw0QkFBRTs4Q0FBcEQsU0FBQyxFQUFFLFNBQUM7b0JBQ1IsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxFQUFFO3dCQUMzQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDakMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQ0FDMUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUU7b0NBQ2pFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxvQkFBb0IsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFO3dDQUN4RyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQTt3Q0FDMUQsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHNCQUFzQixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7NENBQzVHLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFBO3lDQUMvRDs2Q0FBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTs0Q0FDbkgsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUE7eUNBQy9EO3FDQUNGO3lDQUFNO3dDQUNMLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFBO3dDQUMxRCxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTs0Q0FDNUcsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUE7eUNBQy9EOzZDQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFOzRDQUNuSCxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQTt5Q0FDL0Q7cUNBQ0Y7b0NBQ0QsSUFBSSxFQUFFLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO29DQUMzSixJQUFJLEVBQUUsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7b0NBQ3JKLEtBQUssSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO3dDQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO3dDQUNqRCxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQztxQ0FDekQ7aUNBQ0Y7NkJBQ0Y7eUJBQ0Y7cUJBQ0Y7aUJBQ0Y7Ozs7Ozs7OztTQUNGO0lBQ0gsQ0FBQztJQUVELGdFQUFRLEdBQVI7UUFDRSxJQUFNLGVBQWUsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQ25DLElBQU0sV0FBVyxHQUFHLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNsRCxJQUFNLFlBQVksR0FBRyxlQUFlLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3BELElBQU0sV0FBVyxHQUFHLGVBQWUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUM5QyxJQUFJLENBQUMsV0FBVyxHQUFHLFlBQVksR0FBRyxHQUFHLEdBQUcsV0FBVyxHQUFHLEdBQUcsR0FBRyxXQUFXLENBQUM7UUFDeEUsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEdBQUcsTUFBTSxHQUFHLFdBQVcsRUFBRSxZQUFZLEdBQUcsR0FBRyxHQUFHLFFBQVEsR0FBRyxHQUFHLEdBQUcsV0FBVyxDQUFDLENBQUM7UUFDM0csTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzFELENBQUM7O2dCQXJKa0IsU0FBUztnQkFDWixZQUFZO2dCQUNBLGlCQUFpQjs7SUE1Q3BDO1FBQVIsS0FBSyxFQUFFO3dGQUFvQjtJQUNuQjtRQUFSLEtBQUssRUFBRTsrRUFBTTtJQUVMO1FBQVIsS0FBSyxFQUFFO2dGQUFPO0lBQ047UUFBUixLQUFLLEVBQUU7d0ZBQWU7SUFDZDtRQUFSLEtBQUssRUFBRTtxRkFBWTtJQUNYO1FBQVIsS0FBSyxFQUFFO3VGQUFjO0lBRVo7UUFBVCxNQUFNLEVBQUU7a0ZBQXFDO0lBVTlDO1FBREMsU0FBUyxDQUFDLGNBQWMsQ0FBQzt1RkFDRDtJQUd6QjtRQURDLFlBQVksQ0FBQyxZQUFZLENBQUM7MEZBQ1k7SUFHdkM7UUFEQyxZQUFZLENBQUMsZ0JBQWdCLENBQUM7MEZBQ1E7SUFHdkM7UUFEQyxZQUFZLENBQUMsY0FBYyxDQUFDO3dGQUNRO0lBeUJyQztRQUZDLFlBQVksQ0FBQyxlQUFlLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQztpRkFJekM7SUF4RFUsNkNBQTZDO1FBTHpELFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSw2Q0FBNkM7WUFDdkQscUVBQXVFOztTQUV4RSxDQUFDO09BQ1csNkNBQTZDLENBbU16RDtJQUFELG9EQUFDO0NBQUEsQUFuTUQsSUFtTUM7U0FuTVksNkNBQTZDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEVsZW1lbnRSZWYsIFZpZXdDaGlsZCwgVmlld0NoaWxkcmVuLCBRdWVyeUxpc3QsIFJlbmRlcmVyMiwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBTaW1wbGVDaGFuZ2VzLCBDaGFuZ2VEZXRlY3RvclJlZiwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGV4dGVuZE1vbWVudCB9IGZyb20gJ21vbWVudC1yYW5nZSc7XHJcbmltcG9ydCAqIGFzIE1vbWVudCBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgeyBDYWxlbmRhckNvbnN0YW50cyB9IGZyb20gJy4uL3NoYXJlZC9jb25zdGFudHMvQ2FsZW5kYXIuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTWVkaWFNYXRjaGVyIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2xheW91dCc7XHJcblxyXG5jb25zdCBtb21lbnQgPSBleHRlbmRNb21lbnQoTW9tZW50KTtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLXJlc291cmNlLW1hbmFnZW1lbnQtcmVzb3VyY2UtYWxsb2NhdGlvbicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3Jlc291cmNlLW1hbmFnZW1lbnQtcmVzb3VyY2UtYWxsb2NhdGlvbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vcmVzb3VyY2UtbWFuYWdlbWVudC1yZXNvdXJjZS1hbGxvY2F0aW9uLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUmVzb3VyY2VNYW5hZ2VtZW50UmVzb3VyY2VBbGxvY2F0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgc2VsZWN0ZWRNb250aDogYW55O1xyXG4gIEBJbnB1dCgpIGRhdGE7XHJcbiAgLy8gQElucHV0KCkgZGF0ZXNzcztcclxuICBASW5wdXQoKSB1c2VycztcclxuICBASW5wdXQoKSBzZWxlY3RlZEl0ZW1zO1xyXG4gIEBJbnB1dCgpIGFsbG9jYXRpb247XHJcbiAgQElucHV0KCkgc2VsZWN0ZWRZZWFyO1xyXG5cclxuICBAT3V0cHV0KCkgcmVtb3ZlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55W10+KCk7XHJcblxyXG4gIHNjcm9sbGVkID0gZmFsc2U7XHJcbiAgZGF0ZXMgPSBbXTtcclxuICB2aWV3VHlwZUNlbGxTaXplID0gMTUwOy8vMzU7XHJcbiAgbW9tZW50UHRyID0gbW9tZW50O1xyXG4gIGN1cnJlbnRWaWV3U3RhcnREYXRlOiBtb21lbnQuTW9tZW50O1xyXG4gIGN1cnJlbnRWaWV3RW5kRGF0ZTogbW9tZW50Lk1vbWVudDtcclxuXHJcbiAgQFZpZXdDaGlsZCgnY2FsZW5kYXJWaWV3JylcclxuICBjYWxlbmRhclZpZXc6IEVsZW1lbnRSZWY7XHJcblxyXG4gIEBWaWV3Q2hpbGRyZW4oJ2RhdGVIZWFkZXInKVxyXG4gIGNhbGVuZGFySGVhZGVyczogUXVlcnlMaXN0PEVsZW1lbnRSZWY+O1xyXG5cclxuICBAVmlld0NoaWxkcmVuKCdwYW5lbENvbnRhaW5lcicpXHJcbiAgcGFuZWxDb250YWluZXJzOiBRdWVyeUxpc3Q8RWxlbWVudFJlZj47XHJcblxyXG4gIEBWaWV3Q2hpbGRyZW4oJ2NhbGVuZGFyQ2VsbCcpXHJcbiAgY2FsZW5kYXJDZWxsczogUXVlcnlMaXN0PEVsZW1lbnRSZWY+O1xyXG5cclxuICBjdXJyZW50RGF0ZTtcclxuXHJcbiAgc2lkZWJhclN0eWxlID0ge307XHJcblxyXG4gIGRhdGFzO1xyXG4gIE9iamVjdCA9IE9iamVjdDtcclxuXHJcbiAgTWF0aCA9IE1hdGg7XHJcblxyXG4gIHNjcmVlbldpZHRoOiBhbnk7XHJcbiAgaGVpZ2h0O1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyByZW5kZXJlcjogUmVuZGVyZXIyLFxyXG4gICAgcHVibGljIG1lZGlhOiBNZWRpYU1hdGNoZXIsXHJcbiAgICBwdWJsaWMgY2hhbmdlRGV0ZWN0b3JSZWY6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICkge1xyXG4gICAgLy8xOTFcclxuICAgIHRoaXMuZ2V0SGVpZ2h0Rm9yU2NyZWVuKCk7XHJcbiAgfVxyXG5cclxuICBASG9zdExpc3RlbmVyKCd3aW5kb3c6cmVzaXplJywgWyckZXZlbnQnXSlcclxuXHJcbiAgb25SZXNpemUoZXZlbnQpIHtcclxuICAgIHRoaXMuZ2V0SGVpZ2h0Rm9yU2NyZWVuKCk7XHJcbiAgfVxyXG5cclxuICAvLzE5MVxyXG4gIGdldEhlaWdodEZvclNjcmVlbigpIHtcclxuICAgIGNvbnN0IHdpZHRoID0gMTUzNjtcclxuICAgIGNvbnN0IGhlaWdodCA9IDI2O1xyXG4gICAgY29uc3QgbWF4X2hlaWdodCA9IDM5O1xyXG4gICAgdGhpcy5zY3JlZW5XaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoO1xyXG4gICAgY29uc3QgZGlmZiA9IE1hdGguZmxvb3IoKHRoaXMuc2NyZWVuV2lkdGggLSB3aWR0aCkgLyAxNzEpO1xyXG4gICAgdGhpcy5oZWlnaHQgPSBoZWlnaHQgKyAoZGlmZiAqIDIpIDw9IG1heF9oZWlnaHQgPyBoZWlnaHQgKyAoZGlmZiAqIDIpIDogMzg7XHJcbiAgfVxyXG5cclxuICAvLyB0byByZW1vdmUgdGhlIGZpbHRlclxyXG4gIHJlbW92ZUZpbHRlcihmaWx0ZXIpIHtcclxuICAgIGlmIChmaWx0ZXIuUk9MRV9OQU1FIHx8IGZpbHRlci5OQU1FKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRJdGVtcy5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xyXG4gICAgICAgIGlmICgoaXRlbS5ST0xFX05BTUUgIT09IHVuZGVmaW5lZCAmJiBmaWx0ZXIuUk9MRV9OQU1FICE9PSB1bmRlZmluZWQgJiYgaXRlbS5ST0xFX05BTUUgPT09IGZpbHRlci5ST0xFX05BTUUpIHx8IChpdGVtLk5BTUUgIT09IHVuZGVmaW5lZCAmJiBmaWx0ZXIuTkFNRSAhPT0gdW5kZWZpbmVkICYmIGl0ZW0uTkFNRSA9PT0gZmlsdGVyLk5BTUUpKSB7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkSXRlbXMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgIHRoaXMucmVtb3ZlZC5uZXh0KGZpbHRlci5OQU1FICE9PSB1bmRlZmluZWQgJiYgZmlsdGVyWydNUE1fVGVhbXMtaWQnXSAhPT0gdW5kZWZpbmVkICYmIGZpbHRlclsnTVBNX1RlYW1zLWlkJ10uSWQgIT09IHVuZGVmaW5lZCA/IFsnVEVBTSddIDogWydST0xFJ10pO1xyXG4gICAgICAgICAgLy8gdGhpcy5yZW1vdmVkLm5leHQoZmlsdGVyLlJPTEVfTkFNRSAhPT0gdW5kZWZpbmVkID8gWydST0xFJ10gOiBbJ1RFQU0nXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRJdGVtcy5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xyXG4gICAgICAgIGlmICgoaXRlbS5GdWxsTmFtZSAhPT0gdW5kZWZpbmVkICYmIGZpbHRlci5GdWxsTmFtZSAhPT0gdW5kZWZpbmVkICYmIGl0ZW0uRnVsbE5hbWUgPT09IGZpbHRlci5GdWxsTmFtZSkgfHwgKGl0ZW0ubmFtZSAhPT0gdW5kZWZpbmVkICYmIGZpbHRlci5uYW1lICE9PSB1bmRlZmluZWQgJiYgaXRlbS5uYW1lID09PSBmaWx0ZXIubmFtZSkpIHtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRJdGVtcy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAoZmlsdGVyLkZ1bGxOYW1lIHx8IGZpbHRlci5uYW1lKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiIyMjXCIpXHJcbiAgICAgIGNvbnNvbGUubG9nKGZpbHRlcik7XHJcbiAgICAgIHRoaXMucmVtb3ZlZC5uZXh0KFtmaWx0ZXJdKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKiAqIFNjcm9sbCBmb3IgRml4aW5nIHRoZSBkYXRlcyBhbmQgaGVhZGVyICovXHJcbiAgc2Nyb2xsaW5nID0gKGVsKTogdm9pZCA9PiB7XHJcbiAgICBjb25zdCBzY3JvbGxUb3AgPSBlbC5zcmNFbGVtZW50LnNjcm9sbFRvcDtcclxuICAgIGlmIChzY3JvbGxUb3AgPj0gMTAwKSB7XHJcbiAgICAgIHRoaXMuc2Nyb2xsZWQgPSB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zY3JvbGxlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqIENBTEVOREFSIE1FVEhPRFMgKiovXHJcblxyXG4gIC8vIGJ1aWxkIGNhbGVuZGFyIGJhc2VkIG9uIGdpdmVuIHN0YXJ0ICBhbmQgZW5kIGRhdGVcclxuICBidWlsZENhbGVuZGFyKHN0YXJ0RGF0ZSwgZW5kRGF0ZSkge1xyXG4gICAgdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSA9IHN0YXJ0RGF0ZTtcclxuICAgIHRoaXMuY3VycmVudFZpZXdFbmREYXRlID0gZW5kRGF0ZTtcclxuICAgIGNvbnN0IHN0YXJ0ID0gc3RhcnREYXRlO1xyXG4gICAgY29uc3QgZW5kID0gZW5kRGF0ZTtcclxuICAgIGNvbnN0IHJhbmdlID0gdGhpcy5tb21lbnRQdHIucmFuZ2Uoc3RhcnQsIGVuZCk7XHJcbiAgICBjb25zdCBkYXlzID0gQXJyYXkuZnJvbShyYW5nZS5ieSgnZGF5cycpKTtcclxuICAgIHRoaXMuZGF0ZXMgPSBkYXlzO1xyXG4gIH1cclxuXHJcbiAgZ2V0Tm9PZkRheXMoc2VsZWN0ZWRNb250aCkge1xyXG4gICAgbGV0IG5vT2ZEYXlzO1xyXG4gICAgaWYgKENhbGVuZGFyQ29uc3RhbnRzLmV2ZW5Nb250aC5pbmNsdWRlcyhzZWxlY3RlZE1vbnRoKSkge1xyXG4gICAgICBub09mRGF5cyA9IDMxO1xyXG4gICAgfSBlbHNlIGlmIChDYWxlbmRhckNvbnN0YW50cy5vZGRNb250aC5pbmNsdWRlcyhzZWxlY3RlZE1vbnRoKSkge1xyXG4gICAgICBub09mRGF5cyA9IDMwO1xyXG4gICAgfSBlbHNlIGlmIChDYWxlbmRhckNvbnN0YW50cy5sZWFwTW9udGguaW5jbHVkZXMoc2VsZWN0ZWRNb250aCkpIHtcclxuICAgICAgLyogY29uc3QgZGF0ZSA9IG5ldyBEYXRlKHRoaXMueWVhciwgMSwgMjkpO1xyXG4gICAgICByZXR1cm4gZGF0ZS5nZXRNb250aCgpID09PSAxOyAqL1xyXG4gICAgICBub09mRGF5cyA9IDI4O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG5vT2ZEYXlzO1xyXG4gIH1cclxuXHJcbiAgZ2V0RGF0ZURpZmZlcmVuY2Uoc3RhcnREYXRlLCBlbmREYXRlKSB7XHJcbiAgICBsZXQgc2Q6IGFueSA9IChuZXcgRGF0ZShzdGFydERhdGUpKTtcclxuICAgIGxldCBlZDogYW55ID0gbmV3IERhdGUoZW5kRGF0ZSk7XHJcbiAgICByZXR1cm4gKE1hdGguZmxvb3IoZWQgLSBzZCkgLyAoMTAwMCAqIDYwICogNjAgKiAyNCkpICsgMTtcclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIGlmIChjaGFuZ2VzICYmIGNoYW5nZXMuc2VsZWN0ZWRNb250aCAmJiAhY2hhbmdlcy5zZWxlY3RlZE1vbnRoLmZpcnN0Q2hhbmdlKSB7XHJcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgQ2FsZW5kYXJDb25zdGFudHMubW9udGhzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgaWYgKENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1tpXSA9PT0gY2hhbmdlcy5zZWxlY3RlZE1vbnRoLmN1cnJlbnRWYWx1ZSkge1xyXG4gICAgICAgICAgdGhpcy5idWlsZENhbGVuZGFyKGkgKyAxICsgJy8wMS8nICsgdGhpcy5zZWxlY3RlZFllYXIsIGkgKyAxICsgJy8nICsgdGhpcy5nZXROb09mRGF5cyhjaGFuZ2VzLnNlbGVjdGVkTW9udGguY3VycmVudFZhbHVlKSArICcvJyArIHRoaXMuc2VsZWN0ZWRZZWFyKTtcclxuICAgICAgICAgIC8qICB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlID0gaSArIDEgKyAnLzAxLycgKyB0aGlzLnNlbGVjdGVkWWVhcjtcclxuICAgICAgICAgICAgIHRoaXMuY3VycmVudFZpZXdFbmREYXRlID0gaSArIDEgKyAnLycgKyB0aGlzLmdldE5vT2ZEYXlzKGNoYW5nZXMuc2VsZWN0ZWRNb250aC5jdXJyZW50VmFsdWUpICsgJy8nICsgdGhpcy5zZWxlY3RlZFllYXI7ICovXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGNoYW5nZXMgJiYgY2hhbmdlcy5kYXRhICYmIGNoYW5nZXMuZGF0YS5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgdGhpcy5kYXRhcyA9IE9iamVjdC5rZXlzKGNoYW5nZXMuZGF0YS5jdXJyZW50VmFsdWUpLmxlbmd0aCAtIDE7XHJcbiAgICAgIGxldCBrOiBhbnksIHY6IGFueSwgc3RhcnRkYXRlLCBlbmRkYXRlO1xyXG4gICAgICAvLyB0byBjb21wYXJlIHRoZSBkYXRlIGFuZCB1cGRhdGUgdGhlIG92ZXJsYXAgb2YgZGF0ZVxyXG4gICAgICBmb3IgKFtrLCB2XSBvZiBPYmplY3QuZW50cmllcyhjaGFuZ2VzLmRhdGEuY3VycmVudFZhbHVlKSkge1xyXG4gICAgICAgIGlmICh0eXBlb2YgKHYpICE9PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB2Lmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGogPSBpICsgMTsgaiA8PSB2Lmxlbmd0aCAtIDE7IGorKykge1xyXG4gICAgICAgICAgICAgIGlmICh2W2pdICYmIHZbal0uZGVsaXZlcmFibGVEZXRhaWwgJiYgdltqXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodltpXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX0RVRV9EQVRFID49IHZbal0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9EVUVfREFURSkge1xyXG4gICAgICAgICAgICAgICAgICBlbmRkYXRlID0gdltqXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX0RVRV9EQVRFXHJcbiAgICAgICAgICAgICAgICAgIGlmICh2W2pdLmRlbGl2ZXJhYmxlRGV0YWlsLmRhdGEuREVMSVZFUkFCTEVfU1RBUlRfREFURSA8PSB2W2ldLmRlbGl2ZXJhYmxlRGV0YWlsLmRhdGEuREVMSVZFUkFCTEVfU1RBUlRfREFURSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHN0YXJ0ZGF0ZSA9IHZbaV0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9TVEFSVF9EQVRFXHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodltpXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUgPD0gdltqXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUpIHtcclxuICAgICAgICAgICAgICAgICAgICBzdGFydGRhdGUgPSB2W2pdLmRlbGl2ZXJhYmxlRGV0YWlsLmRhdGEuREVMSVZFUkFCTEVfU1RBUlRfREFURVxyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICBlbmRkYXRlID0gdltpXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX0RVRV9EQVRFXHJcbiAgICAgICAgICAgICAgICAgIGlmICh2W2ldLmRlbGl2ZXJhYmxlRGV0YWlsLmRhdGEuREVMSVZFUkFCTEVfU1RBUlRfREFURSA8PSB2W2pdLmRlbGl2ZXJhYmxlRGV0YWlsLmRhdGEuREVMSVZFUkFCTEVfU1RBUlRfREFURSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHN0YXJ0ZGF0ZSA9IHZbal0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9TVEFSVF9EQVRFXHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodltqXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUgPD0gdltpXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUpIHtcclxuICAgICAgICAgICAgICAgICAgICBzdGFydGRhdGUgPSB2W2ldLmRlbGl2ZXJhYmxlRGV0YWlsLmRhdGEuREVMSVZFUkFCTEVfU1RBUlRfREFURVxyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsZXQgc2QgPSBwYXJzZUludChzdGFydGRhdGUuc3BsaXQoJ1QnKVswXS5zcGxpdCgnLScpWzJdIDwgMTAgPyBzdGFydGRhdGUuc3BsaXQoJ1QnKVswXS5zcGxpdCgnLScpWzJdLnNwbGl0KCcwJylbMV0gOiBzdGFydGRhdGUuc3BsaXQoJ1QnKVswXS5zcGxpdCgnLScpWzJdKVxyXG4gICAgICAgICAgICAgICAgbGV0IGVkID0gcGFyc2VJbnQoZW5kZGF0ZS5zcGxpdCgnVCcpWzBdLnNwbGl0KCctJylbMl0gPCAxMCA/IGVuZGRhdGUuc3BsaXQoJ1QnKVswXS5zcGxpdCgnLScpWzJdLnNwbGl0KCcwJylbMV0gOiBlbmRkYXRlLnNwbGl0KCdUJylbMF0uc3BsaXQoJy0nKVsyXSlcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IG0gPSBzZCAtIDE7IG0gPCBlZDsgbSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGNoYW5nZXMuZGF0YS5jdXJyZW50VmFsdWVba10uZGF0ZVttXSlcclxuICAgICAgICAgICAgICAgICAgY2hhbmdlcy5kYXRhLmN1cnJlbnRWYWx1ZVtrXS5kYXRlW21dLmlzT3ZlcmxhcCA9ICd0cnVlJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgY29uc3QgY3VycmVudEZ1bGxEYXRlID0gbmV3IERhdGUoKTtcclxuICAgIGNvbnN0IGN1cnJlbnRZZWFyID0gY3VycmVudEZ1bGxEYXRlLmdldEZ1bGxZZWFyKCk7XHJcbiAgICBjb25zdCBjdXJyZW50TW9udGggPSBjdXJyZW50RnVsbERhdGUuZ2V0TW9udGgoKSArIDE7XHJcbiAgICBjb25zdCBjdXJyZW50RGF0ZSA9IGN1cnJlbnRGdWxsRGF0ZS5nZXREYXRlKCk7XHJcbiAgICB0aGlzLmN1cnJlbnREYXRlID0gY3VycmVudE1vbnRoICsgJy8nICsgY3VycmVudERhdGUgKyAnLycgKyBjdXJyZW50WWVhcjtcclxuICAgIGxldCBub09mRGF5cyA9IHRoaXMuZ2V0Tm9PZkRheXModGhpcy5zZWxlY3RlZE1vbnRoKTtcclxuICAgIHRoaXMuYnVpbGRDYWxlbmRhcihjdXJyZW50TW9udGggKyAnLzAxLycgKyBjdXJyZW50WWVhciwgY3VycmVudE1vbnRoICsgJy8nICsgbm9PZkRheXMgKyAnLycgKyBjdXJyZW50WWVhcik7XHJcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5zY3JvbGxpbmcsIHRydWUpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19