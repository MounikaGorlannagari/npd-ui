export const integrationConfig: any = {
    resourceManagementRoles: [
        {
            'ROLE_NAME':'GFG',
            'NAME':'GFG',
            'Id': '114695',
            'ROLE_DN': 'cn=GFG,cn=organizational roles,o=NPD,cn=cordys,cn=defaultInst,o=MEC.internal',
        },
        {
            'ROLE_NAME':'Regulatory Affairs',
            'NAME':'Regulatory Affairs',
            'Id': '114696',
            'ROLE_DN': 'cn=Regulatory Affairs,cn=organizational roles,o=NPD,cn=cordys,cn=defaultInst,o=MEC.internal',
        }
    ],
    reportsUrl: 'https://mmqa.monsterenergy.com/iportal/loginservlet',
    reportsIframeUrl: 'https://mmqa.monsterenergy.com/iportal/Monster_NPD_Reports/report.html',
    loadUrl: false,
    userId : null,
    reports : [
        {
            name:'Report 1',
            url:'https://mmqa.monsterenergy.com/iportal/Monster_NPD_Reports/report.html'
        },
        {
            name:'Report 2',
            url:'https://mmqa.monsterenergy.com/iportal/Monster_NPD_Reports/report.html'
        }
    ]
};
 
// export const integrationConfig: any = {
//     resourceManagementRoles: [
//         {
//             'ROLE_NAME':'GFG',
//             'NAME':'GFG',
//             'Id': '65541',
//             'ROLE_DN': 'cn=GFG,cn=NPD Organization Model,cn=cordys,cn=TEST,o=HBC.internal',
//         },
//         {
//             'ROLE_NAME':'Regulatory Affairs',
//             'NAME':'Regulatory Affairs',
//             'Id': '65543',
//             'ROLE_DN': 'cn=Regulatory Affairs,cn=organizational roles,o=NPD,cn=cordys,cn=TEST,o=HBC.internal',
//         }
//     ],
//     reportsUrl: 'https://mmqa2.monsterenergy.com/iportal/loginservlet',
//     reportsIframeUrl : 'https://mmqa2.monsterenergy.com/iportal/Monster_NPD_Reports/report.html',
//     loadUrl: false,
//     userId : 'Jaymal@NPD Users'//'mmihubuser@OTMM',//null
// };
 
