import { EnvConfig } from 'mpm-library';
// import { npdEnvConfig } from 'src/environments/environment'

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// export interface npdEnvConfig extends EnvConfig {
//   projectClassificationItemId?: any
// }

export const environment: EnvConfig = {
  production: true,
  environment: 'prod',
  gatewayProtocol: 'https://',
  // gatewayHost: 'starz.rtecm.com',
  gatewayHost: 'mmdev.monsterenergy.com',
  gatewayPort: '',
  psHome: 'home',
  organizationName: 'NPD',
  otdsProtocol: 'https://',
  otdsHost: 'mmdev.monsterenergy.com',
  otdsPort: '',
  otdsRestUrl: '/otdsws',
  gatewayUrl: '/com.eibus.web.soap.Gateway.wcp',
  psSSOLoginUrl: '/wcp/sso/com.eibus.sso.web.authentication.LogMeIn.wcp',
  psSSOLogoutUrl: '/wcp/sso/com.eibus.sso.web.authentication.LogMeOut.wcp',
  psGatewayLogoutUrl: '/wcp/sso/com.eibus.sso.web.authentication.Logout.wcp',
  preLoginUrl: '/com.eibus.sso.web.authentication.PreLoginInfo.wcp',
  psTicketConsumerUrl: '/com.eibus.sso.otds.TicketConsumerService.wcp',
  psDeployedPath: '/tracker',
  logoutUrl: 'assets/logout.htm',
  instanceIdentifier: 'cn=cordys,cn=defaultInst,o=MEC.internal',
  psOtdsResource: '7cff35be-dd20-400d-b504-408c739b36c3',
  otdsTicketCookieName: '',
  otmmSessionCookieName: 'JSESSIONID',
  maxFileSize: null,
  maxFiles: 10,
  qdsProtocol: 'https://',
  qdsHost: 'mmdev.monsterenergy.com',
  qdsPort: '8081',
  qdsVersion: '1.5.4',
  qdsLibraryUrl: '/qds/xfer/v1/otmm/static/qds_otmm.js',
  brandConfigPath: 'styles/brandConfig.json',
  projectClassificationItemId: '005056AAD0B6A1EE852C0419473FE9D2.'
};
