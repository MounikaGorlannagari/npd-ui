import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'apps', loadChildren: () => import('./core/core.module').then(m => m.CoreModule) },
  { path: 'login', loadChildren: () => import('./login/app-login.module').then(m => m.AppLoginModule) },
  { path: '**', redirectTo: '/apps/mpm' },
  { path: '', redirectTo: '/apps/mpm', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
