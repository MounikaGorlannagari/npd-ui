import { Component, OnInit, Renderer2, RendererFactory2 } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd } from '@angular/router';
import { LoaderService, UtilService, SharingService } from 'mpm-library';
import { environment } from 'src/environments/environment';
import { GloabalConfig as config } from 'mpm-library';
import { ApplicationConfigConstants } from 'mpm-library';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ScriptService } from 'mpm-library';
import { AppDynamicConfigService } from 'mpm-library';
import { AppDynamicConfig } from 'mpm-library';
import { NotificationService } from 'mpm-library';
import { EntityHelperService } from './core/npd-ui/services/entity-helper.service';
import { NPDConfig as npdConfig } from "./core/npd-ui/utils/config/NPDConfig"


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'acheron-mpm';
  private jsonURL;
  private renderer: Renderer2;
  private head: HTMLElement;
  private body: HTMLElement;
  private themeLinks: HTMLElement[] = [];
  brandConfig: any;
  showCopyRight: boolean;

  constructor(
    private router: Router,
    private loaderService: LoaderService,
    private utilService: UtilService,
    private sharingService: SharingService,
    private http: HttpClient,
    rendererFactory: RendererFactory2,
    private appDynamicConfigService: AppDynamicConfigService,
    private scriptService: ScriptService,
    private notificationService: NotificationService,
    private entityHelperService: EntityHelperService,
  ) {
    this.head = document.head;
    this.body = document.body;
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  showConsoleWarning() {
    console.log('%cWelcome to MPM', 'color: #b4b4b4; font-size: 18px;')
    console.log('%cWARNING!', 'color: red; background: #f4ec42; font-size: 16px;')
    console.log('%cUsing this console may allow attackers to impersonate you and steal your information using an attack called Self-XSS.'
      + '\nDo not enter or paste code that you do not understand.', 'color: red; background: yellow; font-size: 14px;');
  }

  setGlobalConfig() {
    const favIconPath = AppDynamicConfig.getFavIcon();
    if (favIconPath) {
      document.getElementById('app-favIcon')['href'] = favIconPath;
    }
  }

  setBrandingConfig(): Observable<boolean> {
    return new Observable(observer => {
      this.appDynamicConfigService.getBrandingAndConfig(this.jsonURL).subscribe(response => {
        AppDynamicConfig.setBrandingConfig(response);
        this.setGlobalConfig();
        observer.next(true);
        observer.complete();
      }, error => {
        console.log('Error while loading dynamic config');
        observer.error(new Error(error));
      });
    });
  }

  loadAppStyle(): Observable<boolean> {
    return new Observable(observer => {
      try {
        //const appThemeName = AppDynamicConfig.getAppThemeName();
        //this.renderer.addClass(this.body, appThemeName);
        let stylePath;
        if (AppDynamicConfig.getDefaultStylePath()) {
          stylePath = AppDynamicConfig.getDefaultStylePath();
        } else {
          console.warn('No style configured');
        }
        //const linkEl: HTMLElement = this.renderer.createElement('link');
        //this.renderer.setAttribute(linkEl, 'rel', 'stylesheet');
        //this.renderer.setAttribute(linkEl, 'type', 'text/css');
        //this.renderer.setAttribute(linkEl, 'href', stylePath);
        //this.renderer.setProperty(linkEl, 'onload', observer);
        //this.renderer.appendChild(this.head, linkEl);
        //this.themeLinks = [...this.themeLinks, linkEl];
        observer.next();
        observer.complete();
      } catch (e) {
        console.warn('Error while loading the style', e);
        observer.next();
        observer.complete();
      }
    });
  }

  loadExternalScripts(): Observable<boolean> {
    return new Observable(oberserver => {
      this.scriptService.load('qdsLibrary')
        .then(data => {
          console.log('QDS library loaded.', data);
        })
        .catch(error => {
          console.log(error);
        });
    });
  }
  // private checkAndInitialize() {
  //   if (this.utilService.APP_ID) {
  //     this.entityHelperService
  //       .initializeAppEntityLists(this.utilService.APP_ID)
  //       .subscribe();
  //   } else {
  //     setTimeout(() => {
  //       this.checkAndInitialize(); // Retry until APP_ID is available
  //     }, 100); // Adjust the interval as needed
  //   }
  // }
  ngOnInit() {
    this.showConsoleWarning();
    this.notificationService.autoHideDuration = 15000;
    this.sharingService.getSubscribableBrandConfig().subscribe(response => {
      if (response) {
        this.showCopyRight = this.utilService.getBooleanValue(response[ApplicationConfigConstants.MPM_BRAND_CONFIG.SHOW_COPYRIGHT]);

        this.entityHelperService
          .initializeAppEntityLists(this.utilService.APP_ID)
          .subscribe();
        // this.checkAndInitialize();

      }
      // this.notificationService.autoHideDuration = 15000;
    });


    this.router.events.subscribe(event => {
      if (event instanceof RouteConfigLoadStart) {
        this.loaderService.show();
      } else if (event instanceof RouteConfigLoadEnd) {
        this.loaderService.hide();
      }
    });

    if (environment.environment !== 'local') {
      config.setConfig(window['envConfig']);
      npdConfig.setIntegrationConfig(window["integrationConfig"]);
    } else {
      config.setConfig(environment);
      npdConfig.setIntegrationConfig(window["integrationConfig"]);
    }
    this.jsonURL = config.getBrandConfigPath();

    this.setBrandingConfig().subscribe(response => {
      this.loadAppStyle().subscribe(appStyleLoadingDone => {
        console.log('Loaded App Style');
      });
    });

    this.loadExternalScripts().subscribe();
  }
}
