import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'mpm-library';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

    constructor(
        public router: Router,
        private notification: NotificationService
    ) { }

    getLastVisitedLinik() {
        return localStorage.getItem('MPM_V3_LAST_LINK');
    }

    onAuthenticated(auth: any) {
        if (auth && auth.isAuthenticated) {
            this.notification.success('Successfully logged in');
            if (this.getLastVisitedLinik() &&
                this.getLastVisitedLinik() !== '') {
                window.location.replace(this.getLastVisitedLinik());
            } else {
                this.router.navigate(['/mpm']);
            }
        } else {
            this.notification.error(auth.reason);
        }
    }

    ngOnInit() { }

}
