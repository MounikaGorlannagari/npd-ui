import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoutingConstants } from './mpm/routingConstants';

@Component({
    selector: 'app-core',
    templateUrl: './core.component.html',
    styleUrls: ['./core.component.scss']
})

export class CoreComponent implements OnInit {

    constructor(
        public router: Router
    ) { }

    ngOnInit() {
        this.router.navigate([RoutingConstants.mpmBaseRoute]);
    }

}
