import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MpmComponent } from './mpm.component';
import { MpmRoutingModule } from './mpm.routing.module';
import { MpmLibraryModule, AppMenuModule, TasksModule, SearchModule } from 'mpm-library';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { MaterialModule } from 'src/app/material.module';
import { ProjectManagementModule } from './project-management/project-management.module';
import { HomeRouteComponent } from './home-route/home-route.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NoAccessComponent } from './no-access/no-access.component';
import { SharedModule } from './shared/shared.module';
import { MpmLibModule } from '../mpm-lib/mpm-lib.module';
import { NotificationTrayModule } from 'mpm-library';
import { MultipleUserPreferencesDashboardComponent } from './multiple-user-preferences-dashboard/multiple-user-preferences-dashboard.component'; /*
import { ResourceManagementModule } from '../mpm/resource-management/resource-management.module'; */
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NpdPaginationModule } from '../npd-ui/npd-pagination/npd-pagination.module';
import { MatSortModule } from '@angular/material/sort/sort-module';
// import { ColumnChooserModul } from '../mpm-lib/column-chooser/column-chooser.module';

@NgModule({
    declarations: [
        MpmComponent,
        HomeLayoutComponent,
        HomeRouteComponent,
        NotFoundComponent,
        NoAccessComponent,
        MultipleUserPreferencesDashboardComponent,
        // MatSortModule
    ],
    imports: [
        CommonModule,
        MpmRoutingModule,
        MaterialModule,
        MpmLibraryModule,
        TasksModule,
        AppMenuModule,
        ProjectManagementModule,
        SearchModule,
        SharedModule,
        MpmLibModule,
        // MatSortModule,
        // ColumnChooserModul,
        NotificationTrayModule,/*, 
        ResourceManagementModule */
        NpdPaginationModule
    ],
    entryComponents: [],
    exports: [
        ProjectManagementModule,
        MultipleUserPreferencesDashboardComponent
    ],
    providers: [MultipleUserPreferencesDashboardComponent,
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
    ],
})

export class MpmModule { }
