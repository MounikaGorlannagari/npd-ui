import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  OnChanges,
  SimpleChanges,
  SimpleChange,
} from '@angular/core';
import { DashboardControlConstant } from './dashboard-control-constants';
import {
  LoaderService,
  NotificationService,
  SharingService,
  MPM_LEVELS,
  SearchChangeEventService,
  SearchConfigConstants,
  SearchDataService,
  FacetChangeEventService,
  FacetConditionList,
  FieldConfigService,
  ViewConfig,
  ProjectTypes,
  IndexerService,
  SearchRequest,
  SearchResponse,
  ViewConfigService,
  OTMMMPMDataTypes,
  MPMSearchOperators,
  MPMFieldConstants,
  ProjectConstant,
  Team,
  ProjectService,
} from 'mpm-library';

import { RouteService } from '../../route.service';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { ProjectDialogComponent } from '../project-dialog/project-dialog.component';
import { MediaMatcher } from '@angular/cdk/layout';
import {
  PMDashboardFilters,
  PMDashboardFilterValues,
} from '../filter/project-dashboard-filter';
import { MPMSearchOperatorNames } from 'mpm-library';
import { UtilService } from 'mpm-library';
import { BulkProjectManagementComponent } from '../bulk-project-management/bulk-project-management.component';
import { ApplicationConfigConstants } from 'mpm-library';
import { ProjectBulkCountService } from 'mpm-library';
import { ActivatedRoute, Router } from '@angular/router';
import { StateService } from 'mpm-library';
import { RoutingConstants } from '../../routingConstants';
import { FormatToLocalePipe } from 'mpm-library';
import { AppService } from 'mpm-library';
import { MenuConstants } from '../../shared/constants/menu.constants';
import { ProjectMilestoneComponent } from '../project-milestone/project-milestone.component';
import { ExportDataComponent } from 'mpm-library';
import { forkJoin, Observable, Subject } from 'rxjs';
import { SessionStorageConstants } from 'mpm-library';
import { ConfirmationModalComponent } from 'mpm-library';
import { IndexerDisplayFields } from 'mpm-library';
import { GloabalConfig as config } from 'mpm-library';
import { takeUntil } from 'rxjs/operators';
import * as $ from 'jquery';
import { NpdProjectService } from 'src/app/core/npd-ui/npd-project-view/services/npd-project.service';
import { NPDFacetService } from 'src/app/core/mpm-lib/facet/facet.service';
import { F } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-manager-dashboard',
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.scss'],
})
export class ManagerDashboardComponent implements OnInit, OnDestroy, OnChanges {
  public unsubscribe$: Subject<any> = new Subject<any>();
  selectedProjectCounts = 0;
  mobileQuery: MediaQueryList;

  GET_ALL_DATA_EXPORT_NS = 'http://schemas.monster.npd.com/export/bpm/1.0';
  GET_ALL_DATA_EXPORT_WS = 'NPDExportData';

  public mobileQueryListener: () => void;
  checking: any = true;
  activeUserPreference: any = null;
  activePreferenceName: string = 'NA';
  isDefaultUserPreference: any;
  // changedPref: boolean
  unSavedPreference: string;
  sectionFields: any;
  constructor(
    public loaderService: LoaderService,
    public routeService: RouteService,
    public searchDataService: SearchDataService,
    public changeDetectorRef: ChangeDetectorRef,
    public dialog: MatDialog,
    public media: MediaMatcher,
    public notificationService: NotificationService,
    public sharingService: SharingService,
    public searchChangeEventService: SearchChangeEventService,
    public facetChangeEventService: FacetChangeEventService,
    public fieldConfigService: FieldConfigService,
    public viewConfigService: ViewConfigService,
    public indexerService: IndexerService,
    public utilService: UtilService,
    public projectBulkCountService: ProjectBulkCountService,
    public activatedRoute: ActivatedRoute,
    public stateService: StateService,
    public router: Router,
    public formatToLocalePipe: FormatToLocalePipe,
    public appService: AppService,
    public npdProjectService: NpdProjectService,
    public facetService: NPDFacetService
  ) {
    this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
    if (this.mobileQuery.matches) {
      // tslint:disable-next-line: deprecation
      this.mobileQuery.addListener(this.mobileQueryListener);
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    //    throw new Error('Method not implemented.');
    // this.freezeColumnCount=SimpleChange
  }

  dashboardMenuConfig;
  searchConditions = [];
  //pageSize = 10;
  page = 1;
  skip = 0;
  // top = 10;
  pageSize: number;
  top: number;
  tempsearchConditionsSubscribe: any;
  searchConfigId = -1;

  facetExpansionState = false;
  facetRestrictionList: FacetConditionList;
  projectConfig: any;
  appConfig: any;
  disableGridView: boolean;
  isTaskListView: any;
  optionDropdown = false;
  selectedProjectData = [];
  dynamicProjectView;

  selectedSortOption;
  selectedSortOrder;
  searchName = null;
  savedSearchName = null;
  pageNumber;
  isdefaultPagination = true;
  isRefresh = true;
  queryParams;
  isCampaignRole;
  isCampaign;
  isCampaignDashboard;
  campaignId;
  check;

  advSearchData = null;
  facetData = null;
  routeParams = null;
  exportTableData = null;
  exportDataSearchRequest: SearchRequest;

  openCampaign;
  enableCampaign;
  projectType;

  previousCampaignRequest;
  previousProjectRequest;
  previousProjectResponse;
  previousCampaignResponse;
  previousFacetCondition;
  validateFacet = false;

  columnChooserFields;
  displayableFields;
  isListView = null;
  defaultViewFilterName;
  hasPagination = null;
  userPreferenceFreezeCount: any;
  isColumnsFreezable: boolean = true;
  enableIndexerFieldRestriction;
  maximumFieldsAllowed;
  facetConditionValue = [];
  /*   masterToggle(event) {
          this.selectedProjectCounts = event;
          if (this.selectedProjectCounts === 1) {
              this.notificationService.info("One or more records do not meet bulk edit criteria");
          }
      } */
  masterToggle(event) { }
  /* selectedProjectsData(event) {
        this.selectedProjectData = event;
    }
 */
  getSelectedProject(projectData) {
    // this.selectedProjects.select(projectData);
    this.selectedProjectData = projectData;
    this.selectedProjectCounts = projectData.length;
  }

  /* getUnSelectedProject(projectData){
      //  this.selectedProjects.deselect(projectData);
        this.selectedProjectData = projectData;
    } */

  /*   selectedProjectsDatas(data){
          this.selectedProjectData = data;
      } */
  selectedProjectCardCount(event) {
    this.selectedProjectCounts = event;
    /* if (this.selectedProjectCounts === 1) {
            this.notificationService.info("One or more records do not meet bulk edit criteria");
        } */
  }
  unselectDeliverable(event) {
    //this.selectedProjectCounts = 0;
    this.refresh();
  }
  facetToggled(event) {
    this.facetExpansionState = event;
    // this.refresh();
  }
  openCampaignInProjectList(event) {
    this.openCampaign = event;
  }
  openProjectDetails(projectId) {
    // this.stateService.setParams(this.queryParams);
    this.stateService.setRoute(
      this.queryParams.menuName,
      this.queryParams,
      this.routeParams
    );
    // tslint:disable-next-line: max-line-length
    if (
      (this.dashboardMenuConfig && this.dashboardMenuConfig.IS_CAMPAIGN_TYPE) ||
      (this.queryParams &&
        this.queryParams.viewName &&
        this.queryParams.viewName.includes('Campaigns')) ||
      this.openCampaign
    ) {
      this.routeService.goToCampaignDetailView(projectId);
    } else {
      this.routeService.goToProjectDetailView(
        projectId,
        !this.dashboardMenuConfig.IS_PROJECT_TYPE,
        this.campaignId ? this.campaignId : null
      );
    }
  }

  openTaskDetails(task) {
    const isTemplate = task.currentProjectType === 'TEMPLATE' ? true : false;
    const appConfig = this.sharingService.getAppConfig();
    // this.isTaskListView = this.utilService.getBooleanValue(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW]) ? true : appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_TASK_VIEW] === 'LIST' ? true : false;
    this.stateService.setRoute(
      this.queryParams.menuName,
      this.queryParams,
      this.routeParams
    );
    this.routeService.goToTaskDetails(
      isTemplate,
      task.currentProjectId,
      task.taskId,
      task.isNewDeliverable,
      this.campaignId ? this.campaignId : null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  editTask(task) {
    const isTemplate = !task.IS_PROJECT_TYPE;
    const appConfig = this.sharingService.getAppConfig();
    this.stateService.setRoute(
      this.queryParams.menuName,
      this.queryParams,
      this.routeParams
    );
    this.isTaskListView = this.utilService.getBooleanValue(
      appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW]
    )
      ? true
      : appConfig[
        ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_TASK_VIEW
      ] === 'LIST'
        ? true
        : false;
    this.routeService.goToTaskView(
      isTemplate,
      task.currentProjectId,
      this.isTaskListView,
      true,
      task.taskId,
      this.campaignId ? this.campaignId : null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  changeView(isListView: boolean) {
    this.dashboardMenuConfig.IS_LIST_VIEW = isListView;
    if (this.isCampaign) {
      this.routeService.goToCampaignProjectView(
        this.campaignId,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData
      );
    } else {
      this.routeService.goToDashboard(
        this.dashboardMenuConfig.selectedViewByName
          ? this.dashboardMenuConfig.selectedViewByName
          : null,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData,
        this.facetData
      );
    }
    this.updateViewDetailsBasedonViewChange(
      this.dashboardMenuConfig.projectViewConfig
    );
    if (this.enableIndexerFieldRestriction) {
      this.refresh();
    }
  }

  onSortChange(sort) {
    this.top = this.utilService.convertStringToNumber(this.pageSize);
    this.pageSize = this.utilService.convertStringToNumber(this.pageSize);
    this.skip = 0;
    this.page = 1;
    this.pageNumber = this.page;

    this.dashboardMenuConfig.selectedSortOption.sortType = sort.sortType
      .toString()
      .toLowerCase();
    this.dashboardMenuConfig.selectedSortOption.option.name = sort.option.name;
    this.dashboardMenuConfig.selectedSortOption.option.displayName =
      sort.option.displayName ||
      this.dashboardMenuConfig.projectSortOptions.find(
        (option) => option.name === sort.option.name
      ).displayName;
    this.dashboardMenuConfig.selectedSortOption.option.indexerId =
      sort.option.indexerId ||
      this.dashboardMenuConfig.projectSortOptions.find(
        (option) => option.name === sort.option.name
      ).indexerId;
    this.selectedSortOption =
      this.dashboardMenuConfig.selectedSortOption.option.displayName;
    this.selectedSortOrder =
      this.dashboardMenuConfig.selectedSortOption.sortType;
    this.loaderService.show();
    if (this.isCampaign) {
      this.routeService.goToCampaignProjectView(
        this.campaignId,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData
      );
    } else {
      this.routeService.goToDashboard(
        this.dashboardMenuConfig.selectedViewByName
          ? this.dashboardMenuConfig.selectedViewByName
          : null,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData,
        this.facetData
      );
    }
    this.handleProjectServiceCall();
    this.setCurrentView();
  }

  pagination(pagination: any) {
    this.selectedProjectCounts = 0;
    this.pageSize = pagination.pageSize;
    this.top = pagination.pageSize;
    this.skip = pagination.pageIndex * pagination.pageSize;
    this.page = 1 + pagination.pageIndex;
    this.pageNumber = this.page;
    this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_PAGE_LIMIT] = (pagination.pageSize).toString();

    this.loaderService.show();
    if (this.isCampaign) {
      this.routeService.goToCampaignProjectView(
        this.campaignId,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData
      );
    } else {
      this.routeService.goToDashboard(
        this.dashboardMenuConfig.selectedViewByName
          ? this.dashboardMenuConfig.selectedViewByName
          : null,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData,
        this.facetData
      );
    }
    this.handleProjectServiceCall();
    this.setCurrentView();
  }

  defaultPagination() {
    // if (this.isPaginationSelected) {
    //     this.top = this.pageSize;
    //     // this.pageSize = this.pageSize;
    //     this.skip = ((this.pageNumber - 1) * this.pageSize);
    //     this.page = this.pageNumber;
    // } else {
    this.top = this.utilService.convertStringToNumber(
      this.appConfig[
      ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION
      ]
    );
    this.pageSize = this.utilService.convertStringToNumber(
      this.appConfig[
      ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION
      ]
    );
    this.skip = 0;
    this.page = 1;
    this.pageNumber = this.page;
    // }
    if (this.isCampaign) {
      this.routeService.goToCampaignProjectView(
        this.campaignId,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData
      );
    } else {
      this.routeService.goToDashboard(
        this.dashboardMenuConfig.selectedViewByName
          ? this.dashboardMenuConfig.selectedViewByName
          : null,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData,
        this.facetData
      );
    }
    this.setCurrentView();
  }

  updatePagination() {
    this.skip = 0;
    this.page = 1;
    this.pageNumber = this.page;
    if (this.isCampaign) {
      this.routeService.goToCampaignProjectView(
        this.campaignId,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData
      );
    } else {
      this.routeService.goToDashboard(
        this.dashboardMenuConfig.selectedViewByName
          ? this.dashboardMenuConfig.selectedViewByName
          : null,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData,
        this.facetData
      );
    }
    this.setCurrentView();
  }

  onDashBoardFilterChange(event) {
    this.selectedProjectCounts = 0;
    sessionStorage.removeItem(
      SessionStorageConstants.STATUS_BY_CATEGORY_LEVEL_NAME
    );
    const lastProjectType = this.dashboardMenuConfig.IS_PROJECT_TYPE;
    const lastCampaignType = this.dashboardMenuConfig.IS_CAMPAIGN_TYPE;
    this.facetRestrictionList = {};
    this.facetChangeEventService.resetFacet();
    let selectedViewName;
    this.dashboardMenuConfig.projectDashBoadFilters.map((filter) => {
      if (filter.VALUE === event.currentTarget.value) {
        if (filter.PROJECT_TYPE === ProjectTypes.CAMPAIGN) {
          this.dashboardMenuConfig.IS_CAMPAIGN_TYPE = true;
          this.dashboardMenuConfig.IS_PROJECT_TYPE = false;
          selectedViewName = SearchConfigConstants.SEARCH_NAME.CAMPAIGN;
        } else if (filter.PROJECT_TYPE === ProjectTypes.PROJECT) {
          this.dashboardMenuConfig.IS_PROJECT_TYPE = true;
          this.dashboardMenuConfig.IS_CAMPAIGN_TYPE = false;
          selectedViewName = SearchConfigConstants.SEARCH_NAME.PROJECT;
        } else {
          this.dashboardMenuConfig.IS_PROJECT_TYPE = false;
          this.dashboardMenuConfig.IS_CAMPAIGN_TYPE = false;
          selectedViewName = SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE;
        }
        this.dashboardMenuConfig.selectedViewByName = filter.NAME;
        this.projectType = filter.PROJECT_TYPE;
        const type = this.dashboardMenuConfig.IS_CAMPAIGN_TYPE
          ? this.dashboardMenuConfig.IS_CAMPAIGN_TYPE
          : this.dashboardMenuConfig.IS_PROJECT_TYPE;
        /* this.dashboardMenuConfig.searchCondition = this.formFilterCondition(filter.VALUE, this.dashboardMenuConfig.IS_PROJECT_TYPE); */
        this.dashboardMenuConfig.searchCondition = this.formFilterCondition(
          filter.VALUE,
          this.dashboardMenuConfig.IS_CAMPAIGN_TYPE,
          this.dashboardMenuConfig.IS_PROJECT_TYPE,
          false
        );
      }
    });
    const selectedView =
      this.sharingService.getViewConfigByName(selectedViewName);
    const isPaginationSelected = this.setPageSetting(
      selectedView['MPM_View_Config-id'].Id
    );
    if (isPaginationSelected) {
      if (this.isCampaign) {
        this.routeService.goToCampaignProjectView(
          this.campaignId,
          this.dashboardMenuConfig.IS_LIST_VIEW,
          this.selectedSortOption,
          this.selectedSortOrder,
          this.pageNumber,
          this.pageSize,
          this.searchName,
          this.savedSearchName,
          this.advSearchData
        );
      } else {
        this.routeService.goToDashboard(
          this.dashboardMenuConfig.selectedViewByName
            ? this.dashboardMenuConfig.selectedViewByName
            : null,
          this.dashboardMenuConfig.IS_LIST_VIEW,
          this.selectedSortOption,
          this.selectedSortOrder,
          this.pageNumber,
          this.pageSize,
          this.searchName,
          this.savedSearchName,
          this.advSearchData,
          this.facetData
        );
      }
      this.setCurrentView();
    } else {
      this.updatePagination();
    }
    this.loaderService.show();
    if (this.isCampaign) {
      this.routeService.goToCampaignProjectView(
        this.campaignId,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData
      );
    } else {
      this.routeService.goToDashboard(
        this.dashboardMenuConfig.selectedViewByName
          ? this.dashboardMenuConfig.selectedViewByName
          : null,
        this.dashboardMenuConfig.IS_LIST_VIEW,
        this.selectedSortOption,
        this.selectedSortOrder,
        this.pageNumber,
        this.pageSize,
        this.searchName,
        this.savedSearchName,
        this.advSearchData,
        this.facetData
      );
    }
    this.getMPMDataFields().subscribe(
      (data) => {
        /*  if(this.dashboardMenuConfig.IS_CAMPAIGN_TYPE){
                 this.handleProjectServiceCall();
             }
             else */
        if (
          lastProjectType !== this.dashboardMenuConfig.IS_PROJECT_TYPE ||
          lastCampaignType !== this.dashboardMenuConfig.IS_CAMPAIGN_TYPE
        ) {
          this.updateSearchConfigId();
        } else {
          if (
            !(this.searchName || this.savedSearchName || this.advSearchData)
          ) {
            this.handleProjectServiceCall();
          }
        }
      },
      (mpmFieldError) => {
        this.loaderService.hide();
        this.notificationService.error(mpmFieldError);
      }
    );
  }

  onSelectionChange(event, field) {
    if (!field || !event) {
      return;
    }

    if (field.fieldName === 'newProjectType') {
      if (event.currentTarget.value === 'Blank_Project') {
        this.stateService.setRoute(
          this.queryParams.menuName,
          this.queryParams,
          this.routeParams
        );
        this.routeService.goToProjectView(
          false,
          this.campaignId ? this.campaignId : null
        );
        return;
      } else if (event.currentTarget.value === 'Project_Template') {
        this.stateService.setRoute(
          this.queryParams.menuName,
          this.queryParams,
          this.routeParams
        );
        this.routeService.goToProjectView(
          true,
          this.campaignId ? this.campaignId : null
        );
        return;
      } else if (event.currentTarget.value === 'Form_Template') {
        this.createProjectFromTemplate();
        return;
      } else if (event.currentTarget.value === 'Create_Campaign') {
        this.stateService.setRoute(
          this.queryParams.menuName,
          this.queryParams,
          this.routeParams
        );
        this.routeService.goToCampaignView();
        return;
      } else if (event.currentTarget.value === 'Bulk_Project_Creation') {
        this.createBulkProject();
        return;
      }
    }
  }

  copyProject(projectObj) {
    if (projectObj.isProjectType) {
      this.openCreateProjectForm(projectObj, 'GENERAL');
    } else {
      this.openCreateProjectForm(projectObj, 'TEMPLATE');
    }
  }

  openMilestoneDialog(projectObj) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40%';

    dialogConfig.data = projectObj.ID;

    const dialogRef = this.dialog.open(ProjectMilestoneComponent, dialogConfig);
    dialogRef.afterClosed().subscribe();
  }

  createBulkProject() {
    this.viewBulkProjectDialog('');
  }

  viewBulkProjectDialog(projectType) {
    const bulkProjectDialogConfig = new MatDialogConfig();
    let templateSearchCondition;
    let projectSearchCondition;
    this.dashboardMenuConfig.projectDashBoadFilters.forEach((filter) => {
      if (
        !(filter.PROJECT_TYPE === ProjectTypes.PROJECT) &&
        filter.VALUE === PMDashboardFilterValues.ALL_TEMPLATES
      ) {
        templateSearchCondition = this.formFilterCondition(
          filter.VALUE,
          false,
          false,
          false
        );
      }
      if (
        filter.PROJECT_TYPE === ProjectTypes.PROJECT &&
        filter.VALUE === PMDashboardFilterValues.ALL_PROJECTS
      ) {
        projectSearchCondition = this.formFilterCondition(
          filter.VALUE,
          false,
          true,
          false
        );
      }
    });
    bulkProjectDialogConfig.disableClose = true;
    bulkProjectDialogConfig.autoFocus = true;
    bulkProjectDialogConfig.width = '60%';

    bulkProjectDialogConfig.data = {
      templateSearchCondition: templateSearchCondition,
      projectSearchCondition: projectSearchCondition,
      projectType: projectType,
      templateViewConfig: this.dashboardMenuConfig.projectViewName
        ? this.dashboardMenuConfig.projectViewName
        : null,
    };

    const bulkProjRef = this.dialog.open(
      BulkProjectManagementComponent,
      bulkProjectDialogConfig
    );
    bulkProjRef.afterClosed().subscribe();
  }

  createProjectFromTemplate() {
    this.openCreateProjectForm(false, '');
  }
  openCreateProjectForm(copyProject, projectType) {
    const dialogConfig = new MatDialogConfig();
    let templateSearchCondition;
    let projectSearchCondition;
    this.dashboardMenuConfig.projectDashBoadFilters.forEach((filter) => {
      if (
        !(filter.PROJECT_TYPE === ProjectTypes.PROJECT) &&
        filter.VALUE === PMDashboardFilterValues.ALL_TEMPLATES
      ) {
        templateSearchCondition = this.formFilterCondition(
          filter.VALUE,
          false,
          false,
          false
        );
      }
      if (
        filter.PROJECT_TYPE === ProjectTypes.PROJECT &&
        filter.VALUE === PMDashboardFilterValues.ALL_PROJECTS
      ) {
        projectSearchCondition = this.formFilterCondition(
          filter.VALUE,
          false,
          true,
          false
        );
      }
      if (
        filter.PROJECT_TYPE === ProjectTypes.CAMPAIGN &&
        filter.VALUE === PMDashboardFilterValues.ALL_CAMPAIGNS
      ) {
        projectSearchCondition = this.formFilterCondition(
          filter.VALUE,
          true,
          true,
          false
        );
      }
    });

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    let isCopyProject = false;
    if (copyProject) {
      isCopyProject = true;
    }
    dialogConfig.data = {
      templateSearchCondition: templateSearchCondition,
      isCopyProject: isCopyProject,
      selectedProject: copyProject,
      projectSearchCondition: projectSearchCondition,
      projectType: projectType,
      viewConfig: this.dashboardMenuConfig.projectViewName
        ? this.dashboardMenuConfig.projectViewName
        : null,
      campaignId: this.campaignId ? this.campaignId : null,
    };

    const dialogRef = this.dialog.open(ProjectDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe();
  }

  refresh() {
    this.loaderService.show();
    this.previousCampaignRequest = null;
    this.previousProjectRequest = null;
    this.selectedProjectCounts = 0;
    this.selectedProjectData = [];
    this.projectBulkCountService.getProjectBulkCount(true);
    this.handleProjectServiceCall();
  }

  exportData() {
    this.previousCampaignRequest = null;
    this.previousProjectRequest = null;
    this.selectedProjectCounts = 0;
    let mapperNames: String = '';
    let index = 0;
    this.displayableFields.forEach((field) => {
      if (field.MAPPER_NAME.length > 0) {
        mapperNames = mapperNames.concat(field.MAPPER_NAME);
        if (index != this.columnChooserFields.length - 1) {
          mapperNames = mapperNames.concat(',');
        }
      }
      index++;
    });




    this.projectBulkCountService.getProjectBulkCount(true);
    const dialogRef = this.dialog.open(ExportDataComponent, {
      width: '40%',
      disableClose: true,
      data: {
        // exportData: this.exportTableData,
        searchRequest: this.exportDataSearchRequest,
        columns: mapperNames,
        totalCount: this.dashboardMenuConfig.totalProjectCount,
      },
    });
  }

  exportAllData() {
    const parameter = {
      filePath: this.appConfig.EXPORT_DATA_FILE_PATH + "//NPD//"
    };


    this.appService.invokeRequest(this.GET_ALL_DATA_EXPORT_NS, this.GET_ALL_DATA_EXPORT_WS, parameter).subscribe(
      (response) =>
        this.notificationService.success('All Data Export Process has been Initiated'),
      (error) => this.notificationService.error("Data Export got Failed")
    )


  }

  getCampaignById(campaignId): Observable<any> {
    return new Observable((observer) => {
      const skip = 0;
      const top = 1;

      const searchConditions = [];
      searchConditions.push(
        this.fieldConfigService.createConditionObject(
          MPMFieldConstants.CONTENT_TYPE,
          MPMSearchOperators.IS,
          MPMSearchOperatorNames.IS,
          undefined,
          OTMMMPMDataTypes.CAMPAIGN
        )
      );
      searchConditions.push({
        field_id: MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ID,
        relational_operator: MPMSearchOperators.AND,
        relational_operator_id: MPMSearchOperators.IS,
        relational_operator_name: MPMSearchOperatorNames.IS,
        type: 'string',
        value: campaignId,
      });
      const parameters: SearchRequest = {
        search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
        keyword: '',
        search_condition_list: {
          search_condition: searchConditions,
        },
        facet_condition_list: {
          facet_condition:
            this.facetRestrictionList &&
              this.facetRestrictionList.facet_condition
              ? this.facetRestrictionList.facet_condition
              : [],
        },
        sorting_list: {
          sort: [],
        },
        cursor: {
          page_index: skip,
          page_size: top,
        },
      };
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          if (response && response.data && response.data[0]) {
            this.dashboardMenuConfig.campaignData = response.data[0];
            observer.next(true);
            observer.complete();
          }
          observer.next(false);
          observer.complete();
        },
        (error) => {
          this.loaderService.hide();
          observer.error(
            'Something went wrong while getting campaign details.'
          );
        }
      );
    });
  }

  getAllProjects(isCursorRequired: boolean): Observable<any> {
    return new Observable((observer) => {
      if (isCursorRequired) {
        this.dashboardMenuConfig.projectData = [];
      }
      let keyWord = '';
      let sortTemp = [];
      let searchFields = [];
      let requiredDisplayableFields = this.dashboardMenuConfig.IS_LIST_VIEW
        ? this.displayableFields
        : this.dashboardMenuConfig.projectViewConfig.R_PM_CARD_VIEW_MPM_FIELDS;
      if (this.enableIndexerFieldRestriction) {
        requiredDisplayableFields.forEach((field) => {
          let indexerDisplayFields: IndexerDisplayFields;
          searchFields.push(
            (indexerDisplayFields = {
              field_id: field.INDEXER_FIELD_ID,
            })
          );
        });
        searchFields.push({ field_id: 'NPD_PROJECT_IS_BULK_EDIT' })
        searchFields.push({ field_id: 'NPD_PROJECT_IS_TASK4_COMPLETED' })
        searchFields.push({ field_id: 'NPD_PROJECT_IS_TASK5_COMPLETED' })
        searchFields.push({ field_id: 'NPD_PROJECT_IS_TASK39_EXIST' })
        searchFields.push({ field_id: 'NPD_PROJECT_IS_TASK42_EXIST' })
        searchFields.push({ field_id: 'NPD_PROJECT_IS_FG_PROJECT' })
        searchFields.push({
          field_id: 'NPD_PROJECT_IS_TASK_34_EXIST_OR_INACTIVE',
        });
        searchFields.push({
          field_id: 'NPD_PROJECT_LEAD_MARKET',
        });
      }
      if (this.dashboardMenuConfig.selectedSortOption) {
        let sortOption = this.dashboardMenuConfig.projectSortOptions.find(
          (option) =>
            option.name ===
            this.dashboardMenuConfig.selectedSortOption.option.name
        );
        if (sortOption === undefined) {
          const dataSet = this.fieldConfigService.formrequiredDataForLoadView(
            this.dashboardMenuConfig.projectViewConfig,
            !this.dashboardMenuConfig.IS_LIST_VIEW
          );
          sortOption = dataSet.SORTABLE_FIELDS.find(
            (option) =>
              option.name ===
              this.dashboardMenuConfig.selectedSortOption.option.name
          );
        }
        sortTemp = sortOption
          ? [
            {
              field_id: sortOption.indexerId,
              order: this.dashboardMenuConfig.selectedSortOption.sortType,
            },
          ]
          : [];
      }
      let searchConditions = this.dashboardMenuConfig.searchCondition;

      if (
        this.searchConditions &&
        this.searchConditions.length > 0 &&
        this.searchConditions[0].metadata_field_id ===
        this.searchDataService.KEYWORD_MAPPER
      ) {
        keyWord = this.searchConditions[0].keyword;
      } else if (this.searchConditions && this.searchConditions.length > 0) {
        searchConditions = searchConditions.concat(this.searchConditions);
      }

      let parameters: SearchRequest = null;

      // if (JSON.parse(sessionStorage.getItem('SELECTED_TASK_FOR_ROUTE')) !== null || this.facetConditionValue.length > 0) {
      //   let selectedTask = JSON.parse(sessionStorage.getItem('SELECTED_TASK_FOR_ROUTE'));
      //   if (this.facetConditionValue.length === 0) {
      //     for (const task of selectedTask) {
      //       this.facetConditionValue.push(task.NPD_PROJECT_BUSINESS_ID);
      //     }
      //   }
      //   parameters = {
      //     search_config_id:
      //       this.searchConfigId > 0 ? this.searchConfigId : null,
      //     keyword: keyWord,
      //     search_condition_list: {
      //       search_condition: searchConditions,
      //     },
      //     displayable_field_list: {
      //       displayable_fields: searchConditions,
      //     },
      //     facet_condition_list: {
      //       facet_condition:
      //       this.facetRestrictionList &&
      //         this.facetRestrictionList.facet_condition
      //         ? this.facetRestrictionList.facet_condition
      //         : [],
      //     },
      //     sorting_list: {
      //       sort: sortTemp,
      //     },
      //     cursor: {
      //       page_index: this.skip,
      //       page_size: this.top,
      //     },
      //   };
      //   sessionStorage.removeItem('SELECTED_TASK_FOR_ROUTE');
      // }
      // else {
      if (!isCursorRequired) {
        parameters = {
          search_config_id:
            this.searchConfigId > 0 ? this.searchConfigId : null,
          keyword: keyWord,
          search_condition_list: {
            search_condition: searchConditions,
          },
          displayable_field_list: {
            displayable_fields: searchFields,
          },
          facet_condition_list: {
            facet_condition:
              this.facetRestrictionList &&
                this.facetRestrictionList.facet_condition
                ? this.facetRestrictionList.facet_condition
                : [],
          },
          sorting_list: {
            sort: sortTemp,
          },
          cursor: {
            page_index: this.skip,
            page_size: this.dashboardMenuConfig.totalProjectCount,
          },
        };
      } else {
        parameters = {
          search_config_id:
            this.searchConfigId > 0 ? this.searchConfigId : null,
          keyword: keyWord,
          search_condition_list: {
            search_condition: searchConditions,
          },
          displayable_field_list: {
            displayable_fields: searchFields,
          },
          facet_condition_list: {
            facet_condition:
              this.facetRestrictionList &&
                this.facetRestrictionList.facet_condition
                ? this.facetRestrictionList.facet_condition
                : [],
          },
          sorting_list: {
            sort: sortTemp,
          },
          cursor: {
            page_index: this.skip,
            page_size: this.top,
          },
        };
      }
      // }


      /*if (JSON.stringify(this.previousProjectRequest) === JSON.stringify(parameters)) {
                const data = [];
                    this.previousProjectResponse.data.forEach(element => {
                        if (element.IS_BULK_UPDATE_PROJECT !== 'true') {
                            data.push(element);
                        }
                    });
       
                    if (isCursorRequired) {
                        this.dashboardMenuConfig.projectData = data;
                        this.dashboardMenuConfig.totalProjectCount = this.previousProjectResponse.cursor.total_records;
                        this.facetChangeEventService.updateFacetFilter(this.previousProjectResponse.facet_field_response_list ? this.previousProjectResponse.facet_field_response_list : []);
                    }
                    this.exportTableData = data;
                observer.next(true);
                observer.complete();
            } else {*/
      this.previousProjectRequest = parameters;
      this.previousCampaignRequest = null;
      this.exportDataSearchRequest = parameters;
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          if (response.data.length === 0 && this.skip > 0) {
            this.skip = 0;
            this.page = 1;
            this.pageNumber = this.page;
            this.notificationService.info(
              'As there is no data in current page, redirecting to first page.'
            );
            this.loaderService.show();
            if (this.isCampaign) {
              this.routeService.goToCampaignProjectView(
                this.campaignId,
                this.dashboardMenuConfig.IS_LIST_VIEW,
                this.selectedSortOption,
                this.selectedSortOrder,
                this.pageNumber,
                this.pageSize,
                this.searchName,
                this.savedSearchName,
                this.advSearchData
              );
            } else {
              this.routeService.goToDashboard(
                this.dashboardMenuConfig.selectedViewByName
                  ? this.dashboardMenuConfig.selectedViewByName
                  : null,
                this.dashboardMenuConfig.IS_LIST_VIEW,
                this.selectedSortOption,
                this.selectedSortOrder,
                this.pageNumber,
                this.pageSize,
                this.searchName,
                this.savedSearchName,
                this.advSearchData,
                this.facetData
              );
            }
            this.setCurrentView();
            this.getAllProjects(true).subscribe((projectResponse) => {
              this.loaderService.hide();
            });
            observer.next(false);
            observer.complete();
          } else {
            this.previousProjectResponse = response;
            const data = [];
            response.data.forEach((element) => {
              if (element.IS_BULK_UPDATE_PROJECT !== 'true') {
                data.push(element);
              }
            });

            if (isCursorRequired) {
              this.dashboardMenuConfig.projectData = data;
              this.dashboardMenuConfig.totalProjectCount =
                response.cursor.total_records;
              this.facetService.updateOrderdDisplayableFields(this.displayableFields);
              this.facetChangeEventService.updateFacetFilter(
                response.facet_field_response_list
                  ? response.facet_field_response_list
                  : []
              );
              // this.facetService.updateOrderdDisplayableFields(searchFields);

            } else {
              this.exportTableData = data;
            }
            observer.next(true);
            observer.complete();
          }
        },
        (error) => {
          this.loaderService.hide();
          observer.error('Something went wrong while getting projects.');
        }
      );
      // }
    });
  }

  getAllCampaign(isCursorRequired: boolean): Observable<any> {
    return new Observable((observer) => {
      if (isCursorRequired) {
        this.dashboardMenuConfig.projectData = [];
      }
      let keyWord = '';
      let sortTemp = [];
      if (this.dashboardMenuConfig.selectedSortOption) {
        const sortOption = this.dashboardMenuConfig.projectSortOptions.find(
          (option) =>
            option.name ===
            this.dashboardMenuConfig.selectedSortOption.option.name
        );
        sortTemp = sortOption
          ? [
            {
              field_id: sortOption.indexerId,
              order: this.dashboardMenuConfig.selectedSortOption.sortType,
            },
          ]
          : [];
      }
      let searchConditions = this.dashboardMenuConfig.searchCondition;
      let searchFields = [];
      if (this.enableIndexerFieldRestriction) {
        let requiredDisplayableFields = this.dashboardMenuConfig.IS_LIST_VIEW
          ? this.displayableFields
          : this.dashboardMenuConfig.projectViewConfig
            .R_PM_CARD_VIEW_MPM_FIELDS;
        requiredDisplayableFields.forEach((field) => {
          let indexerDisplayFields: IndexerDisplayFields;
          searchFields.push(
            (indexerDisplayFields = { field_id: field.INDEXER_FIELD_ID })
          );
        });
      }
      if (
        this.searchConditions &&
        this.searchConditions.length > 0 &&
        this.searchConditions[0].metadata_field_id ===
        this.searchDataService.KEYWORD_MAPPER
      ) {
        keyWord = this.searchConditions[0].keyword;
      } else if (this.searchConditions && this.searchConditions.length > 0) {
        searchConditions = searchConditions.concat(this.searchConditions);
      }
      let parameters: SearchRequest = null;
      if (!isCursorRequired) {
        parameters = {
          search_config_id:
            this.searchConfigId > 0 ? this.searchConfigId : null,
          keyword: keyWord,
          search_condition_list: {
            search_condition: searchConditions,
          },
          displayable_field_list: {
            displayable_fields: searchFields,
          },
          facet_condition_list: {
            facet_condition:
              this.facetRestrictionList &&
                this.facetRestrictionList.facet_condition
                ? this.facetRestrictionList.facet_condition
                : [],
          },
          sorting_list: {
            sort: sortTemp,
          },
          cursor: {
            page_index: this.skip,
            page_size: this.dashboardMenuConfig.totalProjectCount,
          },
        };
      } else {
        parameters = {
          search_config_id:
            this.searchConfigId > 0 ? this.searchConfigId : null,
          keyword: keyWord,
          search_condition_list: {
            search_condition: searchConditions,
          },
          displayable_field_list: {
            displayable_fields: searchFields,
          },
          facet_condition_list: {
            facet_condition:
              this.facetRestrictionList &&
                this.facetRestrictionList.facet_condition
                ? this.facetRestrictionList.facet_condition
                : [],
          },
          sorting_list: {
            sort: sortTemp,
          },
          cursor: {
            page_index: this.skip,
            page_size: this.top,
          },
        };
      }

      /*if (JSON.stringify(this.previousCampaignRequest) === JSON.stringify(parameters)) {
                const data = [];
                    this.previousCampaignResponse.data.forEach(element => {
                        if (element.IS_BULK_UPDATE_PROJECT !== 'true') {
                            data.push(element);
                        }
                    });
                    if (isCursorRequired) {
                        this.dashboardMenuConfig.projectData = data;
                        this.dashboardMenuConfig.totalProjectCount = this.previousCampaignResponse.cursor.total_records;
                        this.facetChangeEventService.updateFacetFilter(this.previousCampaignResponse.facet_field_response_list ? this.previousCampaignResponse.facet_field_response_list : []);
                    }
                    this.exportTableData = data;
                observer.next(true);
                observer.complete();
            } else {*/
      this.previousCampaignRequest = parameters;
      this.previousProjectRequest = null;
      this.exportDataSearchRequest = parameters;
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          if (response.data.length === 0 && this.skip > 0) {
            this.skip = 0;
            this.page = 1;
            this.pageNumber = this.page;
            this.notificationService.info(
              'As there is no data in current page, redirecting to first page.'
            );
            this.loaderService.show();
            if (this.isCampaign) {
              this.routeService.goToCampaignProjectView(
                this.campaignId,
                this.dashboardMenuConfig.IS_LIST_VIEW,
                this.selectedSortOption,
                this.selectedSortOrder,
                this.pageNumber,
                this.pageSize,
                this.searchName,
                this.savedSearchName,
                this.advSearchData
              );
            } else {
              this.routeService.goToDashboard(
                this.dashboardMenuConfig.selectedViewByName
                  ? this.dashboardMenuConfig.selectedViewByName
                  : null,
                this.dashboardMenuConfig.IS_LIST_VIEW,
                this.selectedSortOption,
                this.selectedSortOrder,
                this.pageNumber,
                this.pageSize,
                this.searchName,
                this.savedSearchName,
                this.advSearchData,
                this.facetData
              );
            }
            this.setCurrentView();
            this.getAllCampaign(true).subscribe((projectResponse) => {
              this.loaderService.hide();
            });
            observer.next(false);
            observer.complete();
          } else {
            this.previousCampaignResponse = response;
            const data = [];
            response.data.forEach((element) => {
              if (element.IS_BULK_UPDATE_PROJECT !== 'true') {
                data.push(element);
              }
            });
            if (isCursorRequired) {
              this.dashboardMenuConfig.projectData = data;
              this.dashboardMenuConfig.totalProjectCount =
                response.cursor.total_records;
              this.facetChangeEventService.updateFacetFilter(
                response.facet_field_response_list
                  ? response.facet_field_response_list
                  : []
              );
              // this.facetService.updateOrderdDisplayableFields(searchFields);

            } else {
              this.exportTableData = data;
            }
            observer.next(true);
            observer.complete();
          }
        },
        (error) => {
          this.loaderService.hide();
          observer.error('Something went wrong while getting campaigns.');
        }
      );
      //}
    });
  }

  resetView() {

    sessionStorage.removeItem(
      'USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW
    );
    const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(
      this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id
    );
    const userPreferenceId =
      userPreference &&
        userPreference['MPM_User_Preference-id'] &&
        userPreference['MPM_User_Preference-id'].Id
        ? userPreference['MPM_User_Preference-id'].Id
        : null;
    if (userPreferenceId) {
      this.loaderService.show();
      // this.appService
      //   .deleteUserPreference(userPreferenceId)
      //   .subscribe((response) => {
      this.loaderService.hide();
      //     if (response) {
      //       this.notificationService.success(
      //         'User preference have been reset successfully'
      //       );
      //     }
      //   });
    } else {
      this.notificationService.success(
        'User preference have been reset successfully');
      this.loaderService.hide();
    }

    this.resetCurrentView();
  }

  resetCurrentView() {
    this.dashboardMenuConfig.projectViewConfig.displayFields.forEach(
      (displayField) => {
        displayField.width = '';
      }
    );
    this.dashboardMenuConfig.projectViewConfig.listFieldOrder =
      this.dashboardMenuConfig.projectViewConfig.LIST_FIELD_ORDER;
    this.displayableFields =
      this.dashboardMenuConfig.projectViewConfig.displayFields;
    if (this.enableIndexerFieldRestriction) {
      this.displayableFields = this.displayableFields.slice(
        0,
        this.maximumFieldsAllowed
      );
    }
    this.userPreferenceFreezeCount =
      MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT;
    this.columnChooserFields = this.formColumnChooserField(
      this.dashboardMenuConfig.projectViewConfig.displayFields
    );
    let userPreferenceData = this.formViewData();
    let DefaultField = JSON.parse(userPreferenceData)
    let currentViewData: any = sessionStorage.getItem('CURRENT_VIEW');
    currentViewData = JSON.parse(currentViewData);
    // let requestObj;
    let fieldData;
    //   if (currentViewData.isListView) {
    //     let userPreferenceData: any = sessionStorage.getItem('CURRENT_USER_PREFERENCE-' + currentViewData.viewName);
    //     userPreferenceData = userPreferenceData ? JSON.parse(userPreferenceData) : null;
    //     const userPreferenceViewField = userPreferenceData && userPreferenceData.FIELD_DATA ? userPreferenceData.FIELD_DATA : null;
    //     userPreferenceViewField.forEach(field => {

    //         const fieldId = Object.keys(field)[0];
    //         if (fieldId === currentViewData.sortName) {
    //             field[fieldId].IS_SORTABLE = true;
    //             field[fieldId].SORT_ORDER = currentViewData.sortOrder
    //         }
    //         else {
    //             field[fieldId].IS_SORTABLE = false;
    //             field[fieldId].SORT_ORDER = null
    //         }
    //     });
    //     if (currentViewData.viewName.includes("MEMBER")) {
    //         fieldData = {
    //             data: userPreferenceViewField,
    //             pageSize: currentViewData.pageSize,
    //             pageNumber: currentViewData.pageNumber,
    //             groupBy: currentViewData.groupBy,
    //             taskStatusFilter: currentViewData.taskStatusFilter,
    //             FREEZE_COLUMN_COUNT:userPreferenceData.FREEZE_COLUMN_COUNT
    //         }
    //     }
    //     else {
    //         fieldData = {
    //             data: userPreferenceViewField,
    //             pageSize: currentViewData.pageSize,
    //             pageNumber: currentViewData.pageNumber,
    //             FREEZE_COLUMN_COUNT: userPreferenceData.FREEZE_COLUMN_COUNT
    //         }
    //     }
    // } else {
    //     let userPreferenceViewField = [];
    //     if (currentViewData.sortName) {
    //         userPreferenceViewField = [{
    //             [currentViewData.sortName]: {
    //                 IS_SORTABLE: true,
    //                 SORT_ORDER: currentViewData.sortOrder
    //             }
    //         }];
    //     }
    //     if (currentViewData.viewName.includes("MEMBER")) {
    //         fieldData = {
    //             data: userPreferenceViewField,
    //             pageSize: currentViewData.pageSize,
    //             pageNumber: currentViewData.pageNumber,
    //             groupBy: currentViewData.groupBy,
    //             taskStatusFilter: currentViewData.taskStatusFilter,
    //             FREEZE_COLUMN_COUNT: MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
    //         }
    //     }
    //     else {
    //         fieldData = {
    //             data: userPreferenceViewField,
    //             pageSize: currentViewData.pageSize,
    //             pageNumber: currentViewData.pageNumber,
    //             FREEZE_COLUMN_COUNT: MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
    //         }
    //     }

    // }

    fieldData = {
      data: DefaultField.FIELD_DATA,
      pageSize: currentViewData.pageSize,
      pageNumber: currentViewData.pageNumber,
      FREEZE_COLUMN_COUNT: DefaultField.FREEZE_COLUMN_COUNT
    }

    let defaultUserPreference = sessionStorage.getItem('DEFAULT_USER_PREFERENCE-PM_PROJECT_VIEW')
    let activeUserPreference = sessionStorage.getItem('ACTIVE_USER_PREFERENCE_ID')
    const currentPreferenceId = activeUserPreference ? activeUserPreference : defaultUserPreference
    let IsActiveUserPreference

    if (defaultUserPreference && (!activeUserPreference)) {
      IsActiveUserPreference = true
    }
    else if (defaultUserPreference == activeUserPreference) {
      IsActiveUserPreference = true
    }
    else {
      IsActiveUserPreference = false
    }
    let requestObj = {
      PersonId: this.sharingService.getCurrentPerson().Id,
      PreferenceType: 'VIEW',
      IsListView: currentViewData.isListView,
      IsDefaultView: true,
      ViewId: currentViewData.viewId,
      MenuId: this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id,
      FieldData: JSON.stringify(fieldData),
      UserPreferenceId: currentPreferenceId,
      FilterName: currentViewData.listfilter,
      IsActiveUserPreference: IsActiveUserPreference
    };
    this.appService.invokeRequest('http://schemas.acheron.com/mpm/core/workflow/bpm/1.0', 'UpdateIsActiveUserPreference', requestObj).subscribe(response => {
      if (response) {
        this.loaderService.hide();

      }
    })
    this.activePreferenceName = sessionStorage.getItem('ACTIVE_USER_PREFERENCE_NAME').replace('*', '')
    sessionStorage.setItem('ACTIVE_USER_PREFERENCE_NAME', this.activePreferenceName);
    this.defaultViewFilterName = null;

    // this.ngOnInit();


    this.setCurrentViewData(userPreferenceData);
    sessionStorage.setItem('USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW, userPreferenceData)
    this.ngOnInit();
  }

  setViewData(userPreference) {
    sessionStorage.setItem('USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW, userPreference)
  }

  setCurrentViewData(userPreference) {
    sessionStorage.setItem('CURRENT_USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW, userPreference);
  }

  formViewData() {
    const userPreference = {
      USER_ID: this.sharingService.getCurrentPerson().Id,
      PREFERENCE_TYPE: 'VIEW',
      VIEW_ID:
        this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id,
      FIELD_DATA: [],
      FREEZE_COLUMN_COUNT: 0,
    };

    this.dashboardMenuConfig.projectViewConfig.displayFields.forEach(
      (viewField) => {
        if (viewField.INDEXER_FIELD_ID) {
          const fieldIndex = this.displayableFields.findIndex(
            (displayableField) =>
              displayableField.INDEXER_FIELD_ID === viewField.INDEXER_FIELD_ID
          );
          userPreference.FIELD_DATA.push({
            [viewField.INDEXER_FIELD_ID]: {
              POSITION: fieldIndex >= 0 ? fieldIndex : null,
              WIDTH: viewField && viewField.width ? viewField.width : '',
              IS_DISPLAY: fieldIndex >= 0,
            },
          });
        }
      }
    );
    if (this.userPreferenceFreezeCount === undefined) {
      userPreference.FREEZE_COLUMN_COUNT =
        MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT;
    }
    userPreference.FREEZE_COLUMN_COUNT = this.userPreferenceFreezeCount;
    return JSON.stringify(userPreference);
  }

  updateDisplayColumns(displayColumnFields) {
    this.activePreferenceName = sessionStorage.getItem('ACTIVE_USER_PREFERENCE_NAME')?.replace('*', '') + '*'
    sessionStorage.setItem('ACTIVE_USER_PREFERENCE_NAME', this.activePreferenceName)
    this.displayableFields = [];
    const listFieldOrder = [];

    displayColumnFields.column.sort((a, b) => a.POSITION - b.POSITION);
    displayColumnFields.column.forEach((displayColumnField) => {
      if (displayColumnField.selected === true) {
        const selectedField =
          this.dashboardMenuConfig.projectViewConfig.displayFields.find(
            (field) =>
              field.INDEXER_FIELD_ID === displayColumnField.INDEXER_FIELD_ID
          );
        if (selectedField) {
          this.displayableFields.push(selectedField);
          listFieldOrder.push(selectedField.MAPPER_NAME);
        }
      }
    });
    // this.facetService.updateOrderdDisplayableFields(this.displayableFields);
    //    this.dashboardMenuConfig.projectViewConfig.freezeCount= displayColumnFields.frezeCount
    this.columnChooserFields.sort((a, b) => a.DISPLAY_NAME.localeCompare(b.DISPLAY_NAME));
    this.userPreferenceFreezeCount = displayColumnFields.frezeCount;
    this.dashboardMenuConfig.projectViewConfig.freezeCount = displayColumnFields.frezeCount;
    this.dashboardMenuConfig.projectViewConfig.listFieldOrder =
      listFieldOrder.toString();
    const userPreferenceData = this.formViewData();
    this.setViewData(userPreferenceData);
    this.setCurrentViewData(userPreferenceData);
    if (this.enableIndexerFieldRestriction) {
      this.refresh();
    }
  }

  /**
   * 
   * @param listFields 
   * @description forms the listFields object in required format to send them to column chooser component...
   */
  formColumnChooserField(listFields) {
    const columnChooserFields = [];
    this.dashboardMenuConfig.projectViewConfig.displayFields.forEach(
      (displayField, index) => {
        const selectedFieldIndex = listFields.findIndex(
          (listField) =>
            listField.INDEXER_FIELD_ID === displayField.INDEXER_FIELD_ID
        );
        let currentField;
        if (this.sectionFields.length > 0) {
          currentField = this.sectionFields.find(ele => ele?.FieldIndexerId == displayField.INDEXER_FIELD_ID);
        }

        if (this.enableIndexerFieldRestriction) {
          if (
            selectedFieldIndex >= 0 &&
            selectedFieldIndex < this.maximumFieldsAllowed
          ) {
            columnChooserFields.push({
              INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
              DISPLAY_NAME: displayField.DISPLAY_NAME,
              POSITION: selectedFieldIndex,
              selected: true,
              SECTION_NAME: currentField?.SectionName?.length > 0 ? currentField?.SectionName : "",
              SUB_SECTION_NAME: currentField?.SubSectionName?.length > 0 ? currentField?.SubSectionName : ""
            });
          } else {
            columnChooserFields.push({
              INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
              DISPLAY_NAME: displayField.DISPLAY_NAME,
              POSITION: index,
              selected: false,
              SECTION_NAME: currentField?.SectionName?.length > 0 ? currentField?.SectionName : "",
              SUB_SECTION_NAME: currentField?.SubSectionName?.length > 0 ? currentField?.SubSectionName : ""
            });
          }
        } else {
          if (selectedFieldIndex >= 0) {
            columnChooserFields.push({
              INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
              DISPLAY_NAME: displayField.DISPLAY_NAME,
              POSITION: selectedFieldIndex,
              selected: true,
              SECTION_NAME: currentField?.SectionName?.length > 0 ? currentField?.SectionName : "",
              SUB_SECTION_NAME: currentField?.SubSectionName?.length > 0 ? currentField?.SubSectionName : ""
            });
          } else {
            columnChooserFields.push({
              INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
              DISPLAY_NAME: displayField.DISPLAY_NAME,
              POSITION: index,
              selected: false,
              SECTION_NAME: currentField?.SectionName?.length > 0 ? currentField?.SectionName : "",
              SUB_SECTION_NAME: currentField?.SubSectionName?.length > 0 ? currentField?.SubSectionName : ""
            });
          }
        }
      }
    );
    columnChooserFields.sort((a, b) => a.DISPLAY_NAME.localeCompare(b.DISPLAY_NAME));
    // let uniqueColumnchooserFields = [];
    // columnChooserFields.forEach(field => {
    //   const fieldIndex = uniqueColumnchooserFields.indexOf(eachField => eachField.INDEXER_FIELD_ID == field.INDEXER_FIELD_ID)
    //   if(fieldIndex < 0){
    //     uniqueColumnchooserFields.push(field);
    //   }
    // })
    return columnChooserFields;
  }

  getOrderedDisplayableFields(displayableFields) {
    // this.changedPref=true
    // this.activePreferenceName= sessionStorage.getItem('ACTIVE_USER_PREFERENCE_NAME')+'*'
    // sessionStorage.setItem('ACTIVE_USER_PREFERENCE_NAME',this.activePreferenceName.replace('*','')+'*')
    this.facetService.updateOrderdDisplayableFields(displayableFields);
    this.columnChooserFields = this.formColumnChooserField(displayableFields);
    this.updateDisplayColumns({
      column: this.columnChooserFields,
      frezeCount: this.userPreferenceFreezeCount,
    });
  }

  resizeDisplayableFields() {
    const userPreferenceData = this.formViewData();
    this.setViewData(userPreferenceData);
    this.setCurrentViewData(userPreferenceData);
  }

  setUserPreference() {
    let userPreferenceSessionData: any = sessionStorage.getItem(
      'USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW
    );
    userPreferenceSessionData = userPreferenceSessionData
      ? JSON.parse(userPreferenceSessionData)
      : null;
    const userPreferenceViewField =
      userPreferenceSessionData && userPreferenceSessionData.FIELD_DATA
        ? userPreferenceSessionData.FIELD_DATA
        : null;
    if (userPreferenceViewField) {
      userPreferenceViewField.sort((a, b) => {
        const firstKey: any = Object.keys(a);
        const firstValue = a[firstKey];
        const secondKey: any = Object.keys(b);
        const secondValue = b[secondKey];

        return firstValue.POSITION - secondValue.POSITION;
      });
      const displayableFields = [];
      const listFieldOrder = [];
      userPreferenceViewField.forEach((viewField) => {
        const viewFieldId = Object.keys(viewField)[0];
        if (viewField[viewFieldId].IS_DISPLAY === true) {
          const selectedField =
            this.dashboardMenuConfig.projectViewConfig.displayFields.find(
              (field) => field.INDEXER_FIELD_ID === viewFieldId
            );
          if (selectedField) {
            selectedField.width = viewField[viewFieldId].WIDTH;
            displayableFields.push(selectedField);
            listFieldOrder.push(selectedField.MAPPER_NAME);
          }
        }
      });
      this.dashboardMenuConfig.projectViewConfig.listFieldOrder =
        listFieldOrder.toString();
      this.displayableFields = displayableFields;
      this.facetService.updateOrderdDisplayableFields(this.displayableFields);
      this.columnChooserFields = this.formColumnChooserField(displayableFields);
      this.userPreferenceFreezeCount =
        userPreferenceSessionData.FREEZE_COLUMN_COUNT;
    } else {
      // const userPreference =
      //   this.sharingService.getCurrentUserPreferenceByViewId(
      //     this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id
      //   );


      const getCurrentUserPreference = this.sharingService.getCurrentUserPreference();
      let IsActiveUserPreference
      getCurrentUserPreference.forEach(Prefer => {
        if (Prefer.IS_ACTIVE_USER_PREFERENCE == 'true') {
          IsActiveUserPreference = Prefer
        }
      })

      const userPreference = IsActiveUserPreference
      const currentViewFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;

      this.dashboardMenuConfig.projectViewConfig.freezeCount =
        this.userPreferenceFreezeCount;
      if (
        currentViewFieldData != null &&
        this.utilService.getBooleanValue(userPreference.IS_LIST_VIEW)
      ) {
        this.userPreferenceFreezeCount = currentViewFieldData.FREEZE_COLUMN_COUNT;
        currentViewFieldData.data.sort((a, b) => {
          const firstKey: any = Object.keys(a);
          const firstValue = a[firstKey];
          const secondKey: any = Object.keys(b);
          const secondValue = b[secondKey];
          return firstValue.POSITION - secondValue.POSITION;
        });
        const displayableFields = [];
        const listFieldOrder = [];

        currentViewFieldData.data.forEach((viewField) => {
          const viewFieldId = Object.keys(viewField)[0];
          if (viewField[viewFieldId].IS_DISPLAY === true) {
            const selectedField =
              this.dashboardMenuConfig.projectViewConfig.displayFields.find(
                (field) => field.INDEXER_FIELD_ID === viewFieldId
              );
            if (selectedField) {
              selectedField.width = viewField[viewFieldId].WIDTH;
              displayableFields.push(selectedField);
              listFieldOrder.push(selectedField.MAPPER_NAME);
            }
          }
        });
        this.dashboardMenuConfig.projectViewConfig.listFieldOrder =
          listFieldOrder.toString();
        this.displayableFields = displayableFields;
        this.columnChooserFields =
          this.formColumnChooserField(displayableFields);
      } else {
        this.dashboardMenuConfig.projectViewConfig.listFieldOrder =
          this.dashboardMenuConfig.projectViewConfig.LIST_FIELD_ORDER;
        this.displayableFields =
          this.dashboardMenuConfig.projectViewConfig.displayFields;
        this.columnChooserFields = this.formColumnChooserField(
          this.dashboardMenuConfig.projectViewConfig.displayFields);
        this.userPreferenceFreezeCount =
          MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT;
      }
    }
    if (this.enableIndexerFieldRestriction) {
      this.displayableFields = this.displayableFields.slice(
        0,
        this.maximumFieldsAllowed
      );
    }
  }

  setCurrentView() {
    const currentViewData = {
      sortName: this.dashboardMenuConfig.selectedSortOption.option.indexerId,
      sortOrder: this.dashboardMenuConfig.selectedSortOption.sortType,
      pageNumber: this.pageNumber,
      pageSize: this.pageSize,
      isListView: this.dashboardMenuConfig.IS_LIST_VIEW,
      viewId:
        this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id,
      viewName: this.dashboardMenuConfig.projectViewConfig.VIEW,
      listfilter: this.dashboardMenuConfig.selectedViewByName,
      isPMView: true,
    };
    sessionStorage.setItem('CURRENT_VIEW', JSON.stringify(currentViewData));
  }

  setDefaultView() {
    const menu = this.sharingService.getCurrentMenu();
    const defaultView =
      this.sharingService.getDefaultCurrentUserPreferenceByMenu(
        menu['MPM_Menu-id'].Id
      );
    if (defaultView || this.defaultViewFilterName) {
      if (
        defaultView.FILTER_NAME !==
        this.dashboardMenuConfig.selectedViewByName ||
        this.defaultViewFilterName !==
        this.dashboardMenuConfig.selectedViewByName
      ) {
        const dialogRef = this.dialog.open(ConfirmationModalComponent, {
          width: '40%',
          disableClose: true,
          data: {
            message:
              'Are you sure you want to override default view?<br /> <b>Current Default View: </b>' +
              defaultView.FILTER_NAME,
            submitButton: 'Yes',
            cancelButton: 'No',
          },
        });
        dialogRef.afterClosed().subscribe((result) => {
          if (result && result.isTrue) {
            this.appService
              .updateDefaultUserPreference(
                this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id']
                  .Id,
                this.dashboardMenuConfig.selectedViewByName,
                menu['MPM_Menu-id'].Id,
                null
              )
              .subscribe((response) => {
                if (response) {
                  this.notificationService.success(
                    'Default view has been saved successfully'
                  );
                  this.defaultViewFilterName =
                    this.dashboardMenuConfig.selectedViewByName;
                } else {
                  this.notificationService.error(
                    'Something went wrong while updating default view'
                  );
                }
              });
          }
        });
      }
    } else {
      this.appService
        .updateDefaultUserPreference(
          this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id,
          this.dashboardMenuConfig.selectedViewByName,
          menu['MPM_Menu-id'].Id,
          null
        )
        .subscribe((response) => {
          if (response) {
            this.notificationService.success(
              'Default view has been saved successfully'
            );
            this.defaultViewFilterName =
              this.dashboardMenuConfig.selectedViewByName;
          } else {
            this.notificationService.error(
              'Something went wrong while updating default view'
            );
          }
        });
    }
  }

  setPageSetting(viewId?) {
    const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(
      viewId
        ? viewId
        : this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id
    );
    const userPreferenceFieldData =
      userPreference &&
        !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA)
        ? JSON.parse(userPreference.FIELD_DATA)
        : null;
    if (userPreferenceFieldData) {
      this.pageSize = userPreferenceFieldData.pageSize;
      this.top = this.pageSize;
      this.pageNumber = userPreferenceFieldData.pageNumber;
      this.page = this.pageNumber;
      this.skip = (this.pageNumber - 1) * this.pageSize;
      this.isdefaultPagination = false;
      return true;
    }
    return false;
  }

  updateViewDetailsBasedonViewChange(viewDetails: ViewConfig) {
    if (
      !this.dashboardMenuConfig.projectViewConfig ||
      viewDetails.VIEW !== this.dashboardMenuConfig.projectViewConfig.VIEW
    ) {
      this.dashboardMenuConfig.projectViewConfig = viewDetails;
      this.updateSearchConfigId();
    }

    const getCurrentUserPreference = this.sharingService.getCurrentUserPreference();
    let IsActiveUserPreference
    getCurrentUserPreference.forEach(Prefer => {
      if (Prefer.IS_ACTIVE_USER_PREFERENCE == 'true') {
        IsActiveUserPreference = Prefer
      }
    })

    const userPreference = IsActiveUserPreference
    const userPreferenceFieldData =
      userPreference &&
        !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA)
        ? JSON.parse(userPreference.FIELD_DATA)
        : null;
    if (
      userPreference &&
      this.isListView === null &&
      !this.utilService.isNullOrEmpty(userPreference.IS_LIST_VIEW) &&
      !this.disableGridView
    ) {
      this.dashboardMenuConfig.IS_LIST_VIEW = this.utilService.getBooleanValue(
        userPreference.IS_LIST_VIEW
      );
    }
    this.dashboardMenuConfig.projectViewConfig = viewDetails;
    // this.dashboardMenuConfig.projectViewConfig.displayFields = this.dashboardMenuConfig.IS_LIST_VIEW ? viewDetails.R_PM_LIST_VIEW_MPM_FIELDS : viewDetails.R_PM_CARD_VIEW_MPM_FIELDS;
    this.dashboardMenuConfig.projectViewConfig.displayFields =
      viewDetails.R_PM_LIST_VIEW_MPM_FIELDS;
    //MPM_V3 -1799
    const level = viewDetails.VIEW.includes('CAMPAIGN')
      ? MPM_LEVELS.CAMPAIGN
      : MPM_LEVELS.PROJECT;
    //MPM_V3 -1799   /*  this.dashboardMenuConfig.metaDataFields = this.fieldConfigService.getFieldsbyFeildCategoryLevel(MPM_LEVELS.PROJECT); */
    this.dashboardMenuConfig.metaDataFields =
      this.fieldConfigService.getFieldsbyFeildCategoryLevel(level);
    const dataSet = this.fieldConfigService.formrequiredDataForLoadView(
      viewDetails,
      this.dashboardMenuConfig.IS_LIST_VIEW
    );
    //MPMV3-1925
    const compDataSet = this.fieldConfigService.formrequiredDataForLoadView(
      viewDetails,
      !this.dashboardMenuConfig.IS_LIST_VIEW
    );
    //const dataSet = this.fieldConfigService.formrequiredDataForLoadView(viewDetails, true);-shree
    let arrayList = this.dashboardMenuConfig.IS_LIST_VIEW
      ? viewDetails.R_PM_LIST_VIEW_MPM_FIELDS
      : viewDetails.R_PM_CARD_VIEW_MPM_FIELDS;
    arrayList = arrayList ? arrayList : [];
    //arrayList.forEach(ele => ele.sectionName = "Manikanta");
    this.setUserPreference();
    this.dashboardMenuConfig.projectSortOptions = dataSet.SORTABLE_FIELDS;
    this.dashboardMenuConfig.projectSearchableFields =
      dataSet.SEARCHABLE_FIELDS;
    let existingSortColumn: boolean;
    const sortInput = compDataSet.SORTABLE_FIELDS.find(
      (option) => option.displayName === this.selectedSortOption
    );
    if (sortInput === undefined) {
      existingSortColumn = true;
    } else {
      existingSortColumn = false;
    }
    const sortableFields =
      existingSortColumn === false
        ? compDataSet.SORTABLE_FIELDS
        : dataSet.SORTABLE_FIELDS;
    let isDefaultSort = true;
    if (sortableFields.length > 0) {
      let sortOptions = {
        option: sortableFields[0],
        //sortType: MatSortDirections.ASC
        sortType:
          this.appConfig[
            ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER
          ] === 'ASC'
            ? 'asc'
            : 'desc',
      };
      sortOptions = dataSet.DEFAULT_SORT_FIELD
        ? dataSet.DEFAULT_SORT_FIELD
        : sortOptions;

      if (this.selectedSortOrder || this.selectedSortOption) {
        const sortData = sortableFields.find(
          (option) => option.displayName === this.selectedSortOption
        );
        const selectedSort = {
          option: sortData,
          sortType: this.selectedSortOrder
            ? this.selectedSortOrder
            : sortOptions.sortType,
        };
        isDefaultSort = false;
        this.dashboardMenuConfig.selectedSortOption = sortData
          ? JSON.parse(JSON.stringify(selectedSort))
          : JSON.parse(JSON.stringify(sortOptions));
        this.dashboardMenuConfig.sortFieldName =
          this.dashboardMenuConfig.selectedSortOption.option.name;
        this.dashboardMenuConfig.sortOrder =
          this.dashboardMenuConfig.selectedSortOption.sortType;
      } else {
        if (userPreferenceFieldData) {
          userPreferenceFieldData.data.forEach((field) => {
            const fieldId = Object.keys(field)[0];
            if (field[fieldId].IS_SORTABLE === true) {
              const selectedField = sortableFields.find(
                (field) => field.indexerId === fieldId
              );
              if (selectedField) {
                const selectedSort = {
                  option: selectedField,
                  sortType: field[fieldId].SORT_ORDER
                    ? field[fieldId].SORT_ORDER
                    : this.appConfig[
                      ApplicationConfigConstants.MPM_APP_CONFIG
                        .DEFAULT_SORT_ORDER
                    ] === 'ASC'
                      ? 'asc'
                      : 'desc',
                };
                isDefaultSort = false;
                this.dashboardMenuConfig.selectedSortOption = JSON.parse(
                  JSON.stringify(selectedSort)
                );
                this.dashboardMenuConfig.sortFieldName =
                  this.dashboardMenuConfig.selectedSortOption.option.name;
                this.dashboardMenuConfig.sortOrder =
                  this.dashboardMenuConfig.selectedSortOption.sortType;
              }
            }
          });
        }
      }
      if (isDefaultSort) {
        this.dashboardMenuConfig.selectedSortOption = JSON.parse(
          JSON.stringify(sortOptions)
        );
        this.dashboardMenuConfig.sortFieldName =
          this.dashboardMenuConfig.selectedSortOption.option.name;
        this.dashboardMenuConfig.sortOrder =
          this.dashboardMenuConfig.selectedSortOption.sortType;
      }
    }
    if (!this.hasPagination) {
      this.setPageSetting();
    }
    this.setCurrentView();
    const userPreferenceData = this.formViewData();
    this.setCurrentViewData(userPreferenceData);
  }

  getMPMDataFields(): Observable<any> {
    return new Observable((observer) => {
      let viewConfig;

      if (
        this.dashboardMenuConfig.projectViewName ||
        this.dashboardMenuConfig.templateViewName ||
        this.dashboardMenuConfig.campaignViewName
      ) {
        //MPM_V3 -1799        /* viewConfig = this.dashboardMenuConfig.IS_PROJECT_TYPE ? this.dashboardMenuConfig.projectViewName ? this.dashboardMenuConfig.projectViewName : SearchConfigConstants.SEARCH_NAME.PROJECT : this.dashboardMenuConfig.templateViewName ? this.dashboardMenuConfig.templateViewName : SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE; //this.sharingService.getViewConfigById(this.dashboardMenuConfig.projectViewId).VIEW; */

        viewConfig = this.dashboardMenuConfig.IS_CAMPAIGN_TYPE
          ? this.dashboardMenuConfig.campaignViewName
            ? this.dashboardMenuConfig.campaignViewName
            : SearchConfigConstants.SEARCH_NAME.CAMPAIGN
          : this.dashboardMenuConfig.IS_PROJECT_TYPE
            ? this.dashboardMenuConfig.projectViewName
              ? this.dashboardMenuConfig.projectViewName
              : SearchConfigConstants.SEARCH_NAME.PROJECT
            : this.dashboardMenuConfig.templateViewName
              ? this.dashboardMenuConfig.templateViewName
              : SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE;
      } else {
        //MPM_V3 -1799           /* viewConfig = this.dashboardMenuConfig.IS_PROJECT_TYPE ? SearchConfigConstants.SEARCH_NAME.PROJECT : SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE; */
        viewConfig = this.dashboardMenuConfig.IS_CAMPAIGN_TYPE
          ? SearchConfigConstants.SEARCH_NAME.CAMPAIGN
          : this.dashboardMenuConfig.IS_PROJECT_TYPE
            ? SearchConfigConstants.SEARCH_NAME.PROJECT
            : SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE;
      }
      this.npdProjectService.getMPMFieldsForView(viewConfig).subscribe((res) => {
        this.sectionFields = res['MPM_Field_Section_Name'];


        this.viewConfigService
          .getAllDisplayFeildForViewConfig(viewConfig)
          .subscribe(
            (viewDetails: ViewConfig) => {
              this.updateViewDetailsBasedonViewChange(viewDetails);
              observer.next(true);
              observer.complete();
            },
            (metadataError) => {
              observer.error(metadataError);
            }
          );
      },
        error => {
          this.viewConfigService
            .getAllDisplayFeildForViewConfig(viewConfig)
            .subscribe(
              (viewDetails: ViewConfig) => {
                this.updateViewDetailsBasedonViewChange(viewDetails);
                observer.next(true);
                observer.complete();
              },
              (metadataError) => {
                observer.error(metadataError);
              }
            );
        });
    });
  }

  intializeProjectFilters() {
    this.dashboardMenuConfig.projectDashBoadFilters = PMDashboardFilters;
    if (this.defaultViewFilterName) {
      this.dashboardMenuConfig.projectDashBoadFilters.forEach((filter) => {
        if (filter.NAME === this.defaultViewFilterName) {
          filter.IS_DEFAULT = true;
        } else {
          filter.IS_DEFAULT = false;
        }
      });
    }
    const showTemplate = this.utilService.getBooleanValue(
      this.projectConfig[
      ApplicationConfigConstants.MPM_PROJECT_CONFIG.SHOW_TEMPLATE
      ]
    );
    const showCampaign = this.utilService.getBooleanValue(
      this.projectConfig[
      ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_CAMPAIGN
      ]
    );
    //  this.enableCampaign = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CAMPAIGN]);
    this.dashboardMenuConfig.projectDashBoadFilters.forEach((filter) => {
      if (this.isCampaign) {
        if (filter.VALUE === PMDashboardFilterValues.ALL_PROJECTS) {
          this.dashboardMenuConfig.IS_PROJECT_TYPE = true;
          this.dashboardMenuConfig.searchCondition = this.formFilterCondition(
            filter.VALUE,
            this.dashboardMenuConfig.IS_CAMPAIGN_TYPE,
            this.dashboardMenuConfig.IS_PROJECT_TYPE,
            true
          );
        }
      } else if (this.dashboardMenuConfig.selectedViewByName !== '') {
        if (this.dashboardMenuConfig.selectedViewByName === filter.NAME) {
          if (filter.PROJECT_TYPE === ProjectTypes.CAMPAIGN) {
            this.dashboardMenuConfig.IS_CAMPAIGN_TYPE = true;
            this.dashboardMenuConfig.IS_PROJECT_TYPE = false;
          } else if (filter.PROJECT_TYPE === ProjectTypes.PROJECT) {
            this.dashboardMenuConfig.IS_PROJECT_TYPE = true;
            this.dashboardMenuConfig.IS_CAMPAIGN_TYPE = false;
          } else {
            this.dashboardMenuConfig.IS_PROJECT_TYPE = false;
            this.dashboardMenuConfig.IS_CAMPAIGN_TYPE = false;
          }
          const type = this.dashboardMenuConfig.IS_CAMPAIGN_TYPE
            ? this.dashboardMenuConfig.IS_CAMPAIGN_TYPE
            : this.dashboardMenuConfig.IS_PROJECT_TYPE;
          //MPM_V3 -1799
          /* this.dashboardMenuConfig.searchCondition = this.formFilterCondition(filter.VALUE, this.dashboardMenuConfig.IS_PROJECT_TYPE); */
          this.dashboardMenuConfig.searchCondition = this.formFilterCondition(
            filter.VALUE,
            this.dashboardMenuConfig.IS_CAMPAIGN_TYPE,
            this.dashboardMenuConfig.IS_PROJECT_TYPE,
            false
          );

          /*  this.dashboardMenuConfig.searchCondition = this.formFilterCondition(filter.VALUE, this.dashboardMenuConfig.IS_PROJECT_TYPE);
           */
        }
      } else if (filter.IS_DEFAULT) {
        if (filter.PROJECT_TYPE === ProjectTypes.CAMPAIGN) {
          this.dashboardMenuConfig.IS_CAMPAIGN_TYPE = true;
          this.dashboardMenuConfig.IS_PROJECT_TYPE = false;
        } else if (filter.PROJECT_TYPE === ProjectTypes.PROJECT) {
          //  filter.IS_PROJECT_TYPE = true;
          this.dashboardMenuConfig.IS_PROJECT_TYPE = true;
          this.dashboardMenuConfig.IS_CAMPAIGN_TYPE = false;
        } else {
          // filter.IS_PROJECT_TYPE = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.SHOW_TEMPLATE]);
          // this.dashboardMenuConfig.projectDashBoadFilters.IS_PROJECT_TYPE = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.SHOW_TEMPLATE]);
          this.dashboardMenuConfig.IS_PROJECT_TYPE = false;
          this.dashboardMenuConfig.IS_CAMPAIGN_TYPE = false;
        }
        //MPM_V3 -1799
        if (filter.PROJECT_TYPE === ProjectTypes.CAMPAIGN) {
          if (
            !(this.isCampaignRole && this.isCampaignRole.length > 0) ||
            !this.enableCampaign
          ) {
            filter.IS_DEFAULT = false;
            filter.IS_SHOWN = false;
          } else {
            this.dashboardMenuConfig.selectedViewByName = filter.NAME;
            this.projectType = filter.PROJECT_TYPE;
          }
        } else {
          this.dashboardMenuConfig.selectedViewByName = filter.NAME;
          this.projectType = filter.PROJECT_TYPE;
        }
        //MPM_V3 -1799
        /*  this.dashboardMenuConfig.searchCondition = this.formFilterCondition(filter.VALUE, this.dashboardMenuConfig.IS_PROJECT_TYPE);
         */
        const type = this.dashboardMenuConfig.IS_CAMPAIGN_TYPE
          ? this.dashboardMenuConfig.IS_CAMPAIGN_TYPE
          : this.dashboardMenuConfig.IS_PROJECT_TYPE;
        /* this.dashboardMenuConfig.searchCondition = this.formFilterCondition(filter.VALUE, this.dashboardMenuConfig.IS_PROJECT_TYPE); */
        this.dashboardMenuConfig.searchCondition = this.formFilterCondition(
          filter.VALUE,
          this.dashboardMenuConfig.IS_CAMPAIGN_TYPE,
          this.dashboardMenuConfig.IS_PROJECT_TYPE,
          false
        );
      }
      if (filter.PROJECT_TYPE === ProjectTypes.CAMPAIGN) {
        if (
          !(this.isCampaignRole && this.isCampaignRole.length > 0) ||
          !this.enableCampaign
        ) {
          filter.IS_DEFAULT = false;
          filter.IS_SHOWN = false;
        }
      } else if (
        !showTemplate &&
        filter.PROJECT_TYPE === ProjectTypes.TEMPLATE
      ) {
        filter.IS_SHOWN = showTemplate;
      }
    });
  }

  handleProjectServiceCall() {
    if (this.isCampaignDashboard) {
      this.getAllCampaign(true).subscribe(
        (campaignsResponse) => {
          if (campaignsResponse) {
            this.loaderService.hide();
            this.isRefresh = false;
          }
        },
        (campaignsError) => {
          this.loaderService.hide();
          this.notificationService.error(campaignsError);
        }
      );
    } else {
      this.getAllProjects(true).subscribe(
        (projectsResponse) => {
          if (projectsResponse) {
            this.loaderService.hide();
            this.isRefresh = false;
          }
        },
        (projectsError) => {
          this.loaderService.hide();
          this.notificationService.error(projectsError);
        }
      );
    }
    // this.skip = 0;
  }
  formFilterCondition(
    filterValues: any,
    IS_CAMPAIGN_TYPE: boolean,
    IS_PROJECT_TYPE: boolean,
    isCampaignProject: boolean
  ): Array<any> {
    /* const searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(ProjectConstant.GET_ALL_PROJECT_SEARCH_CONDITION_LIST); */
    let searchCondition;
    let condition = null;
    if (isCampaignProject) {
      searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(
        ProjectConstant.GET_ALL_PROJECT_SEARCH_CONDITION_LIST
      );
      condition = this.fieldConfigService.createConditionObject(
        MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ID,
        MPMSearchOperators.IS,
        MPMSearchOperatorNames.IS,
        MPMSearchOperatorNames.AND,
        this.campaignId
      );
      if (condition) {
        searchCondition.push(condition);
      }
    } else if (IS_CAMPAIGN_TYPE) {
      // MPM_V3 -1799
      searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(
        ProjectConstant.GET_ALL_CAMPAIGN_SEARCH_CONDITION_LIST
      );
      condition = this.fieldConfigService.createConditionObject(
        MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_TYPE,
        MPMSearchOperators.IS,
        MPMSearchOperatorNames.IS,
        MPMSearchOperators.AND,
        'CAMPAIGN'
      );
      if (condition) {
        searchCondition.push(condition);
      }
      if (
        !(
          filterValues === PMDashboardFilterValues.ALL_TEMPLATES ||
          filterValues === PMDashboardFilterValues.ALL_PROJECTS ||
          filterValues === PMDashboardFilterValues.ALL_CAMPAIGNS
        )
      ) {
        condition = this.fieldConfigService.createConditionObject(
          MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_OWNER_ID,
          MPMSearchOperators.IS,
          MPMSearchOperatorNames.IS,
          MPMSearchOperators.AND,
          this.sharingService.getCurrentUserItemID()
        );
        if (condition) {
          searchCondition.push(condition);
        }
      } else {
        /* if (this.sharingService.getCRActions()) {
   
                    this.sharingService.getCRActions().forEach((eachTeam: Team, index) => {
                        condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TEAM_ID,
                            MPMSearchOperators.IS, MPMSearchOperatorNames.IS, index === 0 ? MPMSearchOperators.AND : MPMSearchOperators.OR, eachTeam['MPM_Teams-id'].Id);
                        if (condition) {
                            searchCondition.push(condition);
                        }
                    });
                } */
      }
    } else {
      searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(
        ProjectConstant.GET_ALL_PROJECT_SEARCH_CONDITION_LIST
      );
      condition = this.fieldConfigService.createConditionObject(
        MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE,
        MPMSearchOperators.IS,
        MPMSearchOperatorNames.IS,
        MPMSearchOperators.AND,
        IS_PROJECT_TYPE ? 'PROJECT' : 'TEMPLATE'
      );
      if (condition) {
        searchCondition.push(condition);
      }
      if (
        !(
          filterValues === PMDashboardFilterValues.ALL_TEMPLATES ||
          filterValues === PMDashboardFilterValues.ALL_PROJECTS
        )
      ) {
        condition = this.fieldConfigService.createConditionObject(
          MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_OWNER_ID,
          MPMSearchOperators.IS,
          MPMSearchOperatorNames.IS,
          MPMSearchOperators.AND,
          this.sharingService.getCurrentUserItemID()
        );
        if (condition) {
          searchCondition.push(condition);
        }
      } else {
        if (this.sharingService.getCRActions()) {
          this.sharingService
            .getCRActions()
            .forEach((eachTeam: Team, index) => {
              condition = this.fieldConfigService.createConditionObject(
                MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TEAM_ID,
                MPMSearchOperators.IS,
                MPMSearchOperatorNames.IS,
                index === 0 ? MPMSearchOperators.AND : MPMSearchOperators.OR,
                eachTeam['MPM_Teams-id'].Id
              );
              if (condition) {
                searchCondition.push(condition);
              }
            });
        }
      }
    }
    return searchCondition;
  }

  onOpenCommentsModule(projectId) {
    this.stateService.setRoute(
      this.queryParams.menuName,
      this.queryParams,
      this.routeParams
    );
    if (
      this.isCampaignDashboard ||
      (this.dashboardMenuConfig && this.dashboardMenuConfig.IS_CAMPAIGN_TYPE)
    ) {
      this.routeService.goToComments(
        projectId,
        true,
        this.campaignId ? this.campaignId : null
      );
    } else {
      this.routeService.goToComments(
        projectId,
        false,
        this.campaignId ? this.campaignId : null
      );
    }
  }

  updateSearchConfigId() {
    if (this.dashboardMenuConfig.projectViewConfig) {
      this.searchConfigId =
        this.dashboardMenuConfig.projectViewConfig.R_PO_ADVANCED_SEARCH_CONFIG;
      this.searchChangeEventService.update(
        this.dashboardMenuConfig.projectViewConfig
      );
    }
  }
  getProjectConfig() {
    const newProjectType = this.dashboardMenuConfig.NEW_PROJECT_TYPES;
    this.projectConfig = this.sharingService.getProjectConfig();
    this.appConfig = this.sharingService.getAppConfig();
    newProjectType.isShown = this.utilService.getBooleanValue(
      this.projectConfig[newProjectType.appDynamicConfigId]
    );
    // AppDynamicConfig.getDashboardBrandingConfigValue(newProjectType.appDynamicConfigId);
    newProjectType.options.map((option) => {
      option.isShown = this.utilService.getBooleanValue(
        this.projectConfig[option.appDynamicConfigId]
      );
      this.optionDropdown = this.optionDropdown || option.isShown;

      if (!option.IS_PROJECT_TYPE && option.isShown) {
        option.isShown = this.utilService.getBooleanValue(
          this.projectConfig[
          ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_TEMPLATE
          ]
        );
        // AppDynamicConfig.getDashboardBrandingConfigValue('SHOW_TEMPLATE_OPTION');
      }
      if (
        option.appDynamicConfigId ===
        ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_CAMPAIGN
      ) {
        if (
          !(this.isCampaignRole && this.isCampaignRole.length > 0) ||
          this.isCampaign ||
          !this.enableCampaign
        ) {
          option.isShown = false;
        }
      }
    });
    newProjectType.isShown =
      this.utilService.getBooleanValue(
        this.projectConfig[newProjectType.appDynamicConfigId]
      ) && this.optionDropdown;
    this.dashboardMenuConfig.NEW_PROJECT_TYPES = newProjectType;
    // AppDynamicConfig.getDashboardBrandingConfigValue('DEFAULT_VIEW');
    /* if (defaultView === 'LIST') {
            this.dashboardMenuConfig.IS_LIST_VIEW = true;
        } */
    /* this.dashboardMenuConfig.projectViewList.options.map(option => {
            if (!option.IS_PROJECT_TYPE) {
                option.isShown = projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.SHOW_TEMPLATE]; //AppDynamicConfig.getDashboardBrandingConfigValue('SHOW_TEMPLATE_OPTION');
            }
        }); */
  }

  removeSideBarExpansionState() {
    localStorage.removeItem('PM-OVERVIEW-SIDE-BAR-EXPANDED-STATE');
  }

  refreshData() {
    this.refresh();
  }

  resetPreference() {
    // this.npdProjectService.getResetTrigger();
    this.resetView()

    // return
  }

  ngOnInit() {

    if (this.npdProjectService.subsVar == undefined) {
      this.npdProjectService.subsVar = this.npdProjectService.invokeFirstComponentFunction.subscribe(eee => {
        this.resetPreference();
      })
    }
    // this.changedPref=false
    this.npdProjectService.selectedMenu.subscribe(res => {
      this.activeUserPreference = res
      const menu = this.sharingService.getCurrentMenu();
      const getCurrentUserPreference = this.sharingService.getCurrentUserPreference();
      getCurrentUserPreference.forEach(Prefer => {
        if (Prefer.IS_ACTIVE_USER_PREFERENCE == 'true') {
          this.isDefaultUserPreference = Prefer
          sessionStorage.setItem('DEFAULT_USER_PREFERENCE-PM_PROJECT_VIEW', this.isDefaultUserPreference['MPM_User_Preference-id']?.Id)
        }
      })
      this.activePreferenceName = sessionStorage.getItem('ACTIVE_USER_PREFERENCE_NAME') ? sessionStorage.getItem('ACTIVE_USER_PREFERENCE_NAME') : this.isDefaultUserPreference?.USER_PREFERENCE_NAME
      sessionStorage.setItem('ACTIVE_USER_PREFERENCE_NAME', this.activePreferenceName)
      if (menu) {
        const defaultUserPreferenceView =
          this.sharingService.getDefaultCurrentUserPreferenceByMenu(
            menu['MPM_Menu-id'].Id
          );

        if (this.isDefaultUserPreference) {
          this.defaultViewFilterName = this.isDefaultUserPreference.FILTER_NAME;
        }
      }
      this.enableIndexerFieldRestriction = config.getFieldRestriction();
      this.maximumFieldsAllowed = config.getMaximumFieldsAllowed();
      this.removeSideBarExpansionState();
      this.routeService.handleViewRouting(MenuConstants.PROJECT_MANAGEMENT);
      this.loaderService.show();
      this.appConfig = this.sharingService.getAppConfig();
      const defaultView =
        this.appConfig[
        ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PROJECT_VIEW
        ];
      this.disableGridView = this.utilService.getBooleanValue(
        this.appConfig[
        ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW
        ]
      );
      this.top = this.utilService.convertStringToNumber(
        this.appConfig[
        ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION
        ]
      );
      this.pageSize = this.utilService.convertStringToNumber(
        this.appConfig[
        ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION
        ]
      );
      this.pageNumber = 1;
      this.enableCampaign = this.utilService.getBooleanValue(
        this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CAMPAIGN]
      );
      this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_PAGE_LIMIT] = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION];
      this.isCampaignRole = this.sharingService.getCampaignRole();
      // this.dashboardMenuConfig = {
      //     FILTER_LIST: [],
      //     APPLIED_FILTER: [],
      //     SERACH_FILTER: [],
      //     IS_PROJECT_TYPE: true,
      //     IS_CAMPAIGN_TYPE: false,
      //     SELECTED_SORT: [],
      //     IS_LIST_VIEW: defaultView === 'LIST' ? true : false,
      //     NEW_PROJECT_TYPES: DashboardControlConstant.newProjectType,
      //     projectFilterList: [],
      //     projectDashBoadFilters: [],
      //     selectedViewByName: '',
      //     applyFilterList: [],
      //     totalProjectCount: 0,
      //     projectData: [],
      //     isViewTypeChanged: false,
      //     isAdvancedSearchOpen: false,
      //     searchInputValue: '',
      //     searchCondition: [],
      //     projectSortOptions: [],
      //     projectSearchableFields: [],
      //     projectViewConfig: null,
      //     projectViewName: null,
      //     templateViewName: null,
      //     campaignData: [],
      //     campaignViewName: null /* 'PM_CAMPAIGN_VIEW' */
      // };
      this.dashboardMenuConfig = {
        FILTER_LIST: [],
        APPLIED_FILTER: [],
        SERACH_FILTER: [],
        IS_PROJECT_TYPE: true,
        IS_CAMPAIGN_TYPE: false,
        SELECTED_SORT: [],
        IS_LIST_VIEW: defaultView === 'LIST' ? true : false,
        NEW_PROJECT_TYPES: DashboardControlConstant.newProjectType,
        projectFilterList: [],
        projectDashBoadFilters: [],
        selectedViewByName: '',
        applyFilterList: [],
        totalProjectCount: 0,
        projectData: [],
        isViewTypeChanged: false,
        isAdvancedSearchOpen: false,
        searchInputValue: '',
        searchCondition: [],
        projectSortOptions: [],
        projectSearchableFields: [],
        assetViewConfig: null,
        projectViewConfig: null,
        projectViewName: null,
        templateViewName: null,
        campaignData: [],
        campaignViewName: null /* 'PM_CAMPAIGN_VIEW' */,
        freezeCount: 0,
      };
      if (this.router.url.includes(RoutingConstants.campaignViewBaseRoute)) {
        this.isCampaign = true;
      }
      if (this.router.url.includes('Campaigns')) {
        this.isCampaignDashboard = true;
      }
      this.activatedRoute.parent.params.subscribe((params) => {
        this.campaignId = params.campaignId;
        this.getCampaignById(this.campaignId).subscribe((response) => { });
      });
      if (this.activatedRoute.parent && this.activatedRoute.parent.params) {
        this.activatedRoute.parent.params.subscribe((params) => {
          if (params) {
            this.routeParams = params;
          }
        });
      }
      this.activatedRoute.queryParams.subscribe((params) => {
        this.queryParams = params;
        this.isdefaultPagination = true;
        if (params.viewName) {
          this.dashboardMenuConfig.selectedViewByName = params.viewName;
        }
        /* this.isCampaign = params.viewName.includes('Campaigns') ? true : false; */
        if (params.isListView) {
          this.dashboardMenuConfig.IS_LIST_VIEW =
            this.utilService.getBooleanValue(params.isListView);
          this.isListView = this.dashboardMenuConfig.IS_LIST_VIEW;
        }
        if (params.sortBy) {
          this.selectedSortOption = params.sortBy;
          this.selectedSortOrder = params.sortOrder;
        }
        if (params.searchName) {
          if (this.searchName !== params.searchName) {
            this.searchName = params.searchName;
            if (!this.isRefresh) {
              this.updatePagination();
            }
          }
        } else if (!params.searchName && this.searchName !== null) {
          this.searchName = null;
          this.updatePagination();
        }
        if (params.savedSearchName) {
          if (this.savedSearchName !== params.savedSearchName) {
            this.savedSearchName = params.savedSearchName;
            if (!this.isRefresh) {
              this.updatePagination();
            }
          }
        } else if (!params.savedSearchName && this.savedSearchName !== null) {
          this.savedSearchName = null;
          this.updatePagination();
        }
        if (params.advSearchData) {
          if (this.advSearchData !== params.advSearchData) {
            this.advSearchData = params.advSearchData;
            if (!this.isRefresh) {
              this.updatePagination();
            }
          }
        } else if (!params.advSearchData && this.advSearchData !== null) {
          this.advSearchData = null;
          this.updatePagination();
        }
        if (params.facetData) {
          if (this.facetData !== params.facetData) {
            this.facetData = params.facetData;
            if (!this.isRefresh) {
              this.updatePagination();
            }
          }
        } else if (!params.facetData && this.facetData !== null) {
          this.facetData = null;
          this.updatePagination();
        }
        if (params.pageSize && this.pageSize !== Number(params.pageSize)) {
          this.pageSize = Number(params.pageSize);
          this.top = Number(params.pageSize);
          // this.isPagination = true;
        }
        if (params.pageNumber && this.page !== Number(params.pageNumber)) {
          this.pageNumber = Number(params.pageNumber);
          this.page = Number(params.pageNumber);
          this.skip = (Number(this.pageNumber) - 1) * Number(this.pageSize);
          this.isdefaultPagination = false;
        }
        if (params.pageSize || params.pageNumber) {
          this.hasPagination = this.isRefresh;
        }
        // this.isRefresh = false;
      });
      this.getProjectConfig();
      this.intializeProjectFilters();
      if (this.disableGridView) {
        this.dashboardMenuConfig.IS_LIST_VIEW = true;
      }
      this.getMPMDataFields().subscribe(
        (metadataResponse) => {
          this.tempsearchConditionsSubscribe =
            this.searchDataService.searchData.subscribe((data) => {
              this.loaderService.show();
              this.searchConditions = data;
              this.skip = this.isdefaultPagination
                ? data && data.length > 0
                  ? 0
                  : this.skip
                : this.skip;
              this.page = this.isdefaultPagination
                ? data && data.length > 0
                  ? 1
                  : this.page
                : this.page;
              if (
                (this.searchName ||
                  this.savedSearchName ||
                  this.advSearchData ||
                  this.facetData) &&
                this.isRefresh
              ) {
                if (data && data.length > 0) {
                  if (
                    !this.facetData ||
                    (this.facetData &&
                      this.facetRestrictionList &&
                      (this.advSearchData || this.savedSearchName))
                  ) {
                    this.handleProjectServiceCall();
                  } else {
                    this.validateFacet = true;
                  }
                }
              } else if (
                this.searchName === null &&
                this.savedSearchName === null &&
                this.advSearchData === null &&
                this.facetData === null &&
                this.isRefresh
              ) {
                this.handleProjectServiceCall();
              } else if (!this.isRefresh) {
                this.handleProjectServiceCall();
              }
            });
          this.facetChangeEventService.onFacetCondidtionChange
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((facetRestrictionList) => {
              if (JSON.stringify(facetRestrictionList) !== '{}') {
                if (!(this.previousFacetCondition === facetRestrictionList)) {
                  this.loaderService.show();
                  this.facetRestrictionList = facetRestrictionList;
                  this.previousFacetCondition = this.facetRestrictionList;
                  if (this.facetData && this.isRefresh) {
                    if (
                      facetRestrictionList &&
                      facetRestrictionList.facet_condition &&
                      facetRestrictionList.facet_condition.length > 0
                    ) {
                      if (
                        !(this.advSearchData || this.savedSearchName) ||
                        this.validateFacet
                      ) {
                        this.handleProjectServiceCall();
                        this.validateFacet = false;
                      }
                    }
                  } else if (!this.isRefresh) {
                    this.previousCampaignRequest = null;
                    this.previousProjectRequest = null;
                    this.handleProjectServiceCall();
                  }
                }
              }
            });
        },
        (metadataError) => {
          this.loaderService.hide();
          this.notificationService.error(metadataError);
        }
      );
    })
    this.sharingService.resetViewData().subscribe((viewId) => {
      if (
        viewId &&
        this.dashboardMenuConfig &&
        this.dashboardMenuConfig.projectViewConfig &&
        this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'] &&
        this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id &&
        viewId ===
        this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id
      ) {
        this.resetCurrentView();
      }
    });
  }

  ngOnDestroy() {
    if (this.tempsearchConditionsSubscribe) {
      this.searchChangeEventService.update(null);
      this.tempsearchConditionsSubscribe.unsubscribe();
    }
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

