import { ApplicationConfigConstants } from 'mpm-library';

export const DashboardControlConstant = {
    dashboardControls: [
        'newProjectType'
    ],
    newProjectType: {
        fieldName: 'newProjectType',
        inputType: 'singleSelect',
        label: 'New',
        placeholder: 'Create new Project or Template',
        defaultValue: '',
        appDynamicConfigId: ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_PROJECT_TEMPLATE_CREATION,
        isShown: '',
        options: [{
            name: 'Blank Project',
            value: 'Blank_Project',
            appDynamicConfigId: ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_PROJECT,
            isShown: '',
            icon: 'description',
            IS_PROJECT_TYPE: true,
            IS_CAMPAIGN_TYPE: false
        }, {
            name: 'From Template',
            value: 'Form_Template',
            appDynamicConfigId: ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_PROJECT,
            isShown: '',
            icon: 'library_books',
            IS_PROJECT_TYPE: true,
            IS_CAMPAIGN_TYPE: false
        }, {
            name: 'Project Template',
            value: 'Project_Template',
            appDynamicConfigId: ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_TEMPLATE,//SHOW_COPY_PROJECT
            isShown: '',
            icon: 'library_add',
            IS_PROJECT_TYPE: false,
            IS_CAMPAIGN_TYPE: false
        }, {
            name: 'Create Campaign',
            value: 'Create_Campaign',
            appDynamicConfigId: ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_CAMPAIGN,
            isShown: '',
            icon: 'create_new_folder',
            IS_PROJECT_TYPE: true,
            IS_CAMPAIGN_TYPE: true
        }/*,
        {
            name: 'Bulk Project Creation',
            value: 'Bulk_Project_Creation',
            appDynamicConfigId: ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_PROJECT,
            isShown: '',
            icon: 'library_books',
            IS_PROJECT_TYPE: true,
            IS_CAMPAIGN_TYPE: false
        }*/
        ]
    }
};
