import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NpdManagerDashboardComponent } from '../../npd-ui/npd-project-view/npd-project-management/npd-manager-dashboard/npd-manager-dashboard.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';

const routes: Routes = [
    {
        path: '',
        component: NpdManagerDashboardComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ProjectManagementRoutingModule { }
