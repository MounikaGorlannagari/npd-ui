import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';
import { ProjectCardViewComponent } from './project-card-view/project-card-view.component';
import { ProjectManagementRoutingModule } from './project-management.routing.module';
import { MaterialModule } from '../../../material.module';
import { PMDashboardModule, MpmLibraryModule, PaginationModule, FacetModule, DeliverableModule, ColumnChooserModule } from 'mpm-library';
import { ProjectDialogComponent } from './project-dialog/project-dialog.component';
import { MpmLibModule } from '../../mpm-lib/mpm-lib.module';
import { BulkProjectManagementComponent } from './bulk-project-management/bulk-project-management.component';
import { ProjectMilestoneComponent } from './project-milestone/project-milestone.component';
import { NpdProjectCardComponent } from '../../npd-ui/npd-project-view/pm-dashboard/npd-project-card/npd-project-card.component';
import { NpdProjectListComponent } from '../../npd-ui/npd-project-view/pm-dashboard/npd-project-list/npd-project-list.component';
import { NpdTaskModule } from '../../npd-ui/npd-task-view/npd-task.module';
import { NpdManagerDashboardComponent } from '../../npd-ui/npd-project-view/npd-project-management/npd-manager-dashboard/npd-manager-dashboard.component';

@NgModule({
    declarations: [
        ManagerDashboardComponent,
        ProjectCardViewComponent,
        ProjectDialogComponent,
        BulkProjectManagementComponent,
        ProjectMilestoneComponent,
        NpdProjectListComponent,
        NpdProjectCardComponent,
        NpdManagerDashboardComponent,
    ],
    imports: [
        CommonModule,
        MaterialModule,
        ProjectManagementRoutingModule,
        DeliverableModule,
        PMDashboardModule,
        MpmLibraryModule,
        PaginationModule,
        FacetModule,
        MpmLibModule,
        ColumnChooserModule,
        NpdTaskModule
    ],
    entryComponents: []
})

export class ProjectManagementModule { }
