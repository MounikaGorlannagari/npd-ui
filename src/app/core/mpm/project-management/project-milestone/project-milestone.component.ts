import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Observable } from 'rxjs';
import { LoaderService, NotificationService, ProjectService } from 'mpm-library';

@Component({
  selector: 'app-project-milestone',
  templateUrl: './project-milestone.component.html',
  styleUrls: ['./project-milestone.component.scss']
})
export class ProjectMilestoneComponent implements OnInit {


  tasks: string[] = [];
  isMilestoneEmpty: boolean = false;

  constructor(
    private dialogRef: MatDialogRef<ProjectMilestoneComponent>,
    @Inject(MAT_DIALOG_DATA) public projectId: any,
    public projectService: ProjectService,
    public loaderService: LoaderService,
    public notificationService: NotificationService
    
) { }



close() {
    this.dialogRef.close(true);
}



  ngOnInit(): void {
    
    this.loaderService.show();
    this.projectService.getCompletedMilestonesByProjectId(this.projectId).subscribe(response => {
      this.loaderService.show();
      if (response && response.tuple && response.tuple) {
          if (!Array.isArray(response.tuple)) {
            response.tuple = [response.tuple];
          }
          let milestoneList;
          milestoneList = response.tuple;
          if (milestoneList && milestoneList.length && milestoneList.length > 0) {
            milestoneList.map(milestone => {
                  if(milestone && milestone.old && milestone.old.Task){
                    this.tasks.push(milestone.old.Task.TaskName);
                  }
                  
              });
              this.loaderService.hide();
          }
          if(this.tasks.length <= 0){
            this.loaderService.hide();
            this.isMilestoneEmpty = true;
          }
          else{
            this.loaderService.hide();
            this.isMilestoneEmpty = false;
          }
      }else{
        this.loaderService.hide();
        this.isMilestoneEmpty = true;
      }
    }, () => {
      this.loaderService.hide();
      this.isMilestoneEmpty = true;
  });

  this.loaderService.hide();
  }

}
