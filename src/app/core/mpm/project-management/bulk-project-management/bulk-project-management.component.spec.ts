import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkProjectManagementComponent } from './bulk-project-management.component';

describe('BulkProjectManagementComponent', () => {
  let component: BulkProjectManagementComponent;
  let fixture: ComponentFixture<BulkProjectManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkProjectManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkProjectManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
