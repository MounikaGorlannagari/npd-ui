import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { ConfirmationModalComponent } from 'mpm-library';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ProjectFromTemplateService } from 'mpm-library';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { LoaderService, NotificationService, MPMFieldConstants, FieldConfigService, ProjectConstant } from 'mpm-library';
import { SharingService } from 'mpm-library';
import { ViewConfigService } from 'mpm-library';
import { IndexerService } from 'mpm-library';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { saveAs } from 'file-saver';
import { AppService } from 'mpm-library';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProjectService } from 'mpm-library';


@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-bulk-project-management',
  templateUrl: './bulk-project-management.component.html',
  styleUrls: ['./bulk-project-management.component.scss']
})
export class BulkProjectManagementComponent implements OnInit {


  @Output() notificationHandler = new EventEmitter<any>();
  @Output() closeCallbackHandler = new EventEmitter<any>();
  @Output() loadingHandler = new EventEmitter<any>();
  @Output() valueChangeEvent = new EventEmitter<any>();

  templateform: FormGroup;
  templateAutocompleteField;
  categoryAutocompleteField;
  documentContent;
  documentName;
  allTemplateList;
  allCategoriesList;
  private files: Array<FileUploadModel> = [];
  userID;
  documentURL;
  fileUrl;
  templateSearchConditions = {
    projectSearchCondition: [],
    templateSearchCondition: [],
  };

  constructor(
    private dialogRef: MatDialogRef<BulkProjectManagementComponent>,
    @Inject(MAT_DIALOG_DATA) public projectData: any,
    public fb: FormBuilder,
    private bulkProjectRef: MatDialogRef<BulkProjectManagementComponent>,
    private sharingService: SharingService,
    public projectFromTemplateService: ProjectFromTemplateService,
    public loaderService: LoaderService,
    public fieldConfigService: FieldConfigService,
    public viewConfigService: ViewConfigService,
    public indexerService: IndexerService,
    public httpClient: HttpClient,
    public sanitizer: DomSanitizer,
    public appService: AppService,
    public dialog: MatDialog,
    public notification: NotificationService,
    public projectService: ProjectService) { }

  initializeForm() {
    this.templateform = this.fb.group({
      fileControl: new FormControl(),
      template: new FormControl(),
      category: new FormControl(),
    });

    /* category auto complete*/
    this.categoryAutocompleteField = {
      formControl: this.templateform.controls.category,
      label: 'Category',
      filterOptions: this.allCategoriesList,
    }
    /* template auto complete */
    this.templateAutocompleteField = {
      formControl: this.templateform.controls.template,
      label: 'Template',
      filterOptions: this.allTemplateList,
    };
  }


  fileTemplateClick() {
    const categoryId = this.templateform.controls.category.value['MPM_Category-id'].Id;
    this.projectService.downloadEncodedExcelFile(this.userID, categoryId).subscribe(response => {
      if (response && response.DownloadExcelTemplateResponse && response.DownloadExcelTemplateResponse.tuple && response.DownloadExcelTemplateResponse.tuple.old && response.DownloadExcelTemplateResponse.tuple.old.downloadExcelTemplate && response.DownloadExcelTemplateResponse.tuple.old.downloadExcelTemplate.downloadExcelTemplate) {
        const encodedString = response.DownloadExcelTemplateResponse.tuple.old.downloadExcelTemplate.downloadExcelTemplate;
        this.downloadFile(encodedString);
      }

    });
  }

  fileUploadClick(fileInput: any) {//shadow variables
    const file = document.getElementById('file') as HTMLInputElement;
    file.onchange = () => {
      for (let index = 0; index < file.files.length; index++) {//for of
        const fileInput = file.files[index];
        this.files.push({
          data: fileInput, state: 'in',
          inProgress: false, progress: 0, canRetry: false, canCancel: true
        });
      }
      this.updateFile(file, fileInput);
    };
    file.click();
  }

  updateFile(file: HTMLInputElement, fileInput: any) {
    let name = file.value;
    name = name.substring(12, name.length);//check
    this.templateform.controls.fileControl.setValue(name);
    const readfile = file.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(readfile);
    reader.onload = () => {
      let result = reader.result;
      result = result + "";// check
      this.documentContent = result.split('base64')[1];
      this.documentContent = this.documentContent.toString().replace(',', "");
      this.projectService.createDocument(this.documentName, this.documentContent).subscribe(
        response => {
          this.documentURL = response.DocumentURL;
        });
    }
  }

  downloadFile(encodedString) {
    const byteString = window.atob(encodedString);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }//for each or for of
    const categoryName = this.templateform.controls.category.value['CATEGORY_NAME'];
    const timeStamp = new Date().getTime();
    this.documentName = categoryName + '_' + this.userID + '_' + timeStamp + '.xlsx';
    const blob = new Blob([int8Array], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      window.URL.createObjectURL(blob)
    );
    saveAs(blob, this.documentName);
  }
  closeDialog() {
    if (this.templateform.pristine) {
      this.bulkProjectRef.close();
    } else {
      const dialogRef = this.dialog.open(ConfirmationModalComponent, {
        width: '40%',
        disableClose: true,
        data: {
          message: 'Are you sure you want to close?',
          submitButton: 'Yes',
          cancelButton: 'No'
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result && result.isTrue) {
          this.bulkProjectRef.close();
        }
      });
    }
  }

  createProject() {

    const templateId = this.templateform.controls.template.value.ID;

    this.projectService.bulkProjectCreation(this.documentURL, templateId).subscribe(
      response => {
        this.notification.success('Bulk Project Creation has been initiated successfully');
        this.loadingHandler.next(false);
        this.bulkProjectRef.close();
      });
  }

  ngOnInit(): void {

    this.templateSearchConditions.projectSearchCondition = this.projectData.projectSearchCondition;
    // check this.project.viewconfi
    this.templateSearchConditions.templateSearchCondition = this.projectData.templateSearchCondition;

    this.loaderService.show();
    this.projectFromTemplateService.getAllTemplates(this.templateSearchConditions.templateSearchCondition, this.projectData.viewConfig)
      .subscribe(templateObj => {
        this.loaderService.hide();
        if (templateObj.allTemplateCount > 0) {
          this.allTemplateList = templateObj.allTemplateList;
          const indexerField = this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
          this.allTemplateList.map(data => {
            data.displayName = data[indexerField];
            data.name = data[indexerField];
          });
          this.initializeForm();
        } else {
          this.notification.warn('There are no templates');
          this.bulkProjectRef.close();
        }
      }, error => {
        this.loaderService.hide();
      });

    this.userID = this.sharingService.getCurrentUserID();
    this.allCategoriesList = this.sharingService.getAllCategories();
    this.allCategoriesList.map(data => {
      data.displayName = data['CATEGORY_NAME'];
      data.name = data['CATEGORY_NAME']
    });
  }
}

export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
}
