import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { LoaderService, NotificationService, ProjectService, ProjectFromTemplatesComponent, MPMFieldConstants, FieldConfigService, ProjectConstant } from 'mpm-library';
import { RouteService } from '../../route.service';
import { PROJECT_FROM_TEMPLATE_CONSTANT } from 'mpm-library';

@Component({
    selector: 'app-project-dialog',
    templateUrl: './project-dialog.component.html',
    styleUrls: ['./project-dialog.component.scss']
})

export class ProjectDialogComponent implements OnInit {

    @ViewChild(ProjectFromTemplatesComponent) projectFromTemplatesComponent: ProjectFromTemplatesComponent;

    modalTitle = 'Create Project From Template';
    isDataLoaded = false;
    projectType;
    selectedProject;
    viewConfig;
    isCopyTemplate;
    campaignId;
    templateSearchConditions = {
        projectSearchCondition: [],
        templateSearchCondition: [],
    };

    constructor(
        private dialogRef: MatDialogRef<ProjectDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public projectData: any,
        private mpmRouteService: RouteService,
        private loaderService: LoaderService,
        private notification: NotificationService,
        private projectService: ProjectService,
        private fieldConfigService: FieldConfigService,
        private adapter: DateAdapter<any>) {
    }
    closeDialog() {
        this.projectFromTemplatesComponent.closeDialog();
    }
    afterCloseProject(event) {
        this.dialogRef.close(true);
    }
    afterSaveProject(event) {
        if (event.projectId) {
            this.mpmRouteService.goToProjectDetailView(event.projectId, event.isTemplate, null);
        }
    }
    notificationHandler(event) {
        if (event.message) {
            event.type === ProjectConstant.NOTIFICATION_LABELS.SUCCESS ? this.notification.success(event.message) :
                event.type === ProjectConstant.NOTIFICATION_LABELS.INFO ? this.notification.info(event.message) : this.notification.error(event.message);
        }
    }
    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }

    ngOnInit() {
        this.templateSearchConditions.projectSearchCondition = this.projectData.projectSearchCondition;
        this.projectType = this.projectData.projectType;
        this.viewConfig = this.projectData.viewConfig ? this.projectData.viewConfig : null;
        this.campaignId = this.projectData.campaignId;
        this.selectedProject = this.projectData.selectedProject;
        this.isCopyTemplate = this.projectData.selectedProject.isCopyTemplate;
        if(this.isCopyTemplate) {
            this.modalTitle = 'Copy Template';
        }
        this.templateSearchConditions.templateSearchCondition = this.projectData.templateSearchCondition;

        if (navigator.language !== undefined) {
            this.adapter.setLocale(navigator.language);
        }
        if (this.projectData.projectType === PROJECT_FROM_TEMPLATE_CONSTANT.PROJECT_TYPE_GENERAL) {
            const indexerField = this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
            this.modalTitle = 'Copy Project' + ' - ' + this.projectData.selectedProject[indexerField];
            this.isDataLoaded = true;
        } else {
            this.isDataLoaded = true;
        }
    }

}
