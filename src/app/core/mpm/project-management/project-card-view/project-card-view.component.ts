import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { DashboardMenuConfig, ProjectConstant } from 'mpm-library';

@Component({
    selector: 'app-project-card-view',
    templateUrl: './project-card-view.component.html',
    styleUrls: ['./project-card-view.component.scss']
})

export class ProjectCardViewComponent implements OnInit {

    @Input() projectData: Array<any>;
    @Input() dashboardMenuConfig: DashboardMenuConfig;
    @Output() projectGridView = new EventEmitter<any[]>();
    @Output() openProjectDetail = new EventEmitter<any[]>();
    @Output() copyProject = new EventEmitter<any[]>();
    @Output() openCommentsModule = new EventEmitter<any[]>();
    @Output() selectedProjectCardCount = new EventEmitter<any>();
    @Output() refreshEmitter = new EventEmitter<any>();
    rowHeight: number;
    colWidth = 305;
    cols: number;

    constructor(
        private element: ElementRef
    ) {
    }

    openProjectDetails(project) {
        this.openProjectDetail.next(project);
    }

    openCopyProject(project) {
        this.copyProject.next(project);
    }
    onOpenCommentsModule(projectId) {
        this.openCommentsModule.emit(projectId);
    }

    onResize() {
        this.cols = Math.floor(this.element.nativeElement.offsetParent.offsetWidth / this.colWidth);
    }
    trackByItemId(index, project) {
        return project.ITEM_ID;
    }
    selectedProjectsCardCount(event) {
        this.selectedProjectCardCount.next(event.length);
    }
    refresh() {
        this.refreshEmitter.next();
    }
    ngOnInit() {
        this.cols = Math.floor(this.element.nativeElement.offsetParent.offsetWidth / this.colWidth);
        if (this.dashboardMenuConfig.IS_CAMPAIGN_TYPE) {
            this.rowHeight = 300;
        } else if (this.dashboardMenuConfig.IS_PROJECT_TYPE) {
            this.rowHeight = 290;
        } else {
            this.rowHeight = 300;
        }
    }
}
