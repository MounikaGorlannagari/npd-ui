import { ProjectType, ProjectTypes } from 'mpm-library';

interface PMDashboardFilterObj {
    IS_DEFAULT: boolean;
    PROJECT_TYPE: ProjectType;
    VALUE: string;
    NAME: string;
    SEARCH_CONDITION: Array<any>;
    IS_PROJECT_TYPE?: boolean;
    IS_SHOWN?:boolean;
    IS_CAMPAIGN_TYPE?: boolean;
}

export enum PMDashboardFilterValues {
    MY_PROJECTS = 'MY_PROJECTS',
    ALL_PROJECTS = 'ALL_PROJECTS',
    MY_TEMPLATES = 'MY_TEMPLATES',
    ALL_TEMPLATES = 'ALL_TEMPLATES',
    MY_CAMPAIGNS = 'MY_CAMPAIGNS',
    ALL_CAMPAIGNS = 'ALL_CAMPAIGNS'
}

export const PMDashboardFilters: Array<PMDashboardFilterObj> = [/*{
    IS_DEFAULT: true,
    PROJECT_TYPE: ProjectTypes.CAMPAIGN,
    VALUE: PMDashboardFilterValues. MY_CAMPAIGNS,
    NAME: 'My Campaigns',
    SEARCH_CONDITION: [], // this.sharingService.getCurrentUserID()
    IS_SHOWN: true
},{
    IS_DEFAULT: false,
    PROJECT_TYPE: ProjectTypes.CAMPAIGN,
    VALUE: PMDashboardFilterValues. ALL_CAMPAIGNS,
    NAME: 'All Campaigns',
    SEARCH_CONDITION: [], // this.sharingService.getCurrentUserID()
    IS_SHOWN: true
},*/{
    IS_DEFAULT: true,
    PROJECT_TYPE: ProjectTypes.PROJECT,
    VALUE: PMDashboardFilterValues.MY_PROJECTS,
    NAME: 'My Projects',
    SEARCH_CONDITION: [], // this.sharingService.getCurrentUserID()
    IS_SHOWN: true
}, {
    IS_DEFAULT: false,
    PROJECT_TYPE: ProjectTypes.PROJECT,
    VALUE: PMDashboardFilterValues.ALL_PROJECTS,
    NAME: 'All Projects',
    SEARCH_CONDITION: [],
    IS_SHOWN: true
},/* {
    IS_DEFAULT: false,
    PROJECT_TYPE: ProjectTypes.TEMPLATE,
    VALUE: PMDashboardFilterValues.MY_TEMPLATES,
    NAME: 'My Templates',
    SEARCH_CONDITION: [], // this.sharingService.getCurrentUserID()
    IS_SHOWN: true
}, {
    IS_DEFAULT: false,
    PROJECT_TYPE: ProjectTypes.TEMPLATE,
    VALUE: PMDashboardFilterValues.ALL_TEMPLATES,
    NAME: 'All Templates',
    SEARCH_CONDITION: [],
    IS_SHOWN: true
},*/

];
