export const MenuConstants = {
    PROJECT_MANAGEMENT: 'ProjectManagement',
    OVERVIEW: 'Overview',
    PROJECTS: 'Projects',
    TASKS: 'Tasks',
    DELIVERABLES: 'Deliverables',
    ASSETS: 'Assets',
    COMMENTS: 'Comments',
    DELIVERABLE_TASK_VIEW: 'deliverableWorkView',
    RESOURCE_MANAGEMENT: 'ResourceManagement',
    REQUEST: 'Request'
};
