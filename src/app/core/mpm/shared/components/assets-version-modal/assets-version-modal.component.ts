import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { OTMMService, NotificationService, LoaderService, SharingService } from 'mpm-library';
import { ShareAssetsModalComponent } from '../share-assets-modal/share-assets-modal.component';

@Component({
    selector: 'app-assets-version-modal',
    templateUrl: './assets-version-modal.component.html',
    styleUrls: ['./assets-version-modal.component.scss']
})

export class AssetsVersionModalComponent implements OnInit {

    assets: Array<any>;

    constructor(
        public dialogRef: MatDialogRef<AssetsVersionModalComponent>,
        @Inject(MAT_DIALOG_DATA) public asset: any,
        private dialog: MatDialog,
        private otmmService: OTMMService,
        private notificationService: NotificationService,
        private loaderService: LoaderService,
        private sharingService: SharingService,
    ) { }


    close() {
        this.dialogRef.close(true);
    }

    shareAsset(event) {
        const currentUserEmail = this.sharingService.getCurrentUserEmailId();

        if (currentUserEmail && currentUserEmail.length > 0) {
            this.dialog.open(ShareAssetsModalComponent, {
                width: '50%',
                disableClose: true,
                data: event
            });
        } else {
            this.notificationService.error('The user does not have Email Id');
        }
    }

    ngOnInit() {
        this.loaderService.show();
        this.otmmService.getAssetVersionsByAssetId(this.asset.original_asset_id)
            .subscribe(versions => {
                this.assets = versions;
                this.loaderService.hide();
            }, error => {
                this.dialogRef.close(true);
                this.loaderService.hide();
                this.notificationService.error('Something went wrong while getting asset versions');
            });
    }

}
