import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-asset-details',
    templateUrl: './asset-details.component.html',
    styleUrls: ['./asset-details.component.scss']
})

export class AssetDetailsComponent implements OnInit {

    assetInheritedData;

    constructor(
        public dialogRef: MatDialogRef<AssetDetailsComponent>,
        @Inject(MAT_DIALOG_DATA) public asset: any
    ) { }

    close() {
        this.dialogRef.close(true);
    }

    ngOnInit() {
    }

}
