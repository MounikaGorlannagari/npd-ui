import { Component, OnInit, Inject, isDevMode, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharingService } from 'mpm-library';
import { MatPaginator } from '@angular/material/paginator';
import * as $ from 'jquery';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-asset-preview',
  templateUrl: './asset-preview.component.html',
  styleUrls: ['./asset-preview.component.scss']
})
export class AssetPreviewComponent implements OnInit {

  otmmBaseUrl;
  pageSize;
  page;
  totalAssetsCount;
  assetUrl;

  videoAssetUrl: SafeResourceUrl;
  pdfAssetUrl: SafeResourceUrl;

  @ViewChild(MatPaginator, { static: true }) matPaginator: MatPaginator;

  constructor(
    public dialogRef: MatDialogRef<AssetPreviewComponent>,
    @Inject(MAT_DIALOG_DATA) public asset: any,
    private sharingService: SharingService,
    public sanitizer: DomSanitizer
  ) { }

  close() {
    this.dialogRef.close(true);
  }

  pagination(pagination: any) {
    this.page = 1 + pagination.pageIndex;
    this.asset.asset = this.asset.data[pagination.pageIndex];
    if (this.asset && this.asset.asset && this.asset.asset.rendition_content && this.asset.asset.rendition_content.preview_content &&
      this.asset.asset.rendition_content.preview_content.url && this.asset.asset.content_type === 'VIDEO') {
      this.videoAssetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.otmmBaseUrl + this.asset.asset.rendition_content.preview_content.url.slice(1));
    }
    if (this.asset && this.asset.asset && this.asset.asset.rendition_content && (this.asset.asset.rendition_content.pdf_preview_content !== undefined)
      && (this.asset.asset.content_type !== 'VIDEO') && (this.asset.asset.mime_type.includes('pdf'))) {
      this.pdfAssetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.otmmBaseUrl + this.asset.asset.rendition_content.pdf_preview_content.url.slice(1));
    }
  }

  changePaginator() {
    this.pagination({ 'pageIndex': this.matPaginator.pageIndex });
  }

  ngAfterViewInit() {
    if ($(".asset-preview-paggination .mat-paginator-range-label")) {
      $(".asset-preview-paggination .mat-paginator-range-label").remove();
    }
  }

  ngOnInit(): void {
    this.pageSize = 1;
    this.totalAssetsCount = this.asset.data.length;
    this.page = 1 + this.asset.data.indexOf(this.asset.asset);
    this.otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
    this.matPaginator.page.subscribe(() => {
      this.changePaginator();
    });
    if (this.asset && this.asset.asset && this.asset.asset.rendition_content && this.asset.asset.rendition_content.preview_content &&
      this.asset.asset.rendition_content.preview_content.url && this.asset.asset.content_type === 'VIDEO') {
      this.videoAssetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.otmmBaseUrl + this.asset.asset.rendition_content.preview_content.url.slice(1));
    }
    if (this.asset && this.asset.asset && this.asset.asset.rendition_content && (this.asset.asset.rendition_content.pdf_preview_content !== undefined)
      && (this.asset.asset.content_type !== 'VIDEO') && (this.asset.asset.mime_type.includes('pdf'))) {
      this.pdfAssetUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.otmmBaseUrl + this.asset.asset.rendition_content.pdf_preview_content.url.slice(1));
    }
   
  }

}
