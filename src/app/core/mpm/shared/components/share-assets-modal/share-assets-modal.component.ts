import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MPMShareAssetsComponent } from 'src/app/core/mpm-lib/share-assets/share-assets.component';

@Component({
    selector: 'app-share-assets-modal',
    templateUrl: './share-assets-modal.component.html',
    styleUrls: ['./share-assets-modal.component.scss']
})

export class ShareAssetsModalComponent implements OnInit {

    @ViewChild(MPMShareAssetsComponent) shareAssetsComponent: MPMShareAssetsComponent;

    constructor(
        public dialogRef: MatDialogRef<ShareAssetsModalComponent>,
        @Inject(MAT_DIALOG_DATA) public assets: any,
    ) { }

    closeModal() {
        this.dialogRef.close(true);
    }

    close() {
        this.shareAssetsComponent.closeDialog();
    }

    ngOnInit(): void {

    }

}
