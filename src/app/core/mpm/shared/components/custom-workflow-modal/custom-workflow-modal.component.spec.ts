import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomWorkflowModalComponent } from './custom-workflow-modal.component';

describe('CustomWorkflowModalComponent', () => {
  let component: CustomWorkflowModalComponent;
  let fixture: ComponentFixture<CustomWorkflowModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomWorkflowModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomWorkflowModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
