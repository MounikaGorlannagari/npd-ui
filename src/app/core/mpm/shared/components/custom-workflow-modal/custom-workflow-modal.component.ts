import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationModalComponent, LoaderService, NotificationService, StatusTypes, TaskService, UtilService } from 'mpm-library';

@Component({
  selector: 'app-custom-workflow-modal',
  templateUrl: './custom-workflow-modal.component.html',
  styleUrls: ['./custom-workflow-modal.component.scss']
})
export class CustomWorkflowModalComponent implements OnInit {

  ruleList = [];
  taskData = [];
  projectId;
  enableInitiate;
  isRulesInitiated;
  existingRuleList = [];
  existingRuleListGroup = [];
  isTemplate;
  isCurrentTaskRule = false;
  projectRuleList = [];
  ruleListGroup = new Array();
  noPredecessorRuleList = [];

  constructor(
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private taskService: TaskService,
    private loaderService: LoaderService,
    private utilService: UtilService,
    private dialogRef: MatDialogRef<CustomWorkflowModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  close() {
    if (!this.isTemplate) {
      if (JSON.stringify(this.existingRuleListGroup) !== JSON.stringify(this.ruleListGroup)) {
        const ruleIds = [];
        this.ruleList.forEach(rule => {
          if (rule.isExist === false && rule.isInitialRule === true && rule.workflowRuleId && this.isRulesInitiated) {
            ruleIds.push({
              Id: rule.workflowRuleId
            });
          }
        });
        let message;
        if (ruleIds && ruleIds.length > 0) {
          message = 'Provided initial rules will be removed, click yes to proceed';
        } else {
          message = 'Are you sure you want to close?';
        }
        const dialogRef = this.dialog.open(ConfirmationModalComponent, {
          width: '40%',
          disableClose: true,
          data: {
            message: message,
            submitButton: 'Yes',
            cancelButton: 'No'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result && result.isTrue) {
            const isStatusRules = this.ruleList.find(rule => rule.isExist === false && rule.triggerType === 'STATUS') ? true : false;
            if (isStatusRules) {
              this.taskService.UpdateDateOnRuleInitiate(this.projectId).subscribe();
            }
            if (ruleIds) {
              this.taskService.deleteWorkflowRules(ruleIds).subscribe();
            }
            let triggerWorkflowAction = false;
            const actionRules = this.ruleList.filter(rule => rule.triggerType === 'ACTION');
            actionRules.forEach(actionRule => {
              const existingRule = this.existingRuleList.find(existingActionRule => existingActionRule.workflowRuleId === actionRule.workflowRuleId);
              if (existingRule) {
                if (existingRule.triggerData !== actionRule.triggerData) {
                  triggerWorkflowAction = true;
                }
              } else {
                triggerWorkflowAction = true;
              }
            });
            this.dialogRef.close(triggerWorkflowAction);
          }
        });
      } else {
        this.dialogRef.close(false);
      }
    } else {
      this.dialogRef.close(false);
    }
  }

  addRule() {
    let canAddRule = true;
    this.ruleListGroup.forEach(rule => {
      if (rule.isExist === false) {
        if (!rule.isRuleAdded) {
          canAddRule = false;
        }
      }
    });
    if (canAddRule) {
      if (this.data.currentTaskData) {
        this.ruleListGroup.push({
          currentTask: [this.data.currentTaskData.ID],
          triggerType: '',
          triggerData: '',
          targetType: '',
          targetData: '',
          workflowRuleId: '',
          isExist: false,
          isInitialRule: false,
          isTaskView: true
        });
      } else {
        this.ruleListGroup.push({
          currentTask: [],
          triggerType: '',
          triggerData: '',
          targetType: '',
          targetData: '',
          workflowRuleId: '',
          isExist: false,
          isInitialRule: false,
          isTaskView: false
        });
      }
    } else {
      this.notificationService.error('Kindly add the created rule first');
    }
  }

  removeRule(ruleData) {
    if(ruleData){
      for(let i=0; i < this.ruleListGroup.length; i++){
        if(JSON.stringify(this.ruleListGroup[i].currentTask.sort()) == JSON.stringify(ruleData.currentTask.sort())
          && JSON.stringify(this.ruleListGroup[i].workflowRuleId.sort()) == JSON.stringify(ruleData.workflowRuleId.sort())
          &&this.ruleListGroup[i].triggerData == ruleData.triggerData && this.ruleListGroup[i].triggerData == ruleData.triggerData
          &&this.ruleListGroup[i].targetData == ruleData.targetData && this.ruleListGroup[i].targetType == ruleData.targetType
          &&this.ruleListGroup[i].isInitialRule == ruleData.isInitialRule){
            this.ruleListGroup.splice(i,1);
            break;
        }
      }
    }else{
      this.ruleListGroup.pop();
    }
    if (this.ruleList && this.ruleList.length === 0) {
      this.enableInitiate = false;
    }
  }

  checkRuleDependencies(ruleListGroup){
    this.ruleListGroup = [];
    this.ruleListGroup = ruleListGroup;
  }

  getTaskWorkflowRules(taskId) {
    this.taskService.getWorkflowRulesByTask(taskId).subscribe(response => {
      this.loaderService.hide();
      if (response) {
        let workflowRules;
        if (response && response.length > 0) {
          workflowRules = response;
        } else {
          workflowRules = [response];
        }
        workflowRules.forEach(rule => {
          const currentTask = this.taskData.find(task => task.ID === rule.R_PO_TASK['Task-id'].Id);
          this.ruleList.push({
            currentTask: rule.R_PO_TASK['Task-id'].Id,
            triggerType: rule.TRIGGER_TYPE,
            triggerData: rule.TRIGGER_TYPE === 'STATUS' ? rule.R_PO_STATUS['MPM_Status-id'].Id : rule.R_PO_ACTIONS['MPM_Workflow_Actions-id'].Id,
            targetType: rule.TARGET_TYPE,
            targetData: rule.TARGET_TYPE === 'TASK' ? rule.R_PO_TARGET_TASK['Task-id'].Id : rule.R_PO_EVENT['MPM_Events-id'].Id,
            isExist: true,
            isTaskView: true,
            isInitialRule: this.utilService.getBooleanValue(rule.IS_INITIAL_RULE),
            workflowRuleId: rule['Workflow_Rules-id'].Id,
            isFinalTask: currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED
              || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
            isActiveTask: this.utilService.getBooleanValue(currentTask.IS_ACTIVE_TASK)
          });
          this.existingRuleList.push({
            currentTask: rule.R_PO_TASK['Task-id'].Id,
            triggerType: rule.TRIGGER_TYPE,
            triggerData: rule.TRIGGER_TYPE === 'STATUS' ? rule.R_PO_STATUS['MPM_Status-id'].Id : rule.R_PO_ACTIONS['MPM_Workflow_Actions-id'].Id,
            targetType: rule.TARGET_TYPE,
            targetData: rule.TARGET_TYPE === 'TASK' ? rule.R_PO_TARGET_TASK['Task-id'].Id : rule.R_PO_EVENT['MPM_Events-id'].Id,
            isExist: true,
            isTaskView: true,
            isInitialRule: this.utilService.getBooleanValue(rule.IS_INITIAL_RULE),
            workflowRuleId: rule['Workflow_Rules-id'].Id,
            isFinalTask: currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED
              || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
            isActiveTask: this.utilService.getBooleanValue(currentTask.IS_ACTIVE_TASK)
          });
        });
        this.enableInitiate = this.ruleList.find(rule => rule.isInitialRule === true) ? this.enableInitiate : false;
      } else {
        this.enableInitiate = false;
      }
    });
  }

  groupRulesOnSave(){
    this.ruleList = [];
    this.ngOnInit();
  }

  enableInitiateWorkflow() {
    this.enableInitiate = this.ruleListGroup.find(rule => rule.isInitialRule === true) ? true : false;
  }

  triggerWorkflow() {
    const unsavedRules = this.ruleList.find(rule => rule.workflowRuleId === '');
    if (unsavedRules) {
      this.notificationService.warn('Unsaved rules will not be part of workflow');
    }
    let isStatusRules;
    if (this.isRulesInitiated) {
      isStatusRules = this.ruleList.find(rule => rule.isExist === false && rule.triggerType === 'STATUS') ? true : false;
    } else {
      isStatusRules = this.ruleList.find(rule => rule.triggerType === 'STATUS') ? true : false;
    }
    this.loaderService.show();
    this.taskService.triggerWorkflow(this.projectId, isStatusRules).subscribe(response => {
      this.loaderService.hide();
      if (response) {
        let triggerWorkflowAction = false;
        const actionRules = this.ruleList.filter(rule => rule.triggerType === 'ACTION');
        actionRules.forEach(actionRule => {
          const existingRule = this.existingRuleList.find(existingActionRule => existingActionRule.workflowRuleId === actionRule.workflowRuleId);
          if (existingRule) {
            if (existingRule.triggerData !== actionRule.triggerData) {
              triggerWorkflowAction = true;
            }
          } else {
            triggerWorkflowAction = true;
          }
        });
        this.dialogRef.close(triggerWorkflowAction);
        this.notificationService.info('Workflow rules initiated successfully, dates will be aligned as per rule configured');
      }
    });
  }

  ngOnInit(): void {
    this.taskData = this.data.taskData;
    this.isTemplate = this.data.isTemplate;
    const activeTasks = this.taskData.find(task => task.IS_ACTIVE_TASK === 'true');
    const completedTasks = this.taskData.find(task => task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED || task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED);
    if (activeTasks || completedTasks) {
      this.isRulesInitiated = true;
      this.enableInitiate = false;
    } else {
      this.isRulesInitiated = false;
      this.enableInitiate = true;
    }
    const projectItemId = this.taskData && this.taskData[0] && this.taskData[0].PROJECT_ITEM_ID ? this.taskData[0].PROJECT_ITEM_ID : null;
    this.projectId = projectItemId ? projectItemId.split('.')[1] : null;
    if (this.data && this.data.currentTaskData) {
      this.isCurrentTaskRule = true;
      const taskId = this.data.currentTaskData.ID;
      const isApprovalTask = this.utilService.getBooleanValue(this.data.currentTaskData.IS_APPROVAL_TASK);
      if (taskId) {
        this.loaderService.show();
        this.taskService.getWorkflowRulesByProject(this.projectId).subscribe(response => {
          let hasRuleToActivate = !isApprovalTask;
          if (response) {
            let workflowRules;
            if (response && response.length > 0) {
              workflowRules = response;
            } else {
              workflowRules = [response];
            }
            if (isApprovalTask) {
              hasRuleToActivate = workflowRules.find(workflowRule => workflowRule.TARGET_TYPE === 'TASK' && workflowRule.R_PO_TARGET_TASK['Task-id'].Id === taskId) ? true : false;
            }
            workflowRules.forEach(rule => {
              const currentTask = this.taskData.find(task => task.ID === rule.R_PO_TASK['Task-id'].Id);
              this.projectRuleList.push({
                currentTask: rule.R_PO_TASK['Task-id'].Id,
                triggerType: rule.TRIGGER_TYPE,
                triggerData: rule.TRIGGER_TYPE === 'STATUS' ? rule.R_PO_STATUS['MPM_Status-id'].Id : rule.R_PO_ACTIONS['MPM_Workflow_Actions-id'].Id,
                targetType: rule.TARGET_TYPE,
                targetData: rule.TARGET_TYPE === 'TASK' ? rule.R_PO_TARGET_TASK['Task-id'].Id : rule.R_PO_EVENT['MPM_Events-id'].Id,
                isExist: true,
                isTaskView: true,
                isInitialRule: this.utilService.getBooleanValue(rule.IS_INITIAL_RULE),
                workflowRuleId: rule['Workflow_Rules-id'].Id,
                isFinalTask: currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED
                  || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
                isActiveTask: this.utilService.getBooleanValue(currentTask.IS_ACTIVE_TASK)
              });
            });
          }
          if (hasRuleToActivate) {
            this.getTaskWorkflowRules(taskId);
          } else {
            this.loaderService.hide();
            this.notificationService.error('As there is no rule to activate approval task, cannot configure rule for this task');
            this.dialogRef.close(false);
          }
        });
      }
    } else {
      if (this.projectId) {
        this.loaderService.show();
        this.taskService.getWorkflowRulesByProject(this.projectId).subscribe(response => {
          this.loaderService.hide();
          if (response) {
            let workflowRules;
            if (response && response.length > 0) {
              workflowRules = response;
            } else {
              workflowRules = [response];
            }
            workflowRules.forEach(rule => {
              const currentTask = this.taskData.find(task => task.ID === rule.R_PO_TASK['Task-id'].Id);
              this.ruleList.push({
                currentTask: rule.R_PO_TASK['Task-id'].Id,
                triggerType: rule.TRIGGER_TYPE,
                triggerData: rule.TRIGGER_TYPE === 'STATUS' ? rule.R_PO_STATUS['MPM_Status-id'].Id : rule.R_PO_ACTIONS['MPM_Workflow_Actions-id'].Id,
                targetType: rule.TARGET_TYPE,
                targetData: rule.TARGET_TYPE === 'TASK' ? rule.R_PO_TARGET_TASK['Task-id'].Id : rule.R_PO_EVENT['MPM_Events-id'].Id,
                isExist: true,
                isTaskView: false,
                isInitialRule: this.utilService.getBooleanValue(rule.IS_INITIAL_RULE),
                workflowRuleId: rule['Workflow_Rules-id'].Id,
                isFinalTask: currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED
                  || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
                isActiveTask: this.utilService.getBooleanValue(currentTask.IS_ACTIVE_TASK)
              });
              this.existingRuleList.push({
                currentTask: rule.R_PO_TASK['Task-id'].Id,
                triggerType: rule.TRIGGER_TYPE,
                triggerData: rule.TRIGGER_TYPE === 'STATUS' ? rule.R_PO_STATUS['MPM_Status-id'].Id : rule.R_PO_ACTIONS['MPM_Workflow_Actions-id'].Id,
                targetType: rule.TARGET_TYPE,
                targetData: rule.TARGET_TYPE === 'TASK' ? rule.R_PO_TARGET_TASK['Task-id'].Id : rule.R_PO_EVENT['MPM_Events-id'].Id,
                isExist: true,
                isTaskView: false,
                isInitialRule: this.utilService.getBooleanValue(rule.IS_INITIAL_RULE),
                workflowRuleId: rule['Workflow_Rules-id'].Id,
                isFinalTask: currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED
                  || currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
                isActiveTask: this.utilService.getBooleanValue(currentTask.IS_ACTIVE_TASK)
              });
            });
            this.projectRuleList = this.ruleList;
            this.ruleList.forEach(rule => {
              rule.currentTask = Array.isArray(rule.currentTask) ? rule.currentTask : [rule.currentTask];
              rule.workflowRuleId = Array.isArray(rule.workflowRuleId) ? rule.workflowRuleId : [rule.workflowRuleId];

            })
            //GROUPING THE RULES...
            this.ruleListGroup = [];
            this.ruleListGroup = [...this.ruleList];
            for(let i=0; i < this.ruleListGroup.length; i++){
              for(let j=i+1; j < this.ruleListGroup.length; j++){
                if(this.ruleListGroup[i].targetData == this.ruleListGroup[j].targetData 
                  && this.ruleListGroup[i].isInitialRule == this.ruleListGroup[j].isInitialRule
                  && this.ruleListGroup[i].targetType == this.ruleListGroup[j].targetType
                  && this.ruleListGroup[i].triggerType == this.ruleListGroup[j].triggerType
                  && this.ruleListGroup[i].triggerData == this.ruleListGroup[j].triggerData
                  && this.ruleListGroup[i].targetType == this.ruleListGroup[j].targetType)
                  {
                    this.ruleListGroup[i].currentTask = this.ruleListGroup[i].currentTask.concat(this.ruleListGroup[j].currentTask);
                    this.ruleListGroup[i].workflowRuleId = this.ruleListGroup[i].workflowRuleId.concat(this.ruleListGroup[j].workflowRuleId);
                    this.ruleListGroup.splice(j,1);
                    j -= 1;
                    }
              }
            }
            this.ruleListGroup.forEach(eachRule => this.existingRuleListGroup.push(eachRule));
            this.enableInitiate = this.ruleList.find(rule => rule.isInitialRule === true) ? this.enableInitiate : false;
          } else {
            this.enableInitiate = false;
          }
        });
      }
    }
    this.ruleListGroup.forEach(rule => {
      rule.hasPredecessor = true;
    })
    
  }
  

}
