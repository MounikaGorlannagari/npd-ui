import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../material.module';
import { ShareAssetsModalComponent } from './components/share-assets-modal/share-assets-modal.component';
import { MPMShareAssetsModule } from '../../mpm-lib/share-assets/share-assets.module';
import { MpmLibModule } from '../../mpm-lib/mpm-lib.module';
import { CustomWorkflowModalComponent } from './components/custom-workflow-modal/custom-workflow-modal.component';

@NgModule({
    declarations: [
        ShareAssetsModalComponent,
        CustomWorkflowModalComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        MPMShareAssetsModule,
        MpmLibModule
    ],
    entryComponents: [
        ShareAssetsModalComponent
    ]
})

export class SharedModule { }
