import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MpmComponent } from './mpm.component';
import { AuthGuard, AppGuard } from 'mpm-library';
import { HomeRouteComponent } from './home-route/home-route.component';
import { RoutingConstants } from './routingConstants';
import { NoAccessComponent } from './no-access/no-access.component';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: MpmComponent,
        children: [
            { path: '', component: HomeRouteComponent },
            {
                path: RoutingConstants.projectManagement,
                canActivate: [AppGuard],
                loadChildren: () => import('./project-management/project-management.module').then(m => m.ProjectManagementModule)
            },
            {
                canActivate: [AppGuard],
                path: RoutingConstants.prjectView,
                loadChildren: () => import('./project-view/project-view.module').then(m => m.ProjectViewModule)
            },
            {
                canActivate: [AppGuard],
                path: RoutingConstants.prjectView + '/:projectId',
                loadChildren: () => import('./project-view/project-view.module').then(m => m.ProjectViewModule)
            },
            {
                canActivate: [AppGuard],
                path: RoutingConstants.campaignView,
                loadChildren: () => import('./project-view/project-view.module').then(m => m.ProjectViewModule)
            },
            {
                canActivate: [AppGuard],
                path: RoutingConstants.campaignView + '/:campaignId',
                loadChildren: () => import('./project-view/project-view.module').then(m => m.ProjectViewModule)
            },
            {
                canActivate: [AppGuard],
                path: RoutingConstants.deliverableWorkView,
                loadChildren: () => import('./deliverables-task-view/deliverables-task-view.module').then(m => m.DeliverablesTaskViewModule)
            },
            {
                canActivate: [AppGuard],
                path: 'test',//remove this-check issue
                loadChildren: () => import('../mpm-lib/resource-management/resource-management.module').then(m => m.ResourceManagementModule)
            },
            {
                canActivate: [AppGuard],
                path: RoutingConstants.resourceManagement,
                //loadChildren: () => import('../mpm-lib/resource-management/resource-management.module').then(m => m.ResourceManagementModule)
                loadChildren: () => import('../npd-ui/npd-resource-management/npd-resource-management.module').then(m => m.NpdResourceManagementModule)
            },
            {
                path: RoutingConstants.requestManagement,
                canActivate: [AppGuard],
                loadChildren: () => import('../npd-ui/request-management/request-management.module').then(m => m.RequestManagementModule)
            },
            {
                canActivate: [AppGuard],
                path: RoutingConstants.requestView,
                loadChildren: () => import('../npd-ui/request-view/request-view.module').then(m => m.RequestViewModule)
            },
            {
                canActivate: [AppGuard],
                path: RoutingConstants.reports,
                loadChildren: () => import('../npd-ui/reports/reports.module').then(m => m.ReportsModule)
            },
            {
                canActivate: [AppGuard],
                path: RoutingConstants.requestView + '/:requestId',
                loadChildren: () => import('../npd-ui/request-view/request-view.module').then(m => m.RequestViewModule)
            },
            {
                path: '*',
                component: NotFoundComponent
            }
        ]
    },
    {
        path: RoutingConstants.noAccess,
        component: NoAccessComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MpmRoutingModule { }
