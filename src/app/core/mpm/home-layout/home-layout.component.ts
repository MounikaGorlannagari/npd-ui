import { Component, OnInit, Input, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { AppRouteService, NotificationService, SharingService, SearchChangeEventService, SearchDataService, UtilService, ApplicationConfigConstants, ExportDataTrayModalComponent, FacetChangeEventService } from 'mpm-library';
import { Router, ActivatedRoute } from '@angular/router';
import { RoutingConstants } from '../routingConstants';
import { AppDynamicConfig } from 'mpm-library';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { QdsTransferTrayModalComponent } from 'mpm-library';
import { DownloadsTransferTrayModalComponent } from 'mpm-library';

import { otmmServicesConstants } from 'mpm-library';
import { RouteService } from '../route.service';
import { StateService } from 'mpm-library';
import { MenuConstants } from '../shared/constants/menu.constants';
import { EmailNotificationPreferenceComponent } from 'mpm-library';
import { AppService, LoaderService } from 'mpm-library';
import { MPMFieldConstants } from 'mpm-library';
import { MultipleUserPreferencesDashboardComponent } from '../multiple-user-preferences-dashboard/multiple-user-preferences-dashboard.component';
import { NpdProjectService } from '../../npd-ui/npd-project-view/services/npd-project.service';
@Component({
    selector: 'app-home-layout',
    templateUrl: './home-layout.component.html',
    styleUrls: ['./home-layout.component.scss']
})

export class HomeLayoutComponent implements OnInit, OnDestroy {

    @Input() menus;

    showExternalApp = false;
    showQDSTray = false;
    iframeObj;
    private mobileQueryListener: () => void;
    mobileQuery: MediaQueryList;
    branding = {
        logoPath: ''
    };
    showMyApplication = true;
    appConfig: any;
    canDownload: boolean;

    selectedView;
    isListView;
    selectedSortOption;
    selectedSortOrder;
    pageNumber;
    pageSize;

    taskId;
    deliverableId;
    routeProjectId;
    routeTaskId;
    routeCampaignId;
    campaignId;
    isTemplate;
    isEditTask;
    viewByResource;
    isNewDeliverable;
    isEditDeliverable;
    facetData;

    queryParams;
    enableSystemNotification;

    resourceManagementPanel = false;
    currentMenuName: any;

    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private media: MediaMatcher,
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private appRouteService: AppRouteService,
        private sharingService: SharingService,
        private searchDataService: SearchDataService,
        private dialog: MatDialog,
        private utilService: UtilService,
        private routeService: RouteService,
        private stateService: StateService,
        private loaderService: LoaderService,
        private appService: AppService,
        private notificationService: NotificationService,
        public npdProjectService: NpdProjectService,
        private facetChangeEventService: FacetChangeEventService

        // private multipleUserPreferencesDashboardComponent:MultipleUserPreferencesDashboardComponent

    ) {
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
    }

    loggedInUserName: string = this.sharingService.getCurrentUserDisplayName();

    readUrlParams(callback) {
        this.activatedRoute.queryParams.subscribe(queryParams => {
            this.activatedRoute.parent.params.subscribe(routeParams => {
                callback(routeParams, queryParams);
            });
        });
    }

    onMenuClick(route) {
        this.facetChangeEventService.updateFacetSelectFacetCondition({});
        this.stateService.setRoute(this.queryParams.menuName, this.queryParams, null);
        const routeData = this.stateService.getRoute(route);
        const params = routeData && routeData.params ? routeData.params : null;
        if (params) {
            this.router.navigate([RoutingConstants.mpmBaseRoute + route], {
                queryParams: {
                    menuName: route,
                    viewName: params.queryParams.viewName,
                    isListView: params.queryParams.isListView,
                    sortBy: params.queryParams.sortBy,
                    sortOrder: params.queryParams.sortOrder,
                    pageNumber: params.queryParams.pageNumber,
                    pageSize: params.queryParams.pageSize,
                    searchName: params.queryParams.searchName,
                    savedSearchName: params.queryParams.savedSearchName,
                    advSearchData: params.queryParams.advSearchData,
                    // facetData: params.queryParams.facetData,
                    groupByFilter: params.queryParams.groupByFilter,
                    deliverableTaskFilter: params.queryParams.deliverableTaskFilter,
                    listDependentFilter: params.queryParams.listDependentFilter,
                    emailLinkId: params.queryParams.emailLinkId
                }
            });
        } else {
            this.router.navigate([RoutingConstants.mpmBaseRoute + route], {
                queryParams: {
                    menuName: route
                }
            });
        }
    }

    getSearchConditionList(searchConditionList) {
        this.searchDataService.setSearchData(searchConditionList);
        let advancedSearchData;
        const advancedSearchConditions = [];
        if (searchConditionList && Array.isArray(searchConditionList) && !(searchConditionList[0] && (searchConditionList[0].NAME || searchConditionList[0].keyword))) {
            if (this.queryParams && this.queryParams.menuName) {
                if (this.queryParams.menuName === MenuConstants.PROJECT_MANAGEMENT) {
                    this.stateService.setProjectAdvancedSearch(searchConditionList);
                } else if (this.queryParams.menuName === MenuConstants.TASKS) {
                    this.stateService.setTaskAdvancedSearch(searchConditionList);
                } else if (this.queryParams.menuName === MenuConstants.PROJECTS) {
                    this.stateService.setCampaignProjectAdvancedSearch(searchConditionList);
                } else if (this.queryParams.menuName === MenuConstants.DELIVERABLE_TASK_VIEW) {
                    this.stateService.setDeliverableTaskViewAdvancedSearch(searchConditionList);
                } else if (this.queryParams.menuName === MenuConstants.RESOURCE_MANAGEMENT) {
                    this.stateService.setResourceManagementAdvancedSearchData(searchConditionList);
                }
            }
            searchConditionList.forEach(condition => {
                const data = {
                    searchIdentifier: condition.searchIdentifier,
                    fieldId: condition.mapper_field_id,
                    mapperName: condition.mapper_name,
                    view: condition.view,
                    hasTwoValues: condition.filterRowData.hasTwoValues,
                    relationalOperatorName: condition.filterRowData.searchOperatorName,
                    value: condition.value
                };
                advancedSearchConditions.push(data);
            });
        }
        advancedSearchData = advancedSearchConditions && advancedSearchConditions.length > 0 ? JSON.stringify(advancedSearchConditions) : null;
        if (this.router.url.includes(RoutingConstants.projectManagement)) {
            this.routeService.goToDashboard(this.selectedView, this.isListView, this.selectedSortOption, this.selectedSortOrder, this.pageNumber, this.pageSize,
                searchConditionList && searchConditionList[0] && searchConditionList[0].keyword ? searchConditionList[0].keyword : null,
                searchConditionList && searchConditionList[0] && searchConditionList[0].NAME ? searchConditionList[0].NAME : null,
                advancedSearchData ? advancedSearchData : null, this.facetData ? this.facetData : null);
            this.invokeMenuCHange();
        } else if (this.router.url.includes(RoutingConstants.deliverableWorkView)) {
            this.routeService.goToDeliverableTaskView(this.queryParams, searchConditionList && searchConditionList[0] && searchConditionList[0].keyword ? searchConditionList[0].keyword : null,
                searchConditionList && searchConditionList[0] && searchConditionList[0].NAME ? searchConditionList[0].NAME : null,
                advancedSearchData ? advancedSearchData : null, this.queryParams.facetData);
        } else if (this.routeProjectId && !this.routeTaskId) {
            this.routeService.goToTaskView(this.isTemplate, this.routeProjectId, this.isListView, this.isEditTask, this.taskId, this.campaignId,
                this.selectedSortOption, this.selectedSortOrder, this.viewByResource,
                searchConditionList && searchConditionList[0] && searchConditionList[0].keyword ? searchConditionList[0].keyword : null,
                searchConditionList && searchConditionList[0] && searchConditionList[0].NAME ? searchConditionList[0].NAME : null, advancedSearchData ? advancedSearchData : null);
        } else if (this.routeProjectId && this.routeTaskId) {
            this.routeService.goToTaskDetails(this.isTemplate, this.routeProjectId, this.routeTaskId, this.isNewDeliverable, this.campaignId,
                this.isEditDeliverable, this.deliverableId, this.selectedSortOption, this.selectedSortOrder, this.viewByResource,
                searchConditionList && searchConditionList[0] && searchConditionList[0].keyword ? searchConditionList[0].keyword : null,
                searchConditionList && searchConditionList[0] && searchConditionList[0].NAME ? searchConditionList[0].NAME : null, advancedSearchData ? advancedSearchData : null);
        } else if (this.routeCampaignId) {
            this.routeService.goToCampaignProjectView(this.routeCampaignId, this.isListView, this.selectedSortOption, this.selectedSortOrder, this.pageNumber, this.pageSize,
                searchConditionList && searchConditionList[0] && searchConditionList[0].keyword ? searchConditionList[0].keyword : null,
                searchConditionList && searchConditionList[0] && searchConditionList[0].NAME ? searchConditionList[0].NAME : null, advancedSearchData ? advancedSearchData : null);
        } else if (this.router.url.includes(RoutingConstants.resourceManagement)) {
            this.routeService.goToResourceManagement(this.queryParams, searchConditionList && searchConditionList[0] && searchConditionList[0].keyword ? searchConditionList[0].keyword : null,
                searchConditionList && searchConditionList[0] && searchConditionList[0].NAME ? searchConditionList[0].NAME : null,
                advancedSearchData ? advancedSearchData : null, this.queryParams.facetData);
        }
    }

    logout() {
        this.appRouteService.goToLogout();
    }

    openQDSTransferTrayModal() {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        dialogConfig.data = {};

        this.dialog.open(QdsTransferTrayModalComponent, dialogConfig);
    }

    openMyDownloadsModal() {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        dialogConfig.data = {};

        this.dialog.open(DownloadsTransferTrayModalComponent, dialogConfig);
    }

    openExportDataModal() {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        dialogConfig.data = {};

        this.dialog.open(ExportDataTrayModalComponent, dialogConfig);
    }

    handleMenuChange() {
        if (this.router.url.includes(RoutingConstants.projectViewBaseRoute) || this.router.url.includes(RoutingConstants.campaignViewBaseRoute)) {
            this.menus.forEach(menu => {
                if (menu.LOCATION === RoutingConstants.projectManagement) {
                    menu.ACTIVE = true;
                    this.sharingService.setCurrentMenu(menu);
                } else {
                    menu.ACTIVE = false;
                }
            });
        } else if (this.router.url.includes(RoutingConstants.requestViewBaseRoute)) {
            if (this.menus && this.menus.length > 0) {
                this.menus.forEach(menu => {
                    if (menu.LOCATION === RoutingConstants.requestManagement || this.router.url.includes(menu.LOCATION)) { //if (this.router.url.includes(menu.LOCATION)) {
                        menu.ACTIVE = true;
                        this.sharingService.setCurrentMenu(menu);
                    } else {
                        menu.ACTIVE = false;
                    }
                });
            }
        } else {
            if (this.menus && this.menus.length > 0) {
                this.menus.forEach(menu => {
                    if (this.router.url.includes(menu.LOCATION)) {
                        menu.ACTIVE = true;
                        this.sharingService.setCurrentMenu(menu);
                    } else {
                        menu.ACTIVE = false;
                    }
                });
            }
        }
    }

    emailPreference() {
        const dialogRef = this.dialog.open(EmailNotificationPreferenceComponent, {
            width: '40%',
            height: '90%',
            disableClose: true,
            data: {
                message: 'Email Subscription',
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result && result.isTrue) {

            }
        });
    }

    savePagePreference() {
        let currentViewData: any = sessionStorage.getItem('CURRENT_VIEW');

        let requestObj;
        if (currentViewData) {
            currentViewData = JSON.parse(currentViewData);
            let fieldData;
            if (currentViewData.isListView) {
                let userPreferenceData: any = sessionStorage.getItem('CURRENT_USER_PREFERENCE-' + currentViewData.viewName);
                userPreferenceData = userPreferenceData ? JSON.parse(userPreferenceData) : null;
                const userPreferenceViewField = userPreferenceData && userPreferenceData.FIELD_DATA ? userPreferenceData.FIELD_DATA : null;
                userPreferenceViewField.forEach(field => {

                    const fieldId = Object.keys(field)[0];
                    if (fieldId === currentViewData.sortName) {
                        field[fieldId].IS_SORTABLE = true;
                        field[fieldId].SORT_ORDER = currentViewData.sortOrder
                    }
                    else {
                        field[fieldId].IS_SORTABLE = false;
                        field[fieldId].SORT_ORDER = null
                    }
                });
                if (currentViewData.viewName.includes("MEMBER")) {
                    fieldData = {
                        data: userPreferenceViewField,
                        pageSize: currentViewData.pageSize,
                        pageNumber: currentViewData.pageNumber,
                        groupBy: currentViewData.groupBy,
                        taskStatusFilter: currentViewData.taskStatusFilter,
                        FREEZE_COLUMN_COUNT: userPreferenceData.FREEZE_COLUMN_COUNT
                    }
                }
                else {
                    fieldData = {
                        data: userPreferenceViewField,
                        pageSize: currentViewData.pageSize,
                        pageNumber: currentViewData.pageNumber,
                        FREEZE_COLUMN_COUNT: userPreferenceData.FREEZE_COLUMN_COUNT
                    }
                }
            } else {
                let userPreferenceViewField = [];
                if (currentViewData.sortName) {
                    userPreferenceViewField = [{
                        [currentViewData.sortName]: {
                            IS_SORTABLE: true,
                            SORT_ORDER: currentViewData.sortOrder
                        }
                    }];
                }
                if (currentViewData.viewName.includes("MEMBER")) {
                    fieldData = {
                        data: userPreferenceViewField,
                        pageSize: currentViewData.pageSize,
                        pageNumber: currentViewData.pageNumber,
                        groupBy: currentViewData.groupBy,
                        taskStatusFilter: currentViewData.taskStatusFilter,
                        FREEZE_COLUMN_COUNT: MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
                    }
                }
                else {
                    fieldData = {
                        data: userPreferenceViewField,
                        pageSize: currentViewData.pageSize,
                        pageNumber: currentViewData.pageNumber,
                        FREEZE_COLUMN_COUNT: MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
                    }
                }

            }
            requestObj = {
                R_PO_PERSON: {
                    'Person-id': {
                        Id: this.sharingService.getCurrentPerson().Id
                    }
                },
                PREFERENCE_TYPE: 'VIEW',
                IS_LIST_VIEW: currentViewData.isListView,
                R_PO_MPM_VIEW_CONFIG: {
                    'MPM_View_Config-id': {
                        Id: currentViewData.viewId
                    }
                },
                R_PO_MPM_MENU_ID: {
                    'MPM_Menu-id': {
                        Id: this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id
                    }
                },
                FIELD_DATA: JSON.stringify(fieldData),
                FILTER_NAME: currentViewData.listfilter,

            };
            this.loaderService.show();
            let existingUserPreference;
            if (currentViewData.isPMView) {
                existingUserPreference = this.sharingService.getCurrentUserPreferenceByViewId(currentViewData.viewId);
            } else {
                existingUserPreference = this.sharingService.getCurrentUserPreferenceByList(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, currentViewData.listfilter);
            }
            if (currentViewData.viewName == "PM_PROJECT_VIEW") {
                let defaultUserPreference = sessionStorage.getItem('DEFAULT_USER_PREFERENCE-PM_PROJECT_VIEW')
                let activeUserPreferenceId = sessionStorage.getItem('ACTIVE_USER_PREFERENCE_ID')
                let activeUserPreferenceName = sessionStorage.getItem('ACTIVE_USER_PREFERENCE_NAME');

                const currentPreferenceId = activeUserPreferenceId ? activeUserPreferenceId : defaultUserPreference
                let IsActiveUserPreference

                if (defaultUserPreference && (!activeUserPreferenceId)) {
                    IsActiveUserPreference = true
                }
                else if (defaultUserPreference == activeUserPreferenceId) {
                    IsActiveUserPreference = true
                }
                else {
                    IsActiveUserPreference = false
                }
                requestObj = {
                    PersonId: this.sharingService.getCurrentPerson().Id,
                    PreferenceType: 'VIEW',
                    IsListView: currentViewData.isListView,
                    IsDefaultView: true,
                    ViewId: currentViewData.viewId,
                    MenuId: this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id,
                    FieldData: JSON.stringify(fieldData),
                    UserPreferenceId: currentPreferenceId,
                    FilterName: currentViewData.listfilter,
                    IsActiveUserPreference: IsActiveUserPreference
                };
                // this.appService.invokeRequest('http://schemas.acheron.com/mpm/core/workflow/bpm/1.0','UpdateIsActiveUserPreference',requestObj).subscribe(response=>{
                //     if(response){
                //     this.loaderService.hide();
                //       console.log('update success');
                //     }
                // })
                if (requestObj.UserPreferenceId == null) {
                    this.notificationService.error('Please Add a preference before saving!')
                    this.loaderService.hide();
                    return
                }
                this.appService.invokeRequest('http://schemas.acheron.com/mpm/core/workflow/bpm/1.0', 'UpdateIsActiveUserPreference', requestObj).subscribe(response => {
                    if (response) {
                        this.notificationService.success('User preference have been saved successfully');
                        this.loaderService.hide();
                    }
                })
                sessionStorage.setItem('ACTIVE_USER_PREFERENCE_NAME', activeUserPreferenceName.replace('*', ''))
                this.npdProjectService.setData();
            }
            else {
                const existingUserPreferenceId = existingUserPreference && existingUserPreference['MPM_User_Preference-id'] && existingUserPreference['MPM_User_Preference-id'].Id ?
                    existingUserPreference['MPM_User_Preference-id'].Id : null;
                this.appService.updateUserPreference(requestObj, existingUserPreferenceId).subscribe(response => {
                    this.loaderService.hide();
                    if (response) {
                        this.notificationService.success('User preference have been saved successfully');
                    }
                });
            }
        }
    }

    managePagePrefernce() {
        let currentViewData: any = sessionStorage.getItem('CURRENT_VIEW');
        if (currentViewData) {
            currentViewData = JSON.parse(currentViewData);
            const dialogRef = this.dialog.open(MultipleUserPreferencesDashboardComponent, {
                width: '40%',
                maxHeight: 'calc(100vh - 50px)',
                height: 'auto',
                data: {
                    preference: currentViewData
                }
            })
        }
    }

    resetPagePreference() {

        let currentViewData: any = sessionStorage.getItem('CURRENT_VIEW');
        currentViewData = JSON.parse(currentViewData);
        if (currentViewData) {
            if (currentViewData.viewName == "PM_PROJECT_VIEW") {
                let defaultUserPreference = sessionStorage.getItem('DEFAULT_USER_PREFERENCE-PM_PROJECT_VIEW')
                let activeUserPreference = sessionStorage.getItem('ACTIVE_USER_PREFERENCE_ID')
                const currentPreferenceId = activeUserPreference ? activeUserPreference : defaultUserPreference
                if (currentPreferenceId == null) {
                    this.notificationService.error('No User Preference saved')
                    this.loaderService.hide();
                    return
                } else {
                    this.npdProjectService.getResetTrigger();
                    return
                }
            }
            let fieldData;
            sessionStorage.removeItem('CURRENT_USER_PREFERENCE-' + currentViewData.viewName);
            let userPreference;
            if (currentViewData.isPMView) {
                userPreference = this.sharingService.getCurrentUserPreferenceByViewId(currentViewData.viewId);
            } else {
                userPreference = this.sharingService.getCurrentUserPreferenceByList(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, currentViewData.listfilter);
            }
            const userPreferenceId = userPreference && userPreference['MPM_User_Preference-id'] && userPreference['MPM_User_Preference-id'].Id ?
                userPreference['MPM_User_Preference-id'].Id : null;
            if (userPreferenceId) {
                this.loaderService.show();
                this.appService.deleteUserPreference(userPreferenceId).subscribe(response => {
                    this.loaderService.hide();
                    if (response) {
                        this.sharingService.onResetView(currentViewData.viewId);
                        this.notificationService.success('User preference have been reset successfully');
                    }
                });
            } else {
                this.sharingService.onResetView(currentViewData.viewId);
                this.notificationService.success('User preference have been reset successfully');
            }
        }
    }

    invokeMenuCHange() {
        this.router.events.subscribe(res => {
            this.handleMenuChange();
        });
    }

    ngOnDestroy(): void {
        // tslint:disable-next-line: deprecation
        this.mobileQuery.removeListener(this.mobileQueryListener);
    }

    getBranding() {
        this.branding.logoPath = AppDynamicConfig.getAppLogo();
    }

    ngOnInit() {
        /*this.router.events.subscribe(res => {
            console.log(res);
            this.handleMenuChange();
        });*/
        this.menus = this.menus.sort((a, b) => {
            return a.SEQUENCE - b.SEQUENCE;
        });
        this.appConfig = this.sharingService.getAppConfig();
        this.showMyApplication = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.SHOW_MY_APPLICATION]);
        this.canDownload = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_ASSET_DOWNLOAD]);
        this.showQDSTray = (this.sharingService.getMediaManagerConfig().enableQDS && otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient);
        this.enableSystemNotification = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_SYSTEM_NOTIFICATION]);

        // if (this.activatedRoute.firstChild) {
        /* this.activatedRoute.firstChild.params.subscribe(params => {
            console.log(params);
            this.routeProjectId = params.projectId;
        }); */
        // }
        // if (this.activatedRoute.firstChild.firstChild.firstChild) {
        /* this.activatedRoute.firstChild.firstChild.firstChild.params.subscribe(params => {
            console.log(params);
            this.routeTaskId = params.taskId;
        }); */
        // } else {
        //     this.routeTaskId = null;
        // }
        this.readUrlParams((routeParams, queryParams) => {
            if (this.activatedRoute.firstChild) {
                this.activatedRoute.firstChild.params.subscribe(params => {
                    this.routeProjectId = params.projectId;
                    this.routeCampaignId = params.campaignId;
                });
            }
            if (this.activatedRoute.firstChild && this.activatedRoute.firstChild.firstChild && this.activatedRoute.firstChild.firstChild.firstChild) {
                this.activatedRoute.firstChild.firstChild.firstChild.params.subscribe(params => {
                    this.routeTaskId = params.taskId;
                });
            }
            this.queryParams = queryParams;
            this.selectedView = queryParams.viewName;
            this.isListView = queryParams.isListView;
            this.selectedSortOption = queryParams.sortBy;
            this.selectedSortOrder = queryParams.sortOrder;
            this.pageNumber = queryParams.pageNumber;
            this.pageSize = queryParams.pageSize;

            this.taskId = queryParams.taskId;
            this.deliverableId = queryParams.deliverableId;
            this.campaignId = queryParams.campaignId;
            this.isTemplate = queryParams.isTemplate;
            this.isEditTask = queryParams.isEditTask;
            this.viewByResource = queryParams.viewByResource;
            this.isNewDeliverable = queryParams.isNewDeliverable;
            this.isEditDeliverable = queryParams.isEditDeliverable;

            this.facetData = queryParams.facetData;
            this.currentMenuName = this.queryParams.menuName

            this.handleMenuChange();
            if (this.menus.length === 1) {
                this.menus.forEach(menu => {
                    if (menu.LOCATION === MenuConstants.RESOURCE_MANAGEMENT) {
                        this.resourceManagementPanel = true;
                    }
                });
            }
            this.getBranding();
            if (queryParams.isCreativeReview && queryParams.isCreativeReview === 'true' && queryParams.crData) {
                this.iframeObj = JSON.parse(queryParams.crData);
                this.showExternalApp = true;
            } else {
                this.showExternalApp = false;
            }
        });
    }
}
