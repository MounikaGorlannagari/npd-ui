import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RoutingConstants } from './routingConstants';
import { MPM_MENU_TYPES, SharingService } from 'mpm-library';
import { StateService } from 'mpm-library';
import { MenuConstants } from './shared/constants/menu.constants';
import { RequestConstant } from '../npd-ui/request-management/constants/request-constant';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class RouteService {
  routingConstants = RoutingConstants;

  constructor(
    public router: Router,
    public sharingService: SharingService,
    public stateService: StateService,
    private http:HttpClient
  ) {}

  goToDashboard(
    viewName,
    isListView,
    sortBy,
    sortOrder,
    pageNumber,
    pageSize,
    searchName,
    savedSearchName,
    advSearchData,
    facetData
  ) {
    this.router.navigate(['apps/mpm/ProjectManagement'], {
      queryParams: {
        menuName: 'ProjectManagement',
        viewName,
        isListView,
        sortBy,
        sortOrder,
        pageNumber,
        pageSize,
        searchName,
        savedSearchName,
        advSearchData,
        facetData,
      },
    });
  }

  goToProjectView(isTemplate, campaignId) {
    this.router.navigate(
      [this.routingConstants.projectViewBaseRoute + 'Overview'],
      {
        queryParams: {
          menuName: 'Overview',
          isTemplate,
          campaignId,
        },
      }
    );
  }

  goToCampaignView() {
    this.router.navigate(
      [this.routingConstants.campaignViewBaseRoute + 'Overview'],
      {
        queryParams: {
          menuName: 'Overview',
        },
      }
    );
  }

  goToProjectDetailView(projectId, isTemplate, campaignId) {
    this.router.navigate(
      [this.routingConstants.projectViewBaseRoute + projectId + '/Overview'],
      {
        queryParams: {
          menuName: 'Overview',
          isTemplate,
          campaignId,
        },
      }
    );
  }
  goToProjectDashBoardInNewTab(
    viewName,
    isListView,
    sortBy,
    sortOrder,
    pageNumber,
    pageSize,
    searchName,
    savedSearchName,
    advSearchData,
    facetData
  ) {
    const url =
      '#' +
      this.routingConstants.mpmBaseRoute +
      this.routingConstants.projectManagement +
      '?&menuName=' +
      this.routingConstants.projectManagement +
      '&viewName=' +
      viewName +
      '&isListView=' +
      isListView +
      '&pageNumber=' +
      pageNumber +
      '&pageSize=' +
      pageSize +
      '&facetData=' +
      facetData;
    window.open(url, '_blank');
  }
  goToProjectDetailViewNewTab(projectId, isTemplate, campaignId) {
    const url =
      '#' +
      this.routingConstants.projectViewBaseRoute +
      projectId +
      '/Overview?menuName=Overview&isTemplate=' +
      isTemplate;
    window.open(url, '_blank');
  }

  /**
   * @author JeevaR
   * @param projectId 
   */
  goToMPMProjectAssetViewNewTab(projectId) {
    let environment:any={};
      if(window.location.hostname === 'mmqa.monsterenergy.com'){
        environment.gatewayProtocol='https://';
        environment.gatewayHost = 'mmqa.monsterenergy.com';
        environment.psHome='bpm',
        environment.mpmOrganizationName='MPM'
        environment.mpmPsDeployedPath='portal'
      }
      else if (window.location.hostname === 'mm.monsterenergy.com') {
        environment.gatewayProtocol = 'https://';
        environment.gatewayHost = 'mm.monsterenergy.com';
        environment.psHome = 'bpm';
        environment.mpmOrganizationName = 'MPM';
        environment.mpmPsDeployedPath = 'portal';
      } else {
        environment.gatewayProtocol = 'https://';
        environment.gatewayHost = window.location.hostname;
        environment.psHome = 'home';
        environment.mpmOrganizationName = 'MPM';
        environment.mpmPsDeployedPath = 'portal';
      }
      const url =
        environment.gatewayProtocol +
        environment.gatewayHost +
        '/' +
        environment.psHome +
        '/' +
        environment.mpmOrganizationName +
        '/' +
        environment.mpmPsDeployedPath +
        '#' +
        this.routingConstants.projectViewBaseRoute +
        projectId +
        '/Assets?menuName=Assets&isTemplate=false';
      window.open(url, '_blank', 'noopener=true');
    
  }

  goToCampaignDetailView(campaignId) {
    this.router.navigate(
      [this.routingConstants.campaignViewBaseRoute + campaignId + '/Overview'],
      {
        queryParams: {
          menuName: 'Overview',
        },
      }
    );
  }

  goToProjectViewMenus(menuName, urlParams, isTemplate, projectId, campaignId) {
    this.stateService.removeRoute(MenuConstants.DELIVERABLES);
    if (projectId) {
      const type =
        menuName === MenuConstants.PROJECTS || menuName === MenuConstants.TASKS
          ? menuName
          : 'Project' + menuName;
      const route = this.stateService.getRoute(type);
      const params = route && route.params ? route.params : null;
      if (params) {
        this.router.navigate(
          [
            this.routingConstants.projectViewBaseRoute +
              projectId +
              '/' +
              menuName,
          ],
          {
            queryParams: {
              menuName,
              isTemplate,
              campaignId,
              isListView: params.queryParams.isListView,
              sortBy: params.queryParams.sortBy,
              sortOrder: params.queryParams.sortOrder,
              pageNumber: params.queryParams.pageNumber,
              pageSize: params.queryParams.pageSize,
              searchName: params.queryParams.searchName,
              savedSearchName: params.queryParams.savedSearchName,
              advSearchData: params.queryParams.advSearchData,
              viewByResource: params.queryParams.viewByResource,
              isEditTask: params.queryParams.isEditTask,
              taskId: params.queryParams.taskId,
            },
          }
        );
      } else {
        this.router.navigate(
          [
            this.routingConstants.projectViewBaseRoute +
              projectId +
              '/' +
              menuName,
          ],
          {
            queryParams: {
              menuName,
              isTemplate,
              campaignId,
            },
          }
        );
      }
    } else {
      this.router.navigate(
        [this.routingConstants.projectViewBaseRoute + menuName],
        {
          queryParams: {
            menuName,
            isTemplate,
            campaignId,
          },
        }
      );
    }
  }

  goToCampaignViewMenus(menuName, campaignId) {
    if (campaignId) {
      const type =
        menuName === MenuConstants.PROJECTS || menuName === MenuConstants.TASKS
          ? menuName
          : 'Campaign' + menuName;
      const route = this.stateService.getRoute(type);
      const params = route && route.params ? route.params : null;
      if (params) {
        this.router.navigate(
          [
            this.routingConstants.campaignViewBaseRoute +
              campaignId +
              '/' +
              menuName,
          ],
          {
            queryParams: {
              menuName,
              isListView: params.queryParams.isListView,
              sortBy: params.queryParams.sortBy,
              sortOrder: params.queryParams.sortOrder,
              pageNumber: params.queryParams.pageNumber,
              pageSize: params.queryParams.pageSize,
              searchName: params.queryParams.searchName,
              savedSearchName: params.queryParams.savedSearchName,
              advSearchData: params.queryParams.advSearchData,
            },
          }
        );
      } else {
        this.router.navigate(
          [
            this.routingConstants.campaignViewBaseRoute +
              campaignId +
              '/' +
              menuName,
          ],
          {
            queryParams: {
              menuName: menuName,
            },
          }
        );
      }
    } else {
      this.router.navigate(
        [this.routingConstants.campaignViewBaseRoute + menuName],
        {
          queryParams: {
            menuName: menuName,
          },
        }
      );
    }
  }

  goToTaskView(
    isTemplate,
    projectId,
    isListView,
    isEditTask,
    taskId,
    campaignId,
    sortBy,
    sortOrder,
    viewByResource,
    searchName,
    savedSearchName,
    advSearchData
  ) {
    this.router.navigate(
      [this.routingConstants.projectViewBaseRoute + projectId + '/Tasks'],
      {
        queryParams: {
          menuName: 'Tasks',
          isTemplate,
          isListView,
          isEditTask,
          taskId,
          campaignId,
          sortBy,
          sortOrder,
          viewByResource,
          searchName,
          savedSearchName,
          advSearchData,
        },
      }
    );
  }

  goToCampaignProjectView(
    campaignId,
    isListView,
    sortBy,
    sortOrder,
    pageNumber,
    pageSize,
    searchName,
    savedSearchName,
    advSearchData
  ) {
    this.router.navigate(
      [this.routingConstants.campaignViewBaseRoute + campaignId + '/Projects'],
      {
        queryParams: {
          menuName: 'Projects',
          isListView,
          sortBy,
          sortOrder,
          pageNumber,
          pageSize,
          searchName,
          savedSearchName,
          advSearchData,
        },
      }
    );
  }

  goToTaskDetails(
    isTemplate,
    projectId,
    taskId,
    isNewDeliverable,
    campaignId,
    isEditDeliverable,
    deliverableId,
    sortBy,
    sortOrder,
    viewByResource,
    searchName,
    savedSearchName,
    advSearchData
  ) {
    this.router.navigate(
      [
        this.routingConstants.projectViewBaseRoute +
          projectId +
          '/Tasks/' +
          taskId,
      ],
      {
        queryParams: {
          menuName: 'Deliverables',
          isTemplate,
          isNewDeliverable,
          campaignId,
          isEditDeliverable,
          deliverableId,
          sortBy,
          sortOrder,
          viewByResource,
          searchName,
          savedSearchName,
          advSearchData,
        },
      }
    );
  }

  getTaskRouting(isTemplate, projectId) {
    return {
      url: this.routingConstants.projectViewBaseRoute + projectId + '/Tasks',
      params: {
        isTemplate,
      },
    };
  }

  getTaskDetailRouting(isTemplate, projectId, taskId) {
    return {
      url:
        this.routingConstants.projectViewBaseRoute +
        projectId +
        '/Tasks/' +
        taskId,
      params: {
        isTemplate: isTemplate ? isTemplate : false,
      },
    };
  }

  getAssetRouting(projectId) {
    return {
      url: this.routingConstants.projectViewBaseRoute + projectId + '/Assets',
    };
  }

  openCR(crData) {
    this.router.navigate([this.routingConstants.projectViewBaseRoute], {
      queryParams: {
        isCreativeReview: true,
        crData: JSON.stringify(crData),
      },
    });
  }

  gotoProjectAssets(isTemplate, projectId, campaignId, pageNumber, pageSize) {
    this.router.navigate(
      [this.routingConstants.projectViewBaseRoute + projectId + '/' + 'Assets'],
      {
        queryParams: {
          menuName: 'Assets',
          isTemplate,
          campaignId,
          pageNumber,
          pageSize,
        },
      }
    );
  }

  gotoCampaignAssets(isTemplate, campaignId, pageNumber, pageSize) {
    this.router.navigate(
      [
        this.routingConstants.campaignViewBaseRoute +
          campaignId +
          '/' +
          'Assets',
      ],
      {
        queryParams: {
          menuName: 'Assets',
          isTemplate,
          pageNumber,
          pageSize,
        },
      }
    );
  }

  goToComments(projectId, isTemplate, campaignId) {
    if (isTemplate) {
      this.router.navigate(
        [this.routingConstants.campaignViewBaseRoute + projectId + '/Comments'],
        {
          queryParams: {
            menuName: 'Comments',
            campaignId,
          },
          // isTemplate,
        }
      );
    } else {
      this.router.navigate(
        [this.routingConstants.projectViewBaseRoute + projectId + '/Comments'],
        {
          queryParams: {
            menuName: 'Comments',
            isTemplate,
            campaignId,
          },
        }
      );
    }
  }
  goToBackRoute(backRouteObj) {
    if (backRouteObj && backRouteObj.url) {
      this.router.navigate([backRouteObj.url], {
        queryParams: backRouteObj.params,
      });
    }
  }

  goToDeliverableTaskView(
    queryParams,
    searchName,
    savedSearchName,
    advSearchData,
    facetData
  ) {
    this.router.navigate(
      [
        this.routingConstants.mpmBaseRoute +
          '/' +
          this.routingConstants.deliverableWorkView,
      ],
      {
        queryParams: {
          menuName: 'deliverableWorkView',
          groupByFilter: queryParams.groupByFilter,
          deliverableTaskFilter: queryParams.deliverableTaskFilter,
          listDependentFilter: queryParams.listDependentFilter,
          emailLinkId: queryParams.emailLinkId,
          sortBy: queryParams.sortBy,
          sortOrder: queryParams.sortOrder,
          pageNumber: queryParams.pageNumber,
          pageSize: queryParams.pageSize,
          searchName: searchName,
          savedSearchName: savedSearchName,
          advSearchData: advSearchData,
          facetData: facetData,
        },
      }
    );
  }

  routeTo(
    menu: string,
    filterBy: string,
    groupBy: string,
    sortBy: string,
    sortType: string,
    search: string,
    pageSize: any,
    page: any,
    view?: any
  ) {
    this.router.navigate([this.routingConstants.mpmBaseRoute + menu], {
      queryParams: {
        groupBy,
        filterBy,
        search,
        sortBy,
        sortType,
        pageSize,
        page,
        view,
      },
    });
  }

  handleViewRouting(viewName) {
    const menusForUser = this.sharingService.getMenuByType(
      MPM_MENU_TYPES.HEADER
    );
    const isAvailableRoute = menusForUser.some(
      (menu) => menu.LOCATION === viewName
    );
    if (!isAvailableRoute) {
      const route = this.stateService.getRoute(viewName);
      const params = route && route.params ? route.params : null;
      if (params) {
        this.router.navigate([RoutingConstants.mpmBaseRoute], {
          queryParams: {
            menuName: viewName,
            viewName: params.queryParams.viewName,
            isListView: params.queryParams.isListView,
            sortBy: params.queryParams.sortBy,
            sortOrder: params.queryParams.sortOrder,
            pageNumber: params.queryParams.pageNumber,
            pageSize: params.queryParams.pageSize,
            searchName: params.queryParams.searchName,
            savedSearchName: params.queryParams.savedSearchName,
            advSearchData: params.queryParams.advSearchData,
            facetData: params.queryParams.facetData,
            groupByFilter: params.groupByFilter,
            deliverableTaskFilter: params.deliverableTaskFilter,
            listDependentFilter: params.listDependentFilter,
            emailLinkId: params.emailLinkId,
          },
        });
      } else {
        this.router.navigate([RoutingConstants.mpmBaseRoute]);
      }
    }
    return;
  }

  // Resource Management MPMV3-2056

  goToResourceManagement(
    queryParams,
    searchName,
    savedSearchName,
    advSearchData,
    facetData
  ) {
    this.router.navigate(
      [
        this.routingConstants.mpmBaseRoute +
          this.routingConstants.resourceManagement,
      ],
      {
        queryParams: {
          menuName: 'ResourceManagement',
          groupByFilter: queryParams.groupByFilter,
          sortBy: queryParams.sortBy,
          sortOrder: queryParams.sortOrder,
          pageNumber: queryParams.pageNumber,
          pageSize: queryParams.pageSize,
          searchName: searchName,
          savedSearchName: savedSearchName,
          advSearchData: advSearchData,
          facetData: facetData,
        },
      }
    );
  }

  goToRequestView(view, menuName) {
    this.router.navigate(
      [this.routingConstants.requestViewBaseRoute + view + '/requestDetails'],
      {
        queryParams: {
          menu: menuName,
        },
      }
    );
  }

  goToRequestDashboard(menu?: string, listDependentFilter?) {
    if (menu && listDependentFilter) {
      this.router.navigate(['apps/mpm/Request'], {
        queryParams: {
          menu: menu,
          listDependentFilter: listDependentFilter,
        },
      });
    } else if (menu) {
      this.router.navigate(['apps/mpm/Request'], {
        queryParams: {
          menu: menu,
        },
      });
    } else {
      this.router.navigate(['apps/mpm/Request']);
    }
  }

  goToRequestDetailView(requestId, params, requestType?, requestSubjectType?) {
    let url = '';
    /* if (requestType === RequestConstant.LFL_REQUEST_TYPE_VALUE) {
            if (requestSubjectType === LocalizationConstants.TASK_ACTIVITY_PROXY_GEN) {
                url = this.routingConstants.requestViewBaseRoute + requestId + '/proxyDetails/';
            } else {
                url = this.routingConstants.requestViewBaseRoute + '/' + requestId + '/lflRequestDetails/';
            } 
        } else if (requestType && requestType === RequestConstant.VERSION_REQUEST_TYPE_VALUE){
            url = this.routingConstants.requestViewBaseRoute + '/' + requestId + '/versionRequestDetails/';
        } else if (requestType && requestType === RequestConstant.MATERIAL_REQUEST_TYPE_VALUE){
            url = this.routingConstants.requestViewBaseRoute + '/' + requestId + '/materialRequestDetails/';
        } else{
            url = this.routingConstants.requestViewBaseRoute + '/' + requestId + '/'+ requestType+ '/requestDetails/';
        } */
    url =
      this.routingConstants.requestViewBaseRoute +
      '/' +
      requestId +
      '/' +
      requestType +
      '/requestDetails/';
    this.router.navigate([url], {
      queryParams: params,
    });
    // this.router.navigate([this.routingConstants.requestViewBaseRoute+'/requestDetails/'+ requestId,  {title: params['type']}]);
  }

  goToRequestViewMenus(menuName, params, requestId) {
    const queryParams = params;
    if (requestId) {
      this.router.navigate(
        [
          this.routingConstants.requestViewBaseRoute +
            requestId +
            '/' +
            menuName,
        ],
        {
          queryParams,
        }
      );
    } else {
      this.router.navigate(
        [this.routingConstants.requestViewBaseRoute + menuName],
        {
          queryParams,
        }
      );
    }
  }

  /**
   * @author JeevaR
   * @returns envConfig 
   */
  getEnvironmentConfig():Observable<any>{
    const headers = new HttpHeaders()
      // .set('content-type', 'application/json')
      // .set('Access-Control-Allow-Origin', '*');
      return new Observable(observer =>{
        this.http.get('/assets/envconfig/envConfig.json',{withCredentials:true}).subscribe(res=>{
         observer.next(res);
         observer.complete();
        }, error => {
            observer.error(error);
        });
      }) 
  }
}
