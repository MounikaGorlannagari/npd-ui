import { Component, OnInit } from '@angular/core';
import { SharingService } from 'mpm-library';
import { MPM_MENU_TYPES } from 'mpm-library';

@Component({
    selector: 'app-mpm',
    templateUrl: './mpm.component.html',
    styleUrls: ['./mpm.component.scss']
})

export class MpmComponent implements OnInit {

    constructor(
        private sharingService: SharingService
    ) { }

    menus: any[];

    ngOnInit() {
        this.menus = this.sharingService.getMenuByType(MPM_MENU_TYPES.HEADER);
    }

}
