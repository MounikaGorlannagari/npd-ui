import { LiveAnnouncer } from '@angular/cdk/a11y';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { AppService, ConfirmationModalComponent, LoaderService, MPMFieldConstants, NotificationService, SessionStorageConstants, SharingService } from 'mpm-library';
import { forkJoin, Observable } from 'rxjs';
import { NpdProjectService } from '../../npd-ui/npd-project-view/services/npd-project.service';
@Component({
  selector: 'app-multiple-user-preferences-dashboard',
  templateUrl: './multiple-user-preferences-dashboard.component.html',
  styleUrls: ['./multiple-user-preferences-dashboard.component.scss']
})


export class MultipleUserPreferencesDashboardComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<string[]>;  
  preferenceNameList: any=[];
  projectDashUserPref: any[];
  disableSort: boolean;

  
  constructor(private  dialogRef:  MatDialogRef<MultipleUserPreferencesDashboardComponent>, 
    @Inject(MAT_DIALOG_DATA) public  data:  any,
    public dialog: MatDialog,
    public sharingService : SharingService,
    public appService: AppService,
    private loaderService: LoaderService,
    private notificationService: NotificationService,
    private npdProjectService:NpdProjectService,
    private _liveAnnouncer: LiveAnnouncer
    ) {}

  dataSource = new MatTableDataSource()
  // dataSource
  displayedColumns: string[] = ['Select','USER_PREFERENCE_NAME','Default', 'Actions'];
  defaultPreference;
  newPrefernceName: FormControl;
  userPreferNameEntered: any;
  userPreferenceList=[]
  totalListDataCount: number=0;
  selectedRow;
  selection = new SelectionModel<any>(true, []);
  disableAction: boolean;
  matMenuTimer: any;

    
  ngOnInit(): void {
    this.getCurrentUserPreference()
  }

  public  closeMe() {
      this.dialogRef.close();
  }

    captureEnteredPrefName(event){
       this.userPreferNameEntered = event      
    }

    //Getting the list of User Preference for Logged In User
  getCurrentUserPreference(){
      this.loaderService.show();
      this.dataSource.data=[]
      this.userPreferNameEntered=''
      this.disableAction=false
      this.preferenceNameList=[]
      this.userPreferenceList=[]
      this.projectDashUserPref=[]
      const parameter = {
          UserId: this.sharingService.getcurrentUserItemID()
      };
      let defaultUserPrefIndex
      let activeUserPref=sessionStorage.getItem('ACTIVE_USER_PREFERENCE_ID')
      this.appService.invokeRequest('http://schemas/AcheronMPMCore/MPM_User_Preference/operations', 'GetMPMUserPreferenceByUser', parameter).subscribe(response => {
          this.userPreferenceList = response?.MPM_User_Preference
          if(this.userPreferenceList?.length>1){
          this.userPreferenceList.forEach(list=>{
              if(list.FILTER_NAME=='All Projects' || list.FILTER_NAME =='My Projects'){
                this.projectDashUserPref.push(list)
              }
          })
              this.projectDashUserPref.forEach(list=>{
              list.isEditable=false;
              list.isActive=false;
              if(list['MPM_User_Preference-id'].Id==activeUserPref){ 
                list.isActive=true;
              }
              if(list?.IS_ACTIVE_USER_PREFERENCE=='true'){
                defaultUserPrefIndex= this.projectDashUserPref.indexOf(list)
                this.defaultPreference=list
                if(activeUserPref==undefined){
                  list.isActive = true
                }
              }
              this.preferenceNameList.push(list.USER_PREFERENCE_NAME)
            })

            if(typeof(defaultUserPrefIndex)=='number'){
            this.projectDashUserPref.splice(defaultUserPrefIndex,1)
            // this.projectDashUserPref=this.projectDashUserPref.reverse();
            this.projectDashUserPref.sort((a,b) => a?.USER_PREFERENCE_NAME.localeCompare(b?.USER_PREFERENCE_NAME));
            this.projectDashUserPref.splice(0,0,this.defaultPreference);
            }else{
              this.projectDashUserPref.sort((a,b) => a?.USER_PREFERENCE_NAME.localeCompare(b?.USER_PREFERENCE_NAME));
            // this.projectDashUserPref=this.projectDashUserPref.reverse();
            }
            
            this.dataSource.data= this.projectDashUserPref
            this.table.renderRows();
            this.totalListDataCount=this.projectDashUserPref.length
          }
          else{
            if(response?.MPM_User_Preference?.FILTER_NAME == 'All Projects' || response?.MPM_User_Preference?.FILTER_NAME =='My Projects'){
                  this.userPreferenceList= response.MPM_User_Preference
                  // this.dataSource.data.push(this.userPreferenceList)
                  this.userPreferenceList['isEditable']=false;
                  // this.userPreferenceList['isActive']=true;
                  if(this.userPreferenceList['MPM_User_Preference-id'].Id==activeUserPref){
                    this.userPreferenceList['isActive']=true;
                  }
                  this.defaultPreference=this.userPreferenceList
                  this.totalListDataCount=1
                  this.preferenceNameList.push(this.userPreferenceList['USER_PREFERENCE_NAME'])
                  this.dataSource.data[0] =this.userPreferenceList
                  this.table.renderRows();
            }
          }
          this.loaderService.hide();
      });
      }

  // clearSort() {
  //   this.sort.sort({id: '', start: 'asc', disableClear: false});
  // }

    //Adding a new empty row in the table
  addNewUserPreference(){
      // this.clearSort();
      this.userPreferNameEntered=''
      const newPreference = [{
        PersonId: this.sharingService.getCurrentPerson().Id,
        PreferenceType: 'VIEW',
        IsListView: '',
        IsDefaultView:true,
        ViewId: '',
        MenuId: this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id,
        FieldData: '',
        FilterName: '',
        IsActivePreference:false,
        UserPreferenceName:this.newPrefernceName,
        isEditable:false
    }];
    // this.userPreferenceList.push(newPreference)
      // this.disableSort=true;
      this.dataSource.data.splice(0,0,newPreference); 
      this.dataSource.data=[...this.dataSource.data];
      // this.dataSource.filteredData.splice(0,1)
      // this.dataSource.push()
      this.table.renderRows();
      this.disableAction=true
  }


    //Selecting a default User Prefernce
  changeDefaultUserPreference(selectedRow) {
      this.loaderService.show();
      let currentViewData: any = sessionStorage.getItem('CURRENT_VIEW');
      let requestObjA;
      let requestObjB;
     
      requestObjA = {
              R_PO_PERSON: {
                  'Person-id': {
                      Id: this.sharingService.getCurrentPerson().Id
                  }
              },
              PREFERENCE_TYPE: selectedRow.PREFERENCE_TYPE,
              IS_LIST_VIEW: selectedRow.IS_LIST_VIEW,
              R_PO_MPM_VIEW_CONFIG: {
                  'MPM_View_Config-id': {
                      Id: selectedRow.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id
                  }
              },
              R_PO_MPM_MENU_ID: {
                  'MPM_Menu-id': {
                      Id: selectedRow.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id
                  }
              },
              FIELD_DATA: selectedRow.FIELD_DATA,
              FILTER_NAME: selectedRow.FILTER_NAME,
             
          };
      const existingUserPreferenceId = selectedRow['MPM_User_Preference-id'].Id;
      this.appService.updateUserPreference(requestObjA, existingUserPreferenceId).subscribe(response => {
          if (response) {
          this.loaderService.hide();
          }
      });

      requestObjB = {
          PersonId: this.sharingService.getCurrentPerson().Id,
          PreferenceType:  selectedRow.PREFERENCE_TYPE,
          IsListView:selectedRow.IS_LIST_VIEW,
          IsDefaultView:true,
          ViewId: selectedRow.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id,
          MenuId:selectedRow.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id,
          FieldData: selectedRow.FIELD_DATA,
          FilterName: currentViewData.listfilter,
          UserPreferenceName: selectedRow.USER_PREFERENCE_NAME,
          UserPreferenceId:selectedRow['MPM_User_Preference-id'].Id,
          IsActiveUserPreference:true
      };
          this.appService.invokeRequest('http://schemas.acheron.com/mpm/core/workflow/bpm/1.0','UpdateIsActiveUserPreference',requestObjB).subscribe(response=>{
              if(response){
                this.notificationService.success('Default User preference has been changed successfully');
                this.getCurrentUserPreference();
              }
          })
  }

  UpdatePreference(rowDetails,operationType){
      if( this.userPreferNameEntered.length==0){
        this.notificationService.error('Pease enter a preference Name')
        return
      }
      if(this.preferenceNameList.includes(this.userPreferNameEntered)){
        this.notificationService.error('Duplicate Names are not allowed')
        return
      }
      this.loaderService.show();
      let currentViewData: any = sessionStorage.getItem('CURRENT_VIEW');
      currentViewData = JSON.parse(currentViewData);
      let fieldData;
      if (currentViewData.isListView) {
        let userPreferenceData: any = sessionStorage.getItem('CURRENT_USER_PREFERENCE-' + currentViewData.viewName);
        userPreferenceData = userPreferenceData ? JSON.parse(userPreferenceData) : null;
        const userPreferenceViewField = userPreferenceData && userPreferenceData.FIELD_DATA ? userPreferenceData.FIELD_DATA : null;
        userPreferenceViewField.forEach(field => {
          
            const fieldId = Object.keys(field)[0];
            if (fieldId === currentViewData.sortName) {
                field[fieldId].IS_SORTABLE = true;
                field[fieldId].SORT_ORDER = currentViewData.sortOrder
            }
            else {
                field[fieldId].IS_SORTABLE = false;
                field[fieldId].SORT_ORDER = null
            }
        });
        if (currentViewData.viewName.includes("MEMBER")) {
            fieldData = {
                data: userPreferenceViewField,
                pageSize: currentViewData.pageSize,
                pageNumber: currentViewData.pageNumber,
                groupBy: currentViewData.groupBy,
                taskStatusFilter: currentViewData.taskStatusFilter,
                FREEZE_COLUMN_COUNT:userPreferenceData.FREEZE_COLUMN_COUNT
            }
        }
        else {
            fieldData = {
                data: userPreferenceViewField,
                pageSize: currentViewData.pageSize,
                pageNumber: currentViewData.pageNumber,
                FREEZE_COLUMN_COUNT: userPreferenceData.FREEZE_COLUMN_COUNT
            }
        }
      } else {
          let userPreferenceViewField = [];
          if (currentViewData.sortName) {
              userPreferenceViewField = [{
                  [currentViewData.sortName]: {
                      IS_SORTABLE: true,
                      SORT_ORDER: currentViewData.sortOrder
                  }
              }];
          }
          if (currentViewData.viewName.includes("MEMBER")) {
              fieldData = {
                  data: userPreferenceViewField,
                  pageSize: currentViewData.pageSize,
                  pageNumber: currentViewData.pageNumber,
                  groupBy: currentViewData.groupBy,
                  taskStatusFilter: currentViewData.taskStatusFilter,
                  FREEZE_COLUMN_COUNT: MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
              }
          }
          else {
              fieldData = {
                  data: userPreferenceViewField,
                  pageSize: currentViewData.pageSize,
                  pageNumber: currentViewData.pageNumber,
                  FREEZE_COLUMN_COUNT: MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
              }
          }
      }
      if(operationType=='rename'){
        this.renameUserPreference(rowDetails,currentViewData)
      }
      if(operationType=='create'){
        if(this.totalListDataCount==0){
          this.createNewWithDefaultPreference(rowDetails,currentViewData,fieldData)
        }else{
        this.createNewPreference(rowDetails,currentViewData,fieldData)
        }
      }
    }

    createNewPreference(rowDetails,currentViewData,fieldData){
      let createdUserPreferenceId = null
      let requestObjCreate = {
        PersonId: this.sharingService.getCurrentPerson().Id,
        PreferenceType: 'VIEW',
        IsListView: currentViewData.isListView,
        IsDefaultView:true,
        ViewId: currentViewData.viewId,
        MenuId: this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id,
        FilterName: currentViewData.listfilter,
        UserPreferenceName:  this.userPreferNameEntered || rowDetails.USER_PREFERENCE_NAME,
        FieldData:JSON.stringify(fieldData)
    };
      this.appService.invokeRequest('http://schemas.acheron.com/mpm/core/workflow/bpm/1.0','CreateMultipleUserPreference',requestObjCreate).subscribe(response=>{
        if(response){
          // this.clearSort();
          // this.disableSort=false;
          this.getCurrentUserPreference();
          // this.loaderService.hide();
        }
      }) 
   }

  createNewWithDefaultPreference(rowDetails,currentViewData,fieldData){
      let createdUserPreferenceId = null
      let requestObjCreate = {
        PersonId: this.sharingService.getCurrentPerson().Id,
        PreferenceType: 'VIEW',
        IsListView: currentViewData.isListView,
        IsDefaultView:true,
        ViewId: currentViewData.viewId,
        MenuId: this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id,
        FilterName: currentViewData.listfilter,
        UserPreferenceName:  this.userPreferNameEntered || rowDetails.USER_PREFERENCE_NAME,
        FieldData:JSON.stringify(fieldData)
      };
      let  requestObjUpdate = {
          R_PO_PERSON: {
              'Person-id': {
                  Id: this.sharingService.getCurrentPerson().Id
              }
          },
          PREFERENCE_TYPE: 'VIEW',
          IS_LIST_VIEW: currentViewData.isListView,
          R_PO_MPM_VIEW_CONFIG: {
              'MPM_View_Config-id': {
                  Id: currentViewData.viewId
              }
          },
          R_PO_MPM_MENU_ID: {
              'MPM_Menu-id': {
                  Id:  this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id
              }
          },
          FIELD_DATA: JSON.stringify(fieldData),
          FILTER_NAME: currentViewData.listfilter, 
        };
      this.appService.invokeRequest('http://schemas.acheron.com/mpm/core/workflow/bpm/1.0','CreateMultipleUserPreference',requestObjCreate).subscribe(response=>{
        if(response && response?.CreateMultipleUserResponse?.MPM_User_Preference['MPM_User_Preference-id']?.Id){
          createdUserPreferenceId=response.CreateMultipleUserResponse.MPM_User_Preference['MPM_User_Preference-id'].Id
          requestObjCreate['UserPreferenceId']=createdUserPreferenceId
          requestObjCreate['IsActiveUserPreference']=true
          forkJoin([ this.appService.invokeRequest('http://schemas.acheron.com/mpm/core/workflow/bpm/1.0','UpdateIsActiveUserPreference',requestObjCreate).subscribe(defResponse=>{
          if(defResponse){
            this.getCurrentUserPreference()
          }
         }),
          this.appService.updateUserPreference(requestObjUpdate, createdUserPreferenceId).subscribe()])
          // this.loaderService.hide()
        }
      })
  }

  renameUserPreference(rowDetails,currentViewData){
      let requestObjUpdate = {
        PersonId: this.sharingService.getCurrentPerson().Id,
        PreferenceType: 'VIEW',
        IsListView: currentViewData.isListView,
        IsDefaultView:true,
        ViewId: currentViewData.viewId,
        MenuId: this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id,
        FilterName: currentViewData.listfilter,
        UserPreferenceName:  this.userPreferNameEntered || rowDetails.USER_PREFERENCE_NAME,
        UserPreferenceId:rowDetails['MPM_User_Preference-id'].Id,
        IsActiveUserPreference:rowDetails.IS_ACTIVE_USER_PREFERENCE,
        FieldData:rowDetails.FIELD_DATA
      };
      this.appService.invokeRequest('http://schemas.acheron.com/mpm/core/workflow/bpm/1.0','UpdateIsActiveUserPreference',requestObjUpdate).subscribe(response=>{
          if(response){
          // this.loaderService.hide();
          this.getCurrentUserPreference()
          }
      })
      if(rowDetails.isActive){
          this.applyPreference(rowDetails,false)
      }
  }
  
  applyPreference(preferenceRow,closeDialog?){
      let parsedData= JSON.parse(preferenceRow.FIELD_DATA)
      const userPreference = {
        USER_ID: this.sharingService.getCurrentPerson().Id,
        PREFERENCE_TYPE: 'VIEW',
        VIEW_ID:this.sharingService.getCurrentPerson().Id,
        FIELD_DATA: parsedData.data,
        FREEZE_COLUMN_COUNT: parsedData.FREEZE_COLUMN_COUNT,
      }
      sessionStorage.setItem('ACTIVE_USER_PREFERENCE_ID',preferenceRow['MPM_User_Preference-id'].Id)
      sessionStorage.setItem('ACTIVE_USER_PREFERENCE_NAME', this.userPreferNameEntered || preferenceRow.USER_PREFERENCE_NAME)
      let finalData=JSON.stringify(userPreference)
      sessionStorage.setItem('USER_PREFERENCE-PM_PROJECT_VIEW',finalData);
      if(closeDialog){
        this.closeDialog()
        
      }
      this.npdProjectService.setData();
   }

    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const currentPageItemCount = this.dataSource.connect().value.length
      return numSelected == currentPageItemCount;
    }
  
    masterToggle() {
      this.isAllSelected() ? this.DeselectRows(): this.selectRows();
    }
    
    DeselectRows(){
      this.selection.clear();
    }

    selectRows(){
      // if(this.disableAction){
      //   this.dataSource.data.splice(0,1)
      // }
      let endIndex :number;
      if(this.dataSource.data.length>(this.dataSource.paginator.pageIndex +1)* this.dataSource.paginator.pageSize){
        endIndex = (this.dataSource.paginator.pageIndex + 1) * this.dataSource.paginator.pageSize;
      }else{
        endIndex = this.dataSource.data.length;
      }
      for (let index = (this.dataSource.paginator.pageIndex * this.dataSource.paginator.pageSize); index < endIndex; index++) {
        this.selection.select(this.dataSource.data[index]);
      }
    }

    pageEvents(event){
        this.selection.clear();
    }

    ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

  //Applying an User Preference from the list.


    //Deleting a single User Preference
    deleteUserPreference(selectedPreference){
      if(selectedPreference.IS_ACTIVE_USER_PREFERENCE=='true'){
        this.notificationService.error('Default Preference cannot be deleted.')
      }
      else{
        this.confirmDelete((result)=>{
          if(result){   
              this.loaderService.show();
              let userPreferenceId = selectedPreference['MPM_User_Preference-id'].Id
              this.appService.deleteUserPreference(userPreferenceId).subscribe(response => {
              if (response) {
                if(selectedPreference?.isActive==true){
                  this.applyPreference(this.defaultPreference)
                }
                  this.dataSource.data=[]
                  this.notificationService.success('User preference has been deleted successfully');
                  this.getCurrentUserPreference();
                  // this.loaderService.hide();
              }
              });
            }
          })
      }
    }

    //Deleting Multiple User Preference
    deleteMultipleUserPreference(){
      let deleteCount=0
      let conditionCount = 0
      let activePrefernceSelected = false;
      if(this.selection.selected.length==0){
        this.notificationService.error('No Preference selected !')
      }else{
          this.confirmDelete((result)=>{
            if(result){     
              this.loaderService.show();
            if(this.disableAction){
              this.selection.selected.splice(0,1)
            }
              let selectedPreferenceList = this.selection.selected
              selectedPreferenceList.forEach(preference=>{
                  if(preference.IS_ACTIVE_USER_PREFERENCE=='true'){
                    conditionCount ++
                  }
              })
              if(conditionCount>0){
                this.notificationService.error('Multiple Deletion failed as default Preference is selected !')
                this.selection.clear();
                this.loaderService.hide();
              }else{
                selectedPreferenceList.forEach(preference=>{
                  if(preference.isActive==true){
                      activePrefernceSelected = true;
                  }
                  let userPreferenceId = preference['MPM_User_Preference-id'].Id
                  this.appService.deleteUserPreference(userPreferenceId).subscribe(response => {
                    deleteCount++
                    if(deleteCount==selectedPreferenceList.length){
                      this.dataSource.data=[]
                      this.selection.clear();
                       this.notificationService.success('Selected User Preferences have been deleted !')
                       this.getCurrentUserPreference();
                      this.loaderService.hide();
                    }
                  });
                })
                if(activePrefernceSelected==true){
                  this.applyPreference(this.defaultPreference)
                }
              }
            }
          })
        }
    }
    

    confirmDelete(callback) {
      const dialogRef = this.dialog.open(ConfirmationModalComponent, {
        width: '40%',
        disableClose: true,
        data: {
          message: 'Are you sure you want to delete the Preference?',
          submitButton: 'Yes',
          cancelButton: 'No'
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (callback) { callback(result ? result.isTrue : null) };
      });
    }

    cardDoubleClick(preferenceRow): void {
        clearTimeout(this.matMenuTimer);
        this.matMenuTimer = undefined;
        this.makingFieldEditable(preferenceRow);
    }
    
    //Making input field editable
    makingFieldEditable(preferenceRow){
      this.userPreferNameEntered=''
      if( this.totalListDataCount>1){
        if(preferenceRow.isEditable==true){
          this.userPreferenceList?.forEach(list=>{
            list.isEditable=false
          })
          preferenceRow.isEditable=false
        }else{
          this.userPreferenceList?.forEach(list=>{
            list.isEditable=false
          })
          preferenceRow.isEditable=true
        }
      }else{
        if(preferenceRow.isEditable==true){
          preferenceRow.isEditable=false
        }else{
          preferenceRow.isEditable=true
        }
      }
    }

    closeDialog() {
      this.dialogRef.close(); 
    }

    // sortData(target){
    //     this.selection.clear()
    //     this.dataSource.sortingDataAccessor = (data: any, sortHeaderId: string): string => {
    //     if (typeof data[sortHeaderId] === 'string') {
    //       return data[sortHeaderId].toLocaleLowerCase();
    //     }
    //       return data[sortHeaderId];
    //   };
    //   if(target){
    //   this.dataSource.sortData(this.dataSource.filteredData,this.dataSource.sort);
    //   }
    //   }
    // (matSortChange)="sortData($event)"

}