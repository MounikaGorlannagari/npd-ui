import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import {
    ListFilter, CustomSelectComponent, DeliverableTaskMetadataService, MatSortDirections, Person, OTMMFolderFilterTypes,
    NotificationService, SharingService, OTMMService, SearchChangeEventService, SearchDataService,
    FacetChangeEventService, AssetService, FacetConditionList, EmailDataService, ViewConfig, FieldConfigService, SearchRequest, MPMField, IndexerService,
    SearchResponse, MPMFieldConstants, MPMSearchOperators, MPMFieldKeys, ViewConfigService, OTMMMPMDataTypes, SearchConfigConstants,
    AssetConstants, DeliverableConstants, TaskService, MPM_ROLES, ExportDataComponent
} from 'mpm-library';
import { getTaskFilters, getAccessibleTaskFilters } from './taskListfilters/TaskListFilter';
import { CustomSelect, LoaderService } from 'mpm-library';
import { GROUP_BY_FILTERS } from './groupByFilter/GroupByFilter';
import { DeliverableTaskViewDataService } from './service/deliverable-task-view-data.service';
import { FilterHelperService } from './service/filter-helper.service';
import { MPM_LEVEL, MPM_LEVELS } from 'mpm-library';
import { GroupByFilterTypes } from './groupByFilter/GroupByFilterTypes';
import { TaskFilterType } from './taskListfilters/TaskFilterType';
import { MatSort } from '@angular/material/sort';
import { getTaskListDependentFilters } from './taskListfilters/TaskListDependentFilter';
import { TitleCasePipe } from '@angular/common';
import { AssetDetailsComponent } from '../shared/components/asset-details/asset-details.component';
import { AssetsVersionModalComponent } from '../shared/components/assets-version-modal/assets-version-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { ShareAssetsModalComponent } from '../shared/components/share-assets-modal/share-assets-modal.component';
import { MediaMatcher } from '@angular/cdk/layout';
import { RoutingConstants } from '../routingConstants';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, Subject, Subscription } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { DeliverableTasksViewComponentParams, TaskSearchConditions } from './object/DeliverableTasksViewComponentParams';
import { RouteService } from '../route.service';
import { MPMSearchOperatorNames } from 'mpm-library';
import { ApplicationConfigConstants } from 'mpm-library';
import { UtilService } from 'mpm-library';
import { BulkCommentsComponent } from 'mpm-library';
import { EntityAppDefService, AppService } from 'mpm-library';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StatusTypes } from 'mpm-library';
import { StatusService } from 'mpm-library';
import * as acronui from 'mpm-library';
import { getTaskListDependentReviewFilters } from './taskListfilters/TaskListDependentReviewFilter';
import { DeliverableCreationModalComponent } from '../project-view/task/task-details/deliverable-creation-modal/deliverable-creation-modal.component';
import { GROUP_BY_ALL_FILTERS } from './groupByFilter/GroupByFilter';
import { ConfirmationModalComponent } from 'mpm-library';
import { GloabalConfig as config } from 'mpm-library';
import { IndexerDisplayFields } from 'mpm-library';
import { MPMDeliverableViewConstants } from './deliverable-view-constants';
import { NpdDeliverableTaskListComponent } from '../../npd-ui/npd-task-view/npd-deliverable-task-list/npd-deliverable-task-list.component';
import { NpdProjectService } from '../../npd-ui/npd-project-view/services/npd-project.service';
import { NPDFacetService } from '../../mpm-lib/facet/facet.service';

@Component({
    selector: 'app-tasks-view',
    templateUrl: './deliverables-task-view.component.html',
    styleUrls: ['./deliverables-task-view.component.scss']
})
export class DeliverableTasksViewComponent implements OnInit, OnDestroy {
    private unsubscribe$: Subject<any> = new Subject<any>();
    readonly componentParams: Observable<DeliverableTasksViewComponentParams> = this.activatedRoute.queryParamMap.pipe(
        map(params => this.deliverableTasksViewComponentRoute(params)),
    );
    private subscriptions: Array<Subscription> = [];

    mobileQuery: MediaQueryList;
    private mobileQueryListener: () => void;
    // myTasksFilter : EventEmitter<any> = new EventEmitter<any>
    myTasksFilter;

    selectedTask = [];

    constructor(
        private dialog: MatDialog,
        private loaderService: LoaderService,
        private notificationService: NotificationService,
        private filterHelperService: FilterHelperService,
        private titlecasePipe: TitleCasePipe,
        private sharingService: SharingService,
        private media: MediaMatcher,
        private changeDetectorRef: ChangeDetectorRef,
        private deliverableTaskViewDataService: DeliverableTaskViewDataService,
        private deliverableTaskMetadataService: DeliverableTaskMetadataService,
        private searchDataService: SearchDataService,
        private otmmService: OTMMService,
        private searchChangeEventService: SearchChangeEventService,
        private facetChangeEventService: FacetChangeEventService,
        private assetService: AssetService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private routeService: RouteService,
        private emailDataService: EmailDataService,
        private fieldConfigService: FieldConfigService,
        private indexerService: IndexerService,
        private viewConfigService: ViewConfigService,
        private notification: NotificationService,
        private taskService: TaskService,
        private utilService: UtilService,

        private entityAppdefService: EntityAppDefService,

        private appService: AppService,
        private formBuilder: FormBuilder,
        private statusService: StatusService,
        private npdProjectService: NpdProjectService,
        private npdFacetService: NPDFacetService
        /*  private deliverableService: DeliverableService, */

    ) {
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
    }
    @ViewChild('taskStatusFilter') taskStatusFilter: CustomSelectComponent;
    @ViewChild('groupByFilter') groupByFilter: CustomSelectComponent;
    @ViewChild('deliverableListView') deliverableListView: NpdDeliverableTaskListComponent
    unselectDeliverable;
    defaultViewFilterName;
    defaultGroupViewFilterName;
    defaultListDependentFilterName;
    page = 1;
    skip = 0;
    //top = 10;
    //pageSize = 10;
    top: number;
    pageSize: number;
    pageNumber: number;
    isdefaultPagination = true;
    isRefresh = true;
    hasPagination = null;
    totalListDataCount = 0;
    totalDataCount;
    componentRendering = false;
    currentfilter;
    isnotified = false;

    skipPageSetting = false;

    isCampaignDashboard;
    isCampaign;

    sortableFields: Array<any> = [];
    selectedSort: MatSort = new MatSort();

    level: MPM_LEVEL;
    listData = [];
    selectedListFilter: ListFilter;
    selectedGroupByFilter: CustomSelect;
    selectedListDependentFilter: CustomSelect;

    allListDependentFilters: any;
    listDependentFilters: Array<CustomSelect>;
    groupByFilters = [];

    calculatedDeliverableTaskFilters: Array<ListFilter>;
    deliverableTaskFilters: Array<ListFilter>;
    currentUserIdentityDetails: Person;
    displayColumns: Array<string>;
    displayableMetadataFields: Array<MPMField> = [];
    levelFields: Array<MPMField> = [];

    searchConfigId = -1;
    searchConditions = [];
    isToggleHappened = false;
    pageUpdated = false;

    sortChangeName;
    sortChangeDirection;
    sortHandled = false;

    facetExpansionState = false;
    facetRestrictionList: FacetConditionList;
    emailLinkId = '';
    emailSearchCondition = [];
    storeSearchconditionObject: TaskSearchConditions = null;
    appConfig: any;

    status = ['Completed', 'Cancelled', 'Archived'];
    enableBulkEdit = false;
    bulkEdit = false;
    selectedDeliverableCount = false;
    count;
    delCount;
    bulkActions = true;
    deliverableData;
    projectData;
    crRole;
    commentFormGroup: FormGroup;
    bulkStatus;
    statusId;
    viewConfigName = null;
    bulkStatusAction;
    defaultUserPreferenceView;
    taskMenuConfig;
    pageSelected;
    isDefaultFiltersPresent = true;

    mpmLevel = MPM_LEVELS;
    projectStatus = {
        REQUIRE_REASON: 'true',
        REQUIRE_COMMENTS: 'true',
        MPM_Status_Reason: ''
    };

    reviewerRole;
    isReviewer;
    isApprover;
    approverRole;
    isMember;
    memberRole;
    otmmMPMDataTypes;
    projectId;
    totalAssetsCount;
    assets;
    folderId = [];
    project = [];
    assetDetailData = [];
    assetDetail;

    assetDetailDatas = [];
    otmmFolderId;
    queryParams

    exportTableData;
    exportDataSearchRequest;
    isReferenceAsset;
    enableReferenceAsset;

    enableCampaign;
    roles = [];
    selectedSortOrder;
    selectedSortBy;
    selectedPageNumber;
    selectedPageSize;

    searchName;
    savedSearchName;
    advSearchData;
    facetData;
    isSearch = false;

    isReset = false;
    isRoute = false;

    isPageRefresh = true;
    isDataRefresh = true;
    previousRequest;
    validateFacet = false;

    columnChooserFields;
    userPreference;
    menuId;

    bulkResponseData;
    userPreferenceFreezeCount: any
    // isColumnsFreezable:boolean=true
    isColumnsFreezable: boolean = true


    enableIndexerFieldRestriction;
    maximumAllowedFields;
    selectedTaskData;
    selectedTaskCount;

    bulkEditSelectedTasks(operation) {
        //  this.myTasksFilter = this.selectedListFilter?.value=='MY_TASKS' && this.selectedGroupByFilter?.value=='GROUP_BY_TASK' && this.selectedListDependentFilter.value=='TO_DO_FILTER'
        this.deliverableListView.openBulkEditDialog(operation);
    }

    routeToProjectDashboardView(): void {
        this.deliverableListView.routeToProjectDashboardView();
    }


    exportData() {
        let mapperNames: String = "";
        let index = 0;
        this.displayableMetadataFields.forEach(field => {
            if (field.MAPPER_NAME.length > 0) {
                mapperNames = mapperNames.concat(field.MAPPER_NAME);
                if (index != (this.columnChooserFields.length - 1)) {
                    mapperNames = mapperNames.concat(",");
                }
            }
            index++;
        });
        this.previousRequest = null;

        // this.getlistDataIndex(true).subscribe(projectsResponse => {

        const dialogRef = this.dialog.open(ExportDataComponent, {
            width: '40%',
            disableClose: true,
            data:
            {
                // exportData: this.exportTableData,
                searchRequest: this.exportDataSearchRequest,
                columns: mapperNames,
                totalCount: this.totalDataCount
            }
        });
        this.loaderService.hide();
        // }, projectsError => {
        //     this.loaderService.hide();
        //     this.notificationService.error(projectsError);
        // });
    }

    bulkCheck(event) {
        this.bulkEdit = event;
    }

    deliverableBulkCheckCount(data) {
        this.selectedDeliverableCount = data;
    }
    selectedDeliverableData(projectData) {
        this.projectData = projectData;
    }

    selectedDeliverableCounts(data) {
        this.count = data;

        this.delCount = data;
        /* if (this.count === 1 && this.delCount === 1) {
            this.notificationService.info("One or more records do not meet bulk edit criteria");
        } */
        this.bulkActions = true;
    }

    selectedDeliverableDatas(data) {
        this.deliverableData = data;
    }

    masterToogle(event) {
        this.bulkActions = event.checked;
        this.unselectDeliverable = this.bulkActions;
        this.reload(false, 'bulk');
    }

    routeComponentView() {
        const queryParams: DeliverableTasksViewComponentParams = {};
        if (this.selectedGroupByFilter.value) {
            queryParams['groupByFilter'] = this.selectedGroupByFilter.value;
        }
        if (this.selectedListFilter.value) {
            queryParams['deliverableTaskFilter'] = this.selectedListFilter.value;
        }
        if (this.selectedListDependentFilter.value) {
            queryParams['listDependentFilter'] = this.selectedListDependentFilter.value;
        }
        if (this.emailLinkId) {
            queryParams['emailLinkId'] = this.emailLinkId;
        }
        if (this.selectedSort) {
            queryParams['sortBy'] = this.selectedSortBy ? this.selectedSortBy : null;
            queryParams['sortOrder'] = this.selectedSortOrder ? this.selectedSortOrder : null;
        }
        if (this.searchName) {
            queryParams['searchName'] = this.searchName;
        }
        if (this.savedSearchName) {
            queryParams['savedSearchName'] = this.savedSearchName;
        }
        if (this.advSearchData) {
            queryParams['advSearchData'] = this.advSearchData;
        }
        if (this.facetData) {
            queryParams['facetData'] = this.facetData;
        }
        if (this.pageSize) {
            queryParams['pageSize'] = this.pageSize;
        }
        if (this.page) {
            if (this.isReset) {
                this.resetPagination();
                //this.selectedPageNumber = this.page;
                this.isReset = false;
            }
            this.selectedPageNumber = this.page;
            queryParams['pageNumber'] = this.page;
        }
        this.routeService.goToDeliverableTaskView(queryParams, this.searchName, this.savedSearchName, this.advSearchData, this.facetData);
    }

    clearEmailFiters() {
        this.emailLinkId = '';
        this.emailSearchCondition = [];
        this.isRoute = true;
        this.routeComponentView();
    }

    openCR(event) {
        this.router.navigate([RoutingConstants.projectViewBaseRoute], {
            queryParams: {
                isCreativeReview: true,
                crData: JSON.stringify(event)
            }
        });
    }

    facetToggled(event) {
        this.facetExpansionState = event;
        this.previousRequest = null;
        // this.reload(false);
    }
    //to get reference assets
    getAssets() {
        return new Observable(observer => {
            this.otmmService.checkOtmmSession()
                .subscribe(sessionResponse => {
                    let userSessionId;
                    if (sessionResponse && sessionResponse.session_resource && sessionResponse.session_resource.session && sessionResponse.session_resource.session.id) {
                        userSessionId = sessionResponse.session_resource.session.id;
                    }/* this.skip */
                    //  this.folderId.forEach(folderId => {
                    this.assetService.getAssets(AssetConstants.REFERENCE_ASSET_SEARCH_CONDITION, this.otmmFolderId, 0, this.top, userSessionId)?.subscribe(res => {
                        if (res && res.search_result_resource && res.search_result_resource.asset_list) {
                            //  this.loadingHandler(true);
                            this.assets = [];
                            this.totalAssetsCount = res.search_result_resource.search_result.total_hit_count;
                            this.assets = res.search_result_resource.asset_list;
                            /*this.assetData = [];
                           this.assets.forEach(asset => {
                               asset.metadata.metadata_element_list.forEach(metadataElement => {
                                   if (metadataElement.id === 'MPM.ASSET.GROUP') {
                                       // [0].metadata_element_list
                                       metadataElement.metadata_element_list.forEach(metadataElementList => {
                                           if (metadataElementList.id === 'MPM.ASSET.IS_REFERENCE_ASSET') {
                                               if (metadataElementList.value && metadataElementList.value.value && metadataElementList.value.value.value === 'true') {
                                                   this.assetData.push(asset);
                                               }
                                           }
                                       });
                                   }
                               });
                           }); */
                        }
                        observer.next(this.assets);

                        observer.complete();
                    });
                });
        }
        );
    }

    rowExpanded(row) {
        this.otmmFolderId = row.OTMM_FOLDER_ID !== undefined ? row.OTMM_FOLDER_ID : row.CAMPAIGN_OTMM_FOLDER_ID;
        /* this.assetDetailDatas = []; */
        row.assetDetailDatas = [];
        this.enableReferenceAsset = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_REFERENCE_ASSET]);
        if (this.enableReferenceAsset) {
            this.loaderService.show();
            this.getAssets().subscribe(assetData => {
                /*  this.assetDetailDatas.push(assetData); */
                row.assetDetailDatas.push(assetData);
                this.loaderService.hide();
                this.isRefresh = false;
            });
        }
        let viewConfig;
        if (row.deliverableDetails && Array.isArray(row.deliverableDetails) && row.deliverableDetails.length > 0) {
            return;
        }
        //MPM_LEVELS.CAMPAIGN ? MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_CAMPAIGN_ID :
        const displayField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME,
            this.level === MPM_LEVELS.CAMPAIGN ? MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_CAMPAIGN_ID : this.level === MPM_LEVELS.PROJECT ? MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_PROJECT_ID : MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TASK_ID, MPM_LEVELS.DELIVERABLE);
        if (!displayField || !row['ITEM_ID']) {
            return;
        }
        const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level);
        const otmmMPMDataTypes = ((this.isApprover || this.isMember) && this.selectedListFilter.value != 'MY_NA_REVIEW_TASKS') ? OTMMMPMDataTypes.DELIVERABLE : this.isReviewer ? OTMMMPMDataTypes.DELIVERABLE_REVIEW : ' ';

        const condition = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, otmmMPMDataTypes);
        condition ? conditionObject.DELIVERABLE_CONDTION.push(condition) : console.log('');
        conditionObject.DELIVERABLE_CONDTION.push({
            type: displayField ? displayField.DATA_TYPE : 'string',
            field_id: displayField.INDEXER_FIELD_ID,
            relational_operator_id: MPMSearchOperators.IS,
            relational_operator_name: MPMSearchOperatorNames.IS,
            relational_operator: 'and',
            value: row['ITEM_ID']
        });
        if (this.viewConfigName) {
            viewConfig = this.viewConfigName;//this.sharingService.getViewConfigById(this.viewConfigId).VIEW;
        } else {
            viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
        }
        let deliverableDisplayFields = [];
        this.viewConfigService.getAllDisplayFeildForViewConfig(MPMDeliverableViewConstants.VIEW_CONFIG).subscribe((viewDetails: ViewConfig) => {
            deliverableDisplayFields = viewDetails.R_PM_CARD_VIEW_MPM_FIELDS;
        })
        let searchFields = [];
        if (this.enableIndexerFieldRestriction) {
            deliverableDisplayFields.forEach((field) => {
                let indexerDisplayFields: IndexerDisplayFields;
                searchFields.push(indexerDisplayFields = { field_id: field.INDEXER_FIELD_ID })
            })
        }
        this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails: ViewConfig) => {
            const searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
            const parameters: SearchRequest = {
                search_config_id: searchConfig ? searchConfig : null,
                keyword: '',
                // grouping: {
                //     group_by: this.level,
                //     group_from_type: MPM_LEVELS.DELIVERABLE,
                //     search_condition_list: {
                //         search_condition: conditionObject.DELIVERABLE_CONDTION
                //     }
                // },
                search_condition_list: {
                    search_condition: conditionObject.DELIVERABLE_CONDTION
                },
                displayable_field_list: {
                    displayable_fields: searchFields,
                },
                facet_condition_list: {
                    facet_condition: []
                },
                sorting_list: {
                    sort: []
                },
                cursor: {
                    page_index: 0,
                    page_size: 100
                }
            };
            this.loaderService.show();


            this.subscriptions.push(
                this.indexerService.search(parameters).subscribe((data: SearchResponse) => {
                    row.deliverableDetails = [];
                    if (data.data.length > 0) {
                        data.data.forEach(item => {
                            const deliverableDetail = {
                                deliverableData: item,
                                projectData: {
                                    OTMM_FOLDER_ID: ''
                                },
                                taskDat: {
                                    IS_APPROVAL_TASK: false
                                },
                                //   assetData: assetData
                            };
                            if (item.BULK_ACTION !== 'true') {
                                row.deliverableDetails.push(deliverableDetail);
                            }
                        });
                        if (row.deliverableDetails.length === 0) {
                            this.loaderService.hide();
                        }
                    } else {
                        this.loaderService.hide();
                    }
                })
            );
        });
    }

    calculateListFilterCounts() {
        if (Array.isArray(this.calculatedDeliverableTaskFilters) && this.calculatedDeliverableTaskFilters.length > 0) {
            return;
        }
        const existingDeliverableTaskFilters = this.deliverableTaskFilters;
        this.deliverableTaskFilters = [this.selectedListFilter];
        this.loaderService.show();
        let folderFilterType = OTMMFolderFilterTypes.DIRECT;
        if (this.level === MPM_LEVELS.TASK) {
            folderFilterType = OTMMFolderFilterTypes.ALL;
        }
        const customCondition = JSON.parse(JSON.stringify(this.selectedGroupByFilter));
        if (Array.isArray(this.searchConditions)) {
            customCondition.searchCondition = customCondition.searchCondition.concat(this.searchConditions);
        }
        if (Array.isArray(this.emailSearchCondition)) {
            customCondition.searchCondition = customCondition.searchCondition.concat(this.emailSearchCondition);
        }
        const allListDependentFilters = ((this.sharingService.getIsApprover() || this.sharingService.getIsMember()) && this.selectedListFilter.value != 'MY_NA_REVIEW_TASKS') ? getTaskListDependentFilters() : this.sharingService.getIsReviewer() ? getTaskListDependentReviewFilters() : '';
        this.subscriptions.push(
            this.deliverableTaskViewDataService.prefetchListFilterCount(this.selectedListFilter,
                existingDeliverableTaskFilters, allListDependentFilters, this.emailSearchCondition, this.level,
                ((this.facetRestrictionList && this.facetRestrictionList.facet_condition) ?
                    this.facetRestrictionList.facet_condition : [])).subscribe(data => {
                        this.loaderService.hide();
                        this.calculatedDeliverableTaskFilters = data;
                        this.deliverableTaskFilters = data;
                    }, error => {
                        this.deliverableTaskFilters = existingDeliverableTaskFilters;
                        this.loaderService.hide();
                    })
        );
    }

    getlistDataIndex(isExportData) {
        return new Observable(observer => {
            this.loaderService.show();
            this.listData = [];
            //  this.assetDetailData = [];
            this.totalListDataCount = 0;
            let searchFields = [];
            if (this.enableIndexerFieldRestriction) {
                this.displayableMetadataFields.forEach((field) => {
                    let indexerDisplayFields: IndexerDisplayFields;
                    searchFields.push(indexerDisplayFields = {
                        field_id: field.INDEXER_FIELD_ID
                    });
                })
                searchFields.push({
                    field_id: 'NPD_TASK_IS_CP1_COMPLETED'
                })
            }
            let keyWord = '';
            // this.filterHelperService.getSearchCondidtion(this.selectedListFilter,this.selectedListDependentFilter, this.selectedGroupByFilter);
            const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level);
            this.storeSearchconditionObject = Object.assign({}, conditionObject);
            const value = this.selectedListFilter && !this.utilService.isNullOrEmpty(this.selectedListFilter) ? this.selectedListFilter.value : null;

            const otmmMPMDataTypes = ((this.isApprover || this.isMember) && value != 'MY_NA_REVIEW_TASKS') ? OTMMMPMDataTypes.DELIVERABLE : this.isReviewer ? OTMMMPMDataTypes.DELIVERABLE_REVIEW : ' ';
            let sortTemp = [];
            if (this.selectedSort && this.selectedSort.active) {
                const sortOption = this.taskMenuConfig.taskSortOptions.find(option => option.name === this.selectedSort.active);
                sortTemp = sortOption ? [{
                    field_id: sortOption.indexerId,
                    order: this.selectedSort.direction
                }] : [];
            }
            if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
                keyWord = this.searchConditions[0].keyword;
            } else if (this.searchConditions && this.searchConditions.length > 0) {
                conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(this.searchConditions);
            }
            this.emailSearchCondition = this.emailSearchCondition && Array.isArray(this.emailSearchCondition) ? this.emailSearchCondition : [];
            let parameters: SearchRequest = null;
            if (isExportData) {
                parameters = {
                    search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
                    keyword: keyWord,
                    grouping: {
                        group_by: this.level,
                        group_from_type: otmmMPMDataTypes, /* OTMMMPMDataTypes.DELIVERABLE, */
                        search_condition_list: {
                            search_condition: [...conditionObject.DELIVERABLE_CONDTION, ...this.emailSearchCondition]
                        }
                    },
                    search_condition_list: {
                        search_condition: conditionObject.TASK_CONDITION
                    },
                    displayable_field_list: {
                        displayable_fields: searchFields,
                    },
                    facet_condition_list: {
                        facet_condition: (this.facetRestrictionList && this.facetRestrictionList.facet_condition) ? this.facetRestrictionList.facet_condition : []
                    },
                    sorting_list: {
                        sort: sortTemp
                    },
                    cursor: {
                        page_index: this.skip,
                        page_size: this.totalDataCount
                    }
                };
            } else {
                parameters = {
                    search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
                    keyword: keyWord,
                    grouping: {
                        group_by: this.level,
                        group_from_type: otmmMPMDataTypes, /* OTMMMPMDataTypes.DELIVERABLE, */
                        search_condition_list: {
                            search_condition: [...conditionObject.DELIVERABLE_CONDTION, ...this.emailSearchCondition]
                        }
                    },
                    search_condition_list: {
                        search_condition: conditionObject.TASK_CONDITION
                    },
                    displayable_field_list: {
                        displayable_fields: searchFields,
                    },
                    facet_condition_list: {
                        facet_condition: (this.facetRestrictionList && this.facetRestrictionList.facet_condition) ? this.facetRestrictionList.facet_condition : []
                    },
                    sorting_list: {
                        sort: sortTemp
                    },
                    cursor: {
                        page_index: this.skip,
                        page_size: this.top
                    }
                };
            }
            // let obj = {
            //     field_id : "ID",
            //     relational_operator : "and",
            //     relational_operator_id : "MPM.OPERATOR.CHAR.IS_NOT_EMPTY",
            //     relational_operator_name : "is not empty",
            //     type: "string",
            // }
            // conditionObject.TASK_CONDITION.push(obj);
            if (!(JSON.stringify(this.previousRequest) === JSON.stringify(parameters))) {
                this.previousRequest = parameters;
                this.exportDataSearchRequest = parameters; // MVSS-241
                this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
                    /**Redirecting to first page if no data present */
                    if (response.data.length === 0 && this.skip > 0) {
                        this.skip = 0;
                        this.page = 1;
                        this.pageNumber = this.page;
                        if (!this.isnotified) {
                            this.notificationService.info('As there is no data in current page, redirecting to first page.');
                            this.isnotified = true;
                        }
                        this.loaderService.show();
                        this.routeComponentView();
                        this.setCurrentView();
                        this.getlistDataIndex(true).subscribe(response => {
                            this.loaderService.hide();
                        });
                    }
                    this.isDataRefresh = false;
                    this.loaderService.hide();
                    this.npdFacetService.updateOrderdDisplayableFields(this.displayableMetadataFields);
                    this.facetChangeEventService.updateFacetFilter(response.facet_field_response_list ? response.facet_field_response_list : []);
                    this.listData = response.data;
                    if (!isExportData) {
                        this.listData.forEach(data => {
                            const projectStatus = this.status.find(ele => ele === data?.PROJECT_STATUS_VALUE);
                            const taskStatus = this.status.find(ele => ele === data.TASK_STATUS_VALUE);
                            const deliverableStatus = this.status.find(ele => ele === this.selectedListDependentFilter.name);
                            if (data && (data.IS_ACTIVE_TASK || data.IS_ACTIVE_PROJECT) && projectStatus === undefined && taskStatus === undefined && deliverableStatus === undefined) {
                                data.bulkEdit = true;
                                this.enableBulkEdit = true;
                            } else {
                                data.bulkEdit = false;
                                this.enableBulkEdit = false;
                            }
                        });

                        this.totalListDataCount = response.cursor.total_records;
                        this.totalDataCount = response.cursor.total_records;
                        this.deliverableTaskFilters.forEach(filter => {
                            if (filter && filter.value && this.selectedListFilter && filter.value === this.selectedListFilter.value) {
                                filter.count = this.totalListDataCount;
                            }
                        });
                    } else {
                        this.exportTableData = response.data;
                    }
                    const proceed = response.data.length === 0 ? false : true;
                    observer.next(proceed);
                    observer.complete();

                }, error => {
                    this.notificationService.error('Failed while getting ' + this.titlecasePipe.transform(this.level) + ' lists');
                    this.loaderService.hide();
                });
            } else {
                this.loaderService.hide();
            }
        });

    }

    setLevel(value) {
        if (this.selectedGroupByFilter && this.selectedGroupByFilter.value === GroupByFilterTypes.GROUP_BY_CAMPAIGN || value === GroupByFilterTypes.GROUP_BY_CAMPAIGN) {
            this.level = MPM_LEVELS.CAMPAIGN;
        } else if (this.selectedGroupByFilter && this.selectedGroupByFilter.value === GroupByFilterTypes.GROUP_BY_PROJECT || value === GroupByFilterTypes.GROUP_BY_PROJECT) {
            this.level = MPM_LEVELS.PROJECT;
        } else if (this.selectedGroupByFilter && this.selectedGroupByFilter.value === GroupByFilterTypes.GROUP_BY_TASK || value === GroupByFilterTypes.GROUP_BY_TASK) {
            this.level = MPM_LEVELS.TASK;
        }
    }

    updateSearchConfigId() {
        if (this.taskMenuConfig.taskViewConfig) {
            const lastValue = this.searchConfigId;
            this.searchConfigId = parseInt(this.taskMenuConfig.taskViewConfig.R_PO_ADVANCED_SEARCH_CONFIG, 10);
            if (this.searchConfigId > 0 && lastValue !== this.searchConfigId) {
                this.searchChangeEventService.update(this.taskMenuConfig.taskViewConfig);
            }
        }
    }

    resetPagination() {
        this.page = 1;
        this.skip = 0;
    }

    reload(resetPagination: boolean, value?: any) {
        this.selectedTask = [];
        if (this.isReset && !this.isSearch) {
            this.resetPagination();
            this.isReset = false;
        }
        if (value === 'refresh' || value === 'bulk' || 'reload') {
            this.previousRequest = null;
        }
        this.getlistDataIndex(false).subscribe(projectsResponse => {
            this.loaderService.hide();
        }, projectsError => {
            this.loaderService.hide();
            this.notificationService.error(projectsError);
        });
        if (value != undefined) {
            this.count = 0;
        }
    }

    resetDisplayColumns() {
        sessionStorage.removeItem('USER_PREFERENCE-' + this.taskMenuConfig.taskViewConfig.VIEW);
        sessionStorage.removeItem('CURRENT_USER_PREFERENCE-' + this.taskMenuConfig.taskViewConfig.VIEW);
        let userPreference;
        if (this.defaultViewFilterName && !this.selectedListFilter) {
            userPreference = this.sharingService.getDefaultCurrentUserPreferenceByMenu(this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id);
        } else {
            userPreference = this.sharingService.getCurrentUserPreferenceByListView(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, this.selectedListFilter.name, this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id);
        }
        const userPreferenceId = userPreference && userPreference['MPM_User_Preference-id'] && userPreference['MPM_User_Preference-id'].Id ?
            userPreference['MPM_User_Preference-id'].Id : null;
        if (userPreferenceId) {
            this.loaderService.show();
            this.appService.deleteUserPreference(userPreferenceId).subscribe(response => {
                this.loaderService.hide();
                if (response) {
                    this.notificationService.success('User preference have been reset successfully');
                }
            });
        } else {
            this.notificationService.success('User preference have been reset successfully');
        }
        this.resetCurrentView();
        this.reload(false);
    }

    resetView() {
        sessionStorage.removeItem('USER_PREFERENCE-' + this.taskMenuConfig.taskViewConfig.VIEW);
        sessionStorage.removeItem('CURRENT_USER_PREFERENCE-' + this.taskMenuConfig.taskViewConfig.VIEW);
        let userPreference;
        if (this.defaultViewFilterName && !this.selectedListFilter) {
            userPreference = this.sharingService.getDefaultCurrentUserPreferenceByMenu(this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id);
        } else {
            userPreference = this.sharingService.getCurrentUserPreferenceByListView(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, this.selectedListFilter.name, this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id);
        }
        const userPreferenceId = userPreference && userPreference['MPM_User_Preference-id'] && userPreference['MPM_User_Preference-id'].Id ?
            userPreference['MPM_User_Preference-id'].Id : null;
        if (userPreferenceId) {
            this.loaderService.show();
            this.appService.deleteUserPreference(userPreferenceId).subscribe(response => {
                this.loaderService.hide();
                if (response) {
                    this.notificationService.success('User preference have been reset successfully');
                }
            });
        } else {
            this.notificationService.success('User preference have been reset successfully');
        }
        this.resetCurrentView();
    }

    resetCurrentView() {
        this.taskMenuConfig.taskViewConfig.displayFields.forEach(displayField => {
            displayField.width = '';
        });
        this.displayableMetadataFields = this.taskMenuConfig.taskViewConfig.displayFields;
        this.columnChooserFields = this.formColumnChooserField(this.taskMenuConfig.taskViewConfig.displayFields);
        this.userPreferenceFreezeCount = MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
        const userPreferenceData = this.formViewData();
        this.setCurrentViewData(userPreferenceData);
        this.defaultViewFilterName = null;
        this.setUserPreference();
    }

    setCurrentView() {
        const currentViewData = {
            sortName: (this.selectedSort && this.selectedSort.active) || this.sortChangeName,
            sortOrder: (this.selectedSort && this.selectedSort.direction) || this.sortChangeDirection,
            pageNumber: this.page,
            pageSize: this.pageSize,
            isListView: true,
            viewId: this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id,
            viewName: this.taskMenuConfig.taskViewConfig.VIEW,
            groupBy: this.selectedGroupByFilter && this.selectedGroupByFilter.value,
            taskStatusFilter: this.selectedListDependentFilter && this.selectedListDependentFilter.value,
            listfilter: this.selectedListFilter && this.selectedListFilter.name,
            isPMView: false
        }
        sessionStorage.setItem('CURRENT_VIEW', JSON.stringify(currentViewData));
        this.myTasksFilter = this.selectedListFilter?.value == 'MY_TASKS' && this.selectedGroupByFilter?.value == 'GROUP_BY_TASK' && this.selectedListDependentFilter.value == 'TO_DO_FILTER'
        //  this.myTasksFilter.emit();

    }

    setPageSetting() {
        if (!this.pageSelected) {
            const userPreference = this.sharingService.getCurrentUserPreferenceByList(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, this.selectedListFilter.name);
            const userPreferenceFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
            if (userPreferenceFieldData) {
                if (userPreferenceFieldData && !userPreferenceFieldData.pageSize) {
                    this.updatePagination();
                } else {
                    this.pageSize = userPreferenceFieldData.pageSize;
                    this.top = this.pageSize;
                    this.pageNumber = userPreferenceFieldData.pageNumber;
                    this.page = this.pageNumber
                    this.skip = (this.pageNumber - 1) * this.pageSize;
                    this.isdefaultPagination = false;
                }

                return true;
            }
        }
        return false;
    }

    saveDisplayColumns(columnFields) {
        this.updateDisplayColumns(columnFields);
        const requestObj = {
            R_PO_PERSON: {
                'Person-id': {
                    Id: this.sharingService.getCurrentPerson().Id
                }
            },
            PREFERENCE_TYPE: 'VIEW',
            R_PO_MPM_VIEW_CONFIG: {
                'MPM_View_Config-id': {
                    Id: this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id
                }
            },
            FIELD_DATA: JSON.stringify(this.userPreference.FIELD_DATA)
        };

        this.loaderService.show();
        let existingUserPreference;
        if (this.defaultViewFilterName && !this.selectedListFilter) {
            existingUserPreference = this.sharingService.getDefaultCurrentUserPreferenceByMenu(this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id);
        } else {
            existingUserPreference = this.sharingService.getCurrentUserPreferenceByListView(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, this.selectedListFilter.name, this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id);
        }
        const existingUserPreferenceId = existingUserPreference && existingUserPreference['MPM_User_Preference-id'] && existingUserPreference['MPM_User_Preference-id'].Id ?
            existingUserPreference['MPM_User_Preference-id'].Id : null;
        this.appService.updateUserPreference(requestObj, existingUserPreferenceId).subscribe(response => {
            this.loaderService.hide();
            if (response) {
                this.notificationService.success('User preference have been saved successfully');
            }
        });
    }

    updateDisplayColumns(displayColumnFields) {
        this.displayableMetadataFields = [];
        this.displayColumns = [];
        const displayableMetadataFields = [];
        const displayableColumns = [];

        displayColumnFields.column.sort((a, b) => a.POSITION - b.POSITION);
        displayColumnFields.column.forEach(displayColumnField => {
            if (displayColumnField.selected === true) {
                const selectedField = this.taskMenuConfig.taskViewConfig.displayFields.find(field => field.INDEXER_FIELD_ID === displayColumnField.INDEXER_FIELD_ID);
                if (selectedField) {
                    displayableMetadataFields.push(selectedField);
                    displayableColumns.push(selectedField.INDEXER_FIELD_ID);
                }
            }
        });
        this.userPreferenceFreezeCount = displayColumnFields.frezeCount
        if (this.enableIndexerFieldRestriction) {
            this.reload(false);
        }
        this.displayableMetadataFields = displayableMetadataFields;
        this.displayColumns = displayableColumns;
        this.displayColumns.unshift('Select');
        this.displayColumns.unshift('Expand');
        const userPreferenceData = this.formViewData();
        this.setViewData(userPreferenceData);
        this.setCurrentViewData(userPreferenceData);
        this.setCurrentView();
        this.reload(false);
    }

    formColumnChooserField(listFields) {
        const columnChooserFields = [];
        this.taskMenuConfig.taskViewConfig.displayFields.forEach((displayField, index) => {
            const selectedFieldIndex = listFields.findIndex(listField => listField.INDEXER_FIELD_ID === displayField.INDEXER_FIELD_ID);
            if (this.enableIndexerFieldRestriction) {
                if (selectedFieldIndex >= 0 && selectedFieldIndex < this.maximumAllowedFields) {
                    columnChooserFields.push({
                        INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
                        DISPLAY_NAME: displayField.DISPLAY_NAME,
                        POSITION: selectedFieldIndex,
                        selected: true
                    });
                } else {
                    columnChooserFields.push({
                        INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
                        DISPLAY_NAME: displayField.DISPLAY_NAME,
                        POSITION: index,
                        selected: false
                    });
                }
            } else {
                if (selectedFieldIndex >= 0) {
                    columnChooserFields.push({
                        INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
                        DISPLAY_NAME: displayField.DISPLAY_NAME,
                        POSITION: selectedFieldIndex,
                        selected: true
                    });
                } else {
                    columnChooserFields.push({
                        INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
                        DISPLAY_NAME: displayField.DISPLAY_NAME,
                        POSITION: index,
                        selected: false
                    });
                }
            }
        });
        return columnChooserFields;
    }

    setDefaultView() {
        const currUserPreference = this.sharingService.getCurrentUserPreferenceByList(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, this.selectedListFilter.name);
        let fieldDatas;
        /**Existing user preference present then updating the list dependent and groupby filter else updating directly */
        if (currUserPreference && currUserPreference["FIELD_DATA"].length > 0) {
            const data = JSON.parse(currUserPreference.FIELD_DATA);
            data.taskStatusFilter = this.selectedListDependentFilter.value;
            currUserPreference.FILTER_NAME = this.selectedListFilter.name;
            fieldDatas = data;
        }
        else {
            fieldDatas = {
                'groupBy': this.selectedGroupByFilter.value,
                'taskStatusFilter': this.selectedListDependentFilter.value
            }
        }

        const defaultView = this.sharingService.getDefaultCurrentUserPreferenceByMenu(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id);
        if (defaultView) { //|| this.defaultViewFilterName
            if (defaultView && defaultView.FILTER_NAME !== this.selectedListFilter.name ||
                this.defaultViewFilterName !== this.selectedListFilter.name) {
                const defaultFieldData = defaultView && !this.utilService.isNullOrEmpty(defaultView.FIELD_DATA) ? JSON.parse(defaultView.FIELD_DATA) : null;
                const defaultListFilter = this.deliverableTaskFilters.find(deliverableTaskFilter => deliverableTaskFilter.name === defaultView.FILTER_NAME);
                const allListDependentFilters = ((this.sharingService.getIsApprover() || this.sharingService.getIsMember()) && this.selectedListFilter.value != 'MY_NA_REVIEW_TASKS') ? getTaskListDependentFilters() : this.sharingService.getIsReviewer() ? getTaskListDependentReviewFilters() : '';
                this.listDependentFilters = allListDependentFilters[defaultListFilter.value];
                const defaultListDependentFilter = this.listDependentFilters.find(listDependentFilter => listDependentFilter.value === defaultFieldData.taskStatusFilter);
                const defaultGroupByFilter = this.groupByFilters.find(groupByFilter => groupByFilter.value === defaultFieldData.groupBy);

                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                    width: '40%',
                    disableClose: true,
                    data: {
                        message: 'Are you sure you want to override default view?<br />  <b>Current List View:</b> ' + defaultView.FILTER_NAME + " <br /> <b> Current GroupBy View: </b>" + defaultGroupByFilter.name + " <br /> <b> Current List Dependent View:</b>" + defaultListDependentFilter.name + "<br /> ",
                        submitButton: 'Yes',
                        cancelButton: 'No'
                    }
                });
                dialogRef.afterClosed().subscribe(result => {
                    if (result && result.isTrue) {
                        this.appService.updateDefaultUserPreference(this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id, this.selectedListFilter.name, this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, JSON.stringify(fieldDatas)).
                            subscribe(response => {
                                if (response) {
                                    this.setUpdatedDefaultView();
                                    this.notificationService.success('Default view has been saved successfully');
                                }
                            });
                    }
                });
            }
        } else {
            this.appService.updateDefaultUserPreference(this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id, this.selectedListFilter.name, this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, JSON.stringify(fieldDatas)).
                subscribe(response => {
                    this.setUpdatedDefaultView();
                    this.notificationService.success('Default view has been saved successfully');
                });
        }
    }

    setUpdatedDefaultView() {
        this.isDefaultFiltersPresent = false;
        this.defaultGroupViewFilterName = this.selectedGroupByFilter.value;
        this.defaultListDependentFilterName = this.selectedListDependentFilter.value;
        this.defaultViewFilterName = this.selectedListFilter.name;
    }

    getOrderedDisplayableFields(displayableFields) {
        this.columnChooserFields = this.formColumnChooserField(displayableFields);
        this.updateDisplayColumns({ 'column': this.columnChooserFields, 'frezeCount': this.userPreferenceFreezeCount });
    }

    resizeDisplayableFields() {
        const userPreference = this.formViewData();
        this.setViewData(userPreference);
        this.setCurrentViewData(userPreference);
    }

    setUserPreference() {
        let userPreferenceSessionData: any = sessionStorage.getItem('USER_PREFERENCE-' + this.taskMenuConfig.taskViewConfig.VIEW);
        userPreferenceSessionData = userPreferenceSessionData ? JSON.parse(userPreferenceSessionData) : null;
        const userPreferenceViewField = userPreferenceSessionData && userPreferenceSessionData.FIELD_DATA ? userPreferenceSessionData.FIELD_DATA : null;
        if (userPreferenceViewField) {
            userPreferenceViewField.sort((a, b) => {

                const firstKey: any = Object.keys(a);
                const firstValue = a[firstKey];
                const secondKey: any = Object.keys(b);
                const secondValue = b[secondKey];
                return firstValue.POSITION - secondValue.POSITION;
            });
            const displayableFields = [];
            const displayableColumns = [];
            userPreferenceViewField.forEach(viewField => {
                const viewFieldId = Object.keys(viewField)[0];
                if (viewField[viewFieldId].IS_DISPLAY === true) {
                    const selectedField = this.taskMenuConfig.taskViewConfig.displayFields.find(field => field.INDEXER_FIELD_ID === viewFieldId);
                    if (selectedField) {
                        selectedField.width = viewField[viewFieldId].WIDTH;
                        displayableFields.push(selectedField);
                        displayableColumns.push(selectedField.INDEXER_FIELD_ID);
                    }
                }
            });
            this.userPreferenceFreezeCount = userPreferenceSessionData.FREEZE_COLUMN_COUNT

            // if(this.userPreferenceFreezeCount===undefined)
            // this.userPreferenceFreezeCount= MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
            this.displayableMetadataFields = displayableFields;
            this.displayColumns = displayableColumns;
            this.displayColumns.unshift('Select');
            this.displayColumns.unshift('Expand');
            this.columnChooserFields = this.formColumnChooserField(displayableFields);

        } else {
            let userPreference;
            if (this.defaultViewFilterName && !this.selectedListFilter) {
                userPreference = this.sharingService.getDefaultCurrentUserPreferenceByMenu(this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id);
            } else {
                userPreference = this.sharingService.getCurrentUserPreferenceByListView(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, this.selectedListFilter.name, this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id);
            }
            let currentViewFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
            this.taskMenuConfig.taskViewConfig.freezeCount = this.userPreferenceFreezeCount
            if (currentViewFieldData != null && this.utilService.getBooleanValue(userPreference.IS_LIST_VIEW)) {
                this.userPreferenceFreezeCount = currentViewFieldData.FREEZE_COLUMN_COUNT
                const actualData = [];
                if (currentViewFieldData && currentViewFieldData.data) {
                    currentViewFieldData.data.forEach(data => {
                        actualData.push(data);
                    });
                    currentViewFieldData = actualData;
                    currentViewFieldData.sort((a, b) => {
                        const firstKey: any = Object.keys(a);
                        const firstValue = a[firstKey];
                        const secondKey: any = Object.keys(b);
                        const secondValue = b[secondKey];
                        return firstValue.POSITION - secondValue.POSITION;
                    });
                    const displayableFields = [];
                    const displayableColumns = [];
                    currentViewFieldData.forEach(viewField => {
                        const viewFieldId = Object.keys(viewField)[0];
                        if (viewField[viewFieldId].IS_DISPLAY === true) {
                            const selectedField: any = this.taskMenuConfig.taskViewConfig.displayFields.find(field => field.INDEXER_FIELD_ID === viewFieldId);
                            if (selectedField) {
                                selectedField.width = viewField[viewFieldId].WIDTH;
                                displayableFields.push(selectedField);
                                displayableColumns.push(selectedField.INDEXER_FIELD_ID);
                            }
                        }
                    });
                    this.displayableMetadataFields = displayableFields;
                    this.displayColumns = displayableColumns;
                    this.displayColumns.unshift('Select');
                    this.displayColumns.unshift('Expand');
                    this.columnChooserFields = this.formColumnChooserField(displayableFields);
                }
            } else {
                this.displayableMetadataFields = this.taskMenuConfig.dataSet.DISPLAY_DEFAULT_FIELDS;
                this.displayColumns = this.taskMenuConfig.dataSet.DISPLAY_COLUMNS.slice();
                this.displayColumns.unshift('Select');
                this.displayColumns.unshift('Expand');
                this.columnChooserFields = this.formColumnChooserField(this.taskMenuConfig.dataSet.DISPLAY_DEFAULT_FIELDS);
                this.userPreferenceFreezeCount = MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
            }
        }
        if (this.enableIndexerFieldRestriction) {
            this.displayableMetadataFields = this.displayableMetadataFields.slice(0, this.maximumAllowedFields);
            this.displayColumns = this.displayColumns.slice(0, this.maximumAllowedFields + 2);
        }
    }

    setViewData(userPreference) {
        sessionStorage.setItem('USER_PREFERENCE-' + this.taskMenuConfig.taskViewConfig.VIEW, userPreference);
    }

    setCurrentViewData(userPreference) {
        sessionStorage.setItem('CURRENT_USER_PREFERENCE-' + this.taskMenuConfig.taskViewConfig.VIEW, userPreference);
    }

    getLevelDetails(trigger?) {
        if (this.level) {
            this.subscriptions.push(
                this.deliverableTaskMetadataService.getMPMFieldsByLevel(this.level).subscribe((viewInfo: ViewConfig) => {
                    if (!this.taskMenuConfig.taskViewConfig || (this.taskMenuConfig.taskViewConfig.VIEW !== viewInfo.VIEW)) {
                        this.taskMenuConfig.taskViewConfig = viewInfo;
                        this.updateSearchConfigId();
                    }
                    const dataSet = this.fieldConfigService.formrequiredDataForLoadView(viewInfo, true);
                    this.taskMenuConfig.dataSet = dataSet;

                    this.taskMenuConfig.taskViewConfig = viewInfo;
                    let userPreference;
                    if (this.defaultViewFilterName && !this.selectedListFilter) {
                        userPreference = this.sharingService.getDefaultCurrentUserPreferenceByMenu(this.sharingService.getCurrentMenu()['MPM_Menu-id'].Id);
                    } else {
                        userPreference = this.sharingService.getCurrentUserPreferenceByListView(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, this.selectedListFilter.name, this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id);
                    }
                    userPreference = userPreference ? userPreference : null;
                    let userPreferenceFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
                    this.taskMenuConfig.taskViewConfig.displayFields = viewInfo.R_PM_LIST_VIEW_MPM_FIELDS;
                    this.setUserPreference();
                    /** Sorting */
                    this.taskMenuConfig.taskSortOptions = dataSet.SORTABLE_FIELDS;
                    let isDefaultSort = true;
                    if (this.selectedSortBy || this.selectedSortOrder) {
                        const selectedSortData = this.taskMenuConfig.taskSortOptions.find(sortableField => sortableField.name === this.selectedSortBy);
                        if (selectedSortData) {
                            isDefaultSort = false;
                            this.selectedSort.active = this.selectedSortBy;
                            this.selectedSort.direction = this.selectedSortOrder ? this.selectedSortOrder : this.selectedSort.direction;
                        } else {
                            isDefaultSort = false;
                            this.selectedSort.active = dataSet.DEFAULT_SORT_FIELD.option.name;
                            this.selectedSort.direction = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc';
                        }
                    } else if (userPreferenceFieldData && userPreferenceFieldData.data) {
                        userPreferenceFieldData.data.forEach(field => {
                            const fieldId = Object.keys(field)[0];
                            const sortData = field;
                            if (field[fieldId].IS_SORTABLE === true) {
                                const selectedField = this.taskMenuConfig.taskSortOptions.find(field => field.indexerId === fieldId);
                                if (selectedField) {
                                    isDefaultSort = false;
                                    this.selectedSort = sortData;
                                    this.selectedSort.direction = field[fieldId].SORT_ORDER ? field[fieldId].SORT_ORDER : (this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc');
                                    this.selectedSort.active = selectedField.name;
                                }
                            }
                        });
                    }
                    if (isDefaultSort) {
                        this.selectedSort = this.taskMenuConfig.taskSortOptions[0];
                        this.selectedSort.active = this.taskMenuConfig.taskSortOptions[0]?.name;
                        this.selectedSort.direction = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc';
                        if (dataSet.DEFAULT_SORT_FIELD) {
                            this.selectedSort.active = dataSet.DEFAULT_SORT_FIELD.option.name;
                        }
                    }

                    /** GroupByFilter */
                    if (!this.selectedGroupByFilter) {
                        if (this.defaultGroupViewFilterName) {
                            this.groupByFilters.forEach(groupByFilter => {
                                if (groupByFilter.value === this.defaultGroupViewFilterName) {
                                    this.selectedGroupByFilter = groupByFilter;
                                }
                            });
                        } else {
                            this.selectedGroupByFilter = this.enableCampaign ? GROUP_BY_ALL_FILTERS[0] : GROUP_BY_ALL_FILTERS[1];
                        }
                    }
                    /** ListFilter and ListDependentFilter */
                    if (!this.selectedListFilter) {
                        if (!this.utilService.isNullOrEmpty(userPreferenceFieldData)) {
                            this.selectedListFilter = this.deliverableTaskFilters.find(deliverableTaskFilter => deliverableTaskFilter.name === this.defaultViewFilterName);
                            this.listDependentFilters = this.allListDependentFilters[this.selectedListFilter?.value];
                            this.listDependentFilters?.forEach(listDependentFilter => {
                                if (listDependentFilter.value === userPreferenceFieldData.taskStatusFilter) {
                                    this.selectedListDependentFilter = listDependentFilter;
                                }
                            });
                        }
                    }

                    if (!this.sortHandled && !this.skipPageSetting) {
                        this.setPageSetting();
                        this.sortHandled = false;
                    } else {
                        this.resetPagination();
                    }

                    if (!(trigger !== undefined && trigger === true)) {
                        this.reload(false);
                    }
                }, (MPMFieldError) => {
                    this.loaderService.hide();
                    this.notificationService.error(MPMFieldError);
                })
            );

            this.setCurrentView();
            const userPreferenceData = this.formViewData();
            this.setCurrentViewData(userPreferenceData);
        }
    }

    initializeTaskFilters() {
        if (!this.level) {
            if (this.defaultGroupViewFilterName) {
                this.groupByFilters.forEach(groupByFilter => {
                    if (groupByFilter.value === this.defaultGroupViewFilterName) {
                        this.setLevel(groupByFilter.value);
                    }
                });
            } else {
                if (this.selectedGroupByFilter) {
                    this.setLevel('');
                }
            }
        }

        if (this.defaultUserPreferenceView) {
            if (this.selectedListDependentFilter) {
                this.isDefaultFiltersPresent = this.defaultViewFilterName !== this.selectedListFilter.name;
            } else {
                this.isDefaultFiltersPresent = false;
            }
        } else {
            this.isDefaultFiltersPresent = true;
        }
    }

    deliverableTaskFilterChanged(filter: ListFilter) {
        this.isReset = true;
        this.calculatedDeliverableTaskFilters = [];
        this.facetRestrictionList = {};
        this.facetChangeEventService.resetFacet();
        this.selectedListFilter = filter;
        this.currentfilter = this.selectedListFilter;
        const allListDependentFilters = ((this.sharingService.getIsApprover() || this.sharingService.getIsMember()) && this.selectedListFilter.value != 'MY_NA_REVIEW_TASKS') ? getTaskListDependentFilters() : this.sharingService.getIsReviewer() ? getTaskListDependentReviewFilters() : '';
        this.listDependentFilters = allListDependentFilters[filter.value];

        if (!(this.searchName || this.savedSearchName || this.advSearchData || this.facetData)) {
            this.isRoute = true;
        }

        /**Changing  group by and list dependent filter based on the user preference */
        const userPreference = this.sharingService.getCurrentUserPreferenceByList(this.sharingService.getCurrentMenu()["MPM_Menu-id"].Id, this.selectedListFilter.name);
        const fieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
        if (userPreference && !this.utilService.isNullOrEmpty(userPreference) && fieldData && (userPreference.FILTER_NAME === this.selectedListFilter.name)) {
            this.listDependentFilters.forEach(listDependentFilter => {
                if (listDependentFilter.value === fieldData.taskStatusFilter) {
                    this.selectedListDependentFilter = listDependentFilter;
                }
            });
            this.groupByFilters.forEach(groupByFilter => {
                if (groupByFilter.value === fieldData.groupBy) {
                    this.selectedGroupByFilter = groupByFilter;
                    this.setLevel(this.selectedGroupByFilter.value);
                }
            });

            this.isReset = false;
            this.isSearch = true;
            this.pageSelected = false;
            this.skipPageSetting = false;

            this.setPageSetting();
            this.routeComponentView();
        } else {
            this.selectedListDependentFilter = allListDependentFilters[filter.value] && allListDependentFilters[filter.value][0] != undefined ? allListDependentFilters[filter.value][0] : allListDependentFilters[filter.value];
            this.updatePagination();
        }
        const userPreferenceData = this.formViewData();
        this.setCurrentViewData(userPreferenceData);
        this.setCurrentView();
    }

    listDependentFilterChange(option: CustomSelect) {
        this.isReset = true;
        this.selectedListDependentFilter = option;
        this.facetRestrictionList = {};
        this.facetChangeEventService.resetFacet();
        this.initializeTaskFilters();

        if (!this.facetData) {
            this.isRoute = true;
        }
        this.routeComponentView();
    }

    groupByFilterChange(option: CustomSelect) {
        this.isReset = true;
        this.selectedGroupByFilter = option;
        this.facetRestrictionList = {};
        this.facetChangeEventService.resetFacet();
        this.setLevel('');

        if (!(this.searchName || this.savedSearchName || this.advSearchData || this.facetData)) {
            this.isRoute = true;
        }
        this.routeComponentView();
    }

    pagination(pagination: any) {
        this.pageSelected = true;
        this.pageSize = pagination.pageSize;
        this.top = pagination.pageSize;
        this.skip = (pagination.pageIndex * pagination.pageSize);
        this.page = 1 + pagination.pageIndex;
        this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_PAGE_LIMIT] = (pagination.pageSize).toString();
        this.isRoute = true;
        this.skipPageSetting = false;
        this.sortHandled = false;
        const userPreferenceData = this.formViewData();
        this.setCurrentViewData(userPreferenceData);
        this.setCurrentView();
        this.routeComponentView();
    }

    sortChangeHandler(sortData: MatSort) {

        this.top = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.pageSize = this.utilService.convertStringToNumber(this.pageSize);
        this.skip = 0;
        this.page = 1;
        this.pageNumber = this.page;
        this.sortHandled = true;

        if (!sortData.active || !sortData.direction) {
            this.selectedSort = null;
        } else {
            this.selectedSort = sortData;
            this.selectedSortBy = this.selectedSort.active;
            this.selectedSortOrder = this.selectedSort.direction;
        }
        this.isRoute = true;
        this.routeComponentView();
        this.loaderService.show();
    }

    /* deliverableDetails(event) {
        this.assetService.getMetadataFields('ASSET_DETAILS').subscribe(response => {
            const asset = event;
            asset.metadataFields = response.MPM_Metadata_Fields;
            asset.isTemplate = false;
            this.dialog.open(AssetDetailsComponent, {
                width: '70%',
                disableClose: true,
                data: asset
            });
        });
    } */
    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }

    notificationHandler(event) {
        if (event.message) {
            this.notification.error(event.message);
        }
    }
    deliverableDetails(event) {
        const deliverableItemId = this.taskService.getProperty(event, DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID);
        const assetDetailSearchCondition = {
            search_condition_list: {
                search_condition: [{
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.UTILS.DATA_TYPE',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    value: OTMMMPMDataTypes.DELIVERABLE
                }, {
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.DELIVERABLE.ITEM_ID',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    value: deliverableItemId,
                    relational_operator: 'and'
                }]
            }
        };
        const searchCondition = JSON.parse(JSON.stringify(AssetConstants.ASSET_DETAILS_CONDITION));
        if (event && event.metadata) {
            const fieldConfigs = this.fieldConfigService.getFieldsbyFeildCategoryLevel(event.metadata.object_type);

            searchCondition.search_condition_list.search_condition.map(function (search) {
                const fieldConfig = fieldConfigs.find(function (field) { return search.value == field.MAPPER_NAME });
                const metadata = event.metadata.metadata_element_list[0].metadata_element_list.find(function (metadata) {
                    return metadata.id == fieldConfig.OTMM_FIELD_ID;
                })
                return search.value = metadata.value.value.value;
            });
        }
        else {
            const fieldConfigs = this.fieldConfigService.getFieldsbyFeildCategoryLevel('ASSET');
            /*  const searchCondition = JSON.parse(JSON.stringify(AssetConstants.ASSET_DETAILS_CONDITION)); */

            /*   var task_id = event.TASK_ITEM_ID.split('.')[1];
              var task_Id = event.TASK_ITEM_ID.split('.')[0];
              var taskfinal = task_Id + '.' + task_id;

              var del_id = event.deliverableItemId.split('.')[1];
              var del_Id = event.deliverableItemId.split('.')[0];
              var delfinal2 = del_Id + '.' + del_id; */
            var ids = [];
            ids.push(event.PROJECT_ITEM_ID);
            ids.push(event.TASK_ITEM_ID);
            ids.push(event.deliverableItemId);
            /*  ids.push(taskfinal);
             ids.push(delfinal2); */

            searchCondition.search_condition_list.search_condition.map(function (search, index) {
                const fieldConfig = fieldConfigs.find(function (field) { return search.value == field.MAPPER_NAME });
                /* const metadata = event.metadata.metadata_element_list[0].metadata_element_list.find(function (metadata) {
                    return metadata.id == fieldConfig.OTMM_FIELD_ID;
                }) */

                /*  return search.value = metadata.value.value.value; */

                return search.value = ids[index];
            });
        }
        this.loadingHandler(true);
        this.otmmService.checkOtmmSession()
            .subscribe(sessionResponse => {
                let userSessionId;
                const skip = 0;
                const top = 10;
                if (sessionResponse && sessionResponse.session_resource && sessionResponse.session_resource.session && sessionResponse.session_resource.session.id) {
                    userSessionId = sessionResponse.session_resource.session.id;
                }
                this.assetService.getAssetDetails(searchCondition).subscribe(asset_details => {
                    if (asset_details && asset_details.data && asset_details.data.length > 0) {
                        let asset = {};
                        asset['data'] = asset_details.data;
                        asset['metadata_fields'] = this.sharingService.getAllApplicationViewConfig().find(application_views => { return application_views.VIEW === 'ASSET_DETAILS_VIEW' });
                        if (asset) {
                            asset['isTemplate'] = true;
                            this.dialog.open(AssetDetailsComponent, {
                                width: '70%',
                                disableClose: true,
                                data: asset
                            });
                            this.loadingHandler(false);
                        } else {
                            const eventData = { message: 'Error while getting asset details', type: 'error' };
                            this.notificationHandler(eventData);
                        }
                    } else {
                        this.loadingHandler(false);
                    }
                }, asset_details_error => {
                    this.loadingHandler(false);
                    const eventData = { message: 'Error getting Asset related details', type: 'error' };
                    this.notificationHandler(eventData);
                });
            }, session_error => {
                this.loadingHandler(false);
                const eventData = { message: 'Error getting OTMM session details', type: 'error' };
                this.notificationHandler(eventData);
            });

    }
    retrieveAssetData(event) {
        const deliverableItemId = this.taskService.getProperty(event, DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID);
        const assetDetailSearchCondition = {
            search_condition_list: {
                search_condition: [{
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.UTILS.DATA_TYPE',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    value: OTMMMPMDataTypes.DELIVERABLE
                }, {
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.DELIVERABLE.ITEM_ID',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    value: deliverableItemId,
                    relational_operator: 'and'
                }]
            }
        };

        const fieldConfigs = this.fieldConfigService.getFieldsbyFeildCategoryLevel(event.metadata.object_type);

        if (event && event.metadata && event.metadata.metadata_element_list) {
            event.metadata.metadata_element_list.forEach(metadataElement => {
                if (metadataElement.id === 'MPM.ASSET.GROUP') {
                    metadataElement.metadata_element_list.forEach(metadataElementList => {
                        if (metadataElementList.id === 'MPM.ASSET.IS_REFERENCE_ASSET') {
                            this.isReferenceAsset = metadataElementList.value.value.value === 'true' ? true : false;
                        }
                    });
                }
            });
        }

        // const searchCondition = JSON.parse(JSON.stringify(AssetConstants.ASSET_DETAILS_CONDITION));

        const searchCondition = this.isReferenceAsset ? JSON.parse(JSON.stringify(AssetConstants.REFERENCE_ASSET_DETAILS_CONDITION)) : JSON.parse(JSON.stringify(AssetConstants.ASSET_DETAILS_CONDITION));

        searchCondition.search_condition_list.search_condition.map(function (search) {
            const fieldConfig = fieldConfigs.find(function (field) { return search.value == field.MAPPER_NAME });
            const metadata = event.metadata.metadata_element_list[0].metadata_element_list.find(function (metadata) {
                return metadata.id === fieldConfig.OTMM_FIELD_ID;
            });
            return search.value = (metadata && metadata.value && metadata.value.value && metadata.value.value.value) ? metadata.value.value.value : '';
        });

        this.loadingHandler(true);
        this.otmmService.checkOtmmSession()
            .subscribe(sessionResponse => {
                let userSessionId;
                const skip = 0;
                const top = 10;
                if (sessionResponse && sessionResponse.session_resource && sessionResponse.session_resource.session && sessionResponse.session_resource.session.id) {
                    userSessionId = sessionResponse.session_resource.session.id;
                }
                this.assetService.getAssetDetails(searchCondition).subscribe(asset_details => {
                    if (asset_details && asset_details.data && asset_details.data.length > 0) {
                        let asset = {};
                        asset['data'] = asset_details.data;
                        asset['metadata_fields'] = this.sharingService.getAllApplicationViewConfig().find(application_views => { return application_views.VIEW === 'ASSET_DETAILS_VIEW' });
                        if (asset) {
                            asset['isTemplate'] = true;
                            this.dialog.open(AssetDetailsComponent, {
                                width: '70%',
                                disableClose: true,
                                data: asset
                            });
                            this.loadingHandler(false);
                        } else {
                            const eventData = { message: 'Error while getting asset details', type: 'error' };
                            this.notificationHandler(eventData);
                        }
                    } else {
                        this.loadingHandler(false);
                    }
                }, asset_details_error => {
                    this.loadingHandler(false);
                    const eventData = { message: 'Error getting Asset related details', type: 'error' };
                    this.notificationHandler(eventData);
                });
            }, session_error => {
                this.loadingHandler(false);
                const eventData = { message: 'Error getting OTMM session details', type: 'error' };
                this.notificationHandler(eventData);
            });
    }
    viewVersions(event) {
        this.dialog.open(AssetsVersionModalComponent, {
            width: '70%',
            disableClose: true,
            data: event
        });
    }

    bulkAction(bulkStatus, bulkStatusAction) {
        this.bulkStatus = bulkStatus;
        this.bulkStatusAction = bulkStatusAction;
        const dialogRef = this.dialog.open(BulkCommentsComponent, {
            width: '40%',
            disableClose: true,
            data:
            {
                message: 'Bulk Approval',
                hasConfirmationComment: true,
                status: this.bulkStatus,
                deliverableData: this.deliverableData,
                projectData: this.projectData,
                statusMessage: 'Approval State',
                commentText: 'Comments',
                isCommentRequired: true,
                errorMessage: 'Kindly fill all the required fields',
                submitButton: 'Yes',
                cancelButton: 'No',
                statusData: this.projectStatus,
            }
        });
        dialogRef.afterClosed().subscribe(bulkComment => {
            if (bulkComment) {
                const appId = this.entityAppdefService.getApplicationID();
                const crActions = this.sharingService.getCRActions();
                const teamItemId = this.projectData.R_PO_TEAM['MPM_Teams-id'].Id;
                const crAction = crActions.find(el => el['MPM_Teams-id'].Id === teamItemId);
                const selectedReasonsData = bulkComment.SelectedReasons;
                const assetId = '';
                const versioning = true;
                const isBulkApprove = true;
                const taskName = '';
                let teamRole;
                const userRoles = acronui.findObjectsByProp(crAction, 'MPM_Team_Role_Mapping');
                userRoles.forEach(role => {
                    const appRoles = acronui.findObjectsByProp(role, 'MPM_APP_Roles');
                    const roleObj = appRoles.find(appRole => {
                        return appRole.ROLE_NAME === MPM_ROLES.APPROVER;
                    });
                    if (roleObj) {
                        teamRole = role;
                    }
                });
                if (teamRole && teamRole.Approver_CR_Role && teamRole.Approver_CR_Role.ROLE_ID) {
                    this.crRole = teamRole.Approver_CR_Role;
                }

                this.performBulkAction(this.bulkStatusAction, appId, this.deliverableData, assetId, this.crRole.ROLE_ID, versioning, taskName, bulkComment.comment, isBulkApprove, selectedReasonsData);
            }
        });
    }

    getBulkActionData(appId, deliverableId, assetId, crRoleId, versioning, taskName, bulkComment, isBulkApprove, statusId, selectedReasonsData): Observable<any> {
        return new Observable(observer => {
            this.appService.handleCRApproval(appId, deliverableId, assetId, taskName, crRoleId, versioning, bulkComment, isBulkApprove, statusId, selectedReasonsData, '')
                .subscribe(response => {
                    if (response) {
                        this.bulkResponseData = deliverableId;
                        this.notificationService.info('Bulk Approval process initiated');
                        this.isReset = true;
                        this.reload(false, 'bulk');
                        this.loaderService.hide();
                    } else {
                        observer.error('Something went wrong in Bulk Approval process');
                    }
                }, error => {
                    observer.error(error);
                });
        });
    }

    performBulkAction(action: 'Approve' | 'Reject' | 'Complete', appId, deliverableData, assetId, crRole, versioning, taskName, bulkComment, isBulkApprove, selectedReasonsData) {
        let statusType;
        this.loaderService.show();

        if (action === 'Approve') {
            statusType = StatusTypes.FINAL_APPROVED;
        } else if (action === 'Reject') {
            statusType = StatusTypes.FINAL_REJECTED;
        } else if (action === 'Complete') {
            statusType = StatusTypes.FINAL_COMPLETED;
        }

        this.statusService.getStatusByCategoryLevelAndStatusType(MPM_LEVELS.DELIVERABLE, statusType)
            .subscribe(response => {
                const status = response.find(el => el.STATUS_TYPE === StatusTypes.FINAL_COMPLETED || el.STATUS_TYPE === StatusTypes.FINAL_APPROVED || el.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                if (status) {
                    this.statusId = status['MPM_Status-id'].Id;
                    this.getBulkActionData(appId, deliverableData, assetId, crRole, versioning, taskName, bulkComment, isBulkApprove, this.statusId, selectedReasonsData)
                        .subscribe(crData => {
                            this.loaderService.hide();
                            //this.crHandler.next(crData);
                        }, error => {
                            this.loaderService.hide();
                            this.notificationService.error('Something went wrong while opening Creative Review');
                        });
                } else {
                    this.loaderService.hide();
                    this.notificationService.error('Something went wrong while performing the deliverable action');
                }
            }, error => {
                this.loaderService.hide();
                this.notificationService.error('Something went wrong while performing the deliverable action');
            });
    }

    shareAsset(event) {
        const currentUserEmail = this.sharingService.getCurrentUserEmailId();

        if (currentUserEmail && currentUserEmail.length > 0) {
            this.dialog.open(ShareAssetsModalComponent, {
                width: '50%',/* 38% */
                disableClose: true,
                data: event
            });
        } else {
            this.notificationService.error('The user does not have Email Id');
        }
    }

    deliverableTasksViewComponentRoute(params: ParamMap): DeliverableTasksViewComponentParams {
        const groupByFilter = params.get('groupByFilter') || '';
        const deliverableTaskFilter = params.get('deliverableTaskFilter') || '';
        const listDependentFilter = params.get('listDependentFilter') || '';
        const emailLinkId = params.get('emailLinkId') || '';
        const sortBy = params.get('sortBy') || '';
        const sortOrder = params.get('sortOrder') === 'asc' ? MatSortDirections.ASC : (params.get('sortOrder') === 'desc' ? MatSortDirections.DESC : null);
        const pageNumber = params.get('pageNumber') ? Number(params.get('pageNumber')) : null;
        const pageSize = params.get('pageSize') ? Number(params.get('pageSize')) : null;
        const searchName = params.get('searchName') ? params.get('searchName') : null;
        const savedSearchName = params.get('savedSearchName') ? params.get('savedSearchName') : null;
        const advSearchData = params.get('advSearchData') ? params.get('advSearchData') : null;
        const facetData = params.get('facetData') ? params.get('facetData') : null;
        return {
            groupByFilter,
            deliverableTaskFilter,
            listDependentFilter,
            emailLinkId,
            sortBy,
            sortOrder,
            pageNumber,
            pageSize,
            searchName,
            savedSearchName,
            advSearchData,
            facetData
        };
    }

    initalizeComponent(routeData: DeliverableTasksViewComponentParams) {
        if (!(this.selectedGroupByFilter && routeData.groupByFilter === this.selectedGroupByFilter.value && this.selectedListFilter &&
            routeData.deliverableTaskFilter === this.selectedListFilter.value && this.selectedListDependentFilter &&
            routeData.listDependentFilter === this.selectedListDependentFilter.value && routeData.sortBy === this.selectedSortBy &&
            routeData.sortOrder === this.selectedSortOrder && routeData.pageNumber === this.selectedPageNumber && routeData.pageSize === this.selectedPageSize &&
            routeData.searchName === this.searchName && routeData.savedSearchName === this.savedSearchName && routeData.advSearchData === this.advSearchData &&
            routeData.facetData === this.facetData) || this.isRoute) {

            this.isRoute = false;
            let routedGroupFilter;
            let routedDeliverableTaskFilter;
            let routedListDependentFilters;

            /**groupby filter */
            if (routeData.groupByFilter) {
                routedGroupFilter = this.enableCampaign ? GROUP_BY_ALL_FILTERS.find(filter => filter.value === routeData.groupByFilter) : GROUP_BY_FILTERS.find(filter => filter.value === routeData.groupByFilter);
            }
            if (routedGroupFilter === undefined) {
                if (this.selectedGroupByFilter === undefined) {
                    this.selectedGroupByFilter = this.enableCampaign ? GROUP_BY_ALL_FILTERS[0] : GROUP_BY_ALL_FILTERS[1];
                }
            }
            else {
                this.selectedGroupByFilter = this.selectedGroupByFilter ? this.selectedGroupByFilter : routedGroupFilter;
            }
            this.setLevel('');

            if (routeData.deliverableTaskFilter) {
                routedDeliverableTaskFilter = this.deliverableTaskFilters.find(filter => filter.value === routeData.deliverableTaskFilter);
            }
            this.selectedListFilter = routedDeliverableTaskFilter ? routedDeliverableTaskFilter : this.selectedListFilter;

            if (this.selectedListFilter === undefined) {
                let filter;
                if (this.defaultViewFilterName) {
                    filter = this.deliverableTaskFilters.find(filter => filter.name === this.defaultViewFilterName);
                }
                this.selectedListFilter = filter === undefined ? (routedDeliverableTaskFilter ? routedDeliverableTaskFilter : this.deliverableTaskFilters[0]) : filter;//ternary
                const allListDependentFilters = ((this.sharingService.getIsApprover() || this.sharingService.getIsMember()) && this.selectedListFilter.value !== 'MY_NA_REVIEW_TASKS') ?
                    getTaskListDependentFilters() : this.sharingService.getIsReviewer() ? getTaskListDependentReviewFilters() : '';
                this.listDependentFilters = allListDependentFilters[this.selectedListFilter.value];
                if (routeData.listDependentFilter) {
                    routedListDependentFilters = this.listDependentFilters.find(filter => filter.value === routeData.listDependentFilter);//update find
                }
                this.selectedListDependentFilter = routedListDependentFilters ? routedListDependentFilters : this.listDependentFilters[0];
            } else {
                if (routeData.listDependentFilter) {
                    const allListDependentFilters = ((this.sharingService.getIsApprover() || this.sharingService.getIsMember()) && this.selectedListFilter.value !== 'MY_NA_REVIEW_TASKS') ?
                        getTaskListDependentFilters() : this.sharingService.getIsReviewer() ? getTaskListDependentReviewFilters() : '';
                    this.listDependentFilters = allListDependentFilters[this.selectedListFilter.value];
                    routedListDependentFilters = this.listDependentFilters.find(filter => filter.value === routeData.listDependentFilter);//update find
                }
                this.selectedListDependentFilter = routedListDependentFilters ? routedListDependentFilters : this.selectedListDependentFilter;
            }

            if (routeData.sortBy) {
                this.selectedSortBy = routeData.sortBy;
            }
            if (routeData.sortOrder) {
                this.selectedSortOrder = routeData.sortOrder;
            }
            if (routeData.pageNumber || this.page) {
                this.selectedPageNumber = this.page === undefined ? routeData.pageNumber : this.page;
            }
            if (routeData.pageSize || this.pageSize) {
                this.selectedPageSize = this.pageSize === undefined ? routeData.pageSize : this.pageSize;
            }

            if ((routeData.searchName === null && this.searchName) || (this.searchName === null && routeData.searchName)
                || (routeData.savedSearchName === null && this.savedSearchName) || (this.savedSearchName === null && routeData.savedSearchName)
                || (routeData.advSearchData === null && this.advSearchData) || (this.advSearchData === null && routeData.advSearchData)) {
                this.selectedPageNumber = 1;
                this.searchName = routeData.searchName;
                this.savedSearchName = routeData.savedSearchName;
                this.advSearchData = routeData.advSearchData;
                this.isReset = true;
                this.routeComponentView();
            } else if ((routeData.facetData === null && this.facetData) || (this.facetData === null && routeData.facetData)) {
                this.selectedPageNumber = 1;
                this.facetData = routeData.facetData;
                this.isReset = true;
                this.routeComponentView();
            } else {
                if (routeData.searchName) {
                    this.searchName = routeData.searchName;
                } else {
                    this.searchName = null;
                }
                if (routeData.savedSearchName) {
                    this.savedSearchName = routeData.savedSearchName;
                } else {
                    this.savedSearchName = null;
                }
                if (routeData.advSearchData) {
                    this.advSearchData = routeData.advSearchData;
                } else {
                    this.advSearchData = null;
                }
                if (routeData.facetData) {
                    this.facetData = routeData.facetData;
                } else {
                    this.facetData = null;
                }
                this.componentRendering = false;
                if (routeData.emailLinkId) {
                    this.emailDataService.getEmailSearchDataByGuid(this.emailLinkId).subscribe(data => {
                        if (data.SEARCH_CONDITION.conditions) {
                            this.emailSearchCondition = data.SEARCH_CONDITION.conditions;
                        }
                        this.getLevelDetails();
                    });
                } else {
                    this.emailLinkId = '';
                    this.emailSearchCondition = [];
                    if (this.isPageRefresh && (this.searchName || this.advSearchData || this.savedSearchName)) {
                        this.getLevelDetails();// true
                    } else if (!(this.isPageRefresh && this.facetData)) {
                        this.getLevelDetails(); // true
                    }
                }
            }
        }
        this.isPageRefresh = false;
    }

    formViewData() {
        const userPreference = {
            USER_ID: this.sharingService.getCurrentPerson().Id,
            PREFERENCE_TYPE: 'VIEW',
            VIEW_ID: this.taskMenuConfig.taskViewConfig['MPM_View_Config-id'].Id,
            FIELD_DATA: [],
            FREEZE_COLUMN_COUNT: 0
        };
        this.taskMenuConfig.taskViewConfig.displayFields.forEach(viewField => {

            if (viewField.INDEXER_FIELD_ID) {
                const fieldIndex = this.displayableMetadataFields.findIndex(displayableField => displayableField.INDEXER_FIELD_ID === viewField.INDEXER_FIELD_ID);
                userPreference.FIELD_DATA.push({
                    [viewField.INDEXER_FIELD_ID]: {
                        POSITION: fieldIndex >= 0 ? fieldIndex : null,
                        WIDTH: viewField && viewField.width ? viewField.width : '',
                        IS_DISPLAY: fieldIndex >= 0
                    }
                });
            }
        });
        if (this.userPreferenceFreezeCount === undefined) {
            userPreference.FREEZE_COLUMN_COUNT = MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT
        }
        userPreference.FREEZE_COLUMN_COUNT = this.userPreferenceFreezeCount
        return JSON.stringify(userPreference);
    }

    editDeliverable(deliverableConfig) {
        const dialogRef = this.dialog.open(DeliverableCreationModalComponent, {
            width: '38%',
            disableClose: true,
            data: {
                projectId: deliverableConfig.projectId,
                isTemplate: deliverableConfig.isTemplate,
                taskId: deliverableConfig.taskId,
                deliverableId: deliverableConfig.deliverableId,
                selectedProject: deliverableConfig.selectedProject,
                selectedTask: deliverableConfig.selectedTask,
                modalLabel: 'Edit Deliverable'
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                //this.taskService.setDeliverableRefreshData(200);
            }
        });
    }

    updatePagination() {
        this.skip = 0;
        this.page = 1;
        this.pageNumber = this.page;
        this.pageUpdated = true;
        this.routeComponentView();
        this.setCurrentView();
    }

    ngOnInit(): void {
        this.taskMenuConfig = {
            IS_PROJECT_TYPE: true,
            IS_CAMPAIGN_TYPE: false,
            IS_LIST_VIEW: true,
            listDependentFilters: [],
            groupByFilters: [],
            taskListFilters: [],
            selectedViewByName: '',
            totalTaskCount: 0,
            taskData: [],
            searchCondition: [],
            taskSortOptions: [],
            taskSearchableFields: [],
            taskViewConfig: null,
            taskViewName: null,
            campaignData: [],
            campaignViewName: null /* 'PM_CAMPAIGN_VIEW' */,
            freezeCount: 0
        };

        this.enableIndexerFieldRestriction = config.getFieldRestriction();
        this.maximumAllowedFields = config.getMaximumFieldsAllowed();
        this.initializeTaskFilters();
        this.pageSelected = false;

        if (this.router.url.includes(RoutingConstants.campaignViewBaseRoute)) {
            this.isCampaign = true;
        }
        if (this.router.url.includes('Campaigns')) {
            this.isCampaignDashboard = true;
        }
        this.routeService.handleViewRouting('deliverableWorkView');
        //this.loaderService.show();
        this.componentRendering = true;
        this.appConfig = this.sharingService.getAppConfig();
        this.enableCampaign = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CAMPAIGN]);
        this.groupByFilters = this.enableCampaign ? GROUP_BY_ALL_FILTERS : GROUP_BY_FILTERS;

        this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_PAGE_LIMIT] = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION];

        this.statusService.getAllCRStatusReasons().subscribe(response => {
            this.projectStatus.MPM_Status_Reason = response;
        });
        const currentUser = this.sharingService.getCurrentUserObject();
        const userAppRoles = this.sharingService.getUserRoles();
        this.reviewerRole = this.sharingService.getReviewerRole();
        this.approverRole = this.sharingService.getApproverRole();
        this.memberRole = this.sharingService.getMemberRole();
        if (currentUser && currentUser.ManagerFor && currentUser.ManagerFor.Target) {
            // let count = 0;
            currentUser.ManagerFor.Target.forEach(userRoles => {
                this.reviewerRole.forEach(data => {
                    if (userRoles.Id === data.Id) {
                        this.isReviewer = true;
                        this.sharingService.setIsReviewer(this.isReviewer);
                    }
                });
                if (this.approverRole) {
                    this.approverRole.forEach(data => {
                        if (userRoles.Id === data.Id) {
                            this.isApprover = true;
                            this.sharingService.setIsApprover(this.isApprover);
                        }
                    });
                } if (this.memberRole) {
                    this.memberRole.forEach(data => {
                        if (userRoles.Id === data.Id) {
                            this.isMember = true;
                            this.sharingService.setIsMember(this.isMember);
                        }
                    });
                }

            });
        }
        if (this.sharingService.getIsReviewer()) {
            if (this.sharingService.getIsApprover()) {
                this.roles.push(MPM_ROLES.APPROVER);
            }
            if (this.sharingService.getIsMember()) {
                this.roles.push(MPM_ROLES.MEMBER);
            }
            if (this.sharingService.getIsManager()) {
                this.roles.push(MPM_ROLES.MANAGER);
            }
        }

        const taskListFilters = getTaskFilters(this.roles);
        this.deliverableTaskFilters = getAccessibleTaskFilters(taskListFilters, this.sharingService.getUserRoles());
        //this.otmmMPMDataTypes = (this.isApprover || this.isMember) && this.selectedListFilter.value !== TaskFilterTypes.MY_NA_REVIEW_TASKS ?  OTMMMPMDataTypes.DELIVERABLE : this.isReviewer ? OTMMMPMDataTypes.DELIVERABLE_REVIEW : ' ';
        this.allListDependentFilters = (this.sharingService.getIsApprover() || this.sharingService.getIsMember()) ? getTaskListDependentFilters() : this.sharingService.getIsReviewer() ? getTaskListDependentReviewFilters() : '';
        let initialLoad = true;

        this.top = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.pageSize = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);

        /*this.activatedRoute.queryParams.subscribe(params => {
            if (params.pageSize || params.pageNumber) {
                this.hasPagination = this.isRefresh;
            }
        });*/

        /* this.subscriptions.push(
            this.filterHelperService.loadStatusesAndTypes().subscribe(loadedFlag => {
                if (this.level && this.isDataRefresh && !(this.advSearchData || this.savedSearchName)) {
                    this.getLevelDetails();
                }
            })
        ); */
        this.subscriptions.push(
            this.searchDataService.searchData.subscribe(data => {
                this.searchConditions = (data && Array.isArray(data) ? this.otmmService.addRelationalOperator(data) : []);
                if ((this.searchName || this.savedSearchName || this.advSearchData || this.facetData) && this.isDataRefresh) {
                    if (data && data.length > 0) {
                        if (!this.facetData || ((this.facetData && this.facetRestrictionList) && (this.advSearchData || this.savedSearchName))) {
                            this.getLevelDetails();
                        } else {
                            this.validateFacet = true;
                        }
                    }
                } else if (this.searchName === null && this.savedSearchName === null && this.advSearchData === null && this.facetData === null && this.isDataRefresh) {
                    this.getLevelDetails();
                } else if (!this.isDataRefresh) {
                    if (!initialLoad) {
                        this.isReset = true;
                        //code hardening search fired 3 times to overcome it
                        initialLoad = true;
                        this.skipPageSetting = ((data && data.length > 0) || !this.searchName) ? false : true;
                        this.getLevelDetails();
                    }
                }
                initialLoad = false;
            })
        );
        initialLoad = true;
        this.subscriptions.push(
            this.facetChangeEventService.onFacetCondidtionChange.pipe(
                takeUntil(this.unsubscribe$)
            ).subscribe(data => {
                if (JSON.stringify(data) !== '{}') {
                    this.facetRestrictionList = data;
                    if (this.facetData && this.isDataRefresh) {
                        if (data && data.facet_condition && data.facet_condition.length > 0) {
                            if (!(this.advSearchData || this.savedSearchName) || this.validateFacet) {
                                this.getLevelDetails();
                                this.validateFacet = false;
                            }
                        }
                    } else if (this.facetData === null && this.isDataRefresh) {
                        this.getLevelDetails();
                    } else if (!this.isDataRefresh) {
                        if (!initialLoad) {
                            this.previousRequest = null;
                            this.isReset = true;
                            this.getLevelDetails();
                        }
                    }
                    initialLoad = false;
                }
            })
        );

        this.subscriptions.push(
            this.componentParams.subscribe(routeData => {
                if (this.isPageRefresh) {
                    this.emailLinkId = '';
                    this.emailSearchCondition = [];
                }

                const menu = this.sharingService.getCurrentMenu();
                this.defaultUserPreferenceView = this.sharingService.getDefaultCurrentUserPreferenceByMenu(menu['MPM_Menu-id'].Id);

                if (this.defaultUserPreferenceView) {
                    this.defaultViewFilterName = this.defaultUserPreferenceView.FILTER_NAME;
                    if (this.defaultUserPreferenceView && this.defaultUserPreferenceView.FIELD_DATA) {
                        const fieldData = this.defaultUserPreferenceView.FIELD_DATA;
                        if (!this.utilService.isNullOrEmpty(fieldData) && typeof fieldData === 'string') {
                            const parsedData = JSON.parse(this.defaultUserPreferenceView.FIELD_DATA);
                            this.defaultGroupViewFilterName = parsedData.groupBy;
                            this.defaultListDependentFilterName = parsedData.taskStatusFilter;
                            this.initializeTaskFilters();
                            this.getLevelDetails();
                        }
                    }
                } else {
                    if (!this.selectedGroupByFilter) {
                        let filtername;
                        taskListFilters.forEach(taskListFilter => {
                            if (taskListFilter.default) {
                                filtername = taskListFilter.name;
                            }
                        });
                        filtername = filtername ? filtername : taskListFilters[0].name;
                        const userPref = this.sharingService.getCurrentUserPreferenceByList(menu['MPM_Menu-id'].Id, filtername);
                        if (userPref && !this.utilService.isNullOrEmpty(userPref.FIELD_DATA)) {
                            const fieldData = JSON.parse(userPref.FIELD_DATA);
                            this.setLevel(fieldData.groupBy);
                            this.groupByFilters.forEach(groupByFilter => {
                                if (groupByFilter.value === fieldData.groupBy) {
                                    this.selectedGroupByFilter = groupByFilter;
                                }
                            });
                            this.selectedListFilter = this.deliverableTaskFilters.find(deliverableTaskFilter => deliverableTaskFilter.name === userPref.FILTER_NAME);
                            this.listDependentFilters = this.allListDependentFilters[this.selectedListFilter.value];
                            this.listDependentFilters.forEach(listDependentFilter => {
                                if (listDependentFilter.value === fieldData.taskStatusFilter) {
                                    this.selectedListDependentFilter = listDependentFilter;
                                }
                            });
                            this.getLevelDetails();
                        }
                    }
                }

                if (routeData.emailLinkId) {
                    this.emailLinkId = routeData.emailLinkId;
                    this.initalizeComponent(routeData);
                } else {
                    this.initalizeComponent(routeData);
                }

            })
        );
        this.reload(false, 'refresh');
    }

    ngOnDestroy(): void {
        this.searchChangeEventService.update(null);
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

}
