import { MPM_LEVEL, Status } from 'mpm-library';

export type StatusCache = { [key in MPM_LEVEL] ?: Array<Status> };
