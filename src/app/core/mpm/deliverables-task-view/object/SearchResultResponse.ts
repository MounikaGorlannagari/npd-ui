import { Facet } from 'mpm-library';

export interface SearchResultResponse {
    search_result_resource: {
        search_result: {
            total_hit_count: number;
            facet_field_response_list: Array<Facet>
        };
        asset_list: Array<any>;
    };
}
