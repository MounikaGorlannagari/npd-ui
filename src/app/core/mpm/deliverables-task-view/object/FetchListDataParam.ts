import { FacetConditionList } from 'mpm-library';

export interface FetchListDataParam {
    skip?: number;
    top?: number;
    searchConditionList?: Array<any>;
    sortString?: string;
    search_config_id?: number;
    facetRestrictionList?: FacetConditionList;
}
