import { CustomSelect } from 'mpm-library';
import { BulkEditFilterTypes } from './BulkEditFilterTypes';

export let BULK_EDIT_FILTERS: Array<CustomSelect> = [
    {
        value: BulkEditFilterTypes.APPROVED,
        name: 'Approved',
        default: true,
    },
    {
        value: BulkEditFilterTypes.REJECTED,
        name: 'Rejected',
        default: false,
    }
];