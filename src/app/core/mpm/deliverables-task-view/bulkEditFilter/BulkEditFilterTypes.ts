export enum BulkEditFilterTypes {
    APPROVED = 'APPROVED',
    REJECTED = 'REJECTED'
}
