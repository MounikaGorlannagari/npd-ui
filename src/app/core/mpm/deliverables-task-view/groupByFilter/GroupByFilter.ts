import { CustomSelect } from 'mpm-library';
import { GroupByFilterTypes } from './GroupByFilterTypes';
import { OTMMMPMDataTypes } from 'mpm-library';

export let GROUP_BY_DELIVERABLE_FILTER = {
    value: GroupByFilterTypes.GROUP_BY_TASK,
    name: 'Tasks',
    default: false,
    searchCondition: [{
        type: 'com.artesia.search.SearchScalarCondition',
        metadata_field_id: 'MPM.UTILS.DATA_TYPE',
        relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
        relational_operator_name: 'is',
        value: OTMMMPMDataTypes.DELIVERABLE,
        relational_operator: 'and'
    }]
};

export let GROUP_BY_FILTERS: Array<CustomSelect> = [
   {
        value: GroupByFilterTypes.GROUP_BY_PROJECT,
        name: 'Projects',
        default: true,
        searchCondition: [{
            type: 'com.artesia.search.SearchScalarCondition',
            metadata_field_id: 'MPM.UTILS.DATA_TYPE',
            relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
            relational_operator_name: 'is',
            value: OTMMMPMDataTypes.PROJECT,
            relational_operator: 'and'
        }]
    }, 
    {
        value: GroupByFilterTypes.GROUP_BY_TASK,
        name: 'Tasks',
        default: false,
        searchCondition: [{
            type: 'com.artesia.search.SearchScalarCondition',
            metadata_field_id: 'MPM.UTILS.DATA_TYPE',
            relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
            relational_operator_name: 'is',
            value: OTMMMPMDataTypes.TASK,
            relational_operator: 'and'
        }]
    }
];

export let GROUP_BY_ALL_FILTERS: Array<CustomSelect> = [
 /*    {
        value: GroupByFilterTypes.GROUP_BY_CAMPAIGN,
        name: 'Campaigns',
        default: true,
        searchCondition: [{
            type: 'com.artesia.search.SearchScalarCondition',
            metadata_field_id: 'MPM.UTILS.DATA_TYPE',
            relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
            relational_operator_name: 'is',
            value: OTMMMPMDataTypes.CAMPAIGN,
            relational_operator: 'and'
        }]
    }, 
    */
    {
        value: GroupByFilterTypes.GROUP_BY_TASK,
        name: 'Tasks',
        default: false,
        searchCondition: [{
            type: 'com.artesia.search.SearchScalarCondition',
            metadata_field_id: 'MPM.UTILS.DATA_TYPE',
            relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
            relational_operator_name: 'is',
            value: OTMMMPMDataTypes.TASK,
            relational_operator: 'and'
        }]
    },
    {
        value: GroupByFilterTypes.GROUP_BY_PROJECT,
        name: 'Projects',
        default: false,
        searchCondition: [{
            type: 'com.artesia.search.SearchScalarCondition',
            metadata_field_id: 'MPM.UTILS.DATA_TYPE',
            relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
            relational_operator_name: 'is',
            value: OTMMMPMDataTypes.PROJECT,
            relational_operator: 'and'
        }]
    },
];
