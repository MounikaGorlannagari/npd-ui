import { GroupByFilterTypes } from './GroupByFilterTypes';

export type GroupByFilterType = GroupByFilterTypes.GROUP_BY_PROJECT | GroupByFilterTypes.GROUP_BY_TASK;

