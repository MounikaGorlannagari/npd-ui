import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliverableTasksViewComponent } from './deliverables-task-view.component';

const routes: Routes = [
    {
        path: '',
        component: DeliverableTasksViewComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class DeliverablesTaskViewRoutingModule { }
