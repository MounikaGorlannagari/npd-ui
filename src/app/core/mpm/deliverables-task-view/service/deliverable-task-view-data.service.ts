import { Injectable } from '@angular/core';
import {
  SharingService, otmmServicesConstants, OTMMFolderFilterTypes,
  OTMMFolderFilterType, CustomSelect, ListFilter, MPM_LEVEL,
  SearchResponse, IndexerService, SearchRequest, OTMMMPMDataTypes
} from 'mpm-library';
import { Observable, forkJoin } from 'rxjs';
import { OTMMService } from 'mpm-library';
import { FetchListDataParam } from '../object/FetchListDataParam';
import { SearchResultResponse } from '../object/SearchResultResponse';
import { FilterHelperService } from './filter-helper.service';
import { FacetFieldCondition } from 'mpm-library';

@Injectable({
  providedIn: 'root'
})
export class DeliverableTaskViewDataService {

  constructor(
    private otmmService: OTMMService,
    private sharingService: SharingService,
    private filterHelperService: FilterHelperService,
    private indexerService: IndexerService
  ) { }

  folderFilter = this.sharingService.getSelectedCategory()?.PROJECT_FOLDER_ID;
  otmmProjectsSearchConfig = {
    load_type: 'metadata',
    level_of_detail: 'slim',
    child_count_load_type: 'folders',
    facet_restriction_list: null,
    search_condition_list: {
      search_condition_list: {
        search_condition: []
      }
    },
    metadata_to_return: '',
    folder_filter_type: OTMMFolderFilterTypes.DIRECT,
    folder_filter: this.folderFilter,
    sortString: '',
    search_config_id: null
  };

  prefetchListFilterCount(selectedListFilter: ListFilter, allListFilters: Array<ListFilter>,
    allListDependentFilters: Array<CustomSelect>, emailCondition, level: MPM_LEVEL, facetConditions: FacetFieldCondition[]): Observable<Array<ListFilter>> {
    return new Observable(observer => {
      if (!selectedListFilter || !allListFilters || !(allListFilters?.length > 0)) {
        observer.next([]);
        observer.complete();
      }
      const filersResponseOrderValues = [];
      const promises: Array<Observable<SearchResponse>> = [];
      emailCondition = emailCondition && Array.isArray(emailCondition) ? emailCondition : [];

      allListFilters.forEach(filter => {
        /*     if (filter.value !== selectedListFilter.value) { */
        // code hardening
        if (filter.value === selectedListFilter.value) {
          const conditionObject = this.filterHelperService.getSearchConditionNew(filter, allListDependentFilters[filter.value][0], level);

          const parameters: SearchRequest = {
            search_config_id: null,
            keyword: '',
            grouping: {
              group_by: level,
              group_from_type: OTMMMPMDataTypes.DELIVERABLE,
              search_condition_list: {
                search_condition: [...conditionObject.DELIVERABLE_CONDTION, ...emailCondition]
              }
            },
            search_condition_list: {
              search_condition: conditionObject.TASK_CONDITION
            },
            facet_condition_list: {
              facet_condition: facetConditions ? facetConditions : []
            },
            sorting_list: {
              sort: []
            },
            cursor: {
              page_index: 0,
              page_size: 1
            }
          };
          promises.push(this.indexerService.search(parameters));
        }
      });
      forkJoin(promises).subscribe(responses => {
        const keyCounMapping = {};
        if (responses.length === (allListFilters.length - 1)) {
          for (let i = 0; i < responses.length; i++) {
            keyCounMapping[filersResponseOrderValues[i]] = responses[i].cursor.total_records;
          }
        }
        allListFilters.forEach(filter => {
          if (filter && filter.value && keyCounMapping[filter.value] >= 0) {
            filter.count = keyCounMapping[filter.value];
          }
        });
        observer.next(allListFilters);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
  }

}
