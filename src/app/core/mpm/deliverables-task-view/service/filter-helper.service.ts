import { Injectable } from '@angular/core';
import { ListFilter, CustomSelect, OTMMService, OTMMMPMDataTypes, MPMFieldConstants, MPM_LEVEL, MPMFieldKeys, MPMSearchOperators, MPM_ROLES, Team, Roles, AppService, DeliverableTypes, StatusLevels } from 'mpm-library';
import { OTMMSearchCondition, SharingService, MPM_LEVELS, Status, StatusType, StatusTypes, SearchRequest, IndexerDataTypes, FieldConfigService } from 'mpm-library';
import { TaskFilterTypes } from '../taskListfilters/TaskFilterTypes';
import { DataCacheService } from './data-cache.service';
import { Observable } from 'rxjs';
import { TaskListDependentFilterTypes } from '../taskListfilters/TaskListDependentFilterTypes';
import { TaskSearchConditions } from '../object/DeliverableTasksViewComponentParams';
import { StatusService, ProjectConstant, MPMSearchOperatorNames } from 'mpm-library';

@Injectable({
  providedIn: 'root'
})
export class FilterHelperService {

  TEAM_ROLE_MAPPING_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Team_Role_Mapping/operations';
  GET_TEAM_ROLEs_FOR_TEAM_WS = 'GetTeamRolesForTeam';

  constructor(
    private sharingservice: SharingService,
    private dataCacheService: DataCacheService,
    private fieldConfigService: FieldConfigService,
    private appService: AppService,
    private statusService: StatusService
  ) { }

  MULTI_VALUE_DELIMITER = '-';

  searchConditions: Array<OTMMSearchCondition> = [];

  deliverableStatuses: Array<Status> = [];
  projectStatuses: Array<Status> = [];

  teams;
  isReviewer;
  reviewerRole;
  isApprover;
  approverRole;
  isMember;
  memberRole;

  loadStatus() {
    const projectStatus: any = this.statusService.getAllStatusBycategoryName(MPM_LEVELS.PROJECT);
    this.projectStatuses = projectStatus && projectStatus.length > 0 && Array.isArray(projectStatus) ? projectStatus[0] : projectStatus;

    const deliverableStatus: any = this.statusService.getAllStatusBycategoryName(MPM_LEVELS.DELIVERABLE);
    this.deliverableStatuses = deliverableStatus && deliverableStatus.length > 0 && Array.isArray(deliverableStatus) ? deliverableStatus[0] : deliverableStatus;

  }

  getStatusIdByStatusType(statuses: Array<Status>, statusType: any): Array<string> {
    if (!statuses || !statusType) {
      return null;
    }
    let currStatusIds: Array<string> = [];
    currStatusIds = statuses.filter(status => {
      return status['STATUS_TYPE'] === statusType ? true : (statusType === StatusTypes.INITIAL && status['STATUS_TYPE'] === StatusTypes.INTERMEDIATE) ? true : false;
    }).map(data => {
      return data['MPM_Status-id'].Id;
    });
    return currStatusIds && currStatusIds.length > 0 ? currStatusIds : null;
  }

  getStatusIdByStatusTypes(statuses: Array<Status>, statusType: any): Array<string> {
    if (!statuses || !statusType) {
      return null;
    }
    let currStatusIds: Array<string> = [];
    currStatusIds = statuses.filter(status => {
      return status['STATUS_TYPE'] === statusType;
    }).map(data => {
      return data['MPM_Status-id'].Id;
    });
    return currStatusIds && currStatusIds.length > 0 ? currStatusIds : null;
  }

  loadStatusesAndTypes(): Observable<boolean> {
    return new Observable(observer => {
      this.dataCacheService.getStatusbyLevel(MPM_LEVELS.PROJECT).subscribe(projectStatusResponse => {
        this.dataCacheService.getStatusbyLevel(MPM_LEVELS.DELIVERABLE).subscribe(deliverableStatusResponse => {
          this.deliverableStatuses = deliverableStatusResponse;
          this.projectStatuses = projectStatusResponse;
          observer.next(true);
          observer.complete();
        });
      });
    });
  }

  getSortString(sortPropName: string, sortDirection: string): string {
    if (!sortPropName || !sortDirection) {
      return '';
    }
    return sortDirection + '_' + sortPropName;
  }

  getArchievedFilter(statusTypeId: StatusType): OTMMSearchCondition {
    const projectStatusId = this.getStatusIdByStatusType(this.projectStatuses, statusTypeId);
    if (projectStatusId) {
      return {
        type: 'com.artesia.search.SearchScalarCondition',
        metadata_field_id: 'MPM.PROJECT.STATUS_ID',
        relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
        relational_operator_name: 'is',
        value: projectStatusId,
        relational_operator: 'and'
      };
    } else {
      console.warn('No Completed Status found for Project status');
      return;
    }
  }

  getNotArchievedFilter(statusTypeId: StatusType): OTMMSearchCondition {
    const projectStatusId = this.getStatusIdByStatusType(this.projectStatuses, statusTypeId);
    if (projectStatusId) {
      return {
        type: 'com.artesia.search.SearchScalarCondition',
        metadata_field_id: 'MPM.PROJECT.STATUS_ID',
        relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS NOT',
        relational_operator_name: 'is not',
        value: projectStatusId,
        relational_operator: 'and'
      };
    } else {
      console.warn('No Completed Status found for Project status');
      return;
    }
  }

  getMyTaskFilterConditions(listDependentFilter: CustomSelect): Array<OTMMSearchCondition> {
    const conditions: Array<OTMMSearchCondition> = [{
      type: 'com.artesia.search.SearchTabularCondition',
      metadata_table_id: 'MPM.TABLE.DELIVERABLE.DETAILS',
      tabular_field_list: [{
        type: 'com.artesia.search.SearchTabularFieldCondition',
        metadata_field_id: 'MPM.FIELD.DELIVERABLE.OWNER.STATUS',
        relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
        relational_operator_name: 'is',
        value: this.sharingservice.getcurrentUserItemID() + this.MULTI_VALUE_DELIMITER
          + this.getStatusIdByStatusType(this.deliverableStatuses, listDependentFilter.searchCondition)
      }]
    }];
    return conditions;
  }

  getMyTeamTaskFilterConditions(listDependentFilter: CustomSelect): Array<OTMMSearchCondition> {
    const conditions = [];
    let consdition: OTMMSearchCondition = {};
    if (listDependentFilter.searchCondition) {
      consdition = {
        type: 'com.artesia.search.SearchTabularCondition',
        metadata_table_id: 'MPM.TABLE.DELIVERABLE.DETAILS',
        tabular_field_list: [{
          type: 'com.artesia.search.SearchTabularFieldCondition',
          // metadata_field_id: 'MPM.FIELD.DELIVERABLE.OWNER.DELIVERABLE.TYPE',
          metadata_field_id: 'MPM.FIELD.DELIVERABLE.TYPE',
          relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
          relational_operator_name: 'is',
          // value: this.sharingservice.getcurrentUserItemID() + this.MULTI_VALUE_DELIMITER + listDependentFilter.searchCondition
          value: listDependentFilter.searchCondition
        }]
      };
    } else {
      consdition = {
        type: 'com.artesia.search.SearchTabularCondition',
        metadata_table_id: 'MPM.TABLE.DELIVERABLE.DETAILS',
        tabular_field_list: [{
          type: 'com.artesia.search.SearchTabularFieldCondition',
          // metadata_field_id: 'MPM.FIELD.DELIVERABLE.OWNER',
          metadata_field_id: 'MPM.FIELD.DELIVERABLE.OWNER.STATUS',
          relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS NOT',
          relational_operator_name: 'is not',
          value: this.sharingservice.getcurrentUserItemID()
        }]
      };
    }
    conditions.push(consdition);
    return conditions;
  }

  getOwnerTypeStatusFilterConditions(listFilter: ListFilter, listDependentFilter: CustomSelect): Array<OTMMSearchCondition> {
    const conditions: Array<OTMMSearchCondition> = [{
      type: 'com.artesia.search.SearchTabularCondition',
      metadata_table_id: 'MPM.TABLE.DELIVERABLE.DETAILS',
      tabular_field_list: [{
        type: 'com.artesia.search.SearchTabularFieldCondition',
        metadata_field_id: 'MPM.FIELD.DELIVERABLE.OWNER.STATUS',
        relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
        relational_operator_name: 'is',
        value: this.sharingservice.getcurrentUserItemID() + this.MULTI_VALUE_DELIMITER
          + this.getStatusIdByStatusType(this.deliverableStatuses, listDependentFilter.searchCondition)
      }]
    }, {
      type: 'com.artesia.search.SearchTabularCondition',
      metadata_table_id: 'MPM.TABLE.DELIVERABLE.DETAILS',
      tabular_field_list: [{
        type: 'com.artesia.search.SearchTabularFieldCondition',
        metadata_field_id: 'MPM.FIELD.DELIVERABLE.OWNER.DELIVERABLE.TYPE',
        relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
        relational_operator_name: 'is',
        value: this.sharingservice.getcurrentUserItemID() + this.MULTI_VALUE_DELIMITER + listFilter.searchCondition
      }],
      relational_operator: 'and'
    }, {
      type: 'com.artesia.search.SearchTabularCondition',
      metadata_table_id: 'MPM.TABLE.DELIVERABLE.DETAILS',
      tabular_field_list: [{
        type: 'com.artesia.search.SearchTabularFieldCondition',
        metadata_field_id: 'MPM.FIELD.DELIVERABLE.TYPE.STATUS',
        relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
        relational_operator_name: 'is',
        value: listFilter.searchCondition + this.MULTI_VALUE_DELIMITER +
          this.getStatusIdByStatusType(this.deliverableStatuses, listDependentFilter.searchCondition)
      }],
      relational_operator: 'and'
    }];
    return conditions;
  }

  getSearchCondidtion(listFilter: ListFilter, listDependentFilter: CustomSelect, groupByFilter: CustomSelect): Array<OTMMSearchCondition> {
    this.searchConditions = [];
    if (!listFilter || !groupByFilter) {
      console.warn('List filter and group by filter and provided to getSearchCondidtion');
      return this.searchConditions;
    }
    const groupByFilterSearchCondition: Array<any> = groupByFilter?.searchCondition;
    if (listFilter.value === TaskFilterTypes.MY_TASKS) {
      this.searchConditions = this.getMyTaskFilterConditions(listDependentFilter);
    } else if (listFilter.value === TaskFilterTypes.MY_TEAM_TASKS) {
      this.searchConditions = this.getMyTeamTaskFilterConditions(listDependentFilter);
    } else {
      this.searchConditions = this.getOwnerTypeStatusFilterConditions(listFilter, listDependentFilter);
    }
    this.searchConditions.push({
      type: 'com.artesia.search.SearchTabularCondition',
      metadata_table_id: 'MPM.TABLE.DELIVERABLE.DETAILS',
      tabular_field_list: [{
        type: 'com.artesia.search.SearchTabularFieldCondition',
        metadata_field_id: 'MPM.FIELD.DELIVERABLE.ITEM_ID',
        relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS NOT EMPTY',
        relational_operator_name: 'is not empty'
      }],
      relational_operator: 'and'
    });
    if (listDependentFilter.value === TaskListDependentFilterTypes.ARCHIEVED && this.getArchievedFilter(listDependentFilter.searchCondition)) {
      this.searchConditions.push(this.getArchievedFilter(listDependentFilter.searchCondition));
    } else {
      this.searchConditions.push(this.getNotArchievedFilter(StatusTypes.FINAL_COMPLETED));
    }
    if (Array.isArray(groupByFilterSearchCondition) && groupByFilterSearchCondition.length > 0) {
      this.searchConditions.push(...groupByFilterSearchCondition);
    }
    return this.searchConditions;
  }

  addDefaultConditionsToList(level: MPM_LEVEL, searchConditions: TaskSearchConditions): TaskSearchConditions {
    let condition = null;
    condition = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS,
      MPMSearchOperatorNames.IS, undefined, (level === MPM_LEVELS.CAMPAIGN ? OTMMMPMDataTypes.CAMPAIGN : level === MPM_LEVELS.PROJECT ? OTMMMPMDataTypes.PROJECT : OTMMMPMDataTypes.TASK));
    condition ? searchConditions.TASK_CONDITION.push(condition) : console.log('');
    if (level === MPM_LEVELS.PROJECT) {
      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE,
        MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, MPM_LEVELS.PROJECT);
      condition ? searchConditions.TASK_CONDITION.push(condition) : console.log('');
    }
    condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_ITEM_ID, MPMSearchOperators.IS_NOT_EMPTY,
      MPMSearchOperatorNames.IS_NOT_EMPTY, undefined, undefined);
    condition ? searchConditions.DELIVERABLE_CONDTION.push(condition) : console.log('');
    /* condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_IS_ACTIVE, MPMSearchOperators.IS,
      MPMSearchOperatorNames.IS, MPMSearchOperators.AND, 'true'); */
    // condition ? searchConditions.DELIVERABLE_CONDTION.push(condition) : console.log('');
    // condition = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, OTMMMPMDataTypes.DELIVERABLE);
    // condition ? searchConditions.DELIVERABLE_CONDTION.push(condition) : console.log('');

    return searchConditions;
  }

  handleArchivedCondition(searchConditions: TaskSearchConditions, isArchivedFilter: boolean, operatorId: string, operatorName: string): TaskSearchConditions {
    let currStatusIds: Array<string> = [];
    if (isArchivedFilter) {
      currStatusIds = this.projectStatuses.filter(status => {
        return status['STATUS_TYPE'] === StatusTypes.INITIAL || status['STATUS_TYPE'] === StatusTypes.INTERMEDIATE ||
          status['STATUS_TYPE'] === StatusTypes.FINAL_CANCELLED || status['STATUS_TYPE'] === StatusTypes.FINAL_APPROVED;
      }).map(data => {
        return data['MPM_Status-id'].Id;
      });
    } else {
      currStatusIds = this.projectStatuses.filter(status => {
        return status['STATUS_TYPE'] === StatusTypes.FINAL_COMPLETED || status['STATUS_TYPE'] === StatusTypes.FINAL_CANCELLED || status['STATUS_TYPE'] === StatusTypes.FINAL_APPROVED || (status['STATUS_TYPE'] === StatusTypes.INTERMEDIATE && status['TYPE'] === StatusTypes.ONHOLD);
      }).map(data => {
        return data['MPM_Status-id'].Id;
      });
    }
    let condition;
    currStatusIds.forEach((element, index) => {
      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS_ID, operatorId, operatorName, index === 0 ? 'and' : 'or', element);
      searchConditions.TASK_CONDITION.push(condition);
    });
    return searchConditions;
  }

  // MPM_V3-2057
  handleNonArchivedCondition(searchConditions: TaskSearchConditions, isArchivedFilter: boolean, operatorId: string, operatorName: string): TaskSearchConditions {
    let currStatusIds: Array<string> = [];
    if (isArchivedFilter) {
      currStatusIds = this.projectStatuses.filter(status => {
        return status['STATUS_TYPE'] === StatusTypes.INITIAL || status['STATUS_TYPE'] === StatusTypes.INTERMEDIATE ||
          status['STATUS_TYPE'] === StatusTypes.FINAL_CANCELLED || status['STATUS_TYPE'] === StatusTypes.FINAL_APPROVED;
      }).map(data => {
        return data['MPM_Status-id'].Id;
      });
    } else {
      currStatusIds = this.projectStatuses.filter(status => {
        // tslint:disable-next-line: max-line-length
        /*  return status['STATUS_TYPE'] === StatusTypes.INITIAL || status['STATUS_TYPE'] === StatusTypes.INTERMEDIATE; */
        return status['STATUS_LEVEL'] === StatusLevels.FINAL || (status['STATUS_TYPE'] === StatusTypes.INTERMEDIATE && status['TYPE'] === StatusTypes.ONHOLD);
      }).map(data => {
        return data['MPM_Status-id'].Id;
      });
    }
    let condition;
    currStatusIds.forEach((element, index) => {
      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS_ID, operatorId, operatorName, index === 0 ? 'and' : 'or', element);
      searchConditions.TASK_CONDITION.push(condition);
    });
    return searchConditions;
  }

  handleStatusCheckAndConditionPush(searchConditions: TaskSearchConditions, mapperName: string, searchCondition: string,
    isStatus: boolean, isTask: boolean, operatorId: string, operatorName: string): TaskSearchConditions {
    if (mapperName && searchConditions) {
      let currValue;
      let condition = null;
      if (mapperName === 'DELIVERABLE_STATUS_TYPE') {
        currValue = searchCondition;
        condition = this.fieldConfigService.createConditionObject(mapperName, operatorId, operatorName, 'or', currValue);
        condition ? (isTask ? searchConditions.TASK_CONDITION.push(condition) : searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');

      } else if (mapperName === 'PROJECT_TYPE') {
        currValue = searchCondition;
        condition = this.fieldConfigService.createConditionObject(mapperName, operatorId, operatorName, 'and', currValue);
        condition ? (isTask ? searchConditions.TASK_CONDITION.push(condition) : searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');
      } else {
        currValue = isStatus ? this.getStatusIdByStatusType(isTask ? this.projectStatuses : this.deliverableStatuses, searchCondition) : searchCondition;

        if (isStatus && !currValue) {
          console.warn('Status not found for the search condition: ', searchCondition, '\nMapper name: ', mapperName);
          return searchConditions;
        }
        if (isStatus && currValue && Array.isArray(currValue) && currValue.length > 1) {
          currValue.forEach((element, index) => {
            condition = this.fieldConfigService.createConditionObject(mapperName, operatorId, operatorName, index === 0 ? 'and' : 'or', element);
            condition ? (isTask ? searchConditions.TASK_CONDITION.push(condition) : searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');
          });
        } else {
          condition = this.fieldConfigService.createConditionObject(mapperName, operatorId, operatorName, 'and', isStatus ? currValue[0] : currValue);
          condition ? (isTask ? searchConditions.TASK_CONDITION.push(condition) : searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');
        }
      }
    }
    return searchConditions;
  }

  addFilterForUserTeam(searchConditions: TaskSearchConditions): TaskSearchConditions {
    let userRoleId;
    const currentUserRoleObject = this.sharingservice.getCurrentUserObject();
    if (currentUserRoleObject && currentUserRoleObject.ManagerFor && currentUserRoleObject.ManagerFor.Target) {
      let count = 0;
      const allTeams = this.sharingservice.getAllTeams();
      const allTeamRoles = this.sharingservice.getAllTeamRoles();
      currentUserRoleObject.ManagerFor.Target.forEach(userRoles => {
       
        const userAppRoles = this.sharingservice.getUserRoles().find(userAppRole => userAppRole.ROLE_DN === userRoles.Id);
        if (!(userAppRoles && userAppRoles.ROLE_DN)) {
        
          const teams = allTeams.find(role => { return role.ROLE_DN === userRoles.Id });
          const teamRoles = allTeamRoles.find(role => { return role.ROLE_DN === userRoles.Id });
          const Id = teams && teams['MPM_Teams-id'] && teams['MPM_Teams-id'].Id ? teams['MPM_Teams-id'].Id : teamRoles && teamRoles['MPM_Team_Role_Mapping-id'] && teamRoles['MPM_Team_Role_Mapping-id'].Id ? teamRoles['MPM_Team_Role_Mapping-id'].Id : '';
          /* let condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_OWNER_ROLE_NAME, MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS, MPMSearchOperators.OR, userRoles.Name); */
          if (Id) {
            let condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_OWNER_ROLE_ID, MPMSearchOperators.IS,
              MPMSearchOperatorNames.IS, MPMSearchOperators.OR, Id);
            if (count === 0) {
              /*   condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_OWNER_ROLE_NAME, MPMSearchOperators.IS,
                  MPMSearchOperatorNames.IS, MPMSearchOperators.AND, userRoles.Name); */
              condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_OWNER_ROLE_ID, MPMSearchOperators.IS,
                MPMSearchOperatorNames.IS, MPMSearchOperators.AND, Id);
              count++;
            }
            searchConditions.DELIVERABLE_CONDTION.push(condition);
          }
        }
      });

      currentUserRoleObject.ManagerFor.Target.forEach(userRoles => {
        const userAppRoles = this.sharingservice.getUserRoles().find(userAppRole => {

          return userAppRole.ROLE_DN === userRoles.Id;

        });

        if (!(userAppRoles && userAppRoles.ROLE_DN)) {

          const teams = allTeams.find(role => { return role.ROLE_DN === userRoles.Id });
          const teamRoles = allTeamRoles.find(role => { return role.ROLE_DN === userRoles.Id });
          const Id = teams && teams['MPM_Teams-id'] && teams['MPM_Teams-id'].Id ? teams['MPM_Teams-id'].Id : teamRoles && teamRoles['MPM_Team_Role_Mapping-id'] && teamRoles['MPM_Team_Role_Mapping-id'].Id ? teamRoles['MPM_Team_Role_Mapping-id'].Id : '';
          if (Id) {
            /*  const condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_APPROVER_ROLE_NAME, MPMSearchOperators.IS,
             MPMSearchOperatorNames.IS, MPMSearchOperators.OR, userRoles.Name); */

            const condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_APPROVER_ROLE_ID, MPMSearchOperators.IS,
              MPMSearchOperatorNames.IS, MPMSearchOperators.OR, Id);
            searchConditions.DELIVERABLE_CONDTION.push(condition);
          }
        }
      });

    }
    return searchConditions;
  }

  getTeamRolesForTeam(roleDN): Observable<any> {
    return new Observable(observer => {
      this.appService.invokeRequest(this.TEAM_ROLE_MAPPING_METHOD_NS, this.GET_TEAM_ROLEs_FOR_TEAM_WS, roleDN).subscribe(response => {
        observer.next(response);
        observer.complete();
      }, error => {
        observer.error(error);

      });
    });
  }

  getSearchConditionNew(listFilter: ListFilter, listDependentFilter: CustomSelect, level: MPM_LEVEL, dates?): TaskSearchConditions {
    this.loadStatus();
    let searchConditions: TaskSearchConditions = {
      DELIVERABLE_CONDTION: [],
      TASK_CONDITION: []
    };
    searchConditions = this.addDefaultConditionsToList(level, searchConditions);
    if (!listFilter && listFilter !== undefined) {
      console.warn('List filter and group by filter and provided to getSearchCondidtion');
      return searchConditions;
    }
    let condition = null;
    const currentUser = this.sharingservice.getCurrentUserObject();
    const userAppRoles = this.sharingservice.getUserRoles();
    this.reviewerRole = this.sharingservice.getReviewerRole();
    this.approverRole = this.sharingservice.getApproverRole();
    this.memberRole = this.sharingservice.getMemberRole();
    if (currentUser && currentUser.ManagerFor && currentUser.ManagerFor.Target) {
      // let count = 0;
      currentUser.ManagerFor.Target.forEach(userRoles => {
        if (this.reviewerRole) {
          this.reviewerRole.forEach(data => {
            if (userRoles.Id === data.Id) {
              this.isReviewer = true;
              this.sharingservice.setIsReviewer(this.isReviewer);
            }
          });
        }
        if (this.approverRole) {
          this.approverRole.forEach(data => {
            if (userRoles.Id === data.Id) {
              this.isApprover = true;
              this.sharingservice.setIsApprover(this.isApprover);
            }
          });
        } if (this.memberRole) {
          this.memberRole.forEach(data => {
            if (userRoles.Id === data.Id) {
              this.isMember = true;
              this.sharingservice.setIsMember(this.isMember);
            }
          });
        }
      });
    }

    // MPM_V3-2057

    if (listDependentFilter === undefined && listFilter === undefined) {
      if ((this.isApprover || this.isMember)) {
        condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_ACTIVE_USER_ID,
          MPMSearchOperators.IS,
          MPMSearchOperatorNames.IS, MPMSearchOperators.AND, this.sharingservice.getCurrentUserItemID());
        condition ? searchConditions.DELIVERABLE_CONDTION.push(condition) : console.log('');
      } else if (this.isReviewer) {
        condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_REVIEW_MPM_FIELDS.DELIVERABLE_REVIEWER_ID,
          MPMSearchOperators.IS,
          MPMSearchOperatorNames.IS, MPMSearchOperators.AND, this.sharingservice.getCurrentUserItemID());
        condition ? searchConditions.DELIVERABLE_CONDTION.push(condition) : console.log('');

      }

      const currValue: any = this.getStatusIdByStatusTypes(this.deliverableStatuses, StatusTypes.INITIAL);
      // tslint:disable-next-line: max-line-length
      condition = currValue ? this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 'and', currValue[0]) : '';
      condition && currValue && currValue[0] ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');

      // To form the date query
      // tslint:disable-next-line: max-line-length
      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_START_DATE, MPMSearchOperators.IS_BETWEEN, MPMSearchOperatorNames.IS_BETWEEN, 'AND', dates);
      condition ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');

      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_OWNER_NAME, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 'AND', 'NA');
      condition ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');

      // upload Task
      // tslint:disable-next-line: max-line-length
      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TYPE, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 'AND', DeliverableTypes.UPLOAD);
      condition ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');

      // ACTION Task
      // tslint:disable-next-line: max-line-length
      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TYPE, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 'OR', DeliverableTypes.ACTION);
      condition ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');

      // Approval Task
      // tslint:disable-next-line: max-line-length
      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TYPE, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 'OR', DeliverableTypes.REVIEW);
      condition ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');

      // for project status that are not archived
      this.handleNonArchivedCondition(searchConditions, false, MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT);

      if (level === MPM_LEVELS.CAMPAIGN) {
        const searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(ProjectConstant.GET_ALL_CAMPAIGN_SEARCH_CONDITION_LIST);
        condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_TYPE,
          MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, 'CAMPAIGN');
        if (condition) {
          searchConditions.TASK_CONDITION.push(condition);
        }

        /* condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_OWNER_ID,
          MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, this.sharingservice.getCurrentUserItemID());
        if (condition) {
          searchConditions.TASK_CONDITION.push(condition);
        } */
        searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE, 'TEMPLATE', true, true,
          MPMSearchOperators.IS_NOT,
          MPMSearchOperatorNames.IS_NOT);
        /*  if (condition) {
            searchConditions.TASK_CONDITION.push(condition);
          } */
      }
      if (level === MPM_LEVELS.TASK) {
        searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE, 'TEMPLATE', true, true,
          MPMSearchOperators.IS_NOT,
          MPMSearchOperatorNames.IS_NOT);
      }

    } else {
      if ((this.isApprover || this.isMember) && listFilter.value !== TaskFilterTypes.MY_NA_REVIEW_TASKS) {
        condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_ACTIVE_USER_ID,
          (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS) ? MPMSearchOperators.IS : MPMSearchOperators.IS_NOT,
          (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS) ? MPMSearchOperatorNames.IS : MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, this.sharingservice.getCurrentUserItemID());
        condition ? searchConditions.DELIVERABLE_CONDTION.push(condition) : console.log('');
      } else if (this.isReviewer) {
        condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_REVIEW_MPM_FIELDS.DELIVERABLE_REVIEWER_ID,
          (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS) ? MPMSearchOperators.IS : MPMSearchOperators.IS_NOT,
          (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS) ? MPMSearchOperatorNames.IS : MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, this.sharingservice.getCurrentUserItemID());
        condition ? searchConditions.DELIVERABLE_CONDTION.push(condition) : console.log('');

      }

      if (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS && listFilter.value !== TaskFilterTypes.MY_NA_REVIEW_TASKS) {
        searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, listDependentFilter.mapperName,
          listDependentFilter.searchCondition, true, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
        if (listDependentFilter.statusFilterList && listDependentFilter.statusFilterList.length > 0) {
          listDependentFilter.statusFilterList.forEach(filter => {
            const currValue: any = this.getStatusIdByStatusType(this.deliverableStatuses, filter);
            condition = currValue ? this.fieldConfigService.createConditionObject(listDependentFilter.mapperName, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 'or', currValue[0]) : '';
            condition && currValue && currValue[0] ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');
          });
        }
      } else {
        if (listFilter.value !== TaskFilterTypes.MY_NA_REVIEW_TASKS) {
          searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, listDependentFilter.mapperName,
            listDependentFilter.searchCondition, false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
          searchConditions = this.addFilterForUserTeam(searchConditions);
        }
      }
      if (listFilter.value === TaskFilterTypes.MY_NA_REVIEW_TASKS) {
        searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, listDependentFilter.mapperName,
          listDependentFilter.searchCondition, true, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
        if (listDependentFilter.statusFilterList && listDependentFilter.statusFilterList.length > 0) {
          listDependentFilter.statusFilterList.forEach(filter => {
            const currValue: any = this.getStatusIdByStatusType(this.deliverableStatuses, filter);
            condition = currValue ? this.fieldConfigService.createConditionObject(listDependentFilter.mapperName, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 'or', currValue[0]) : '';
            condition && currValue && currValue[0] ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');
          });
        }
      }
      searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, listFilter.mapperName, listFilter.searchCondition, false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);

      searchConditions = this.handleArchivedCondition(searchConditions, listDependentFilter.value === TaskListDependentFilterTypes.ARCHIEVED, MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT);

      searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE, 'TEMPLATE', true, true,
        MPMSearchOperators.IS_NOT,
        MPMSearchOperatorNames.IS_NOT);


      if ((this.isApprover || this.isMember) && listFilter.value !== TaskFilterTypes.MY_NA_REVIEW_TASKS) {
        searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_IS_ACTIVE,
          ((listDependentFilter.value === TaskListDependentFilterTypes.COMPLETED || listDependentFilter.value === TaskListDependentFilterTypes.ARCHIEVED) ? 'false' : 'true'),
          false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
      } else if (this.isReviewer) {
        searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_IS_ACTIVE,
          'true',
          false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
        searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.DELIVERABLE_REVIEW_MPM_FIELDS.IS_DELIVERABLE_REVIEW_ACTIVE,
          'true',
          false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
      }
    }

    /*  if ((this.isApprover || this.isMember) && listFilter.value !== TaskFilterTypes.MY_NA_REVIEW_TASKS) {
       condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_ACTIVE_USER_ID,
         (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS) ? MPMSearchOperators.IS : MPMSearchOperators.IS_NOT,
         (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS) ? MPMSearchOperatorNames.IS : MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, this.sharingservice.getCurrentUserItemID());
       condition ? searchConditions.DELIVERABLE_CONDTION.push(condition) : console.log('');
     } else if (this.isReviewer) {
       condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.DELIVERABLE_REVIEW_MPM_FIELDS.DELIVERABLE_REVIEWER_ID,
         (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS) ? MPMSearchOperators.IS : MPMSearchOperators.IS_NOT,
         (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS) ? MPMSearchOperatorNames.IS : MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, this.sharingservice.getCurrentUserItemID());
       condition ? searchConditions.DELIVERABLE_CONDTION.push(condition) : console.log('');

     }

     if (listFilter.value !== TaskFilterTypes.MY_TEAM_TASKS && listFilter.value !== TaskFilterTypes.MY_NA_REVIEW_TASKS) {
       searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, listDependentFilter.mapperName,
         listDependentFilter.searchCondition, true, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
       if (listDependentFilter.statusFilterList && listDependentFilter.statusFilterList.length > 0) {
         listDependentFilter.statusFilterList.forEach(filter => {
           const currValue: any = this.getStatusIdByStatusType(this.deliverableStatuses, filter);
           condition = currValue ? this.fieldConfigService.createConditionObject(listDependentFilter.mapperName, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 'or', currValue[0]) : '';
           condition && currValue && currValue[0] ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');
         });
       }
     } else {
       if (listFilter.value !== TaskFilterTypes.MY_NA_REVIEW_TASKS) {
         searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, listDependentFilter.mapperName,
           listDependentFilter.searchCondition, false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
         searchConditions = this.addFilterForUserTeam(searchConditions);
       }
     }
     if (listFilter.value === TaskFilterTypes.MY_NA_REVIEW_TASKS) {
       searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, listDependentFilter.mapperName,
         listDependentFilter.searchCondition, true, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
       if (listDependentFilter.statusFilterList && listDependentFilter.statusFilterList.length > 0) {
         listDependentFilter.statusFilterList.forEach(filter => {
           const currValue: any = this.getStatusIdByStatusType(this.deliverableStatuses, filter);
           condition = currValue ? this.fieldConfigService.createConditionObject(listDependentFilter.mapperName, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 'or', currValue[0]) : '';
           condition && currValue && currValue[0] ? (searchConditions.DELIVERABLE_CONDTION.push(condition)) : console.log('');
         });
       }
     }
     searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, listFilter.mapperName, listFilter.searchCondition, false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);

     searchConditions = this.handleArchivedCondition(searchConditions, listDependentFilter.value === TaskListDependentFilterTypes.ARCHIEVED, MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT);

     searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE, 'TEMPLATE', true, true,
       MPMSearchOperators.IS_NOT,
       MPMSearchOperatorNames.IS_NOT);


     if ((this.isApprover || this.isMember) && listFilter.value !== TaskFilterTypes.MY_NA_REVIEW_TASKS) {
       searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_IS_ACTIVE,
         ((listDependentFilter.value === TaskListDependentFilterTypes.COMPLETED || listDependentFilter.value === TaskListDependentFilterTypes.ARCHIEVED) ? 'false' : 'true'),
         false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
     } else if (this.isReviewer) {
       searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_IS_ACTIVE,
         'true',
         false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
       searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.DELIVERABLE_REVIEW_MPM_FIELDS.IS_DELIVERABLE_REVIEW_ACTIVE,
         'true',
         false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);
     } */
    return searchConditions;
  }
}
