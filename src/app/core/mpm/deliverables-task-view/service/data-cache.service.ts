import { Injectable } from '@angular/core';
import { StatusService, MPM_LEVELS, SharingService, Status } from 'mpm-library';
import { Observable } from 'rxjs';
import { MPM_LEVEL } from 'mpm-library';
import { StatusCache } from '../object/StatusCache';

@Injectable({
    providedIn: 'root'
})

export class DataCacheService {

    constructor(
        private statusService: StatusService,
        private sharingservice: SharingService
    ) { }

    statusCache: StatusCache = {};
    cachedListDependentFilter: any;
    status;

    getStatusbyLevel(level: MPM_LEVEL): Observable<Array<Status> | null> {
        return new Observable(observer => {
            if (level && Array.isArray(this.statusCache[level]) && this.statusCache[level].length > 0) {
                observer.next(this.statusCache[level]);
                observer.complete();
            } else {
                /* this.statusService.getStatusByCategoryLevelName(level).subscribe(statuesResponse => {
                    this.statusCache[level] = statuesResponse;
                    observer.next(this.statusCache[level]);
                    observer.complete();
                }); */
                const status = this.statusService.getAllStatusBycategoryName(level);
                this.status = status && status.length > 0 && Array.isArray(status) ? status[0] : status;
                this.statusCache[level] = this.status;
                observer.next(this.statusCache[level]);
                observer.complete();
            }
        });
    }

}
