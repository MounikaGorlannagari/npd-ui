export enum ConditionTypes {
    STATUS = 'STATUS',
    PROJECT_STATUS = 'PROJECT_STATUS',
    DELIVERABLE_TYPE = 'DELIVERABLE_TYPE',
    USER_ID = 'USER_ID'
}