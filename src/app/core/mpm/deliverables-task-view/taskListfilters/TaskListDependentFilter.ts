import { TaskFilterTypes } from './TaskFilterTypes';
import { CustomSelect, StatusTypes, DeliverableTypes } from 'mpm-library';
import { TaskListDependentFilterTypes } from './TaskListDependentFilterTypes';
import { MPMFieldConstants } from 'mpm-library';

export function getTaskListDependentFilters(): any {
    const TO_DO_FILTER: CustomSelect = {
        value: TaskListDependentFilterTypes.TO_DO_FILTER,
        name: 'To Do',
        default: true,
        searchCondition: StatusTypes.INITIAL,
        mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS_ID,
        statusFilterList: [StatusTypes.FINAL_REJECTED]
    };

    const COMPLETED: CustomSelect = {
        value: TaskListDependentFilterTypes.COMPLETED,
        name: 'Completed',
        default: false,
        searchCondition: StatusTypes.FINAL_COMPLETED,
        mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS_ID,
        statusFilterList: [StatusTypes.FINAL_APPROVED, StatusTypes.FINAL_REJECTED]
    };

    const ARCHIEVED: CustomSelect = {
        value: TaskListDependentFilterTypes.ARCHIEVED,
        name: 'Archived',
        default: false,
        searchCondition: StatusTypes.FINAL_COMPLETED,
        mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS_ID,
        statusFilterList: [StatusTypes.FINAL_APPROVED]
    };

    const ALL_DELIVERABLE_TYPES: CustomSelect = {
        value: TaskListDependentFilterTypes.ALL_DELIVERABLE_TYPES,
        name: 'All',
        default: true,
        searchCondition: undefined
    };

    const UPLOAD_DELIVERABLE: CustomSelect = {
        value: TaskListDependentFilterTypes.UPLOAD_DELIVERABLE,
        name: 'Upload',
        default: false,
        searchCondition: DeliverableTypes.UPLOAD,
        mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TYPE
    };

    const REVIW_DELIVERABLE: CustomSelect = {
        value: TaskListDependentFilterTypes.REVIW_DELIVERABLE,
        name: 'Review',
        default: false,
        searchCondition: DeliverableTypes.REVIEW,
        mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TYPE
    };

    const ACTION_DELIVERABLE: CustomSelect = {
        value: TaskListDependentFilterTypes.ACTION_DELIVERABLE,
        name: 'Action',
        default: false,
        searchCondition: DeliverableTypes.ACTION,
        mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TYPE
    };

    const TO_BE_UPLOADED: CustomSelect = {
        value: TaskListDependentFilterTypes.TO_BE_UPLOADED,
        name: 'To be Uploaded',
        default: true,
        searchCondition: StatusTypes.INITIAL,
        mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS_ID
    };

    const REQUESTED_CHANGES: CustomSelect = {
        value: TaskListDependentFilterTypes.REQUESTED_CHANGES,
        name: 'Requested Changes',
        default: true,
        searchCondition: StatusTypes.FINAL_REJECTED,
        mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS_ID
    };

    const TO_BE_REVIEWED: CustomSelect = {
        value: TaskListDependentFilterTypes.TO_BE_REVIEWED,
        name: 'To be Reviewed',
        default: true,
        searchCondition: StatusTypes.INITIAL,
        mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS_ID
    };

    type TaskListDependentFilter = { [key in TaskFilterTypes]?: Array<CustomSelect> };

    const TASK_LIST_DEPENDENT_FILTER: TaskListDependentFilter = {};

    TASK_LIST_DEPENDENT_FILTER[TaskFilterTypes.MY_TASKS] = [TO_DO_FILTER, COMPLETED, ARCHIEVED];
    TASK_LIST_DEPENDENT_FILTER[TaskFilterTypes.MY_TEAM_TASKS] = [ALL_DELIVERABLE_TYPES,// UPLOAD_DELIVERABLE,
        //REVIW_DELIVERABLE,
         ACTION_DELIVERABLE];
    TASK_LIST_DEPENDENT_FILTER[TaskFilterTypes.MY_UPLOAD_TASKS] = [TO_BE_UPLOADED, REQUESTED_CHANGES, COMPLETED, ARCHIEVED];
    TASK_LIST_DEPENDENT_FILTER[TaskFilterTypes.MY_REVIEW_TASKS] = [TO_BE_REVIEWED, COMPLETED, ARCHIEVED];
    TASK_LIST_DEPENDENT_FILTER[TaskFilterTypes.MY_ACTION_TASKS] = [TO_DO_FILTER, COMPLETED, ARCHIEVED];
    TASK_LIST_DEPENDENT_FILTER[TaskFilterTypes.MY_NA_REVIEW_TASKS] = [TO_DO_FILTER, COMPLETED, ARCHIEVED];
    return TASK_LIST_DEPENDENT_FILTER;
}
