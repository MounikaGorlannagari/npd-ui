export enum TaskListDependentFilterTypes {
    TO_DO_FILTER = 'TO_DO_FILTER',
    COMPLETED = 'COMPLETED',
    ARCHIEVED = 'ARCHIVED',
    ALL_DELIVERABLE_TYPES = 'ALL_DELIVERABLE_TYPES',
    UPLOAD_DELIVERABLE = 'UPLOAD_DELIVERABLE',
    REVIW_DELIVERABLE = 'REVIW_DELIVERABLE',
    ACTION_DELIVERABLE = 'ACTION_DELIVERABLE',
    TO_BE_UPLOADED = 'TO_BE_UPLOADED',
    REQUESTED_CHANGES = 'REQUESTED_CHANGES',
    TO_BE_REVIEWED = 'TO_BE_REVIEWED'
}
