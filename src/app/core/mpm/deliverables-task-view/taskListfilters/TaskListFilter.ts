import { ListFilter, DeliverableTypes, MPM_ROLES } from 'mpm-library';
import { TaskFilterTypes } from './TaskFilterTypes';
import { Roles, MPMFieldConstants, StatusTypes } from 'mpm-library';

export function getTaskFilters(roles?: any): Array<ListFilter> {
    const TASK_FILTERS: Array<ListFilter> = [
        {
            value: TaskFilterTypes.MY_TASKS,
            name: 'My Tasks',
            count: 0,
            default: true,
            searchCondition: '',
            deliverableSearchConditions: [],
            acessRoles: [MPM_ROLES.REVIEWER, MPM_ROLES.APPROVER, MPM_ROLES.MEMBER],
            mapperName: undefined
        },
        {
            value: TaskFilterTypes.MY_TEAM_TASKS,
            name: 'My Team Tasks',
            count: 0,
            searchCondition: '',
            deliverableSearchConditions: [],
            /*  acessRoles: [MPM_ROLES.REVIEWER, MPM_ROLES.APPROVER, MPM_ROLES.MEMBER], */
            acessRoles: [MPM_ROLES.APPROVER, MPM_ROLES.MEMBER],
            mapperName: undefined
        },
        // {
        //     value: TaskFilterTypes.MY_UPLOAD_TASKS,
        //     name: 'My Upload Tasks',
        //     count: 0,
        //     searchCondition: DeliverableTypes.UPLOAD,
        //     deliverableSearchConditions: [],
        //     acessRoles: [MPM_ROLES.MEMBER],
        //     mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TYPE
        // },
        // {
        //     value: TaskFilterTypes.MY_REVIEW_TASKS,
        //     name: 'My Review Tasks',
        //     count: 0,
        //     searchCondition: DeliverableTypes.REVIEW,
        //     deliverableSearchConditions: [],
        //     /*  acessRoles: [MPM_ROLES.REVIEWER, MPM_ROLES.APPROVER], */
        //     acessRoles: [MPM_ROLES.APPROVER],
        //     mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TYPE
        // },
        // {
        //     value: TaskFilterTypes.MY_ACTION_TASKS,
        //     name: 'My Action Tasks',
        //     count: 0,
        //     searchCondition: DeliverableTypes.ACTION,
        //     deliverableSearchConditions: [],
        //     acessRoles: [MPM_ROLES.MEMBER],
        //     mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TYPE
        // },
        // {
        //     value: TaskFilterTypes.MY_NA_REVIEW_TASKS,
        //     name: 'My NA Review Tasks',
        //     count: 0,
        //     // default: true,
        //     searchCondition: '',
        //     deliverableSearchConditions: [],
        //     acessRoles: roles ? roles : [],
        //     mapperName: ''
        // },
    ];
    return TASK_FILTERS;
}

const isRoleinRoles = (role, roles: Array<Roles>): boolean => {
    let isRolePresent = false;
    if (!role || !roles) {
        return isRolePresent;
    }
    for (const roleItem of roles) {
        if (roleItem.ROLE_NAME === role) {
            isRolePresent = true;
            break;
        }
    }
    return isRolePresent;
};

const userhasFilterRole = (menuRoles: Array<MPM_ROLES>, userRoles: Array<Roles>): boolean => {
    let userHasAnyOneRole = false;
    if (!menuRoles || !userRoles) {
        return userHasAnyOneRole;
    }
    for (const menuRole of menuRoles) {
        if (isRoleinRoles(menuRole, userRoles)) {
            userHasAnyOneRole = true;
            break;
        }
    }
    return userHasAnyOneRole;
};

export function getAccessibleTaskFilters(filters: Array<ListFilter>, userRoles: Array<Roles>): Array<ListFilter> {
    const TASK_FILTERS: Array<ListFilter> = [];
    if (!filters || !userRoles) {
        return TASK_FILTERS;
    }
    filters.forEach(filter => {
        if (userhasFilterRole(filter.acessRoles, userRoles)) {
            TASK_FILTERS.push(filter);
        }
    });
    return TASK_FILTERS;
}
