import { TaskFilterTypes } from './TaskFilterTypes';

export type TaskFilterType = TaskFilterTypes.MY_TASKS | TaskFilterTypes.MY_TEAM_TASKS
    | TaskFilterTypes.MY_UPLOAD_TASKS |
    TaskFilterTypes.MY_REVIEW_TASKS |
    TaskFilterTypes.MY_ACTION_TASKS;
