import { NgModule } from '@angular/core';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { MaterialModule } from '../../../material.module';
import { DeliverableTasksViewComponent } from './deliverables-task-view.component';
import { DeliverablesTaskViewRoutingModule } from './deliverables-task-view.routing.module';
import {
  DeliverableTaskModule,
  ListFilterModule,
  RefreshButtonModule,
  FacetModule,
  CustomSelectModule,
  ColumnChooserModule,
  PaginationModule
} from 'mpm-library';
import { MpmLibModule } from '../../mpm-lib/mpm-lib.module';

@NgModule({
  declarations: [
    DeliverableTasksViewComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FacetModule,
    ListFilterModule,
    RefreshButtonModule,
    CustomSelectModule,
    ColumnChooserModule,
    DeliverablesTaskViewRoutingModule,
    DeliverableTaskModule,
    PaginationModule,
    MpmLibModule
  ],
  providers: [
    TitleCasePipe
  ]
})

export class DeliverablesTaskViewModule { }
