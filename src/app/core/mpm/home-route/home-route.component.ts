import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharingService } from 'mpm-library';
import { RoutingConstants } from '../routingConstants';
import { MPM_MENU_TYPES } from 'mpm-library';

@Component({
    selector: 'app-home-route',
    template: ``,
    styleUrls: []
})

export class HomeRouteComponent implements OnInit {

    menusForUser: any[];

    constructor(
        private sharingService: SharingService,
        private router: Router,
    ) { }

    ngOnInit() {
        this.menusForUser = this.sharingService.getMenuByType(MPM_MENU_TYPES.HEADER);

        this.menusForUser = this.menusForUser.sort((a, b) => {
            return a.SEQUENCE - b.SEQUENCE;
        });
        
        if (this.menusForUser && this.menusForUser.length > 0 && this.menusForUser[0].LOCATION) {
            this.menusForUser.forEach(menu => {
                if (menu["SEQUENCE"] == 1) {
                    this.router.navigate(
                        [RoutingConstants.mpmBaseRoute + menu.LOCATION],
                        { queryParams: { menuName: menu.LOCATION } }
                    );
                }
              else{
                    this.router.navigate(
                        [RoutingConstants.mpmBaseRoute + this.menusForUser[0].LOCATION],
                        { queryParams: { menuName: this.menusForUser[0].LOCATION } }
                    );
                }
            })
            this.router.navigate(
                [RoutingConstants.mpmBaseRoute + this.menusForUser[0].LOCATION],
                { queryParams: { menuName: this.menusForUser[0].LOCATION } }
            );
        } else {
            this.router.navigate([RoutingConstants.noAccess]);
        }
    }

}
