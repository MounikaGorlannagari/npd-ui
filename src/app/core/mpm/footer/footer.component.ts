import { Component, OnInit } from '@angular/core';
import { VERSION } from '../../../../environments/version';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})

export class FooterComponent {
    currentDate = new Date();
    currentYear = this.currentDate.getFullYear();
    version = VERSION;
}
