import { Component, OnInit, ChangeDetectorRef, OnDestroy, isDevMode, OnChanges, SimpleChanges } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ActivatedRoute, Router } from '@angular/router';
import { RouteService } from '../../route.service';
import { SharingService, AppService, Project, Campaign, AssetService, UtilService, AssetConstants } from 'mpm-library';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MPM_MENU_TYPES, ProjectService } from 'mpm-library';
import { ApplicationConfigConstants } from 'mpm-library';
import { MPMField, FieldConfigService } from 'mpm-library';
import { MPMFieldKeys } from 'mpm-library';
import { MPMFieldConstants } from 'mpm-library';
import { Observable } from 'rxjs';
import { OTMMService } from 'mpm-library';
import { StateService } from 'mpm-library';
import { RoutingConstants } from '../../routingConstants';
import { CampaignUpdateObj } from 'mpm-library';
import { MPM_LEVELS } from 'mpm-library';
import { MenuConstants } from '../../shared/constants/menu.constants';
import { NpdProjectService } from 'src/app/core/npd-ui/npd-project-view/services/npd-project.service';

import { MonsterUtilService } from 'src/app/core/npd-ui/services/monster-util.service';



@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
    animations: [
        trigger('menuToggle', [
            state('collapsed', style({ width: '64px' })),
            state('expanded', style({ width: '*' })),
            transition('expanded <=> collapsed', animate('100ms ease-in-out')),
        ]),
        trigger('rotatedState', [
            state('default', style({ transform: 'rotate(0deg)' })),
            state('rotated', style({ transform: 'rotate(180deg)' })),
            transition('rotated => default', animate('250ms ease-out')),
            transition('default => rotated', animate('250ms ease-in'))
        ])
    ]
})

export class LayoutComponent implements OnInit, OnDestroy,OnChanges {
    mobileQuery: MediaQueryList;
    private mobileQueryListener: () => void;
    selectedProject: Project;
    selectedCampaign: Campaign;
    projectId;
    campaignId;
    isCampaign;
    currentMenuName;
    urlParams;
    isTemplate;
    projectHeading;
    isExpanded = true;
    menus: any[] = [];
    state = 'default';
    projectInfoSubscribe = null;
    assetUrl = '';
    PROJECT_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Project/operations';
    CAMPAIGN_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Campaign/operations';

    GET_PROJECT_DETAILS_WS_METHOD_NAME = 'GetProjectByID';
    GET_CAMPAIGN_WS_METHOD_NAME = 'GetCampaignById';

    selectedCampaignId;

    projectThumbnailType: any;

    routeParams;
    projectDetails: any;
    manufacturingSite: any;
    enableReferenceAsset:any;
    totalAssetsCount:number;
    assetInfoSubscribe=null;

    constructor(
        private media: MediaMatcher,
        private router: Router,
        private changeDetectorRef: ChangeDetectorRef,
        private mpmRouteService: RouteService,
        private activatedRoute: ActivatedRoute,
        private sharingService: SharingService,
        private appService: AppService,
        private projectService: ProjectService,
        private fieldConfigService: FieldConfigService,
        private otmmService: OTMMService,
        private stateService: StateService,
        private npdProjectService: NpdProjectService,
        private assetService:AssetService,
        private utilService:UtilService,
        private monsterUtilService:MonsterUtilService
    ) {
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
        this.assetInfoSubscribe=this.monsterUtilService.overViewProjectAssetCount$.subscribe(res=>{
            this.totalAssetsCount=res;
        })

    }
    ngOnChanges(changes: SimpleChanges): void {
        //throw new Error('Method not implemented.');
        // this.readUrlParams(callback)
    }

    setSideBarExpansionSate() {
        localStorage.setItem('PM-OVERVIEW-SIDE-BAR-EXPANDED-STATE', (this.isExpanded ? 'true' : 'false'));
    }

    rotate() {
        this.state = (this.state === 'default' ? 'rotated' : 'default');
        this.setSideBarExpansionSate();
    }

    getSideBarExpansionSate(): string {
        return localStorage.getItem('PM-OVERVIEW-SIDE-BAR-EXPANDED-STATE');
    }

    routeToSelectedMenu(menu) {
        this.currentMenuName = menu;
        if (this.isCampaign) {
            let menuName;
            if (this.urlParams.menuName === MenuConstants.PROJECTS || this.urlParams.menuName === MenuConstants.TASKS || this.urlParams.menuName === MenuConstants.DELIVERABLES) {
                menuName = this.urlParams.menuName;
            } else {
                menuName = 'Campaign' + this.urlParams.menuName;
            }
            this.stateService.setRoute(menuName, this.urlParams, this.routeParams);
            this.mpmRouteService.goToCampaignViewMenus(menu, this.campaignId);
        } else if (this.urlParams && this.urlParams.isTemplate) {
            let menuName;
            if (this.urlParams.menuName === MenuConstants.PROJECTS || this.urlParams.menuName === MenuConstants.TASKS || this.urlParams.menuName === MenuConstants.DELIVERABLES) {
                menuName = this.urlParams.menuName;
            } else {
                menuName = 'Project' + this.urlParams.menuName;
            }
            this.stateService.setRoute(menuName, this.urlParams, this.routeParams);
            this.mpmRouteService.goToProjectViewMenus(menu, this.urlParams, this.urlParams.isTemplate, this.projectId, this.selectedCampaignId ? this.selectedCampaignId : null);
        } else {
            let menuName;
            if (this.urlParams.menuName === MenuConstants.PROJECTS || this.urlParams.menuName === MenuConstants.TASKS || this.urlParams.menuName === MenuConstants.DELIVERABLES) {
                menuName = this.urlParams.menuName;
            } else {
                menuName = 'Project' + this.urlParams.menuName;
            }
            this.stateService.setRoute(menuName, this.urlParams, this.routeParams);
            this.mpmRouteService.goToProjectViewMenus(menu, this.urlParams, false, this.projectId, this.selectedCampaignId ? this.selectedCampaignId : null);
        }
    }

    backToDashboard(isBackOption) {
        if (this.selectedCampaignId && isBackOption) {
            this.stateService.setRoute((this.urlParams.menuName === MenuConstants.PROJECTS || this.urlParams.menuName === MenuConstants.TASKS)
                ? this.urlParams.menuName : 'Project' + this.urlParams.menuName,
                this.urlParams, this.routeParams);
            const route = this.stateService.getRoute(MenuConstants.PROJECTS);
            const params = route ? route.params : null;
            this.stateService.removeRoute('Project' + MenuConstants.OVERVIEW);
            this.stateService.removeRoute('Project' + MenuConstants.ASSETS);
            this.stateService.removeRoute('Project' + MenuConstants.COMMENTS);
            this.stateService.removeRoute(MenuConstants.TASKS);
            this.stateService.removeRoute(MenuConstants.DELIVERABLES);
            this.stateService.resetSelectedProjectAssets();
            this.stateService.resetAllProjectAssets();
            if (params) {
                this.mpmRouteService.goToCampaignProjectView(params.routeParams.campaignId, params.queryParams.isListView, params.queryParams.sortBy, params.queryParams.sortOrder,
                    params.queryParams.pageNumber, params.queryParams.pageSize, params.queryParams.searchName, params.queryParams.savedSearchName, params.queryParams.advSearchData);
            } else {
                this.mpmRouteService.goToCampaignProjectView(this.selectedCampaignId, null, null, null, null, null, null, null, null);
            }
        } else {
            const routeData = this.stateService.getRoute(MenuConstants.PROJECT_MANAGEMENT);
            const params = routeData ? routeData.params : null;
            this.stateService.resetState();
            this.stateService.resetSelectedProjectAssets();
            this.stateService.resetSelectedCampaignAssets();
            this.stateService.resetAllProjectAssets();
            this.stateService.resetAllCampaignAssets();
            if (params) {
                this.mpmRouteService.goToDashboard(params.queryParams.viewName ? params.queryParams.viewName : null, params.queryParams.isListView ? params.queryParams.isListView : null,
                    params.queryParams.sortBy ? params.queryParams.sortBy : null, params.queryParams.sortOrder ? params.queryParams.sortOrder : null,
                    params.queryParams.pageNumber ? params.queryParams.pageNumber : null, params.queryParams.pageSize ? params.queryParams.pageSize : null,
                    params.queryParams.searchName ? params.queryParams.searchName : null, params.queryParams.savedSearchName ? params.queryParams.savedSearchName : null,
                    params.queryParams.advSearchData ? params.queryParams.advSearchData : null, params.queryParams.facetData ? params.queryParams.facetData : null);
            } else {
                this.mpmRouteService.goToDashboard(null, null, null, null, null, null, null, null, null, null);
            }
        }
    }

    readUrlParams(callback) {
        this.activatedRoute.queryParams.subscribe(queryParams => {
            this.activatedRoute.parent.params.subscribe(parentRouteParams => {
                callback(parentRouteParams, queryParams);
            });
        });
    }

    handleMenuChange() {
        if (this.menus && this.menus.length > 0) {
            this.menus.forEach(menu => {
                if (this.router.url.includes(menu.LOCATION)) {
                    menu.ACTIVE = true;
                } else {
                    menu.ACTIVE = false;
                }
            });
        }
    }

    getProjectDetail(projectId) {
        
        const getRequestObject = {
            projectID: projectId
        };
        this.appService.invokeRequest(this.PROJECT_DETAILS_METHOD_NS, this.GET_PROJECT_DETAILS_WS_METHOD_NAME, getRequestObject)
            .subscribe(response => {
                if (response && response.Project) {
                    this.selectedProject = response.Project;
                    this.getProjectThumbnailURL(this.selectedProject, 'APP_WORK', this.projectThumbnailType).subscribe(assetThumbnailURL => {
                        this.assetUrl = assetThumbnailURL;
                    })
                }
            });
            this.npdProjectService.getProjectDetailsById(this.projectId).subscribe(projResponse => {
                this.projectDetails=projResponse[0];
                if(this.projectDetails.NPD_PROJECT_FINAL_MANUFACTURING_SITE && this.projectDetails.NPD_PROJECT_FINAL_MANUFACTURING_SITE!== 'NA'){
                    this.manufacturingSite=this.projectDetails.NPD_PROJECT_FINAL_MANUFACTURING_SITE
                  }else{
                    this.manufacturingSite=this.projectDetails.PR_EARLY_PROJECT_MANUFACTURING_LOCATION_SITE
                  }
                  this.getAssetDetails();
            });

    }
    getCampaignDetail(campaignId) {
        const getRequestObject = {
            CampaignId: campaignId
        };
        this.appService.invokeRequest(this.CAMPAIGN_DETAILS_METHOD_NS, this.GET_CAMPAIGN_WS_METHOD_NAME, getRequestObject)
            .subscribe(response => {
                if (response && response.Campaign) {
                    this.selectedCampaign = response.Campaign;
                    this.getProjectThumbnailURL(this.selectedCampaign, 'APP_WORK', this.projectThumbnailType).subscribe(assetThumbnailURL => {
                        this.assetUrl = assetThumbnailURL;
                    })
                }
            });
    }
    getProperty(displayColum: MPMField, projectData: any): string {
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColum, projectData);
    }

    getPropertyByMapper(projectData, propertyId) {
        const currMPMField: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, propertyId, MPM_LEVELS.PROJECT);
        return this.getProperty(currMPMField, projectData);
    }

    formProjectThumbnailURL(projectData: any, source: string): Observable<string> {
        return new Observable(observe => {
            let assetID = null;
            let assetThumbnailURL = null;
            if (source === 'APP_WORK') {
                assetID = typeof projectData.THUMBNAIL_ASSET_ID === 'string' ? projectData.THUMBNAIL_ASSET_ID : null;
            } else if (source === 'SOLR') {
                const thumbnailAssetID = this.getPropertyByMapper(projectData, MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_THUMBNAIL_ASSET_ID);
                assetID = thumbnailAssetID !== 'NA' ? thumbnailAssetID : null;
            }
            if (!assetID) {
                observe.next(null);
                observe.complete();
            } else {
                const otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
                this.otmmService.getAssetByAssetId(assetID)
                    .subscribe(assetData => {
                        if (assetData && assetData.rendition_content) {
                            assetThumbnailURL = otmmBaseUrl + assetData.rendition_content.thumbnail_content.url.slice(1);
                        }
                        observe.next(assetThumbnailURL);
                        observe.complete();
                    }, error => {
                        observe.next(assetThumbnailURL);
                        observe.complete();
                    });
            }
        });
    }

    getProjectThumbnailURL(projectData: any, source: string, thumbnailType: string): Observable<string> {
        return new Observable(observe => {
            let assetThumbnailURL = null;
            if (source === 'APP_WORK') {
                assetThumbnailURL = thumbnailType === 'THUMBNAIL_ASSET_ID' ? this.formProjectThumbnailURL(projectData, source).subscribe(data => {
                    observe.next(data);
                    observe.complete();
                })
                    : (typeof projectData.THUMBNAIL_URL === 'string' ? projectData.THUMBNAIL_URL : null);
            } else if (source === 'SOLR') {
                const thumbnailURL = this.getPropertyByMapper(projectData, MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_THUMBNAIL_URL);
                assetThumbnailURL = thumbnailType === 'THUMBNAIL_ASSET_ID' ? this.formProjectThumbnailURL(projectData, source).subscribe(data => {
                    observe.next(data);
                    observe.complete();
                })
                    : (thumbnailURL !== 'NA' ? thumbnailURL : null);
            }
            if (thumbnailType !== 'THUMBNAIL_ASSET_ID') {
                observe.next(assetThumbnailURL);
                observe.complete();
            }
        });
    }

    ngOnInit() {
        const projectConfig = this.sharingService.getProjectConfig();
        this.projectThumbnailType = projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.PROJECT_THUMBNAIL_TYPE];
        this.projectId = null;
        this.campaignId = null;
        this.router.events.subscribe(res => {
            this.handleMenuChange();
        });
        const lastExpansionState = this.getSideBarExpansionSate();
        if (lastExpansionState) {
            this.isExpanded = lastExpansionState === 'true' ? true : false;
        }
        this.readUrlParams((routeParams, queryParams) => {
            
            if (queryParams && routeParams) {
                this.projectId = routeParams.projectId;
                this.campaignId = routeParams.campaignId;
                this.selectedCampaignId = queryParams.campaignId;
                this.urlParams = queryParams;
                this.routeParams = routeParams;
                if (this.urlParams && this.urlParams.isTemplate && this.urlParams.isTemplate === 'true') {
                    this.isTemplate = true;
                }
                if (this.router.url.includes(RoutingConstants.campaignViewBaseRoute)) {
                    this.isCampaign = true;
                    this.menus = this.sharingService.getMenuByType(MPM_MENU_TYPES.CAMPAIGN_OVERVIEW);
                } else if (this.isTemplate) {
                    this.menus = this.sharingService.getMenuByType(MPM_MENU_TYPES.TEMPLATE_OVERVIEW);
                } else {
                    this.menus = this.sharingService.getMenuByType(MPM_MENU_TYPES.PROJECT_OVERVIEW);                
                }
            }
            this.handleMenuChange();
            if (this.projectId) {
                this.getProjectDetail(this.projectId);
            } else if (this.campaignId) {
                this.getCampaignDetail(this.campaignId);
            }
            this.projectInfoSubscribe = this.projectService.onUpdateProjectInfo.subscribe(data => {
                this.selectedProject = data;
            });
        });

        // if (this.projectId) {
        //     this.getProjectDetail(this.projectId);
        // } else if (this.campaignId) {
        //     this.getCampaignDetail(this.campaignId);
        // }
        // this.projectInfoSubscribe = this.projectService.onUpdateProjectInfo.subscribe(data => {
        //     this.selectedProject = data;
        // });
        const appConf=this.sharingService.getAppConfig();
        this.enableReferenceAsset=this.utilService.getBooleanValue(appConf[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_REFERENCE_ASSET]);
        
    }
    ngOnDestroy(): void {
        if (this.projectInfoSubscribe) {
            this.projectInfoSubscribe.unsubscribe();
        }
        if(this.assetInfoSubscribe){
            this.assetInfoSubscribe.unsubscribe();
        }
    }
    
    getAssetDetails(){
        this.otmmService.checkOtmmSession()
            .subscribe(sessionResponse=>{
                let userSessionId;
                if(sessionResponse && sessionResponse.session_resource && sessionResponse.session_resource.session && sessionResponse.session_resource.session.id){
                    userSessionId=sessionResponse.session_resource.session.id;
                }
                const assetSearch=(!this.enableReferenceAsset)?AssetConstants.ADD_REFERENCE_ASSET_SEARCH_CONDITION:AssetConstants.ASSET_SEARCH_CONDITION;
                this.assetService.getAssets(assetSearch,this.projectDetails.OTMM_FOLDER_ID,0,1,userSessionId).subscribe(res=>{
                    this.totalAssetsCount = res.search_result_resource.search_result.total_hit_count;
                },error=>{
                    const eventData={message:'Error while getting asset details',type:'error'}
                }
                );
            },error=>{
                const eventData={message:'Error while getting otmm session',type:'error'};
            }
            )
    }

    

}
