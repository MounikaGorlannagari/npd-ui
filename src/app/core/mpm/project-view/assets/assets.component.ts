import { Component, OnInit, isDevMode, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService, AssetService, OTMMService, DeliverableConstants, TaskService, OTMMMPMDataTypes, SharingService, UploadComponent, EntityAppDefService, StatusTypes, TaskConstants, AppService, DashboardMenuConfig, ViewConfigService, ViewConfig } from 'mpm-library';
import { NotificationService, AssetConstants, SearchRequest, FieldConfigService, IndexerService } from 'mpm-library';
import { MatDialog } from '@angular/material/dialog';
import { AssetDetailsComponent } from '../../shared/components/asset-details/asset-details.component';
import { AssetsVersionModalComponent } from '../../shared/components/assets-version-modal/assets-version-modal.component';
import { ShareAssetsModalComponent } from '../../shared/components/share-assets-modal/share-assets-modal.component';
import { AssetPreviewComponent } from '../../shared/components/asset-preview/asset-preview.component';
import { ApplicationConfigConstants } from 'mpm-library';
import { UtilService } from 'mpm-library';
import { SelectionModel } from '@angular/cdk/collections';
import { otmmServicesConstants } from 'mpm-library';
import { RouteService } from '../../route.service';
import { RoutingConstants } from '../../routingConstants';
import { Observable } from 'rxjs';
import { StateService, SearchConfigConstants } from 'mpm-library';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { MonsterUtilService } from 'src/app/core/npd-ui/services/monster-util.service';



@Component({
    selector: 'app-assets',
    templateUrl: './assets.component.html',
    styleUrls: ['./assets.component.scss']
})
export class AssetsComponent implements OnInit {
  public userSessionId;

    projectId;
    assets;
    urlParams;
    totalAssetsCount;
    selectedAssets;
    folderId;
    downloadTypeObj;
    selectedDeliverables = new SelectionModel(true, []);
    //pageSize = 10;
    page = 1;
    skip = 0;
    //top = 10;
    pageSize: number;
    top: number;
    canDownload: boolean;
    pageNumber: number;
    enableReferenceAsset;
    project;
    isReferenceAsset = false;
    enableAddReferenceAsset;
    campaignId;
    isCampaignAssetView;
    campaign;
    disableReferenceAsset = false;
    crRole;
    deliverableId = [];
    taskId = [];
    assetId = [];
    deliverables = [];
    taskName = [];
    taskData;
    assetStatus;
    selectedAssetData = [];
    allAssetData = [];
    currentViewConfig;
    assetMenuconfig;
    assetDisplayableFields;
    assetCardFields;
    columnChooserFields;
    isRetrieveAssetClicked: boolean = false;
    columnChooserData;
    defaultViewFilterName;
    isColumnsFreezable:boolean = false;
    constructor(
        public activatedRoute: ActivatedRoute,
        public loaderService: LoaderService,
        public notification: NotificationService,
        public dialog: MatDialog,
        public assetService: AssetService,
        public otmmService: OTMMService,
        public taskService: TaskService,
        public sharingService: SharingService,
        public fieldConfigService: FieldConfigService,
        public indexerService: IndexerService,
        public utilService: UtilService,
        public routeService: RouteService,
        public entityAppDefService: EntityAppDefService,
        public router: Router,
        public appService: AppService,
        public stateService: StateService,
        public notificationService: NotificationService,
        public viewConfigService: ViewConfigService,
        public monsterUtilService:MonsterUtilService
    ) { }

    readUrlParams(callback) {
        this.activatedRoute.queryParams.subscribe(queryParams => {
            this.activatedRoute.parent.params.subscribe(parentRouteParams => {
                callback(parentRouteParams, queryParams);
            });
        });
    }

    notificationHandler(event) {
        if (event.message) {
            this.notification.error(event.message);
        }
    }
    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }

    getAssetMenuConfig(){
        this.viewConfigService.getAllDisplayFeildForViewConfig('ASSET_DETAILS_VIEW').subscribe(
            (viewDetails: ViewConfig) => {
                this.assetMenuconfig.assetViewConfig = viewDetails;
                this.assetDisplayableFields = this.assetMenuconfig.assetViewConfig.R_PM_LIST_VIEW_MPM_FIELDS;
                this.assetCardFields = this.assetMenuconfig.assetViewConfig.R_PM_CARD_VIEW_MPM_FIELDS;
                this.assetMenuconfig.assetViewConfig.assetDisplayableFields = this.assetDisplayableFields;
            }
        )
        const preference = sessionStorage.getItem("CURRENT_USER_PREFERENCE-ASSET_DETAILS_VIEW")
        if(!preference){
            this.resetView();
        }
        const currentUserPreference = sessionStorage.getItem('CURRENT_ASSETS_VIEW');
        this.assetMenuconfig.IS_LIST_VIEW = JSON.parse(currentUserPreference)?.isListView ? JSON.parse(currentUserPreference).isListView : true;
    }

    hasReferenceAsset(assets) {
        let referenceAsset = false;
        // assets.forEach(asset => {
        for (const asset of assets) {
            if (asset.metadata && asset.metadata.metadata_element_list) {
                asset.metadata.metadata_element_list.forEach(metadataElement => {
                    if (metadataElement.id === 'MPM.DELIVERABLE.GROUP') {
                        metadataElement.metadata_element_list.forEach(metadataElementList => {
                            if (metadataElementList.id === 'MPM.DELIVERABLE.STATUS') {
                                this.assetStatus = metadataElementList.value.value.value;
                            }
                        });
                    } else if (metadataElement.id === 'MPM.ASSET.GROUP') {
                        metadataElement.metadata_element_list.forEach(metadataElementList => {
                            if (metadataElementList.id === 'MPM.ASSET.IS_REFERENCE_ASSET') {
                                referenceAsset = (metadataElementList && metadataElementList.value && metadataElementList.value.value && metadataElementList.value.value.value === 'true') ? true : false;
                            }
                        });
                    }
                });
            }
            if (referenceAsset) {
                return referenceAsset;

            }
        }
        return referenceAsset;
    }

    setCurrentView() {
        const currentViewData = {
            sortName: null,
            sortOrder: null,
            pageNumber: this.pageNumber,
            pageSize: this.pageSize,
            isListView: this.assetMenuconfig.IS_LIST_VIEW,
            viewId: this.currentViewConfig['MPM_View_Config-id'].Id,
            viewName: this.currentViewConfig.VIEW
        }
        sessionStorage.setItem('CURRENT_ASSETS_VIEW', JSON.stringify(currentViewData));
    }

    retrieveSelectedAssets(event) {
        this.selectedAssets = event;
        const selectedAssetIds = [];
        if (this.selectedAssets.selected) {
            this.selectedAssets.selected.forEach(asset => {
                selectedAssetIds.push(asset.asset_id);
            });
            if (this.isCampaignAssetView) {
                this.stateService.setSelectedCampaignAssets(selectedAssetIds);
            } else {
                this.stateService.setSelectedProjectAssets(selectedAssetIds);
            }
            this.isReferenceAsset = false;
            if (this.selectedAssets.selected.length > 0) {
                this.isReferenceAsset = this.hasReferenceAsset(this.selectedAssets.selected);
            }

        } else {
            if (this.isCampaignAssetView) {
                this.stateService.setSelectedCampaignAssets(selectedAssetIds);
            } else {
                this.stateService.setSelectedProjectAssets(selectedAssetIds);
            }
            this.isReferenceAsset = false;
        }
    }

    download(event) {
        this.assetService.initiateDownload(event.assetURL, event.assetName);
    }

    downloadAsset() {
        if (otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload) {
            if (this.downloadTypeObj.selected.value !== AssetConstants.DOWNLOAD_TYPES.ZIP) {
                const downloadParams = {
                    assetObjects: []
                };
                this.selectedAssets.selected.map(asset => {
                    if (asset && asset.asset_id) {
                        downloadParams.assetObjects.push({
                            id: asset.asset_id,
                            type: 'ORIGINAL'
                        })
                    }
                });
                this.assetService.initQDSDownload(downloadParams);
            } else {
                this.downloadAsZip();
            }
        } else {
            if (this.downloadTypeObj.selected.value !== AssetConstants.DOWNLOAD_TYPES.ZIP) {
                const otmmBaseUrl = isDevMode() ? '' : this.sharingService.getMediaManagerConfig().url;
                this.selectedAssets.selected.map(asset => {
                    if (asset) {
                        const assetURL = otmmBaseUrl + asset?.master_content_info.url.slice(1);
                        this.utilService.initiateDownload(assetURL, asset.name);
                    }
                });
            } else {
                this.downloadAsZip();
            }
        }
    }

    getOrderedDisplayableFields(displayableFields) {
        this.columnChooserFields = this.formColumnChooserField(displayableFields);
        this.updateDisplayColumns({'column':this.columnChooserFields,'frezeCount':0});
    }

    downloadAsZip() {
        const assetObjects = [];
        this.selectedAssets.selected.map(asset => {
            if (asset && asset.asset_id) {
                assetObjects.push(asset.asset_id);
            }
        });
        this.loaderService.show();
        this.otmmService.createExportJob(assetObjects, 'ZIP')
            .subscribe(response => {
                this.loaderService.hide();
                if (response && response.export_job_handle && response.export_job_handle.export_response &&
                    response.export_job_handle.export_response.exportable_count &&
                    response.export_job_handle.export_response.exportable_count > 0) {
                    this.notification.info('Download has been initiated, Please check the download tray');
                }
            }, error => {
                this.loaderService.hide();
            });
    }

    setViewData(userPreference) {
        sessionStorage.setItem('USER_PREFERENCE-' + this.assetMenuconfig.assetViewConfig.VIEW, userPreference);
    }


    updateDisplayColumns(displayColumnFields) {



        this.assetDisplayableFields = [];
        const listFieldOrder = [];
            displayColumnFields.column.sort((a, b) => a.POSITION - b.POSITION);
            displayColumnFields.column.forEach(displayColumnField => {
                if (displayColumnField.selected === true) {
                    const selectedField = this.assetMenuconfig.assetViewConfig.assetDisplayableFields.find(field => field.INDEXER_FIELD_ID === displayColumnField.INDEXER_FIELD_ID);
                    if (selectedField) {
                        this.assetDisplayableFields.push(selectedField);
                        listFieldOrder.push(selectedField.MAPPER_NAME);
                    }
                }
            });
        this.assetMenuconfig.assetViewConfig.listFieldOrder = listFieldOrder.toString();
        const userPreferenceData = this.formViewData();
        this.setViewData(userPreferenceData);
        this.setCurrentViewData(userPreferenceData);
    }

    resetView() {
        sessionStorage.removeItem('USER_PREFERENCE-' + this.assetMenuconfig.assetViewConfig.VIEW);
        const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.assetMenuconfig.assetViewConfig['MPM_View_Config-id'].Id);
``
        const userPreferenceId = userPreference && userPreference['MPM_User_Preference-id'] && userPreference['MPM_User_Preference-id'].Id ?
            userPreference['MPM_User_Preference-id'].Id : null;

        if (userPreferenceId) {
            this.loaderService.show();
            this.appService.deleteUserPreference(userPreferenceId).subscribe(response => {
                this.loaderService.hide();
                if (response) {
                    // this.notificationService.success('User preference have been reset successfully');
                }
            });
        } else {
            // this.notificationService.success('User preference have been reset successfully');
        }
        this.resetCurrentView();
    }

    resetCurrentView() {
        this.assetMenuconfig.assetViewConfig.assetDisplayableFields.forEach(displayField => {
            displayField.width = '';
        });
        this.assetMenuconfig.assetViewConfig.listFieldOrder = this.assetMenuconfig.assetViewConfig.LIST_FIELD_ORDER;
        this.assetDisplayableFields = this.assetMenuconfig.assetViewConfig.assetDisplayableFields;
        this.columnChooserFields = this.formColumnChooserField(this.assetMenuconfig.assetViewConfig.assetDisplayableFields);
        const userPreferenceData = this.formViewData();
        this.setCurrentViewData(userPreferenceData);
        this.defaultViewFilterName = null;
    }

    setCurrentViewData(userPreference) {
        sessionStorage.setItem('CURRENT_USER_PREFERENCE-' + this.assetMenuconfig.assetViewConfig.VIEW, userPreference);
    }

    formViewData( isListView?: boolean) {
        const userPreference = {
            USER_ID: this.sharingService.getCurrentPerson().Id,
            PREFERENCE_TYPE: 'VIEW',
            VIEW_ID: this.assetMenuconfig.assetViewConfig['MPM_View_Config-id'].Id,
            IS_LIST_VIEW: isListView,
            FIELD_DATA: []
        };
        this.assetMenuconfig.assetViewConfig.assetDisplayableFields.forEach(viewField => {
            if (viewField.INDEXER_FIELD_ID) {
                const fieldIndex = this.assetDisplayableFields.findIndex(displayableField => displayableField.INDEXER_FIELD_ID === viewField.INDEXER_FIELD_ID);
                userPreference.FIELD_DATA.push({
                    [viewField.INDEXER_FIELD_ID]: {
                        POSITION: fieldIndex >= 0 ? fieldIndex : null,
                        WIDTH: viewField && viewField.width ? viewField.width : '',
                        IS_DISPLAY: fieldIndex >= 0
                    }
                });
            }
        });
        return JSON.stringify(userPreference);
    }

    setUserPreference() {
        let userPreferenceSessionData: any = sessionStorage.getItem('USER_PREFERENCE-' + this.assetMenuconfig.assetViewConfig.VIEW);
        userPreferenceSessionData = userPreferenceSessionData ? JSON.parse(userPreferenceSessionData) : null;
        const userPreferenceViewField = userPreferenceSessionData && userPreferenceSessionData.FIELD_DATA ? userPreferenceSessionData.FIELD_DATA : null;
        if (userPreferenceViewField) {
            userPreferenceViewField.sort((a, b) => {
                const firstKey: any = Object.keys(a);
                const firstValue = a[firstKey];
                const secondKey: any = Object.keys(b);
                const secondValue = b[secondKey];
                return firstValue.POSITION - secondValue.POSITION;
            });
            const displayableFields = [];
            const listFieldOrder = [];
            userPreferenceViewField.forEach(viewField => {
                const viewFieldId = Object.keys(viewField)[0];
                if (viewField[viewFieldId].IS_DISPLAY === true) {
                    const selectedField = this.assetMenuconfig.assetViewConfig.assetDisplayableFields.find(field => field.INDEXER_FIELD_ID === viewFieldId);
                    if (selectedField) {
                        selectedField.width = viewField[viewFieldId].WIDTH;
                        displayableFields.push(selectedField);
                        listFieldOrder.push(selectedField.MAPPER_NAME);
                    }
                }
            });
            this.assetMenuconfig.assetViewConfig.listFieldOrder = listFieldOrder.toString();
            this.assetDisplayableFields = displayableFields;
            this.columnChooserFields = this.formColumnChooserField(displayableFields);
        } else {
            const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.assetMenuconfig.assetViewConfig['MPM_View_Config-id'].Id);
            const currentViewFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA).data : null;
            if (currentViewFieldData && this.utilService.getBooleanValue(userPreference.IS_LIST_VIEW)) {
                currentViewFieldData.sort((a, b) => {
                    const firstKey: any = Object.keys(a);
                    const firstValue = a[firstKey];
                    const secondKey: any = Object.keys(b);
                    const secondValue = b[secondKey];
                    return firstValue.POSITION - secondValue.POSITION;
                });
                const displayableFields = [];
                const listFieldOrder = [];
                currentViewFieldData.forEach(viewField => {
                    const viewFieldId = Object.keys(viewField)[0];
                    if (viewField[viewFieldId].IS_DISPLAY === true) {
                        const selectedField = this.assetMenuconfig.assetViewConfig.assetDisplayableFields.find(field => field.INDEXER_FIELD_ID === viewFieldId);
                        if (selectedField) {
                            selectedField.width = viewField[viewFieldId].WIDTH;
                            displayableFields.push(selectedField);
                            listFieldOrder.push(selectedField.MAPPER_NAME);
                        }
                    }
                });
                this.assetMenuconfig.assetViewConfig.listFieldOrder = listFieldOrder.toString();
               this.assetDisplayableFields = displayableFields;
                this.columnChooserFields = this.formColumnChooserField(displayableFields);
            } else {
                this.assetMenuconfig.assetViewConfig.listFieldOrder = this.assetMenuconfig.assetViewConfig.LIST_FIELD_ORDER;
               this.assetDisplayableFields = this.assetMenuconfig.assetViewConfig.assetDisplayableFields;
                this.columnChooserFields = this.formColumnChooserField(this.assetMenuconfig.assetViewConfig.assetDisplayableFields);

            }
        }
    }


    formColumnChooserField(listFields) {
        const columnChooserFields = [];
        this.assetMenuconfig.assetViewConfig.assetDisplayableFields.forEach((displayField, index) => {
            const selectedFieldIndex = listFields.findIndex(listField => listField?.INDEXER_FIELD_ID === displayField.INDEXER_FIELD_ID);
            if (selectedFieldIndex >= 0) {
                columnChooserFields.push({
                    INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
                    DISPLAY_NAME: displayField.DISPLAY_NAME,
                    POSITION: selectedFieldIndex,
                    selected: true
                });
            } else {
                columnChooserFields.push({
                    INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
                    DISPLAY_NAME: displayField.DISPLAY_NAME,
                    POSITION: index,
                    selected: false
                });
            }
        });
        return columnChooserFields;
    }

    retrieveAssetData(event) {
        const currentAsset = event;
        const deliverableItemId = this.taskService.getProperty(event, DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID);
        const assetDetailSearchCondition = {
            search_condition_list: {
                search_condition: [{
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.UTILS.DATA_TYPE',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    value: OTMMMPMDataTypes.DELIVERABLE
                }, {
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.DELIVERABLE.ITEM_ID',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    value: deliverableItemId,
                    relational_operator: 'and'
                }]
            }
        };

        const fieldConfigs = this.fieldConfigService.getFieldsbyFeildCategoryLevel(event.metadata.object_type);

        if (event && event.metadata && event.metadata.metadata_element_list) {
            event.metadata.metadata_element_list.forEach(metadataElement => {
                if (metadataElement.id === 'MPM.ASSET.GROUP') {
                    metadataElement.metadata_element_list.forEach(metadataElementList => {
                        if (metadataElementList.id === 'MPM.ASSET.IS_REFERENCE_ASSET') {
                            this.isReferenceAsset = metadataElementList.value.value.value === 'true' ? true : false;
                        }
                    });
                }
            });
        }

        const searchCondition = this.isReferenceAsset ? JSON.parse(JSON.stringify(AssetConstants.REFERENCE_ASSET_DETAILS_CONDITION)) : JSON.parse(JSON.stringify(AssetConstants.ASSET_DETAILS_CONDITION));

        searchCondition.search_condition_list.search_condition.map(function (search) {
            const fieldConfig = fieldConfigs.find(function (field) { return search.value == field.MAPPER_NAME });
            const metadata = event.metadata.metadata_element_list[0].metadata_element_list.find(function (metadata) {
                return metadata.id === fieldConfig.OTMM_FIELD_ID;
            });
            return search.value = (metadata && metadata.value && metadata.value.value && metadata.value.value.value) ? metadata.value.value.value : '';
        });

        this.loadingHandler(true);
        this.otmmService.checkOtmmSession()
            .subscribe(sessionResponse => {
                let userSessionId;
                const skip = 0;
                const top = 10;
                if (sessionResponse && sessionResponse.session_resource && sessionResponse.session_resource.session && sessionResponse.session_resource.session.id) {
                    userSessionId = sessionResponse.session_resource.session.id;
                }
                this.assetService.getAssetDetails(searchCondition).subscribe(asset_details => {
                    if (asset_details && asset_details.data && asset_details.data.length > 0) {
                        let asset = {};
                        asset['data'] = asset_details.data;
                        asset['metadata_fields'] = this.sharingService.getAllApplicationViewConfig().find(application_views => { return application_views.VIEW === 'ASSET_DETAILS_VIEW' });
                        if (asset) {
                                asset['isTemplate'] = true;
                                this.dialog.open(AssetDetailsComponent, {
                                    width: '70%',
                                    disableClose: true,
                                    data: asset
                                });
                            this.loadingHandler(false);
                            this.isRetrieveAssetClicked = false;
                        } else {
                            const eventData = { message: 'Error while getting asset details', type: 'error' };
                            this.notificationHandler(eventData);
                        }
                    } else {
                        this.loadingHandler(false);
                    }
                }, asset_details_error => {
                    this.loadingHandler(false);
                    const eventData = { message: 'Error getting Asset related details', type: 'error' };
                    this.notificationHandler(eventData);
                });
            }, session_error => {
                this.loadingHandler(false);
                const eventData = { message: 'Error getting OTMM session details', type: 'error' };
                this.notificationHandler(eventData);
            });
    }

    retrieveAssetVersionsData(event) {
        this.dialog.open(AssetsVersionModalComponent, {
            width: '70%',
            disableClose: true,
            data: event
        });
    }

    retrieveAssetPreviewData(event) {
        this.dialog.open(AssetPreviewComponent, {
            // width: '100%',
            width: '70%',
            disableClose: true,
            data: { 'asset': event, 'data': this.assets }
        });
    }

    retrieveAssets(event) {
        this.totalAssetsCount = event.length;
    }

    shareAsset(event) {
        const currentUserEmail = this.sharingService.getCurrentUserEmailId();

        if (currentUserEmail && currentUserEmail.length > 0) {
            this.dialog.open(ShareAssetsModalComponent, {
                width: '50%',
                disableClose: true,
                data: event
            });
        } else {
            this.notification.error('The user does not have Email Id');
        }
    }

    selectAllDeliverables(event) {

        if (event) {
            this.isReferenceAsset = false;
            this.isReferenceAsset = this.hasReferenceAsset(this.assets);
        }
        this.assets.map(asset=> {
            let tempAsset={selected:[asset]}
            this.retrieveSelectedAssets(tempAsset)
        })
    }

    openCR(event) {
        this.router.navigate([RoutingConstants.projectViewBaseRoute], {
            queryParams: {
                isCreativeReview: true,
                crData: JSON.stringify(event)
            }
        });
    }

    getProperty(asset, property) {
        return this.taskService.getProperty(asset, property);
    }

    getFileProofingSessionData(event, appId, deliverable, assetId, crRoleId, versioning, taskName): Observable<any> {
        return new Observable(observer => {
            this.appService.getMultipleFileProofingSession(appId, deliverable, assetId, taskName, crRoleId, versioning)
                .subscribe(response => {
                    if (response && response.sessionDetails && response.sessionDetails.sessionUrl) {
                        this.loaderService.hide();
                        let crData = [];
                        event.forEach(asset => {
                            crData.push({
                                url: response.sessionDetails.sessionUrl,
                                headerInfos: [{
                                    title: 'Name',
                                    value: this.getProperty(asset, DeliverableConstants.DELIVERABLE_NAME_METADATAFIELD_ID),
                                    type: 'string'
                                }]
                            });
                        });
                        observer.next(crData);
                        observer.complete();
                    } else {
                        observer.error('Something went wrong while opening Creative Review.');
                    }
                }, error => {
                    observer.error(error);
                });
        });
    }

    bulkOpenCR(event) {
        this.deliverables = [];
        this.taskName = [];
        const appId = this.entityAppDefService.getApplicationID();
        this.deliverableId = [];
        this.taskId = [];
        this.assetId = [];
        // this.taskName = this.taskData.NAME;

        event.forEach(element => {
            this.deliverableId.push(this.taskService.getProperty(element, DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID).split('.')[1]);
            this.taskId.push(this.taskService.getProperty(element, TaskConstants.TASK_ITEM_ID_METADATAFIELD_ID).split('.')[1]);
            this.assetId.push(element.original_asset_id);
        });
        this.taskId.forEach(task => {
            this.taskService.getTaskById(task).subscribe(taskResponse => {
                this.taskData = taskResponse;
                this.taskName.push(this.taskData.NAME);
            });
        });

        this.assetService.GetCRIsViewer(true).subscribe(crResponse => {
            this.crRole = crResponse.MPM_CR_Role;
            this.loaderService.show();
            this.deliverableId.forEach(deliverable => {
                this.deliverables.push({
                    deliverableId: deliverable,
                    versioning: true
                });

            });

            this.getFileProofingSessionData(event, appId, this.deliverables, this.assetId, this.crRole.ROLE_ID, 'true', this.taskName)/* this.taskName */
                .subscribe(crData => {
                    this.loaderService.hide();
                    // this.crHandler.next(crData);
                    this.crRouting(crData);
                }, error => {
                    this.loaderService.hide();
                    //  this.notificationService.error('Something went wrong while opening Creative Review');
                });
        });
    }

    crRouting(event) {
        event.forEach(crUrl => {
            this.router.navigate([RoutingConstants.projectViewBaseRoute], {
                queryParams: {
                    isCreativeReview: true,
                    crData: JSON.stringify(crUrl)
                }
            });
        });
    }

    pagination(pagination: any) {
        this.pageSize = pagination.pageSize;
        this.top = pagination.pageSize;
        this.skip = (pagination.pageIndex * pagination.pageSize);
        this.page = 1 + pagination.pageIndex;
        this.pageNumber = this.page;

        this.loadingHandler(true);
        if (this.isCampaignAssetView) {
            this.routeService.gotoCampaignAssets(this.urlParams.isTemplate, this.campaignId, this.pageNumber, this.pageSize);
        } else {
            this.routeService.gotoProjectAssets(this.urlParams.isTemplate, this.projectId, this.urlParams.campaignId, this.pageNumber, this.pageSize);
        }
        this.getAssets();
        this.setCurrentView();
    }

    refresh() {
        if (this.isCampaignAssetView) {
            this.assetService.getCampaignDetails(this.campaignId).subscribe(response => {
                if (response && response.Campaign) {
                    this.campaign = response.Campaign;
                    this.folderId = response.Campaign.OTMM_FOLDER_ID;
                    this.getAssets();
                } else {
                    this.loadingHandler(false);
                }
                //  this.enableAddReferenceAsset = (this.project.R_PO_PROJECT_OWNER["Identity-id"].Id === this.sharingService.getCurrentUserItemID()) ? true : false;
            }, error => {
                this.loadingHandler(false);
                const eventData = { message: 'Error while getting project details', type: 'error' };
                this.notificationHandler(eventData);
            });
        } else {
            this.assetService.getProjectDetails(this.projectId).subscribe(response => {
                if (response && response.Project) {
                    this.project = response.Project;
                    this.folderId = response.Project.OTMM_FOLDER_ID;
                    this.entityAppDefService.getStatusById(this.project.R_PO_STATUS['MPM_Status-id'].Id).subscribe(projectStatus => {
                        if (projectStatus.STATUS_TYPE === StatusTypes.FINAL_COMPLETED || projectStatus.STATUS_TYPE === StatusTypes.FINAL_CANCELLED || projectStatus.TYPE === StatusTypes.ONHOLD) {
                            this.disableReferenceAsset = true;
                        }
                        this.getAssets();
                    });
                } else {
                    this.loadingHandler(false);
                }
                this.enableAddReferenceAsset = (this.project.R_PO_PROJECT_OWNER["Identity-id"].Id === this.sharingService.getCurrentUserItemID()) ? true : false;
            }, error => {
                this.loadingHandler(false);
                const eventData = { message: 'Error while getting project details', type: 'error' };
                this.notificationHandler(eventData);
            });
        }
        this.loadingHandler(true);
    }

    referenceAsset() {
        let fileToRevision = '';
        let isRevision = false;
        let maxFiles = 1;
        const assetConfig = this.sharingService.getAssetConfig();
        const appConfig = this.sharingService.getAppConfig();
        const allowDuplicateDeliverable = this.utilService.getBooleanValue(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_DUPLICATE_DELIVERABLES]);
        const maxFileSize = assetConfig[ApplicationConfigConstants.MPM_ASSET_CONFIG.MAX_FILE_SIZE];

        maxFiles = assetConfig[ApplicationConfigConstants.MPM_ASSET_CONFIG.MAX_NO_OF_FILES];
        const dialogRef = this.dialog.open(UploadComponent, {
            width: '60%',
            // height: '80%',
            disableClose: true,
            data: {
                modalName: 'Add Reference Assets',
                needReference: true,
                deliverable: '',
                isRevisionUpload: '',
                fileToRevision,
                maxFiles,
                maxFileSize,
                folderId: this.folderId,
                project: this.project
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                
            }
        });
    }

    getAssets() {
        this.otmmService.checkOtmmSession()
            .subscribe(sessionResponse => {
                let userSessionId;
                if (sessionResponse && sessionResponse.session_resource && sessionResponse.session_resource.session && sessionResponse.session_resource.session.id) {
                    userSessionId = sessionResponse.session_resource.session.id;
                    this.userSessionId = userSessionId;
                }
                const assetSearch = (!this.enableReferenceAsset) ? AssetConstants.ADD_REFERENCE_ASSET_SEARCH_CONDITION : AssetConstants.ASSET_SEARCH_CONDITION;
                this.assetService.getAssets(assetSearch, this.folderId, this.skip, this.top, userSessionId).subscribe(res => {
                    if (res && res.search_result_resource && res.search_result_resource.asset_list) {
                        if (res.search_result_resource.asset_list.length === 0 && this.skip > 0) {
                            this.skip = 0;
                            this.page = 1;
                            this.pageNumber = this.page;
                            this.notification.info('As there is no data in current page, redirecting to first page.');
                            this.loaderService.show();
                            this.setCurrentView();
                            this.getAssets();
                        } else {
                            this.loadingHandler(false);
                            this.totalAssetsCount = res.search_result_resource.search_result.total_hit_count;
                            this.monsterUtilService.sendAssetCount(this.totalAssetsCount);
                            this.assets = res.search_result_resource.asset_list;
                            if (this.isCampaignAssetView) {
                                this.selectedAssetData = this.stateService.getSelectedCampaignAssets();
                                this.stateService.setAllCampaignAssets(this.assets);
                                this.allAssetData = this.stateService.getAllCampaignAssets();
                            } else {
                                this.selectedAssetData = this.stateService.getSelectedProjectAssets();
                                this.stateService.setAllProjectAssets(this.assets);
                                this.allAssetData = this.stateService.getAllProjectAssets();
                            }
                        }
                    } else {
                        this.loadingHandler(false);
                    }
                }, error => {
                    this.loadingHandler(false);
                    const eventData = { message: 'Error while getting asset details', type: 'error' };
                    this.notificationHandler(eventData);
                });
            }, error => {
                this.loadingHandler(false);
                const eventData = { message: 'Error while getting otmm session', type: 'error' };
                this.notificationHandler(eventData);
            });
    }
    changeDownloadType(selectedOption) {
        this.downloadTypeObj.selected = selectedOption;
    }

    onChangeView(isListView:boolean){
        this.assetMenuconfig.IS_LIST_VIEW = isListView;
        this.setCurrentView();
    }

    ngOnInit() {
        this.projectId = null;
        this.campaignId = null;
        this.assetMenuconfig = {
            IS_LIST_VIEW: null,
            assetViewConfig: null,
        };
        const appConfig = this.sharingService.getAppConfig();
        this.top = this.utilService.convertStringToNumber(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.pageSize = this.utilService.convertStringToNumber(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.pageNumber = this.page;
        this.canDownload = this.utilService.getBooleanValue(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_ASSET_DOWNLOAD]);
        this.downloadTypeObj = AssetConstants.DOWNLOAD_TYPES_OBJECTS;
        this.enableReferenceAsset = this.utilService.getBooleanValue(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_REFERENCE_ASSET]);
        this.currentViewConfig = this.sharingService.getViewConfigByName(SearchConfigConstants.SEARCH_NAME.ASSET_DETAILS);
        if (this.currentViewConfig) {
            const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.currentViewConfig['MPM_View_Config-id'].Id);
            const userPreferenceFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
            if (userPreferenceFieldData) {
                this.pageSize = userPreferenceFieldData.pageSize;
                this.top = this.pageSize;
                this.pageNumber = userPreferenceFieldData.pageNumber;
                this.page = this.pageNumber
                this.skip = (this.pageNumber - 1) * this.pageSize;
            }
        }
        this.readUrlParams((routeParams, queryParams) => {
            if (queryParams && routeParams) {
                this.campaignId = routeParams.campaignId;
                this.projectId = routeParams.projectId;
                this.isCampaignAssetView = (this.campaignId && (this.campaignId != undefined)) ? true : false;
                this.urlParams = queryParams;
                if (queryParams.pageSize) {
                    if (this.pageSize !== Number(queryParams.pageSize)) {
                        this.top = Number(queryParams.pageSize);
                        this.pageSize = Number(queryParams.pageSize);
                    }
                }
                if (queryParams.pageNumber) {
                    if (this.page !== Number(queryParams.pageNumber)) {
                        this.page = Number(queryParams.pageNumber);
                        this.pageNumber = Number(queryParams.pageNumber);
                        this.skip = ((Number(this.pageNumber) - 1) * Number(this.pageSize));
                    }
                }
            }
        });


        // this.setCurrentView();
        this.refresh();
        this.getAssetMenuConfig();
        this.setUserPreference();
    }

    

}
