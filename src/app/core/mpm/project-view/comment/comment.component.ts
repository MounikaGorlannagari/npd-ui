import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit,OnChanges {
  projectId: number;
  isManager: boolean;
  deliverableId: number;
  constructor(
    private activatedRoute: ActivatedRoute,
  ) {
  }
  ngOnChanges(changes: SimpleChanges): void {
    
  }
  readUrlParams(callback) {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.activatedRoute.parent.params.subscribe(parentRouteParams => {
        callback(parentRouteParams, queryParams);
      });
    });
  }
  ngOnInit(): void {
    this.projectId = null;
    this.readUrlParams((routeParams, queryParams) => {
      if (queryParams && routeParams) {
        this.projectId = routeParams.projectId;
        this.isManager = true;
        this.deliverableId = null;
      }
    });
  }

}
