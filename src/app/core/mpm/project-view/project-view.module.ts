import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectViewRoutingModule } from './project-view.routing.module';
import { OverviewComponent } from './overview/overview.component';
import { LayoutComponent } from './layout/layout.component';
import {
  MpmLibraryModule,
  TasksModule,
  DeliverableModule,
  PaginationModule,
  CommentsModule,
  ColumnChooserModule,
} from 'mpm-library';
import { MaterialModule } from '../../../material.module';
import { AssetsComponent } from './assets/assets.component';
import { AssetsVersionModalComponent } from '../shared/components/assets-version-modal/assets-version-modal.component';
import { AssetDetailsComponent } from '../shared/components/asset-details/asset-details.component';
import { CommentComponent } from './comment/comment.component';
import { AssetPreviewComponent } from '../shared/components/asset-preview/asset-preview.component';
import { MpmLibModule } from '../../mpm-lib/mpm-lib.module';
import { CollabModule } from '../../npd-ui/collab/collab.module';
import { ProjectCommentsComponent } from '../../npd-ui/npd-project-view/project-comments/project-comments.component';
import { NpdProjectOverviewComponent } from '../../npd-ui/npd-project-view/project-overview/npd-project-overview.component';
import { ProjectRequestDetailsComponent } from '../../npd-ui/npd-project-view/project-request-details/project-request-details.component';
import { RequestViewModule } from '../../npd-ui/request-view/request-view.module';
import { NpdCustomFieldComponent } from '../../npd-ui/npd-project-view/npd-custom-field/npd-custom-field.component';
import { NpdProjectViewModule } from '../../npd-ui/npd-project-view/npd-project-view.module';
import { NpdAssetsComponent } from '../../npd-ui/npd-project-view/npd-assets/npd-assets.component';

@NgModule({
  declarations: [
    OverviewComponent,
    LayoutComponent,
    AssetsComponent,
    AssetsVersionModalComponent,
    AssetDetailsComponent,
    CommentComponent,
    AssetPreviewComponent,
    AssetPreviewComponent,
    ProjectCommentsComponent,
    NpdProjectOverviewComponent,
    ProjectRequestDetailsComponent,
    NpdCustomFieldComponent,
    NpdAssetsComponent,
  ],
  imports: [
    CommonModule,
    ProjectViewRoutingModule,
    MpmLibraryModule,
    ColumnChooserModule,
    MaterialModule,
    TasksModule,
    DeliverableModule,
    PaginationModule,
    CommentsModule,
    MpmLibModule,
    CollabModule,
    RequestViewModule,
    NpdProjectViewModule,
  ],
  entryComponents: [],
})
export class ProjectViewModule {}
