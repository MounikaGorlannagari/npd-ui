import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  ElementRef,
} from '@angular/core';
import {
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { DateAdapter } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  LoaderService,
  TaskCreationComponent,
  ProjectConstant,
} from 'mpm-library';
import { NotificationService } from 'mpm-library';
import { ActivatedRoute, Router } from '@angular/router';
import { MPMTaskCreationComponent } from '../../../mpm-lib/project/tasks/task-creation/task-creation.component';
import { NpdTaskEditComponent } from 'src/app/core/npd-ui/npd-task-view/npd-task-edit/npd-task-edit.component';
import { NpdTaskCreationComponent } from 'src/app/core/npd-ui/npd-task-view/npd-task-creation/npd-task-creation.component';
import { NpdTaskService } from 'src/app/core/npd-ui/npd-task-view/services/npd-task.service';
import { RouteService } from '../../route.service';

@Component({
  selector: 'app-task-creation-modal',
  templateUrl: './task-creation-modal.component.html',
  styleUrls: ['./task-creation-modal.component.scss'],
})
export class TaskCreationModalComponent implements OnInit {
  @ViewChild(MPMTaskCreationComponent)
  taskCreationComponent: MPMTaskCreationComponent;
  @ViewChild(NpdTaskCreationComponent)
  npdTaskCreationComponent: MPMTaskCreationComponent;

  @ViewChild(NpdTaskEditComponent) npdTaskEditComponent: NpdTaskEditComponent;

  taskConfig = {
    projectId: null,
    isTemplate: false,
    taskId: null,
    projectObj: null,
    isApprovalTask: false,
    isTaskActive: false,
    router: '',
    taskType: null,
    totalData: null,
    selectedTask: null,
    allTask: null,
  };
  modalLabel;
  urlParams;

  taskHeaderData: any;
  taskDetail: any;
  taskName: string;
  manufacturingSite: string;

  constructor(
    private mpmRouteService: RouteService,
    public dialogRef: MatDialogRef<TaskCreationModalComponent>,
    @Inject(MAT_DIALOG_DATA) public taskData: any,
    private notificationService: NotificationService,
    private loaderService: LoaderService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private npdTaskService: NpdTaskService
  ) { }

  afterCloseProject(event) {
    this.dialogRef.close();
  }
  closeDialog() {
    if (this.taskCreationComponent) {
      this.taskCreationComponent.cancelTaskCreation();
    }
    if (this.npdTaskCreationComponent) {
      this.npdTaskCreationComponent.cancelTaskCreation();
    }
    if (this.npdTaskEditComponent) {
      this.npdTaskEditComponent.cancelTaskCreation();
    }
  }
  afterSaveProject(event) {
    if (event.saveOption && event.saveOption.value === 'SAVE') {
      this.dialogRef.close(true);
    } else {
      this.dialogRef.close(event.saveOption.value);
    }
  }
  notificationHandler(event) {
    if (event.message) {
      event.type === ProjectConstant.NOTIFICATION_LABELS.SUCCESS
        ? this.notificationService.success(event.message)
        : event.type === ProjectConstant.NOTIFICATION_LABELS.INFO
          ? this.notificationService.info(event.message)
          : this.notificationService.error(event.message);
    }
  }
  loadingHandler(event) {
    event ? this.loaderService.show() : this.loaderService.hide();
  }
  ngOnInit() {

    this.loadingHandler(true);
    this.taskConfig.totalData = this.taskData.taskConfig;
    this.taskConfig.allTask = this.taskData.alltask;
    this.taskConfig.selectedTask = this.taskData.selectedtask;
   
    this.taskConfig.isTemplate = this.taskData.isTemplate;
    this.taskConfig.projectId = this.taskData.projectId;
    this.taskConfig.taskId = this.taskData.taskId;
    this.taskConfig.projectObj = this.taskData.projectObj;
    this.taskConfig.isApprovalTask = this.taskData.isApprovalTask;
    this.taskConfig.isTaskActive =
      this.taskData?.isTaskActive == 'true' ? true : false;
    this.taskConfig.taskType = this.taskData.taskType;
    this.taskConfig.router = this.router.url;
    this.modalLabel = this.taskData.modalLabel;
    this.taskConfig.selectedTask = this.taskData?.selectedtask || this.taskData.selectedTask
    this.taskData.selectedTask = this.taskData.selectedTask

  }

  editTaskHeaderData(event: any) {
    
    this.taskHeaderData = event;
    this.npdTaskService.getEditTaskHeaderData().subscribe((taskDetail) => {
      this.taskDetail = taskDetail;
      if (this.taskDetail.TASK_NAME.includes('CP')) {
        this.taskName = this.taskDetail.TASK_NAME;
      } else {
        this.taskName = this.taskDetail.TASK_NAME.split(' ')[1];
      }
    });
    if (this.taskHeaderData.NPD_PROJECT_FINAL_MANUFACTURING_SITE && this.taskHeaderData.NPD_PROJECT_FINAL_MANUFACTURING_SITE !== 'NA') {
      this.manufacturingSite = this.taskHeaderData.NPD_PROJECT_FINAL_MANUFACTURING_SITE
    } else {
      this.manufacturingSite = this.taskHeaderData.PR_EARLY_PROJECT_MANUFACTURING_LOCATION_SITE
    }
  }
  onRowClick(projectId) {
    this.mpmRouteService.goToProjectDetailViewNewTab(projectId, false, null);
  }
}
