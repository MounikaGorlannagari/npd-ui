import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService, ProjectOverviewComponent } from 'mpm-library';
import { NotificationService } from 'mpm-library';
import { RouteService } from '../../route.service';
import { Observable } from 'rxjs';
import { ProjectConstant } from 'mpm-library';
import { MPMProjectOverviewComponent } from '../../../mpm-lib/project/project-overview/project-overview.component';
import { RoutingConstants } from '../../routingConstants';
import { StateService } from 'mpm-library';
import { MenuConstants } from '../../shared/constants/menu.constants';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss'],
    styles: [`
        :host {
            width:100%;
        }
    `]
})

export class OverviewComponent implements OnInit {
    @ViewChild(MPMProjectOverviewComponent, { static: true }) ProjectOverviewComponent: MPMProjectOverviewComponent;

    projectId;
    isTemplate;

    urlParams;
    isCampaign;
    campaignId;
    selectedCampaignId;
    constructor(
        private activatedRoute: ActivatedRoute,
        private loaderService: LoaderService,
        private notification: NotificationService,
        private mpmRouteService: RouteService,
        private router: Router,
        private stateService: StateService
    ) { }

    readUrlParams(callback) {
        this.activatedRoute.queryParams.subscribe(queryParams => {
            this.activatedRoute.parent.params.subscribe(parentRouteParams => {
                callback(parentRouteParams, queryParams);
            });
        });
    }

    afterCloseProject(event) { }

    canDeactivate(): Observable<boolean> {

        return new Observable(observer => {
            if (this.ProjectOverviewComponent
                && ((this.ProjectOverviewComponent.projectOverviewForm
                    && this.ProjectOverviewComponent.projectOverviewForm.dirty)
                    || ((this.ProjectOverviewComponent.customFieldsGroup
                        && this.ProjectOverviewComponent.customFieldsGroup.dirty)))
            ) {
                //this.loadingHandler(true);
                this.ProjectOverviewComponent.confirmExitOnEdit().subscribe(
                    choise => {
                        observer.next(choise);
                        observer.complete();
                    }
                );
            } else {
                observer.next(true);
                observer.complete();
            }
        });
    }
    afterSaveProject(event) {
        if (event.projectId) {
            this.mpmRouteService.goToProjectDetailView(event.projectId, event.isTemplate, this.selectedCampaignId ? this.selectedCampaignId : null);
        } else {
            this.mpmRouteService.goToCampaignDetailView(event.campaignId);
        }
    }

    restartProjectHandler() {
        const routeData = this.stateService.getRoute(MenuConstants.PROJECT_MANAGEMENT);
        const params = routeData ? routeData.params : null;
        this.stateService.resetState();
        if (params) {
            this.mpmRouteService.goToDashboard(params.queryParams.viewName ? params.queryParams.viewName : null, params.queryParams.isListView ? params.queryParams.isListView : null,
                params.queryParams.sortBy ? params.queryParams.sortBy : null, params.queryParams.sortOrder ? params.queryParams.sortOrder : null,
                params.queryParams.pageNumber ? params.queryParams.pageNumber : null, params.queryParams.pageSize ? params.queryParams.pageSize : null,
                params.queryParams.searchName ? params.queryParams.searchName : null, params.queryParams.savedSearchName ? params.queryParams.savedSearchName : null,
                params.queryParams.advSearchData ? params.queryParams.advSearchData : null, params.queryParams.facetData ? params.queryParams.facetData : null);
        } else {
            this.mpmRouteService.goToDashboard(null, null, null, null, null, null, null, null, null, null);
        }
    }

    notificationHandler(event) {
        if (event.message && event.type) {
            event.type === ProjectConstant.NOTIFICATION_LABELS.SUCCESS ? this.notification.success(event.message) :
                event.type === ProjectConstant.NOTIFICATION_LABELS.INFO ? this.notification.info(event.message) : this.notification.error(event.message);
        }
    }
    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }
    ngOnInit() {
        this.projectId = null;
        this.campaignId = null;
        this.readUrlParams((routeParams, queryParams) => {
            if (queryParams && routeParams) {
                this.projectId = routeParams.projectId;
                this.campaignId = routeParams.campaignId;
                this.selectedCampaignId = queryParams.campaignId;
                this.urlParams = queryParams;
                if (this.router.url.includes(RoutingConstants.campaignViewBaseRoute)) {
                    this.isCampaign = true;
                } else if (this.urlParams && this.urlParams.isTemplate && this.urlParams.isTemplate === 'true') {
                    this.isCampaign = false;
                    this.isTemplate = true;
                } else {
                    this.isCampaign = false;
                    this.isTemplate = false;
                }
            }
        });
    }
}
