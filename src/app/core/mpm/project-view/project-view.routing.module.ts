import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { OverviewComponent } from './overview/overview.component';
import { AssetsComponent } from './assets/assets.component';
import { CanDeactivateGuard } from 'mpm-library';
import { CommentComponent } from './comment/comment.component';
import { ManagerDashboardComponent } from '../project-management/manager-dashboard/manager-dashboard.component';
import { ProjectCommentsComponent } from '../../npd-ui/npd-project-view/project-comments/project-comments.component';
import { ProjectRequestDetailsComponent } from '../../npd-ui/npd-project-view/project-request-details/project-request-details.component';
import { NpdAssetsComponent } from '../../npd-ui/npd-project-view/npd-assets/npd-assets.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'Overview',
        component: OverviewComponent,
        canDeactivate: [CanDeactivateGuard],
      },
      {
        path: 'Projects',
        component: ManagerDashboardComponent,
      },
      {
        path: 'Tasks',
        loadChildren: () =>
          import('./task/task.module').then((m) => m.TaskModule),
      },
      {
        path: 'Tasks/:taskId',
        loadChildren: () =>
          import('./task/task-details/task-details.module').then(
            (m) => m.TaskDetailsModule
          ),
      },
      {
        path: 'Assets',
        component: NpdAssetsComponent, //using mpm component after merge
      },
      {
        path: 'Comments',
        component: ProjectCommentsComponent,
      },
      {
        path: 'charts',
        loadChildren: () =>
          import('./../../npd-ui/npd-gantt/npd-gantt.module').then(
            (m) => m.NpdGanttModule
          ),
      },
      {
        path: 'requestDetails',
        component: ProjectRequestDetailsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectViewRoutingModule {}
