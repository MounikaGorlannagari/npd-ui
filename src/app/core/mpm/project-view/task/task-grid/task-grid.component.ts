import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { SharingService } from 'mpm-library';
import { RouteService } from '../../../route.service';
import { MPMSwimLaneComponent } from '../../../../mpm-lib/project/tasks/swim-lane/swim-lane.component';
import { MPMTaskListViewComponent } from '../../../../mpm-lib/project/tasks/task-list-view/task-list-view.component';
import { ActivatedRoute } from '@angular/router';
import { StateService } from 'mpm-library';
import { NpdTaskListViewComponent } from 'src/app/core/npd-ui/npd-task-view/npd-task-list-view/npd-task-list-view.component';

@Component({
  selector: 'app-task-grid',
  templateUrl: './task-grid.component.html',
  styleUrls: ['./task-grid.component.scss'],
})
export class TaskGridComponent implements OnInit {
  @Input() taskConfig;
  @Input() displayableFields;
  @Output() refreshTaskGrid = new EventEmitter<any>();
  @Output() editTaskHandler = new EventEmitter<any>();
  @Output() sortChange = new EventEmitter<any>();
  @Output() customWorkflowHandler = new EventEmitter<any>();
  @Output() orderedDisplayableFields = new EventEmitter<any>();
  @Output() resizeDisplayableFields = new EventEmitter<any>();

  @ViewChild(MPMTaskListViewComponent)
  taskListViewComponent: MPMTaskListViewComponent;
  @ViewChild(NpdTaskListViewComponent)
  npdTaskListViewComponent: NpdTaskListViewComponent;
  @ViewChild(MPMSwimLaneComponent) swimLaneComponent: MPMSwimLaneComponent;

  appConfig: any;
  selectedCampaignId;

  queryParams;
  routeParams;

  constructor(
    private routeService: RouteService,
    private sharingService: SharingService,
    private activatedRoute: ActivatedRoute,
    private stateService: StateService
  ) {}

  refreshChild(taskConfig) {
    this.taskConfig = taskConfig;
    if (this.taskConfig.isTaskListView) {
      this.npdTaskListViewComponent.refreshData(this.taskConfig);
    } else {
      this.swimLaneComponent.initialiseSwimList();
    }
  }

  refreshTaskList(event) {
    this.refreshTaskGrid.next(event);
  }

  refreshSwimLine(event) {
    this.refreshTaskGrid.next(event);
  }

  openTaskDetails(eventData) {
   
    this.stateService.setRoute(
      this.queryParams.menuName,
      this.queryParams,
      this.routeParams
    );
    this.routeService.goToTaskDetails(
      eventData.isTemplate,
      this.taskConfig.projectId,
      this.taskConfig.taskId,
      this.taskConfig.isNewDeliverable,
      this.selectedCampaignId ? this.selectedCampaignId : null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }

  openCustomWorkflow(eventData) {
    this.customWorkflowHandler.next(eventData);
  }

  editTask(eventData) {
    this.editTaskHandler.next(eventData);
  }

  onSortChange(eventData): void {
    this.sortChange.next(eventData);
  }

  getOrderedDisplayableFields(displayableFields) {
    this.orderedDisplayableFields.next(displayableFields);
  }

  resizingDisplayableFields() {
    this.resizeDisplayableFields.next();
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.selectedCampaignId = params.campaignId;
      this.queryParams = params;
    });
    if (
      this.activatedRoute.parent &&
      this.activatedRoute.parent.parent &&
      this.activatedRoute.parent.parent.params
    ) {
      this.activatedRoute.parent.parent.params.subscribe((params) => {
        this.routeParams = params;
      });
    }
  }
}
