import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskComponent } from './task.component';
import { LayoutComponent } from '../layout/layout.component';
import { ManagerDashboardComponent } from '../../project-management/manager-dashboard/manager-dashboard.component';



const routes: Routes = [
    {
        path: '',
        component: TaskComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TaskRoutingModule { }
