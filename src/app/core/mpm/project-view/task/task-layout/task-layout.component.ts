import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import {
  ProjectService,
  SharingService,
  TaskConstants,
  NotificationService,
  TaskService,
  ProjectConstant,
  CategoryService,
  EntityAppDefService,
  MPM_LEVELS,
  OTMMService,
  LoaderService,
  OtmmMetadataService,
  SearchChangeEventService,
  SearchConfigService,
  SearchConfigConstants,
  SearchDataService,
  ViewConfigService,
  ViewConfig,
  StatusLevels,
  FieldConfigService,
  MPMField,
  SearchRequest,
  IndexerService,
  SearchResponse,
  MPMFieldConstants,
  MPMFieldKeys,
  MPMSearchOperators,
  AppService,
} from 'mpm-library';
import { Observable } from 'rxjs';
import { TaskCreationModalComponent } from '../../task-creation-modal/task-creation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { TaskGridComponent } from '../task-grid/task-grid.component';
import { TaskTypes } from 'mpm-library';
import { ActivatedRoute } from '@angular/router';
import { RouteService } from '../../../route.service';
import { MPMSearchOperatorNames } from 'mpm-library';
import { ApplicationConfigConstants } from 'mpm-library';
import { UtilService } from 'mpm-library';
import { MPMTaskToolbarComponent } from '../../../../mpm-lib/project/tasks/task-toolbar/task-toolbar.component';
import { CustomWorkflowModalComponent } from '../../../shared/components/custom-workflow-modal/custom-workflow-modal.component';
import { IndexerDisplayFields } from 'mpm-library';
import { GloabalConfig as config } from 'mpm-library';
@Component({
  selector: 'app-task-layout',
  templateUrl: './task-layout.component.html',
  styleUrls: ['./task-layout.component.scss'],
})
export class TaskLayoutComponent implements OnInit, OnDestroy {
  @Input() taskConfig;
  public currentProjectDetails:any;

  skip = 0;
  top = 100;
  tempsearchConditionsSubscribe: any;
  searchConditionList: Array<any> = [];
  searchConfigId = -1;
  appConfig: any;
  enableCustomWorkflow: any;

  selectedCampaignId;
  selectedSortByParam;
  selectedSortOrderParam;

  searchName;
  savedSearchName;
  advSearchData;
  isPageRefresh = true;

  columnChooserFields = [];
  displayableFields;
  enableIndexerFieldRestriction;
  maximumFieldsAllowed;

  constructor(
    public projectService: ProjectService,
    public sharingService: SharingService,
    public taskService: TaskService,
    public entityAppDefService: EntityAppDefService,
    public otmmService: OTMMService,
    public notificationService: NotificationService,
    public dialog: MatDialog,
    public loaderService: LoaderService,
    public searchDataService: SearchDataService,
    public searchChangeEventService: SearchChangeEventService,
    public activatedRoute: ActivatedRoute,
    public routeService: RouteService,
    public viewConfigService: ViewConfigService,
    public fieldConfigService: FieldConfigService,
    public indexerService: IndexerService,
    public utilService: UtilService,
    public appService: AppService
  ) {}

  @ViewChild(TaskGridComponent) taskGridComponent: TaskGridComponent;
  @ViewChild(MPMTaskToolbarComponent)
  thaskToolbarComponent: MPMTaskToolbarComponent;

  resetView() {
    sessionStorage.removeItem(
      'USER_PREFERENCE-' + this.taskConfig.viewConfig.VIEW
    );
    const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(
      this.taskConfig.viewConfig['MPM_View_Config-id'].Id
    );
    const userPreferenceId =
      userPreference &&
      userPreference['MPM_User_Preference-id'] &&
      userPreference['MPM_User_Preference-id'].Id
        ? userPreference['MPM_User_Preference-id'].Id
        : null;
    if (userPreferenceId) {
      this.loaderService.show();
      this.appService
        .deleteUserPreference(userPreferenceId)
        .subscribe((response) => {
          this.loaderService.hide();
          if (response) {
            this.notificationService.success(
              'User preference have been reset successfully'
            );
          }
        });
    } else {
      this.notificationService.success(
        'User preference have been reset successfully'
      );
    }
    this.resetCurrentView();
  }

  resetCurrentView() {
    this.taskConfig.viewConfig.displayFields.forEach((displayField) => {
      displayField.width = '';
    });
    this.taskConfig.viewConfig.listFieldOrder =
      this.taskConfig.viewConfig.LIST_FIELD_ORDER;
    this.displayableFields = this.taskConfig.viewConfig.displayFields;
    if (this.enableIndexerFieldRestriction) {
      this.displayableFields = this.displayableFields.slice(
        0,
        this.maximumFieldsAllowed
      );
    }
    this.columnChooserFields = this.formColumnChooserField(
      this.taskConfig.viewConfig.displayFields
    );
  }

  storeViewData() {
    const userPreference = {
      USER_ID: this.sharingService.getCurrentPerson().Id,
      PREFERENCE_TYPE: 'VIEW',
      VIEW_ID: this.taskConfig.viewConfig['MPM_View_Config-id'].Id,
      FIELD_DATA: [],
    };
    this.taskConfig.viewConfig.displayFields.forEach((viewField) => {
      if (viewField.INDEXER_FIELD_ID) {
        const fieldIndex = this.displayableFields.findIndex(
          (displayableField) =>
            displayableField.INDEXER_FIELD_ID === viewField.INDEXER_FIELD_ID
        );
        userPreference.FIELD_DATA.push({
          [viewField.INDEXER_FIELD_ID]: {
            POSITION: fieldIndex >= 0 ? fieldIndex : null,
            WIDTH: viewField && viewField.width ? viewField.width : '',
            IS_DISPLAY: fieldIndex >= 0,
          },
        });
      }
    });
    sessionStorage.setItem(
      'USER_PREFERENCE-' + this.taskConfig.viewConfig.VIEW,
      JSON.stringify(userPreference)
    );
  }

  storeCurrentViewData() {
    const userPreference = {
      USER_ID: this.sharingService.getCurrentPerson().Id,
      PREFERENCE_TYPE: 'VIEW',
      VIEW_ID: this.taskConfig.viewConfig['MPM_View_Config-id'].Id,
      FIELD_DATA: [],
    };
    this.taskConfig.viewConfig.displayFields.forEach((viewField) => {
      if (viewField.INDEXER_FIELD_ID) {
        const fieldIndex = this.displayableFields.findIndex(
          (displayableField) =>
            displayableField.INDEXER_FIELD_ID === viewField.INDEXER_FIELD_ID
        );
        userPreference.FIELD_DATA.push({
          [viewField.INDEXER_FIELD_ID]: {
            POSITION: fieldIndex >= 0 ? fieldIndex : null,
            WIDTH: viewField && viewField.width ? viewField.width : '',
            IS_DISPLAY: fieldIndex >= 0,
          },
        });
      }
    });
    sessionStorage.setItem(
      'CURRENT_USER_PREFERENCE-' + this.taskConfig.viewConfig.VIEW,
      JSON.stringify(userPreference)
    );
  }

  saveDisplayColumns(columnFields) {
    /* this.updateDisplayColumns(columnFields);
        const requestObj = {
            R_PO_PERSON: {
                'Person-id': {
                    Id: this.sharingService.getCurrentPerson().Id
                }
            },
            PREFERENCE_TYPE: 'VIEW',
            R_PO_MPM_VIEW_CONFIG: {
                'MPM_View_Config-id': {
                    Id: this.taskConfig.viewConfig['MPM_View_Config-id'].Id
                }
            },
            FIELD_DATA: JSON.stringify(this.userPreference.FIELD_DATA)
        };
        this.loaderService.show();
        const existingUserPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.taskConfig.viewConfig['MPM_View_Config-id'].Id);
        const existingUserPreferenceId = existingUserPreference && existingUserPreference['MPM_User_Preference-id'] && existingUserPreference['MPM_User_Preference-id'].Id ?
            existingUserPreference['MPM_User_Preference-id'].Id : null;
        this.appService.updateUserPreference(requestObj, existingUserPreferenceId).subscribe(response => {
            this.loaderService.hide();
            if (response) {
                this.notificationService.success('User preference have been saved successfully');
            }
        }); */
  }

  updateDisplayColumns(displayColumnFields) {
    this.displayableFields = [];
    const listFieldOrder = [];

      displayColumnFields.column.sort((a, b) => a.POSITION - b.POSITION);
      displayColumnFields.column.forEach((displayColumnField) => {
        if (displayColumnField.selected === true) {
          const selectedField = this.taskConfig.viewConfig.displayFields.find(
            (field) =>
              field.INDEXER_FIELD_ID === displayColumnField.INDEXER_FIELD_ID
          );
          if (selectedField) {
            this.displayableFields.push(selectedField);
            listFieldOrder.push(selectedField.MAPPER_NAME);
          }
        }
      });
    this.taskConfig.viewConfig.listFieldOrder = listFieldOrder.toString();
    this.storeViewData();

    // commented below code to restrict refresh call on columns switch in task-list-view

    // if (this.enableIndexerFieldRestriction) {
    //   this.taskService.refreshData.subscribe(() => {
    //     this.getTasks();
    //   });
    // }
  }

  formColumnChooserField(listFields) {
    const columnChooserFields = [];
    this.taskConfig.viewConfig.displayFields.forEach((displayField, index) => {
      const selectedFieldIndex = listFields.findIndex(
        (listField) =>
          listField.INDEXER_FIELD_ID === displayField.INDEXER_FIELD_ID
      );
      if (this.enableIndexerFieldRestriction) {
        if (
          selectedFieldIndex >= 0 &&
          selectedFieldIndex < this.maximumFieldsAllowed
        ) {
          columnChooserFields.push({
            INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
            DISPLAY_NAME: displayField.DISPLAY_NAME,
            POSITION: selectedFieldIndex,
            selected: true,
          });
        } else {
          columnChooserFields.push({
            INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
            DISPLAY_NAME: displayField.DISPLAY_NAME,
            POSITION: index,
            selected: false,
          });
        }
      } else {
        if (selectedFieldIndex >= 0) {
          columnChooserFields.push({
            INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
            DISPLAY_NAME: displayField.DISPLAY_NAME,
            POSITION: selectedFieldIndex,
            selected: true,
          });
        } else {
          columnChooserFields.push({
            INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
            DISPLAY_NAME: displayField.DISPLAY_NAME,
            POSITION: index,
            selected: false,
          });
        }
      }
    });
    return columnChooserFields;
  }

  getOrderedDisplayableFields(displayableFields) {
    this.columnChooserFields = this.formColumnChooserField(displayableFields);
    this.updateDisplayColumns({'column':this.columnChooserFields,'frezeCount':0});
  }

  resizeDisplayableFields() {
    this.storeViewData();
  }

  setUserPreference() {
    let userPreferenceSessionData: any = sessionStorage.getItem(
      'USER_PREFERENCE-' + this.taskConfig.viewConfig.VIEW
    );
    
    userPreferenceSessionData = userPreferenceSessionData
      ? JSON.parse(userPreferenceSessionData)
      : null;
    const userPreferenceViewField =
      userPreferenceSessionData && userPreferenceSessionData.FIELD_DATA
        ? userPreferenceSessionData.FIELD_DATA
        : null;
    if (userPreferenceViewField) {
      userPreferenceViewField.sort((a, b) => {
        const firstKey: any = Object.keys(a);
        const firstValue = a[firstKey];
        const secondKey: any = Object.keys(b);
        const secondValue = b[secondKey];
        return firstValue.POSITION - secondValue.POSITION;
      });
      const displayableFields = [];
      const listFieldOrder = [];
      userPreferenceViewField.forEach((viewField) => {
        const viewFieldId = Object.keys(viewField)[0];
        if (viewField[viewFieldId].IS_DISPLAY === true) {
          const selectedField = this.taskConfig.viewConfig.displayFields.find(
            (field) => field.INDEXER_FIELD_ID === viewFieldId
          );
          if (selectedField) {
            selectedField.width = viewField[viewFieldId].WIDTH;
            displayableFields.push(selectedField);
            listFieldOrder.push(selectedField.MAPPER_NAME);
          }
        }
      });
      this.taskConfig.viewConfig.listFieldOrder = listFieldOrder.toString();
      this.displayableFields = displayableFields;
      this.columnChooserFields = this.formColumnChooserField(displayableFields);
    } else {
      const userPreference =
        this.sharingService.getCurrentUserPreferenceByViewId(
          this.taskConfig.viewConfig['MPM_View_Config-id'].Id
        );
      
      const currentViewFieldData =
        userPreference &&
        !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA)
          ? JSON.parse(userPreference.FIELD_DATA).data
          : null;
      if (
        currentViewFieldData &&
        this.utilService.getBooleanValue(userPreference.IS_LIST_VIEW)
      ) {
        currentViewFieldData.sort((a, b) => {
          const firstKey: any = Object.keys(a);
          const firstValue = a[firstKey];
          const secondKey: any = Object.keys(b);
          const secondValue = b[secondKey];
          return firstValue.POSITION - secondValue.POSITION;
        });
        const displayableFields = [];
        const listFieldOrder = [];
        currentViewFieldData.forEach((viewField) => {
          const viewFieldId = Object.keys(viewField)[0];
          if (viewField[viewFieldId].IS_DISPLAY === true) {
            const selectedField = this.taskConfig.viewConfig.displayFields.find(
              (field) => field.INDEXER_FIELD_ID === viewFieldId
            );
            if (selectedField) {
              selectedField.width = viewField[viewFieldId].WIDTH;
              displayableFields.push(selectedField);
              listFieldOrder.push(selectedField.MAPPER_NAME);
            }
          }
        });
        this.taskConfig.viewConfig.listFieldOrder = listFieldOrder.toString();
        this.displayableFields = displayableFields;
        this.columnChooserFields =
          this.formColumnChooserField(displayableFields);
      } else {
        this.taskConfig.viewConfig.listFieldOrder =
          this.taskConfig.viewConfig.LIST_FIELD_ORDER;
        this.displayableFields = this.taskConfig.viewConfig.displayFields;
        this.columnChooserFields = this.formColumnChooserField(
          this.taskConfig.viewConfig.displayFields
        );
      }
    }
    if (this.enableIndexerFieldRestriction) {
      this.displayableFields = this.displayableFields.slice(
        0,
        this.maximumFieldsAllowed
      );
    }
  }

  setCurrentView() {
    const currentViewData = {
      sortName: this.taskConfig.isTaskListView
        ? this.taskConfig.selectedTaskListSortOption.option.indexerId
        : this.taskConfig.selectedTaskCardSortOption.option.indexerId,
      sortOrder: this.taskConfig.isTaskListView
        ? this.taskConfig.taskListSortOrder
        : this.taskConfig.taskCardSortOrder,
      pageNumber: null,
      pageSize: null,
      isListView: this.taskConfig.isTaskListView,
      viewId: this.taskConfig.viewConfig['MPM_View_Config-id'].Id,
      viewName: this.taskConfig.viewConfig.VIEW,
    };
    sessionStorage.setItem('CURRENT_VIEW', JSON.stringify(currentViewData));
  }

  getMPMFields(): Observable<any> {
    let viewConfig;
    return new Observable((observer) => {
      this.loaderService.show();
      if (this.taskConfig && this.taskConfig.taskViewName) {
        viewConfig = this.taskConfig.taskViewName; // this.sharingService.getViewConfigById(this.taskConfig.taskViewId).VIEW;
      } else {
        viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
      }
      this.viewConfigService
        .getAllDisplayFeildForViewConfig(viewConfig)
        .subscribe(
          (viewDetails: ViewConfig) => {
            if (!this.taskConfig.viewConfig) {
              this.taskConfig.viewConfig = viewDetails;
              this.updateSearchConfigId();
            }
            this.taskConfig.viewConfig = viewDetails;
            // this.taskConfig.viewConfig.displayFields = this.taskConfig.isTaskListView ? viewDetails.R_PM_LIST_VIEW_MPM_FIELDS : viewDetails.R_PM_CARD_VIEW_MPM_FIELDS;
            this.taskConfig.viewConfig.displayFields =
              viewDetails.R_PM_LIST_VIEW_MPM_FIELDS;
            this.taskConfig.metaDataFields =
              this.fieldConfigService.getFieldsbyFeildCategoryLevel(
                MPM_LEVELS.TASK
              );

            const taskListDataSet =
              this.fieldConfigService.formrequiredDataForLoadView(
                viewDetails,
                true
              );
            const taskCardDataSet =
              this.fieldConfigService.formrequiredDataForLoadView(
                viewDetails,
                false
              );

            this.setUserPreference();
            const taskListViewArray = viewDetails.R_PM_LIST_VIEW_MPM_FIELDS
              ? viewDetails.R_PM_LIST_VIEW_MPM_FIELDS
              : [];
            const taskCardViewArray = viewDetails.R_PM_CARD_VIEW_MPM_FIELDS
              ? viewDetails.R_PM_CARD_VIEW_MPM_FIELDS
              : [];
            const userPreference =
              this.sharingService.getCurrentUserPreferenceByViewId(
                this.taskConfig.viewConfig['MPM_View_Config-id'].Id
              );
            const userPreferenceFieldData =
              userPreference &&
              !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA)
                ? JSON.parse(userPreference.FIELD_DATA).data
                : null;
            this.taskConfig.taskCardSortOptions =
              taskCardDataSet.SORTABLE_FIELDS;
            this.taskConfig.taskListSortOptions =
              taskListDataSet.SORTABLE_FIELDS;
            if (this.taskConfig.taskCardSortOptions.length > 0) {
              let selectedSort;
              if (this.selectedSortByParam && !this.taskConfig.isTaskListView) {
                selectedSort = this.taskConfig.taskCardSortOptions.find(
                  (sort) => sort.displayName === this.selectedSortByParam
                );
              }
              if (selectedSort) {
                this.taskConfig.selectedTaskCardSortOption = {
                  option: selectedSort,
                  sortType: this.selectedSortOrderParam
                    ? this.selectedSortOrderParam
                    : this.appConfig[
                        ApplicationConfigConstants.MPM_APP_CONFIG
                          .DEFAULT_SORT_ORDER
                      ] === 'ASC'
                    ? 'asc'
                    : 'desc',
                };
                this.taskConfig.taskCardSortFieldName =
                  this.taskConfig.selectedTaskCardSortOption.option.name;
                this.taskConfig.taskCardSortOrder =
                  this.taskConfig.selectedTaskCardSortOption.sortType;
              } else {
                if (
                  userPreferenceFieldData &&
                  (userPreference.IS_LIST_VIEW === false ||
                    userPreference.IS_LIST_VIEW === 'false')
                ) {
                  userPreferenceFieldData.forEach((field) => {
                    const fieldId = Object.keys(field)[0];
                    if (field[fieldId].IS_SORTABLE === true) {
                      const selectedSortOption =
                        this.taskConfig.taskCardSortOptions.find(
                          (field) => field.indexerId === fieldId
                        );
                      if (selectedSortOption) {
                        this.taskConfig.selectedTaskCardSortOption = {
                          option: selectedSortOption,
                          sortType: field[fieldId].SORT_ORDER
                            ? field[fieldId].SORT_ORDER
                            : this.appConfig[
                                ApplicationConfigConstants.MPM_APP_CONFIG
                                  .DEFAULT_SORT_ORDER
                              ] === 'ASC'
                            ? 'asc'
                            : 'desc',
                        };
                        this.taskConfig.taskCardSortFieldName =
                          this.taskConfig.selectedTaskCardSortOption.option.name;
                        this.taskConfig.taskCardSortOrder =
                          this.taskConfig.selectedTaskCardSortOption.sortType;
                      }
                    }
                  });
                }
              }
              if (
                !(
                  this.taskConfig.selectedTaskCardSortOption &&
                  this.taskConfig.selectedTaskCardSortOption.option &&
                  this.taskConfig.selectedTaskCardSortOption.option.name
                )
              ) {
                this.taskConfig.selectedTaskCardSortOption = {
                  option: this.taskConfig.taskCardSortOptions[0],
                  sortType:
                    this.appConfig[
                      ApplicationConfigConstants.MPM_APP_CONFIG
                        .DEFAULT_SORT_ORDER
                    ] === 'ASC'
                      ? 'asc'
                      : 'desc',
                };
                this.taskConfig.selectedTaskCardSortOption =
                  taskCardDataSet.DEFAULT_SORT_FIELD
                    ? taskCardDataSet.DEFAULT_SORT_FIELD
                    : this.taskConfig.selectedTaskCardSortOption;
                this.taskConfig.taskCardSortFieldName =
                  this.taskConfig.selectedTaskCardSortOption.option.name;
                this.taskConfig.taskCardSortOrder =
                  this.taskConfig.selectedTaskCardSortOption.sortType;
              }
            }
            if (this.taskConfig.taskListSortOptions.length > 0) {
              let selectedSort;
              if (this.selectedSortByParam && this.taskConfig.isTaskListView) {
                selectedSort = this.taskConfig.taskListSortOptions.find(
                  (sort) => sort.displayName === this.selectedSortByParam
                );
              }
              if (selectedSort) {
                this.taskConfig.selectedTaskListSortOption = {
                  option: selectedSort,
                  sortType: this.selectedSortOrderParam
                    ? this.selectedSortOrderParam
                    : this.appConfig[
                        ApplicationConfigConstants.MPM_APP_CONFIG
                          .DEFAULT_SORT_ORDER
                      ] === 'ASC'
                    ? 'asc'
                    : 'desc',
                };
                this.taskConfig.taskListSortFieldName =
                  this.taskConfig.selectedTaskListSortOption.option.name;
                this.taskConfig.taskListSortOrder =
                  this.taskConfig.selectedTaskListSortOption.sortType;
              } else {
                if (
                  userPreferenceFieldData &&
                  (userPreference.IS_LIST_VIEW === true ||
                    userPreference.IS_LIST_VIEW === 'true')
                ) {
                  userPreferenceFieldData.forEach((field) => {
                    const fieldId = Object.keys(field)[0];
                    if (field[fieldId].IS_SORTABLE === true) {
                      const selectedSortOption =
                        this.taskConfig.taskListSortOptions.find(
                          (field) => field.indexerId === fieldId
                        );
                      if (selectedSortOption) {
                        this.taskConfig.selectedTaskListSortOption = {
                          option: selectedSortOption,
                          sortType: field[fieldId].SORT_ORDER
                            ? field[fieldId].SORT_ORDER
                            : this.appConfig[
                                ApplicationConfigConstants.MPM_APP_CONFIG
                                  .DEFAULT_SORT_ORDER
                              ] === 'ASC'
                            ? 'asc'
                            : 'desc',
                        };
                        this.taskConfig.taskListSortFieldName =
                          this.taskConfig.selectedTaskListSortOption.option.name;
                        this.taskConfig.taskListSortOrder =
                          this.taskConfig.selectedTaskListSortOption.sortType;
                      }
                    }
                  });
                }
              }
              if (
                !(
                  this.taskConfig.selectedTaskListSortOption &&
                  this.taskConfig.selectedTaskListSortOption.option &&
                  this.taskConfig.selectedTaskListSortOption.option.name
                )
              ) {
                this.taskConfig.selectedTaskListSortOption = {
                  option: this.taskConfig.taskListSortOptions[0],
                  sortType:
                    this.appConfig[
                      ApplicationConfigConstants.MPM_APP_CONFIG
                        .DEFAULT_SORT_ORDER
                    ] === 'ASC'
                      ? 'asc'
                      : 'desc',
                };
                this.taskConfig.selectedTaskListSortOption =
                  taskListDataSet.DEFAULT_SORT_FIELD
                    ? taskListDataSet.DEFAULT_SORT_FIELD
                    : this.taskConfig.selectedTaskListSortOption;
                this.taskConfig.taskListSortFieldName =
                  this.taskConfig.selectedTaskListSortOption.option.name;
                this.taskConfig.taskListSortOrder =
                  this.taskConfig.selectedTaskListSortOption.sortType;
              }
            }
            this.storeCurrentViewData();
            this.setCurrentView();
            if (this.taskConfig.taskStatus.length === 0) {
              this.entityAppDefService
                .getStatusByCategoryLevel(MPM_LEVELS.TASK)
                .subscribe(
                  (statusResponse) => {
                    if (statusResponse) {
                      this.taskConfig.taskStatus = statusResponse;
                    }
                    observer.next(true);
                    observer.complete();
                    this.loaderService.hide();
                  },
                  (error) => {
                    observer.error(error);
                    this.loaderService.hide();
                  }
                );
            } else {
              observer.next(true);
              observer.complete();
              this.loaderService.hide();
            }
          },
          (metadataError) => {
            observer.error(metadataError);
          }
        );
    });
  }

  getProjectDetails(): Observable<any> {
    return new Observable((observer) => {
      if (this.taskConfig.selectedProject) {
        observer.next(true);
        observer.complete();
        return;
      }
      this.projectService
        .getProjectById(this.taskConfig.projectId)
        .subscribe((projectObj) => {
      
          this.currentProjectDetails=projectObj;
          if (projectObj) {
            if (
              projectObj.R_PO_CATEGORY &&
              projectObj.R_PO_CATEGORY['MPM_Category-id']
            ) {
              this.taskConfig.categoryId =
                projectObj.R_PO_CATEGORY['MPM_Category-id'].Id;
            }
            if (
              projectObj.R_PO_STATUS &&
              projectObj.R_PO_STATUS['MPM_Status-id']
            ) {
              this.taskConfig.projectStatusId =
                projectObj.R_PO_STATUS['MPM_Status-id'].Id;
            }
            const currStatusName = this.projectService.getCurrProjectStatus(
              this.taskConfig.projectStatusId
            );
            this.taskConfig.isReadOnly =
              currStatusName === StatusLevels.FINAL ? true : false;
            this.taskConfig.OTMMFolderID = projectObj.OTMM_FOLDER_ID;
            this.taskConfig.OTMMWorkflowFolderID =
              projectObj.OTMM_WORKFLOW_FOLDER_ID;
            this.taskConfig.taskCardSortFieldName = '';
            this.taskConfig.taskListSortFieldName = '';
            this.taskConfig.taskCardSortOrder =
              this.appConfig[
                ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER
              ] === 'ASC'
                ? 'asc'
                : 'desc';
            this.taskConfig.taskListSortOrder =
              this.appConfig[
                ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER
              ] === 'ASC'
                ? 'asc'
                : 'desc';

            this.taskConfig.isOnHoldProject = projectObj.IS_ON_HOLD;

            this.taskConfig.selectedProject = projectObj;

            if (this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW === 'true') {
              this.appService.getTaskEvents().subscribe((response) => {
                this.sharingService.setTaskEvents(
                  response && response.MPM_Events ? response.MPM_Events : null
                );
              });
              this.appService.getTaskWorkflowActions().subscribe((response) => {
                this.sharingService.setTaskWorkflowActions(
                  response && response.MPM_Workflow_Actions
                    ? response.MPM_Workflow_Actions
                    : null
                );
              });
              this.taskService
                .getActionRulesByProjectId(this.taskConfig.projectId)
                .subscribe((response) => {
                  if (response) {
                    this.taskConfig.workflowActionRules = response;
                  } else {
                    this.taskConfig.workflowActionRules = [];
                  }
                });
            }
            this.taskConfig.projectItemId = projectObj['Project-id'].ItemId;
            this.projectService.setSelectedProjectDetails(projectObj);
            const currentUSerID = this.sharingService.getCurrentUserItemID();
            if (
              currentUSerID &&
              this.taskConfig.selectedProject &&
              currentUSerID ===
                this.taskConfig.selectedProject.R_PO_PROJECT_OWNER[
                  'Identity-id'
                ].Id
            ) {
              this.taskConfig.isProjectOwner = true;
            }
            this.taskConfig.taskTotalCount = 0;
            this.getMPMFields().subscribe(
              () => {
                observer.next(true);
                observer.complete();
              },
              () => {
                this.notificationService.error(
                  'Somthing went wrong while getting Task List'
                );
              }
            );
          }
        });
    });
  }

  openCustomWorkflow(eventData?) {
    const dialogRef = this.dialog.open(CustomWorkflowModalComponent, {
      width: '100%',
      // minHeight: '15vh',
      // maxHeight: '100vh',
      minHeight: '25%',
      maxHeight: '100%',
      disableClose: true,
      data: {
        currentTaskData: eventData ? eventData : null,
        taskData: this.taskConfig.taskList,
        isTemplate: this.taskConfig.isTemplate,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.taskService
          .getActionRulesByProjectId(this.taskConfig.projectId)
          .subscribe((response) => {
            this.taskConfig.workflowActionRules = response;
          });
      }
    });
  }

  createNewTask(eventData) {
    const dialogRef = this.dialog.open(TaskCreationModalComponent, {
      width: '38%',
      disableClose: true,
      data: {
        projectId: eventData.projectId,
        isTemplate: eventData.isTemplate,
        projectObj: this.taskConfig.selectedProject,
        modalLabel: 'Add Task',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.initializeLayout();
      }
    });
  }

  createNewTaskByType(eventData) {
    if (eventData && eventData.projectId && eventData.taskCreationType) {
      const objectData = {
        projectId: eventData.projectId,
        isTemplate: eventData.isTemplate,
        projectObj: this.taskConfig.selectedProject,
        isApprovalTask: false,
        taskType: TaskTypes.NORMAL_TASK,
        modalLabel: 'Add Task',
      };
      if (
        TaskTypes.NORMAL_TASK === eventData.taskCreationType ||
        TaskTypes.APPROVAL_TASK === eventData.taskCreationType
      ) {
        objectData.isApprovalTask =
          eventData.taskCreationType !== TaskTypes.NORMAL_TASK ? true : false;
        objectData.taskType = eventData.taskCreationType;
      } else {
        objectData.isApprovalTask = false;
        objectData.taskType = TaskTypes.TASK_WITHOUT_DELIVERABLE;
      }
      const dialogRef = this.dialog.open(TaskCreationModalComponent, {
        width: '40%',
        disableClose: true,
        data: objectData,
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.initializeLayout();
          if (result === 'CREATE_ANOTHER') {
            this.reopenCreateTask(eventData);
          }
        }
      });
    }
  }

  editTask(eventData) {
    this.taskConfig.isEditTask = true;
    const sortOption = this.taskConfig.isTaskListView
      ? this.taskConfig.selectedTaskListSortOption &&
        this.taskConfig.selectedTaskListSortOption.option &&
        this.taskConfig.selectedTaskListSortOption.option.displayName
        ? this.taskConfig.selectedTaskListSortOption.option.displayName
        : null
      : this.taskConfig.selectedTaskCardSortOption &&
        this.taskConfig.selectedTaskCardSortOption.option &&
        this.taskConfig.selectedTaskCardSortOption.option.displayName
      ? this.taskConfig.selectedTaskCardSortOption.option.displayName
      : null;
    const sortOrder = this.taskConfig.isTaskListView
      ? this.taskConfig.selectedTaskListSortOption &&
        this.taskConfig.selectedTaskListSortOption.option &&
        this.taskConfig.selectedTaskListSortOption.option.displayName &&
        this.taskConfig.taskListSortOrder
        ? this.taskConfig.taskListSortOrder
        : null
      : this.taskConfig.selectedTaskCardSortOption &&
        this.taskConfig.selectedTaskCardSortOption.option &&
        this.taskConfig.selectedTaskCardSortOption.option.displayName &&
        this.taskConfig.taskCardSortOrder
      ? this.taskConfig.taskCardSortOrder
      : null;
    this.routeService.goToTaskView(
      this.taskConfig.isTemplate,
      this.taskConfig.projectId,
      this.taskConfig.isTaskListView,
      this.taskConfig.isEditTask,
      eventData.taskId,
      this.selectedCampaignId ? this.selectedCampaignId : null,
      sortOption,
      sortOrder,
      this.taskConfig.selectedResources &&
        this.taskConfig.selectedResources.length > 0
        ? this.taskConfig.selectedResources.toString()
        : null,
      this.searchName ? this.searchName : null,
      this.savedSearchName ? this.savedSearchName : null,
      this.advSearchData ? this.advSearchData : null
    );
    const dialogRef = this.dialog.open(TaskCreationModalComponent, {
      width: '38%',
      disableClose: true,
      data: {
        projectId: eventData.projectId,
        isTemplate: eventData.isTemplate,
        taskId: eventData.taskId,
        projectObj: this.taskConfig.selectedProject,
        isApprovalTask: eventData.isApprovalTask,
        isTaskActive: eventData?.taskList?.find(task=>task.ID == eventData.taskId)?.IS_ACTIVE_TASK,
        modalLabel: 'Edit Task',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.taskConfig.isEditTask = false;
      this.routeService.goToTaskView(
        this.taskConfig.isTemplate,
        this.taskConfig.projectId,
        this.taskConfig.isTaskListView,
        this.taskConfig.isEditTask,
        null,
        this.selectedCampaignId ? this.selectedCampaignId : null,
        sortOrder,
        sortOption,
        this.taskConfig.selectedResources &&
          this.taskConfig.selectedResources.length > 0
          ? this.taskConfig.selectedResources.toString()
          : null,
        this.searchName ? this.searchName : null,
        this.savedSearchName ? this.savedSearchName : null,
        this.advSearchData ? this.advSearchData : null
      );
      if (result) {
        this.getTasks();
      }
    });
  }

  refreshTaskGrid(eventData?, editData?) {
    if (this.taskConfig) {
      this.getProjectDetails().subscribe(() => {
        this.getTasks(editData);
      });
    }
  }

  toolBarConfigChange(taskConfig) {
    if (taskConfig.isSortChange) {
      this.selectedSortByParam =
        this.taskConfig.selectedTaskCardSortOption.option &&
        this.taskConfig.selectedTaskCardSortOption.option.displayName
          ? this.taskConfig.selectedTaskCardSortOption.option.displayName
          : null;
      this.selectedSortOrderParam =
        this.taskConfig.selectedTaskCardSortOption.option &&
        this.taskConfig.selectedTaskCardSortOption.option.displayName &&
        this.taskConfig.taskCardSortOrder
          ? this.taskConfig.taskCardSortOrder
          : null;
      taskConfig.isSortChange = null;
    }
    this.taskConfig = taskConfig;
    this.getTasks();
    this.routeComponentView();
    this.setCurrentView();
  }

  onSortChange(eventData) {
    if (eventData && eventData.selectedSortItem && eventData.sortType) {
      if (this.taskConfig.isTaskListView) {
        this.taskConfig.taskListSortFieldName = eventData.selectedSortItem;
        this.taskConfig.taskListSortOrder = eventData.sortType.toLowerCase();
        this.taskConfig.selectedTaskListSortOption = {
          option: this.taskConfig.taskListSortOptions.find(
            (data) => data.name === this.taskConfig.taskListSortFieldName
          ),
          sortType: this.taskConfig.taskListSortOrder,
        };
        this.selectedSortByParam =
          this.taskConfig.selectedTaskListSortOption.option &&
          this.taskConfig.selectedTaskListSortOption.option.displayName
            ? this.taskConfig.selectedTaskListSortOption.option.displayName
            : null;
        this.selectedSortOrderParam =
          this.taskConfig.selectedTaskListSortOption.option &&
          this.taskConfig.selectedTaskListSortOption.option.displayName &&
          this.taskConfig.taskListSortOrder
            ? this.taskConfig.taskListSortOrder
            : null;
      } else {
        this.taskConfig.taskCardSortFieldName = eventData.selectedSortItem;
        this.taskConfig.taskCardSortOrder = eventData.sortType.toLowerCase();
        this.taskConfig.selectedTaskCardSortOption = {
          option: this.taskConfig.taskCardSortOptions.find(
            (data) => data.name === this.taskConfig.taskCardSortFieldName
          ),
          sortType: this.taskConfig.taskCardSortOrder,
        };
        this.selectedSortByParam =
          this.taskConfig.selectedTaskCardSortOption.option &&
          this.taskConfig.selectedTaskCardSortOption.option.displayName
            ? this.taskConfig.selectedTaskCardSortOption.option.displayName
            : null;
        this.selectedSortOrderParam =
          this.taskConfig.selectedTaskCardSortOption.option &&
          this.taskConfig.selectedTaskCardSortOption.option.displayName &&
          this.taskConfig.taskCardSortOrder
            ? this.taskConfig.taskCardSortOrder
            : null;
      }
      this.getTasks();
      this.routeComponentView();
      this.setCurrentView();
    }
  }

  getTasks(editData?) {
    if (this.taskConfig && this.taskConfig.projectItemId) {
      this.loaderService.show();
      let keyWord = '';
      let sortTemp = [];
      this.taskConfig.taskTotalCount = 0;
      let projectSearch = null;
      let searchFields = [];
      let requiredFields = this.taskConfig.isTaskListView
        ? this.displayableFields
        : this.taskConfig.viewConfig.R_PM_CARD_VIEW_MPM_FIELDS;
      requiredFields.forEach((field) => {
        let indexerDisplayFields: IndexerDisplayFields;
        searchFields.push(
          (indexerDisplayFields = { field_id: field.INDEXER_FIELD_ID })
        );
      });
      let searchConditionList =
        this.fieldConfigService.formIndexerIdFromMapperId(
          TaskConstants.GET_ALL_TASKS_SEARCH_CONDITION_LIST
            .search_condition_list.search_condition
        );

      if (this.taskConfig.selectedResources) {
        const resourceSearchCondtionList = [];
        this.taskConfig.selectedResources.forEach((resource, index) => {
          const allPersons = this.sharingService.getAllPersons();
          const user = allPersons.find((person) => {
            return person.Title.Value === resource;
          });
          const Id = user ? user.PersonToUser['Identity-id'].Id : 0;
          /*  projectSearch = this.fieldConfigService.createConditionObject(
                         MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_NAME, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, index > 0 ? MPMSearchOperators.OR : MPMSearchOperators.AND, resource);
                      */
          projectSearch = this.fieldConfigService.createConditionObject(
            MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            index > 0 ? MPMSearchOperators.OR : MPMSearchOperators.AND,
            Id
          );

          if (projectSearch) {
            resourceSearchCondtionList.push(projectSearch);
          }
        });

        if (resourceSearchCondtionList.length > 0) {
          searchConditionList = searchConditionList.concat(
            resourceSearchCondtionList
          );
        }
      }
      projectSearch = this.fieldConfigService.createConditionObject(
        MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID,
        MPMSearchOperators.IS,
        MPMSearchOperatorNames.IS,
        MPMSearchOperators.AND,
        this.taskConfig.projectItemId
      );
      if (projectSearch) {
        searchConditionList.push(projectSearch);
      }
      if (
        this.searchConditionList &&
        this.searchConditionList.length > 0 &&
        this.searchConditionList[0].metadata_field_id ===
          this.searchDataService.KEYWORD_MAPPER
      ) {
        keyWord = this.searchConditionList[0].keyword;
      } else if (
        this.searchConditionList &&
        this.searchConditionList.length > 0
      ) {
        searchConditionList = searchConditionList.concat(
          this.searchConditionList
        );
      }
      if (this.taskConfig.isListView) {
        if (
          this.taskConfig.selectedTaskListSortOption &&
          this.taskConfig.selectedTaskListSortOption.option
        ) {
          sortTemp = [
            {
              field_id: this.taskConfig.selectedTaskListSortOption.option.name,
              order: this.taskConfig.selectedTaskListSortOption.sortType,
            },
          ];
        }
      } else {
        if (
          this.taskConfig.selectedTaskCardSortOption &&
          this.taskConfig.selectedTaskCardSortOption.option
        ) {
          sortTemp = [
            {
              field_id: this.taskConfig.selectedTaskCardSortOption.option.name,
              order: this.taskConfig.selectedTaskCardSortOption.sortType,
            },
          ];
        }
      }
      const parameters: SearchRequest = {
        search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
        keyword: keyWord,
        search_condition_list: {
          search_condition: searchConditionList,
        },
        displayable_field_list: {
          displayable_fields: searchFields,
        },
        facet_condition_list: {
          facet_condition: [],
        },
        sorting_list: {
          sort: sortTemp,
        },
        cursor: {
          page_index: this.skip,
          page_size: this.top,
        },
      };
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          if(editData){
            this.editTask(editData);
          }
          this.isPageRefresh = false;
          const data = [];
          response.data.forEach((element) => {
            if (element.IS_BULK_UPDATE_TASK !== 'true') {
              data.push(element);
            }
          });
          this.taskConfig.taskList = data;
          this.taskConfig.taskTotalCount = response.cursor.total_records;
          const ownerFeild: MPMField =
            this.fieldConfigService.getFieldByKeyValue(
              MPMFieldKeys.MAPPER_NAME,
              MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_NAME,
              MPM_LEVELS.TASK
            );
          this.taskConfig.taskList.map((task) => {
            const resourceName =
              this.fieldConfigService.getFieldValueByDisplayColumn(
                ownerFeild,
                task
              );
            const isExist = this.taskConfig.taskOwnerResources.find(
              (element) => element === resourceName
            );
            if (!isExist && resourceName) {
              this.taskConfig.taskOwnerResources.push(resourceName);
            }
          });
          this.taskGridComponent.refreshChild(this.taskConfig);
          this.loaderService.hide();
        },
        () => {
          this.loaderService.hide();
        }
      );
    }
  }

  updateSearchConfigId() {
    if (this.taskConfig.viewConfig) {
      this.searchConfigId =
        this.taskConfig.viewConfig.R_PO_ADVANCED_SEARCH_CONFIG;
      this.searchChangeEventService.update(this.taskConfig.viewConfig);
    }
  }
  initializeLayout(editData?) {
    if (this.taskConfig && this.taskConfig.projectId) {
      this.refreshTaskGrid({}, editData);

    }
  }

  reopenCreateTask(objectData) {
    this.createNewTaskByType(objectData);
  }

  routeComponentView() {
    const queryParams = {};
    this.routeService.goToDeliverableTaskView(
      queryParams,
      null,
      null,
      null,
      null
    );
    const sortOption = this.taskConfig.isTaskListView
      ? this.taskConfig.selectedTaskListSortOption &&
        this.taskConfig.selectedTaskListSortOption.option &&
        this.taskConfig.selectedTaskListSortOption.option.displayName
        ? this.taskConfig.selectedTaskListSortOption.option.displayName
        : null
      : this.taskConfig.selectedTaskCardSortOption &&
        this.taskConfig.selectedTaskCardSortOption.option &&
        this.taskConfig.selectedTaskCardSortOption.option.displayName
      ? this.taskConfig.selectedTaskCardSortOption.option.displayName
      : null;
    const sortOrder = this.taskConfig.isTaskListView
      ? this.taskConfig.selectedTaskListSortOption &&
        this.taskConfig.selectedTaskListSortOption.option &&
        this.taskConfig.selectedTaskListSortOption.option.displayName &&
        this.taskConfig.taskListSortOrder
        ? this.taskConfig.taskListSortOrder
        : null
      : this.taskConfig.selectedTaskCardSortOption &&
        this.taskConfig.selectedTaskCardSortOption.option &&
        this.taskConfig.selectedTaskCardSortOption.option.displayName &&
        this.taskConfig.taskCardSortOrder
      ? this.taskConfig.taskCardSortOrder
      : null;
    this.routeService.goToTaskView(
      this.taskConfig.isTemplate,
      this.taskConfig.projectId,
      this.taskConfig.isTaskListView,
      false,
      null,
      this.selectedCampaignId ? this.selectedCampaignId : null,
      this.selectedSortByParam,
      this.selectedSortOrderParam,
      this.taskConfig.selectedResources &&
        this.taskConfig.selectedResources.length > 0
        ? this.taskConfig.selectedResources.toString()
        : null,
      this.searchName ? this.searchName : null,
      this.savedSearchName ? this.savedSearchName : null,
      this.advSearchData ? this.advSearchData : null
    );
  }

  handleViewChange(isListView) {
    this.taskConfig.isTaskListView = isListView ? true : false;
    this.routeComponentView();
    this.setCurrentView();
  }

  ngOnInit(): void {
    this.appConfig = this.sharingService.getAppConfig();
    this.enableIndexerFieldRestriction = config.getFieldRestriction();
    this.maximumFieldsAllowed = config.getMaximumFieldsAllowed();
    this.taskConfig.taskStatus = [];
    this.taskConfig.taskOwnerResources = [];
    this.taskConfig.selectedResources = [];
    this.taskConfig.taskCardSortOptions = [];
    this.taskConfig.taskListSortOptions = [];
    this.taskConfig.metaDataFields = [];
    this.taskConfig.taskList = [];
    this.taskConfig.taskTotalCount = 0;
    let initialLoad = true;
    this.activatedRoute.queryParams.subscribe((params) => {
    
      if (params.isListView) {
        this.taskConfig.isTaskListView =
          params.isListView === 'true' ? true : false;
      }
      this.selectedCampaignId = params.campaignId;
      this.searchName = params.searchName;
      this.savedSearchName = params.savedSearchName;
      this.advSearchData = params.advSearchData;

      const resources = params.viewByResource
        ? params.viewByResource.split(',')
        : [];
      if (
        params.sortBy !== this.selectedSortByParam ||
        params.sortOrder !== this.selectedSortOrderParam ||
        this.selectedCampaignId !== params.campaignId ||
        this.searchName !== params.searchName ||
        this.savedSearchName !== params.savedSearchName ||
        this.advSearchData !== params.advSearchData ||
        JSON.stringify(this.taskConfig.selectedResources) !==
          JSON.stringify(resources) ||
        this.isPageRefresh
      ) {
        if (
          params.isEditTask === 'true' &&
          String(this.taskConfig.isEditTask) !== params.isEditTask
        ) {
          this.taskService.getTaskById(params.taskId).subscribe((response) => {
            const data = {
              projectId: this.taskConfig.projectId,
              isTemplate: this.taskConfig.isTemplate,
              taskId: params.taskId,
              isApprovalTask: this.utilService.getBooleanValue(
                response.IS_APPROVAL_TASK
              ),
            };
            this.initializeLayout(data);
          });
        }
        else{
          this.initializeLayout();
        }
      }
      if (params.sortBy) {
        this.selectedSortByParam = params.sortBy;
        this.selectedSortOrderParam = params.sortOrder;
      }
      if (params.viewByResource) {
        const resources = params.viewByResource.split(',');
        if (
          JSON.stringify(this.taskConfig.selectedResources) !==
          JSON.stringify(resources)
        ) {
          this.getTasks();
          this.routeComponentView();
        }
      }
    });
    this.sharingService.resetViewData().subscribe((viewId) => {
      if (
        viewId &&
        this.taskConfig &&
        this.taskConfig.viewConfig &&
        this.taskConfig.viewConfig['MPM_View_Config-id'] &&
        this.taskConfig.viewConfig['MPM_View_Config-id'].Id &&
        viewId === this.taskConfig.viewConfig['MPM_View_Config-id'].Id
      ) {
        this.resetCurrentView();
      }
    });
    this.tempsearchConditionsSubscribe =
      this.searchDataService.searchData.subscribe((data) => {
        this.searchConditionList =
          data && Array.isArray(data)
            ? this.otmmService.addRelationalOperator(data)
            : [];
        if (
          (this.searchName || this.savedSearchName || this.advSearchData) &&
          initialLoad
        ) {
          if (data && data.length > 0) {
            this.getTasks();
          }
        } else {
          if (!initialLoad) {
            this.getTasks();
          }
        }
        initialLoad = false;
      });
    this.taskService.refreshData.subscribe(() => {
      this.getTasks();
    });
  }

  ngOnDestroy(): void {
    if (this.tempsearchConditionsSubscribe) {
      this.searchChangeEventService.update(null);
      this.tempsearchConditionsSubscribe.unsubscribe();
    }
  }
}
