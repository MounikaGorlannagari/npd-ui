import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskLayoutComponent } from './task-layout/task-layout.component';
import { TaskRoutingModule } from './task.routing.module';
import { TaskComponent } from './task.component';
import {
  ColumnChooserModule,
  MpmLibraryModule,
  TasksModule,
} from 'mpm-library';
import { TaskGridComponent } from './task-grid/task-grid.component';
import { TaskCreationModalComponent } from '../task-creation-modal/task-creation-modal.component';
import { MaterialModule } from '../../../../material.module';
import { MpmLibModule } from '../../../mpm-lib/mpm-lib.module';
import { NpdTaskModule } from 'src/app/core/npd-ui/npd-task-view/npd-task.module';
import { NpdTaskLayoutComponent } from 'src/app/core/npd-ui/npd-task-view/npd-task-layout/npd-task-layout.component';
import { NpdTaskToolbarComponent } from 'src/app/core/npd-ui/npd-task-view/npd-task-toolbar/npd-task-toolbar.component';
@NgModule({
  declarations: [
    TaskLayoutComponent,
    TaskComponent,
    TaskCreationModalComponent,
    TaskGridComponent,
    NpdTaskLayoutComponent,
    NpdTaskToolbarComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    TaskRoutingModule,
    MpmLibraryModule,
    TasksModule,
    MpmLibModule,
    NpdTaskModule,
    ColumnChooserModule,
  ],
})
export class TaskModule {}
