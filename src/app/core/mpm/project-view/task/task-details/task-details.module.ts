import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskDetailsComponent } from './task-details.component';
import { DeliverableCreationModalComponent } from './deliverable-creation-modal/deliverable-creation-modal.component';
import { MaterialModule } from '../../../../../material.module';
import { MpmLibraryModule, DeliverableModule } from 'mpm-library';
import { TaskDetailsRoutingModule } from './task-details.routing.module';
import { TaskDetailsLayoutComponent } from './task-details-layout/task-details-layout.component';
import { MpmLibModule } from '../../../../mpm-lib/mpm-lib.module';

@NgModule({
    declarations: [
        TaskDetailsComponent,
        DeliverableCreationModalComponent,
        TaskDetailsLayoutComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        MpmLibraryModule,
        DeliverableModule,
        TaskDetailsRoutingModule,
        MpmLibModule
    ]
})

export class TaskDetailsModule { }
