import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoaderService, DeliverableCreationComponent, ProjectConstant } from 'mpm-library';
import { NotificationService } from 'mpm-library';
import { MPMDeliverableCreationComponent } from '../../../../../mpm-lib/project/tasks/deliverable/deliverable-creation/deliverable-creation.component';

@Component({
    selector: 'app-deliverable-creation-modal',
    templateUrl: './deliverable-creation-modal.component.html',
    styleUrls: ['./deliverable-creation-modal.component.scss']
})

export class DeliverableCreationModalComponent implements OnInit {

    @ViewChild(MPMDeliverableCreationComponent) deliverableCreationComponent: MPMDeliverableCreationComponent;

    deliverableConfig = {};
    modalLabel;
    urlParams;

    constructor(
        public dialogRef: MatDialogRef<DeliverableCreationModalComponent>,
        @Inject(MAT_DIALOG_DATA) public deliverableData: any,
        private notificationService: NotificationService,
        private loaderService: LoaderService,
    ) { }

    afterCloseDeliverable(event) {
        this.dialogRef.close(true);
    }

    closeDialog() {
        this.deliverableCreationComponent.cancelDeliverableCreation();
    }

    afterSaveDeliverable(event) {
        if (event.saveOption && event.saveOption.value === 'SAVE') {
            this.dialogRef.close(true);
        }
    }

    notificationHandler(event) {
        if (event.message) {
            event.type === ProjectConstant.NOTIFICATION_LABELS.SUCCESS ? this.notificationService.success(event.message) :
                event.type === ProjectConstant.NOTIFICATION_LABELS.INFO ? this.notificationService.info(event.message) : this.notificationService.error(event.message);
        }
    }

    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }

    ngOnInit() {
        this.loadingHandler(true);
        this.deliverableConfig = this.deliverableData;
        this.modalLabel = this.deliverableData.modalLabel;
    }

}
