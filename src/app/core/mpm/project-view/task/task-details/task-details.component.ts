import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliverableCreationModalComponent } from './deliverable-creation-modal/deliverable-creation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import {
    TaskService, LoaderService, OTMMService, AssetService, SharingService, NotificationService, SearchDataService,
    DeliverableConstants, FieldConfigService, AssetConstants, OTMMMPMDataTypes
} from 'mpm-library';
import { RouteService } from '../../../route.service';
import { RoutingConstants } from '../../../routingConstants';
import { AssetsVersionModalComponent } from '../../../shared/components/assets-version-modal/assets-version-modal.component';
import { AssetDetailsComponent } from '../../../shared/components/asset-details/asset-details.component';
import { ShareAssetsModalComponent } from '../../../shared/components/share-assets-modal/share-assets-modal.component';
import { StateService } from 'mpm-library';
import { MenuConstants } from '../../../shared/constants/menu.constants';

@Component({
    selector: 'app-task-details',
    templateUrl: './task-details.component.html',
    styleUrls: ['./task-details.component.scss']
})

export class TaskDetailsComponent implements OnInit {
    deliverableConfig = {
        projectId: null,
        taskId: null,
        isTemplate: false,
        isNewDeliverable: false,
        isListView: true,
        deliverableViewName: null,
        isEditDeliverable: false
    };
    urlParams: any;
    searchConditionList = [];
    taskConfig: any;
    selectedCampaignId;

    selectedSortOrder;
    selectedSortOption;
    selectedViewResource;

    searchName;
    savedSearchName;
    advSearchData;

    queryParams;
    routeParams = {
        campaignId: null,
        projectId: null,
        taskId: null
    };

    constructor(
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private dialog: MatDialog,
        private taskService: TaskService,
        private loaderService: LoaderService,
        private routeService: RouteService,
        private searchDataService: SearchDataService,
        private otmmService: OTMMService,
        private assetService: AssetService,
        private sharingService: SharingService,
        private notification: NotificationService,
        private fieldConfigService: FieldConfigService,
        private stateService: StateService
    ) { }

    goToTaskView() {
        // const appConfig = this.sharingService.getAppConfig();
        // this.taskConfig.isTaskListView = appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_TASK_VIEW] === 'LIST'? true : false; 
        this.stateService.setRoute(this.queryParams.menuName, this.queryParams, this.routeParams);
        this.stateService.removeRoute(MenuConstants.DELIVERABLES);
        const routeData = this.stateService.getRoute(MenuConstants.TASKS);
        const params = routeData ? routeData.params : null;
        if (params) {
            this.routeService.goToTaskView(params.queryParams.isTemplate, params.routeParams.projectId, params.queryParams.isListView, params.queryParams.isEditTask, params.queryParams.taskId,
                params.queryParams.campaignId, params.queryParams.sortBy, params.queryParams.sortOrder, params.queryParams.viewByResource, params.queryParams.searchName,
                params.queryParams.savedSearchName, params.queryParams.advSearchData);
        } else {
            this.routeService.goToTaskView(this.deliverableConfig.isTemplate, this.deliverableConfig.projectId, null, false, null,
                this.selectedCampaignId ? this.selectedCampaignId : null, null, null, null, null, null, null);
        }
        // this.deliverableConfig.isListView params.queryParams.
    }

    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }

    /* deliverableDetails(event) {
        this.assetService.getMetadataFields('ASSET_DETAILS').subscribe(response => {
            const asset = event;
            asset.metadataFields = response.MPM_Metadata_Fields;
            asset.isTemplate = this.deliverableConfig.isTemplate;
            this.dialog.open(AssetDetailsComponent, {
                width: '70%',
                disableClose: true,
                data: asset
            });
        });
    }
 */

    notificationHandler(event) {
        if (event.message) {
            this.notification.error(event.message);
        }
    }
    deliverableDetails(event) {
        const deliverableItemId = this.taskService.getProperty(event, DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID);
        const assetDetailSearchCondition = {
            search_condition_list: {
                search_condition: [{
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.UTILS.DATA_TYPE',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    value: OTMMMPMDataTypes.DELIVERABLE
                }, {
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.DELIVERABLE.ITEM_ID',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    value: deliverableItemId,
                    relational_operator: 'and'
                }]
            }
        };
        const searchCondition = JSON.parse(JSON.stringify(AssetConstants.ASSET_DETAILS_CONDITION));
        if (event && event.metadata) {
            const fieldConfigs = this.fieldConfigService.getFieldsbyFeildCategoryLevel(event.metadata.object_type);

            searchCondition.search_condition_list.search_condition.map(function (search, index) {
                const fieldConfig = fieldConfigs.find(function (field) { return search.value == field.MAPPER_NAME });
                const metadata = event.metadata.metadata_element_list[0].metadata_element_list.find(function (metadata) {
                    return metadata.id == fieldConfig.OTMM_FIELD_ID;
                })

                return search.value = metadata.value.value.value;

            });
        }
        else {


            /*   const fieldConfigs = this.fieldConfigService.getFieldsbyFeildCategoryLevel(event.metadata.object_type); */
            const fieldConfigs = this.fieldConfigService.getFieldsbyFeildCategoryLevel('ASSET');
            /*  const searchCondition = JSON.parse(JSON.stringify(AssetConstants.ASSET_DETAILS_CONDITION)); */

            /*  var task_id = event.TASK_ITEM_ID.split('.')[1];
             var task_Id = event.TASK_ITEM_ID.split('.')[0];
             var taskfinal = task_Id + '.' + task_id;
 
             var del_id = event.deliverableItemId.split('.')[1];
             var del_Id = event.deliverableItemId.split('.')[0];
             var delfinal2 = del_Id + '.' + del_id; */
            var ids = [];
            ids.push(event.PROJECT_ITEM_ID);
            ids.push(event.TASK_ITEM_ID);
            ids.push(event.deliverableItemId);
            /*   ids.push(taskfinal);
              ids.push(delfinal2); */
           

            searchCondition.search_condition_list.search_condition.map(function (search, index) {
                const fieldConfig = fieldConfigs.find(function (field) { return search.value == field.MAPPER_NAME });

                
                return search.value = ids[index];
            });
        }

        this.loadingHandler(true);
        this.otmmService.checkOtmmSession()
            .subscribe(sessionResponse => {
                let userSessionId;
                const skip = 0;
                const top = 10;
                if (sessionResponse && sessionResponse.session_resource && sessionResponse.session_resource.session && sessionResponse.session_resource.session.id) {
                    userSessionId = sessionResponse.session_resource.session.id;
                }
                this.assetService.getAssetDetails(searchCondition).subscribe(asset_details => {
                    if (asset_details && asset_details.data && asset_details.data.length > 0) {
                        let asset = {};
                        asset['data'] = asset_details.data;
                        asset['metadata_fields'] = this.sharingService.getAllApplicationViewConfig().find(application_views => { return application_views.VIEW === 'ASSET_DETAILS_VIEW' });
                        if (asset) {
                            asset['isTemplate'] = true;
                            this.dialog.open(AssetDetailsComponent, {
                                width: '70%',
                                height: '88%',
                                disableClose: true,
                                data: asset
                            });
                            this.loadingHandler(false);
                        } else {
                            const eventData = { message: 'Error while getting asset details', type: 'error' };
                            this.notificationHandler(eventData);
                        }
                    } else {
                        this.loadingHandler(false);
                    }
                }, asset_details_error => {
                    this.loadingHandler(false);
                    const eventData = { message: 'Error getting Asset related details', type: 'error' };
                    this.notificationHandler(eventData);
                });
            }, session_error => {
                this.loadingHandler(false);
                const eventData = { message: 'Error getting OTMM session details', type: 'error' };
                this.notificationHandler(eventData);
            });
    }

    viewVersions(event) {
        this.dialog.open(AssetsVersionModalComponent, {
            width: '70%',
            disableClose: true,
            data: event
        });
    }

    shareAsset(event) {
        const currentUserEmail = this.sharingService.getCurrentUserEmailId();

        if (currentUserEmail && currentUserEmail.length > 0) {
            this.dialog.open(ShareAssetsModalComponent, {
                width: '50%',
                disableClose: true,
                data: event
            });
        } else {
            this.notification.error('The user does not have Email Id');
        }
    }

    createNewDeliverable(deliverableConfig) {
        deliverableConfig.isNewDeliverable = true;
        this.routeService.goToTaskDetails(deliverableConfig.isTemplate, deliverableConfig.projectId, deliverableConfig.taskId
            , deliverableConfig.isNewDeliverable, this.selectedCampaignId, null, null, this.selectedSortOption, this.selectedSortOrder, this.selectedViewResource,
            this.searchName ? this.searchName : null, this.savedSearchName ? this.savedSearchName : null, this.advSearchData ? this.advSearchData : null);
        const dialogRef = this.dialog.open(DeliverableCreationModalComponent, {
            width: '39%', /* '38%', */
            disableClose: true,
            data: {
                projectId: deliverableConfig.projectId,
                isTemplate: deliverableConfig.isTemplate,
                taskId: deliverableConfig.taskId,
                selectedProject: deliverableConfig.selectedProject,
                selectedTask: deliverableConfig.selectedTask,
                isNewDeliverable: deliverableConfig.isNewDeliverable,
                modalLabel: 'Add Deliverable'
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            deliverableConfig.isNewDeliverable = false;
            this.routeService.goToTaskDetails(deliverableConfig.isTemplate, deliverableConfig.projectId, deliverableConfig.taskId
                , deliverableConfig.isNewDeliverable, this.selectedCampaignId, null, null, this.selectedSortOption, this.selectedSortOrder, this.selectedViewResource,
                this.searchName ? this.searchName : null, this.savedSearchName ? this.savedSearchName : null, this.advSearchData ? this.advSearchData : null);
            if (result) {
                this.taskService.setDeliverableRefreshData(200);
            }
        });
    }

    editDeliverable(deliverableConfig) {
        deliverableConfig.isEditDeliverable = true;
        this.routeService.goToTaskDetails(deliverableConfig.isTemplate, deliverableConfig.projectId, deliverableConfig.taskId
            , deliverableConfig.isNewDeliverable, this.selectedCampaignId, deliverableConfig.isEditDeliverable, deliverableConfig.deliverableId,
            this.selectedSortOption, this.selectedSortOrder, this.selectedViewResource,
            this.searchName ? this.searchName : null, this.savedSearchName ? this.savedSearchName : null, this.advSearchData ? this.advSearchData : null);
        const dialogRef = this.dialog.open(DeliverableCreationModalComponent, {
            width: '38%',
            disableClose: true,
            data: {
                projectId: deliverableConfig.projectId,
                isTemplate: deliverableConfig.isTemplate,
                taskId: deliverableConfig.taskId,
                deliverableId: deliverableConfig.deliverableId,
                selectedProject: deliverableConfig.selectedProject,
                selectedTask: deliverableConfig.selectedTask,
                isEditDeliverable: deliverableConfig.isEditDeliverable,
                modalLabel: 'Edit Deliverable'
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            deliverableConfig.isEditDeliverable = false;
            this.routeService.goToTaskDetails(deliverableConfig.isTemplate, deliverableConfig.projectId, deliverableConfig.taskId
                , deliverableConfig.isNewDeliverable, this.selectedCampaignId, deliverableConfig.isEditDeliverable, null, this.selectedSortOption, this.selectedSortOrder, this.selectedViewResource,
                this.searchName ? this.searchName : null, this.savedSearchName ? this.savedSearchName : null, this.advSearchData ? this.advSearchData : null);
            if (result) {
                this.taskService.setDeliverableRefreshData(200);
            }
        });
    }

    openCR(crData) {
        this.stateService.setRoute(this.queryParams.menuName, this.queryParams, this.routeParams);
        this.router.navigate([RoutingConstants.projectViewBaseRoute], {
            queryParams: {
                isCreativeReview: true,
                crData: JSON.stringify(crData)
            }
        });
    }

    readUrlParams(callback) {
        this.activatedRoute.params.subscribe(taskparams => {
            this.activatedRoute.queryParams.subscribe(queryParams => {
                this.activatedRoute.parent.parent.params.subscribe(parentRouteParams => {
                    callback(parentRouteParams, queryParams, taskparams);
                });
            });
        });
    }

    ngOnInit() {
        this.readUrlParams((parentRouteParams, queryParams, taskparams) => {
            if (queryParams && parentRouteParams) {
                this.queryParams = queryParams;
                if (parentRouteParams.campaignId) {
                    this.routeParams.campaignId = parentRouteParams.campaignId;
                }
                if (parentRouteParams.projectId) {
                    this.routeParams.projectId = parentRouteParams.projectId;
                }
                if (taskparams && taskparams.taskId) {
                    this.routeParams.taskId = taskparams.taskId;
                }
                this.selectedCampaignId = queryParams.campaignId;
                this.deliverableConfig.isListView = queryParams.isListView;
                this.selectedSortOption = queryParams.sortBy;
                this.selectedSortOrder = queryParams.sortOrder;
                this.selectedViewResource = queryParams.viewByResource;
                this.searchName = queryParams.searchName;
                this.savedSearchName = queryParams.savedSearchName;
                this.advSearchData = queryParams.advSearchData;
                if (queryParams.isTemplate && queryParams.isTemplate === 'true') {
                    this.deliverableConfig.isTemplate = true;
                }
                if (queryParams.isNewDeliverable && queryParams.isNewDeliverable === 'true') {
                    this.deliverableConfig.isNewDeliverable = true;
                }
                /*                 if (queryParams.isEditDeliverable && queryParams.isEditDeliverable === 'true') {
                                    this.deliverableConfig.isEditDeliverable = true;
                                } */
                if (parentRouteParams.projectId && taskparams.taskId) {
                    this.deliverableConfig.projectId = parentRouteParams.projectId;
                    this.deliverableConfig.taskId = taskparams.taskId;
                }
            }
        });

        this.searchDataService.searchData
            .subscribe(data => {
                if (data && Array.isArray(data)) {
                    this.searchConditionList = this.otmmService.addRelationalOperator(data);
                }
            });
    }

}
