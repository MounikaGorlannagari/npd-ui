import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import {
    OTMMService, ProjectService, TaskService, EntityAppDefService, SharingService,
    NotificationService, LoaderService, DeliverableConstants,
    MPM_LEVELS, SearchChangeEventService, SearchDataService,
    ViewConfigService, FieldConfigService, ViewConfig, MPMField,
    SearchRequest, IndexerService, SearchResponse,
    ProjectConstant, MPMFieldConstants, StatusLevels, MPMFieldKeys, IndexerDataTypes, SearchConfigConstants, MPMSearchOperators, MPMSearchOperatorNames
} from 'mpm-library';
import { Observable } from 'rxjs';
import { ApplicationConfigConstants } from 'mpm-library';
import { MPMDeliverableSwimLaneComponent } from '../../../../../mpm-lib/project/tasks/deliverable/deliverable-swim-lane/deliverable-swim-lane.component';
import { MPMDeliverableToolbarComponent } from '../../../../../mpm-lib/project/tasks/deliverable/deliverable-toolbar/deliverable-toolbar.component';
import { ActivatedRoute } from '@angular/router';
import { RouteService } from '../../../../route.service';
import { StateService } from 'mpm-library';
import { MenuConstants } from '../../../../shared/constants/menu.constants';
import { UtilService } from 'mpm-library';
import { IndexerDisplayFields } from 'mpm-library';
import { GloabalConfig as config } from 'mpm-library';

@Component({
    selector: 'app-task-details-layout',
    templateUrl: './task-details-layout.component.html',
    styleUrls: ['./task-details-layout.component.scss']
})

export class TaskDetailsLayoutComponent implements OnInit, OnDestroy {

    @Input() deliverableConfig;
    @Input() isViewByResourceFilterEnabled;

    @Output() createNewDeliverableHandler = new EventEmitter<any>();
    @Output() deliverableDetailsHandler = new EventEmitter<any>();
    @Output() versionsHandler = new EventEmitter<any>();
    @Output() crHandler = new EventEmitter<any>();
    @Output() shareAssetHandler = new EventEmitter<any>();
    @Output() editDeliverableHandler = new EventEmitter<any>();

    @Output() masterToogleDeliverable = new EventEmitter<any>();

    masterSelectedDeliverable;
    enableAction = false;
    crBulkResponse;
    skip = 0;
    top = 100;
    customMetadataFields;
    PROJECT_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Project/operations';
    TASK_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Task/operations';
    GET_PROJECT_DETAILS_WS_METHOD_NAME = 'GetProjectByID';
    GET_TASK_DETAILS_WS_METHOD_NAME = 'ReadTask';
    hasAllDetails = false;
    tempsearchConditionsSubscribe: any;
    searchConditionList: Array<any> = [];
    searchConfigId = -1;
    resourceDuplicateMapper = {};
    deliverableTaskType = null;
    appConfig: any;
    deliverableData;
    projectData;
    delCount;


    isRefresh;
    selectedCampaignId;

    isNewDeliverable;
    isEditDeliverable;
    deliverableId;

    searchName;
    savedSearchName;
    advSearchData;
    selectedSortOption;
    selectedSortOrder;
    bulkResponseData;

    enableIndexerFieldRestriction;
    maximumFieldsAllowed;

    constructor(
        private otmmService: OTMMService,
        private projectService: ProjectService,
        private taskService: TaskService,
        private entityAppDefService: EntityAppDefService,
        private notificationService: NotificationService,
        private loaderService: LoaderService,
        private searchDataService: SearchDataService,
        private searchChangeEventService: SearchChangeEventService,
        private viewConfigService: ViewConfigService,
        private fieldConfigService: FieldConfigService,
        private indexerService: IndexerService,
        private sharingService: SharingService,
        private activatedRoute: ActivatedRoute,
        private routeService: RouteService,
        private stateService: StateService,
        private utilService: UtilService
    ) { }

    @ViewChild(MPMDeliverableToolbarComponent) deliverableToolbarComponent: MPMDeliverableToolbarComponent;
    @ViewChild(MPMDeliverableSwimLaneComponent) deliverableSwimLaneComponent: MPMDeliverableSwimLaneComponent;

    refreshChild() {
        if (this.deliverableToolbarComponent) {
            this.deliverableToolbarComponent.refreshChild(this.deliverableConfig);
        }
        if (this.deliverableSwimLaneComponent) {
            this.deliverableSwimLaneComponent.initialiseSwimList();
        }
    }

    //deliverable view details
    deliverableDetails(event) {
        if (event.asset) {
            this.deliverableDetailsHandler.emit(event.asset);
        }
        else {
            this.deliverableDetailsHandler.emit(event);
        }
    }

    deliverableBulkCheckCount(data) {
        this.enableAction = data;
    }

    selectedDeliverableCount(data) {
        this.delCount = data;
        /*  if (this.delCount === 1) {
             this.notificationService.info("One or more records do not meet bulk edit criteria");
         } */
        this.masterSelectedDeliverable = true;
    }

    toggleResourceFilter(viewByResourceFilter) {
        this.isViewByResourceFilterEnabled = viewByResourceFilter;
    }

    selectedDeliverableDatas(data) {
        this.deliverableData = data;
    }

    selectedDeliverableData(data) {
        this.projectData = data;
    }
    viewVersions(event) {
        this.versionsHandler.emit(event);
    }

    openCR(event) {
        this.crHandler.emit(event);
    }

    shareAsset(event) {
        this.shareAssetHandler.emit(event);
    }

    createNewDeliverable(eventData) {
        this.createNewDeliverableHandler.emit(eventData);
    }

    toolBarConfigChange(deliverableConfig) {
        this.deliverableConfig = deliverableConfig;
        this.getDeliverables();
        this.isViewByResourceFilterEnabled = true;
        // MPMV3-1055
        const currStatusName = this.projectService.getCurrProjectStatus(this.deliverableConfig.taskStatusId);
        if (currStatusName !== StatusLevels.FINAL) {
            this.getTaskDetails().subscribe(data => {
            });
        }
        this.routeService.goToTaskDetails(this.deliverableConfig.isTemplate, this.deliverableConfig.projectId, this.deliverableConfig.taskId,
            this.isNewDeliverable, this.selectedCampaignId, this.isEditDeliverable, this.deliverableId, this.deliverableConfig.selectedSortOption && this.deliverableConfig.selectedSortOption.option
                && this.deliverableConfig.selectedSortOption.option.displayName ? this.deliverableConfig.selectedSortOption.option.displayName : null,
            this.deliverableConfig.selectedSortOption && this.deliverableConfig.selectedSortOption.sortType ? this.deliverableConfig.selectedSortOption.sortType : null,
            this.deliverableConfig.selectedResources && this.deliverableConfig.selectedResources.length > 0 ? this.deliverableConfig.selectedResources.toString() : null,
            this.searchName ? this.searchName : null, this.savedSearchName ? this.savedSearchName : null, this.advSearchData ? this.advSearchData : null);
        this.setCurrentView();
    }

    refreshDeliverableGrid(eventdata) {
        if (this.deliverableConfig) {
            this.getDeliverables();
        }
    }

    getDeliverables() {
        this.loaderService.show();
        let keyWord = '';
        let sortTemp = [];
        let searchFields = [];
        if(this.enableIndexerFieldRestriction){
            this.deliverableConfig.viewConfig.R_PM_CARD_VIEW_MPM_FIELDS.forEach((field) =>{
                let indexerDisplayFields: IndexerDisplayFields
                searchFields.push(indexerDisplayFields = {field_id : field.INDEXER_FIELD_ID});
            })
        }
        if (this.deliverableConfig && this.deliverableConfig.projectItemId && this.deliverableConfig.taskItemId) {

            let searchConditionList = this.fieldConfigService.formIndexerIdFromMapperId(DeliverableConstants.DELIVERABLE_SEARCH_CONDITION.search_condition_list.search_condition);
            searchConditionList.push({
                type: IndexerDataTypes.STRING,
                field_id: this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TASK_ID),
                relational_operator_id: MPMSearchOperators.IS,
                relational_operator_name: MPMSearchOperatorNames.IS,
                value: this.deliverableConfig.taskItemId,
                relational_operator: MPMSearchOperators.AND
            });
            searchConditionList.push({
                type: IndexerDataTypes.STRING,
                field_id: this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_PROJECT_ID),
                relational_operator_id: MPMSearchOperators.IS,
                relational_operator_name: MPMSearchOperatorNames.IS,
                value: this.deliverableConfig.projectItemId,
                relational_operator: MPMSearchOperators.AND
            });

            if (this.searchConditionList && this.searchConditionList.length > 0 && this.searchConditionList[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
                keyWord = this.searchConditionList[0].keyword;
            } else if (this.searchConditionList && this.searchConditionList.length > 0) {
                searchConditionList = searchConditionList.concat(this.searchConditionList);
            }

            if (this.deliverableConfig.selectedSortOption) {
                sortTemp = [{
                    field_id: this.deliverableConfig.selectedSortOption.option.name,
                    order: this.deliverableConfig.selectedSortOption.sortType
                }];
            }
            if (this.deliverableConfig.selectedResources) {
                let resourceSearchCondtionList = [];
                this.deliverableConfig.selectedResources.forEach((resource, index) => {
                    let resourceSearchCondtion = JSON.parse(JSON.stringify(DeliverableConstants.DELIVERABLE_RESOURCE_SEARCH_CONDITION));
                    const allPersons = this.sharingService.getAllPersons();
                    const user = allPersons.find(person => { return person.Title.Value === resource });
                    const Id = user ? user.PersonToUser["Identity-id"].Id : 0;
                    resourceSearchCondtion[0].value = Id;
                    if (this.deliverableTaskType === 'APPROVAL_TASK') {
                        resourceSearchCondtion[1].value = Id;
                    } else {
                        resourceSearchCondtion = [resourceSearchCondtion[0]];
                    }
                    if (index > 0) {
                        resourceSearchCondtion[0].relational_operator = 'or';
                    }
                    resourceSearchCondtionList = resourceSearchCondtionList.concat(resourceSearchCondtion);
                });

                if (resourceSearchCondtionList.length > 0) {
                    searchConditionList = searchConditionList.concat(resourceSearchCondtionList);
                }
            }
            const parameters: SearchRequest = {
                search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
                keyword: keyWord,
                search_condition_list: {
                    search_condition: searchConditionList
                },
                displayable_field_list: {
                    displayable_fields: searchFields,
                },
                facet_condition_list: {
                    facet_condition: []
                },
                sorting_list: {
                    sort: sortTemp
                },
                cursor: {
                    page_index: this.skip,
                    page_size: this.top
                }
            };

            this.indexerService.search(parameters).subscribe((response: SearchResponse) => {

                this.deliverableConfig.deliverableTotalCount = response.cursor.total_records;
                this.deliverableConfig.deliverableList = response.data;
                // MPMV3-958
                const deliverableOwnerField: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_OWNER_NAME
                    , MPM_LEVELS.DELIVERABLE);
                const deliverableApproverField: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_APPROVER_NAME
                    , MPM_LEVELS.DELIVERABLE);
                this.deliverableConfig.deliverableList.map(deliverable => {
                    const deliverableOwner = this.fieldConfigService.getFieldValueByDisplayColumn(deliverableOwnerField, deliverable);
                    this.resourceDuplicateMapper[deliverableOwner] = this.resourceDuplicateMapper[deliverableOwner] ? 2 : 1;
                    if (this.resourceDuplicateMapper[deliverableOwner] === 1) {
                        this.deliverableConfig.deliverableResources.push(deliverableOwner);
                    }
                    let deliverableApprover = '';
                    if (this.deliverableTaskType === 'APPROVAL_TASK') {
                        deliverableApprover = this.fieldConfigService.getFieldValueByDisplayColumn(deliverableApproverField, deliverable);
                        this.resourceDuplicateMapper[deliverableApprover] = this.resourceDuplicateMapper[deliverableApprover] ? 2 : 1;
                        if (this.resourceDuplicateMapper[deliverableApprover] === 1) {
                            this.deliverableConfig.deliverableResources.push(deliverableApprover);
                        }
                    }
                });
                this.deliverableConfig.deliverableResources = this.deliverableConfig.deliverableResources.sort();
                this.hasAllDetails = true;
                this.loaderService.hide();
                if (this.deliverableSwimLaneComponent) {
                    this.deliverableSwimLaneComponent.initialiseSwimList();
                }
            }, error => {
                this.loaderService.hide();
                const eventData = { message: 'Something went wrong while fetching search results', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            });
        }
    }

    setCurrentView() {
        const currentViewData = {
            sortName: this.deliverableConfig.selectedSortOption.option.indexerId,
            sortOrder: this.deliverableConfig.selectedSortOption.sortType,
            pageNumber: null,
            pageSize: null,
            isListView: null,
            viewId: this.deliverableConfig.viewConfig['MPM_View_Config-id'].Id,
            viewName: this.deliverableConfig.viewConfig.VIEW
        }
        sessionStorage.setItem('CURRENT_VIEW', JSON.stringify(currentViewData));
    }

    getMPMFields(): Observable<any> {
        let viewConfig;
        return new Observable(observer => {
            if (this.deliverableConfig && this.deliverableConfig.deliverableViewName) {
                viewConfig = this.deliverableConfig.deliverableViewName;//this.sharingService.getViewConfigById(this.deliverableConfig.deliverableViewId).VIEW;
            } else {
                viewConfig = SearchConfigConstants.SEARCH_NAME.DELIVERABLE;
            }
            this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails: ViewConfig) => {
                if (!this.deliverableConfig.viewConfig) {
                    this.deliverableConfig.viewConfig = viewDetails;
                    this.updateSearchConfigId();
                }
                this.deliverableConfig.viewConfig = viewDetails;
                this.deliverableConfig.metaDataFields = this.fieldConfigService.getFieldsbyFeildCategoryLevel(MPM_LEVELS.DELIVERABLE);
                const dataSet = this.fieldConfigService.formrequiredDataForLoadView(viewDetails, false);
                this.deliverableConfig.deliverableSortOptions = dataSet.SORTABLE_FIELDS;
                if (this.deliverableConfig.deliverableSortOptions.length > 0) {
                    const selectedSort = this.deliverableConfig.deliverableSortOptions.find(sort => sort.displayName === this.selectedSortOption);
                    if (selectedSort) {
                        this.deliverableConfig.selectedSortOption = {
                            option: selectedSort,
                            sortType: this.selectedSortOrder ? this.selectedSortOrder :
                                (this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc')
                        };
                        this.deliverableConfig.sortFieldName = this.deliverableConfig.selectedSortOption.option.name;
                        this.deliverableConfig.sortOrder = this.deliverableConfig.selectedSortOption.sortType;
                    } else {
                        const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.deliverableConfig.viewConfig['MPM_View_Config-id'].Id);
                        const userPreferenceFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA).data : null;
                        if (userPreferenceFieldData) {
                            userPreferenceFieldData.forEach(field => {
                                const fieldId = Object.keys(field)[0];
                                if (field[fieldId].IS_SORTABLE === true) {
                                    const selectedSortOption = this.deliverableConfig.deliverableSortOptions.find(field => field.indexerId === fieldId);
                                    if (selectedSortOption) {
                                        this.deliverableConfig.selectedSortOption = {
                                            option: selectedSortOption,
                                            sortType: field[fieldId].SORT_ORDER ? field[fieldId].SORT_ORDER :
                                                (this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc')
                                        };
                                        this.deliverableConfig.sortFieldName = this.deliverableConfig.selectedSortOption.option.name;
                                        this.deliverableConfig.sortOrder = this.deliverableConfig.selectedSortOption.sortType;
                                    }
                                }
                            });
                        }
                    }
                    if (!(this.deliverableConfig.selectedSortOption && this.deliverableConfig.selectedSortOption.option && this.deliverableConfig.selectedSortOption.option.name)) {
                        this.deliverableConfig.selectedSortOption = {
                            option: this.deliverableConfig.deliverableSortOptions[0],
                            sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc'
                        };
                        this.deliverableConfig.selectedSortOption = dataSet.DEFAULT_SORT_FIELD ? dataSet.DEFAULT_SORT_FIELD : this.deliverableConfig.selectedSortOption;
                        this.deliverableConfig.sortFieldName = this.deliverableConfig.selectedSortOption.option.name;
                        this.deliverableConfig.sortOrder = this.deliverableConfig.selectedSortOption.sortType;
                    }
                }
                this.setCurrentView();
                this.entityAppDefService.getStatusByCategoryLevel(MPM_LEVELS.DELIVERABLE)
                    .subscribe(statusResponse => {
                        if (statusResponse) {
                            this.deliverableConfig.deliverableStatus = statusResponse;
                            observer.next(true);
                            observer.complete();
                        }
                    }, statusError => {
                        observer.error(statusError);
                    });
            });
        });
    }

    getTaskDetails(): Observable<any> {
        return new Observable(observer => {
            this.taskService.getTaskById(this.deliverableConfig.taskId)
                .subscribe(taskObj => {
                    if (taskObj) {
                        this.deliverableConfig.taskItemId = taskObj['Task-id'].ItemId;
                        this.deliverableConfig.selectedTask = taskObj;
                        this.deliverableTaskType = this.deliverableConfig.selectedTask.TASK_TYPE;
                        this.deliverableConfig.taskStatusId = taskObj.R_PO_STATUS['MPM_Status-id'].Id;
                        // MPMV3-1055
                        const currStatusName = this.projectService.getCurrProjectStatus(this.deliverableConfig.taskStatusId);
                        this.deliverableConfig.isReadOnly = (currStatusName === StatusLevels.FINAL) ? true : this.deliverableConfig.isReadOnly;
                        this.entityAppDefService.getStatusById(taskObj.R_PO_STATUS['MPM_Status-id'].Id)
                            .subscribe(taskStatus => {
                                this.deliverableConfig.selectedTask.statusDetails = taskStatus;
                                observer.next(true);
                                observer.complete();
                            }, taskStatusError => {
                                observer.error(taskStatusError);
                            });
                    } else {
                        observer.error('No task details available.');
                    }
                }, error => {
                    observer.error(error);
                });
        });
    }

    getProjectDetails(): Observable<any> {
        return new Observable(observer => {
            this.projectService.getProjectById(this.deliverableConfig.projectId)
                .subscribe(projectObj => {
                    if (projectObj) {
                        if (projectObj.R_PO_CATEGORY && projectObj.R_PO_CATEGORY['MPM_Category-id']) {
                            this.deliverableConfig.categoryId = projectObj.R_PO_CATEGORY['MPM_Category-id'].Id;
                            this.deliverableConfig.deliverableSortOptions = [];
                            this.deliverableConfig.metaDataFields = [];
                            this.deliverableConfig.deliverableStatus = [];
                            this.deliverableConfig.deliverableList = [];
                            this.deliverableConfig.deliverableTotalCount = 0;
                        }
                        if (projectObj.R_PO_STATUS && projectObj.R_PO_STATUS['MPM_Status-id']) {
                            this.deliverableConfig.projectStatusId = projectObj.R_PO_STATUS['MPM_Status-id'].Id;
                        }
                        const currStatusName = this.projectService.getCurrProjectStatus(this.deliverableConfig.projectStatusId);
                        this.deliverableConfig.projectItemId = projectObj['Project-id'].ItemId;
                        this.deliverableConfig.isReadOnly = (currStatusName === StatusLevels.FINAL) ? true : false;
                        this.deliverableConfig.OTMMFolderID = projectObj.OTMM_FOLDER_ID;
                        this.deliverableConfig.OTMMWorkflowFolderID = projectObj.OTMM_WORKFLOW_FOLDER_ID;
                        this.deliverableConfig.sortFieldName = '';
                        //this.deliverableConfig.sortOrder = 'asc';
                        this.deliverableConfig.sortOrder = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc';
                        this.deliverableConfig.selectedProject = projectObj;
                        const currentUSerID = this.sharingService.getCurrentUserItemID();
                        if (currentUSerID && this.deliverableConfig.selectedProject &&
                            currentUSerID === this.deliverableConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
                            this.deliverableConfig.isProjectOwner = true;
                        }
                        observer.next(true);
                        observer.complete();
                    } else {
                        observer.error('No project details available.');
                    }
                }, error => {
                    observer.error(error);
                });
        });
    }

    getDetails(): Observable<any> {
        return new Observable(observe => {
            this.loaderService.show();
            this.getProjectDetails()
                .subscribe(projectResponse => {
                    this.getTaskDetails()
                        .subscribe(taskResponse => {
                            this.getMPMFields()
                                .subscribe(metadataFieldsResponse => {
                                    if (!(this.searchName || this.savedSearchName || this.advSearchData)) {
                                        this.getDeliverables();
                                    }
                                    observe.next(true);
                                    observe.complete();
                                }, metadataFieldsError => {
                                    this.loaderService.hide();
                                    this.notificationService.error('Something went wrong while getting deliverables');
                                    observe.error('');
                                });
                        }, taskError => {
                            this.loaderService.hide();
                            this.notificationService.error('Something went wrong while getting deliverables');
                            observe.error('');
                        });
                }, projectError => {
                    this.loaderService.hide();
                    this.notificationService.error('Something went wrong while getting deliverables');
                    observe.error('');
                });
        });
    }
    updateSearchConfigId() {
        if (this.deliverableConfig.viewConfig) {
            this.searchConfigId = this.deliverableConfig.viewConfig.R_PO_ADVANCED_SEARCH_CONFIG;
            this.searchChangeEventService.update(this.deliverableConfig.viewConfig);
        }
    }
    editDeliverable(eventData) {
        // this.deliverableConfig.editDeliverable = true;
        eventData['selectedTask'] = this.deliverableConfig.selectedTask;
        this.editDeliverableHandler.emit(eventData);
    }

    masterToogle(event) {
        this.masterSelectedDeliverable = event;
        this.delCount = 0;
        this.refreshDeliverableGrid(event);
    }

    bulkResponse(data) {
        this.refreshDeliverableGrid(data);
        this.delCount = 0;
        this.crBulkResponse = data;
    }

    getBulkResponseData(data) {
        this.bulkResponseData = data;
    }

    ngOnInit() {
        this.isViewByResourceFilterEnabled = false;
        this.enableIndexerFieldRestriction = config.getFieldRestriction();
        this.maximumFieldsAllowed = config.getMaximumFieldsAllowed();
        const route = this.stateService.getRoute(MenuConstants.DELIVERABLES);
        const params = route && route.params ? route.params : null;
        if (params) {
            this.routeService.goToTaskDetails(params.queryParams.isTemplate, params.routeParams.projectId, params.routeParams.taskId,
                params.queryParams.isNewDeliverable, params.queryParams.campaignId, params.queryParams.isEditDeliverable, params.queryParams.deliverableId,
                params.queryParams.sortBy, params.queryParams.sortOrder, params.queryParams.viewByResource, params.queryParams.searchName,
                params.queryParams.savedSearchName, params.queryParams.advSearchData);
        }
        this.appConfig = this.sharingService.getAppConfig();
        this.isRefresh = true;
        if (this.deliverableConfig && this.deliverableConfig.projectId) {
           
            this.deliverableConfig.deliverableResources = [];
            this.activatedRoute.queryParams.subscribe(queryParams => {
               
                let toolbarChange = false;
                this.isNewDeliverable = queryParams.isNewDeliverable;
                this.isEditDeliverable = queryParams.isEditDeliverable;
                this.deliverableId = queryParams.deliverableId;
                this.searchName = queryParams.searchName;
                this.savedSearchName = queryParams.savedSearchName;
                this.advSearchData = queryParams.advSearchData;
                this.selectedCampaignId = queryParams.campaignId;
                if (queryParams.sortBy && this.deliverableConfig.selectedSortOption && this.deliverableConfig.selectedSortOption.option &&
                    this.deliverableConfig.selectedSortOption.option.displayName !== queryParams.sortBy) {
                    const selectedSort = this.deliverableConfig.deliverableSortOptions.find(sort => sort.displayName === queryParams.sortBy);
                    this.deliverableConfig.selectedSortOption = {
                        option: selectedSort,
                        sortType: queryParams.sortOrder ? queryParams.sortOrder :
                            (this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc')
                    };
                    this.deliverableConfig.sortFieldName = this.deliverableConfig.selectedSortOption.option.name;
                    this.deliverableConfig.sortOrder = this.deliverableConfig.selectedSortOption.sortType;
                    toolbarChange = true;
                } else {
                    this.selectedSortOption = queryParams.sortBy ? queryParams.sortBy : null;
                    this.selectedSortOrder = queryParams.sortOrder ? queryParams.sortOrder : null;
                }
                if (queryParams.viewByResource) {
                    const resources = queryParams.viewByResource.split(',');
                    if (JSON.stringify(this.deliverableConfig.selectedResources) !== JSON.stringify(resources)) {
                        this.deliverableConfig.selectedResources = resources;
                        toolbarChange = true;
                    }
                }
                if (toolbarChange && !this.isRefresh) {
                    this.toolBarConfigChange(this.deliverableConfig);
                    toolbarChange = false;
                }
            });

            this.getDetails().subscribe(tempData => {
                this.taskService.deliverableRefreshData.subscribe(data => {
                    if (!(this.searchName || this.savedSearchName || this.advSearchData)) {
                        this.refreshDeliverableGrid(data);
                    }
                    this.taskService.setDeliverableRefreshData(100);
                });
                this.activatedRoute.queryParams.subscribe(queryParams => {
                    if (queryParams.isEditDeliverable && this.isRefresh) {
                        if (queryParams.isEditDeliverable === 'true') {
                            const deliverable = {
                                projectId: this.deliverableConfig.projectId,
                                taskId: this.deliverableConfig.taskId,
                                deliverableId: queryParams.deliverableId,
                                isTemplate: this.deliverableConfig.isTemplate,
                                selectedProject: this.deliverableConfig.selectedProject,
                                selectedTask: this.deliverableConfig.selectedTask
                            };
                            this.editDeliverableHandler.emit(deliverable);
                        }
                    }
                    this.isRefresh = false;
                });
                let initialLoad = true;
                this.tempsearchConditionsSubscribe = this.searchDataService.searchData
                    .subscribe(dataList => {
                        this.searchConditionList = (dataList && Array.isArray(dataList) ? this.otmmService.addRelationalOperator(dataList) : []);
                        if ((this.searchName || this.savedSearchName || this.advSearchData) && initialLoad) {
                            if (dataList && dataList.length > 0) {
                                this.getDeliverables();
                            }
                        } else {
                            if (!initialLoad) {
                                this.getDeliverables();
                            }
                        }
                        initialLoad = false;
                    });
            });

        }
    }
    ngOnDestroy(): void {
        if (this.tempsearchConditionsSubscribe) {
            this.searchChangeEventService.update(null);
            this.tempsearchConditionsSubscribe.unsubscribe();
        }
    }

}
