import { Component, OnInit } from '@angular/core';
import { LoaderService, NotificationService, TaskService, OTMMService, SearchChangeEventService, SearchDataService } from 'mpm-library';
import { ActivatedRoute } from '@angular/router';
import { TaskCreationModalComponent } from '../task-creation-modal/task-creation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { RouteService } from '../../route.service';
import { SharingService } from 'mpm-library';
import { ApplicationConfigConstants } from 'mpm-library';
import { UtilService } from 'mpm-library';
import { SearchConfigConstants } from 'mpm-library';

@Component({
    selector: 'app-task',
    templateUrl: './task.component.html',
    styleUrls: ['./task.component.scss']
})

export class TaskComponent implements OnInit {
    urlParams;
    taskConfig = {
        projectId: null,
        isTemplate: false,
        isTaskListView: true,
        taskViewName: null
    };
    searchConditionList = [];

    constructor(
        private dialog: MatDialog,
        private loaderService: LoaderService,
        private notification: NotificationService,
        private activatedRoute: ActivatedRoute,
        private taskService: TaskService,
        private routeService: RouteService,
        private sharingService: SharingService,
        private utilService: UtilService
    ) { }

    goToDashboardView() {
        this.routeService.goToDashboard(null, null, null, null, null, null, null, null, null, null);
    }

    notificationHandler(event) {
        if (event.message) {
            this.notification.error(event.message);
        }
    }

    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }

    createNewTask(taskConfig) {
        const dialogRef = this.dialog.open(TaskCreationModalComponent, {
            width: '38%',
            disableClose: true,
            data: {
                projectId: taskConfig.projectId,
                isTemplate: taskConfig.isTemplate,
                modalLabel: 'Add Task'
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // this.taskLayoutComponent.refreshTaskGrid(this.taskConfig);
            }
        });
    }

    editTask(taskConfig) {
        const dialogRef = this.dialog.open(TaskCreationModalComponent, {
            width: '38%',
            disableClose: true,
            data: {
                projectId: taskConfig.projectId,
                isTemplate: taskConfig.isTemplate,
                taskId: taskConfig.taskId,
                modalLabel: 'Edit Task'
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // this.taskLayoutComponent.refreshTaskGrid(this.taskConfig);
                this.taskService.setRefreshData(200);
            }
        });
    }

    readUrlParams(callback) {
        this.activatedRoute.queryParams.subscribe(queryParams => {
            this.activatedRoute.parent.parent.params.subscribe(parentRouteParams => {
                callback(parentRouteParams, queryParams);
            });
        });
    }

    ngOnInit() {
        const appConfig = this.sharingService.getAppConfig();
        const currentViewConfig = this.sharingService.getViewConfigByName(SearchConfigConstants.SEARCH_NAME.TASK);
        if (currentViewConfig) {
            const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(currentViewConfig['MPM_View_Config-id'].Id);
            if (userPreference && !this.utilService.isNullOrEmpty(userPreference.IS_LIST_VIEW) && !this.utilService.getBooleanValue(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW])) {
                this.taskConfig.isTaskListView = this.utilService.getBooleanValue(userPreference.IS_LIST_VIEW);
            } else {
                this.taskConfig.isTaskListView = this.utilService.getBooleanValue(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW]) ? true : appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_TASK_VIEW] === 'LIST' ? true : false;
            }
        } else {
            this.taskConfig.isTaskListView = this.utilService.getBooleanValue(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW]) ? true : appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_TASK_VIEW] === 'LIST' ? true : false;
        }
        this.readUrlParams((routeParams, queryParams) => {
            if (queryParams && routeParams) {
                this.taskConfig.projectId = routeParams.projectId;
                this.urlParams = queryParams;
                if (this.urlParams && this.urlParams.isTemplate && this.urlParams.isTemplate === 'true') {
                    this.taskConfig.isTemplate = true;
                } else {
                    this.taskConfig.isTemplate = false;
                }
            }
        });

    }

}
