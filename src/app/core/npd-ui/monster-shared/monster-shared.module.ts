import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonsterConfirmationModalComponent } from './monster-confirmation-modal/monster-confirmation-modal.component';
import { MaterialModule } from 'src/app/material.module';


@NgModule({
  declarations: [MonsterConfirmationModalComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    MonsterConfirmationModalComponent
  ]
})
export class MonsterSharedModule { }
