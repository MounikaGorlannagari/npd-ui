import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from 'mpm-library';

@Component({
  selector: 'app-monster-confirmation-modal',
  templateUrl: './monster-confirmation-modal.component.html',
  styleUrls: ['./monster-confirmation-modal.component.scss']
})
export class MonsterConfirmationModalComponent implements OnInit {

  
  message;
  confirmationComment = '';
  confirmationHeading = 'Confirmation';
  taskName;
  confirmationForm : FormGroup;
  validators = [];

  get formControl() { return this.confirmationForm.controls };
  constructor(
      public dialogRef: MatDialogRef<MonsterConfirmationModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,
      public notificationService: NotificationService,private formBuilder: FormBuilder
  ) { }

  closeDialog() {
      const result = {
          isTrue: false
      };
      this.dialogRef.close();
  }

  ngOnInit() {
      this.confirmationForm = this.formBuilder.group({
        confirmationComment:  [''],
      });
      this.message = this.data.message;
      this.taskName = this.data.name;
      if (this.data.confirmationHeading) {
          this.confirmationHeading = this.data.confirmationHeading;
      }
      this.addValidators();
  }

  addValidators() {
    if(this.data.isCommentRequired) {
        this.validators.push(Validators.required)
      }
      if(this.data.maxLength) {
          this.validators.push(Validators.maxLength(this.data.maxLength))
      }
      this.confirmationForm.controls['confirmationComment'].setValidators(this.validators)
      this.confirmationForm.controls['confirmationComment'].updateValueAndValidity();
  }

  onYesClick() {
      const result = {
          isTrue: true,
          confirmationComment: ''
      };
      if (this.data.hasConfirmationComment) {
          if (this.data.isCommentRequired && this.confirmationComment.trim().length === 0) {
              this.notificationService.error(this.data.errorMessage);
              return;
          }
          result.confirmationComment = this.confirmationComment;
      }
      if(this.data.maxLength && this.formControl.confirmationComment.hasError('maxlength')) {
        this.notificationService.error('Exceeding more than '+this.data.maxLength +' characters');
        return;
      }
      this.dialogRef.close(result);
  }

 /*  checkMaxLength() {
    if(this.data.maxLength && this.confirmationComment.length > this.data.maxLength) {
        return true;
    } else {
        return false;
    }
  } */

}
