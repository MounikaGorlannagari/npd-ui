import { E } from '@angular/cdk/keycodes';
import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService, FacetChangeEventService,  FieldConfigService, FormatToLocalePipe, IndexerService, LoaderService, MPMFieldConstants, MPMSearchOperatorNames, MPMSearchOperators, NotificationService, ProjectBulkCountService, ProjectConstant, SearchChangeEventService, SearchDataService, SearchRequest, SearchResponse, SharingService, StateService, Team, UtilService, ViewConfigService } from 'mpm-library';
import { forkJoin, Observable } from 'rxjs';
import { NPDFacetService } from 'src/app/core/mpm-lib/facet/facet.service';
import { PMDashboardFilterValues } from 'src/app/core/mpm/project-management/filter/project-dashboard-filter';
import { ManagerDashboardComponent } from 'src/app/core/mpm/project-management/manager-dashboard/manager-dashboard.component';
import { RouteService } from 'src/app/core/mpm/route.service';
import { FilterConfigService } from '../../../filter-configs/filter-config.service';
import { NpdSessionStorageConstants } from '../../../request-view/constants/sessionStorageConstants';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { BulkEditProjectComponent } from '../../bulk-edit-project/bulk-edit-project/bulk-edit-project.component';

import { NpdFieldConstants } from '../../constants/ProjectSearchConstants';
import { NpdProjectService } from '../../services/npd-project.service';
import { BusinessConstants } from '../../../request-view/constants/BusinessConstants';
@Component({
  selector: 'app-npd-manager-dashboard',
  templateUrl: './npd-manager-dashboard.component.html',
  styleUrls: ['./npd-manager-dashboard.component.scss'],
})
export class NpdManagerDashboardComponent
  extends ManagerDashboardComponent
  implements OnInit
{
  GET_ALL_OVERVIEWSECTION_NS =
    'http://schemas/NPDLookUpDomainModel/Overview_Section/operations';
  GET_ALL_OVERVIEWSECTION_WS = 'getAllOverviewSections';
  GET_ALL_ACCESS_ROLE_WS = 'GetR_PM_ACCESS_ROLE';

  GET_ALL_OVERVIEW_FIELDS_CONFIG_WS = 'GetR_PM_FIELDS_CONFIG';
  leadMarketRegion;

  constructor(
    public loaderService: LoaderService,
    public routeService: RouteService,
    public searchDataService: SearchDataService,
    public changeDetectorRef: ChangeDetectorRef,
    public dialog: MatDialog,
    public media: MediaMatcher,
    public notificationService: NotificationService,
    public sharingService: SharingService,
    public searchChangeEventService: SearchChangeEventService,
    public facetChangeEventService: FacetChangeEventService,
    public fieldConfigService: FieldConfigService,
    public viewConfigService: ViewConfigService,
    public indexerService: IndexerService,
    public utilService: UtilService,
    public projectBulkCountService: ProjectBulkCountService,
    public activatedRoute: ActivatedRoute,
    public stateService: StateService,
    public router: Router,
    public formatToLocalePipe: FormatToLocalePipe,
    public appService: AppService,
    public npdProjectService: NpdProjectService,
    public monsterConfigApiService: MonsterConfigApiService,
    public bulkEditDialog: MatDialog,
    public filterConfigService: FilterConfigService,
    public facetService: NPDFacetService,
    private mpmRouteService: RouteService
  ) {
    super(
      loaderService,
      routeService,
      searchDataService,
      changeDetectorRef,
      dialog,
      media,
      notificationService,
      sharingService,
      searchChangeEventService,
      facetChangeEventService,
      fieldConfigService,
      viewConfigService,
      indexerService,
      utilService,
      projectBulkCountService,
      activatedRoute,
      stateService,
      router,
      formatToLocalePipe,
      appService,
      npdProjectService,
      facetService
    );
  }

  openBulkEditDialog(): void {
    if (this.selectedProjectCounts !== 0) {
      let roleAccess = {
        'E2E PM': true,
        'Ops PM': true,
        'Corp PM': true,
        'Regional Technical Manager': true,
        RTM: true,
        'Project Specialist': true,
        GFG: true,
        CorpLead: true,
        SecondaryPackagingAWS: true,
        FPMS : true,
      };
      let isTask4AndTask5 = true;
      let isTask39 = true;
      let isTask39NotCompleted = true;
      let isTask42NotCompleted = true;
      let isTask42 = true;
      let isFGProject = true;
      let isLeadMarket = true;
      let isTask34Exist = true;
      let bulkEditFields = [];
      let rolesName = [];
      let isActiveProjects = this.selectedProjectData.some(
        (data) => data.PROJECT_STATUS_VALUE !== 'Active'
      );
      let overviewConstants = JSON.parse(
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_OVERVIEWFIELDS_CONFIG
        )
      );
      if (!isActiveProjects) {
        this.leadMarketRegion =
          this.selectedProjectData[0]?.NPD_PROJECT_LEAD_MARKET;
        for (let selectedProject of this.selectedProjectData) {
          // if (selectedProject?.NPD_PROJECT_LEAD_MARKET !== this.leadMarketRegion) {
          //     isLeadMarket = false;
          // }
          if (
            (selectedProject?.NPD_PROJECT_IS_TASK4_COMPLETED === undefined ||
              selectedProject?.NPD_PROJECT_IS_TASK4_COMPLETED === 'true' ||
              selectedProject?.NPD_PROJECT_IS_TASK4_COMPLETED === true) &&
            (selectedProject.NPD_PROJECT_IS_TASK5_COMPLETED === undefined ||
              selectedProject?.NPD_PROJECT_IS_TASK5_COMPLETED === 'true' ||
              selectedProject?.NPD_PROJECT_IS_TASK5_COMPLETED === true)
          ) {
            isTask4AndTask5 = false;
          }
          // if ((selectedProject?.NPD_PROJECT_IS_TASK4_COMPLETED === undefined || selectedProject?.NPD_PROJECT_IS_TASK4_COMPLETED === 'false' || selectedProject?.NPD_PROJECT_IS_TASK4_COMPLETED === false) && (selectedProject.NPD_PROJECT_IS_TASK5_COMPLETED === undefined || selectedProject?.NPD_PROJECT_IS_TASK5_COMPLETED === 'true' || selectedProject?.NPD_PROJECT_IS_TASK5_COMPLETED === true)) {
          //     isTask4AndTask5 = false;
          // }
          // if ((selectedProject?.NPD_PROJECT_IS_TASK4_COMPLETED === undefined || selectedProject?.NPD_PROJECT_IS_TASK4_COMPLETED === 'true' || selectedProject?.NPD_PROJECT_IS_TASK4_COMPLETED === true) && (selectedProject.NPD_PROJECT_IS_TASK5_COMPLETED === undefined || selectedProject?.NPD_PROJECT_IS_TASK5_COMPLETED === 'false' || selectedProject?.NPD_PROJECT_IS_TASK5_COMPLETED === false)) {
          //     isTask4AndTask5 = false;
          // }

          if (
            selectedProject?.NPD_PROJECT_IS_TASK39_EXIST === undefined ||
            selectedProject?.NPD_PROJECT_IS_TASK39_EXIST === 'false' ||
            selectedProject?.NPD_PROJECT_IS_TASK39_EXIST === false ||
            selectedProject?.NPD_PROJECT_IS_TASK39_COMPLETED == 'true'
          ) {
            isTask39 = false;
          }
          if (selectedProject?.NPD_PROJECT_IS_TASK39_COMPLETED == 'true') {
            isTask39NotCompleted = false;
          }
          if (selectedProject?.NPD_PROJECT_IS_TASK42_COMPLETED == 'true') {
            isTask42NotCompleted = false;
          }
          if (
            selectedProject?.NPD_PROJECT_IS_TASK42_EXIST === undefined ||
            selectedProject?.NPD_PROJECT_IS_TASK42_EXIST === 'false' ||
            selectedProject?.NPD_PROJECT_IS_TASK42_EXIST === false ||
            selectedProject?.NPD_PROJECT_IS_TASK42_COMPLETED == 'true'
          ) {
            isTask42 = false;
          }
          if (
            selectedProject?.NPD_PROJECT_IS_FG_PROJECT === undefined ||
            selectedProject?.NPD_PROJECT_IS_FG_PROJECT === 'false' ||
            selectedProject?.NPD_PROJECT_IS_FG_PROJECT === false
          ) {
            isFGProject = false;
          }
          if (
            selectedProject?.NPD_PROJECT_IS_TASK_34_EXIST_OR_INACTIVE ===
              undefined ||
            selectedProject?.NPD_PROJECT_IS_TASK_34_EXIST_OR_INACTIVE ===
              'false' ||
            selectedProject?.NPD_PROJECT_IS_TASK_34_EXIST_OR_INACTIVE === false
          ) {
            isTask34Exist = false;
          }
          let loginUser = this.sharingService.getCurrentUserDisplayName();
          roleAccess['E2E PM'] = roleAccess['E2E PM']
            ? this.filterConfigService.isE2EPMRole() &&
              selectedProject?.NPD_PROJECT_E2E_PM_NAME === loginUser
            : roleAccess['E2E PM'];
          roleAccess['Corp PM'] = roleAccess['Corp PM']
            ? this.filterConfigService.isCorpPMRole() &&
              selectedProject?.NPD_PROJECT_CORP_PM_NAME === loginUser
            : roleAccess['Corp PM'];
          roleAccess['Ops PM'] = roleAccess['Ops PM']
            ? this.filterConfigService.isOPSPMRole() &&
              selectedProject?.NPD_PROJECT_OPS_PM_NAME === loginUser
            : roleAccess['Ops PM'];
          roleAccess['Regional Technical Manager'] = roleAccess[
            'Regional Technical Manager'
          ]
            ? this.filterConfigService.isRegionalTechnicalManagerRole() &&
              selectedProject?.NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME ===
                loginUser
            : roleAccess['Regional Technical Manager'];
          roleAccess.RTM = roleAccess.RTM
            ? this.filterConfigService.isRegsTrafficManager()
            : roleAccess.RTM;
          roleAccess['Project Specialist'] = roleAccess['Project Specialist']
            ? this.filterConfigService.isProjectSpecialistRole() &&
              selectedProject?.NPD_PROJECT_PR_SPECIALIST_NAME === loginUser
            : roleAccess['Project Specialist'];
          roleAccess.GFG = roleAccess.GFG
            ? this.filterConfigService.isGFGRole() &&
              selectedProject?.NPD_PROJECT_GFG_NAME === loginUser
            : roleAccess.GFG;
          roleAccess.CorpLead = roleAccess.CorpLead
            ? this.filterConfigService.isCorpProjectLeader()
            : roleAccess.CorpLead;
          roleAccess.SecondaryPackagingAWS = roleAccess.SecondaryPackagingAWS
            ? this.filterConfigService.isSecondaryPackagingRole() &&
              selectedProject?.NPD_PROJECT_SECONDARY_AW_SPECIALIST_NAME ===
                loginUser
            : roleAccess.SecondaryPackagingAWS;
          roleAccess.FPMS = roleAccess.FPMS
            ? this.filterConfigService.isFPMSRole() && 
            selectedProject?.NPD_PROJECT_FPMS_NAME ===
                loginUser
            : roleAccess.FPMS;
        }
        function bulkEditFieldMapping(fields) {
          if (
            isTask4AndTask5 &&
            isFGProject &&
            isTask39 &&
            isTask42 &&
            isLeadMarket
          ) {
            bulkEditFields.push(...fields.fieldsConfig);
          } else {
            if (fields.SectionName === 'Project Classification') {
              // if (!isTask4AndTask5 && isLeadMarket) {
            if (isTask4AndTask5) {
                bulkEditFields.push(...fields.fieldsConfig);
              }
            } else if (fields.SectionName === 'OPS PM Data') {
              for (let field of fields.fieldsConfig) {
                if (
                  isTask39 &&
                  field.FieldName === 'Trial Week confirmed with bottler'
                ) {
                  bulkEditFields.push(field);
                } else if (
                  isTask42 &&
                  field.FieldName ===
                    'Final Agreed 1st Production Week confirmed with bottler'
                ) {
                  bulkEditFields.push(field);
                } else if (
                  isTask39 &&
                  isTask39NotCompleted &&
                  field.FieldName === 'Trial Volume (24 EQ cases)'
                ) {
                  bulkEditFields.push(field);
                } else if (
                  isTask42 &&
                  isTask42NotCompleted &&
                  field.FieldName === '1st Production volume (24 EQ cases)'
                ) {
                  bulkEditFields.push(field);
                } else if (
                  isFGProject &&
                  field.FieldName === 'BOM Sent Actual Date'
                ) {
                  bulkEditFields.push(field);
                }
                // ||field.FieldName==="Trial Volume (24 EQ cases)"
                else if (
                  isTask34Exist &&
                  field.FieldName === 'Name of Can Company (s)'
                ) {
                  bulkEditFields.push(field);
                } else if (
                  field.FieldName !== 'Trial Week confirmed with bottler' &&
                  field.FieldName !== 'Trial Volume (24 EQ cases)' &&
                  field.FieldName !==
                    'Final Agreed 1st Production Week confirmed with bottler' &&
                  field.FieldName !== 'BOM Sent Actual Date' &&
                  field.FieldName !== '1st Production volume (24 EQ cases)' &&
                  field.FieldName !== 'BOM Sent Actual Date' &&
                  field.FieldName !== 'Name of Can Company (s)'
                ) {
                  bulkEditFields.push(field);
                }
              }
            } else if (fields.SectionName === 'TECHNICAL') {
              bulkEditFields.push(...fields.fieldsConfig);
            } else if (fields.SectionName === 'Project Overview') {
              bulkEditFields.push(...fields.fieldsConfig);
            } else if (fields.SectionName === 'Project Core Data - SAP') {
              bulkEditFields.push(...fields.fieldsConfig);
            } else if (fields.SectionName === 'Program Manager') {
              bulkEditFields.push(...fields.fieldsConfig);
            } else if (fields.SectionName === 'Project Team') {
              bulkEditFields.push(...fields.fieldsConfig);
            }else if (fields.SectionName === 'Secondary Packaging') {
              bulkEditFields.push(...fields.fieldsConfig);
            }
          }
        }
        if (
          JSON.parse(sessionStorage.getItem('IS_ADMIN')) == true ||
          JSON.parse(sessionStorage.getItem('IS_ADMIN')) == 'true' ||
          this.filterConfigService.isProgramManagerRole()
        ) {
          for (let fields of overviewConstants) {
            bulkEditFieldMapping(fields);
          }
        } else {
          for (let key in roleAccess) {
            if (roleAccess[key] === true) {
              rolesName.push(key);
            }
          }
          for (let fields of overviewConstants) {
            let isRoleincluded = rolesName.some((role) =>
              fields.accessRole.includes(role)
            );

            if (isRoleincluded) {
              bulkEditFieldMapping(fields);
            } else {
              if (fields.SectionName === 'Project Team' 
              || fields.SectionName === 'Project Core Data - SAP'
              || fields.SectionName === 'Secondary Packaging') {
                let duplicatefields = [];
                let uniquefields = [];
                let fieldID = [];

                for (let field of fields.fieldsConfig) {
                  if (roleAccess['E2E PM'] === true) {
                    if (
                      field.FieldName === 'SP Scoped?'
                    ) {
                      duplicatefields.push(field);
                    }
                  }
                  if (roleAccess['Corp PM'] === true) {
                    if (
                      field.FieldName === 'Regulatory Lead' ||
                      field.FieldName === 'GFG' ||
                      field.FieldName === 'Project Specialist' || 
                      field.FieldName === 'FPMS' || 
                      field.FieldName === 'SP Scoped?'
                    ) {
                      duplicatefields.push(field);
                    }
                  }
                  if (roleAccess['Ops PM'] === true) {
                    if (field.FieldName === 'Ops Planner') {
                      duplicatefields.push(field);
                    }
                  }
                  if (roleAccess.RTM === true) {
                    if (field.FieldName === 'Regulatory Lead') {
                      duplicatefields.push(field);
                    }
                  }
                  if (roleAccess.SecondaryPackagingAWS === true) {
                    if (field.FieldName === 'Secondary Artwork Manager') {
                      duplicatefields.push(field);
                    }
                  }
                  if (roleAccess.CorpLead === true) {
                    if (
                      field.FieldName === 'Regulatory Lead' ||
                      field.FieldName === 'Corp PM' ||
                      field.FieldName === 'Project Specialist' || 
                      field.FieldName === 'FPMS'
                    ) {
                      duplicatefields.push(field);
                    }
                  }
                  if (roleAccess.FPMS === true) {
                    if (
                      field.FieldName === 'FPMS' || 
                      field.FieldName === 'GFG' || 
                      field.FieldName === 'Formula / HBC#'
                    ) {
                      duplicatefields.push(field);
                    }
                  }
                  if(this.filterConfigService.isSecondaryPackagingRole()){
                    if(field.FieldName === 'SP Scoped?'){
                      duplicatefields.push(field);
                    }
                  }
                }
                if (duplicatefields.length > 1) {
                  uniquefields = duplicatefields.filter((field) => {
                    const isDuplicateFieldId = fieldID.includes(field.FieldID);
                    if (!isDuplicateFieldId) {
                      fieldID.push(field.FieldID);
                      return true;
                    }
                    return false;
                  });
                } else {
                  uniquefields = duplicatefields;
                }
                bulkEditFields.push(...uniquefields);
              }
            }
          }
        }
        if (!bulkEditFields.length) {
          this.notificationService.warn(
            "User don't have the permission to edit the selected projects fields"
          );
        } else {
          const dialogRef = this.bulkEditDialog.open(BulkEditProjectComponent, {
            // width: '700px',
            // height: '65%',
            disableClose: true,
            data: {
              bulkEditFields: bulkEditFields,
              selectedProjectData: this.selectedProjectData,
              leadMarketRegion: this.leadMarketRegion,
              isTask4AndTask5: isTask4AndTask5,
            },
          });
          dialogRef.afterClosed().subscribe((data) => {
            this.refresh();
          });
        }
      } else {
        this.notificationService.warn(
          'Selected list of projects contains OnHold/Cancelled/Completed projects'
        );
      }
    } else {
      this.notificationService.warn(
        'Please select a project(s) to perform bulk edit operation.'
      );
    }
  }
  formFilterCondition(
    filterValues: any,
    IS_CAMPAIGN_TYPE: boolean,
    IS_PROJECT_TYPE: boolean,
    isCampaignProject: boolean
  ): Array<any> {
    /* const searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(ProjectConstant.GET_ALL_PROJECT_SEARCH_CONDITION_LIST); */
    let searchCondition;
    let condition = null;
    if (isCampaignProject) {
      searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(
        ProjectConstant.GET_ALL_PROJECT_SEARCH_CONDITION_LIST
      );
      condition = this.fieldConfigService.createConditionObject(
        MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ID,
        MPMSearchOperators.IS,
        MPMSearchOperatorNames.IS,
        MPMSearchOperatorNames.AND,
        this.campaignId
      );
      if (condition) {
        searchCondition.push(condition);
      }
    } else if (IS_CAMPAIGN_TYPE) {
      // MPM_V3 -1799
      searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(
        ProjectConstant.GET_ALL_CAMPAIGN_SEARCH_CONDITION_LIST
      );
      condition = this.fieldConfigService.createConditionObject(
        MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_TYPE,
        MPMSearchOperators.IS,
        MPMSearchOperatorNames.IS,
        MPMSearchOperators.AND,
        'CAMPAIGN'
      );
      if (condition) {
        searchCondition.push(condition);
      }
      if (
        !(
          filterValues === PMDashboardFilterValues.ALL_TEMPLATES ||
          filterValues === PMDashboardFilterValues.ALL_PROJECTS ||
          filterValues === PMDashboardFilterValues.ALL_CAMPAIGNS
        )
      ) {
        condition = this.fieldConfigService.createConditionObject(
          MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_OWNER_ID,
          MPMSearchOperators.IS,
          MPMSearchOperatorNames.IS,
          MPMSearchOperators.AND,
          this.sharingService.getCurrentUserItemID()
        );
        if (condition) {
          searchCondition.push(condition);
        }
      } else {
        /* if (this.sharingService.getCRActions()) {
     
                    this.sharingService.getCRActions().forEach((eachTeam: Team, index) => {
                        condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TEAM_ID,
                            MPMSearchOperators.IS, MPMSearchOperatorNames.IS, index === 0 ? MPMSearchOperators.AND : MPMSearchOperators.OR, eachTeam['MPM_Teams-id'].Id);
                        if (condition) {
                            searchCondition.push(condition);
                        }
                    });
                } */
      }
    } else {
      searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(
        ProjectConstant.GET_ALL_PROJECT_SEARCH_CONDITION_LIST
      );
      condition = this.fieldConfigService.createConditionObject(
        MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE,
        MPMSearchOperators.IS,
        MPMSearchOperatorNames.IS,
        MPMSearchOperators.AND,
        IS_PROJECT_TYPE ? 'PROJECT' : 'TEMPLATE'
      );
      if (condition) {
        searchCondition.push(condition);
      }
      if (
        !(
          filterValues === PMDashboardFilterValues.ALL_TEMPLATES ||
          filterValues === PMDashboardFilterValues.ALL_PROJECTS
        )
      ) {
        condition = this.fieldConfigService.createConditionObject(
          MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_OWNER_ID,
          MPMSearchOperators.IS,
          MPMSearchOperatorNames.IS,
          MPMSearchOperators.AND,
          this.sharingService.getCurrentUserItemID()
        );
        condition.left_paren = '(';
        let npdSearchCondition = [];
        npdSearchCondition = [
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS.PROJECT_E2E_PM_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserItemID()
          ), //this.sharingService.getCurrentUserItemID()
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS.PROJECT_CORP_PM_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserItemID()
          ),
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS.PROJECT_OPS_PM_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserItemID()
          ), //this.sharingService.getCurrentUserItemID()
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS
              .PROJECT_REGIONAL_TECHNICAL_MANAGER_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserItemID()
          ),
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS.PROJECT_OPS_PLANNER_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserItemID()
          ),
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS.PROJECT_GFG_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserItemID()
          ),
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS.PROJECT_PR_SPECIALIST_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserItemID()
          ),
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS.PROJECT_REGULATORY_LEAD_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserItemID()
          ),
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS.PROJECT_COMMERCIAL_LEAD_ID,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserItemID()
          ),
          this.fieldConfigService.createConditionObject(
            NpdFieldConstants.PROJECT_FIELDS.PROJECT_FPMS_NAME,
            MPMSearchOperators.IS,
            MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR,
            this.sharingService.getCurrentUserDisplayName()
          ),
        ];
        let cond = this.fieldConfigService.createConditionObject(
          NpdFieldConstants.PROJECT_FIELDS.PROJECT_SECONDARY_AW_MANAGER,
          MPMSearchOperators.IS,
          MPMSearchOperatorNames.IS,
          MPMSearchOperators.OR,
          this.sharingService.getCurrentUserDisplayName()
        );
        cond.right_paren = ')';
        if (condition) {
          searchCondition.push(condition);
        }
        searchCondition = searchCondition?.concat(npdSearchCondition);
        searchCondition.push(cond);
        /*   searchCondition.push(searchCond1);
                  searchCondition.push(searchCond2);   /*  let searchCond1 = this.fieldConfigService.createConditionObject("NPD_PROJECT_E2E_PM_ITEM_ID",
                      MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.OR, 'F8B156E1037111E6E9CB0FBF3334FBBF.81924');//this.sharingService.getCurrentUserItemID()
                  let searchCond2 = this.fieldConfigService.createConditionObject("NPD_PROJECT_CORP_PM_ITEM_ID", 
                      MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.OR, this.sharingService.getCurrentUserItemID()); */
      } else {
        if (this.sharingService.getCRActions()) {
          this.sharingService
            .getCRActions()
            .forEach((eachTeam: Team, index) => {
              condition = this.fieldConfigService.createConditionObject(
                MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TEAM_ID,
                MPMSearchOperators.IS,
                MPMSearchOperatorNames.IS,
                index === 0 ? MPMSearchOperators.AND : MPMSearchOperators.OR,
                eachTeam['MPM_Teams-id'].Id
              );
              if (condition) {
                searchCondition.push(condition);
              }
            });
        }
      }
    }
    return searchCondition;
  }
  setBulkEditProjectOverviewFieldConfig() {
    let fieldsConfig = [];
    if (
      sessionStorage.getItem(
        NpdSessionStorageConstants.ALL_OVERVIEWFIELDS_CONFIG
      ) == null
    ) {
      this.loaderService.show();
      this.appService
        .invokeRequest(
          this.GET_ALL_OVERVIEWSECTION_NS,
          this.GET_ALL_OVERVIEWSECTION_WS,
          null
        )
        .subscribe((response) => {
          let overviewSections = [];
          overviewSections = response.Overview_Section;
          for (var i = 0; i < overviewSections.length; i++) {
            let sectionName = overviewSections[i]?.['SectionName'];
            let parameter = {
              'Overview_Section-id': {
                Id: overviewSections[i]['Overview_Section-id'].Id,
                ItemId: overviewSections[i]['Overview_Section-id'].ItemId,
              },
            };

            forkJoin([
              this.appService.invokeRequest(
                this.GET_ALL_OVERVIEWSECTION_NS,
                this.GET_ALL_ACCESS_ROLE_WS,
                parameter
              ),
              this.appService.invokeRequest(
                this.GET_ALL_OVERVIEWSECTION_NS,
                this.GET_ALL_OVERVIEW_FIELDS_CONFIG_WS,
                parameter
              ),
            ]).subscribe((forkedData) => {
              this.loaderService.hide();
              let accessRole = [];
              let fieldConfigs = [];
              if (forkedData[0].Group.length > 1) {
                for (var j = 0; j < forkedData[0].Group.length; j++) {
                  accessRole.push(forkedData[0].Group[j].Display_Name);
                }
              } else {
                accessRole.push(forkedData[0].Group.Display_Name);
              }
              if (forkedData[1].Overview_Fields_Config.length > 1) {
                for (
                  var j = 0;
                  j < forkedData[1].Overview_Fields_Config.length;
                  j++
                ) {
                  let field: any = {};
                  let data = forkedData[1].Overview_Fields_Config[j];
                  field.DataType = data?.DataType;
                  field.FieldName = data?.FieldName;
                  field.FieldType = data?.FieldType;
                  field.FieldID = data['Overview_Fields_Config-id'].Id;
                  field.ProjectMapperId = data?.ProjectMapperId;
                  field.ProjectMapperName = data?.ProjectMapperName;
                  field.RequestMapper = data?.RequestMapper;
                  field.IndexerMapperId = data?.IndexerMapperId;
                  field.IndexerMapperName = data?.IndexerMapperName;
                  field.IsRequest = data?.IsRequest;
                  field.IsProject = data?.IsProject;
                  field.IsCalculationRequired = data?.IsCalculationRequired;
                  field.IsDynamic = data?.IsDynamic;
                  field.IsUser = data?.IsUser;
                  field.SessionStorageConstant = data?.SessionStorageConstant;
                  fieldConfigs.push(field);
                }
              } else {
                let field: any = {};
                let data = forkedData[1].Overview_Fields_Config;
                field.DataType = data?.DataType;
                field.FieldName = data?.FieldName;
                field.FieldType = data?.FieldType;
                field.FieldID = data['Overview_Fields_Config-id'].Id;
                field.ProjectMapperId = data?.ProjectMapperId;
                field.ProjectMapperName = data?.ProjectMapperName;
                field.ProjectfieldID = data?.ProjectfieldID;
                field.RequestMapper = data?.RequestMapper;
                field.IndexerMapperId = data?.IndexerMapperId;
                field.IndexerMapperName = data?.IndexerMapperName;
                field.IsRequest = data?.IsRequest;
                field.IsProject = data?.IsProject;
                field.IsCalculationRequired = data?.IsCalculationRequired;
                field.IsDynamic = data?.IsDynamic;
                field.IsUser = data?.IsUser;
                field.SessionStorageConstant = data?.SessionStorageConstant;
                fieldConfigs.push(field);
              }
              let fields = {
                SectionName: sectionName,
                accessRole: accessRole,
                fieldsConfig: fieldConfigs,
              };
              fieldsConfig.push(fields);
              sessionStorage.setItem(
                NpdSessionStorageConstants['ALL_OVERVIEWFIELDS_CONFIG'],
                JSON.stringify(fieldsConfig)
              );
            });
          }
        });
    }
  }
  openSelectedProjects() {
    let flag =0 
    for (let selectedProject of this.selectedProjectData) {
      flag =1;
      if (selectedProject.ITEM_ID) {
        this.mpmRouteService.goToProjectDetailViewNewTab(
          selectedProject.ITEM_ID.split('.')[1],
          false,
          null
        );
      }
    }
    if(!flag){
      this.notificationService.warn('Please select atleast one project to open in new window')
    }
  }
  ngOnInit(): void {
    super.ngOnInit();
    this.setBulkEditProjectOverviewFieldConfig();
  }
}
