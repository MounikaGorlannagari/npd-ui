export interface NpdProjectUpdateObj {
    'requestID': string;
    'projectItemId':string;
    
    'bottlerCommunicationStatus'?: string;
    'bottlerPushedOutProduction'?: string;
    'briefDescOfDelay'?: string;
    'hasChangeBeenAligned'?: number;
    'isFGProject'?: boolean;
    'mecCommercial_BottlerRequested'?: boolean;
    'npdOnTrackOriginalCP1Date'?: string;
    'opsAlignedDPWeek'?:string;
    'opsAlignedDPYear'?:string;

    'otherTBC'?: number;
    'other2TBC'?: boolean;
    'pmCP1PhaseDelay'?: boolean;
    'pmSlackTime'?: string;
    'riskSummary'?:string;
    'statusCriticalItems'?:string;
    'currentCommercialAlignedDate'?:string;

    'timelineRisk'?: number;
    'programTag'?:string;
    'trialCommercialProdIncluded'?: boolean;
    'targetCP2Date'?: boolean;
    'comTechOpsAgreedDPWeek'?: string;
    'rag'?:string;
    'projectHealth'?:string;

    'cp1RevisedDate'?: number;
    'noOfWeeksIncrease'?: boolean;
    'eanTargetWeek'?: boolean;
    'eanActualWeek'?: string;
    'prodInfoTargetWeek'?:string;
    'prodInfoActualWeek'?:string;

    'labelUploadedActualDate'?: number;
    'bottlerSpecificComments'?: boolean;
    'bottlerAndCanTargetWeek'?: boolean;
    'bottlerandCanActualWeek'?: string;
    'itemNumber'?:string;
    'numberOfCanCompaniesRequired'?:string;

    'nameOfCanCompany'?: number;
    'poReceivedActualWeek'?: boolean;
    'palletLabelReceivedTargetWeek'?: boolean;
    'palletLabelReceivedActualWeek'?: string;
    'palletLabelSentTargetWeek'?:string;
    'palletLabelSentActualWeek'?:string;

}
