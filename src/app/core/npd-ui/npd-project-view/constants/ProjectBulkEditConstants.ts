export const ProjectBulkEditConstants = {

    'Country Of Manufacture(Final)': {
        draftManufacturingLocation: {
            itemId: '',
            value: ''
        },
        earlyManufacturingSite: {
            itemId: '',
            value: ''
        },
        projectType: {
            itemId: '',
            value: ''
        },
        newFg: {
            value: ''
        },
        newRnDFormula: {
            value: ''
        },
        newHBCFormula: {
            value: ''
        },
        earlyProjectClassificationDesc: {
            value: ''
        },
        earlyProjectClassification: {
            value: ''
        },
        newPrimaryPackaging: {
            value: ''
        },
        secondaryPackaging: {
            value: ''
        },
        registrationDossier: {
            value: ''
        },
        preProdReg: {
            value: ''
        },
        postProdReg: {
            value: ''
        },
        leadFormula:{
            value: ''
        },

    },
    'Reporting Project Type': {
        projectType: {
            itemId: '',
            value: ''
        },
        projectSubType: {
            itemId: '',
            value: ''
        },
        projectDetail: {
            itemId: '',
            value: ''
        }


    },
    DEPENDENT_FIELDS_CONFIG: {
        'Reporting Project Type': {
            'projectType': {
                projectMappers: {
                    ProjectMapperId: 'PMReportingProjectTypeItemId',
                    ProjectMapperName: 'PMReportingProjectType'
                },
                indexerMappers: {
                    IndexerMapperId: 'NPD_PROJECT_PM_REPORTING_PROJECT_TYPE_ITEM_ID',
                    IndexerMapperName: 'NPD_PROJECT_PM_REPORTING_PROJECT_TYPE'
                },
                requestMappers: {
                    RequestMapper: 'R_PO_CP_PROJECT_TYPE/CP_Project_Type-id/ItemId'
                }
            },
            'projectSubType': {
                projectMappers: {
                    ProjectMapperId: 'PMReportingSubProjectTypeItemId',
                    ProjectMapperName: 'PMReportingSubProjectType'
                },
                indexerMappers: {
                    IndexerMapperId: 'NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE_ITEM_ID',
                    IndexerMapperName: 'NPD_PROJECT_PM_REPORTING_SUB_PROJECT_TYPE'
                },
                requestMappers: {
                    RequestMapper: 'R_PO_PROJECT_SUB_TYPE/Project_Sub_Type-id/ItemId'
                }
            },
            'projectDetail': {
                projectMappers: {
                    ProjectMapperId: 'PMReportingProjectDetailItemId',
                    ProjectMapperName: 'PMReportingProjectDetail'
                },
                indexerMappers: {
                    IndexerMapperId: 'NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL_ITEM_ID',
                    IndexerMapperName: 'NPD_PROJECT_PM_REPORTING_PROJECT_DETAIL'
                },
                requestMappers: {
                    RequestMapper: 'R_PO_PROJECT_DETAIL/Project_Detail-id/ItemId'
                }
            }
        },
        'Country Of Manufacture(Final)': {
            draftManufacturingLocation: {
                projectMappers: {
                    ProjectMapperId: 'FinalManufacturingLocationItemId',
                    ProjectMapperName: 'FinalManufacturingLocation'
                },
                indexerMappers: {
                    IndexerMapperId: 'NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID',
                    IndexerMapperName: 'NPD_PROJECT_FINAL_MANUFACTURING_LOCATION'
                },
            },
            earlyManufacturingSite: {
                projectMappers: {
                    ProjectMapperId: 'FinalManufacturingSiteItemId',
                    ProjectMapperName: 'FinalManufacturingSite'
                },
                indexerMappers: {
                    IndexerMapperId: 'NPD_PROJECT_FINAL_MANUFACTURING_SITE_ITEM_ID',
                    IndexerMapperName: 'NPD_PROJECT_FINAL_MANUFACTURING_SITE'
                },
            },
            // projectType:{
            //     projectMappers: {
            //         ProjectMapperId: 'NewProjectTypeItemId',
            //         ProjectMapperName: 'NewProjectType'
            //     },
            //     indexerMappers: {
            //         IndexerMapperId: 'NPD_PROJECT_NEW_PROJECT_TYPE_ITEM_ID',
            //         IndexerMapperName: 'NPD_PROJECT_NEW_PROJECT_TYPE'
            //     },
            // },
            // newFg:{
            //     projectMappers: {
            //         ProjectMapperName: 'NewFG'
            //     },
            //     indexerMappers: {
            //         IndexerMapperName: 'NPD_PROJECT_NEW_FG'
            //     },
            // },
            // newRnDFormula:{
            //     projectMappers: {
            //         ProjectMapperName: 'NewRDFormula'
            //     },
            //     indexerMappers: {
            //         IndexerMapperName: 'NPD_PROJECT_NEW_RD_FORMULA'
            //     },

            // },
            // newHBCFormula:{
            //     projectMappers: {
            //         ProjectMapperName: 'NewHBCFormula'
            //     },
            //     indexerMappers: {
            //         IndexerMapperName: 'NPD_PROJECT_NEW_HBC_FORMULA'
            //     },
            // },
            // earlyProjectClassificationDesc:{
            //     projectMappers: {
            //         ProjectMapperName: 'FinalProjectClassificationDescription'
            //     },
            //     indexerMappers: {
            //         IndexerMapperName: 'PROJECT_FINAL_PROJECT_CLASSIFICATION_DESC'
            //     },
            // },
            // earlyProjectClassification:{
            //     projectMappers: {
            //         ProjectMapperName: 'ProjectFinalClassification'
            //     },
            //     indexerMappers: {
            //         IndexerMapperName: 'PROJECT_FINAL_PROJECT_CLASSIFICATION_DESC'
            //     },
            // },
            // newPrimaryPackaging:{
            //     projectMappers: {
            //         ProjectMapperName: 'NewPrimaryPackaging'
            //     },
            //     indexerMappers: {
            //         IndexerMapperName: 'NPD_PROJECT_NEW_PRIMARY_PACK'
            //     },
            // },
            // secondaryPackaging:{
            //     projectMappers: {
            //         ProjectMapperName: 'NewSecondaryPackaging'
            //     },
            //     indexerMappers: {
            //         IndexerMapperName: 'NPD_PROJECT_NEW_SECONDARY_PACK'
            //     },
            // },
            registrationDossier: {
                projectMappers: {
                    ProjectMapperName: 'NewRegDossier'
                },
                indexerMappers: {
                    IndexerMapperName: 'NPD_PROJECT_NEW_REGISTRATION_DOSSIER'
                },
            },
            preProdReg: {
                projectMappers: {
                    ProjectMapperName: 'NewPreProduction'
                },
                indexerMappers: {
                    IndexerMapperName: 'NPD_PROJECT_NEW_PRE_PRODUCTION'
                },
            },
            postProdReg: {
                projectMappers: {
                    ProjectMapperName: 'NewPostProduction'
                },
                indexerMappers: {
                    IndexerMapperName: 'NPD_PROJECT_NEW_POST_PRODUCTION'
                },
            }

        }
    },
    Form_Group: {
        'Country Of Manufacture(Final)': 'pmClassificationForm',
        'Reporting Project Type': 'pmReportingFieldForm'
    },

    BottlerCommunicationStatusConfig: [
        { displayName: 'NO COMS TO BOTTLER', value: 'NO_COMS_TO_BOTTLER' },
        { displayName: 'NONE', value: 'NONE' },
    ],
}
