import { EventEmitter, Injectable } from '@angular/core';
import { ProjectUpdateObj } from 'mpm-library/lib/project/project-overview/project.update';
import { DeliverableUpdateObject } from 'mpm-library/lib/project/tasks/deliverable/deliverable-creation/deliverable.update';
import { TaskUpdateObj } from 'mpm-library/lib/project/tasks/task-creation/task.update';
import { AppService, CampaignUpdateObj, IndexerService, SearchRequest, SearchResponse, SharingService, UtilService } from 'mpm-library';
import { BehaviorSubject, observable, Observable, Subscription } from 'rxjs';
import { NpdProjectUpdateObj } from '../constants/NpdProjectUpdate';
import { I } from '@angular/cdk/keycodes';
import { NpdSessionStorageConstants } from '../../request-view/constants/sessionStorageConstants';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { BusinessConstants } from '../../request-view/constants/BusinessConstants';
import { OverViewConstants } from '../constants/projectOverviewConstants';
import { ObserversModule } from '@angular/cdk/observers';
import { EntityService } from '../../services/entity.service';
import { EntityListConstant } from '../../request-view/constants/EntityListConstant';
import { ManagerDashboardComponent } from 'src/app/core/mpm/project-management/manager-dashboard/manager-dashboard.component';

@Injectable({
  providedIn: 'root',
})
export class NpdProjectService {
  loggedInUser: string;
  filterConfigService: any;
  isAdmin: boolean;
  linkedMarketsAccess: boolean;
  activePreferenceData: any;
  resetProjectUserPref: boolean;

  constructor(
    private indexerService: IndexerService,
    private appService: AppService,
    private monsterConfigApiService: MonsterConfigApiService,
    private sharingService: SharingService,
    private utilService: UtilService,
    private entityService: EntityService
  ) { }

  UPDATE_NPD_PROJECT_METHOD_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  UPDATE_NPD_PROJECT_METHOD_WS = 'UpdateProject';

  GET_TASKS_NOT_CREATED_METHOD_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  GET_TASKS_NOT_CREATED_METHOD_WS = 'GetTasksNotCreated';

  ADD_NEW_TASKS_WORKFLOW_METHOD_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  ADD_NEW_TASKS_WORKFLOW_METHOD_WS = 'AddNewTasksToWorkflow';

  CALCULATE_RESIDUAL_FIELDS_METHOD_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  CALCULATE_RESIDUAL_FIELDS_METHOD_WS = 'CalculateResidualFields';

  GET_ALL_SECONDARY_PACK_LEADTIMES_NS =
    'http://schemas/NPDLookUpDomainModel/Secondary_Pack_Lead_Times/operations';
  GET_ALL_SECONDARY_PACK_LEADTIMES_WS = 'GetAllSecondaryPackLeadTimes';

  GET_ALL_PRINT_SUPPLIERS_NS =
    'http://schemas/NPDLookUpDomainModel/Print_Supplier/operations';
  GET_ALL_PRINT_SUPPLIERS_WS = 'GetAllPrintSuppliers';

  GET_ALL_TRIAL_SUPERVISION_NPD_NS =
    'http://schemas/NPDLookUpDomainModel/Trial_Supervision_NPD/operations';
  GET_ALL_TRIAL_SUPERVISION_NPD_WS = 'GetAllTrialSupervisionNPD';

  GET_ALL_TRIAL_SUPERVISION_QUALITY_NS =
    'http://schemas/NPDLookUpDomainModel/Trial_Supervision_Quality/operations';
  GET_ALL_TRIAL_SUPERVISION_QUALITY_WS = 'GetAllTrialSupervisionQuality';

  GET_ALL_AUDIT_USERS_NS =
    'http://schemas/NPDLookUpDomainModel/Audit_Users/operations';
  GET_ALL_AUDIT_USERS_WS = 'GetAllAuditUsers';

  GET_ALL_CAN_COMPANY_NS =
    'http://schemas/NPDLookUpDomainModel/Can_Supplier_Data/operations';
  GET_ALL_CAN_COMPANY_WS = 'GetAllCanCompany';

  GET_ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS_NS =
    'http://schemas/NPDLookUpDomainModel/Trial_Protocol_Written_By/operations';
  GET_ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS_WS =
    'GetAllTrialProtocolWrittenByUsers';

  GET_ALL_TASTING_REQUIREMENT_NS =
    'http://schemas/NPDLookUpDomainModel/Tasting_Requirement/operations';
  GET_ALL_TASTING_REQUIREMENT_WS = 'GetAllTastingRequirement';

  GET_ALL_WHO_WILL_COMPLETE_USERS_NS =
    'http://schemas/NPDLookUpDomainModel/Tech_Qual_Users/operations';
  GETALL_WHO_WILL_COMPLETE_USERS_WS = 'GetAllTechQualUsers';

  GET_ALL_MPM_FIELDS_FOR_VIEW_NS =
    'http://schemas/NPDLookUpDomainModel/MPM_Field_Section_Name/operations';
  GET_ALL_MPM_FIELDS_FOR_VIEW_WS = 'GetAllMPMFieldSectionNames';

  GET_ARTWORK_STATUS_OVERVIEW_NS =
    'http://schemas.npd.monster.com/jobrequest/1.0';
  GET_ARTWORK_STATUS_OVERVIEW_WS = 'GetArtworkStatusOverviewByMPMJobsAndGroups';

  GET_PROJECT_TASK_CONFIGS_NS = 'http://schemas.npd.monster.com/jobrequest/1.0';
  GET_PROJECT_TASK_CONFIGS_WS = 'GetProjectTasksConfigsByProjectClassificationNumber'

  allProjectFormProperties: any = {};

  // Forming search field for mpm-indexer input
  invokeFirstComponentFunction = new EventEmitter();
  subsVar: Subscription;
  getResetTrigger() {
    this.resetProjectUserPref = true;
    this.invokeFirstComponentFunction.emit(this.resetProjectUserPref);
  }

  selectedMenu = new BehaviorSubject<any>(false);
  setData() {
    this.selectedMenu.next(null);
  }

  getSearchField(
    type,
    field_id,
    operator_id,
    operator_name,
    value?,
    field_operator?
  ) {
    let searchField = {
      type: type,
      field_id: field_id,
      relational_operator_id: operator_id,
      relational_operator_name: operator_name,
      value: value,
      relational_operator: field_operator,
    };
    return searchField;
  }

  getProjectDetailsById(projectId): Observable<any> {
    let searchConditionList = [
      this.getSearchField(
        'string',
        'CONTENT_TYPE',
        'MPM.OPERATOR.CHAR.IS',
        'is',
        'MPM_PROJECT'
      ),
      this.getSearchField(
        'string',
        'ID',
        'MPM.OPERATOR.CHAR.IS',
        'is',
        projectId,
        'AND'
      ),
    ];

    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: '',
      displayable_field_list: { displayable_fields: [] },
      search_condition_list: {
        search_condition: searchConditionList,
      },
      facet_condition_list: {
        facet_condition: [],
      },
      sorting_list: {
        sort: [],
      },
      cursor: {
        page_index: 0,
        page_size: 1,
      },
    };
    return new Observable((observer) => {
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          observer.next(response.data);
          observer.complete();
        },
        () => {
          observer.error();
        }
      );
    });
  }

  getRequestDetailsById(requestItemd): Observable<any> {
    let searchConditionList = [
      this.getSearchField(
        'string',
        'CONTENT_TYPE',
        'MPM.OPERATOR.CHAR.IS',
        'is',
        'REQUEST'
      ),
      this.getSearchField(
        'string',
        'ITEM_ID',
        'MPM.OPERATOR.CHAR.IS',
        'is',
        requestItemd,
        'AND'
      ),
    ];

    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: '',
      // displayable_fields: [],
      search_condition_list: {
        search_condition: searchConditionList,
      },
      facet_condition_list: {
        facet_condition: [],
      },
      sorting_list: {
        sort: [],
      },
      cursor: {
        page_index: 0,
        page_size: 1,
      },
    };
    return new Observable((observer) => {
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          observer.next(response.data);
          observer.complete();
        },
        () => {
          observer.error();
        }
      );
    });
  }

  createNewProjectUpdateObject(
    projectValue: any,
    projectType: string,
    ownerUserId: string
  ): ProjectUpdateObj {
    const newProject: ProjectUpdateObj = {
      ProjectId: {
        Id: null,
      },
      ProjectName: projectValue.name
        ? projectValue.name.replace(/\s+/g, ' ')
        : projectValue.name,
      DueDate: projectValue.endDate,
      ProjectType: projectType,
      RPOPriority: {
        PriorityID: {
          Id: projectValue.priority,
        },
      },
      RPOStatus: {
        StatusID: {
          Id: projectValue.status,
        },
      },
      RPOProjectOwner: {
        IdentityID: {
          Id: ownerUserId,
        },
      },
      /*  RPOCategoryMetadata: {
         CategoryMetadataID: {
           Id: projectValue.categoryMetadata['value'],
         }
       }, */
      //CustomMetadata: null
    };
    return newProject;
  }

  compareTwoProjectDetails(
    updatedProject:
      | ProjectUpdateObj
      | CampaignUpdateObj
      | TaskUpdateObj
      | DeliverableUpdateObject,
    oldProject:
      | ProjectUpdateObj
      | CampaignUpdateObj
      | TaskUpdateObj
      | DeliverableUpdateObject
  ):
    | ProjectUpdateObj
    | TaskUpdateObj
    | DeliverableUpdateObject
    | CampaignUpdateObj {
    if (oldProject) {
      const comparedObject:
        | ProjectUpdateObj
        | CampaignUpdateObj
        | TaskUpdateObj
        | DeliverableUpdateObject = Object.assign(updatedProject);
      const excludedObject: Array<string> = [
        'ProjectId',
        'TaskId',
        'DeliverableId',
        'CustomMetadata',
      ];
      for (const keys in updatedProject) {
        if (
          excludedObject.indexOf(keys) < 0 &&
          this.isEqual(updatedProject[keys], oldProject[keys])
        ) {
          delete comparedObject[keys];
        } else if (keys === 'CustomMetadata') {
          /* if(!comparedObject[keys])
            delete comparedObject[keys]; */
          if (
            comparedObject[keys] &&
            comparedObject[keys] &&
            comparedObject[keys]['Metadata']['metadataFields'].length === 0
          )
            delete comparedObject[keys];
        }
      }
      return comparedObject;
    }
    return updatedProject;
  }

  isEqual(newObject: any, oldObject: any): boolean {
    if (newObject && oldObject) {
      if (typeof newObject === 'string') {
        return newObject === oldObject;
      } else {
        return JSON.stringify(newObject) === JSON.stringify(oldObject);
      }
    } else if ((newObject && !oldObject) || (!newObject && oldObject)) {
      return false;
    } else if (!newObject && !oldObject) {
      return true;
    }
    return false;
  }

  compareTwoCustomProjectDetails(
    updatedProject: NpdProjectUpdateObj,
    oldProject: NpdProjectUpdateObj
  ) {
    if (oldProject) {
      const comparedObject: NpdProjectUpdateObj = Object.assign(updatedProject);
      const excludedObject: Array<string> = ['requestID', 'projectItemId'];
      for (const keys in updatedProject) {
        if (
          excludedObject.indexOf(keys) < 0 &&
          this.isEqual(updatedProject[keys], oldProject[keys])
        ) {
          delete comparedObject[keys];
        } else if (
          keys === 'CustomMetadata' &&
          comparedObject[keys]['Metadata']['metadataFields'].length === 0
        ) {
          delete comparedObject[keys];
        }
      }
      return comparedObject;
    }
    return updatedProject;
  }

  getCustomMetadata(customFieldsGroup) {
    const customMetadataValues = [];
    //let npdObj :NpdProjectUpdateObj;
    let projectUpdateObj = {};
    if (
      customFieldsGroup &&
      customFieldsGroup['controls'] &&
      customFieldsGroup['controls']['fieldset'] &&
      customFieldsGroup['controls']['fieldset'].controls &&
      customFieldsGroup['controls']['fieldset'].controls.length > 0
    ) {
      const controls: Array<any> =
        customFieldsGroup['controls']['fieldset'].controls;
      for (const control of controls) {
        if (control?.value !== null) {
          // && control?.value !== ''
          const metadataFieldObj = {};
          metadataFieldObj['dataType'] = control.fieldset?.dataType;
          metadataFieldObj['defaultValue'] = control.value;
          metadataFieldObj['fieldId'] = control.fieldset?.id;
          /*  if(Object.keys(npdCustomObj).find(key=> key == metadataFieldObj['fieldId'])) {
             npdCustomObj[metadataFieldObj['fieldId']] = control.value
           } */
          projectUpdateObj[metadataFieldObj['fieldId']] = control.value;
          //customMetadataValues.push(metadataFieldObj);
        }
      }
    }

    return projectUpdateObj; //{metadataFields: customMetadataValues } ;
  }

  updateCustomNpdProjectDetails(projectObj) {
    let projectUpdateObj = {
      updateProject: projectObj,
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.UPDATE_NPD_PROJECT_METHOD_NS,
          this.UPDATE_NPD_PROJECT_METHOD_WS,
          projectUpdateObj
        )
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getTasksNotCreatedDetails(projectId) {
    let params = {
      ProjectId: projectId,
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.GET_TASKS_NOT_CREATED_METHOD_NS,
          this.GET_TASKS_NOT_CREATED_METHOD_WS,
          params
        )
        .subscribe(
          (response) => {
            let tasks = [];
            tasks = response.Tasks?.['Task_Role_Mapping']
              ? response.Tasks['Task_Role_Mapping']
              : [];
            observer.next(tasks);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getProjectTasksConfigsByClassificationNumber(classificationNumber) {
    let params = {
      ClassificationNumber: classificationNumber,
    }
    return new Observable((observer) => {
      this.appService.invokeRequest(this.GET_PROJECT_TASK_CONFIGS_NS, this.GET_PROJECT_TASK_CONFIGS_WS, params).subscribe(response => {
        let taskConfigData = [];
        let uniqueTasksConfig = [];
        if (response.tuple && response.tuple.length > 0) {
          taskConfigData = response.tuple.map(task => ({
            taskName: task.old.ProjectTaskConfig.TASK_NAME,
            taskDescription: task.old.ProjectTaskConfig.TASK_DESCRIPTION
          }));

          //Removing duplicates taskConfig name
          uniqueTasksConfig = taskConfigData.reduce((unique, taskconfig) => {
            if (!unique.some(uniquetask => uniquetask.taskName === taskconfig.taskName && uniquetask.taskDescription === taskconfig.taskDescription)) {
              unique.push(taskconfig);
            }
            return unique;
          }, []);
          taskConfigData = uniqueTasksConfig;
        }
        observer.next(taskConfigData);
        observer.complete();
      },
        (error) => {
          observer.error(error);
        }
      )
    })
  }

  initiateTasksCreationWorkflow(tasks, projectId, classificationNumber) {
    let params = {
      Tasks: {
        TaskName: tasks.map((data) => {
          return data.TaskName;
        }),
      },
      ProjectId: projectId, //'1851397'
      ProjectClassificationNumber: classificationNumber
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.ADD_NEW_TASKS_WORKFLOW_METHOD_NS,
          this.ADD_NEW_TASKS_WORKFLOW_METHOD_WS,
          params
        )
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  calculateResidualFields(projectId) {
    let params = {
      ProjectID: projectId,
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.CALCULATE_RESIDUAL_FIELDS_METHOD_NS,
          this.CALCULATE_RESIDUAL_FIELDS_METHOD_WS,
          params
        )
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getAllTrialProtocolWrittenByUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS
            )
          )
        );
      } else {
        this.fetchAllTrialProtocolWrittenByUsers(() => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS
              )
            )
          );
        });
      }
    });
  }

  fetchAllTrialProtocolWrittenByUsers(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS_NS,
        this.GET_ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS_WS,
        null
      )
      .subscribe(
        (response) => {
          let trialProtocolWrittenByUsersOptions = [];
          const options =
            this.monsterConfigApiService.getPropertyListFromResponse(
              response,
              'Trial_Protocol_Written_By'
            );
          trialProtocolWrittenByUsersOptions =
            this.monsterConfigApiService.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS,
            JSON.stringify(trialProtocolWrittenByUsersOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
        }
      );
  }

  getAllTastingRequirements(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_TASTING_REQUIREMENT
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_TASTING_REQUIREMENT
            )
          )
        );
      } else {
        this.fetchAllTastingRequirements(() => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_TASTING_REQUIREMENT
              )
            )
          );
        });
      }
    });
  }

  fetchAllTastingRequirements(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_TASTING_REQUIREMENT_NS,
        this.GET_ALL_TASTING_REQUIREMENT_WS,
        null
      )
      .subscribe(
        (response) => {
          let tastingRequirementOptions = [];
          const options =
            this.monsterConfigApiService.getPropertyListFromResponse(
              response,
              'Tasting_Requirement'
            );
          tastingRequirementOptions =
            this.monsterConfigApiService.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_TASTING_REQUIREMENT,
            JSON.stringify(tastingRequirementOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_TASTING_REQUIREMENT,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
        }
      );
  }

  getAllWhoWillCompleteUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_WHO_WILL_COMPLETE_USERS
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_WHO_WILL_COMPLETE_USERS
            )
          )
        );
      } else {
        this.fetchAllWhoWillCompleteUsers(() => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_WHO_WILL_COMPLETE_USERS
              )
            )
          );
        });
      }
    });
  }

  fetchAllWhoWillCompleteUsers(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_WHO_WILL_COMPLETE_USERS_NS,
        this.GETALL_WHO_WILL_COMPLETE_USERS_WS,
        null
      )
      .subscribe(
        (response) => {
          let tastingRequirementOptions = [];
          const options =
            this.monsterConfigApiService.getPropertyListFromResponse(
              response,
              'Tech_Qual_Users'
            );
          tastingRequirementOptions =
            this.monsterConfigApiService.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_WHO_WILL_COMPLETE_USERS,
            JSON.stringify(tastingRequirementOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_WHO_WILL_COMPLETE_USERS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
        }
      );
  }

  getAllSecondaryPackLeadTimes(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_SECONDARY_PACK_LEAD_TIMES
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_SECONDARY_PACK_LEAD_TIMES
            )
          )
        );
      } else {
        this.fetchAllSecondaryPackLeadTimes(() => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_SECONDARY_PACK_LEAD_TIMES
              )
            )
          );
        });
      }
    });
  }

  fetchAllSecondaryPackLeadTimes(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_SECONDARY_PACK_LEADTIMES_NS,
        this.GET_ALL_SECONDARY_PACK_LEADTIMES_WS,
        null
      )
      .subscribe(
        (response) => {
          let secondaryPackLeadTimesOptions = [];
          const options =
            this.monsterConfigApiService.getPropertyListFromResponse(
              response,
              'Secondary_Pack_Lead_Times'
            );
          secondaryPackLeadTimesOptions =
            this.monsterConfigApiService.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_SECONDARY_PACK_LEAD_TIMES,
            JSON.stringify(secondaryPackLeadTimesOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_SECONDARY_PACK_LEAD_TIMES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
        }
      );
  }

  getAllPrintSuppliers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_PRINT_SUPPLIERS
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_PRINT_SUPPLIERS
            )
          )
        );
      } else {
        this.fetchAllPrintSuppliers(() => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_PRINT_SUPPLIERS
              )
            )
          );
        });
      }
    });
  }

  fetchAllPrintSuppliers(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_PRINT_SUPPLIERS_NS,
        this.GET_ALL_PRINT_SUPPLIERS_WS,
        null
      )
      .subscribe(
        (response) => {
          let secondaryPackLeadTimesOptions = [];
          const options =
            this.monsterConfigApiService.getPropertyListFromResponse(
              response,
              'Print_Supplier'
            );
          secondaryPackLeadTimesOptions =
            this.monsterConfigApiService.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PRINT_SUPPLIERS,
            JSON.stringify(secondaryPackLeadTimesOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PRINT_SUPPLIERS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
        }
      );
  }

  getAllTrialSupervisionNPD(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_NPD
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_NPD
            )
          )
        );
      } else {
        this.fetchAllTrialSupervisionNPD(() => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_NPD
              )
            )
          );
        });
      }
    });
  }

  fetchAllTrialSupervisionNPD(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_TRIAL_SUPERVISION_NPD_NS,
        this.GET_ALL_TRIAL_SUPERVISION_NPD_WS,
        null
      )
      .subscribe(
        (response) => {
          let trialSupervisionNPDOptions = [];
          const options =
            this.monsterConfigApiService.getPropertyListFromResponse(
              response,
              'Trial_Supervision_NPD'
            );
          trialSupervisionNPDOptions =
            this.monsterConfigApiService.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_NPD,
            JSON.stringify(trialSupervisionNPDOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_NPD,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
        }
      );
  }

  getAllTrialSupervisionQuality(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_QUALITY
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_QUALITY
            )
          )
        );
      } else {
        this.fetchAllTrialSupervisionQuality(() => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_QUALITY
              )
            )
          );
        });
      }
    });
  }

  fetchAllTrialSupervisionQuality(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_TRIAL_SUPERVISION_QUALITY_NS,
        this.GET_ALL_TRIAL_SUPERVISION_QUALITY_WS,
        null
      )
      .subscribe(
        (response) => {
          let trialSupervisionQualityOptions = [];
          const options =
            this.monsterConfigApiService.getPropertyListFromResponse(
              response,
              'Trial_Supervision_Quality'
            );
          trialSupervisionQualityOptions =
            this.monsterConfigApiService.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_QUALITY,
            JSON.stringify(trialSupervisionQualityOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_QUALITY,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
        }
      );
  }

  getAllAuditUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_AUDIT_USERS) !==
        null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_AUDIT_USERS)
          )
        );
      } else {
        this.fetchAllAuditUsers(() => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_AUDIT_USERS)
            )
          );
        });
      }
    });
  }

  fetchAllAuditUsers(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_AUDIT_USERS_NS,
        this.GET_ALL_AUDIT_USERS_WS,
        null
      )
      .subscribe(
        (response) => {
          let auditUserOptions = [];
          const options =
            this.monsterConfigApiService.getPropertyListFromResponse(
              response,
              'Audit_Users'
            );
          auditUserOptions =
            this.monsterConfigApiService.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_AUDIT_USERS,
            JSON.stringify(auditUserOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_AUDIT_USERS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
        }
      );
  }

  getAllCanCompanies(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_CAN_COMPANIES) !==
        null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_CAN_COMPANIES)
          )
        );
      } else {
        this.fetchAllCanCompanies(() => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_CAN_COMPANIES
              )
            )
          );
        });
      }
    });
  }

  fetchAllCanCompanies(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_CAN_COMPANY_NS,
        this.GET_ALL_CAN_COMPANY_WS,
        null
      )
      .subscribe(
        (response) => {
          let canCompanyOptions = [];
          const options =
            this.monsterConfigApiService.getPropertyListFromResponse(
              response,
              'Can_Supplier_Data'
            );
          canCompanyOptions =
            this.monsterConfigApiService.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_CAN_COMPANIES,
            JSON.stringify(canCompanyOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_CAN_COMPANIES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
        }
      );
  }

  mapGovernanceMilestoneConfigurations(isShow, isEditable) {
    let governanceMilestoneData = {
      model: {
        ngIf: isShow,
      },
      stage: {
        disabled: true,
        ngIf: isShow,
      },
      targetStartDate: {
        disabled: true,
        ngIf: isShow,
      },
      liveDate: {
        disabled: true,
        ngIf: isShow,
      },
      revisedDate: {
        disabled: false,
        ngIf: true,
      },
      decisionDate: {
        disabled: false,
        ngIf: isShow,
      },
      decision: {
        disabled: false,
        ngIf: isShow,
      },
      comments: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
    };
    return governanceMilestoneData;
  }

  mapGovernanceMilestoneBaseConfig() {
    let govConfig = {
      isProject: true,
      editGovernanceMilestone: true,
    };
    return govConfig;
  }

  mapSecondaryPackageConfigurations(isShow, isEditable, addButton) {
    let secondaryPackagingData = {
      model: {
        ngIf: isShow,
      },
      addBtn: {
        disabled: addButton ? false : true,
      },
      deleteBtn: {
        disabled: addButton ? false : true,
      },
      required: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      regsNeeded: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      packFormat: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      mpmJobNo: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      mpmGroup: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      printSupplier: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      itemNumber: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      approvedDieLineAvailable: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      colourApproval: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      invoiceApproved: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      statusComments: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      dateDevelopmentCompleted: {
        disabled: true,
        ngIf: isShow,
      },
      dateProofingCompleted: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      isArtwork: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
    };
    return secondaryPackagingData;
  }

  mapCanSupplierConfigurations(isShow, isEditable, project) {
    let canSupplierConfig = {
      model: {
        ngIf: isShow,
      },
      addBtn: {
        disabled: true,
      },
      isEdit: {
        disabled:
          (project?.NPD_PROJECT_IS_TASK_34_EXIST_OR_INACTIVE === 'true' ||
            project?.NPD_PROJECT_IS_TASK_34_EXIST_OR_INACTIVE == true) &&
            isEditable
            ? false
            : true,
      },
      targetWeek: {
        disabled: true,
        ngIf: isShow,
        value: project?.[OverViewConstants.OVERVIEW_MAPPINGS.targetWeek],
      },
      actualWeek: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: project?.[OverViewConstants.OVERVIEW_MAPPINGS.actualWeek],
      },
      itemNumber: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: project?.[OverViewConstants.OVERVIEW_MAPPINGS.itemNumber],
      },
      noOfCanCompaniesRequired: {
        disabled: true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.noOfCanCompaniesRequired
          ],
      },
      nameOfCanCompany: {
        disabled:
          (project?.NPD_PROJECT_IS_TASK_34_EXIST_OR_INACTIVE === 'true' ||
            project?.NPD_PROJECT_IS_TASK_34_EXIST_OR_INACTIVE == true) &&
            isEditable
            ? false
            : true,

        ngIf: isShow,
        value: project?.[OverViewConstants.OVERVIEW_MAPPINGS.nameOfCanCompany],
      },
      nameOfCanCompanyId: {
        value:
          project?.[OverViewConstants.OVERVIEW_MAPPINGS.nameOfCanCompanyId],
      },
    };
    return canSupplierConfig;
  }

  mapprogrammeManagerReportingConfigurations(
    isShow,
    isEditable,
    project,
    linkedMarketsEditable
  ) {
    let programmeManagerReporting = {
      model: {
        ngIf: isShow,
      },
      projectType: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROGRAMME_MANAGER_REPORTING
            .projectTypeItemId
          ],
        formControl: isShow,
        validators: {
          required: isEditable ? false : true,
        },
      },
      projectSubType: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROGRAMME_MANAGER_REPORTING
            .projectSubTypeItemId
          ],
        formControl: isShow,
        validators: {
          required: isEditable ? false : true,
        },
      },
      projectDetail: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROGRAMME_MANAGER_REPORTING
            .projectDetailItemId
          ],
        formControl: isShow,
        validators: {
          required: isEditable ? false : true,
        },
      },
      delivery: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROGRAMME_MANAGER_REPORTING
            .deliveryItemId
          ],
        formControl: isShow,
        validators: {
          required: isEditable ? false : true,
          length: {
            max: 500,
          },
        },
      },
      linkedMarkets: {
        // disabled: true,//isEditable ? false : true,
        disabled: linkedMarketsEditable ? false : true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROGRAMME_MANAGER_REPORTING
            .linkedMarkets
          ],
        formControl: isShow,
        validators: {
          required: isEditable ? false : true,
          length: {
            max: 64,
          },
        },
      },
      pmComments: {
        disabled: isEditable ? false : true,
        ngIf: isShow, //(isPM || (isRequestView)) ? true : false,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROGRAMME_MANAGER_REPORTING
            .comments
          ],
        formControl: isShow,
        validators: {
          length: {
            max: 2000,
          },
        },
      },
      other1: {
        disabled: isEditable ? false : true,
        ngIf: isShow, //(isPM || (isRequestView)) ? true : false,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROGRAMME_MANAGER_REPORTING
            .other1
          ],
        formControl: isShow,
        validators: {
          required: false, //true,
        },
      },
      other2: {
        disabled: isEditable ? false : true,
        ngIf: isShow, //(isPM || (isRequestView)) ? true : false,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROGRAMME_MANAGER_REPORTING
            .other2
          ],
        formControl: isShow,
        validators: {
          required: false, //true,
        },
      },
    };
    return programmeManagerReporting;
  }
  mapArtworkStatusOverviewConfiguration(isShow, isEditable) {
    let artworkStatusOverview = {
      model: {
        ngIf: isShow,
      },
      mpmJobNo: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      mpmGroup: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
      },
      artworkSystemBtn: {
        disabled: false,
        ngIf: isShow,
      },
    };
    return artworkStatusOverview;
  }
  clearFormProperties() {
    this.allProjectFormProperties = {};
  }

  setCustomFormProperty(property, value) {
    if (this.allProjectFormProperties) {
      this.allProjectFormProperties[property] = value;
    }
  }

  mapProjectClassificationConfigurations(isShow, isEditable, project) {
    let projectClassificationData = {
      model: {
        ngIf: isShow,
      },
      earlyProjectClassification: {
        disabled: true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .finalProjectClassification
          ],
        formControl: isShow,
        validators: {
          required: false,
          length: null,
        },
      },
      earlyManufacturingSite: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .finalManufacturingSiteId
          ],
        formControl: isShow,
        validators: {
          required: isEditable ? true : false,
          length: null,
        },
      },
      draftManufacturingLocation: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .finalManufacturingLocationId
          ],
        formControl: isShow,
        validators: {
          required: true,
          length: null,
        },
      },
      projectType: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .projectType
          ],
        formControl: isShow,
        validators: {
          required: true,
          length: null,
        },
      },
      newFg: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: this.getBooleanValue(
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .newFg
          ]
        ),
        formControl: isShow,
        validators: {
          required: true,
          length: null,
        },
      },
      newRnDFormula: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: this.getBooleanValue(
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .newRnDFormula
          ]
        ),
        formControl: isShow,
        validators: {
          required: true,
          length: null,
        },
      },
      newHBCFormula: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: this.getBooleanValue(
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .newHBCFormula
          ]
        ),
        formControl: isShow,
        validators: {
          required: true,
          length: null,
        },
      },
      earlyProjectClassificationDesc: {
        disabled: true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .finalProjectClassificationDesc
          ],
        formControl: isShow,
        validators: {
          required: false,
          length: null,
        },
      },
      newPrimaryPackaging: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: this.getBooleanValue(
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .newPrimaryPackaging
          ]
        ),
        formControl: isShow,
        validators: {
          required: true,
          length: null,
        },
      },
      secondaryPackaging: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: this.getBooleanValue(
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .secondaryPackaging
          ]
        ),
        formControl: isShow,
        validators: {
          required: true,
          length: null,
        },
      },
      // registrationDossier: {
      //   disabled: true,
      //   ngIf: isShow,
      //   value: this.getBooleanValue(
      //     project?.[
      //       OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
      //         .registrationDossier
      //     ]
      //   ),
      //   formControl: isShow,
      //   validators: {
      //     required: false,
      //     length: null,
      //   },
      // },
      // preProdReg: {
      //   disabled: true,
      //   ngIf: isShow,
      //   value: this.getBooleanValue(
      //     project?.[
      //       OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
      //         .preProdReg
      //     ]
      //   ),
      //   formControl: isShow,
      //   validators: {
      //     required: false,
      //     length: null,
      //   },
      // },
      // postProdReg: {
      //   disabled: true,
      //   ngIf: isShow,
      //   value: this.getBooleanValue(
      //     project?.[
      //       OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
      //         .postProdReg
      //     ]
      //   ),
      //   formControl: isShow,
      //   validators: {
      //     required: false,
      //     length: null,
      //   },
      // },
      newClassificationRational: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value:
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .classificationRational
          ],
        formControl: isShow,
        validators: {
          required: false,
          length: {
            max: 500,
          },
        },
      },

      leadMarketRegulatoryClassification: {
        value: project?.NPD_PROJECT_LEAD_MARKET,
      },
      registrationClassification: {
        disabled: true,
        ngIf: isShow,
        value: this.getBooleanValue(
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .registrationClassification
          ]
        ),
        formControl: isShow,
        validators: {
          required: false,
          length: null,
        },
      },
      registrationRequirement: {
        disabled: true,
        ngIf: isShow,
        value: this.getBooleanValue(
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .registrationRequirement
          ]
        ),
        formControl: isShow,
        validators: {
          required: false,
          length: null,
        },
      },
      leadFormula: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: this.getBooleanValue(
          project?.[
          OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
            .leadFormula
          ]
        ),
        formControl: isShow,
        validators: {
          required: false,
          length: null,
        },
      },
    };
    return projectClassificationData;
  }
  mapBulkProjectClassificationConfigurations(
    isShow,
    isEditable,
    leadMarketRegion
  ) {
    let projectClassificationData = {
      model: {
        ngIf: isShow,
      },
      earlyManufacturingSite: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: '',
        formControl: isShow,
        validators: {
          required: isEditable ? true : false,
          length: null,
        },
      },
      draftManufacturingLocation: {
        disabled: isEditable ? false : true,
        ngIf: isShow,
        value: '',
        formControl: isShow,
        validators: {
          required: true,
          length: null,
        },
      },

      registrationDossier: {
        disabled: true,
        ngIf: isShow,
        value: '',
        formControl: isShow,
        validators: {
          required: false,
          length: null,
        },
      },
      preProdReg: {
        disabled: true,
        ngIf: isShow,
        formControl: isShow,
        value: '',
        validators: {
          required: false,
          length: null,
        },
      },
      postProdReg: {
        disabled: true,
        ngIf: isShow,
        value: '',
        formControl: isShow,
        validators: {
          required: false,
          length: null,
        },
      },
      leadMarketRegulatoryClassification: {
        value: leadMarketRegion,
      },
    };
    return projectClassificationData;
  }

  getRoleByName(roleName: string) {
    return this.sharingService
      .getAllRoles()
      .find((role) => role.ROLE_NAME === roleName);
  }

  getUsersByRole(roleName, SessionStrgConstant, callBack) {
    const parameters = {
      //role: this.getRoleByName(BusinessConstants.ROLE_CORPPM) && this.getRoleByName(BusinessConstants.ROLE_CORPPM).ROLE_DN,
      role: this.getRoleByName(roleName)?.ROLE_DN,
      depth: '1',
      sort: 'ascending',
    };

    this.appService.getUsersForRole(parameters).subscribe(
      (response) => {
        const users = [];
        if (response.tuple) {
          //&& response.tuple.length > 0
          let arrayResponse = this.getListAsResponse(response.tuple);
          arrayResponse.map((user) => {
            users.push({
              name: user.old.entry.cn.string,
              value: user.old.entry.description.string,
              actualValue: user.old.entry.authenticationuser.string,
              displayName: user.old.entry.description.string,
              isActive:
                user.old.entry.enable?.string == 'true' ? 'true' : 'false',
            });
          });
          sessionStorage.setItem(
            NpdSessionStorageConstants[SessionStrgConstant],
            JSON.stringify(users)
          );
        }
        if (callBack) {
          callBack();
        }
      },
      (error) => {
        console.log(error);
        sessionStorage.setItem(
          NpdSessionStorageConstants[SessionStrgConstant],
          JSON.stringify([])
        );
        if (callBack) {
          callBack();
        }
      }
    );
  }

  getAllUsersByRole(roleDN): Observable<any[]> {
    const parameters = {
      //role: this.getRoleByName(BusinessConstants.ROLE_CORPPM) && this.getRoleByName(BusinessConstants.ROLE_CORPPM).ROLE_DN,
      role: roleDN,
      depth: '1',
      sort: 'ascending',
    };

    return new Observable((observer) => {
      this.appService.getUsersForRole(parameters).subscribe(
        (response) => {
          let users = [];
          if (response.tuple) {
            //&& response.tuple.length > 0
            let arrayResponse = this.getListAsResponse(response.tuple);
            arrayResponse.map((user) => {
              users.push({
                name: user.old.entry.cn.string,
                value: user.old.entry.description.string,
                actualValue: user.old.entry.authenticationuser.string,
                displayName: user.old.entry.description.string,
                isActive:
                  user.old.entry.enable?.string === 'true' ? 'true' : 'false',
              });
            });
            observer.next(users);
            observer.complete();
          }
        },
        (error) => {
          console.log(error);
          observer.error();
        }
      );
    });
  }

  getListAsResponse(options) {
    let arrayList = [];
    if (options) {
      if (Array.isArray(options)) {
        arrayList = options;
      } else {
        arrayList.push(options);
      }
    }
    return arrayList;
  }

  getAllSecondaryArtworkSpecialists(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_SECONDARY_ARTWORK_USERS
        ) != null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_SECONDARY_ARTWORK_USERS
            )
          )
        );
      } else {
        this.getUsersByRole(
          BusinessConstants.ROLE_SECONDARY_ARTWORK_SPECIALIST,
          'ALL_SECONDARY_ARTWORK_USERS',
          () => {
            observer.next(
              this.monsterConfigApiService.parseStorageValue(
                sessionStorage.getItem(
                  NpdSessionStorageConstants.ALL_SECONDARY_ARTWORK_USERS
                )
              )
            );
          }
        );
      }
    });
  }

  getAllE2EPMUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_E2EPM_USERS) !==
        null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_E2EPM_USERS)
          )
        );
      } else {
        this.getUsersByRole(
          BusinessConstants.ROLE_E2EPM,
          'ALL_E2EPM_USERS',
          () => {
            observer.next(
              this.monsterConfigApiService.parseStorageValue(
                sessionStorage.getItem(
                  NpdSessionStorageConstants.ALL_E2EPM_USERS
                )
              )
            );
          }
        );
      }
    });
  }

  getAllCorpPMUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_CORPPM_USERS) !==
        null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_CORPPM_USERS)
          )
        );
      } else {
        this.getUsersByRole(
          BusinessConstants.ROLE_CORPPM,
          'ALL_CORPPM_USERS',
          () => {
            observer.next(
              this.monsterConfigApiService.parseStorageValue(
                sessionStorage.getItem(
                  NpdSessionStorageConstants.ALL_CORPPM_USERS
                )
              )
            );
          }
        );
      }
    });
  }

  getAllOPSPMUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_OPSPM_USERS) !==
        null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_OPSPM_USERS)
          )
        );
      } else {
        this.getUsersByRole(
          BusinessConstants.ROLE_OPSPM,
          'ALL_OPSPM_USERS',
          () => {
            observer.next(
              this.monsterConfigApiService.parseStorageValue(
                sessionStorage.getItem(
                  NpdSessionStorageConstants.ALL_OPSPM_USERS
                )
              )
            );
          }
        );
      }
    });
  }

  getAllRTMUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_RTM_USERS) !==
        null &&
        this.monsterConfigApiService.parseStorageValue(
          sessionStorage.getItem(NpdSessionStorageConstants.ALL_RTM_USERS)
        ).length > 0
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_RTM_USERS)
          )
        );
      } else {
        this.getUsersByRole(BusinessConstants.ROLE_RTM, 'ALL_RTM_USERS', () => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_RTM_USERS)
            )
          );
        });
      }
    });
  }

  getAllOpsPlannerUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_OPS_PLANNER_USERS
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_OPS_PLANNER_USERS
            )
          )
        );
      } else {
        this.getUsersByRole(
          BusinessConstants.ROLE_OPS_PLANNER,
          'ALL_OPS_PLANNER_USERS',
          () => {
            observer.next(
              this.monsterConfigApiService.parseStorageValue(
                sessionStorage.getItem(
                  NpdSessionStorageConstants.ALL_OPS_PLANNER_USERS
                )
              )
            );
          }
        );
      }
    });
  }

  getAllGFGUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_GFG_USERS) !==
        null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_GFG_USERS)
          )
        );
      } else {
        this.getUsersByRole(BusinessConstants.ROLE_GFG, 'ALL_GFG_USERS', () => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_GFG_USERS)
            )
          );
        });
      }
    });
  }

  getAllFPMSUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_FPMS_USERS) !==
        null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_FPMS_USERS)
          )
        );
      } else {
        this.getUsersByRole(BusinessConstants.ROLE_FPMS, 'ALL_FPMS_USERS', () => {
          observer.next(
            this.monsterConfigApiService.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_FPMS_USERS)
            )
          );
        });
      }
    });
  }

  getAllProjectSpecialistUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_PROJECT_SPECIALIST_USERS
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_PROJECT_SPECIALIST_USERS
            )
          )
        );
      } else {
        this.getUsersByRole(
          BusinessConstants.ROLE_PROJECT_SPECIALIST,
          'ALL_PROJECT_SPECIALIST_USERS',
          () => {
            observer.next(
              this.monsterConfigApiService.parseStorageValue(
                sessionStorage.getItem(
                  NpdSessionStorageConstants.ALL_PROJECT_SPECIALIST_USERS
                )
              )
            );
          }
        );
      }
    });
  }

  getAllRegulatoryLeadUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_REGULATORY_LEAD_USERS
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_REGULATORY_LEAD_USERS
            )
          )
        );
      } else {
        this.getUsersByRole(
          BusinessConstants.ROLE_REGULATORY_LEAD,
          'ALL_REGULATORY_LEAD_USERS',
          () => {
            observer.next(
              this.monsterConfigApiService.parseStorageValue(
                sessionStorage.getItem(
                  NpdSessionStorageConstants.ALL_REGULATORY_LEAD_USERS
                )
              )
            );
          }
        );
      }
    });
  }

  getAllCommercialLeadUsers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_COMMERCIAL_LEAD_USERS
        ) !== null
      ) {
        observer.next(
          this.monsterConfigApiService.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_COMMERCIAL_LEAD_USERS
            )
          )
        );
      } else {
        this.getUsersByRole(
          BusinessConstants.ROLE_COMMERCIAL_LEAD,
          'ALL_COMMERCIAL_LEAD_USERS',
          () => {
            observer.next(
              this.monsterConfigApiService.parseStorageValue(
                sessionStorage.getItem(
                  NpdSessionStorageConstants.ALL_COMMERCIAL_LEAD_USERS
                )
              )
            );
          }
        );
      }
    });
  }

  // getAllRMRoleAllocation() : Observable<any> {
  //   return new Observable((observer) => {
  //     if (sessionStorage.getItem(NpdSessionStorageConstants.ALL_RM_ROLE_ALLOCATION) !== null) {
  //         observer.next(this.monsterConfigApiService.parseStorageValue(sessionStorage.getItem(NpdSessionStorageConstants.ALL_RM_ROLE_ALLOCATION)));
  //     }
  //     else {
  //           this.getRMRoleAllocation('ALL_RM_ROLE_ALLOCATION',() => {
  //             observer.next(this.monsterConfigApiService.parseStorageValue(sessionStorage.getItem(NpdSessionStorageConstants.ALL_RM_ROLE_ALLOCATION)));
  //           });
  //     }
  // });
  // }

  // getRMRoleAllocation(SessionStrgConstant,callBack){
  //   let RMAllocation = [];
  //   let RMRoles = BusinessConstants.RM_ROLE_ALLOCATION;
  //   this.entityService.getEntityObjectsWithCount(EntityListConstant.RESOURCE_MANAGEMENT_ROLE, null, null, null, 0, 20, this.utilService.APP_ID).subscribe(response=>{
  //     response?.result.items.filter(rmRole=>{
  //       RMRoles.some(role=>
  //         {
  //           if(rmRole?.Properties?.Name === role){
  //             RMAllocation.push(rmRole)
  //           }
  //         })

  //     });
  //         sessionStorage.setItem(NpdSessionStorageConstants[SessionStrgConstant], JSON.stringify(RMAllocation));
  //         if (callBack) {
  //           callBack();
  //         }
  //       }, error => {
  //         console.log(error);
  //         sessionStorage.setItem(NpdSessionStorageConstants[SessionStrgConstant], JSON.stringify([]));
  //         if (callBack) {
  //             callBack();
  //         }
  //       });
  // }

  getBooleanValue(value) {
    if (value == 'NA' || value == '') {
      return '';
    } else if (value == 'true') {
      return true;
    } else if (value == 'false') {
      return false;
    } else {
      return '';
    }
  }

  getPureBooleanValue(value) {
    let finalValue = false;
    if (value == 'NA' || value == '') {
      return finalValue;
    } else if (value == 'true') {
      return true;
    } else if (value == 'false') {
      return false;
    } else {
      return finalValue;
    }
  }

  getRoleAssignmentName(roleName): string {
    if (roleName === 'E2E PM') {
      return 'E2EPMUser';
    } else if (roleName === 'E2E PM') {
      return 'E2EPMUser';
    } else if (roleName === 'OPS PM') {
      return 'OPSPMUser';
    } else if (roleName === 'Corp PM') {
      return 'CorpPMUser';
    } else if (roleName === 'Regional Technical Manager') {
      return 'RTMUser';
    } else if (roleName === 'Project Specialist') {
      return 'ProjectSpecialistUser';
    } else if (roleName === 'Regulatory Lead') {
      return 'RegAffairsUser';
    } else if (roleName === 'GFG') {
      return 'GFGUser';
    } else if (roleName === 'Secondary Artwork Manager') {
      return 'SecondaryAWUser';
    }else if(roleName === 'FPMS'){
      return 'FPMSUser';
    }
  }
  getObjectArrayFromPath(pathMapper, itemId) {
    let pathMapperArray: string[] = pathMapper.split('/');
    let pathObjectMapper: any;
    if (pathMapperArray.length == 1) {
      pathObjectMapper =
        '"' + [pathMapper] + '"' + ':' + '"' + itemId.toString() + '"';
    } else {
      for (var index = 0; index < pathMapperArray.length; index++) {
        if (index == pathMapperArray.length - 1) {
          pathObjectMapper =
            pathObjectMapper +
            '"' +
            pathMapperArray[index] +
            '"' +
            ' : ' +
            '"' +
            itemId +
            '"' +
            ' } '.repeat(pathMapperArray.length - 1);
        } else if (index == 0) {
          pathObjectMapper = '"' + pathMapperArray[index] + '"' + ' : { ';
        } else {
          pathObjectMapper =
            pathObjectMapper + '"' + pathMapperArray[index] + '"' + ' : { ';
        }
      }
    }
  
    return pathObjectMapper;
  }

  /**
   * @author Manikanta Jakkamsetti
   */
  getMPMFieldsForView(viewId) {
    let viewObj = {
      viewName: viewId,
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.GET_ALL_MPM_FIELDS_FOR_VIEW_NS,
          this.GET_ALL_MPM_FIELDS_FOR_VIEW_WS,
          viewObj
        )
        .subscribe(
          (res) => {
            observer.next(res);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getArtworkStatusOverviewByMPMJobsAndMPMGroups(mpmJobs, mpmGroups) {
    let artworkJobs = {
      MPMJobs: mpmJobs,
      MPMGroups: mpmGroups,
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.GET_ARTWORK_STATUS_OVERVIEW_NS,
          this.GET_ARTWORK_STATUS_OVERVIEW_WS,
          artworkJobs
        )
        .subscribe(
          (res) => {
            observer.next(res);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getProjectClassificationNumber(projectDetails): string {
    let classificationNumber = '';
    if (projectDetails?.NPD_PROJECT_CURRENT_FINAL_CLASSIFICATION && projectDetails?.NPD_PROJECT_CURRENT_FINAL_CLASSIFICATION !== 'NA') {
      classificationNumber = projectDetails?.NPD_PROJECT_CURRENT_FINAL_CLASSIFICATION;
    }
    else {
      const earlyProjectClassification = projectDetails?.PR_EARLY_PROJECT_CLASSIFICATION;
      classificationNumber = earlyProjectClassification !== 'NA' ? earlyProjectClassification : 'NA';
    }
    return classificationNumber;
  }

  getClassificationNumberByTasksStatus(projectDetails, taskDetails) {
    if (projectDetails && taskDetails) {
      let task = taskDetails?.find(task => task?.TASK_NAME == 'TASK 04' && (task?.TASK_STATUS_VALUE == 'COMPLETED' || task?.TASK_STATUS_VALUE == 'Approved/Completed'));
      if (task) {
        if (
          projectDetails[0]?.NPD_PROJECT_CURRENT_FINAL_CLASSIFICATION &&
          projectDetails[0]?.NPD_PROJECT_CURRENT_FINAL_CLASSIFICATION!='NA'
        ){
          return projectDetails[0]?.NPD_PROJECT_CURRENT_FINAL_CLASSIFICATION;
        }
        else{
          return projectDetails[0]?.NPD_PROJECT_PROJECT_FINAL_PROJECT_CLASSIFICATION;
        }
          
      }
      else {
        let currentClassification = projectDetails[0]?.NPD_PROJECT_CURRENT_FINAL_CLASSIFICATION;
        let earlyClassification = projectDetails[0]?.PR_EARLY_PROJECT_CLASSIFICATION;
        let classificationNumber = currentClassification && currentClassification !== 'NA' ? currentClassification : earlyClassification;
        return classificationNumber;
      }
    }
  }
}
