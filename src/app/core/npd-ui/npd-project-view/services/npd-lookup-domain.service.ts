import { Injectable } from '@angular/core';
import { AppService, CategoryLevel, LoaderService, NotificationService, Priority, ProjectConstant, SessionStorageConstants, SharingService } from 'mpm-library';
import { Observable } from 'rxjs';
import { NpdSessionStorageConstants } from '../../request-view/constants/sessionStorageConstants';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { ProjectBulkEditConstants } from '../constants/ProjectBulkEditConstants';
import { OverViewConstants } from '../constants/projectOverviewConstants';
import { NpdProjectService } from './npd-project.service';

@Injectable({
  providedIn: 'root',
})
export class NpdLookupDomainService {
  constructor(
    public loaderService: LoaderService,
    public sharingService: SharingService,
    public npdProjectService: NpdProjectService,
    public monsterConfigApiService: MonsterConfigApiService,
    public notificationService: NotificationService,
    public appService:AppService
  ) {}
  comboValues = [];
  earlyManufacturingSites = [];
  reportingProjectSubType = [];
  reportingProjectDetail = [];
  allListConfig = {
    projectTypes: [],
  };
  getPriorities(categoryLevelName) {
    this.loaderService.show();
    this.comboValues = [];
    const currCategoryObj: CategoryLevel = this.sharingService
      .getCategoryLevels()
      .find((data) => {
        return data.CATEGORY_LEVEL_TYPE === categoryLevelName;
      });
    if (currCategoryObj) {
      if (
        sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES) !== null
      ) {
        let allPriorities: Array<Priority> = JSON.parse(
          sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES)
        );
        let allPriority: Array<Priority> = allPriorities.filter(
          (priority: Priority) => {
            return (
              priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id ===
              currCategoryObj['MPM_Category_Level-id'].Id
            );
          }
        );
        // let priority = [];
        // priority.push(allPriority);
        this.comboValues = allPriority;
        this.loaderService.hide();
      } else {
        let allPriority: Array<Priority> = this.sharingService
          .getAllPriorities()
          .filter((priority: Priority) => {
            return (
              priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id ===
              currCategoryObj['MPM_Category_Level-id'].Id
            );
          });
        // let priority = [];
        // priority.push(allPriority);
        this.comboValues = allPriority;
        this.loaderService.hide();
      }
    }
  }

  getCanCompanies() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllCanCompanies().subscribe(
      (response) => {
        let canCompanies = [];
        canCompanies = response;
        if (canCompanies?.length > 0) {
          canCompanies.forEach((canCompany) => {
            let canCompanyValue = {
              displayName: canCompany?.CanCompany,
              value: canCompany?.['Can_Supplier_Data-id']?.['Id'],
            };
            this.comboValues.push(canCompanyValue);
          });
        }
        this.loaderService.hide();
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Can companies'
        );
      }
    );
  }
  getProgramTags() {
    this.loaderService.show();
    this.comboValues = [];
    this.monsterConfigApiService.getAllProgramTags().subscribe((response) => {
      let programTag = [];
      programTag = response;
      if (programTag?.length > 0) {
        programTag.forEach((canCompany) => {
          let programTagValue = {
            displayName: canCompany?.DisplayName,
            value: canCompany?.['Program_Tag-id'].Id,
          };
          this.comboValues.push(programTagValue);
        });
        this.loaderService.hide();
      }
    });
  }
  getTastingRequirements() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTastingRequirements().subscribe(
      (response) => {
        let printSuppliers = [];
        printSuppliers = response;
        if (printSuppliers?.length > 0) {
          printSuppliers.forEach((trialSupervisionQuality) => {
            let trialSupervisionQualityValue = {
              displayName: trialSupervisionQuality?.TastingRequirement,
              value:
                trialSupervisionQuality?.['Tasting_Requirement-id']?.['ItemId'],
            };
            this.comboValues.push(trialSupervisionQualityValue);
          });
          this.loaderService.hide();
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Tasting Requirements'
        );
      }
    );
  }
  getTrialProtocolWrittenByUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTrialProtocolWrittenByUsers().subscribe(
      (response) => {
        let printSuppliers = [];
        printSuppliers = response;
        if (printSuppliers?.length > 0) {
          printSuppliers.forEach((trialSupervisionQuality) => {
            let trialSupervisionQualityValue = {
              displayName: trialSupervisionQuality?.Users,
              value:
                trialSupervisionQuality?.['Trial_Protocol_Written_By-id']?.[
                  'ItemId'
                ],
            };
            this.comboValues.push(trialSupervisionQualityValue);
          });
          this.loaderService.hide();
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Can companies'
        );
      }
    );
  }
  getReportingProjectType() {
    this.loaderService.show();
    this.comboValues = [];
    this.monsterConfigApiService
      .getAllReportingProjectType()
      .subscribe((response) => {
        this.comboValues = this.monsterConfigApiService.transformResponseForLookup(
          response,
          'CP_Project_Type-id',
          true
        );
        this.loaderService.hide();
      });
  }
  getOPSPMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllOPSPMUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getE2EPMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllE2EPMUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getCorpPMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllCorpPMUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getRTMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllRTMUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getOpsPlannerUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllOpsPlannerUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }

  getGFGUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllGFGUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getProjectSpecialistUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService
      .getAllProjectSpecialistUsers()
      .subscribe((response) => {
        this.comboValues = response;
        this.loaderService.hide();
      });
  }
  getRegulatoryLeadUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllRegulatoryLeadUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getCommercialLeadUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllCommercialLeadUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getSecondaryArtworkSpecialists() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService
      .getAllSecondaryArtworkSpecialists()
      .subscribe((response) => {
        this.comboValues = response;
        this.loaderService.hide();
      });
  }
  getDraftManufacturingLocation() {
    this.loaderService.show();
    this.comboValues = [];
    this.monsterConfigApiService
      .getAllDraftManufacturingLocations()
      .subscribe((response) => {
        let draftManufacturingLocationOptions = [];
        draftManufacturingLocationOptions = response;
        draftManufacturingLocationOptions.forEach(
          (draftManufacturingLocation) => {
            let location = {
              ItemId: draftManufacturingLocation?.Identity?.ItemId,
              Id: draftManufacturingLocation?.Identity?.Id,
              displayName:
                draftManufacturingLocation?.['R_PO_MARKET$Properties']
                  ?.DisplayName,
              regulatoryClassification:
                draftManufacturingLocation?.['R_PO_MARKET$Properties']
                  ?.RegulatoryClassification,
            };
            this.comboValues.push(location);
            this.loaderService.hide();
          }
        );
      }),
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Country Of Manufacture'
        );
      };
  }
  getTrialSupervisionQuality() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTrialSupervisionQuality().subscribe(
      (response) => {
        let printSuppliers = [];
        printSuppliers = response;
        if (printSuppliers?.length > 0) {
          printSuppliers.forEach((trialSupervisionQuality) => {
            let trialSupervisionQualityValue = {
              displayName: trialSupervisionQuality?.Users,
              value:
                trialSupervisionQuality?.['Trial_Supervision_Quality-id']?.[
                  'ItemId'
                ],
            };
            this.comboValues.push(trialSupervisionQualityValue);
          });
          this.loaderService.hide();
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Quality'
        );
      }
    );
  }

  getTrialSupervisionNPD() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTrialSupervisionNPD().subscribe(
      (response) => {
        let trialSupervisionNPDResponse = [];
        trialSupervisionNPDResponse = response;
        if (trialSupervisionNPDResponse?.length > 0) {
          trialSupervisionNPDResponse.forEach((trialSupervisionNPD) => {
            let trialSupervisionNPDValue = {
              displayName: trialSupervisionNPD?.Users,
              value:
                trialSupervisionNPD?.['Trial_Supervision_NPD-id']?.['ItemId'],
            };
            this.comboValues.push(trialSupervisionNPDValue);
            this.loaderService.hide();
          });
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Trial supervision NPD'
        );
      }
    );
  }
  getWhoWillCompleteUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllWhoWillCompleteUsers().subscribe(
      (response) => {
        let users = [];
        users = response;
        if (users?.length > 0) {
          users.forEach((user) => {
            let userValue = {
              displayName: user?.Users,
              value: user?.['Tech_Qual_Users-id']?.['ItemId'],
            };
            this.comboValues.push(userValue);
            this.loaderService.hide();
          });
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Who Will Complete users'
        );
      }
    );
  }
  getEarlyManufacturingSites(draftManufacturingLocation): Observable<any> {
    let draftManufacturingRequestParams = {
      'Draft_Manufacturing_Location-id': {
        Id:
          draftManufacturingLocation &&
          draftManufacturingLocation.split('.')[1],
        ItemId: draftManufacturingLocation,
      },
    };
    return new Observable((observer) => {
      this.monsterConfigApiService
        .fetchEarlyManufacturingSitesByDraftManufacturingLocation(
          draftManufacturingRequestParams
        )
        .subscribe(
          (response) => {
            this.earlyManufacturingSites =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Early_Manufacturing_Site-id',
                true
              );
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }
  getAllDeliveryQuarters() {
    this.loaderService.show();
    this.comboValues = [];
    let deliveryQuarters = [];
    this.monsterConfigApiService
      .getAllDeliveryQuarters()
      .subscribe((response) => {
        deliveryQuarters =
          this.monsterConfigApiService.transformResponseForLookup(
            response,
            'PM_Delivery_Quarter-id',
            true
          );
        if (deliveryQuarters?.length > 0) {
          deliveryQuarters.forEach((deliveryQuarter) => {
            let deliveryQuarterValue = {
              displayName: deliveryQuarter.displayName,
              value: deliveryQuarter.ItemId,
            };
            this.comboValues.push(deliveryQuarterValue);
            this.loaderService.hide();
          });
        }
      });
  }
  getProjectTypes(): Observable<any> {
    return new Observable((observer) => {
      this.monsterConfigApiService.getAllProjectTypes().subscribe(
        (response) => {
          this.allListConfig.projectTypes =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Project_Type-id',
              true
            );
         
          if (this.allListConfig.projectTypes?.length > 0) {
            this.allListConfig.projectTypes.forEach((projectType) => {
              let projectTypeValue = {
                displayName: projectType.displayName,
                value: projectType.ItemId,
              };
              this.comboValues.push(projectTypeValue);
            });
          }
          observer.next(true);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }
  comboFieldSelection(dataType, selectedField) {
    if (dataType === 'COMBO' || dataType === 'MULTISELECT') {
      let SessionStorageConstant = selectedField[0]
        ? selectedField[0].SessionStorageConstant
        : selectedField.SessionStorageConstant;

      if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_OPSPM_USERS
      ) {
        this.getOPSPMUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_E2EPM_USERS
      ) {
        this.getE2EPMUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_CORPPM_USERS
      ) {
        this.getCorpPMUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_RTM_USERS
      ) {
        this.getRTMUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_OPS_PLANNER_USERS
      ) {
        this.getOpsPlannerUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_GFG_USERS
      ) {
        this.getGFGUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_PROJECT_SPECIALIST_USERS
      ) {
        this.getProjectSpecialistUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_REGULATORY_LEAD_USERS
      ) {
        this.getRegulatoryLeadUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_COMMERCIAL_LEAD_USERS
      ) {
        this.getCommercialLeadUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_SECONDARY_ARTWORK_USERS
      ) {
        this.getSecondaryArtworkSpecialists();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_REPORTING_TYPES
      ) {
        this.getReportingProjectType();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_CAN_COMPANIES
      ) {
        this.getCanCompanies();
      } else if (
        SessionStorageConstant === SessionStorageConstants.ALL_PRIORITIES
      ) {
        this.comboValues= []
        this.comboValues = OverViewConstants.ProjectPriority
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_PROGRAM_TAG
      ) {
        this.getProgramTags();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TASTING_REQUIREMENT
      ) {
        this.getTastingRequirements();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS
      ) {
        this.getTrialProtocolWrittenByUsers();
      } else if (SessionStorageConstant === 'onSite') {
        this.comboValues=[];
        this.comboValues = OverViewConstants.onSite;
      } else if (
        SessionStorageConstant === 'BottlerCommunicationStatusConfig'
      ) {
        this.comboValues =
          ProjectBulkEditConstants.BottlerCommunicationStatusConfig;
      } else if (SessionStorageConstant === 'RAGConfig') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.RAGConfig;
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_DRAFT_MANUFACTURING_LOCATIONS
      ) {
        this.getDraftManufacturingLocation();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_QUALITY
      ) {
        this.getTrialSupervisionQuality();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_WHO_WILL_COMPLETE_USERS
      ) {
        this.getWhoWillCompleteUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_NPD
      ) {
        this.getTrialSupervisionNPD();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_DELIVERY_QUARTERS
      ) {
        this.getAllDeliveryQuarters();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_BOTTLERS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService.getAllBottlers().subscribe((response) => {
          
         
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Bottler-id',
              true
            );
            this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_CASE_PACK_SIZES
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllCasePackSizes()
          .subscribe((response) => {
            
            
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Case_Pack_Size-id',
                true
              );
            this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_SKU_DETAILS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllSKUDetails()
          .subscribe((response) => {
             
            
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Sku_Details-id',
                true
              );
              this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_MARKETS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService.getAllMarkets().subscribe((response) => {
           
          
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Markets-id',
              true,
              false,
              true
            );
            this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_CONSUMER_UNITS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllConsumerUnits()
          .subscribe((response) => {
            
            
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Consumer_Unit_Size-id',
                true
              );
              this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_BRANDS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService.getAllBrands().subscribe((response) => {
          
          
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Brands-id',
              true
            );
            this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_PACKAGING_TYPES
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllPackagingTypes()
          .subscribe((response) => {
           
            
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Packaging_Type-id',
                true
              );
              this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_PLATFORMS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService.getAllPlatforms().subscribe((response) => {
          
          
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Platforms-id',
              true
            );
             this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_COMMERCIAL_CATEGORIES
      ) {
        this.loaderService.show();
            this.comboValues = [];
        this.monsterConfigApiService
          .getAllCommercialCategorization()
          .subscribe((response) => {
            
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Commercial_Categorisation',
                true
              );
               this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_EARLY_MANUFACTURING_SITES
      ) {
        this.loaderService.show();
            this.comboValues = [];
        this.monsterConfigApiService
          .getAllEarlyManufacturingSites()
          .subscribe((response) => {
            
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Early_Manufacturing_Site-id',
                true
              );
               this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_PROJECT_SUBTYPE
      ) {
        this.loaderService.show();
            this.comboValues = [];
        this.monsterConfigApiService
          .getAllProjectSubtype()
          .subscribe((response) => {
            
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Project_Sub_Type-id',
                true
              );
               this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_PROJECT_DETAILS
      ) {
        this.loaderService.show();
            this.comboValues = [];
        this.monsterConfigApiService
          .getAllProjectDetail()
          .subscribe((response) => {
            
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Project_Detail-id',
                true
              );
              this.loaderService.hide();
          });
      } else if (SessionStorageConstant === 'RequestStatus') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.RequestStatus;
      } else if (SessionStorageConstant === 'ProjectStatus') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.ProjectStatus;
      } else if (SessionStorageConstant === 'TaskStatus') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.TaskStatus;
      } else if (SessionStorageConstant === 'TaskSubject') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.TaskSubject;
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_USERS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        let userDetails = JSON.parse(
          sessionStorage.getItem('USER_DETAILS_WORKFLOW')
        ).User.ManagerFor.Target;
        let parameters;
        userDetails.forEach(role=>{
          if (role.Name === 'NPD Access') {
            parameters = {
              role: role.Id,
              depth: 1,
              sort: 'ascending',
            };
          }
        })
        if(!parameters){
          this.loaderService.hide(); 
        }
        else{
           if (sessionStorage.getItem('ALL_NPD_USERS') == null) {
             
             this.appService
               .getUsersForRole(parameters)
               .subscribe((response) => {
                 response.tuple.forEach((user) => {
                   this.comboValues.push({
                     displayName: user.old.entry.description.string,
                   });
                 });
                 this.loaderService.hide();
                 sessionStorage.setItem(
                   'ALL_NPD_USERS',
                   JSON.stringify(this.comboValues)
                 );
               });
           }
           else{
            this.comboValues = JSON.parse(sessionStorage.getItem('ALL_NPD_USERS'))
             this.loaderService.hide();
           }
           
        }
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_PROJECT_TYPES
      ) {
        this.comboValues = [];
        this.loaderService.show();
        this.getProjectTypes().subscribe((response) => {
          this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_VARIANTS
      ) {
        this.comboValues = [];
        this.loaderService.show();
        this.monsterConfigApiService.getAllVariants().subscribe((response) => {
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Variant_SKU-id',
              true
            );
          this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_AUDIT_USERS
      ) {
        this.comboValues = [];
        this.loaderService.show();
        this.npdProjectService.getAllAuditUsers().subscribe((response) => {
          response.forEach((user) => {
            let auditUser = {
              displayName: user?.Users,
              value: user?.Users,
            };
            this.comboValues.push(auditUser);
          });
          this.loaderService.hide();
        });
      }
      // else if (SessionStorageConstant == NpdSessionStorageConstants.ALL_NPD_ROLES) {
      //   this.loaderService.show();
      //   this.comboValues=[]
      //   if (
      //     sessionStorage.getItem(
      //       NpdSessionStorageConstants.ALL_NPD_ROLES
      //     ) == null
      //   ){
      //      this.appService.getAllTeamRoles().subscribe((response) => {
      //        response?.GetTeamRolesResponse?.Teams.MPM_Teams.MPM_Team_Role_Mapping?.forEach(
      //          (role) => {
      //            this.comboValues.push({
      //              displayName: role.NAME,
      //              value: role.NAME,
      //            });
      //          }
      //        );
             
      //        this.loaderService.hide();
      //      });
      //   }
         
      // }

      // return this.comboValues;
    }
  }
}
