import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkFinalClassificationComponent } from './bulk-final-classification.component';

describe('BulkFinalClassificationComponent', () => {
  let component: BulkFinalClassificationComponent;
  let fixture: ComponentFixture<BulkFinalClassificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkFinalClassificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkFinalClassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
