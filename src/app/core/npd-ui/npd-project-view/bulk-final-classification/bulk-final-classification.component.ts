import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NotificationService, LoaderService } from 'mpm-library';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { OverViewConstants } from '../constants/projectOverviewConstants';
import { PmProjectClassificationComponent } from '../pm-project-classification/pm-project-classification.component';
import { NpdProjectService } from '../services/npd-project.service';

@Component({
  selector: 'app-bulk-final-classification',
  templateUrl: './bulk-final-classification.component.html',
  styleUrls: ['./bulk-final-classification.component.scss']
})
export class BulkFinalClassificationComponent extends PmProjectClassificationComponent implements OnInit {

  constructor(public formBuilder: FormBuilder,
    public notificationService: NotificationService,
    public npdProjectService: NpdProjectService,
    public monsterUtilService: MonsterUtilService,
    public monsterConfigApiService: MonsterConfigApiService,
    public loaderService: LoaderService,) {
    super(formBuilder, notificationService, npdProjectService, monsterUtilService, monsterConfigApiService, loaderService);
  }
  projectClassificationDataForm: FormGroup;
  @Output() onBulkFinalClassificationFormChange: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  updateRequestRelationsInfo(relation, formControl) {
    let property;
    let propertyValue;
    let relationProperty;
    let relationvalue;
    if (formControl === 'draftManufacturingLocation') {
      this.projectClassificationDataForm.patchValue({
        'earlyManufacturingSite': ''
      })
    }
    relationProperty = OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROJECT_FINAL_CLASSIFICATION[relation];
    relationvalue = this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;//itemId
    property = OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROJECT_FINAL_CLASSIFICATION[formControl];
    propertyValue = this.getRelationValue(formControl, relationvalue)//fetch value-display name
    if (property) {
      // this.npdProjectService.setCustomFormProperty(property, propertyValue);
      // this.npdProjectService.setCustomFormProperty(relationProperty, relationvalue);
    }
    if (//this.projectClassificationDataForm.get('earlyManufacturingSite').value !== ''
      //&&
       this.projectClassificationDataForm.get('draftManufacturingLocation').value !== ''
    ) {
      this.onBulkFinalClassificationFormChange.emit(this.projectClassificationDataForm);
    }

  }
  updateRequestInfo(formControl, formName, propertyValue?) {
    let propertyName = OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROJECT_FINAL_CLASSIFICATION[formControl];
    let value;
    value = propertyValue ? propertyValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;
    if (propertyName) {
      // this.npdProjectService.setCustomFormProperty(propertyName, value);
    }
    if (//this.projectClassificationDataForm.get('earlyManufacturingSite').value !== ''
      //&&
       this.projectClassificationDataForm.get('draftManufacturingLocation').value !== ''
    ) {
      this.onBulkFinalClassificationFormChange.emit(this.projectClassificationDataForm);
    }

  }

  ngOnInit(): void {
    super.ngOnInit();

  }


}
