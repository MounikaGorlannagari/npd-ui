import { COMMA } from '@angular/cdk/keycodes';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { DateAdapter } from '@angular/material/core';
import { MatTable } from '@angular/material/table';
import { LoaderService, NotificationService } from 'mpm-library';
import { NpdTaskService } from '../../npd-task-view/services/npd-task.service';
import { EntityListConstant } from '../../request-view/constants/EntityListConstant';
import { WeekPickerComponent } from '../../request-view/generic-components/week-picker/week-picker.component';
import { EntityService } from '../../services/entity.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { OverViewConstants } from '../constants/projectOverviewConstants';
import { NpdProjectService } from '../services/npd-project.service';

@Component({
  selector: 'app-pm-can-supplier',
  templateUrl: './pm-can-supplier.component.html',
  styleUrls: ['./pm-can-supplier.component.scss'],
})
export class PmCanSupplierComponent implements OnInit {
  flag: number = 0;
  isEditMode: boolean= false;
  constructor(
    private npdProjectService: NpdProjectService,
    private monsterUtilService: MonsterUtilService,
    private formBuilder: FormBuilder,
    private adapter: DateAdapter<any>,
    private notificationService: NotificationService
  ) {}

  @Input() technicalRequestId;
  @Input() canSupplierConfig;
  @Input() listConfig;
  selectedMultiCanSupplier = [];
  /*
   * Market Scope Grid Variables
   */
  @ViewChild('secondaryPackagingTable') secondaryPackagingTable: MatTable<any>;
  @ViewChild(WeekPickerComponent, { static: false })
  weekPickerComponent: WeekPickerComponent;

  dynamicSecondaryPackagingForm: FormGroup;
  spGridColumns = [];
  packFormats = [];
  canSuppliers = [];
  canSuppliersCopy = [];
  //dataSource = [];
  supplierIds = [];
  supplierValues = [];

  booleanConfigs = [];
  showSecondaryPackagingForm;
  loadingSecondaryPackaging;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [COMMA];
  
  fromDate = {
    actualWeek: null,
    targetWeek: null,
  };
  toDate = {
    actualWeek: null,
    targetWeek: null,
  };
  weekYear = {
    actualWeek: null,
    targetWeek: null,
  };
  show = {
    actualWeek: false,
    targetWeek: false,
  };

  // convenience getters for easy access to form fields
  get dynamicSecondaryPackagingFormControl() {
    return this.dynamicSecondaryPackagingForm.controls;
  }
  get secondaryPackagingFormArray() {
    return this.dynamicSecondaryPackagingFormControl
      .secondaryPackaging as FormArray;
  }
  disabled = false; //true

  getBool(date) {
    this.show[date] = !this.show[date];
  }
  getStopPropogation(event, date) {
    if (this.show[date]) {
      return event.stopPropagation();
    }
  }
  getAllBool() {
    Object.keys(this.show).map((ele) => {
      this.show[ele] = false;
    });
  }

  changeWeek(weekFormat, date, index) {
    this.weekYear[date] = weekFormat;
    this.secondaryPackagingFormArray?.controls?.[index]?.patchValue({
      [date]: this.weekYear[date],
    });
    this.updateSPInfo(date, index);
  }

  bulkCreateSP() {
    if (this.selectedMultiCanSupplier.length == 0) {
      this.notificationService.error('Please select at least one supplier.');
      return;
    }
    this.selectedMultiCanSupplier.forEach((supplier) => {
      //this.dataSource.push(supplier.value);
      this.supplierValues.push(supplier.displayName);
      this.supplierIds.push(supplier.value);
    });
    this.selectedMultiCanSupplier = [];
    this.updateMultiCanSuppliers();
    this.secondaryPackagingFormArray?.controls?.[0]?.patchValue({
      nameOfCanCompany: this.supplierValues?.toString(),
      noOfCanCompaniesRequired: this.supplierValues?.length,
    }); //update in service
    this.npdProjectService.setCustomFormProperty(
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.nameOfCanCompanyId,
      this.supplierIds?.toString()
    );
    this.npdProjectService.setCustomFormProperty(
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.nameOfCanCompany,
      this.supplierValues?.toString()
    );
    this.npdProjectService.setCustomFormProperty(
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.noOfCanCompaniesRequired,
      this.supplierValues?.length
    );
  }

  getSupplierValues() {
    this.canSupplierConfig.addBtn.disabled = !this.isEditMode;
    return this.supplierValues.toString();
  }

  updateFormControlsDisability() {
    for (let i = 0; i < this.secondaryPackagingFormArray.length; i++) {
      this.monsterUtilService.disableFormControls(
        this.secondaryPackagingFormArray.controls[i],
        this.canSupplierConfig
      );
    }
  }

  mapSupplierContentToForm() {
    this.canSuppliers = this.listConfig?.canCompanies;
    this.canSuppliersCopy = this.listConfig?.canCompanies;
    if (
      this.canSupplierConfig?.nameOfCanCompany?.value?.split(',') &&
      this.canSupplierConfig?.nameOfCanCompanyId.value?.split(',')
    ) {
      let companyId =
        this.canSupplierConfig?.nameOfCanCompanyId?.value?.split(',');
      this.supplierIds = companyId?.filter((value) => value != 'NA');
      let supplierValue =
        this.canSupplierConfig?.nameOfCanCompany?.value?.split(',');
      this.supplierValues = supplierValue?.filter((value) => value != 'NA');
    }
    this.updateMultiCanSuppliers();
    this.dynamicSecondaryPackagingForm = this.formBuilder.group({
      secondaryPackaging: new FormArray([]),
      canSupplier: new FormControl({ value: '' }),
    });
    this.spGridColumns = [
      'targetWeek',
      'actualWeek',
      'itemNumber',
      'noOfCanCompaniesRequired',
      'nameOfCanCompany',
    ];
    this.showSecondaryPackagingForm = true;
    for (let i = 0; i < 1; i++) {
      this.secondaryPackagingFormArray.push(
        this.formBuilder.group({
          targetWeek: [
            {
              value: this.canSupplierConfig.targetWeek.value,
              disabled: this.canSupplierConfig.targetWeek.disabled,
            },
          ],
          actualWeek: [
            {
              value: this.canSupplierConfig.actualWeek.value,
              disabled: this.canSupplierConfig.actualWeek.disabled,
            },
          ],
          itemNumber: [
            {
              value: this.canSupplierConfig.itemNumber.value,
              disabled: this.canSupplierConfig.itemNumber.disabled,
            },
          ],
          noOfCanCompaniesRequired: [
            {
              value: this.canSupplierConfig.noOfCanCompaniesRequired.value,
              disabled:
                this.canSupplierConfig.noOfCanCompaniesRequired.disabled,
            },
          ],
          nameOfCanCompany: [
            {
              value: this.canSupplierConfig.nameOfCanCompany.value,
              disabled: this.canSupplierConfig.nameOfCanCompany.disabled,
            },
          ],
        })
      );
    }
  }

  /**
   * To deep copy the objects
   * */
  copyObj(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  /**
   * To update multi markets
   */
  updateMultiCanSuppliers() {
    this.canSuppliersCopy = [];
    if (this.canSuppliers?.length && this.canSuppliers.length>0)
     {
       this.canSuppliers.forEach((supplier) => {
         const matchedSupplier = this.supplierIds.filter((selectedSupplier) => {
           return supplier.value === selectedSupplier;
         });
         if (matchedSupplier.length === 0) {
           this.canSuppliersCopy.push(this.copyObj(supplier));
         }
       });
     }
  }

  /**
   * To re-render material table
   */
  renderSPTableRows() {
    this.secondaryPackagingTable && this.secondaryPackagingTable.renderRows
      ? this.secondaryPackagingTable.renderRows()
      : console.log('No rows in Secondary Packaging.');
  }

  updateSPInfo(formControl, index) {
    let propertyName = OverViewConstants.OVERVIEW_OBJECT_CONSTANTS[formControl];
    let propertyValue =
      this.secondaryPackagingFormArray?.controls[index]['controls'][formControl]
        .value;
    if (propertyName) {
      this.npdProjectService.setCustomFormProperty(propertyName, propertyValue);
    }
  }
  // updateDateValue(date) {
  //   let week:number = date.split('/')[0];
  //   let year:number = date.split('/')[1];
  //   if (this.weekPickerComponent) {
  //     this.weekPickerComponent.calculateDate(week, year);
  //   }
  //   this.flag=0;
  // }
  // onClick(){
  //   if(this.canSupplierConfig.actualWeek.disabled){
  //     return;
  //   }
  //   this.flag+=1;
  // }

  remove(canCompany: any): void {
    const index = this.supplierValues.indexOf(canCompany);

    if (index >= 0) {
      this.supplierValues.splice(index, 1);
      this.supplierIds.splice(index, 1);
    }
    this.renderSPTableRows();
    this.updateMultiCanSuppliers();
    this.secondaryPackagingFormArray?.controls?.[0]?.patchValue({
      nameOfCanCompany: this.supplierValues?.toString(),
      noOfCanCompaniesRequired: this.supplierValues?.length,
    }); //update in service
    this.npdProjectService.setCustomFormProperty(
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.nameOfCanCompanyId,
      this.supplierIds?.toString()
    );
    this.npdProjectService.setCustomFormProperty(
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.nameOfCanCompany,
      this.supplierValues?.toString()
    );
    this.npdProjectService.setCustomFormProperty(
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.noOfCanCompaniesRequired,
      this.supplierValues?.length
    );
    this.canSupplierConfig.addBtn.disabled = !this.isEditMode;
  }
  toggleEdit(isEdit){
    
      this.isEditMode = isEdit;
      this.canSupplierConfig.addBtn.disabled = !isEdit;
      // this.canSupplierConfig.addBtn.disabled = isEdit;
    
  }
  ngOnInit() {
    if (this.canSupplierConfig) {
      this.mapSupplierContentToForm();
    }
    this.canSupplierConfig.addBtn.disabled = !this.isEditMode;
  }
}
