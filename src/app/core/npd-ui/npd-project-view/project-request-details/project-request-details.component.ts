import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { IndexerService, LoaderService, SearchRequest, SearchResponse } from 'mpm-library';
import { Observable } from 'rxjs';
import { BusinessConstants } from '../../request-view/constants/BusinessConstants';

@Component({
  selector: 'app-project-request-details',
  templateUrl: './project-request-details.component.html',
  styleUrls: ['./project-request-details.component.scss']
})
export class ProjectRequestDetailsComponent implements OnInit {

  campaignId: any;
  projectId: any;
  url: any;
  isLoaded: boolean;
  requestId: any;
  requestType: any;
  isTechnical = false;
  isCommercial = false;
  isOperational = false;
  allRequestTypes = [];
  isRequestAvailable = true;

  constructor(
    public activatedRoute: ActivatedRoute,
    private loaderService: LoaderService,
    public sanitizer: DomSanitizer,
    private indexerService: IndexerService,
  ) { }


  readUrlParams(callback) {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.activatedRoute.parent.params.subscribe(parentRouteParams => {
        callback(parentRouteParams, queryParams);
      });
    });
    
  }


  getRequestType = (requestId: String) => {

    if(this.requestId) {
      this.isTechnical = (this.requestType == BusinessConstants.TECHNICAL_REQUEST) || (this.requestType == BusinessConstants.TECHNICAL_REQUEST2) 
      this.isCommercial = (this.requestType == BusinessConstants.COMMERCIAL_REQUEST) || (this.requestType == BusinessConstants.COMMERCIAL_REQUEST2) 
      this.isOperational = (this.requestType == BusinessConstants.OPERATIONAL_REQUEST) || (this.requestType == BusinessConstants.OPERATIONAL_REQUEST2) 
    } else  {
      this.isRequestAvailable = false;
    }
  }

  // Forming search field for mpm-indexer input
  getSearchField(type, field_id, operator_id, operator_name, value?, field_operator?) {

      let searchField = {
          "type": type,
          "field_id": field_id,
          "relational_operator_id": operator_id,
          "relational_operator_name": operator_name,
          "value": value,
          "relational_operator": field_operator
      }
      return searchField;
  }

  getProjectDetailsById(projectId): Observable<any> {
    this.loaderService.show();
    let searchConditionList = [
        this.getSearchField("string", "CONTENT_TYPE", "MPM.OPERATOR.CHAR.IS", "is", "MPM_PROJECT"),
        this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", projectId, "AND"),
    ];

    const parameters: SearchRequest = {
        search_config_id: null,
        keyword: "",
        search_condition_list: {
            search_condition: searchConditionList
        },
        facet_condition_list: {
            facet_condition: []
        },
        sorting_list: {
            sort: []
        },
        cursor: {
            page_index: 0,
            page_size: 1
        }
    };
    return new Observable(observer => {
        this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
            if (response.data.length > 0) {
                this.requestId = response.data[0]['PR_REQUEST_ITEM_ID'];//'005056AACEB9A1EBA6DD27D3623FE821.65623'; //response.data[0];//REFERENCE_LINK_ID
                this.requestType = response.data[0]['PR_REQUEST_TYPE'];
            }
            this.loaderService.hide();
            observer.next();
            observer.complete();
        }, () => {
            this.loaderService.hide();
            observer.error();
        });
    });
  }

  ngOnInit(): void {
    this.readUrlParams((routeParams, queryParams) => {
      if (queryParams && routeParams) {
        this.projectId = routeParams.projectId;
        this.getProjectDetailsById(this.projectId).subscribe(response=> {
          this.getRequestType(this.requestId);
        });
      }
    });
  }

}



/* getRequestDetails() {
     /* this.loaderService.show();
      this.requestService.fetchRequestDetails(this.requestId.split('.')[1]).subscribe(request => {
        this.isRequestAvailable = false;
          if(request) {
            this.getAllRequestTypes().subscribe(response => {
              console.log(response);
              let requestType = this.allRequestTypes.find(requestType => requestType.itemId === 
                request?.R_PO_REQUEST_TYPE$Identity?.ItemId)
              this.isTechnical = (requestType?.value == BusinessConstants.TECHNICAL_REQUEST) 
              this.isCommercial = (requestType?.value == BusinessConstants.COMMERCIAL_REQUEST)
              this.isOperational = (requestType?.value == BusinessConstants.OPERATIONAL_REQUEST)
            });
          } else {
            this.isRequestAvailable = false;
          } */
      /*   if(request?.Properties?.Current_Checkpoint > 0) {
          //this.isTechnical = true;
        }  
       this.loaderService.hide();
      },error=> {
        this.loaderService.hide();
      })
}

getAllRequestTypes() : Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllRequestTypes().subscribe((response) => {
        this.allRequestTypes = response; 
        observer.next(true);
        observer.complete();
      },(error)=> {
        observer.error(error);
      });
    });
  } */