import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LoaderService } from 'mpm-library';
import { Observable } from 'rxjs';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { OverViewConstants } from '../constants/projectOverviewConstants';
import { NpdProjectService } from '../services/npd-project.service';
@Component({
  selector: 'app-pm-programme-manager-reporting',
  templateUrl: './pm-programme-manager-reporting.component.html',
  styleUrls: ['./pm-programme-manager-reporting.component.scss'],
})
export class PmProgrammeManagerReportingComponent implements OnInit {
  programmeManagerReportingForm: FormGroup;
  @Input() requestDetails;
  @Input() programmeManagerReporting;
  @Input() reportingProjectType;
  // @Output() queryMode = new EventEmitter<any>();
  reportingProjectSubType: any;
  reportingProjectDetail: any;
  deliveryQuarters: any;
  constructor(
    public formBuilder: FormBuilder,
    public monsterUtilService: MonsterUtilService,
    public monsterConfigApiService: MonsterConfigApiService,
    public npdProjectService: NpdProjectService,
    public loaderService: LoaderService
  ) {}

  get formControls() {
    return this.programmeManagerReportingForm.controls;
  }

  onValidate() {
    this.programmeManagerReportingForm.markAllAsTouched();
    if (this.programmeManagerReportingForm.invalid) {
      return true;
    } else {
      return false;
    }
  }

  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(
      this.programmeManagerReportingForm,
      this.programmeManagerReporting
    );
  }

  updateRequestInfo(formControl, propertyValue?, index?) {
    let property;
    let value;
    property =
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROGRAMME_MANAGER_REPORTING[
        formControl
      ];
    value = this.programmeManagerReportingForm?.controls?.[formControl]?.value;
    if (property) {
      this.npdProjectService.setCustomFormProperty(property, value);
    }
  }

  updateRequestRelationsInfo(relation, formControl, index?) {
    let property;
    let propertyValue;
    let relationProperty;
    let relationvalue;

    relationProperty =
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROGRAMME_MANAGER_REPORTING[
        relation
      ];
    relationvalue =
      this.programmeManagerReportingForm.controls[formControl] &&
      this.programmeManagerReportingForm.controls[formControl].value; //itemId
    property =
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROGRAMME_MANAGER_REPORTING[
        formControl
      ];
    propertyValue = this.getRelationValue(formControl, relationvalue); //fetch value-display name
    if (property) {
      this.npdProjectService.setCustomFormProperty(property, propertyValue);
      this.npdProjectService.setCustomFormProperty(
        relationProperty,
        relationvalue
      );
    }
  }

  getRelationValue(relation, itemId) {
    let value;
    if (relation == 'projectType') {
      value = this.reportingProjectType?.find(
        (projectType) => projectType.ItemId == itemId
      )?.displayName;
    } else if (relation == 'projectSubType') {
      value = this.reportingProjectSubType?.find(
        (projectSubType) => projectSubType.ItemId == itemId
      )?.displayName;
    } else if (relation == 'projectDetail') {
      value = this.reportingProjectDetail?.find(
        (projectDetail) => projectDetail.ItemId == itemId
      )?.displayName;
    } else if (relation == 'delivery') {
      value = this.deliveryQuarters?.find(
        (deliveryQuarter) => deliveryQuarter.ItemId == itemId
      )?.displayName;
    }
    return value;
  }

  initializeForm() {
    this.programmeManagerReportingForm = this.formBuilder.group({});
    Object.keys(this.programmeManagerReporting).filter(
      (formControlConfigName) => {
        if (
          formControlConfigName &&
          this.programmeManagerReporting[formControlConfigName].ngIf &&
          'formControl' in
            this.programmeManagerReporting[formControlConfigName] &&
          this.programmeManagerReporting[formControlConfigName][
            'formControl'
          ] === true
        ) {
          let validators = [];
          if (
            this.programmeManagerReporting[formControlConfigName].validators
          ) {
            if (
              this.programmeManagerReporting[formControlConfigName].validators
                .required
            ) {
              validators.push(Validators.required);
            }
            if (
              this.programmeManagerReporting[formControlConfigName].validators
                .length &&
              this.programmeManagerReporting[formControlConfigName].validators
                .length.max
            ) {
              validators.push(
                Validators.maxLength(
                  this.programmeManagerReporting[formControlConfigName]
                    .validators.length.max
                )
              );
            }
          }
          /*  if(formControlConfigName == 'projectType') {
           const projectTypeIndex = this.reportingProjectType.findIndex(ele => ele.Id == this.programmeManagerReporting[formControlConfigName].value)
          this.programmeManagerReportingForm.addControl(formControlConfigName, new FormControl({ value: projectTypeIndex == -1 ? '' : this.reportingProjectType[projectTypeIndex].ItemId,
            disabled: this.programmeManagerReporting[formControlConfigName].disabled },validators));
        } else { 
          this.programmeManagerReportingForm.addControl(formControlConfigName, new FormControl({ value: this.programmeManagerReporting[formControlConfigName].value,
          disabled: this.programmeManagerReporting[formControlConfigName].disabled },validators));
        }*/
          this.programmeManagerReportingForm.addControl(
            formControlConfigName,
            new FormControl(
              {
                value:
                  this.programmeManagerReporting[formControlConfigName].value,
                disabled:
                  this.programmeManagerReporting[formControlConfigName]
                    .disabled,
              },
              validators
            )
          );
        }
      }
    );
  }

  ngOnInit(): void {
    if (this.programmeManagerReporting) {
      this.initializeForm();
      this.getDependentDropDownValues();
      this.getAllDeliveryQuarters().subscribe();
    }
  }

  getDependentDropDownValues() {
    if (this.programmeManagerReportingForm.controls.projectType) {
      this.programmeManagerReportingForm.patchValue({
        projectType: this.programmeManagerReporting?.projectType?.value,
      });
    }
    if (this.programmeManagerReportingForm.controls.projectType.value) {
      this.onProjectTypeChange(
        this.programmeManagerReportingForm.controls.projectType.value
      );
      this.onProjectSubTypeChange(
        this.programmeManagerReportingForm.controls.projectSubType.value
      );
    }
  }

  onProjectTypeChange(projectType) {
    let projectTypeRequestParams = {
      'CP_Project_Type-id': {
        Id: projectType && projectType.split('.')[1],
        ItemId: projectType,
      },
    };
    if (projectType) {
      this.loaderService.show();
      this.getReportingProjectSubType(projectTypeRequestParams).subscribe(
        (response) => {
          this.loaderService.hide();
        },
        (error) => {
          this.loaderService.hide();
        }
      );
    }
  }

  onProjectSubTypeChange(projectSubType) {
    let projectSubTypeRequestParams = {
      'Project_Sub_Type-id': {
        Id: projectSubType && projectSubType.split('.')[1],
        ItemId: projectSubType,
      },
    };
    if (projectSubType) {
      this.loaderService.show();
      this.getReportingProjectDetail(projectSubTypeRequestParams).subscribe(
        (response) => {
          this.loaderService.hide();
        },
        (error) => {
          this.loaderService.hide();
        }
      );
    }
  }

  getReportingProjectSubType(parameters): Observable<any> {
    return new Observable((observer) => {
      this.monsterConfigApiService
        .fetchReportingProjectSubType(parameters)
        .subscribe(
          (response) => {
            this.reportingProjectSubType =
              this.monsterConfigApiService.transformResponse(
                response,
                'Project_Sub_Type-id',
                true
              );
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getAllDeliveryQuarters(): Observable<any> {
    return new Observable((observer) => {
      // this.loaderService.show();
      //this.queryMode.emit(50);

      this.monsterConfigApiService.getAllDeliveryQuarters().subscribe(
        (response) => {
          this.deliveryQuarters =
            this.monsterConfigApiService.transformResponse(
              response,
              'PM_Delivery_Quarter-id',
              true
            );
          // this.loaderService.hide();
      //this.queryMode.emit(0);

          observer.next(true);
          observer.complete();
        },
        (error) => {
          // this.loaderService.hide();
      //this.queryMode.emit(0);

          observer.error(error);
        }
      );
    });
  }

  getReportingProjectDetail(parameters): Observable<any> {
    return new Observable((observer) => {
      this.monsterConfigApiService
        .fetchReportingProjectDetail(parameters)
        .subscribe(
          (response) => {
            this.reportingProjectDetail =
              this.monsterConfigApiService.transformResponse(
                response,
                'Project_Detail-id',
                true
              );
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getProjectType(value) {
    if (this.reportingProjectType) {
      return this.reportingProjectType[
        this.reportingProjectType.findIndex((ele) => ele.ItemId == value)
      ]?.displayName;
    }
  }

  getProjectSubType(value) {
    if (this.reportingProjectSubType) {
      return this.reportingProjectSubType[
        this.reportingProjectSubType.findIndex((ele) => ele.ItemId == value)
      ]?.displayName;
    }
  }

  getProjectDetail(value) {
    if (this.reportingProjectDetail) {
      return this.reportingProjectDetail[
        this.reportingProjectDetail.findIndex((ele) => ele.ItemId == value)
      ]?.displayName;
    }
  }

  getProgMgrDeliveryQuarter(value) {
    if (this.deliveryQuarters) {
      return this.deliveryQuarters[
        this.deliveryQuarters.findIndex((ele) => ele.ItemId == value)
      ]?.displayName;
    }
  }

  numericPattern = /^[0-9]*$/;
  numericOnly(event) {
    return this.numericPattern.test(event.key);
  }
}
