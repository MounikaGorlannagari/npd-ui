import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  FormControl,
} from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatTable } from '@angular/material/table';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService, NotificationService, SharingService, UtilService } from 'mpm-library';
import { Observable } from 'rxjs/internal/Observable';
import { NpdTaskService } from '../../npd-task-view/services/npd-task.service';
import { BusinessConstants } from '../../request-view/constants/BusinessConstants';
import { EntityListConstant } from '../../request-view/constants/EntityListConstant';
import { EntityService } from '../../services/entity.service';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { OverViewConstants } from '../constants/projectOverviewConstants';
import { NpdProjectService } from '../services/npd-project.service';
import { MONSTER_ROLES } from '../../filter-configs/role.config';

@Component({
  selector: 'app-pm-secondary-packaging',
  templateUrl: './pm-secondary-packaging.component.html',
  styleUrls: ['./pm-secondary-packaging.component.scss'],
})
export class PmSecondaryPackagingComponent implements OnInit {
  constructor(
    public loaderService: LoaderService,
    public entityService: EntityService,
    public npdProjectService: NpdProjectService,
    public monsterUtilService: MonsterUtilService,
    public formBuilder: FormBuilder,
    public adapter: DateAdapter<any>,
    public notificationService: NotificationService,
    public utilService: UtilService,
    public monsterConfigService: MonsterConfigApiService,
    public sharingService: SharingService, 
  ) { }




  @Input() projectEntityItemId;
  @Input() secondaryPackagingConfig;
  @Input() listConfig;
  @Input() projectData;

  /*
   * Market Scope Grid Variables
   */
  @ViewChild('secondaryPackagingTable') secondaryPackagingTable: MatTable<any>;

  dynamicSecondaryPackagingForm: FormGroup;
  spGridColumns = [];
  packFormats = [];
  printSuppliers = [];
  booleanConfigs = [];
  showSecondaryPackagingForm;
  loadingSecondaryPackaging;
  selection = new SelectionModel<any>(true, []);
  secondaryPackagingResponse: any;
  readonly radioButtonsList = BusinessConstants.radioButtons;
  loggedInUserName=this.sharingService.getCurrentUserDisplayName()

  // convenience getters for easy access to form fields
  get dynamicSecondaryPackagingFormControl() {
    return this.dynamicSecondaryPackagingForm.controls;
  }
  get secondaryPackagingFormArray() {
    return this.dynamicSecondaryPackagingFormControl
      .secondaryPackaging as FormArray;
  }

  onValidate() {
    this.dynamicSecondaryPackagingForm?.markAllAsTouched();
    if (this.dynamicSecondaryPackagingForm?.invalid) {
      return true;
    } else {
      return false;
    }
  }

  bulkCreateSP() {

    const updateData = [];
    let temp = {};
    for (let i = 0; i < 1; i++) {
      temp = {
        operationType: 'Create',
        parentItemId: this.projectEntityItemId, //this.localizationRequestItemId,
        relationName: 'R_PM_SECONDARY_PACK',
        template: null,
        item: {
          Properties: {
            RegsNeeded: false,
            IsArtwork: false,
          },
        },
      };
      updateData.push(temp);
    }
    this.loaderService.show();
    this.entityService
      .bulkCreateEntitywithParent(updateData, null, null)
      .subscribe(
        (response) => {


          const items = response.changeLog.changeEventInfos;
          items.forEach((data) => {
            if (data.changeType !== 'Created') {
              return;
            }
            this.secondaryPackagingFormArray.push(
              this.formBuilder.group({
                required: [],
                regsNeeded: [],
                mpmJobNo: [],
                mpmGroup: [],
                packFormat: [],
                printSupplier: [],
                approvedDieLineAvailable: [],
                itemNumber: [],
                colourApproval: [],
                invoiceApproved: [],
                statusComments: [],
                dateDevelopmentCompleted: [], //"2021-11-16T08:27:00Z"
                dateProofingCompleted: [],
                itemId: [data.item.Identity.ItemId],
                isArtwork: [false],
              })
            );
          });
          this.renderSPTableRows();
          this.dynamicSecondaryPackagingForm.patchValue({
            noOfPiecesRequired:
              this.secondaryPackagingFormArray.controls.length,
          });
          this.npdProjectService.setCustomFormProperty(
            OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.SECONDARY_PACKAGING
              .noOfPiecesRequired,
            this.secondaryPackagingFormArray.controls.length
          );
          this.loaderService.hide();
        },
        () => {
          this.loaderService.hide();
          this.notificationService.error(
            'Error while creating secondary packaging rows.'
          );
        }
      );

  }

  deleteSecondaryPackagingGroup(index?) {
    const formArray = this.dynamicSecondaryPackagingForm.get(
      'secondaryPackaging'
    ) as FormArray;
    const deleteItem = formArray.controls[index].value.itemId;
    this.loaderService.show();
    this.entityService.deleteEntityObjects(deleteItem).subscribe(
      (response) => {
        this.monsterUtilService.removeFormProperty(deleteItem);
        this.monsterUtilService.removeFormRelations(deleteItem);
        this.secondaryPackagingFormArray.removeAt(index);
        //this.dataSource.splice(index, 1);
        this.loaderService.hide();
        this.renderSPTableRows();
        this.dynamicSecondaryPackagingForm.patchValue({
          noOfPiecesRequired: this.secondaryPackagingFormArray.controls.length,
        });
        this.npdProjectService.setCustomFormProperty(
          OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.SECONDARY_PACKAGING
            .noOfPiecesRequired,
          this.secondaryPackagingFormArray.controls.length
        );
        this.notificationService.success(
          'Secondary Pack deleted successfully.'
        );
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error('Something went wrong while deleting.');
      }


    );

    let parameters = {
      MPMProjectId: {
        Id: this.MPMProjectId.split('.')[1],
        ItemId: this.MPMProjectId
      }

    };
    this.monsterConfigService.updateSeconaryPackaging(parameters);
  }
  /**
   * Toggle selected rows in Market Scope table
   */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.secondaryPackagingFormArray.controls.forEach((row) =>
        this.selection.select(row)
      );
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.secondaryPackagingFormArray.controls.length;
    return numSelected === numRows;
  }

  checkboxLabel(row?: string): string {
    return '';
  }

  getIsoFormatDateString(dateObject) {
    const newDate = new Date(dateObject);
    const year = newDate.getFullYear();
    const actualMonth = newDate.getMonth() + 1;
    const month = actualMonth.toString().length <= 1 ? '0' + actualMonth : actualMonth;
    const date = newDate.getDate().toString().length <= 1 ? '0' + newDate.getDate() : newDate.getDate();
    return year + '-' + month + '-' + date + "T00:00:00.000Z"
  }

  bulkDeleteSP() {
    if (
      this.dynamicSecondaryPackagingForm.value.secondaryPackaging.length === 0
    ) {
      this.notificationService.error('Add at Least one Market Scope.');
      return;
    }
    if (this.selection.selected.length < 1) {
      this.notificationService.error('Select Market(s) to delete.');
      return;
    }
    this.loaderService.show();
    const formArray = this.dynamicSecondaryPackagingForm.get(
      'secondaryPackaging'
    ) as FormArray;
    const itemIds = [];
    for (let i = 0; i < this.selection.selected.length; i++) {
      itemIds.push(this.selection.selected[i].value.itemId);
    }
    const formRowsLenght = formArray.controls.length;
    this.entityService
      .deleteMultipleEntityObjects(
        itemIds,
        EntityListConstant.SECONDARY_PACKAGING,
        this.utilService.APP_ID
      )
      .subscribe(
        () => {
          for (let i = formRowsLenght - 1; i > -1; i--) {
            if (itemIds.indexOf(formArray.controls[i].value.itemId) !== -1) {
              this.monsterUtilService.removeFormProperty(
                formArray.controls[i].value.itemId
              );
              this.monsterUtilService.removeFormRelations(
                formArray.controls[i].value.itemId
              );
              this.secondaryPackagingFormArray.removeAt(i);
            }
          }
          this.selection = new SelectionModel<Element>(true, []);
          this.renderSPTableRows();
          this.dynamicSecondaryPackagingForm.patchValue({
            noOfPiecesRequired:
              this.secondaryPackagingFormArray.controls.length,
          });
          this.npdProjectService.setCustomFormProperty(
            OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.SECONDARY_PACKAGING
              .noOfPiecesRequired,
            this.secondaryPackagingFormArray.controls.length
          );
          this.loaderService.hide();
          this.notificationService.success(
            'Successfully completed the deletion process.'
          );
        },

        () => {
          this.notificationService.error(
            'Error while deleting the market scope.'
          );
        }

      );
    let parameters = {
      MPMProjectId: {
        Id: this.MPMProjectId.split('.')[1],
        ItemId: this.MPMProjectId
      }

    };
    this.monsterConfigService.updateSeconaryPackaging(parameters);
  }

  updateFormControlsDisability() {
    for (let i = 0; i < this.secondaryPackagingFormArray.length; i++) {
      this.monsterUtilService.disableFormControls(
        this.secondaryPackagingFormArray.controls[i],
        this.secondaryPackagingConfig
      );
    }
  }

  isErrorPatternValid(index, control) {
    if (
      this.secondaryPackagingFormArray?.controls[index]?.get(control)?.touched
    ) {
      return (
        this.secondaryPackagingFormArray?.controls[index].get(control).errors &&
        this.secondaryPackagingFormArray?.controls[index].get(control).errors
          .pattern
      );
    }
  }
  isErrorMaxLength(index, control) {
    if (
      this.secondaryPackagingFormArray?.controls[index]?.get(control)?.touched
    ) {
      return (
        this.secondaryPackagingFormArray?.controls[index].get(control).errors &&
        this.secondaryPackagingFormArray?.controls[index].get(control).errors
          .maxlength
      );
    }
  }

  mapSecondaryPackagingContentToForm(secondaryPackaging) {
    this.packFormats = this.listConfig?.secondaryPackLeadTime;
    this.printSuppliers = this.listConfig?.printSuppliers;
    //this.booleanConfigs = OverViewConstants.booleanConfig;
    this.dynamicSecondaryPackagingForm = this.formBuilder.group({
      secondaryPackaging: new FormArray([]),
      noOfPiecesRequired: new FormControl({
        value:
          this.projectData?.[
          OverViewConstants.OVERVIEW_MAPPINGS.SECONDARY_PACKAGING
            .noOfPiecesRequired
          ],
        disabled: true,
      }),
      spScoped: new FormControl({
        value:
          this.projectData?.[
          OverViewConstants.OVERVIEW_MAPPINGS.SECONDARY_PACKAGING
            .spScoped
          ]==null || this.projectData?.[
            OverViewConstants.OVERVIEW_MAPPINGS.SECONDARY_PACKAGING
              .spScoped
            ]==undefined?false:this.projectData?.[
              OverViewConstants.OVERVIEW_MAPPINGS.SECONDARY_PACKAGING
                .spScoped
              ],
        disabled: this.isEnabledSPScopedAccess(),
      })
      //NPD_PROJECT_NO_OF_PIECES_REQUIRED
    });
    this.spGridColumns = [
      'select',
      'required',
      'regsNeeded',
      'packFormat',
      'mpmJobNo',
      'mpmGroup',
      'printSupplier',
      'approvedDieLineAvailable',
      'itemNumber',
      'colourApproval',
      'invoiceApproved',
      'statusComments',
      'dateDevelopmentCompleted',
      'dateProofingCompleted',
      'actions',
    ];
    //[{value:targetStartDateValue, disabled: this.governanceMilestoneConfig.targetStartDate.disabled}]
    this.showSecondaryPackagingForm = true;
    for (let i = 0; i < secondaryPackaging.length; i++) {
      this.secondaryPackagingFormArray.push(
        this.formBuilder.group({
          required: [
            {
              value: secondaryPackaging[i]?.Properties?.Required,
              disabled: this.secondaryPackagingConfig.required.disabled,
            },
          ],
          regsNeeded: [
            {
              value: secondaryPackaging[i]?.Properties?.RegsNeeded,
              disabled: this.secondaryPackagingConfig.regsNeeded.disabled,
            },
          ],
          mpmJobNo: [
            {
              value: secondaryPackaging[i]?.Properties?.MPMJobNo,
              disabled: this.secondaryPackagingConfig.mpmJobNo.disabled,
            },
          ],
          mpmGroup: [
            {
              value: secondaryPackaging[i]?.Properties?.SubRequest,
              disabled: this.secondaryPackagingConfig.mpmGroup.disabled,
            },
          ],
          packFormat: [
            {
              value: secondaryPackaging[i]?.R_PO_PACK_FORMAT?.Identity?.ItemId,
              disabled: this.secondaryPackagingConfig.packFormat.disabled,
            },
          ],
          printSupplier: [
            {
              value:
                secondaryPackaging[i]?.R_PO_PRINT_SUPPLIER?.Identity?.ItemId,
              disabled: this.secondaryPackagingConfig.printSupplier.disabled,
            },
          ],
          approvedDieLineAvailable: [
            {
              value:
                secondaryPackaging[i]?.Properties?.ApprovedDieLineAvailable,
              disabled:
                this.secondaryPackagingConfig.approvedDieLineAvailable.disabled,
            },
          ],
          isArtwork: [{ value: secondaryPackaging[i]?.Properties?.isArtwork }],

          itemNumber: [
            {
              value: secondaryPackaging[i]?.Properties?.ItemNumber,
              disabled: this.secondaryPackagingConfig.itemNumber.disabled,
            },
            [Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/)],
          ],
          colourApproval: [
            {
              value: secondaryPackaging[i]?.Properties?.ColourApproval,
              disabled: this.secondaryPackagingConfig.colourApproval.disabled,
            },
          ],
          invoiceApproved: [
            {
              value: secondaryPackaging[i]?.Properties?.InvoiceApproved,
              disabled: this.secondaryPackagingConfig.invoiceApproved.disabled,
            },
          ],
          statusComments: [
            {
              value: secondaryPackaging[i]?.Properties?.StatusComments,
              disabled: this.secondaryPackagingConfig.statusComments.disabled,
            },
            [Validators.maxLength(2000)],
          ],
          dateDevelopmentCompleted: [
            {
              value:
                secondaryPackaging[i]?.Properties?.DateDevelopmentCompleted,
              disabled:
                secondaryPackaging[i]?.Properties?.RegsNeeded === 'false',
            },
          ],
          //"2021-11-16T08:27:00Z"
          dateProofingCompleted: [
            {
              value: secondaryPackaging[i]?.Properties?.DateProofingCompleted,

              disabled:
                this.secondaryPackagingConfig.dateProofingCompleted.disabled,
            },
          ],
          itemId: [secondaryPackaging[i].Identity.ItemId],
        })
      );
      this.renderSPTableRows();
    }
  }

  /**
   * To re-render material table
   */
  renderSPTableRows() {

    this.secondaryPackagingTable && this.secondaryPackagingTable.renderRows
      ? this.secondaryPackagingTable.renderRows()
      : console.log('No rows in Secondary Packaging.');
  }

  /**
   * To load Governance Milestones and set form groups
   */
  getAllSecondaryPackagingData() {

    // this.loaderService.show();
    this.loadingSecondaryPackaging = true;
    this.entityService
      .getEntityItemDetails(
        this.projectEntityItemId,
        EntityListConstant.SECONDARY_PACKAGING_RELATION
      )
      .subscribe(
        (secondaryPackagingResponse) => {
          // create form groups for GovernanceMilestones
          if (secondaryPackagingResponse && secondaryPackagingResponse.items) {

            this.secondaryPackagingResponse = secondaryPackagingResponse;
            secondaryPackagingResponse.items.sort((a, b) => (a.Identity.Id < b.Identity.Id ? -1 : 1));


            this.mapSecondaryPackagingContentToForm(
              secondaryPackagingResponse.items.filter((item) => {
                return item.Properties.IsArtwork === false;
              })
            );
            // this.loaderService.hide();
          }
          this.loadingSecondaryPackaging = false;
        },
        (error) => {
          // this.loaderService.hide();
          this.loadingSecondaryPackaging = false;
        }
      );
  }
  @Input() MPMProjectId;

  updateSPInfo(formControl, index, property?) {
    let propertyValue =
      OverViewConstants.OVERVIEW_SECONDARY_PACKAGING_ENTITY_SAVE_CONSTANTS[
      formControl
      ];
    this.monsterUtilService.setFormProperty(
      this.dynamicSecondaryPackagingForm?.value?.secondaryPackaging[index]
        ?.itemId,
      propertyValue,
      this.dynamicSecondaryPackagingForm?.value?.secondaryPackaging?.[index][
      formControl
      ]




    );
    let parameters = {
      MPMProjectId: {
        Id: this.MPMProjectId.split('.')[1],
        ItemId: this.MPMProjectId
      }

    };
    this.monsterConfigService.updateSeconaryPackaging(parameters);
  }

  updateSPRelationInfo(formControl, index, objValue?) {
    let relationName =
      OverViewConstants.OVERVIEW_SECONDARY_PACKAGING_ENTITY_SAVE_CONSTANTS[
      formControl
      ];
    let objectValue = objValue
      ? objValue
      : this.dynamicSecondaryPackagingForm?.value?.secondaryPackaging?.[index][
      formControl
      ];
    let oldPackFormat =
      this.secondaryPackagingResponse?.items[index]?.R_PO_PACK_FORMAT?.Identity
        ?.ItemId;
    this.monsterUtilService.setFormRelations(
      this.dynamicSecondaryPackagingForm?.value?.secondaryPackaging[index]
        ?.itemId,
      relationName,
      this.dynamicSecondaryPackagingForm?.value?.secondaryPackaging?.[index][
      formControl
      ]
    );
    let newPackFormat =
      this.dynamicSecondaryPackagingForm?.value?.secondaryPackaging?.[index][
      formControl
      ];
    
    if (oldPackFormat === newPackFormat) {
      return;
    }
    let parameter = {
      ProjectId: this.projectData.ID,
      npdProjectId: this.projectData.NPD_PROJECT_ITEM_ID.split('.')[1],
    };

    this.updateRequestData().subscribe(response => {
      this.monsterConfigService
        .handleSecondaryPackChange(parameter)
        .subscribe((response) => {
          console.log('updated');
        });
    }

    )
    let parameters = {
      MPMProjectId: {
        Id: this.MPMProjectId.split('.')[1],
        ItemId: this.MPMProjectId
      }

    };
    this.monsterConfigService.updateSeconaryPackaging(parameters);

  }

  checkDisability(formControl, index, property) {
    let isRegsNeeded =
      OverViewConstants.OVERVIEW_SECONDARY_PACKAGING_ENTITY_SAVE_CONSTANTS[
      formControl
      ];
    return this.dynamicSecondaryPackagingForm?.value?.secondaryPackaging?.[
      index
    ][formControl];
  }
  updateRequestData(): Observable<Array<any>> {
    return new Observable((observer) => {
      this.monsterUtilService.finalBulkUpdateFormProjectData(
        () => {
          observer.next([]);
          observer.complete();
        },
        () => {
          //error callback
          observer.error();
        }
      );
    });
  }

  updateSPScoped(){
    this.npdProjectService.setCustomFormProperty(
      OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.SECONDARY_PACKAGING.spScoped,
      this.dynamicSecondaryPackagingForm?.value?.spScoped
    );
  }

  isEnabledSPScopedAccess():boolean{
    if(this.projectData.PROJECT_STATUS_VALUE == 'Active' || this.projectData.PROJECT_STATUS_VALUE == 'OnHold'){
      if(this.utilService.getUserRoles().some(role=>
        role.Name==MONSTER_ROLES.NPD_SECONDARY_PACKAGING 
        || role.Name==MONSTER_ROLES.NPD_PM
        || role.Name==MONSTER_ROLES.NPD_ADMIN 
      ) || this.loggedInUserName ===this.projectData?.NPD_PROJECT_E2E_PM_NAME 
        || this.loggedInUserName===this.projectData?.NPD_PROJECT_CORP_PM_NAME)
        {
        return false;
        }
    }
   return true;
 }

  ngOnInit() {
    if (this.projectEntityItemId && this.secondaryPackagingConfig) {
      this.getAllSecondaryPackagingData();
    }

    if (navigator.language !== undefined) {
      //for mat date picker
      this.adapter.setLocale(navigator.language);
    }
  }


}
