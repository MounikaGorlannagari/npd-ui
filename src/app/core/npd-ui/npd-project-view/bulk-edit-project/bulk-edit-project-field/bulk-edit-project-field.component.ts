import { I, K, S } from '@angular/cdk/keycodes';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import {
  CategoryLevel,
  LoaderService,
  NotificationService,
  Priority,
  ProjectConstant,
  SessionStorageConstants,
  SharingService,
  UtilService,
} from 'mpm-library';
import { Observable, of } from 'rxjs';
import { NpdSessionStorageConstants } from '../../../request-view/constants/sessionStorageConstants';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { OverViewConstants } from '../../constants/projectOverviewConstants';
import { NpdProjectService } from '../../services/npd-project.service';
import { RequestService } from '../../../services/request.service';
import { ProjectBulkEditConstants } from '../../constants/ProjectBulkEditConstants';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-bulk-edit-project-field',
  templateUrl: './bulk-edit-project-field.component.html',
  styleUrls: ['./bulk-edit-project-field.component.scss'],
})
export class BulkEditProjectFieldComponent implements OnInit {
  @Input() bulkEditFieldsData;
  @Input() leadMarketRegion;
  @Output() selectedField: EventEmitter<any> = new EventEmitter<any>();
  @Output() removeSelectedField: EventEmitter<any> = new EventEmitter<any>();
  @Output() finalBulkEditFieldArray: EventEmitter<any> =
    new EventEmitter<any>();
  selectedValue: string;
  bulkEditFieldForm: FormGroup;
  pmReportingFieldForm: FormGroup;
  pmClassificationForm: FormGroup;
  readonly radioButtonsList = OverViewConstants.booleanConfig;
  fieldLabel: string;
  dataType: string;
  selectedFieldConfig: any;
  selectedFieldFinalConfig: any;
  selectedFieldValue: string = '';
  projectMapper: any;
  indexerMapper: any;
  requestMapper: any;
  itemId;
  ProjectMapperName;
  ProjectMapperId: string;
  IndexerMapperId: string;
  IndexerMapperName: string;
  RequestMapper: string;
  event0: any;
  event1: any;
  event2: any;
  event3: any;
  event4: any;
  event5: any;
  programManagerConfig: any;
  projClassificationConfig: any;
  earlyManufacturingSites = [];
  reportingProjectSubType = [];
  reportingProjectDetail = [];
  fieldType: any;
  allListConfig = {
    projectTypes: [],
  };
  emitted = false;
  canCompanyIds: any[];
  eventx: { order: number; value: any; };
  eventy: { order: number; value: any; };
  constructor(
    private notificationService: NotificationService,
    private npdProjectService: NpdProjectService,
    private requestService: RequestService,
    private loaderService: LoaderService,
    private sharingService: SharingService,
    private monsterConfigApiService: MonsterConfigApiService
  ) {}
  comboValues = [];
  multiSelectArray = [];
  @Input() show = {
    startDate: null,
  };

  fromDate = {
    startDate: null,
  };
  toDate = {
    startDate: null,
  };
  weekYear = {
    startDate: null,
  };

  getBool(date) {
    this.show[date] = !this.show?.[date];
  }
  changeWeek(weekFormat, date?) {
    this.weekYear[date] = weekFormat;
    this.show['startDate'] = false;
    this.bulkEditFieldForm.patchValue({ fieldValue: weekFormat });
    this.onFieldConfirm(true);
  }
  getPriorities(categoryLevelName) {
    this.loaderService.show();
    this.comboValues = [];
    const currCategoryObj: CategoryLevel = this.sharingService
      .getCategoryLevels()
      .find((data) => {
        return data.CATEGORY_LEVEL_TYPE === categoryLevelName;
      });
    if (currCategoryObj) {
      if (
        sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES) !== null
      ) {
        let allPriorities: Array<Priority> = JSON.parse(
          sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES)
        );
        let allPriority: Array<Priority> = allPriorities.filter(
          (priority: Priority) => {
            return (
              priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id ===
              currCategoryObj['MPM_Category_Level-id'].Id
            );
          }
        );
        // let priority = [];
        // priority.push(allPriority);
        this.comboValues = allPriority;
        this.loaderService.hide();
      } else {
        let allPriority: Array<Priority> = this.sharingService
          .getAllPriorities()
          .filter((priority: Priority) => {
            return (
              priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id ===
              currCategoryObj['MPM_Category_Level-id'].Id
            );
          });
        // let priority = [];
        // priority.push(allPriority);
        this.comboValues = allPriority;
        this.loaderService.hide();
      }
    }
  }

  getCanCompanies() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllCanCompanies().subscribe(
      (response) => {
        let canCompanies = [];
        canCompanies = response;
        if (canCompanies?.length > 0) {
          canCompanies.forEach((canCompany) => {
            let canCompanyValue = {
              displayName: canCompany?.CanCompany,
              value: canCompany?.['Can_Supplier_Data-id']?.['Id'],
            };
            this.comboValues.push(canCompanyValue);
            
          });
        }
        this.loaderService.hide();
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Can companies'
        );
      }
    );
  }
  getProgramTags() {
    this.loaderService.show();
    this.comboValues = [];
    this.monsterConfigApiService.getAllProgramTags().subscribe((response) => {
      let programTag = [];
      programTag = response;
      if (programTag?.length > 0) {
        programTag.forEach((canCompany) => {
          let programTagValue = {
            displayName: canCompany?.DisplayName,
            value: canCompany?.['Program_Tag-id'].Id,
          };
          this.comboValues.push(programTagValue);
          
        });
        this.loaderService.hide();
      }
    });
  }
  getTastingRequirements() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTastingRequirements().subscribe(
      (response) => {
        let printSuppliers = [];
        printSuppliers = response;
        if (printSuppliers?.length > 0) {
          printSuppliers.forEach((trialSupervisionQuality) => {
            let trialSupervisionQualityValue = {
              displayName: trialSupervisionQuality?.TastingRequirement,
              value:
                trialSupervisionQuality?.['Tasting_Requirement-id']?.['ItemId'],
            };
            this.comboValues.push(trialSupervisionQualityValue);
            
          });
          this.loaderService.hide();
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Tasting Requirements'
        );
      }
    );
  }
  getTrialProtocolWrittenByUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTrialProtocolWrittenByUsers().subscribe(
      (response) => {
        let printSuppliers = [];
        printSuppliers = response;
        if (printSuppliers?.length > 0) {
          printSuppliers.forEach((trialSupervisionQuality) => {
            let trialSupervisionQualityValue = {
              displayName: trialSupervisionQuality?.Users,
              value:
                trialSupervisionQuality?.['Trial_Protocol_Written_By-id']?.[
                  'ItemId'
                ],
            };
            this.comboValues.push(trialSupervisionQualityValue);
            
          });
          this.loaderService.hide();
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Can companies'
        );
      }
    );
  }
  getReportingProjectType() {
    this.loaderService.show();
    this.comboValues = [];
    this.monsterConfigApiService
      .getAllReportingProjectType()
      .subscribe((response) => {
        this.comboValues = this.monsterConfigApiService.transformResponse(
          response,
          'CP_Project_Type-id',
          true
        );
        this.loaderService.hide();
      });
  }
  getOPSPMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllOPSPMUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getE2EPMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllE2EPMUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getCorpPMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllCorpPMUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getRTMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllRTMUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getOpsPlannerUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllOpsPlannerUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }

  getFPMSUsers(){
    this.loaderService.show();
    this.comboValues=[];
    this.npdProjectService.getAllFPMSUsers().subscribe(response=>{
      this.comboValues=response;
      this.loaderService.hide();
    })
  }

  getGFGUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllGFGUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }

  getProjectSpecialistUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService
      .getAllProjectSpecialistUsers()
      .subscribe((response) => {
        this.comboValues = response;
        this.loaderService.hide();
      });
  }
  getRegulatoryLeadUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllRegulatoryLeadUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getCommercialLeadUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllCommercialLeadUsers().subscribe((response) => {
      this.comboValues = response;
      this.loaderService.hide();
    });
  }
  getSecondaryArtworkSpecialists() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService
      .getAllSecondaryArtworkSpecialists()
      .subscribe((response) => {
        this.comboValues = response;
        this.loaderService.hide();
      });
  }
  getDraftManufacturingLocation() {
    this.loaderService.show();
    this.comboValues = [];
    this.monsterConfigApiService
      .getAllDraftManufacturingLocations()
      .subscribe((response) => {
        let draftManufacturingLocationOptions = [];
        draftManufacturingLocationOptions = response;
        draftManufacturingLocationOptions.forEach(
          (draftManufacturingLocation) => {
            let location = {
              ItemId: draftManufacturingLocation?.Identity?.ItemId,
              Id: draftManufacturingLocation?.Identity?.Id,
              displayName:
                draftManufacturingLocation?.['R_PO_MARKET$Properties']
                  ?.DisplayName,
              regulatoryClassification:
                draftManufacturingLocation?.['R_PO_MARKET$Properties']
                  ?.RegulatoryClassification,
            };
            this.comboValues.push(location);
            this.loaderService.hide();
          }
        );
      }),
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Country Of Manufacture'
        );
      };
  }
  getTrialSupervisionQuality() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTrialSupervisionQuality().subscribe(
      (response) => {
        let printSuppliers = [];
        printSuppliers = response;
        if (printSuppliers?.length > 0) {
          printSuppliers.forEach((trialSupervisionQuality) => {
            let trialSupervisionQualityValue = {
              displayName: trialSupervisionQuality?.Users,
              value:
                trialSupervisionQuality?.['Trial_Supervision_Quality-id']?.[
                  'ItemId'
                ],
            };
            this.comboValues.push(trialSupervisionQualityValue);
           
          });
          this.loaderService.hide();
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Quality'
        );
      }
    );
  }

  getTrialSupervisionNPD() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTrialSupervisionNPD().subscribe(
      (response) => {
        let trialSupervisionNPDResponse = [];
        trialSupervisionNPDResponse = response;
        if (trialSupervisionNPDResponse?.length > 0) {
          trialSupervisionNPDResponse.forEach((trialSupervisionNPD) => {
            let trialSupervisionNPDValue = {
              displayName: trialSupervisionNPD?.Users,
              value:
                trialSupervisionNPD?.['Trial_Supervision_NPD-id']?.['ItemId'],
            };
            this.comboValues.push(trialSupervisionNPDValue);
            this.loaderService.hide();
          });
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Trial supervision NPD'
        );
      }
    );
  }
  getWhoWillCompleteUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllWhoWillCompleteUsers().subscribe(
      (response) => {
        let users = [];
        users = response;
        if (users?.length > 0) {
          users.forEach((user) => {
            let userValue = {
              displayName: user?.Users,
              value: user?.['Tech_Qual_Users-id']?.['ItemId'],
            };
            this.comboValues.push(userValue);
            this.loaderService.hide();
          });
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Who Will Complete users'
        );
      }
    );
  }
  getEarlyManufacturingSites(draftManufacturingLocation): Observable<any> {
    let draftManufacturingRequestParams = {
      'Draft_Manufacturing_Location-id': {
        Id:
          draftManufacturingLocation &&
          draftManufacturingLocation.split('.')[1],
        ItemId: draftManufacturingLocation,
      },
    };
    return new Observable((observer) => {
      this.monsterConfigApiService
        .fetchEarlyManufacturingSitesByDraftManufacturingLocation(
          draftManufacturingRequestParams
        )
        .subscribe(
          (response) => {
            this.earlyManufacturingSites =
              this.monsterConfigApiService.transformResponse(
                response,
                'Early_Manufacturing_Site-id',
                true
              );
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }
  getAllDeliveryQuarters() {
    this.loaderService.show();
    this.comboValues = [];
    let deliveryQuarters = [];
    this.monsterConfigApiService
      .getAllDeliveryQuarters()
      .subscribe((response) => {
        deliveryQuarters = this.monsterConfigApiService.transformResponse(
          response,
          'PM_Delivery_Quarter-id',
          true
        );
        if (deliveryQuarters?.length > 0) {
          deliveryQuarters.forEach((deliveryQuarter) => {
            let deliveryQuarterValue = {
              displayName: deliveryQuarter.displayName,
              value: deliveryQuarter.ItemId,
            };
            this.comboValues.push(deliveryQuarterValue);
            this.loaderService.hide();
          });
        }
      });
  }
  comboFieldSelection(dataType, selectedField) {
    if (dataType === 'COMBO' || dataType === 'MULTISELECT') {
      let SessionStorageConstant = selectedField[0].SessionStorageConstant;

      if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_OPSPM_USERS
      ) {
        this.getOPSPMUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_E2EPM_USERS
      ) {
        this.getE2EPMUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_CORPPM_USERS
      ) {
        this.getCorpPMUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_RTM_USERS
      ) {
        this.getRTMUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_OPS_PLANNER_USERS
      ) {
        this.getOpsPlannerUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_GFG_USERS
      ) {
        this.getGFGUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_PROJECT_SPECIALIST_USERS
      ) {
        this.getProjectSpecialistUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_REGULATORY_LEAD_USERS
      ) {
        this.getRegulatoryLeadUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_COMMERCIAL_LEAD_USERS
      ) {
        this.getCommercialLeadUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_SECONDARY_ARTWORK_USERS
      ) {
        this.getSecondaryArtworkSpecialists();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_REPORTING_TYPES
      ) {
        this.getReportingProjectType();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_CAN_COMPANIES
      ) {
        this.getCanCompanies();
      } else if (
        SessionStorageConstant === SessionStorageConstants.ALL_PRIORITIES
      ) {
        this.getPriorities(ProjectConstant.CATEGORY_LEVEL_PROJECT);
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_PROGRAM_TAG
      ) {
        this.getProgramTags();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TASTING_REQUIREMENT
      ) {
        this.getTastingRequirements();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS
      ) {
        this.getTrialProtocolWrittenByUsers();
      } else if (SessionStorageConstant === 'onSite') {
        this.comboValues = OverViewConstants.onSite;
      } else if (
        SessionStorageConstant === 'BottlerCommunicationStatusConfig'
      ) {
        this.comboValues =
          ProjectBulkEditConstants.BottlerCommunicationStatusConfig;
      } else if (SessionStorageConstant === 'RAGConfig') {
        this.comboValues = OverViewConstants.RAGConfig;
      }
      //DEPENDENT FIELD(FINAL CLASSFICATION)
      else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_DRAFT_MANUFACTURING_LOCATIONS
      ) {
        this.getDraftManufacturingLocation();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_QUALITY
      ) {
        this.getTrialSupervisionQuality();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_WHO_WILL_COMPLETE_USERS
      ) {
        this.getWhoWillCompleteUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_NPD
      ) {
        this.getTrialSupervisionNPD();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_DELIVERY_QUARTERS
      ) {
        this.getAllDeliveryQuarters();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_PROJECT_TYPES
      ) {
        this.comboValues = [];
        this.loaderService.show();
        this.getProjectTypes().subscribe(response=>{
          this.loaderService.hide();
        });
      }else if(
        SessionStorageConstant === NpdSessionStorageConstants.ALL_FPMS_USERS
      ){
        this.comboValues=[];
        this.getFPMSUsers();
      }
    }
  }

  // }
  onFieldSelection(event) {
    const selectedFieldId = event.value;
    this.bulkEditFieldsData.selectedFieldId = selectedFieldId;
    this.bulkEditFieldForm = new FormGroup({
      field: new FormControl(this.bulkEditFieldsData.selectedFieldId),
      fieldValue: new FormControl(''),
    });

    const selectedField =
      this.bulkEditFieldsData.availableBulkEditFields.filter((field) => {
        return field.FieldID === selectedFieldId;
      });
    this.selectedFieldConfig = selectedField;
    this.dataType = selectedField[0].DataType;
    this.fieldLabel = selectedField[0].FieldName;
    this.fieldType = selectedField[0].FieldType;
    this.comboFieldSelection(this.dataType, selectedField);
  }

  onFieldConfirm(event) {
    let isValid = false;
    if (this.bulkEditFieldsData.selectedFieldId) {
      isValid = true;
    }
    if (isValid) {
      if (this.fieldType === 'Dependent') {
        this.loaderService.show();
        let fieldName = this.fieldLabel;
        this.selectedFieldFinalConfig =
          ProjectBulkEditConstants.DEPENDENT_FIELDS_CONFIG[fieldName]; // initialzation
        let selectedDependentValue = ProjectBulkEditConstants[fieldName]; //value
        Object.keys(this.selectedFieldFinalConfig).forEach((key1) => {
          Object.keys(selectedDependentValue).forEach((key2) => {
            if (key1 == key2) {
              this.ProjectMapperId =
                this.selectedFieldFinalConfig[
                  key1
                ].projectMappers?.ProjectMapperId;
              this.ProjectMapperName =
                this.selectedFieldFinalConfig[
                  key1
                ].projectMappers?.ProjectMapperName;
              this.IndexerMapperId =
                this.selectedFieldFinalConfig[
                  key1
                ].indexerMappers?.IndexerMapperId;
              this.IndexerMapperName =
                this.selectedFieldFinalConfig[
                  key1
                ].indexerMappers?.IndexerMapperName;
              this.RequestMapper =
                this.selectedFieldFinalConfig[
                  key1
                ].requestMappers?.RequestMapper;
              this.selectedFieldValue = selectedDependentValue[key1].value;
              this.itemId = selectedDependentValue[key1].itemId;
             
              this.initializeSelectedValue();
              this.emitValue();
            }
          });
        });
        this.loaderService.hide();
      } else if (this.fieldType === 'DATE' || this.fieldType === 'DATETIME') {
        this.selectedFieldValue =
          this.bulkEditFieldForm.controls.fieldValue.value;
        this.ProjectMapperName = this.selectedFieldConfig[0].ProjectMapperName;

        this.ProjectMapperId = this.selectedFieldConfig[0].ProjectMapperId;
        this.IndexerMapperId = this.selectedFieldConfig[0].IndexerMapperId;
        this.IndexerMapperName = this.selectedFieldConfig[0].IndexerMapperName;
        this.RequestMapper = this.selectedFieldConfig[0].RequestMapper;
        let isCalculationRequired =
          this.selectedFieldConfig[0].IsCalculationRequired === 'Yes'
            ? true
            : false;
        if (isCalculationRequired) {
          let eventC = {
            order: 4,
            value: isCalculationRequired,
          };
          this.finalBulkEditFieldArray.emit(eventC);
        }
        this.initializeSelectedValue();
        this.emitValue();
      } else {
        this.emitValue();
      }

      this.bulkEditFieldForm.controls.field.disable();
      this.bulkEditFieldsData.isFieldSelected = true;
      if (this.fieldType !== 'Dependent') {
        this.bulkEditFieldsData.selectedFieldValue = this.selectedFieldValue;
        this.selectedField.next(this.bulkEditFieldsData);
      }
    } else {
      this.notificationService.info('Kindly select field value first');
    }
  }

  onRemoveField() {
    this.removeSelectedField.next(this.bulkEditFieldsData);
  }

  validateSelectedField() {
    if (!this.bulkEditFieldsData.selectedFieldId) {
      this.bulkEditFieldsData.selectedFieldId =
        this.bulkEditFieldsData.availableBulkEditFields[0].FieldID;
    }
    if (this.bulkEditFieldsData.selectedFieldId) {
      const selectedField =
        this.bulkEditFieldsData.availableBulkEditFields.filter((field) => {
          return field.FieldID === this.bulkEditFieldsData.selectedFieldId;
        });

      this.dataType = selectedField[0].DataType;
      this.fieldLabel = selectedField[0].FieldName;
      this.fieldType = selectedField[0].FieldType;
    }
  }

  initializeForm(): void {
    this.validateSelectedField();
    this.bulkEditFieldForm = new FormGroup({
      field: new FormControl(
        this.bulkEditFieldsData.selectedFieldId,
        Validators.required
      ),
      fieldValue: new FormControl('', Validators.required),
    });

    this.bulkEditFieldForm.controls.fieldValue.valueChanges.subscribe(
      (selectedValue) => {
        this.bulkEditFieldsData.selectedFieldValue = selectedValue;
      }
    );
    const selectedField =
      this.bulkEditFieldsData.availableBulkEditFields.filter((field) => {
        return field.FieldID === this.bulkEditFieldsData.selectedFieldId;
      });
    this.selectedFieldConfig = selectedField;
    if (this.selectedFieldConfig[0].DataType === 'COMBO' || this.selectedFieldConfig[0].DataType === 'MULTISELECT') {
      this.comboFieldSelection(
        this.selectedFieldConfig[0].DataType,
        this.selectedFieldConfig
      );
    }
  }

  onFieldValueSelection(event) {
    
    this.multiSelectArray = [];
    this.canCompanyIds = [];
    if (this.dataType === 'MULTISELECT') {
      this.comboValues.forEach((multiSelectField) => {
        event.value.forEach((Id: any) => {
          if (multiSelectField.value == Id) {
            this.multiSelectArray.push(multiSelectField.displayName);
            if(this.selectedFieldConfig[0].FieldName === 'Name of Can Company (s)'){
              this.canCompanyIds.push(multiSelectField.value);
            }
          }
        });
      });
      if (this.selectedFieldConfig[0].FieldName === 'Name of Can Company (s)'){
        this.projectMapper = {
          ['CanCompanyIds']: this.canCompanyIds.toString(),
        };
        this.eventx = {
          order: 2,
          value: this.projectMapper,
        };
        this.indexerMapper = {
          ['NPD_PROJECT_NAME_OF_CAN_COMPANY_IDS']:
            this.canCompanyIds.toString(),
        };
        this.eventy = {
          order: 1,
          value: this.indexerMapper,
        };
        ;
        this.finalBulkEditFieldArray.emit(this.eventx)
        this.finalBulkEditFieldArray.emit(this.eventy);
      }
        
      this.selectedFieldValue = this.multiSelectArray.toString();
      this.itemId = this.multiSelectArray.length;
    } else if (event.value) {
      if (
        this.dataType === 'COMBO' &&
        this.selectedFieldConfig[0].IsUser === 'No' &&
        this.selectedFieldConfig[0].FieldName !== 'Priority'
      ) {
        this.selectedFieldValue = this.comboValues.find((field) => {
          return field.value === event.value;
        })?.displayName;
        if (this.selectedFieldValue === 'NO COMS TO BOTTLER') {
          this.selectedFieldValue = 'NO_COMS_TO_BOTTLER';
        }
        this.itemId = event.value;
      } else {
        this.selectedFieldValue = event.value;
      }
    } else if (event.value == false || event.value == true) {
      this.selectedFieldValue = event.value;
    } else {
      this.selectedFieldValue = this.bulkEditFieldForm.get('fieldValue').value;
      if (this.selectedFieldConfig[0].IsRequest == 'Yes') {
        this.itemId = this.selectedFieldValue;
      }
    }

    this.ProjectMapperName = this.selectedFieldConfig[0].ProjectMapperName;

    this.ProjectMapperId = this.selectedFieldConfig[0].ProjectMapperId;
    this.IndexerMapperId = this.selectedFieldConfig[0].IndexerMapperId;
    this.IndexerMapperName = this.selectedFieldConfig[0].IndexerMapperName;
    this.RequestMapper = this.selectedFieldConfig[0].RequestMapper;
    let isCalculationRequired =
      this.selectedFieldConfig[0].IsCalculationRequired === 'Yes'
        ? true
        : false;
    if (isCalculationRequired) {
      let eventC = {
        order: 4,
        value: isCalculationRequired,
      };
      this.finalBulkEditFieldArray.emit(eventC);
    }
    let isPhaseDelayAvailable =
      this.selectedFieldConfig[0].FieldName ===
      'Program Manager CP1 Phase Delay'
        ? true
        : false;
    if (isPhaseDelayAvailable) {
      let eventC = {
        order: 5,
        value: isPhaseDelayAvailable,
      };
      this.finalBulkEditFieldArray.emit(eventC);
    }
    let isPriorityAvailable =
      this.selectedFieldConfig[0].FieldName === 'Priority' ? true : false;
    if (isPriorityAvailable) {
      let eventC = {
        order: 6,
        value: this.selectedFieldValue,
      };
      this.finalBulkEditFieldArray.emit(eventC);
      this.onFieldConfirm(true);
      return;
    }

    if (
      this.selectedFieldConfig[0].IsDynamic === 'Yes' &&
      this.selectedFieldConfig[0].IsUser === 'Yes'
    ) {
      this.loaderService.show();
      this.requestService.getUserByID(this.selectedFieldValue).subscribe(
        (response) => {
          this.itemId = response.result.items[0].Identity.ItemId;
          this.comboValues.forEach((user) => {
            if (user.name === this.selectedFieldValue) {
              this.selectedFieldValue = user.displayName;
              return;
            }
          });
          this.initializeSelectedValue();
          let userId = this.itemId.split('.')[1];
          let roleName = this.selectedFieldConfig[0].FieldName;
          roleName = this.npdProjectService.getRoleAssignmentName(roleName);
          this.event0 = {
            order: 0,
            value: {
              [roleName]: userId,
            },
          };
          this.onFieldConfirm(true);
          this.loaderService.hide();
        },
        (error) => {
          this.loaderService.hide();
        }
      );
    } else {
      this.initializeSelectedValue();
    }
    this.onFieldConfirm(true);
  }

  initializeSelectedValue() {
    if (typeof this.ProjectMapperId == 'string') {
      this.projectMapper = {
        [this.ProjectMapperId]: this.itemId,
      };
      this.event1 = {
        order: 2,
        value: this.projectMapper,
      };
    }
    if (
      typeof this.ProjectMapperName == 'string' &&
      this.ProjectMapperName != 'null'
    ) {
      this.projectMapper = {
        [this.ProjectMapperName]: this.selectedFieldValue,
      };
      this.event2 = {
        order: 2,
        value: this.projectMapper,
      };
    }

    if (typeof this.IndexerMapperId == 'string') {
      this.indexerMapper = {
        [this.IndexerMapperId]: this.itemId,
      };
      this.event3 = {
        order: 1,
        value: this.indexerMapper,
      };
    }
    if (typeof this.IndexerMapperName == 'string') {
      this.indexerMapper = {
        [this.IndexerMapperName]: this.selectedFieldValue,
      };
      this.event4 = {
        order: 1,
        value: this.indexerMapper,
      };
    }
    if (
      typeof this.RequestMapper == 'string' &&
      this.selectedFieldConfig[0]?.IsRequest === 'Yes'
    ) {
      if (this.itemId) {
        let pathObjectMapper = this.npdProjectService.getObjectArrayFromPath(
          this.RequestMapper,
          this.itemId
        );
        this.event5 = {
          order: 3,
          value: JSON.parse('{' + pathObjectMapper + '}'),
        };
      }
    }
  }

  getReportingProjectSubType(parameters): Observable<any> {
    this.loaderService.show();
    return new Observable((observer) => {
      this.monsterConfigApiService
        .fetchReportingProjectSubType(parameters)
        .subscribe(
          (response) => {
            this.reportingProjectSubType =
              this.monsterConfigApiService.transformResponse(
                response,
                'Project_Sub_Type-id',
                true
              );
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }
  getReportingProjectDetail(parameters): Observable<any> {
    this.loaderService.show();
    return new Observable((observer) => {
      this.monsterConfigApiService
        .fetchReportingProjectDetail(parameters)
        .subscribe(
          (response) => {
            this.reportingProjectDetail =
              this.monsterConfigApiService.transformResponse(
                response,
                'Project_Detail-id',
                true
              );
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }
  getProjectTypes(): Observable<any> {
    return new Observable((observer) => {
      this.monsterConfigApiService.getAllProjectTypes().subscribe(
        (response) => {
          this.allListConfig.projectTypes =
            this.monsterConfigApiService.transformResponse(
              response,
              'Project_Type-id',
              true
            );
          // this.comboValues = this.allListConfig.projectTypes
          if (this.allListConfig.projectTypes?.length > 0) {
            this.allListConfig.projectTypes.forEach((projectType) => {
              let projectTypeValue = {
                displayName: projectType.displayName,
                value: projectType.ItemId,
              };
              this.comboValues.push(projectTypeValue);
            });
          }
          observer.next(true);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }
  onbulkProgrammeManagerReportingForm(pmReportingForm) {
    this.pmReportingFieldForm = pmReportingForm;
    this.bulkEditFieldsData.selectedFieldValue = 'dependent';
    this.selectedField.next(this.bulkEditFieldsData);
    ProjectBulkEditConstants['Reporting Project Type'].projectType.itemId =
      this.pmReportingFieldForm.get('projectType').value;
    ProjectBulkEditConstants['Reporting Project Type'].projectSubType.itemId =
      this.pmReportingFieldForm.get('projectSubType').value;
    ProjectBulkEditConstants['Reporting Project Type'].projectDetail.itemId =
      this.pmReportingFieldForm.get('projectDetail').value;

    const projectTypeID =
      ProjectBulkEditConstants['Reporting Project Type'].projectType.itemId;
    const projectSubTypeID =
      ProjectBulkEditConstants['Reporting Project Type'].projectSubType.itemId;
    const projectDetailID =
      ProjectBulkEditConstants['Reporting Project Type'].projectDetail.itemId;
    const projectTypeRequestParams = {
      'CP_Project_Type-id': {
        Id: projectTypeID && projectTypeID.split('.')[1],
        ItemId: projectTypeID,
      },
    };
    const projectSubTypeRequestParams = {
      'Project_Sub_Type-id': {
        Id: projectSubTypeID && projectSubTypeID.split('.')[1],
        ItemId: projectSubTypeID,
      },
    };
    ProjectBulkEditConstants['Reporting Project Type'].projectType.value =
      this.comboValues.find(
        (projectTypeId) => projectTypeId.ItemId === projectTypeID
      )?.displayName;

    this.getReportingProjectSubType(projectTypeRequestParams).subscribe(
      (response) => {
        if (projectSubTypeID) {
          ProjectBulkEditConstants[
            'Reporting Project Type'
          ].projectSubType.value = this.reportingProjectSubType.find(
            (projectSubtypeId) => projectSubtypeId.ItemId === projectSubTypeID
          )?.displayName;
          this.getReportingProjectDetail(projectSubTypeRequestParams).subscribe(
            (response) => {
              if (projectDetailID) {
                ProjectBulkEditConstants[
                  'Reporting Project Type'
                ].projectDetail.value = this.reportingProjectDetail.find(
                  (projectDetail) => projectDetail.ItemId === projectDetailID
                )?.displayName;
                this.loaderService.hide();
                this.onFieldConfirm(true);
              } else {
                this.onFieldConfirm(true);
              }
            }
          );
        } else {
          this.onFieldConfirm(true);
        }
      }
    );
  }
  onBulkFinalClassificationFormChange(finalClassificationForm) {
    this.bulkEditFieldsData.selectedFieldValue = 'dependent';
    this.selectedField.next(this.bulkEditFieldsData);
    this.assignClassificationField(finalClassificationForm);
  }

  assignClassificationField(finalClassificationForm) {
    this.pmClassificationForm = finalClassificationForm;
    let classficationSectionValue = this.pmClassificationForm.getRawValue();

    ProjectBulkEditConstants[
      'Country Of Manufacture(Final)'
    ].draftManufacturingLocation.itemId =
      classficationSectionValue.draftManufacturingLocation;
    const draftManufacturingLocationId =
      ProjectBulkEditConstants['Country Of Manufacture(Final)']
        .draftManufacturingLocation.itemId;
    ProjectBulkEditConstants[
      'Country Of Manufacture(Final)'
    ].earlyManufacturingSite.itemId =
      classficationSectionValue.earlyManufacturingSite;
    ProjectBulkEditConstants[
      'Country Of Manufacture(Final)'
    ].draftManufacturingLocation.value = this.comboValues?.find(
      (location) => location.ItemId === draftManufacturingLocationId
    )?.displayName;

    this.getEarlyManufacturingSites(draftManufacturingLocationId).subscribe(
      (response) => {
        this.loaderService.show();
        ProjectBulkEditConstants[
          'Country Of Manufacture(Final)'
        ].earlyManufacturingSite.value = this.earlyManufacturingSites.find(
          (site) =>
            site.ItemId ===
            ProjectBulkEditConstants['Country Of Manufacture(Final)']
              .earlyManufacturingSite.itemId
        )?.displayName;
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].projectType.itemId = classficationSectionValue.projectType;
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].projectType.value = this.allListConfig.projectTypes.find(projectType => projectType.ItemId == ProjectBulkEditConstants['Country Of Manufacture(Final)'].projectType.itemId)?.displayName
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].earlyProjectClassification.value = classficationSectionValue.earlyProjectClassification;
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].earlyProjectClassificationDesc.value = classficationSectionValue.earlyProjectClassificationDesc;
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].newFg.value = classficationSectionValue.newFg;
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].newHBCFormula.value = classficationSectionValue.newHBCFormula;
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].newRnDFormula.value = classficationSectionValue.newRnDFormula;
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].newPrimaryPackaging.value = classficationSectionValue.newPrimaryPackaging;
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].secondaryPackaging.value = classficationSectionValue.secondaryPackaging;
        // ProjectBulkEditConstants['Country Of Manufacture(Final)'].projectType.itemId = classficationSectionValue.projectType;
        ProjectBulkEditConstants[
          'Country Of Manufacture(Final)'
        ].registrationDossier.value =
          classficationSectionValue.registrationDossier;
        ProjectBulkEditConstants[
          'Country Of Manufacture(Final)'
        ].preProdReg.value = classficationSectionValue.preProdReg;
        ProjectBulkEditConstants[
          'Country Of Manufacture(Final)'
        ].postProdReg.value = classficationSectionValue.postProdReg;
        this.onFieldConfirm(true);
        this.loaderService.hide();
      },
      (error) => {
        this.loaderService.hide();
      }
    );
  }

  emitValue() {
    this.finalBulkEditFieldArray.emit(this.event0);
    this.finalBulkEditFieldArray.emit(this.event1);
    this.finalBulkEditFieldArray.emit(this.event2);
    this.finalBulkEditFieldArray.emit(this.event3);
    this.finalBulkEditFieldArray.emit(this.event4);
    this.finalBulkEditFieldArray.emit(this.event5);
  }

  
  numericOnly(event){
    let numericPattern = /^[0-9]*$/;
    return numericPattern.test(event.key);
 }

  ngOnInit(): void {
    this.initializeForm();
    this.programManagerConfig =
      this.npdProjectService.mapprogrammeManagerReportingConfigurations(
        true,
        true,
        '',
        false
      );
    this.projClassificationConfig =
      this.npdProjectService.mapBulkProjectClassificationConfigurations(
        true,
        true,
        this.leadMarketRegion
      );
    
  }
}
