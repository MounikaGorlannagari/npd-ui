import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoaderService, NotificationService } from 'mpm-library';
import { AppService } from 'mpm-library';
import { ProjectBulkEditConstants } from '../../constants/ProjectBulkEditConstants';
import { NpdProjectService } from '../../services/npd-project.service';
import { BusinessConstants } from '../../../request-view/constants/BusinessConstants';
@Component({
  selector: 'app-bulk-edit-project',
  templateUrl: './bulk-edit-project.component.html',
  styleUrls: ['./bulk-edit-project.component.scss']
})
export class BulkEditProjectComponent implements OnInit {
  bulkEditFieldsData = [];
  availableBulkEditFields: any = [];
  selectedBulkEditField: any = [];
  finalBulkEditFieldArray = {
    bulkAssignmentUsers: [],
    projectMapper: [],
    requestMapper: [],
    indexerMapper: [],

  }
  IsCalculationRequired: boolean = false;
  IsPriorityAvailable: boolean = false;
  projectItemID: string;
  projectID = [];
  BULK_EDIT_PROJECT_NS = 'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  BULK_EDIT_PROJECT_WS = 'BulkEditProject';
  IsPhaseDelayAvailable: boolean = false;
  priorityId: any;
  projectMappers = {};
  requestMappers = {};
  indexerMappers = {};
  bulkUserAssigment = {};
  selectedFieldConfig: any;
  selectedFieldFinalConfig: any;
  fieldType: any;
  leadMarketRegion;
  selectedProjectsProjectTypes: any[] = [];
  leadFormulaDependents={
    projectTypeFieldId: '',
    newRndFormulaFieldId: '',
    leadFormulaFieldId: ''
  }
  leadFormulaFieldObject:{};
  selectedProjectsNewRndFormula:any[]=[];

  constructor(
    public dialogRef: MatDialogRef<BulkEditProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public notificationService: NotificationService,
    public appService: AppService,
    private npdProjectService: NpdProjectService,
    private loaderService: LoaderService,
  ) { }

  addField() {
    
    let canAddNewField = true;
    this.bulkEditFieldsData.forEach(fieldData => {
      if (fieldData && (fieldData.selectedFieldValue === '' || fieldData.selectedFieldValue === undefined || fieldData.selectedFieldValue === null)) {
        canAddNewField = false;
      }
    });
    if (canAddNewField) {
      if (this.availableBulkEditFields.length > 0) {
        this.bulkEditFieldsData.push({
          selectedFieldId: '',
          selectedFieldValue: '',
          isFieldSelected: false,
          availableBulkEditFields: this.availableBulkEditFields
        });
      } else {
        this.notificationService.warn('No more fields to add');
      }
    } else {
      this.notificationService.warn(
        'Please provide the field value for the selected field(s)'
      );
    }
  }


  isFieldAlreadySelected(fieldID: string) {
    if (!fieldID) {
      return true;
    }
    return this.bulkEditFieldsData.find(field => {
      return field.selectedFieldId === fieldID;
    })
  }
  setAvailableBulkEditFields() {
    
    if(this.data.isTask4AndTask5){
      this.addLeadFormulaField();
    }
    this.availableBulkEditFields = [];
    for (const field of this.data.bulkEditFields) {
      if (!this.isFieldAlreadySelected(field.FieldID)) {
        this.availableBulkEditFields.push(field);
      }
    }
  }

  onRemoveSelectedField(removeField) {
    if (removeField && removeField.selectedFieldId) {
      this.bulkEditFieldsData = this.bulkEditFieldsData.filter(field => {
        return field.selectedFieldId !== removeField.selectedFieldId;
      })
    }
    if (removeField && removeField.selectedFieldId) {
      removeField.availableBulkEditFields.forEach(field => {
        if (field.FieldID == removeField.selectedFieldId) {
          this.selectedFieldConfig = field

          return;
        }

      })
      if (this.selectedFieldConfig) {
        this.fieldType = this.selectedFieldConfig.FieldType;
        if (this.fieldType === 'Dependent') {
          let fieldName = this.selectedFieldConfig.FieldName;
          this.selectedFieldFinalConfig = ProjectBulkEditConstants.DEPENDENT_FIELDS_CONFIG[fieldName] // initialzation
          let selectedDependentValue = ProjectBulkEditConstants[fieldName]; //value
          Object.keys(this.selectedFieldFinalConfig).forEach(key1 => {
            Object.keys(selectedDependentValue).forEach(key2 => {
              if (key1 == key2) {
                let selectedFieldFinalConf: any = {};
                selectedFieldFinalConf.ProjectMapperId = this.selectedFieldFinalConfig[key1].projectMappers.ProjectMapperId
                selectedFieldFinalConf.ProjectMapperName = this.selectedFieldFinalConfig[key1].projectMappers.ProjectMapperName
                selectedFieldFinalConf.IndexerMapperId = this.selectedFieldFinalConfig[key1].indexerMappers.IndexerMapperId
                selectedFieldFinalConf.IndexerMapperName = this.selectedFieldFinalConfig[key1].indexerMappers.IndexerMapperName
                selectedFieldFinalConf.RequestMapper = this.selectedFieldFinalConfig[key1]?.requestMappers?.RequestMapper
                this.removeFieldFromSelection(selectedFieldFinalConf)
              }
            })

          })
        } else {
          this.selectedFieldFinalConfig = this.selectedFieldConfig;
          this.removeFieldFromSelection(this.selectedFieldFinalConfig)
        }

      }
    }
    this.setAvailableBulkEditFields();
  }

  onFieldSelected(event) {
    this.setAvailableBulkEditFields();
  }
  closeDialog(data): void {
    this.dialogRef.close(data);
  }
  assign<T, U>(target: T, source: U): asserts target is T & U {
    Object.assign(target, source)
  }
  onClickSave() {
    if (this.bulkEditFieldsData.length === 0) {
      this.notificationService.warn('Please add atleast one field to save.');
      return
    }
    let canSave = true;
    this.bulkEditFieldsData.forEach(fieldData => {
      if (fieldData && (fieldData.selectedFieldValue === '' || fieldData.selectedFieldValue === undefined || fieldData.selectedFieldValue === null)) {
        canSave = false;
      }
    });
    // let isSave = this.bulkEditFieldsData.every(field => field.selectedFieldValue === "" || field.selectedFieldValue === undefined);
    if (!canSave) {
      this.notificationService.warn('Please provide the field value for the added field(s).');
      return;
    }
    this.loaderService.show()
    this.finalBulkEditFieldArray.indexerMapper.push({ 'NPD_PROJECT_IS_BULK_EDIT': false })
    this.finalBulkEditFieldArray.projectMapper.map(data => { this.assign(this.projectMappers, data) })
    this.finalBulkEditFieldArray.requestMapper.map(data => { this.assign(this.requestMappers, data) })
    this.finalBulkEditFieldArray.indexerMapper.map(data => { this.assign(this.indexerMappers, data) })
    this.finalBulkEditFieldArray.bulkAssignmentUsers.map(data => { this.assign(this.bulkUserAssigment, data) })


    let parameter: any = {
      bulkEditProjects: {
        projectID: [...this.projectID],
        projectItemId: this.projectItemID,
        projectMappers: this.projectMappers,
        requestMappers: this.requestMappers,
        indexerMappers: this.indexerMappers,
        bulkUserAssigment: this.bulkUserAssigment,
        isCalculationRequired: this.IsCalculationRequired,
        priorityId: this.IsPriorityAvailable == true ? this.priorityId : '',
        isPhaseDelayAvailable: this.IsPhaseDelayAvailable
      }


    }
let projectTeamRoles=['NPD_PROJECT_E2E_PM_ITEM_ID',
'NPD_PROJECT_CORP_PM_ITEM_ID',
'NPD_PROJECT_OPS_PM_ITEM_ID',
'NPD_PROJECT_REGULATORY_LEAD_ITEM_ID',
'NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ITEM_ID',
'NPD_PROJECT_PR_SPECIALIST_ITEM_ID',
'NPD_PROJECT_OPS_PLANNER_ITEM_ID',
'NPD_PROJECT_SECONDARY_AW_SPECIALIST',
'NPD_PROJECT_COMMERCIAL_LEAD_ITEM_ID',
'NPD_PROJECT_GFG_ITEM_ID',
'NPD_PROJECT_FPMS_ITEM_ID'];
let uniqueUsers=[];

for(let role in  this.indexerMappers){
  if(projectTeamRoles.includes(role)){
    if(!uniqueUsers.find(x=>x.userID==this.indexerMappers[role].split('.')[1])){
    uniqueUsers.push({userID:this.indexerMappers[role].split('.')[1]});
    }
  }
}
parameter.bulkEditProjects.projectTeamUpdate=uniqueUsers;

    this.appService.invokeRequest(this.BULK_EDIT_PROJECT_NS, this.BULK_EDIT_PROJECT_WS, parameter).subscribe(response => {
      this.loaderService.hide();
      this.notificationService.success('Bulk Edit Process Initiated for the selected Project(s)');
      this.dialogRef.close(true);
    }, (error) => {
      this.notificationService.error('Something went wrong while bulk Editing Project(s).')
      this.loaderService.hide();
      this.dialogRef.close();
    }
    )
  }

  onFieldConfirmFinal(event) {

    if (event && event.order == 0) {
      this.finalBulkEditFieldArray.bulkAssignmentUsers.push(event.value);
    }
    else if (event && event.order == 1) {
      this.finalBulkEditFieldArray.indexerMapper.push(event.value);
    }
    else if (event && event.order == 2) {
      this.finalBulkEditFieldArray.projectMapper.push(event.value);
    }
    else if (event && event.order == 3) {
      this.finalBulkEditFieldArray.requestMapper.push(event.value);
    }
    else if (event && event.order == 4) {
      this.IsCalculationRequired = true;
    }
    else if (event && event.order == 5) {
      this.IsPhaseDelayAvailable = true;
    }
    else if (event && event.order == 6) {
      this.IsPriorityAvailable = true;
      this.priorityId = event.value;
    }


  }

  removeFieldFromSelection(selectedFieldFinalConfig) {
    if (selectedFieldFinalConfig.FieldName === 'Name of Can Company (s)') {
      this.finalBulkEditFieldArray.projectMapper =
        this.finalBulkEditFieldArray.projectMapper.filter((field) => {
          return !('CanCompanyIds' in field);
        });
       this.finalBulkEditFieldArray.indexerMapper =
         this.finalBulkEditFieldArray.indexerMapper.filter((field) => {
           return !('NPD_PROJECT_NAME_OF_CAN_COMPANY_IDS' in field);
         });
    }
      this.finalBulkEditFieldArray.projectMapper =
        this.finalBulkEditFieldArray.projectMapper.filter((field) => {
          return !(selectedFieldFinalConfig.ProjectMapperName in field);
        });
    this.finalBulkEditFieldArray.projectMapper = this.finalBulkEditFieldArray.projectMapper.filter(field => {
      return !(selectedFieldFinalConfig.ProjectMapperId in field)
    })
    this.finalBulkEditFieldArray.indexerMapper = this.finalBulkEditFieldArray.indexerMapper.filter(field => {
      return !(selectedFieldFinalConfig.IndexerMapperName in field)
    })
    this.finalBulkEditFieldArray.indexerMapper = this.finalBulkEditFieldArray.indexerMapper.filter(field => {
      return !(selectedFieldFinalConfig.IndexerMapperId in field)
    })
    if (this.selectedFieldConfig.IsDynamic === 'Yes' && this.selectedFieldConfig.IsRequest === 'Yes') {
      var keyPath = (selectedFieldFinalConfig.RequestMapper).toString().split('/')[0]
      this.finalBulkEditFieldArray.requestMapper = this.finalBulkEditFieldArray.requestMapper.filter(field => {
        const r = Object.keys(field);
        return r[0] && r[0] != keyPath
      })
    }

    if (this.selectedFieldConfig.IsDynamic === 'Yes' && this.selectedFieldConfig.IsUser === 'Yes') {
      let roleName = this.npdProjectService.getRoleAssignmentName(this.selectedFieldConfig.FieldName);
      this.finalBulkEditFieldArray.bulkAssignmentUsers = this.finalBulkEditFieldArray.bulkAssignmentUsers.filter(field => {
        return !(roleName in field)
      })

      this.finalBulkEditFieldArray.bulkAssignmentUsers.splice(this.finalBulkEditFieldArray.bulkAssignmentUsers.findIndex(item => item?.roleName === roleName), 1);

    }
  }
  ngOnInit(): void {
    this.data.selectedProjectData.forEach((data: any) => {
      this.projectItemID = data.ITEM_ID.split('.')[0]
      this.projectID.push({ 'projectId': data.ID });
      this.selectedProjectsProjectTypes.push(data.NPD_PROJECT_NEW_PROJECT_TYPE);
      this.selectedProjectsNewRndFormula.push(data.NPD_PROJECT_NEW_RD_FORMULA)
    })
    this.leadMarketRegion = this.data.leadMarketRegion;
    if(this.data.isTask4AndTask5){
      this.formLeadFormulaRelatedObject();
      this.addLeadFormulaField();
    }
    this.setAvailableBulkEditFields();
    
  }


  formLeadFormulaRelatedObject(){
    this.data.bulkEditFields.forEach(field=>{
      if(field.IndexerMapperName=='NPD_PROJECT_NEW_PROJECT_TYPE'){
        this.leadFormulaDependents.projectTypeFieldId=field.FieldID;
      }else if(field.IndexerMapperName=='NPD_PROJECT_NEW_RD_FORMULA'){
        this.leadFormulaDependents.newRndFormulaFieldId= field.FieldID;
      }else if(field.IndexerMapperName=='NPD_PROJECT_LEAD_FORMULA'){
        this.leadFormulaDependents.leadFormulaFieldId= field.FieldID
        this.leadFormulaFieldObject=field;
      }
    })
  }

  addLeadFormulaField(){
    let preProjectType=this.selectedProjectsProjectTypes.every(projectType=>projectType==BusinessConstants.PROJECT_TYPES.COMMERCIAL_NEW_SKU);
    let preNewRndFormula=this.selectedProjectsNewRndFormula.every(value=>value=='true');
    let selectedProjectType;
    let selectedNewRndFormula;
    let projectType;
    let newRndFormula;
    let isLeadFormulaAddable;
    if(this.bulkEditFieldsData.length==0){
      if(preProjectType && preNewRndFormula){
        this.pushLeadFormulaToBulkFields()
      }else{
        this.data.bulkEditFields=this.data.bulkEditFields.filter(field=>field.IndexerMapperName!='NPD_PROJECT_LEAD_FORMULA');
      }
    }
    else if(this.bulkEditFieldsData.length!=0){
      projectType=this.bulkEditFieldsData.find(field=>field.selectedFieldId==this.leadFormulaDependents.projectTypeFieldId);
      if(projectType){
        selectedProjectType=projectType.selectedFieldValue==BusinessConstants.PROJECT_TYPES.COMMERCIAL_NEW_SKU?true:false;
      }
      newRndFormula=this.bulkEditFieldsData.find(field=>field.selectedFieldId==this.leadFormulaDependents.newRndFormulaFieldId);
      if(newRndFormula){
        selectedNewRndFormula=newRndFormula.selectedFieldValue;
      }
      if(selectedProjectType==null && selectedNewRndFormula==null){
        if(preProjectType && preNewRndFormula){
          this.pushLeadFormulaToBulkFields()
        }
      }else if(selectedProjectType && selectedNewRndFormula){
        this.pushLeadFormulaToBulkFields()
      }else if(selectedProjectType && preNewRndFormula && (selectedNewRndFormula==null || selectedNewRndFormula==undefined)){
        this.pushLeadFormulaToBulkFields()
      }else if(selectedNewRndFormula && preProjectType && (selectedProjectType==null || selectedProjectType==undefined)){
        this.pushLeadFormulaToBulkFields()
      }
      else{
        if(this.isLeadFormulaPresent()){
          this.data.bulkEditFields=this.data.bulkEditFields.filter(field=>field.IndexerMapperName!='NPD_PROJECT_LEAD_FORMULA');
        }
        if(this.isSelectedLeadFormula().isSelectedLeadFormula){
          this.notificationService.warn("Removed the lead market added from the list Due to Improper combination");
          this.onRemoveSelectedField(this.isSelectedLeadFormula().selectedLeadFormulaObject);
        }
      }
    }
  }

  isLeadFormulaPresent(){
    return this.data.bulkEditFields.some(field=>field.IndexerMapperName=='NPD_PROJECT_LEAD_FORMULA');
  }

  isSelectedLeadFormula(){
    let isSelectedLeadFormula=false;
    let selectedLeadFormulaObject;
    this.bulkEditFieldsData.forEach(field=>{
      if(field.selectedFieldId==this.leadFormulaDependents.leadFormulaFieldId){
        isSelectedLeadFormula=true;
        selectedLeadFormulaObject=field;
      }
    })
    return {
      isSelectedLeadFormula: isSelectedLeadFormula,
      selectedLeadFormulaObject: selectedLeadFormulaObject
    }
  }

  pushLeadFormulaToBulkFields(){
    if(this.isLeadFormulaPresent()){
    }else{
      this.data.bulkEditFields.push(this.leadFormulaFieldObject)
    }
  }



}
