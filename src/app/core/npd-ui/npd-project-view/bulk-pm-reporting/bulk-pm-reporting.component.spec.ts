import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkPmReportingComponent } from './bulk-pm-reporting.component';

describe('BulkPmReportingComponent', () => {
  let component: BulkPmReportingComponent;
  let fixture: ComponentFixture<BulkPmReportingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkPmReportingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkPmReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
