import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { outputs } from '@syncfusion/ej2-angular-gantt/src/gantt/gantt.component';
import { LoaderService } from 'mpm-library';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { OverViewConstants } from '../constants/projectOverviewConstants';
import { PmProgrammeManagerReportingComponent } from '../pm-programme-manager-reporting/pm-programme-manager-reporting.component';
import { NpdProjectService } from '../services/npd-project.service';

@Component({
  selector: 'app-bulk-pm-reporting',
  templateUrl: './bulk-pm-reporting.component.html',
  styleUrls: ['./bulk-pm-reporting.component.scss']
})
export class BulkPmReportingComponent extends PmProgrammeManagerReportingComponent implements OnInit {
  @Input() programmeManagerReporting;
  @Input() reportingProjectType;
  bulkProgrammeManagerReportingForm: FormGroup;
  @Output() onbulkProgrammeManagerReportingForm: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  constructor(public formBuilder: FormBuilder, public monsterUtilService: MonsterUtilService,
    public monsterConfigApiService: MonsterConfigApiService, public npdProjectService: NpdProjectService,
    public loaderService: LoaderService) {
    super(formBuilder, monsterUtilService, monsterConfigApiService, npdProjectService, loaderService);
  }


  updateRequestRelationsInfo(relation, formControl, index?) {
    let property;
    let propertyValue;
    let relationProperty;
    let relationvalue;
    if (formControl === 'projectType') {
      this.bulkProgrammeManagerReportingForm.patchValue({
        'projectSubType': '',
        'projectDetail': ''
      })
    }
    if (formControl === 'projectSubType') {
      this.bulkProgrammeManagerReportingForm.patchValue({
        'projectDetail': ''
      })
    }
    relationProperty = OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROGRAMME_MANAGER_REPORTING[relation];
    relationvalue = this.programmeManagerReportingForm.controls[formControl] && this.programmeManagerReportingForm.controls[formControl].value;//itemId
    property = OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROGRAMME_MANAGER_REPORTING[formControl];
    propertyValue = this.getRelationValue(formControl, relationvalue)//fetch value-display name
    if (property) {
      this.npdProjectService.setCustomFormProperty(property, propertyValue);
      this.npdProjectService.setCustomFormProperty(relationProperty, relationvalue);
    }
    if (this.bulkProgrammeManagerReportingForm.get('projectType').value !== '' ){//&& this.bulkProgrammeManagerReportingForm.get('projectSubType').value !== '' && this.bulkProgrammeManagerReportingForm.get('projectDetail').value !== '') {
      this.onbulkProgrammeManagerReportingForm.emit(this.bulkProgrammeManagerReportingForm);
    }
  }
  ngOnInit(): void {
    super.ngOnInit();
    this.bulkProgrammeManagerReportingForm = this.formBuilder.group({
      projectType: [''],
      projectSubType: [''],
      projectDetail: ['']
    })

  }

}
