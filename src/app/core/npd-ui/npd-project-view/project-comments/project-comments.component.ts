import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IndexerService, LoaderService, NotificationService, ProjectService, SearchRequest, SearchResponse } from 'mpm-library';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-project-comments',
  templateUrl: './project-comments.component.html',
  styleUrls: ['./project-comments.component.scss']
})
export class ProjectCommentsComponent implements OnInit {

  projectId;
  requestId;
  isGroupFilter:boolean;

  constructor(
    private activatedRoute: ActivatedRoute,private loaderService:LoaderService,private notificationService:NotificationService,
    private projectService:ProjectService,private indexerService:IndexerService
  ) {
  }

  readUrlParams(callback) {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.activatedRoute.parent.params.subscribe(parentRouteParams => {
        callback(parentRouteParams, queryParams);
      });
    });
  }

  // Forming search field for mpm-indexer input
  getSearchField(type, field_id, operator_id, operator_name, value?, field_operator?) {

        let searchField = {
            "type": type,
            "field_id": field_id,
            "relational_operator_id": operator_id,
            "relational_operator_name": operator_name,
            "value": value,
            "relational_operator": field_operator
        }
        return searchField;
  }

  getProjectDetailsById(projectId): Observable<any> {
    this.loaderService.show();
    let searchConditionList = [
        this.getSearchField("string", "CONTENT_TYPE", "MPM.OPERATOR.CHAR.IS", "is", "MPM_PROJECT"),
        this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", projectId, "AND"),
    ];

    const parameters: SearchRequest = {
        search_config_id: null,
        keyword: "",
        search_condition_list: {
            search_condition: searchConditionList
        },
        facet_condition_list: {
            facet_condition: []
        },
        sorting_list: {
            sort: []
        },
        cursor: {
            page_index: 0,
            page_size: 1
        }
    };
    return new Observable(observer => {
        this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
            if (response.data.length > 0) {
                //response.data[0];
                this.requestId = response.data[0]?.PR_REQUEST_ITEM_ID?.split('.')[1];
                this.isGroupFilter = true
            }
            this.loaderService.hide();
            observer.next();
            observer.complete();
        }, () => {
            this.loaderService.hide();
            observer.error();
        });
    });
  }


/*   getProjectDetails(): Observable<any> {
    this.loaderService.show();
    return new Observable(observer => {
        this.projectService.getProjectById(this.projectId)
            .subscribe(projectObj => {
              this.loaderService.hide();
                if (projectObj) {
                    //this.requestId = projectObj;
                    observer.next(true);
                    observer.complete();
                }
            },(error) => {
              this.loaderService.hide();
              this.notificationService.error('Somthing went wrong while fetching project details');
              observer.next(error);
            });
    });
  } */

  getRequestIdFromProjectDetails() {
    //this.requestId = '005056AACEB9A1EBA6DD27D3623FE821.65623';
    this.getProjectDetailsById(this.projectId).subscribe();
  }

  ngOnInit(): void {
    this.projectId = null;
    this.readUrlParams((routeParams, queryParams) => {
      if (queryParams && routeParams) {
        this.projectId = routeParams.projectId;
        this.getRequestIdFromProjectDetails();
      }
    });
  }


}
