import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UtilService, OtmmMetadataService, FieldConfigService, ViewConfigService, SharingService, ProjectBulkCountService, ProjectService, NotificationService, IndexerDataTypes } from 'mpm-library';
import { MPMProjectCardComponent } from 'src/app/core/mpm-lib/pm-dashboard/project-card/project-card.component';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { RoutingConstants } from 'src/app/core/mpm/routingConstants';

@Component({
  selector: 'app-npd-project-card',
  templateUrl: './npd-project-card.component.html',
  styleUrls: ['./npd-project-card.component.scss']
})
export class NpdProjectCardComponent extends MPMProjectCardComponent {

      constructor(
        public dialog: MatDialog,
        public utilService: UtilService,
        public otmmMetadataService: OtmmMetadataService,
        public fieldConfigService: FieldConfigService,
        public viewConfigService: ViewConfigService,
        public sharingService: SharingService,
        public projectBulkCountService: ProjectBulkCountService,
        public router: Router,
        public projectService: ProjectService,
        public notificationService: NotificationService,
        public monsterUtilService:MonsterUtilService

    ) {
        super(dialog,utilService,otmmMetadataService,fieldConfigService,viewConfigService,sharingService,projectBulkCountService,
          router,projectService,notificationService);
      }
      
      routingConstants = RoutingConstants;

  checkDateType(displayColumn) {
    return displayColumn?.DATA_TYPE?.toLowerCase() === IndexerDataTypes.DATETIME?.toLowerCase()
  }
  
  getWeek(column, dateValue) {
    //let date = this.monsterUtilService.getFieldValueByDisplayColumn(column,row);
    if (dateValue && dateValue != 'NA' && column?.DATA_TYPE.toLowerCase() === IndexerDataTypes.DATETIME.toLowerCase()) {
        return this.monsterUtilService.calculateWeek(dateValue);
    }
  }

  getWeekValue(data,columnName) {
    if (data?.[columnName] && data?.[columnName]!='NA' ) {
      return this.monsterUtilService.calculateWeek(data[columnName]);
    }
  }
  
  // openProjectDetails(projectDetails){
  //   this.router.navigate([this.routingConstants.projectViewBaseRoute +projectDetails.ID], {});
  // }
}
