import { Component, Renderer2 } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
// import { RouteService } from 'mpm-library/lib/shared/services/route.service';
import {
  ProjectService,
  TaskService,
  UtilService,
  NotificationService,
  LoaderService,
  FormatToLocalePipe,
  OtmmMetadataService,
  EntityAppDefService,
  FieldConfigService,
  SharingService,
  ProjectBulkCountService,
  IndexerService,
  ViewConfigService,
  IndexerDataTypes,
  GetPropertyPipe,
  Sort,
} from 'mpm-library';
import { MPMProjectListComponent } from 'src/app/core/mpm-lib/pm-dashboard/project-list/project-list.component';
import { FilterConfigService } from '../../../filter-configs/filter-config.service';
import { EntityService } from '../../../services/entity.service';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { NpdTaskService } from '../../../npd-task-view/services/npd-task.service';
import { SPField } from '../../../request-view/constants/BusinessConstants';
import { RoutingConstants } from 'src/app/core/mpm/routingConstants';
import { WeekPickerService } from '../../../services/week-picker.service';
import { forkJoin } from 'rxjs';
import { NpdProjectService } from '../../services/npd-project.service';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-npd-project-list',
  templateUrl: './npd-project-list.component.html',
  styleUrls: ['./npd-project-list.component.scss'],
})
export class NpdProjectListComponent extends MPMProjectListComponent {
  backgroundCondition: string;
  constructor(
    public projectService: ProjectService,
    public taskService: TaskService,
    public utilService: UtilService,
    public notificationService: NotificationService,
    public loaderService: LoaderService,
    public formatToLocalePipe: FormatToLocalePipe,
    public otmmMetadataService: OtmmMetadataService,
    public entityAppDefService: EntityAppDefService,
    public fieldConfigService: FieldConfigService,
    public sharingService: SharingService,
    public projectBulkCountService: ProjectBulkCountService,
    public router: Router,
    public renderer: Renderer2,
    public dialog: MatDialog,
    public filterConfigService: FilterConfigService,
    public monsterUtilService: MonsterUtilService,
    public getPropertyPipe: GetPropertyPipe,
    public weekPickerService: WeekPickerService,
    private npdProjectService: NpdProjectService,
    private monsterConfigApiService: MonsterConfigApiService // private mpmRouteService: RouteService,
  ) {
    super(
      dialog,
      projectService,
      taskService,
      utilService,
      notificationService,
      loaderService,
      formatToLocalePipe,
      otmmMetadataService,
      entityAppDefService,
      fieldConfigService,
      sharingService,
      projectBulkCountService,
      router,
      renderer,
      getPropertyPipe
    );
    this.getAllFiltersForOverview();
  }
  routingConstants = RoutingConstants;

  checkDateType(displayColumn) {
    return (
      displayColumn?.DATA_TYPE?.toLowerCase() ===
      IndexerDataTypes.DATETIME?.toLowerCase()
    );
  }

  getWeek(column, row) {
    let date = this.monsterUtilService.getFieldValueByDisplayColumn(
      column,
      row
    );
    if (SPField.includes(column.MAPPER_NAME) && date) {
      let usDate = new Date(date);
      if (!isNaN(usDate.getTime())) {
        let localDate = usDate.toLocaleDateString('en-GB', {
          day: '2-digit',
          month: '2-digit',
          year: 'numeric',
        });
        return localDate;
      }
    }

    // if (SPField.includes(column.MAPPER_NAME) && date) {
    //   let usDate = new Date(date);
    //   if (!isNaN(usDate.getTime())) {
    //     let indianDate = usDate.toLocaleString('en-US', {
    //       timeZone: 'Asia/Kolkata',
    //       year: 'numeric', month: '2-digit', day: '2-digit',
    //       hour: '2-digit', minute: '2-digit', second: '2-digit'
    //     });
    //     return indianDate;
    //   }
    // }
    else if (
      date &&
      column?.DATA_TYPE.toLowerCase() ===
        IndexerDataTypes.DATETIME.toLowerCase()
    ) {
      return this.monsterUtilService.calculateWeek(date);
    }
  }
  checkProject(projectData, event) {
    if (event.checked) {
      projectData.selected = true;
      this.selectedProjects.select(projectData);
      this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
    } else {
      this.selectAllProjectChecked = false;
      this.selectedProjects.deselect(projectData);
      this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
    }
    let masterCheck = this.projectDataDataSource.data.every(
      (selectedData) => selectedData.selected === true
    );
    if (masterCheck) {
      this.selectAllProjectChecked = true;
    }
  }
  checkAllProjects() {
    let flag = false;
    this.projectDataDataSource.data.forEach((row) => {
      if (row.NPD_PROJECT_IS_BULK_EDIT === true) {
        flag = true;
      } else {
        row.selected = true;
        this.selectedProjects.select(row);
      }
    });
    if (flag) {
      this.notificationService.warn(
        'Cannot select All Projects ,Project Bulk edit is in progress'
      );
    }
    this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
  }
  changeStatusColor(column, row, status) {
    return this.monsterUtilService.changeProjectStatusColor(status);
  }

  getCurrentTimelineCondition(rowData, ColumnData) {
    this.backgroundCondition = '';
    const governDeliveryWeek = rowData.NPD_PROJECT_GOVERNED_DELIVERY_DATE;
    const currentTimelineWeek = rowData.NPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK;
    if (
      currentTimelineWeek &&
      currentTimelineWeek !== 'NA' &&
      governDeliveryWeek &&
      governDeliveryWeek !== 'NA'
    ) {
      const getDateFromWeek = (weekString) => {
        const [week, year] = weekString.split('/');
        const startDate = new Date(year, 0, 1);
        startDate.setDate(startDate.getDate() + (week - 1) * 7);
        return new Date(startDate.getTime() + 6 * 24 * 60 * 60 * 1000);
      };

      const endDateGovernDeliveryWeek = getDateFromWeek(governDeliveryWeek);
      const endDateCurrentTimelineWeek = getDateFromWeek(currentTimelineWeek);

      const endMilliGovernDeliveryWeek =
        endDateGovernDeliveryWeek.getTime() -
        endDateGovernDeliveryWeek.getTimezoneOffset() * 60000;
      const endMilliCurrentTimelineWeek =
        endDateCurrentTimelineWeek.getTime() -
        endDateCurrentTimelineWeek.getTimezoneOffset() * 60000;

      const diffMillis =
        endMilliGovernDeliveryWeek - endMilliCurrentTimelineWeek;

      // Convert the difference in milliseconds to weeks
      let diffWeeks = Math.floor(diffMillis / (7 * 24 * 60 * 60 * 1000));

      if (diffWeeks < 0) {
        this.backgroundCondition = 'red';
      } else if (diffWeeks > 6) {
        this.backgroundCondition = 'green';
      } else if (0 <= diffWeeks && diffWeeks <= 6) {
        this.backgroundCondition = 'amber';
      }
    }
    return this.backgroundCondition;
  }

  // weekDifference(governDeliveryEndDate,currentTimelineEndDate) {
  //     const totalSecDiff= (governDeliveryEndDate-currentTimelineEndDate)/1000
  //     const daysDiff= Math.floor (totalSecDiff / (60 * 60 * 24));
  //     return daysDiff/7
  // }

  // openProjectDetails(project, mapperName, enableCampaign,isCampaignManager,IsPMView) {
  //     // this.router.navigate(['/apps/mpm/project/' +project.ID], {});
  //   this.router.navigate([this.routingConstants.projectViewBaseRoute +project.ID], {});
  //  }

  /**
   * getAllFiltersForOverview method is to call Api's  related to overview Dashboard as part intial load performance improvement
   */

  tableData(): MatTableDataSource<any> {
    let result = new MatTableDataSource(this.projectDataDataSource.data);
    result.sortingDataAccessor = (item, header) => {
      if (this.monsterUtilService.isDateStringMatches(header)) {
        const weekFormat = item[header];
        if (
          weekFormat &&
          weekFormat != undefined &&
          weekFormat != '' &&
          weekFormat != 'NA'
        ) {
          const [week, year] = weekFormat.split('/').map(Number);
          return new Date(year, 0, (week - 1) * 7); // Convert to a date for proper sorting
        } else {
          const [week, year] = [0, 0];
          return new Date(year, 0, (week - 1) * 7); // Convert to a date for proper sorting
        }
      } else {
        return item[header];
      }
    };
    result.sort = this.sort;
    return result;
  }

  public getAllFiltersForOverview() {
    forkJoin([
      this.npdProjectService.getAllE2EPMUsers(),
      this.npdProjectService.getAllCorpPMUsers(),
      this.npdProjectService.getAllOPSPMUsers(),
      this.npdProjectService.getAllRTMUsers(),
      this.npdProjectService.getAllOpsPlannerUsers(),
      this.npdProjectService.getAllGFGUsers(),
      this.npdProjectService.getAllProjectSpecialistUsers(),
      this.npdProjectService.getAllRegulatoryLeadUsers(),
      this.npdProjectService.getAllCommercialLeadUsers(),
      this.monsterConfigApiService.getAllProjectTypes(),
      this.npdProjectService.getAllSecondaryPackLeadTimes(),
      this.npdProjectService.getAllPrintSuppliers(),
      this.npdProjectService.getAllTastingRequirements(),
      this.npdProjectService.getAllTrialProtocolWrittenByUsers(),
      this.npdProjectService.getAllTrialSupervisionNPD(),
      this.npdProjectService.getAllTrialSupervisionQuality(),
      this.npdProjectService.getAllAuditUsers(),
      this.npdProjectService.getAllSecondaryArtworkSpecialists(),
      this.npdProjectService.getAllCanCompanies(),
      this.monsterConfigApiService.getAllReportingProjectType(),
      this.npdProjectService.getAllWhoWillCompleteUsers(),
      this.monsterConfigApiService.getAllProgramTags(),
    ]).subscribe((res) => {});
  }
}
