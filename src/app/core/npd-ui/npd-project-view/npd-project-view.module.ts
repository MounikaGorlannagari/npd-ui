import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PmProgrammeManagerReportingComponent } from './pm-programme-manager-reporting/pm-programme-manager-reporting.component';
import { MaterialModule } from 'src/app/material.module';
import { PmSecondaryPackagingComponent } from './pm-secondary-packaging/pm-secondary-packaging.component';
import { PmProjectClassificationComponent } from './pm-project-classification/pm-project-classification.component';
import { PmCanSupplierComponent } from './pm-can-supplier/pm-can-supplier.component';
import { RequestViewModule } from '../request-view/request-view.module';
import { NpdAssetsComponent } from './npd-assets/npd-assets.component';
import { NpdManagerDashboardComponent } from './npd-project-management/npd-manager-dashboard/npd-manager-dashboard.component';
import { PmArtworkPrimaryComponent } from './pm-artwork-primary/pm-artwork-primary.component';
import { BulkEditProjectComponent } from './bulk-edit-project/bulk-edit-project/bulk-edit-project.component';
import { BulkEditProjectFieldComponent } from './bulk-edit-project/bulk-edit-project-field/bulk-edit-project-field.component';
import { BulkPmReportingComponent } from './bulk-pm-reporting/bulk-pm-reporting.component';
import { BulkFinalClassificationComponent } from './bulk-final-classification/bulk-final-classification.component';
import { PmArtworkStatusOverviewComponent } from './pm-artwork-status-overview/pm-artwork-status-overview.component';



@NgModule({
  declarations: [
    PmProgrammeManagerReportingComponent,
    PmSecondaryPackagingComponent,
    PmProjectClassificationComponent,
    PmCanSupplierComponent,
    PmArtworkPrimaryComponent,
    BulkEditProjectComponent,
    BulkEditProjectFieldComponent,
    BulkPmReportingComponent,
    BulkFinalClassificationComponent,
    PmArtworkStatusOverviewComponent,
  ],
  imports: [CommonModule, MaterialModule, RequestViewModule],
  exports: [
    PmProgrammeManagerReportingComponent,
    PmSecondaryPackagingComponent,
    PmProjectClassificationComponent,
    PmCanSupplierComponent,
    PmArtworkPrimaryComponent,
    BulkEditProjectComponent,
    BulkEditProjectFieldComponent,
    PmArtworkStatusOverviewComponent,
  ],
})
export class NpdProjectViewModule {}
