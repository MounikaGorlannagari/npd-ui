import { I } from '@angular/cdk/keycodes';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { WeekPickerComponent } from '../../request-view/generic-components/week-picker/week-picker.component';
import { OverViewConstants } from '../constants/projectOverviewConstants';

@Component({
  selector: 'app-npd-custom-field',
  templateUrl: './npd-custom-field.component.html',
  styleUrls: ['./npd-custom-field.component.scss'],
})
export class NpdCustomFieldComponent implements OnInit {
  @Input() index;
  @Input() metadataFieldFormControl;
  @Input() config
  @Input() fieldAccess
  @Output() valueChanges = new EventEmitter<any>();
  @Input() sectionAccess;
  @Output() closeAllWeekPicker = new EventEmitter<any>();
  @Input() projectStatus
  @ViewChild(WeekPickerComponent, { static: false })
  weekPickerComponent: WeekPickerComponent;
  label: string;
  datatype: string;
  fieldType: string;
  requiredField: boolean;
  keyRestriction = {
    NUMBER: [69, 187, 188, 107],
  };
  flag :number=0
  readonly radioButtonsList = OverViewConstants.booleanConfig;
  fromDate = {
    startDate: null,
  };
  toDate = {
    startDate: null,
  };
  weekYear = {
    startDate: null,
  };
  selectValue: string;
  selectedValue : any

  @Input() show = {
    startDate: false,
  };
  showEllipsesValue;
  selectArray : any = []
  isProgramTagEditable: any;
  currentTimeline: any;
  governDeliveryDate: any;
  backgroundColor: any;
  configs: any;
  // @ViewChild('date1') date1: ElementRef;

  constructor(public adapter: DateAdapter<any>) { }

  getBool(date) {
    this.show[date] = !this.show?.[date];
    if (this.index) {
      this.closeAllWeekPicker.emit({
        index: this.index,
        flag: this.show[date],
      });
    }
  }

  getAllBool() {
    Object.keys(this.show).map((ele) => {
      this.show[ele] = false;
    });
  }

  openWeekPicker(date) {
    this.getAllBool();
    this.show[date] = !this.show[date];
  }

  closeWeekPicker(date){
    // this.show.startDate = false;
    this.show[date] = !this.show[date];
  }

  valueChange(event) {
     
     this.selectedValue = this.metadataFieldFormControl.value 
     this.selectedValue = this.selectedValue == 'NA'?'':this.metadataFieldFormControl.value;
    if(event.value == null || event.value._i == " "){
      this.valueChanges.next(this.metadataFieldFormControl);
    }
    this.valueChanges.next(this.metadataFieldFormControl);
  }

  onValueSelected(event) {
    this.metadataFieldFormControl.setValue(event);
    this.valueChange(event.value);
  }

  getStopPropogation(event, date) {
    if (this.show[date]) {
      return event.stopPropagation();
    }
  }

  changeWeek(weekFormat, date?) {
    this.weekYear[date] = weekFormat;
    this.metadataFieldFormControl.setValue(weekFormat);
    this.valueChange(weekFormat);
    //this.taskForm.markAsDirty();
    //[this.fromDate[date], this.toDate[date] ] = this.getWeekFormat(weekFormat);
    this.show[date] = !this.show[date];
  }

  getFormControlValue(value) {
    return value?.toString();
  }

  /*   getFormControl(value) {
    console.log(value);
    return '';
  } */

  refreshField() {
    this.configs=this.config
    if (this.metadataFieldFormControl?.fieldset?.dataType == 'DATETIME') {
      // this.currentTimeline = 
      if (navigator.language !== undefined) {
        this.adapter.setLocale(navigator.language);
      }
    }
    if (this.metadataFieldFormControl) {
      
      this.label = this.metadataFieldFormControl.name;
      this.datatype = this.metadataFieldFormControl.fieldset.dataType;
      this.fieldType = this.metadataFieldFormControl.fieldset.fieldType;
      this.selectedValue = this.metadataFieldFormControl.value;
      this.selectedValue = this.selectedValue == 'NA' || this.selectedValue == undefined?'':this.metadataFieldFormControl.value;

      // if(this.label == "Governed Delivery Date (DP WH)") this.governDeliveryDate = this.selectedValue
      // if(this.label == "Current Timeline (DW)") this.currentTimeline = this.selectedValue

      // if(this.currentTimeline && (this.governDeliveryDate && this.governDeliveryDate !=='NA')){
      //   console.log(this.currentTimeline);
      //   console.log(this.governDeliveryDate);
        
      //   if(this.governDeliveryDate!==this.currentTimeline){
      //     this.backgroundColor='red'
      //   }
      // }

      if(this.metadataFieldFormControl.fieldset.isUser == true){

        this.metadataFieldFormControl.fieldset.values = this.metadataFieldFormControl.fieldset?.values?.filter(value=>{
          return value.isActive == 'true'
        })
      }
     
      

      //this.metadataFieldFormControl.fieldset.dataType !== 'NUMBER' ? this.metadataFieldFormControl.fieldset.edit_type : this.metadataFieldFormControl.fieldset.edit_type === 'COMBO' ? this.metadataFieldFormControl.fieldset.edit_type : this.metadataFieldFormControl.fieldset.dataType;
      if (
        (this.metadataFieldFormControl.errors &&
          this.metadataFieldFormControl.errors.required) ||
        this.metadataFieldFormControl.fieldset.required
      ) {
        this.requiredField = true;
      } else {
        this.requiredField = false;
      }
    }
  }

  restrictKeysOnType(event, datatype) {
    if (
      this.keyRestriction[datatype] &&
      this.keyRestriction[datatype].includes(event.keyCode)
    ) {
      event.preventDefault();
    }
  }

  fetchBooleanValue(value) {
    if (value == true) {
      return 'Yes';
    } else if (value == false) {
      return 'No';
    } else {
      return 'NA';
    }
  }

  showEllipses() {
    if (!this.showEllipsesValue) {
      this.showEllipsesValue = {};
    }
    this.showEllipsesValue = !this.showEllipsesValue;
  }
  updateDateValue(date) {
    let week = date.split('/')[0];
    let year = date.split('/')[1];
    if(!date || date === " "){
      this.metadataFieldFormControl.setValue('');
      this.valueChange('');
      this.flag=0;
      return ;
    }

    if (this.weekPickerComponent) {
      this.weekPickerComponent.calculateDate(week, year);
    }
    this.flag=0;
  }
  onClick(){
    if(this.metadataFieldFormControl.fieldset.editable == false
      ? true
      : this.sectionAccess){
        return
      }
      if(this.flag==0){
        this.flag+=1;
      }
      else{
        this.flag=0;
      }
    
  }

  compareFn(o1: any, o2: any) {
    if(o1.actualValue == o2.actualValue)
    return true;
    else return false
  }

  numericOnly(event){
     let numericPattern = /^[0-9]*$/;
     return numericPattern.test(event.key);
  }

  ngOnInit() {    
    this.flag=0;
    this.refreshField();
  }
}

/* getWeekFormat(weekFormat){
    if(weekFormat && weekFormat!==undefined){
        let week = weekFormat.split("/")[0];
        let year = weekFormat.split("/")[1];
  
        let weekdates:NgbDate[] = this.calculateDateNgb(week,year);
        return [weekdates[0], weekdates[1]]
      }
  }

  calculateDateNgb(week: number, year: number):NgbDate[] {

    let ngbFromDate,ngbToDate;
    console.log("week:"+week);
    console.log("year:"+year);
    const firstDay = new Date(year + "/1/4"); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(`${selectDate.year.toString()}-${selectDate.month.toString().padStart(2,'0')}-${selectDate.day.toString().padStart(2,'0')}`);
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate,ngbToDate];

  } */
