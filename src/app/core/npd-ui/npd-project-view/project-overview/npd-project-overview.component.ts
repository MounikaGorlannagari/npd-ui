import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
} from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  ApplicationConfigConstants,
  AppService,
  BulkCommentsComponent,
  CategoryLevel,
  CategoryService,
  DataTypeConstants,
  DateValidators,
  EntityAppDefService,
  FieldConfigService,
  LoaderService,
  MPMFieldConstants,
  MPM_LEVELS,
  MPM_ROLES,
  NotificationService,
  OtmmMetadataService,
  OTMMMPMDataTypes,
  OTMMService,
  Person,
  Priority,
  ProjectConstant,
  ProjectFromTemplateService,
  ProjectService,
  ProjectTypes,
  ProjectUtilService,
  Roles,
  SessionStorageConstants,
  SharingService,
  StatusLevels,
  StatusService,
  StatusTypes,
  Team,
  UtilService,
} from 'mpm-library';
import { OverViewConstants } from '../constants/projectOverviewConstants';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProjectTaskEditDialogComponent } from '../../request-view/project-task-edit-dialog/project-task-edit-dialog.component';
import { OverViewConfig } from 'mpm-library/lib/project/project-overview/OverviewConfig';
import { forkJoin, Observable } from 'rxjs';
import { DateAdapter } from '@angular/material/core';
import * as acronui from 'mpm-library';
// import { ProjectUpdateObj } from 'mpm-library/lib/project/project-overview/project.update';
import { NpdProjectService } from '../services/npd-project.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { newArray } from '@angular/compiler/src/util';
import { ProjectFromTemplateObj } from 'mpm-library/lib/project/project-from-templates/project.from.template';
import { NpdProjectUpdateObj } from '../constants/NpdProjectUpdate';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { FilterConfigService } from '../../filter-configs/filter-config.service';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { RequestConfig } from '../../request-view/constants/RequestConfig';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { I, T } from '@angular/cdk/keycodes';
import { PmCanSupplierComponent } from '../pm-can-supplier/pm-can-supplier.component';
import { PmProjectClassificationComponent } from '../pm-project-classification/pm-project-classification.component';
import { RequestService } from '../../services/request.service';
import { MatAccordion, MatExpansionPanel } from '@angular/material/expansion';
import { PmSecondaryPackagingComponent } from '../pm-secondary-packaging/pm-secondary-packaging.component';
import { MONSTER_ROLES } from '../../filter-configs/role.config';
import { EntityService } from '../../services/entity.service';
import { ProjectUpdateObj } from 'mpm-library/lib/project/project-overview/project.update';
// import { EntityListConstant } from '../../request-view/constants/EntityListConstant';

@Component({
  selector: 'npd-app-project-overview',
  templateUrl: './npd-project-overview.component.html',
  styleUrls: ['./npd-project-overview.component.scss'],
})
export class NpdProjectOverviewComponent implements OnInit {
  public leadMarket = '';
  public bussinessUnit = '';
  overViewConstants = JSON.parse(JSON.stringify(OverViewConstants));

  //allListConfig = {};
  projectOverviewForm: FormGroup;
  overViewConfig: OverViewConfig;
  projectOwnerDetails: Person;
  selectedStatus;
  disableStatusOptions;
  selectedPriority;
  disableFields;
  categoryLevel;
  disableEndDate;
  enableRestartProject;
  enableProjectRestartConfig;
  disableEditOptions: boolean;
  disableOptions: boolean;
  disableProjectSave = false;
  cancelComment;
  oldProjectMetadata: ProjectUpdateObj;
  enableCustomWorkflow;
  nameStringPattern;
  selectedTeam;
  ownerFilterOptions = [];
  reasons;
  panelHTML: HTMLElement;
  customMetadataFields;
  customFieldsGroup;
  projectRequestConfigDetails;
  requestData;
  selectedTasks;
  isAuditSelected = false;
  isProjectDataSapAccess = false;
  weePickers: any = [[]];
  isNoOfLinkedMarketFieldAccess = false;
  isProgramTagFieldAccess = false;
  loggedInUser;
  isBulkEdit;
  @Input() projectId;
  @Output() loadingHandler = new EventEmitter<any>();
  @Output() closeCallbackHandler = new EventEmitter<any>();
  @Output() saveCallBackHandler = new EventEmitter<any>();
  @Output() notificationHandler = new EventEmitter<any>();

  appConfig: any;
  enableWorkWeek: any;
  selectedEndDate;
  isReadOnly;
  oldStatusInfo;
  requestOverviewEllipses = [];
  projectCoreData = [];
  allProjectFieldsConfig = [];
  allRequestFieldsConfig = [];

  allTasksNotCreated;
  isE2EPM;
  isOPSPM;
  isPM;
  isRegulatory;
  isSecondaryPackaging;
  isTechnicalQual;
  isFinalStatus: boolean = false;

  request = {
    requestId: '',
    requestType: '',
    requestItemId: '',
    requestBusinessId: '',
  };
  npdProjectEntityItemId;

  requestFieldHandler: RequestConfig;
  allListConfig = {
    markets: [],
    governanceApproaches: [],
    casePackSizes: [],
    reportingProjectType: [],
    e2EPMUsers: [],
    corpPMUsers: [],
    opsPMUsers: [],
    rtmUsers: [],
    opsPlannerUsers: [],
    gfgUsers: [],
    projectSpecialistUsers: [],
    regulatoryLeadUsers: [],
    commercialLeadUsers: [],
    projectTypes: [],
    whoWillCompleteUsers: [],
    secondaryArtworkSpecialistUsers: [],
    programTags: [],
    // rmRoleAllocation:[]
    fpmsUsers: [],
  };
  backgroundCondition: any;

  show = {
    endDate: false,
  };
  fromDate = {
    endDate: null,
  };
  toDate = {
    endDate: null,
  };
  weekYear = {
    endDate: null,
  };
  disable = {
    endDate: true,
  };

  govConfig;
  governanceMilestone;
  secondaryPackageConfig;
  artworkPrimaryConfig;
  canSupplierConfig;
  programManagerConfig;
  projClassificationConfig;
  projectStatuss;
  isAdmin: boolean = false;
  isGovernanceOverDue: boolean = false;

  @ViewChild('opsPMPanel') opsPMPanel; //: QueryList<MatExpansionPanel>
  @ViewChild('e2ePMPanel') e2ePMPanel;
  @ViewChild('technicalPanel') technicalPanel;
  @ViewChild('secondaryPackaging') secondaryPackaging;
  @ViewChild('regulatory') regulatory;
  @ViewChild('projCoreData') projCoreData;
  @ViewChild('pmPanel') pmPanel;
  @ViewChild('projClassificationPanel') projClassificationPanel;
  @ViewChild('reqOverviewPanel') reqOverviewPanel;

  @ViewChild('canSupplierComponent')
  canSupplierComponent: PmCanSupplierComponent;

  PROJECT_DETAILS_METHOD_NS =
    'http://schemas/AcheronMPMCore/Project/operations';
  GET_PROJECT_DETAILS_WS_METHOD_NAME = 'GetProjectByID';
  TEAM_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/teams/bpm/1.0';
  GET_TEAM_WS_METHOD_NAME = 'GetTeamsForUser';
  governDeliveryDate: any;
  currentTimeline: any;
  artworkStatusOverviewConfig: any;
  // classificationNumber: any;

  constructor(
    private formBuilder: FormBuilder,
    private loaderService: LoaderService,
    public dialog: MatDialog,
    private sharingService: SharingService,
    private statusService: StatusService,
    public adapter: DateAdapter<any>,
    public utilService: UtilService,
    private projectUtilService: ProjectUtilService,
    private appService: AppService,
    private entityAppDefService: EntityAppDefService,
    private notificationService: NotificationService,
    private projectService: ProjectService,
    private npdProjectService: NpdProjectService,
    private monsterUtilService: MonsterUtilService,
    private categoryService: CategoryService,
    private otmmMetadataService: OtmmMetadataService,
    private otmmService: OTMMService,
    private filterConfigService: FilterConfigService,
    private fieldConfigService: FieldConfigService,
    private projectFromTemplateService: ProjectFromTemplateService,
    private monsterConfigApiService: MonsterConfigApiService,
    private requestService: RequestService
  ) {
    //this.loaderService.hide();
    this.loadingHandler.next(false);
  }

  get formControls() {
    return this.projectOverviewForm?.controls;
  }

  npdCustomFieldsGroup;
  newNpdCustomFieldsGroup;
  npdcustomFields = [];
  oldNpdCustomProjectdata;
  newNpdCustomProjectdata;
  allfieldsetGroups = [];

  sectionAccess = {
    overviewSection: false,
    classificationSection: false,
    programManagerSection: false,
    projCoreDataSection: false,
    e2ePMSection: false,
    regulatorySection: false,
    secondaryPkgSection: false,
    technicalSection: false,
    opsPMSection: false,
  };
  isFGProject;

  @ViewChild('projectClassificationComponent')
  projectClassificationComponent: PmProjectClassificationComponent;
  @ViewChild('secondaryPackagingComponent')
  secondaryPackagingComponent: PmSecondaryPackagingComponent;

  addTasks() {
    if (!(this.selectedTasks?.length > 0)) {
      this.notificationService.warn('Please select a task to create');
      return;
    }
    let tasks = [];
    let taskObj = [];
    this.selectedTasks.forEach((element) => {
      tasks.push({
        TaskName: element.TaskName,
      });
    });
    //taskObj.push(tasks)
    this.loaderService.show();
    this.npdProjectService
      .initiateTasksCreationWorkflow(tasks, this.projectId, '')
      .subscribe(
        (response) => {
          this.notificationService.success('Task(s) creation is initiated');
          this.selectedTasks = [];
          setTimeout(() => {
            this.npdProjectService
              .getTasksNotCreatedDetails(this.projectId)
              .subscribe(
                (response) => {
                  this.allTasksNotCreated = response;
                  this.loaderService.hide();
                },
                (error) => {
                  this.loaderService.hide();
                }
              );
          }, 4000);
        },
        (error) => {
          this.loaderService.hide();
          this.notificationService.error(
            'Something went wrong while task(s) creation'
          );
        }
      );
  }

  validateProject() {
    const validation = {
      /*  projectCoreComponent: true,
              marketScopeComponent: true,
              managerFieldComponent : true,
              requestRationaleComponent : true,
              projectClassificationComponent : true,*/
      secondaryPackagingComponent: true,
      projectClassificationComponentAutoPopulate: false,
      projectClassificationComponentAuto: false,
    };
    // if (this.projectClassificationComponent?.validateEarlyProjectClassificationFields()) {
    //     // this.notificationService.error('Please check if classifications are configured');
    //     validation.projectClassificationComponentAuto = true;
    // } else {
    //     validation.projectClassificationComponentAuto = false;
    // }
    // if (this.projectClassificationComponent?.validateProjClassificationAutoPopulateFields()) {
    //     // this.notificationService.error('Please check if the selected Country of Sales & Country of origin values are configured.');
    //     validation.projectClassificationComponentAutoPopulate = true;
    // } else {
    //     validation.projectClassificationComponentAutoPopulate = false;
    // }
    if (this.secondaryPackagingComponent?.onValidate()) {
      this.notificationService.error('Please check all the fields.');
      validation.secondaryPackagingComponent = true;
    } else {
      validation.secondaryPackagingComponent = false;
    }
    //validation.deliverablesVsThresholdsComponent = false;//remove this after complete code
    //validation.marketScopeComponent = false;//remove this after complete code
    const flag = Object.keys(validation).every(
      (ele) => validation[ele] === false
    );
    // Validate project classification data
    if (flag) {
      return true;
    } else {
      return false;
    }
  }

  getCustomFormValue(value, field) {
    let formValue;
    if (field?.dataType === 'BOOLEAN' || field?.fieldType === 'BOOLEAN') {
      formValue =
        value || value != 'NA'
          ? value == 'true' || value == true
            ? true
            : value == 'false' || value == false
              ? false
              : ''
          : '';
      return formValue;
    }
    formValue = typeof value !== 'undefined' || value != 'NA' ? value : '';
    if (field?.Mapper_Name === 'NPD_PROJECT_NO_OF_WEEKS_INCREASED') {
    }
    return formValue;
  }
  getNpdCustomConfig(customGroup, groupName, disable) {
    const fieldsetGroup = [];
    const fieldGroups = [];
    for (let fieldGroupItem of customGroup) {
      const formControlArray = [];

      for (const field of fieldGroupItem.groupArray) {
        this.npdcustomFields.push(field);
        const validators = [];
        const value = this.getFieldConfig(field?.Mapper_Name, field?.isRequest);
        let formControlObj;

        if (
          groupName == 'npdCustomProjectOverviewFieldGroup' &&
          field.Mapper_Name == 'NPD_PROJECT_PROGRAM_TAG' &&
          this.isProgramTagFieldAccess
        ) {
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        } else if (
          (groupName == 'npdCustomProjectCoreDataFieldGroup' &&
            !this.isFinalStatus &&
            this.isAdmin) ||
          (fieldGroupItem.groupName == 'Project Team' &&
            field.Mapper_Name == 'NPD_PROJECT_CORP_PM_NAME' &&
            this.filterConfigService.isCorpProjectLeader())
        ) {
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        } else if (
          !this.isFinalStatus &&
          groupName == 'npdCustomProjectCoreDataFieldGroup' &&
          fieldGroupItem.groupName == 'Project Team' &&
          field.Mapper_Name == 'NPD_PROJECT_PR_SPECIALIST_NAME' &&
          (this.loggedInUser ==
            this.projectRequestConfigDetails?.NPD_PROJECT_CORP_PM_NAME ||
            this.filterConfigService.isCorpProjectLeader() ||
            this.isAdmin)
        ) {
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        } else if (
          !this.isFinalStatus &&
          groupName == 'npdCustomProjectCoreDataFieldGroup' &&
          fieldGroupItem.groupName == 'Project Team' &&
          field.Mapper_Name == 'NPD_PROJECT_SECONDARY_AW_SPECIALIST_NAME' &&
          (this.loggedInUser ==
            this.projectRequestConfigDetails
              ?.NPD_PROJECT_SECONDARY_AW_SPECIALIST_NAME ||
            this.isAdmin)
        ) {
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        } else if (
          !this.isFinalStatus &&
          groupName == 'npdCustomProjectCoreDataFieldGroup' &&
          fieldGroupItem.groupName == 'Project Core Data - SAP' &&
          this.isProjectDataSapAccess
        ) {
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        }
        //    else if( !this.isFinalStatus &&  (groupName == 'npdCustomProjectCoreDataFieldGroup' && fieldGroupItem.groupName == 'Project Team' && field.Mapper_Name == 'NPD_PROJECT_OPS_PLANNER_NAME' && (this.loggedInUser == this.projectRequestConfigDetails?.NPD_PROJECT_OPS_PLANNER_NAME || this.isAdmin )))
        //    {
        //     field.editable = true;
        //     formControlObj = new FormControl({
        //                 value: this.getCustomFormValue(value,field),
        //                 disabled: false
        //             });
        //    }
        else if (
          !this.isFinalStatus &&
          groupName == 'npdCustomProjectCoreDataFieldGroup' &&
          fieldGroupItem.groupName == 'Project Team' &&
          field.Mapper_Name == 'NPD_PROJECT_REGULATORY_LEAD_NAME' &&
          (this.loggedInUser ==
            this.projectRequestConfigDetails?.NPD_PROJECT_CORP_PM_NAME ||
            this.filterConfigService.isRegsTrafficManager() ||
            this.filterConfigService.isCorpProjectLeader() ||
            this.isAdmin)
        ) {
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        } else if (
          !this.isFinalStatus &&
          groupName == 'npdCustomProjectCoreDataFieldGroup' &&
          fieldGroupItem.groupName == 'Project Team' &&
          field.Mapper_Name == 'NPD_PROJECT_GFG_NAME' &&
          (this.isIncludeUser(
            this.loggedInUser,
            [this.projectRequestConfigDetails?.NPD_PROJECT_CORP_PM_NAME,
              this.projectRequestConfigDetails?.NPD_PROJECT_FPMS_NAME]
              )|| this.isAdmin)
        ) {
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        }else if (
          !this.isFinalStatus &&
          groupName == 'npdCustomProjectCoreDataFieldGroup' &&
          fieldGroupItem.groupName == 'Project Team' &&
          field.Mapper_Name == 'NPD_PROJECT_FPMS_NAME' &&
          ( this.filterConfigService.isCorpProjectLeader() || 
            this.filterConfigService.isProgramManagerRole() || 
            this.isAdmin || 
            this.isIncludeUser(
              this.loggedInUser,
              [this.projectRequestConfigDetails?.NPD_PROJECT_CORP_PM_NAME,
              this.projectRequestConfigDetails?.NPD_PROJECT_E2E_PM_NAME,
              this.projectRequestConfigDetails?.NPD_PROJECT_FPMS_NAME]) )
        ) {
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        }else if(
          groupName == 'npdCustomProjectCoreDataFieldGroup' &&
          fieldGroupItem.groupName == 'Project Core Data - SAP' &&
          field.Mapper_Name == 'NPD_PROJECT_FORMULA_HBC_NUM' && 
          (this.filterConfigService.isProgramManagerRole() || 
            this.isAdmin || 
            (this.isIncludeUser(
              this.loggedInUser,
              [this.projectRequestConfigDetails?.NPD_PROJECT_E2E_PM_NAME,
              this.projectRequestConfigDetails?.NPD_PROJECT_CORP_PM_NAME,
              this.projectRequestConfigDetails?.NPD_PROJECT_OPS_PM_NAME,
              this.projectRequestConfigDetails?.NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME,
              this.projectRequestConfigDetails?.NPD_PROJECT_PR_SPECIALIST_NAME,
              this.projectRequestConfigDetails?.NPD_PROJECT_FPMS_NAME,
              this.projectRequestConfigDetails?.NPD_PROJECT_GFG_NAME])))
        ){
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        } else if (
          !this.isFinalStatus &&
          groupName == 'npdCustomProjectCoreDataFieldGroup' &&
          fieldGroupItem.groupName == 'Project Team' &&
          field.Mapper_Name == 'NPD_PROJECT_OPS_PLANNER_NAME' &&
          (this.loggedInUser ==
            this.projectRequestConfigDetails?.NPD_PROJECT_OPS_PM_NAME ||
            this.isAdmin)
        ) {
          field.editable = true;
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            disabled: false,
          });
        } else if (
          groupName == 'npdCustomOpsPMFieldGroup' &&
          fieldGroupItem.groupName == 'OPS PM - Trial Planning' &&
          field.Mapper_Name == 'NPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER'
        ) {
          if (
            this.projectRequestConfigDetails?.PROJECT_STATUS_VALUE ==
            'Completed' ||
            this.projectRequestConfigDetails?.PROJECT_STATUS_VALUE ==
            'Cancelled' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK39_EXIST ==
            false ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK39_EXIST ==
            'false' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK39_EXIST ==
            undefined
          ) {
            field.editable = false;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: true,
            });
          } else {
            field.editable = true;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: false,
            });
          }
        } else if (
          groupName == 'npdCustomOpsPMFieldGroup' &&
          fieldGroupItem.groupName == 'OPS PM - Trial Planning' &&
          field.Mapper_Name ==
          'NPD_PROJECT_FINAL_AGREED_1ST_PROD_CONFIRMED_WITH_BOTTLER'
        ) {
          if (
            this.projectRequestConfigDetails?.PROJECT_STATUS_VALUE ==
            'Completed' ||
            this.projectRequestConfigDetails?.PROJECT_STATUS_VALUE ==
            'Cancelled' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK42_EXIST ==
            false ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK42_EXIST ==
            'false' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK42_EXIST ==
            undefined
          ) {
            field.editable = false;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: true,
            });
          } else {
            field.editable = true;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: false,
            });
          }
        } else if (
          groupName == 'npdCustomOpsPMFieldGroup' &&
          fieldGroupItem.groupName == 'OPS PM - Trial Planning' &&
          field.Mapper_Name == 'NPD_PROJECT_TRIAL_VOLUME_24_EQ_CASES'
        ) {
          if (
            this.projectRequestConfigDetails?.PROJECT_STATUS_VALUE ==
            'Completed' ||
            this.projectRequestConfigDetails?.PROJECT_STATUS_VALUE ==
            'Cancelled' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK39_EXIST ==
            false ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK39_EXIST ==
            'false' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK39_EXIST ==
            undefined
          ) {
            field.editable = false;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: true,
            });
          } else if (
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK39_COMPLETED ==
            'true' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK39_COMPLETED ==
            true
          ) {
            field.editable = false;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: true,
            });
          } else {
            field.editable = true;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: false,
            });
          }
        } else if (
          groupName == 'npdCustomOpsPMFieldGroup' &&
          fieldGroupItem.groupName == 'OPS PM - Trial Planning' &&
          field.Mapper_Name == 'NPD_PROJECT_FIRST_PRODUCTION_VOLUME_24_EQ_CASES'
        ) {
          if (
            this.projectRequestConfigDetails?.PROJECT_STATUS_VALUE ==
            'Completed' ||
            this.projectRequestConfigDetails?.PROJECT_STATUS_VALUE ==
            'Cancelled' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK42_EXIST ==
            false ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK42_EXIST ==
            'false' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK42_EXIST ==
            undefined
          ) {
            field.editable = false;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: true,
            });
          } else if (
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK42_COMPLETED ==
            'true' ||
            this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK42_COMPLETED ==
            true
          ) {
            field.editable = false;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: true,
            });
          } else {
            field.editable = true;
            formControlObj = new FormControl({
              value: this.getCustomFormValue(value, field),
              disabled: false,
            });
          }
        }else {
          formControlObj = new FormControl({
            value: this.getCustomFormValue(value, field),
            //(typeof value !== 'undefined' || value !='NA') ? value : '',
            disabled: disable ? disable : !field.editable,
            // disabled: true
            //|| ((this.projectId && this.overViewConfig.isReadOnly)))
            //|| ((this.projectId && this.overViewConfig.isReadOnly) || (this.campaignId && this.overViewConfig.isReadOnly)),
          });
        }
        if (field.required) {
          validators.push(Validators.required);
        }
        /*  if (field.edit_type === ProjectConstant.METADATA_EDIT_TYPES.SIMPLE ||
                     field.edit_type === ProjectConstant.METADATA_EDIT_TYPES.TEXTAREA) {
                     field.data_length && validators.push(Validators.maxLength(field.data_length));
                 } */

        /*  if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.DATE) {
                    validators.push(DateValidators.dateFormat);
                }

                if (field.edit_type === ProjectConstant.METADATA_EDIT_TYPES.SIMPLE ||
                    field.edit_type === ProjectConstant.METADATA_EDIT_TYPES.TEXTAREA) {
                    validators.push(Validators.maxLength(field.data_length));
                }
                if (field.data_type === ProjectConstant.METADATA_EDIT_TYPES.NUMBER) {
                    const numericeVlalue = this.getMaxMinValueForCustomMetadata(metadataField.data_length, metadataField.scale);
                    validators.push(Validators.max(numericeVlalue), Validators.min(-(numericeVlalue)));

                } */
        formControlObj.setValidators(validators);
        formControlObj.updateValueAndValidity();
        if (field.checkFGModel) {
          this.isFGProject ? (field.show = true) : (field.show = false);
        }
        if (field.checkConcModel) {
          this.isFGProject ? (field.show = false) : (field.show = true);
        }
        formControlObj['fieldset'] = field;
        formControlObj['name'] = field.DisplayName;
        formControlObj['isCustomStyling'] = field?.isCustomStyling;
        if (field.isCustomStyling) {
          formControlObj['customClass'] = field.customClass;
        }
        if (
          field.dataType === ProjectConstant.METADATA_EDIT_TYPES.COMBO &&
          field.isDynamic
        ) {
          field.values = this.allListConfig[field?.domain_id];
        } else if (
          field.dataType === ProjectConstant.METADATA_EDIT_TYPES.COMBO
        ) {
          //'COMBO'
          field.values = this.overViewConstants[field?.domain_id];
        }

        fieldsetGroup.push(formControlObj);
        this.allfieldsetGroups.push(formControlObj);
        if (groupName == 'npdCustomProjectOverviewFieldGroup') {
          if (formControlObj.name == 'Market') {
            this.leadMarket = formControlObj.value;

          }
          if (formControlObj.name == 'Business Unit') {
            this.bussinessUnit = formControlObj.value;

          }

        }
        formControlArray.push(formControlObj);

      }

      const fieldGroup = this.formBuilder.group({
        fieldset: new FormArray(formControlArray),
      });
      fieldGroup['name'] = fieldGroupItem.groupName;
      fieldGroup['isExpansionGroup'] = fieldGroupItem.isExpansionGroup;
      fieldGroup['isGovernance'] = fieldGroupItem?.isGovernance;
      fieldGroup['showRequestId'] = fieldGroupItem?.showRequestId;
      fieldGroups.push(fieldGroup);
    }
    //this.npdCustomFieldsGroup = this.formBuilder.group({ fieldset: new FormArray(fieldsetGroup) });
    this.projectOverviewForm.addControl(groupName, new FormArray(fieldGroups)); // { updateOn: 'blur' }
    this.projectOverviewForm.updateValueAndValidity();
    //this.oldNpdCustomProjectdata = this.projectId ? this.createNewNpdProjectUpdate() : null;
  }

  initialiseOverviewConfig() {
    this.overViewConfig = {
      isProject: true,
      formType: null,
      priorityOptions: [],
      statusOptions: null,
      currentUserInfo: this.sharingService.getCurrentUserObject(),
      hasProjectAdminRole: false,
      currentUserId: this.sharingService.getCurrentUserID(),
      currentUserItemId: this.sharingService.getCurrentUserItemID(),
      projectOwners: null,
      selectedProject: null,
      metaDataValues: null,
      isReadOnly: true,
      showRequesterField: false,
      ownerFieldConfig: {
        label: 'Owner',
        filterOptions: [],
        formControl: null,
      },
      dateValidation: null,
    };
  }

  initializeForm() {
    let disableFields = true;
    let disableAdminFields = true;

    this.isReadOnly = this.overViewConfig.isReadOnly;
    if (this.overViewConfig.selectedProject) {
      this.disableEditOptions = true;
      if (
        this.overViewConfig.currentUserItemId ===
        this.overViewConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id']
          .Id ||
        this.filterConfigService.isProgramManagerRole()
      ) {
        this.overViewConfig.isReadOnly = false;
        this.isReadOnly = this.overViewConfig.isReadOnly;
      }
      if (
        this.overViewConfig.hasProjectAdminRole ||
        this.filterConfigService.isProgramManagerRole()
      ) {
        disableAdminFields = false;
      }
    }
    disableFields = this.overViewConfig.isReadOnly;
    this.disableOptions = disableFields;
    this.disableStatusOptions = this.disableOptions;
    if (this.overViewConfig.selectedProject) {
      const project = this.overViewConfig.selectedProject;
      /* impolement cancel disable logic */
      this.selectedStatus =
        project.R_PO_STATUS && project.R_PO_STATUS['MPM_Status-id']
          ? project.R_PO_STATUS['MPM_Status-id'].Id
          : '';
      const selectedStatusInfo = this.projectUtilService.getStatusByStatusId(
        this.selectedStatus,
        this.overViewConfig.statusOptions
      );
      this.selectedPriority =
        project.R_PO_PRIORITY && project.R_PO_PRIORITY['MPM_Priority-id']
          ? project.R_PO_PRIORITY['MPM_Priority-id'].Id
          : '';
      // to have the old status type
      this.oldStatusInfo = selectedStatusInfo;
      let isFinalStatus = false;
      if (
        selectedStatusInfo &&
        selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.FINAL
      ) {
        this.overViewConfig.isReadOnly = true;
        this.isReadOnly = this.overViewConfig.isReadOnly;
        disableFields = true;
        this.disableStatusOptions = true;
        isFinalStatus = true;
        this.isFinalStatus = true;
      }
      if (selectedStatusInfo.TYPE === StatusTypes.ONHOLD) {
        this.overViewConfig.isReadOnly = true;
        this.isReadOnly = this.overViewConfig.isReadOnly;
        //   disableFields = true;
        //isFinalStatus = true;
      }
      if (selectedStatusInfo.TYPE === StatusTypes.FINAL_CANCELLED) {
        this.overViewConfig.isReadOnly = true;
        this.isReadOnly = this.overViewConfig.isReadOnly;
        disableFields = true;
        isFinalStatus = true;
        this.isFinalStatus = true;
      }
      let endDateWeekYearValue = '';
      if (this.checkValueIsNullOrEmpty(project.DUE_DATE, '') != '') {
        endDateWeekYearValue = this.monsterUtilService.calculateTaskWeek(
          project.DUE_DATE
        ); //''+(this.calculateWeek(project.DUE_DATE)-1)+'/'+ new Date(project.DUE_DATE).getFullYear();
      }
      let nextGovernanceDueWeek =
        this.projectRequestConfigDetails?.NPD_PROJECT_NEXT_GOVERNANCE_DUE;
      if (
        nextGovernanceDueWeek &&
        nextGovernanceDueWeek != '' &&
        nextGovernanceDueWeek != 'NA' &&
        nextGovernanceDueWeek != 0
      ) {
        this.isGovernanceOverDue =
          this.monsterUtilService.getStartEndDate(nextGovernanceDueWeek)[0] <
          new Date();
      }
      if (nextGovernanceDueWeek == 'NA') {
        this.isGovernanceOverDue = false;
      }
      if (
        nextGovernanceDueWeek == 'NA' ||
        nextGovernanceDueWeek == '' ||
        nextGovernanceDueWeek == null
      ) {
        nextGovernanceDueWeek = 'None';
      }
      this.projectOverviewForm = this.formBuilder.group({
        name: new FormControl(
          { value: project.PROJECT_NAME, disabled: true }, //disableFields
          [
            Validators.required,
            Validators.maxLength(120),
            Validators.pattern(this.nameStringPattern),
          ]
        ),
        owner: new FormControl(
          {
            value: this.formUserDetails(
              {
                UserId: this.projectOwnerDetails.userCN,
                FullName: this.projectOwnerDetails.fullName,
              },
              this.projectOwnerDetails.userCN
            ),
            disabled: true, //disableAdminFields
          },
          [Validators.required]
        ),
        endDate: new FormControl(
          { value: endDateWeekYearValue, disabled: disableFields },
          [Validators.required]
        ),
        nextGovernanceDueWeek: new FormControl({
          value: nextGovernanceDueWeek,
          disabled: true,
        }),
        priority: new FormControl(
          { value: this.selectedPriority, disabled: disableFields },
          [Validators.required]
        ),
        status: new FormControl(
          { value: this.selectedStatus, disabled: this.disableStatusOptions },
          [Validators.required]
        ),
      });
      this.getProjectOTMMAsset(MPM_LEVELS.PROJECT);
      this.npdCustomFieldsGroup = [];
      this.npdcustomFields = [];

      this.getSectionRoleAccess(isFinalStatus);
      this.isFGProject = this.npdProjectService.getBooleanValue(
        this.projectRequestConfigDetails?.NPD_PROJECT_IS_FG_PROJECT
      );
      this.npdProjectEntityItemId =
        this.projectRequestConfigDetails?.NPD_PROJECT_ITEM_ID;
      this.govConfig =
        this.npdProjectService.mapGovernanceMilestoneBaseConfig();
      this.governanceMilestone =
        this.npdProjectService.mapGovernanceMilestoneConfigurations(
          true,
          this.sectionAccess.programManagerSection
        );
      let secondaryPack: boolean = false;
      if (
        this.projectRequestConfigDetails['NPD_PROJECT_NEW_SECONDARY_PACK'] ==
        true ||
        this.projectRequestConfigDetails['NPD_PROJECT_NEW_SECONDARY_PACK'] ==
        'true'
      ) {
        secondaryPack = true;
      } else if (
        this.projectRequestConfigDetails['NPD_PROJECT_NEW_SECONDARY_PACK'] ==
        false ||
        this.projectRequestConfigDetails['NPD_PROJECT_NEW_SECONDARY_PACK'] ==
        'false'
      ) {
        secondaryPack = false;
      } else {
        if (
          this.requestData['RQ_IS_SECONDARY_PACKAGING'] == true ||
          this.requestData['RQ_IS_SECONDARY_PACKAGING'] == 'true'
        ) {
          secondaryPack = true;
        } else if (
          this.requestData['RQ_IS_SECONDARY_PACKAGING'] == false ||
          this.requestData['RQ_IS_SECONDARY_PACKAGING'] == 'false'
        ) {
          secondaryPack = false;
        } else {
          secondaryPack = false;
        }
      }
      this.secondaryPackageConfig =
        this.npdProjectService.mapSecondaryPackageConfigurations(
          true,
          this.sectionAccess.secondaryPkgSection && secondaryPack,
          this.sectionAccess.secondaryPkgSection && secondaryPack
        );
      this.artworkPrimaryConfig =
        this.npdProjectService.mapSecondaryPackageConfigurations(
          true,
          this.sectionAccess.regulatorySection,
          this.sectionAccess.regulatorySection
        );
      this.artworkStatusOverviewConfig =
        this.npdProjectService.mapArtworkStatusOverviewConfiguration(
          true,
          this.sectionAccess.regulatorySection
        );
      this.canSupplierConfig =
        this.npdProjectService.mapCanSupplierConfigurations(
          true,
          this.sectionAccess.opsPMSection,
          this.projectRequestConfigDetails
        );
      this.programManagerConfig =
        this.npdProjectService.mapprogrammeManagerReportingConfigurations(
          true,
          this.sectionAccess.programManagerSection,
          this.projectRequestConfigDetails,
          this.isNoOfLinkedMarketFieldAccess
        );

      this.projClassificationConfig =
        this.npdProjectService.mapProjectClassificationConfigurations(
          true,
          this.sectionAccess.classificationSection,
          this.projectRequestConfigDetails
        );

      let siteAudit =
        this.projectRequestConfigDetails?.NPD_PROJECT_SITE_AUDIT_REQUIRED;
      let siteReadinessFormGroup =
        this.overViewConstants.TECHNICAL.GroupArray.find((group) => {
          return group.groupName == 'Site Readiness';
        });
      siteReadinessFormGroup?.groupArray.forEach((val) => {
        if (val.enableHideOption == true) {
        }
      });

      let isCP1Completed =
        this.projectRequestConfigDetails?.[
        OverViewConstants.OVERVIEW_MAPPINGS.OTHER_FIELDS.isCP1Completed
        ];
      //if((isCP1Completed == true) || (isCP1Completed == 'true')) {
      let phasingGroup =
        this.overViewConstants.PROGRAMME_MANAGER.GroupArray.find((group) => {
          return group?.groupId == 'phasing';
        });
      phasingGroup?.groupArray.forEach((val) => {
        if (val.id == 'pmCP1PhaseDelay') {
          isCP1Completed == true || isCP1Completed == 'true'
            ? (val.editable = false)
            : (val.editable = true);
        }
      });

      //if((siteAudit == false) || (siteAudit == 'false')) {}
      this.getNpdCustomConfig(
        this.overViewConstants.RequestOverview.GroupArray,
        'npdCustomProjectOverviewFieldGroup',
        !this.sectionAccess.overviewSection
      );
      this.getNpdCustomConfig(
        this.overViewConstants.PROJECT_CLASSIFICATION.GroupArray,
        'npdCustomProjectClassificationFieldGroup',
        !this.sectionAccess.classificationSection
      );
      this.getNpdCustomConfig(
        this.overViewConstants.PROGRAMME_MANAGER.GroupArray,
        'npdCustomProgramManagerFieldGroup',
        !this.sectionAccess.programManagerSection
      );
      this.getNpdCustomConfig(
        this.overViewConstants.PROJECT_CORE_DATA.GroupArray,
        'npdCustomProjectCoreDataFieldGroup',
        !this.sectionAccess.projCoreDataSection
      );
      //this.getNpdCustomConfig(this.overViewConstants.E2EPM.GroupArray,'npdCustomE2EPMFieldGroup',!this.isE2EPM);
      this.getNpdCustomConfig(
        this.overViewConstants.E2EPM.GroupArray,
        'npdCustomE2EPMFieldGroup',
        !this.sectionAccess.e2ePMSection
      );

      this.getNpdCustomConfig(
        this.overViewConstants.REGULATORY.GroupArray,
        'npdCustomRegulatoryFieldGroup',
        !this.sectionAccess.regulatorySection
      );
      this.getNpdCustomConfig(
        this.overViewConstants.SECONDARY_PACKAGING.GroupArray,
        'npdCustomSecondaryPackagingFieldGroup',
        !this.sectionAccess.secondaryPkgSection
      );

      this.getNpdCustomConfig(
        this.overViewConstants.TECHNICAL.GroupArray,
        'npdCustomTechnicalFieldGroup',
        !this.sectionAccess.technicalSection
      );
      this.getNpdCustomConfig(
        this.overViewConstants.OPSPM.GroupArray,
        'npdCustomOpsPMFieldGroup',
        !this.sectionAccess.opsPMSection
      );

      //this.getNpdCustomConfig(this.overViewConstants.EMEA.GroupArray,'npdCustomEMEAFieldGroup');
      //this.getNpdCustomConfig(this.overViewConstants.ARTWORK.GroupArray,'npdCustomArtworkFieldGroup');

      this.npdCustomFieldsGroup = this.formBuilder.group({
        fieldset: new FormArray(this.allfieldsetGroups),
      });
      this.oldNpdCustomProjectdata = this.projectId
        ? this.createNewNpdProjectUpdate(this.npdCustomFieldsGroup)
        : null;
      this.newNpdCustomProjectdata = this.oldNpdCustomProjectdata;
    }
  }

  getProjectDataSapAccess(isFinalStatus) {
    const isE2EPM =
      this.filterConfigService.isE2EPMRole() &&
      this.loggedInUser ==
      this.projectRequestConfigDetails?.NPD_PROJECT_E2E_PM_NAME;
    const isCorpPm =
      this.filterConfigService.isCorpPMRole() &&
      this.loggedInUser ==
      this.projectRequestConfigDetails?.NPD_PROJECT_CORP_PM_NAME;
    const isOpsPm =
      this.filterConfigService.isOPSPMRole() &&
      this.loggedInUser ==
      this.projectRequestConfigDetails?.NPD_PROJECT_OPS_PM_NAME;
    const isProjectSpecialist =
      this.filterConfigService.isProjectSpecialistRole() &&
      this.loggedInUser ==
      this.projectRequestConfigDetails?.NPD_PROJECT_PR_SPECIALIST_NAME;
    const isRTM =
      this.filterConfigService.isRegionalTechnicalManagerRole() &&
      this.loggedInUser ==
      this.projectRequestConfigDetails
        ?.NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME;
    const isGFG =
      this.filterConfigService.isGFGRole() &&
      this.loggedInUser ==
      this.projectRequestConfigDetails?.NPD_PROJECT_GFG_NAME;
    const isCorpLead = this.filterConfigService.isCorpProjectLeader();
    this.isProjectDataSapAccess =
      !isFinalStatus &&
      (isE2EPM ||
        isCorpPm ||
        isOpsPm ||
        isProjectSpecialist ||
        isRTM ||
        isGFG ||
        isCorpLead);
  }

  isFieldAccessible(isFinalStatus) {
    const isE2EPM =
      this.filterConfigService.isE2EPMRole() &&
      this.loggedInUser ==
      this.projectRequestConfigDetails?.NPD_PROJECT_E2E_PM_NAME;
    const isPM = this.filterConfigService.isProgramManagerRole();
    const isCorpLead = this.filterConfigService.isCorpProjectLeader();

    this.isNoOfLinkedMarketFieldAccess =
      !isFinalStatus && (isE2EPM || isPM || this.isAdmin);
    this.isProgramTagFieldAccess =
      !isFinalStatus && (isCorpLead || isPM || this.isAdmin || isE2EPM);
    // this.isProgramTagFieldAccess =true ;
  }

  getSectionRoleAccess(isFinalStatus) {
    let userDisplayName: string = JSON.parse(
      sessionStorage.getItem('USER_DETAILS_WORKFLOW')
    )?.User?.UserDisplayName;
    this.loggedInUser = userDisplayName;
    let superAccess: boolean;
    superAccess =
      (!isFinalStatus && this.isAdmin) ||
      this.filterConfigService.isProgramManagerRole() ||
      (this.filterConfigService.isE2EPMRole() &&
        userDisplayName ==
        this.projectRequestConfigDetails?.NPD_PROJECT_E2E_PM_NAME) ||
      (this.filterConfigService.isCorpPMRole() &&
        userDisplayName ==
        this.projectRequestConfigDetails?.NPD_PROJECT_CORP_PM_NAME) ||
      this.overViewConfig?.currentUserItemId ===
      this.overViewConfig?.selectedProject?.R_PO_PROJECT_OWNER?.[
        'Identity-id'
      ]?.Id; //check project owner
    let isTask4_5NotCompleted;
    !superAccess
      ? this.getProjectDataSapAccess(isFinalStatus)
      : (this.isProjectDataSapAccess = true);
    // let isTask4ExistOrTask5Exist;
    this.isFieldAccessible(isFinalStatus);
    isTask4_5NotCompleted = !(
      this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK4_COMPLETED ==
      'true' &&
      this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK5_COMPLETED == 'true'
    );
    // isTask4ExistOrTask5Exist = (this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK4_EXISTS == 'true' || this.projectRequestConfigDetails?.NPD_PROJECT_IS_TASK5_EXISTS == 'true');
    this.sectionAccess.overviewSection = !isFinalStatus && superAccess;
    this.sectionAccess.classificationSection =
      !isFinalStatus && superAccess && isTask4_5NotCompleted; // && (isTask4ExistOrTask5Exist);
    this.sectionAccess.programManagerSection = !isFinalStatus && superAccess;
    this.sectionAccess.projCoreDataSection =
      !isFinalStatus &&
      (this.filterConfigService.isProgramManagerRole() ||
        this.overViewConfig?.currentUserItemId ===
        this.overViewConfig?.selectedProject?.R_PO_PROJECT_OWNER?.[
          'Identity-id'
        ]?.Id);
    this.sectionAccess.e2ePMSection = !isFinalStatus && superAccess;
    this.sectionAccess.regulatorySection =
      !isFinalStatus &&
      (superAccess ||
        this.filterConfigService.isRegsTrafficManager() ||
        (this.filterConfigService.isRegulatoryRole() &&
          userDisplayName ==
          this.projectRequestConfigDetails
            ?.NPD_PROJECT_REGULATORY_LEAD_NAME));
    this.sectionAccess.secondaryPkgSection =
      !isFinalStatus &&
      (superAccess ||
        this.filterConfigService.isRegsTrafficManager() ||
        (this.filterConfigService.isRegulatoryRole() &&
          userDisplayName ==
          this.projectRequestConfigDetails
            ?.NPD_PROJECT_REGULATORY_LEAD_NAME) ||
        (this.filterConfigService.isSecondaryPackagingRole() &&
          userDisplayName ==
          this.projectRequestConfigDetails
            ?.NPD_PROJECT_SECONDARY_AW_SPECIALIST_NAME));
    // this.sectionAccess.secondaryPkgSection = !isFinalStatus && (superAccess || (this.filterConfigService.isRegulatoryRole()) || (this.filterConfigService.isSecondaryPackagingRole() ? userDisplayName == this.projectRequestConfigDetails?.NPD_PROJECT_PR_SPECIALIST_NAME : false));
    this.sectionAccess.technicalSection =
      !isFinalStatus &&
      (superAccess ||
        (this.filterConfigService.isTechnicalQualRole()
          ? userDisplayName ==
          this.projectRequestConfigDetails
            ?.NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME
          : false));
    this.sectionAccess.opsPMSection =
      !isFinalStatus &&
      (superAccess ||
        (this.filterConfigService.isOPSPMRole()
          ? userDisplayName ==
          this.projectRequestConfigDetails?.NPD_PROJECT_OPS_PM_NAME
          : false));
  }

  formUserDetails(userObj: any, userId?: string) {
    if (userObj && userObj.UserDisplayName && userId && userId !== '') {
      return {
        name: userObj.UserDisplayName,
        value: userId,
        displayName: userObj.UserDisplayName + ' (' + userId + ')',
      };
    } else if (userObj && userObj.UserId && userObj.UserId !== '') {
      return {
        name: userObj.Description || userObj.FullName,
        value: userObj.UserId,
        displayName:
          (userObj.Description || userObj.FullName) +
          ' (' +
          userObj.UserId +
          ')',
      };
    } else {
      return null;
    }
  }

  getPriorities(categoryLevelName): Array<Priority> {
    const currCategoryObj: CategoryLevel = this.sharingService
      .getCategoryLevels()
      .find((data) => {
        return data.CATEGORY_LEVEL_TYPE === categoryLevelName;
      });
    //this.loadingHandler.next(true);

    if (currCategoryObj) {
      if (
        sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES) !== null
      ) {
        let allPriorities: Array<Priority> = JSON.parse(
          sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES)
        );
        let allPriority: Array<Priority> = allPriorities.filter(
          (priority: Priority) => {
            return (
              priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id ===
              currCategoryObj['MPM_Category_Level-id'].Id
            );
          }
        );
        let priority = [];
        priority.push(allPriority);
        return priority;
      } else {
        let allPriority: Array<Priority> = this.sharingService
          .getAllPriorities()
          .filter((priority: Priority) => {
            return (
              priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id ===
              currCategoryObj['MPM_Category_Level-id'].Id
            );
          });
        let priority = [];
        priority.push(allPriority);
        return priority;
      }
    }
    return [];
  }

  dateFilter: (date: Date | null) => boolean = (date: Date | null) => {
    if (this.enableWorkWeek) {
      return true;
    } else {
      const day = date.getDay();
      return day !== 0 && day !== 6;
      //0 means sunday
      //6 means saturday
    }
  };

  formCategoryMetadata(metadataId: any) {
    const selectedCategoryMetadata =
      this.overViewConfig.categoryMetadataConfig.filterOptions.find(
        (categoryMetadata) => categoryMetadata.value === metadataId
      );
    return selectedCategoryMetadata ? selectedCategoryMetadata : null;
  }

  getMaxMinValueForCustomMetadata(dataLength, scale): any {
    let data = Math.abs(dataLength - scale);
    let numericValue = newArray(data).fill('9').join('');
    if (scale) {
      numericValue += '.' + newArray(scale).fill('9').join('');
    }
    // tslint:disable-next-line: radix
    return scale ? parseFloat(numericValue) : parseInt(numericValue);
  }

  getLocalValueById(metadataFieldId) {
    const customMetadata = this.utilService.getProjectCustomMetadata();
    if (!customMetadata) {
      return '';
    }
    let resultValue = '';
    customMetadata.Metadata.metadataFields.forEach((element) => {
      if (element.fieldId === metadataFieldId) {
        resultValue = element.defaultValue;
      }
    });
    return resultValue;
  }

  getProjectOTMMAsset(categoryLevel) {
    if (
      this.overViewConfig.selectedProject &&
      this.overViewConfig.selectedProject.OTMM_FOLDER_ID &&
      typeof this.overViewConfig.selectedProject.OTMM_FOLDER_ID === 'string'
    ) {
      this.otmmService
        .getOTMMFodlerById(this.overViewConfig.selectedProject.OTMM_FOLDER_ID)
        .subscribe((folderDetails) => {
          if (folderDetails && folderDetails.metadata) {
            this.overViewConfig.metaDataValues = folderDetails.metadata;
          }
          if (
            this.overViewConfig.selectedProject &&
            this.overViewConfig.selectedProject.R_PO_CATEGORY &&
            this.overViewConfig.selectedProject.R_PO_CATEGORY[
            'MPM_Category-id'
            ] &&
            this.overViewConfig.selectedProject.R_PO_CATEGORY['MPM_Category-id']
              .Id
          ) {
            const categoryMetadataID =
              this.overViewConfig.selectedProject &&
                this.overViewConfig.selectedProject.R_PO_CATEGORY_METADATA
                ? this.overViewConfig.selectedProject.R_PO_CATEGORY_METADATA[
                  'MPM_Category_Metadata-id'
                ].Id
                : null;
            this.getMetaData(categoryLevel, categoryMetadataID);
          }
        });
    } else {
      this.getMetaData(categoryLevel, null);
    }
  }

  getMetaData(categoryLevel, categoryMetadatId) {
    let categoryLevelDetails =
      this.categoryService.getCategoryLevelDetailsByType(categoryLevel);
    if (categoryMetadatId) {
      const selectedCategoryMetadata =
        this.formCategoryMetadata(categoryMetadatId);
      categoryLevelDetails = selectedCategoryMetadata
        ? selectedCategoryMetadata
        : categoryLevelDetails;
    }
    this.otmmService
      .getMetadatModelById(categoryLevelDetails.METADATA_MODEL_ID)
      .subscribe((metaDataModelDetails) => {
        const fieldsetGroup = [];
        const fieldGroups = [];
        this.customMetadataFields = [];
        if (this.projectOverviewForm.get('CustomFieldGroup')) {
          this.projectOverviewForm.removeControl('CustomFieldGroup');
        }
        if (
          metaDataModelDetails &&
          metaDataModelDetails.metadata_element_list &&
          Array.isArray(metaDataModelDetails.metadata_element_list)
        ) {
          for (const metadataFieldGroup of metaDataModelDetails.metadata_element_list) {
            const formControlArray = [];
            if (
              metadataFieldGroup.id ===
              ProjectConstant.MPM_PROJECT_METADATA_GROUP ||
              metadataFieldGroup.id ===
              ProjectConstant.MPM_CAMPAIGN_METADATA_GROUP
            ) {
              continue;
            }
            if (
              metadataFieldGroup.metadata_element_list &&
              Array.isArray(metadataFieldGroup.metadata_element_list)
            ) {
              for (const metadataField of metadataFieldGroup.metadata_element_list) {
                this.customMetadataFields.push(metadataField);
                const validators = [];
                const value = this.overViewConfig.metaDataValues
                  ? this.otmmMetadataService.getFieldValueById(
                    this.overViewConfig.metaDataValues,
                    metadataField.id
                  )
                  : this.getLocalValueById(metadataField.id);

                const formControlObj = new FormControl({
                  value: typeof value !== 'undefined' ? value : '',
                  disabled:
                    !metadataField.editable ||
                    (this.projectId && this.overViewConfig.isReadOnly),
                });

                if (metadataField.required) {
                  validators.push(Validators.required);
                }

                if (
                  metadataField.edit_type ===
                  ProjectConstant.METADATA_EDIT_TYPES.DATE
                ) {
                  validators.push(DateValidators.dateFormat);
                }

                if (
                  metadataField.edit_type ===
                  ProjectConstant.METADATA_EDIT_TYPES.SIMPLE ||
                  metadataField.edit_type ===
                  ProjectConstant.METADATA_EDIT_TYPES.TEXTAREA
                ) {
                  validators.push(
                    Validators.maxLength(metadataField.data_length)
                  );
                }
                if (
                  metadataField.data_type ===
                  ProjectConstant.METADATA_EDIT_TYPES.NUMBER
                ) {
                  const numericeVlalue = this.getMaxMinValueForCustomMetadata(
                    metadataField.data_length,
                    metadataField.scale
                  );
                  validators.push(
                    Validators.max(numericeVlalue),
                    Validators.min(-numericeVlalue)
                  );
                  // (!metadataField.scale) ? validators.push(Validators.pattern(new RegExp('(-(\d{0,5}))|(\d{0,5})$')))
                  //     : validators.push(Validators.pattern(new RegExp('-((\d+\.)?\d{0,' + metadataField.scale + '}$) | ((\d+\.)?\d{0,' + metadataField.scale + '})$')));
                }
                formControlObj.setValidators(validators);
                formControlObj.updateValueAndValidity();

                formControlObj['fieldset'] = metadataField;
                formControlObj['name'] = metadataField.name;

                if (
                  metadataField.edit_type ===
                  ProjectConstant.METADATA_EDIT_TYPES.COMBO
                ) {
                  metadataField.values =
                    this.utilService.getLookupDomainValuesById(
                      metadataField.domain_id
                    );
                }

                fieldsetGroup.push(formControlObj);

                // new code
                formControlArray.push(formControlObj);
              }
            }
            const fieldGroup = this.formBuilder.group({
              fieldset: new FormArray(formControlArray),
            });
            fieldGroup['name'] = metadataFieldGroup.name;
            fieldGroups.push(fieldGroup);
          }
        }
        if (
          this.customMetadataFields &&
          Array.isArray(this.customMetadataFields) &&
          this.customMetadataFields.length > 0
        ) {
          // Need to be tested
          fieldsetGroup.forEach((fieldSet) => {
            if (
              fieldSet.fieldset.data_type === DataTypeConstants.DATE &&
              fieldSet.value
            ) {
              fieldSet.value = new Date(fieldSet.value);
            }
          });
          this.customFieldsGroup = this.formBuilder.group({
            fieldset: new FormArray(fieldsetGroup),
          });
          this.projectOverviewForm.addControl(
            'CustomFieldGroup',
            new FormArray(fieldGroups, { updateOn: 'blur' })
          );
        }
        this.projectOverviewForm.updateValueAndValidity();
        this.oldProjectMetadata = this.projectId
          ? this.createNewProjectUpdate()
          : null;
      });
  }
  getFormGorup(formGorup) {
  }

  refreshView() {
    this.initialiseOverviewConfig();
    this.overViewConfig.formType = ProjectTypes.PROJECT;
    if (this.projectId) {
      this.getProjectDetail(this.projectId);
    }
  }

  getProjectDetail(projectId) {
    const getRequestObject = {
      projectID: projectId,
    };
    forkJoin([
      this.appService.invokeRequest(
        this.PROJECT_DETAILS_METHOD_NS,
        this.GET_PROJECT_DETAILS_WS_METHOD_NAME,
        getRequestObject
      ) /*, this.projectService.getProjectDataValues(projectId)*/,
    ]).subscribe(
      (response) => {
        if (response && response[0].Project) {
          const project = response[0].Project;
          this.overViewConfig.selectedProject = project;
          this.selectedTeam = project?.R_PO_TEAM;
          const projectStatuses = this.statusService.getAllStatusBycategoryName(
            MPM_LEVELS.PROJECT
          );
          const projectStatus: any =
            projectStatuses && projectStatuses.length > 0
              ? projectStatuses[0]
              : projectStatuses;
          //  const initialProjectStatus = projectStatuses.find(status => status.STATUS_TYPE === StatusTypes.INITIAL);
          const initialProjectStatus = projectStatus.find(
            (status) => status.STATUS_TYPE === StatusTypes.INITIAL
          );
          const cancelledProjectStatus = projectStatus.find(
            (status) => status.STATUS_TYPE === StatusTypes.FINAL_CANCELLED
          );
          if (
            initialProjectStatus['MPM_Status-id'].Id !==
            this.overViewConfig.selectedProject.R_PO_STATUS['MPM_Status-id']
              .Id &&
            cancelledProjectStatus['MPM_Status-id'].Id !==
            this.overViewConfig.selectedProject.R_PO_STATUS['MPM_Status-id']
              .Id &&
            this.enableProjectRestartConfig
          ) {
            this.enableRestartProject = true;
          } else {
            this.enableRestartProject = false;
          }
          const projectstatus = projectStatus.find(
            (status) => status.STATUS_TYPE === StatusTypes.FINAL_COMPLETED
          );
          this.projectStatuss =
            projectstatus['MPM_Status-id'].Id ==
              this.overViewConfig.selectedProject.R_PO_STATUS['MPM_Status-id'].Id
              ? true
              : false;
          //this.projectService.setUpdatedProjectInfo(project);
          //this.overViewConfig.dateValidation = response[1];
          let statuses = this.statusService.getAllStatusBycategoryName(
            this.categoryLevel
          );

          const status: any =
            statuses && statuses.length > 0 ? statuses[0] : statuses;
          this.overViewConfig.statusOptions =
            this.projectUtilService.filterStatusOptions(
              this.overViewConfig.selectedProject.R_PO_STATUS['MPM_Status-id']
                .Id,
              status,
              MPM_LEVELS.PROJECT
            );
          if (
            this.overViewConfig?.selectedProject?.R_PO_PROJECT_OWNER &&
            this.overViewConfig.selectedProject.R_PO_PROJECT_OWNER[
              'Identity-id'
            ].Id
          ) {
            const parameters = {
              userId:
                this.overViewConfig.selectedProject.R_PO_PROJECT_OWNER[
                  'Identity-id'
                ].Id,
            };
            // this.initializeForm()
            this.getProjectIndexerDetailsById().subscribe(() => {
              // this.initializeForm();
              this.appService
                .getPersonByIdentityUserId(parameters)
                .subscribe((personDetails) => {
                  this.projectOwnerDetails = personDetails;
                  let priorityOptions = this.getPriorities(this.categoryLevel);
                  const priorities: any =
                    priorityOptions && priorityOptions.length > 0
                      ? priorityOptions[0]
                      : priorityOptions;
                  this.overViewConfig.priorityOptions = priorities;
                  if (
                    !this.selectedPriority &&
                    this.overViewConfig.priorityOptions &&
                    this.overViewConfig.priorityOptions.length > 0
                  ) {
                    this.selectedPriority =
                      this.overViewConfig.priorityOptions[0][
                        'MPM_Priority-id'
                      ].Id;
                  }
                  // this.initializeForm();
                  this.getFilters();
                  // this.loadingHandler.next(false);
                  //    //this.queryMode = 0;
                  // this.loaderService.hide();
                });
            });
          }
        } else {
          //
          //this.loadingHandler.next(false);
          // //this.queryMode = 0;
        }
      },
      (error) => {
        //this.loadingHandler.next(false);
        // //this.queryMode = 0;
        const eventData = {
          message: 'Something went wrong while fetching project details',
          type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
        };
        this.notificationHandler.next(eventData);
      }
    );
    // this.loadingHandler.next(true);
    // //this.queryMode = 50
  }

  getTeams(): Observable<Array<Team>> {
    return new Observable((observer) => {
      const roleDetailObj: Roles = this.sharingService.getRoleByName(
        MPM_ROLES.MANAGER
      );
      const param = {
        roleDN: roleDetailObj.ROLE_DN,
      };
      this.loadingHandler.next(true);
      this.appService
        .invokeRequest(
          this.TEAM_BPM_METHOD_NS,
          this.GET_TEAM_WS_METHOD_NAME,
          param
        )
        .subscribe(
          (response) => {
            const teams: Array<Team> = acronui.findObjectsByProp(
              response,
              'MPM_Teams'
            );
            observer.next(teams);
            observer.complete();

            //  this.loadingHandler.next(false);
          },
          (error) => {
            // this.loadingHandler.next(false);
            const eventData = {
              message: 'Something went wrong while fetching Teams',
              type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
            };
            this.notificationHandler.next(eventData);
            observer.next([]);
            observer.complete();
          }
        );
    });
  }

  getSecondaryPackLeadTimes(): Observable<any> {
    this.allListConfig['secondaryPackLeadTime'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllSecondaryPackLeadTimes().subscribe(
        (response) => {
          let secondaryPackLeadTimes = [];
          secondaryPackLeadTimes = response;
          if (secondaryPackLeadTimes?.length > 0) {
            secondaryPackLeadTimes.forEach((secondaryPackLeadTime) => {
              let spLeadTime = {
                displayName: secondaryPackLeadTime?.PackType,
                value:
                  secondaryPackLeadTime?.['Secondary_Pack_Lead_Times-id']?.[
                  'ItemId'
                  ],
              };
              this.allListConfig['secondaryPackLeadTime'].push(spLeadTime);
            });
          }
          //this.allListConfig['secondaryPackLeadTime'] = response;
          observer.next(response);
          observer.complete();

          //  this.loadingHandler.next(false);
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message:
              'Something went wrong while fetching Secondary Pack lead Times',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getPrintSuppliers(): Observable<any> {
    this.allListConfig['printSuppliers'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllPrintSuppliers().subscribe(
        (response) => {
          let printSuppliers = [];
          printSuppliers = response;
          if (printSuppliers?.length > 0) {
            printSuppliers.forEach((printSupplier) => {
              let spLeadTime = {
                displayName: printSupplier?.Supplier,
                value: printSupplier?.['Print_Supplier-id']?.['ItemId'],
              };
              this.allListConfig['printSuppliers'].push(spLeadTime);
            });
          }
          //this.allListConfig['secondaryPackLeadTime'] = response;
          observer.next(response);
          observer.complete();

          //  this.loadingHandler.next(false);
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message:
              'Something went wrong while fetching Secondary Pack lead Times',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getTrialSupervisionNPD(): Observable<any> {
    this.allListConfig['trialSupervisionNPD'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllTrialSupervisionNPD().subscribe(
        (response) => {
          let trialSupervisionNPDResponse = [];
          trialSupervisionNPDResponse = response;
          if (trialSupervisionNPDResponse?.length > 0) {
            trialSupervisionNPDResponse.forEach((trialSupervisionNPD) => {
              let trialSupervisionNPDValue = {
                displayName: trialSupervisionNPD?.Users,
                value:
                  trialSupervisionNPD?.['Trial_Supervision_NPD-id']?.['ItemId'],
              };
              this.allListConfig['trialSupervisionNPD'].push(
                trialSupervisionNPDValue
              );
            });
          }
          //this.allListConfig['secondaryPackLeadTime'] = response;
          observer.next(response);
          observer.complete();

          //  this.loadingHandler.next(false);
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message:
              'Something went wrong while fetching trial supervision NPD',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getTastingRequirements(): Observable<any> {
    this.allListConfig['tastingRequirement'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllTastingRequirements().subscribe(
        (response) => {
          let printSuppliers = [];
          printSuppliers = response;
          if (printSuppliers?.length > 0) {
            printSuppliers.forEach((trialSupervisionQuality) => {
              let trialSupervisionQualityValue = {
                displayName: trialSupervisionQuality?.TastingRequirement,
                value:
                  trialSupervisionQuality?.['Tasting_Requirement-id']?.[
                  'ItemId'
                  ],
              };
              this.allListConfig['tastingRequirement'].push(
                trialSupervisionQualityValue
              );
            });
          }
          observer.next(response);
          observer.complete();
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message: 'Something went wrong while fetching Tasting Requirements',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getTrialProtocolWrittenByUsers(): Observable<any> {
    this.allListConfig['trialProtocolWrittenByUsers'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllTrialProtocolWrittenByUsers().subscribe(
        (response) => {
          let printSuppliers = [];
          printSuppliers = response;
          if (printSuppliers?.length > 0) {
            printSuppliers.forEach((trialSupervisionQuality) => {
              let trialSupervisionQualityValue = {
                displayName: trialSupervisionQuality?.Users,
                value:
                  trialSupervisionQuality?.['Trial_Protocol_Written_By-id']?.[
                  'ItemId'
                  ],
              };
              this.allListConfig['trialProtocolWrittenByUsers'].push(
                trialSupervisionQualityValue
              );
            });
          }
          observer.next(response);
          observer.complete();
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message:
              'Something went wrong while fetching Trial Protocol Written By Users',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getTrialSupervisionQuality(): Observable<any> {
    this.allListConfig['trialSupervisionQuality'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllTrialSupervisionQuality().subscribe(
        (response) => {
          let printSuppliers = [];
          printSuppliers = response;
          if (printSuppliers?.length > 0) {
            printSuppliers.forEach((trialSupervisionQuality) => {
              let trialSupervisionQualityValue = {
                displayName: trialSupervisionQuality?.Users,
                value:
                  trialSupervisionQuality?.['Trial_Supervision_Quality-id']?.[
                  'ItemId'
                  ],
              };
              this.allListConfig['trialSupervisionQuality'].push(
                trialSupervisionQualityValue
              );
            });
          }
          //this.allListConfig['secondaryPackLeadTime'] = response;
          observer.next(response);
          observer.complete();

          //  this.loadingHandler.next(false);
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message:
              'Something went wrong while fetching Secondary Pack lead Times',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getSecondaryArtworkSpecialists(): Observable<any> {
    this.allListConfig['secondaryArtworkSpecialistUsers'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllSecondaryArtworkSpecialists().subscribe(
        (response) => {
          this.allListConfig.secondaryArtworkSpecialistUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          // this.loadingHandler.next(false);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getAuditUsers(): Observable<any> {
    this.allListConfig['auditUsers'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllAuditUsers().subscribe(
        (response) => {
          let auditUsers = [];
          auditUsers = response;
          if (auditUsers?.length > 0) {
            auditUsers.forEach((user) => {
              let auditUser = {
                displayName: user?.Users,
                value: user?.['Audit_Users-id']?.['ItemId'],
              };
              this.allListConfig['auditUsers'].push(auditUser);
            });
          }
          observer.next(response);
          observer.complete();
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message: 'Something went wrong while fetching Audit users',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getCanCompanies(): Observable<any> {
    this.allListConfig['canCompanies'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllCanCompanies().subscribe(
        (response) => {
          let canCompanies = [];
          canCompanies = response;
          if (canCompanies?.length > 0) {
            canCompanies.forEach((canCompany) => {
              let canCompanyValue = {
                displayName: canCompany?.CanCompany,
                value: canCompany?.['Can_Supplier_Data-id']?.['Id'],
              };
              this.allListConfig['canCompanies'].push(canCompanyValue);
            });
          }
          observer.next(response);
          observer.complete();
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message: 'Something went wrong while fetching Can companies',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getE2EPMUsers(): Observable<any> {
    this.allListConfig['e2EPMUsers'] = [];
    return new Observable((observer) => {
      // this.loadingHandler.next(true);
      this.npdProjectService.getAllE2EPMUsers().subscribe(
        (response) => {
          this.allListConfig.e2EPMUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          // this.loadingHandler.next(false);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getCorpPMUsers(): Observable<any> {
    this.allListConfig['corpPMUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllCorpPMUsers().subscribe(
        (response) => {
          this.allListConfig.corpPMUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getOPSPMUsers(): Observable<any> {
    this.allListConfig['opsPMUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllOPSPMUsers().subscribe(
        (response) => {
          this.allListConfig.opsPMUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getRTMUsers(): Observable<any> {
    this.allListConfig['rtmUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllRTMUsers().subscribe(
        (response) => {
          this.allListConfig.rtmUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getOpsPlannerUsers(): Observable<any> {
    this.allListConfig['opsPlannerUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllOpsPlannerUsers().subscribe(
        (response) => {
          this.allListConfig.opsPlannerUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getGFGUsers(): Observable<any> {
    this.allListConfig['gfgUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllGFGUsers().subscribe(
        (response) => {
          this.allListConfig.gfgUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getFPMSUsers(): Observable<any> {
    this.allListConfig['fpmsUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllFPMSUsers().subscribe(
        (response) => {
          this.allListConfig.fpmsUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getProjectSpecialistUsers(): Observable<any> {
    this.allListConfig['projectSpecialistUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllProjectSpecialistUsers().subscribe(
        (response) => {
          this.allListConfig.projectSpecialistUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getRegulatoryLeadUsers(): Observable<any> {
    this.allListConfig['regulatoryLeadUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllRegulatoryLeadUsers().subscribe(
        (response) => {
          this.allListConfig.regulatoryLeadUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getCommercialLeadUsers(): Observable<any> {
    this.allListConfig['commercialLeadUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllCommercialLeadUsers().subscribe(
        (response) => {
          this.allListConfig.commercialLeadUsers = response;
          observer.next([]);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getProgramTags(): Observable<any> {
    return new Observable((observer) => {
      this.monsterConfigApiService.getAllProgramTags().subscribe(
        (response) => {
          this.allListConfig.programTags = response;
          observer.next(true);
          observer.complete();
        },
        (error) => {
          observer.error(error);
          this.notificationService.error('Unable to get all the Program Tags.');
        }
      );
    });
  }

  getProjectTypes(): Observable<any> {
    return new Observable((observer) => {
      this.monsterConfigApiService.getAllProjectTypes().subscribe(
        (response) => {
          this.allListConfig.projectTypes =
            this.monsterConfigApiService.transformResponse(
              response,
              'Project_Type-id',
              true
            );
          observer.next(true);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getWhoWillCompleteUsers() {
    this.allListConfig['whoWillCompleteUsers'] = [];
    return new Observable((observer) => {
      this.npdProjectService.getAllWhoWillCompleteUsers().subscribe(
        (response) => {
          let users = [];
          users = response;
          if (users?.length > 0) {
            users.forEach((user) => {
              let userValue = {
                displayName: user?.Users,
                value: user?.['Tech_Qual_Users-id']?.['ItemId'],
              };
              this.allListConfig['whoWillCompleteUsers'].push(userValue);
            });
          }
          observer.next(response);
          observer.complete();
        },
        (error) => {
          const eventData = {
            message:
              'Something went wrong while fetching Tech Qual Complete users',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getFilters() {
    //this.getProjectDetail(this.projectId);
    forkJoin([
      // this.getPriorities(this.categoryLevel),
      // this.statusService.getAllStatusBycategoryName(this.categoryLevel),
      this.getTeams(),
      this.getE2EPMUsers(),
      this.getCorpPMUsers(),
      this.getOPSPMUsers(),
      this.getRTMUsers(),
      this.getOpsPlannerUsers(),
      this.getGFGUsers(),
      this.getProjectSpecialistUsers(),
      this.getRegulatoryLeadUsers(),
      this.getCommercialLeadUsers(),
      this.getProjectTypes(),
      this.getSecondaryPackLeadTimes(),
      this.getPrintSuppliers(),
      this.getTastingRequirements(),
      this.getTrialProtocolWrittenByUsers(),
      this.getTrialSupervisionNPD(),
      this.getTrialSupervisionQuality(),
      this.getAuditUsers(),
      this.getSecondaryArtworkSpecialists(),
      this.getCanCompanies(),
      // this.getProjectIndexerDetailsById(),
      this.getReportingProjectType(),
      this.getWhoWillCompleteUsers(),
      this.getProgramTags(),
      this.getFPMSUsers(),
    ]) //this.npdProjectService.getTasksNotCreatedDetails(this.projectId),this.npdProjectService.calculateResidualFields(this.projectId)
      .subscribe(
        (forkedData: any[]) => {
          this.loaderService.hide();
          // this.overViewConfig.priorityOptions = forkedData[0]; //forkedData[0];
          // this.overViewConfig.statusOptions =
          //   this.projectUtilService.filterStatusOptions(
          //     this.overViewConfig.selectedProject.R_PO_STATUS['MPM_Status-id']
          //       .Id,
          //     forkedData[1],
          //     MPM_LEVELS.PROJECT
          //   );
          this.overViewConfig.teamsOptions =
            forkedData[0] && forkedData[0].length > 0
              ? forkedData[0]
              : this.sharingService.teams;
          // TODO: selecting Default Pririty - column to be added to entity
          // if (
          //   !this.selectedPriority &&
          //   this.overViewConfig.priorityOptions &&
          //   this.overViewConfig.priorityOptions.length > 0
          // ) {
          //   this.selectedPriority =
          //     this.overViewConfig.priorityOptions[0]['MPM_Priority-id'].Id;
          // }
          // if (
          //   this.overViewConfig?.selectedProject?.R_PO_PROJECT_OWNER &&
          //   this.overViewConfig.selectedProject.R_PO_PROJECT_OWNER[
          //     'Identity-id'
          //   ].Id
          // ) {
          //   // const parameters = {
          //   //   userId:
          //   //     this.overViewConfig.selectedProject.R_PO_PROJECT_OWNER[
          //   //       'Identity-id'
          //   //     ].Id,
          //   // };
          //   // this.appService
          //   //   .getPersonByIdentityUserId(parameters)
          //   //   .subscribe((personDetails) => {
          //   //     this.projectOwnerDetails = personDetails;

          this.initializeForm();
          //     //   this.loadingHandler.next(false);
          //     //   this.loaderService.hide();
          //     // });
          // }

          //this.loadingHandler.next(false);
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message: 'Something went wrong while fetching configs',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };

          this.loaderService.hide();
          //this.notificationHandler.next(eventData);
        }
      );
    this.npdProjectService.calculateResidualFields(this.projectId).subscribe(res => {
      this.loaderService.hide();
    }, err => {
      this.loaderService.hide();

    })
  }

  displayFn(user?: any): string | undefined {
    return user ? user.displayName : undefined;
  }

  getUsersForTeamRole() {
    return new Observable((observer) => {
      const roleDetailObj: Roles = this.sharingService.getRoleByName(
        MPM_ROLES.MANAGER
      );
      const param = {
        teamID: this.selectedTeam && this.selectedTeam['MPM_Teams-id'].Id,
        roleDN: roleDetailObj.ROLE_DN,
      };
      // this.loadingHandler.next(true);
      this.appService.getUsersForTeamRole(param).subscribe(
        (response) => {
          if (response && response.users && response.users.user) {
            const userList = acronui.findObjectsByProp(response, 'user');
            const users = [];
            if (userList && userList.length && userList.length > 0) {
              userList.forEach((user) => {
                const fieldObj = users.find((e) => e.value === user.cn);
                if (!fieldObj) {
                  users.push({
                    name: user.name,
                    value: user.cn,
                    displayName: user.name + ' (' + user.cn + ')',
                  });
                }
              });
            }
            this.overViewConfig.ownerFieldConfig.filterOptions = users;
            this.ownerFilterOptions = users;
          } else {
            this.overViewConfig.ownerFieldConfig.filterOptions = [];
            this.ownerFilterOptions = [];

            const eventData = {
              message: 'Manager role in the selected team is not available',
              type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
            };
            this.notificationHandler.next(eventData);
          }
          observer.next();
          observer.complete();
        },
        (error) => {
          // this.loadingHandler.next(false);
          const eventData = {
            message: 'Something went wrong while fetching Users',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          observer.next([]);
          observer.complete();
        }
      );
    });
  }

  getProjectOwnerDetails(userCN: string): Observable<any> {
    return new Observable((observer) => {
      if (!userCN) {
        observer.error();
      }
      const parameters = {
        userCN: userCN,
      };
      this.appService
        .getPersonByUserId(parameters)
        .subscribe((personDetails) => {
          this.projectOwnerDetails = personDetails;
          observer.next();
          observer.complete();
        });
    });
  }

  storePanelHTML(event) {
    this.panelHTML = event.srcElement || event.target;
  }

  handlePanelLeaveEvent(event) {
    if (
      this.monsterUtilService.checkElementEventPosition(this.panelHTML, event)
    ) {
      const updateProjObj: ProjectUpdateObj = this.createNewProjectUpdate();
      let ProjectObject = this.npdProjectService.compareTwoProjectDetails(
        updateProjObj,
        this.oldProjectMetadata
      );
      this.updateProject(ProjectObject);
    }
  }
  /*     test2(value) {
            const updateProjObj: ProjectUpdateObj = this.createNewProjectUpdate();
            let ProjectObject = this.npdProjectService.compareTwoProjectDetails(updateProjObj, this.oldProjectMetadata);
            this.loaderService.show();
            this.updateProject(ProjectObject);
        }

        test3(value){
            const updateProjObj: ProjectUpdateObj = this.createNewProjectUpdate();
            let projObj = this.npdProjectService.compareTwoProjectDetails(updateProjObj, this.oldProjectMetadata);
            let projectCustomObj = this.npdProjectService.allProjectFormProperties;
            let formProperties = this.monsterUtilService.allFormProperties;
            let formRelations = this.monsterUtilService.allFormRelations;

            if((Object.keys(projObj).length > 1) || Object.keys(projectCustomObj).length > 0 ||
            Object.keys(formProperties).length > 0 || Object.keys(formRelations).length > 0) {

                setTimeout(()=>{
                    this.loaderService.show();
                },1000);
                forkJoin([this.updateMPMProject(projObj),this.updateCustomProject(),this.updateRequestData()]).subscribe(response=> {
                    this.refreshProjectView(true,false);
                },error => {
                    this.refreshProjectView(false);
                });
            }
        }
      test(value) {
          this.loaderService.show();
          setTimeout(()=>{
            this.loaderService.hide();
            setTimeout(()=>{
                this.testLoader();
            },5000);
          },10000)
      }

      testLoader() {
        this.loaderService.show();
        setTimeout(()=>{
          this.loaderService.hide();
        },10000)
      } */

  getNewCustomFields(fieldsetGroup) {
    this.newNpdCustomFieldsGroup = this.formBuilder.group({
      fieldset: new FormArray(fieldsetGroup),
    });
  }

  updateProject(ProjectObject) {
    // if existing project
    if (!this.validateProject()) {
      //add submit validations
      return;
    }
    if (this.projectOverviewForm.controls.status.dirty) {
      //&& this.isStatusInitialUpdate
      this.loaderService.show();
      this.entityAppDefService
        .getStatusById(this.projectOverviewForm.getRawValue().status)
        .subscribe(
          (projectStatus) => {
            if (
              projectStatus.STATUS_TYPE ===
              ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ||
              projectStatus.TYPE === ProjectConstant.STATUS_TYPE_ONHOLD ||
              (this.oldStatusInfo &&
                this.oldStatusInfo.TYPE === 'ONHOLD' &&
                projectStatus.TYPE === ProjectConstant.STATUS_TYPE_IN_PROGRESS)
            ) {
              const confirmationMessage =
                projectStatus.STATUS_TYPE ===
                  ProjectConstant.STATUS_TYPE_FINAL_CANCELLED
                  ? 'Are you sure you want to cancel the project?'
                  : projectStatus.TYPE === ProjectConstant.STATUS_TYPE_ONHOLD
                    ? 'Are you sure you want to put the project onHold?'
                    : this.oldStatusInfo &&
                      this.oldStatusInfo.TYPE === 'ONHOLD' &&
                      projectStatus.TYPE ===
                      ProjectConstant.STATUS_TYPE_IN_PROGRESS
                      ? 'Are you sure you want to resume the project?'
                      : '';
              this.statusService
                .GetStatusReasonByStatusId(projectStatus['MPM_Status-id'].Id)
                .subscribe((statusReason) => {
                  this.loaderService.hide();
                  projectStatus['MPM_Status_Reason'] = statusReason;
                  const dialogRef = this.dialog.open(BulkCommentsComponent, {
                    width: '40%',
                    disableClose: true,
                    data: {
                      message: confirmationMessage,
                      hasConfirmationComment: true,
                      commentText: 'Comments',
                      isCommentRequired: true,
                      errorMessage: 'Kindly fill the comments field',
                      submitButton: 'Yes',
                      cancelButton: 'No',
                      statusData: projectStatus,
                      statusMessage: 'Project Status',
                    },
                  });
                  dialogRef.afterClosed().subscribe((result) => {
                    this.loaderService.show();
                    if (result && result !== 'true') {
                      setTimeout(() => {
                        this.loaderService.show();
                      }, 1);

                      let reason = '';
                      result.SelectedReasons.forEach(
                        (selectedReason, index) => {
                          const selectedStatusReason =
                            statusReason.MPM_Status_Reason.find(
                              (statusReason) =>
                                statusReason['MPM_Status_Reason-id'].Id ===
                                selectedReason.Id
                            );
                          reason = reason + selectedStatusReason.NAME;
                          if (index < result.SelectedReasons.length - 1) {
                            reason = reason + ',';
                          }
                        }
                      );
                      let newComment;
                      if (reason) {
                        newComment = reason + ' : ' + result.comment;
                      } else {
                        newComment = result.comment;
                      }
                      this.cancelComment = newComment;
                      const successMessage =
                        projectStatus.STATUS_TYPE ===
                          ProjectConstant.STATUS_TYPE_FINAL_CANCELLED
                          ? 'Project cancellation is in progress and it might take some time to complete'
                          : projectStatus.NAME
                            ? 'Project is moved to ' +
                            projectStatus.NAME +
                            ' state'
                            : '';
                      this.notificationService.success(successMessage);
                      this.disableProjectSave = true;
                      if (
                        this.projectOverviewForm.controls.name.value.trim() ===
                        ''
                      ) {
                        let eventData;
                        eventData = {
                          message: 'Please provide a valid project name',
                          type: ProjectConstant.NOTIFICATION_LABELS.INFO,
                        };
                        this.notificationHandler.next(eventData);
                        this.disableProjectSave = false;
                      } else {
                        this.projectOverviewForm.controls.name.setValue(
                          this.projectOverviewForm.controls.name.value.trim()
                        );
                        if (this.overViewConfig.selectedProject) {
                          // TODO: For Edit Project checkProjectOwnerChange
                          this.updateProjectDetails(ProjectObject, true);
                          //  this.loaderService.hide();
                        } else {
                          this.getProjectOwnerDetails(
                            this.projectOverviewForm.controls.owner.value.value
                          ).subscribe((success) => {
                            this.updateProjectDetails(ProjectObject, true);
                          });
                        }
                      }
                    } else {
                      this.loaderService.hide();
                    }
                  });
                });
            } else {
              this.loaderService.hide();
              this.disableProjectSave = true;
              if (this.projectOverviewForm.controls.name.value.trim() === '') {
                let eventData;
                eventData = {
                  message: 'Please provide a valid project name',
                  type: ProjectConstant.NOTIFICATION_LABELS.INFO,
                };
                this.notificationHandler.next(eventData);
                this.disableProjectSave = false;
              } else {
                this.projectOverviewForm.controls.name.setValue(
                  this.projectOverviewForm.controls.name.value.trim()
                );
                if (this.overViewConfig.selectedProject) {
                  // TODO: For Edit Project checkProjectOwnerChange
                  this.updateProjectDetails(ProjectObject, true);
                } else {
                  this.getProjectOwnerDetails(
                    this.projectOverviewForm.controls.owner.value.value
                  ).subscribe((success) => {
                    this.updateProjectDetails(ProjectObject, true);
                  });
                }
              }
            }
          },
          (projectStatusError) => {
            console.log(projectStatusError);
          }
        );
    } else {
      this.updateProjectDetails(ProjectObject, true);
      //this.updateProjectDetails(ProjectObject,false);
    }
  }

  updateProjectDetails(ProjectObject, isRefresh) {
    if (this.overViewConfig.selectedProject) {
      this.disableProjectSave = true;
      let projObj = this.npdProjectService.compareTwoProjectDetails(
        ProjectObject,
        this.oldProjectMetadata
      );
      let projectCustomObj = this.npdProjectService.allProjectFormProperties;
      let formProperties = this.monsterUtilService.allFormProperties;
      let formRelations = this.monsterUtilService.allFormRelations;

      if (
        Object.keys(projObj).length > 1 ||
        Object.keys(projectCustomObj).length > 0 ||
        Object.keys(formProperties).length > 0 ||
        Object.keys(formRelations).length > 0
      ) {
        //this.loaderService.show();
        setTimeout(() => {
          this.loaderService.show();
        }, 1);
        forkJoin([
          this.updateMPMProject(ProjectObject),
          this.updateCustomProject(),
          this.updateRequestData(),
        ]).subscribe(
          (response) => {
            this.refreshProjectView(true, isRefresh);
          },
          (error) => {
            this.refreshProjectView(false);
          }
        );
      }
    }
  }

  updateMPMProject(ProjectObject): Observable<Array<any>> {
    ProjectObject = this.npdProjectService.compareTwoProjectDetails(
      ProjectObject,
      this.oldProjectMetadata
    );
    return new Observable((observer) => {
      if (Object.keys(ProjectObject).length > 1) {
        ProjectObject.BulkOperation = false;
        if (ProjectObject?.DueDate && ProjectObject.DueDate != '') {
          [, ProjectObject.DueDate] = this.getStartEndDate(
            ProjectObject.DueDate
          );
        }
        this.projectService
          .updateProjectDetails(ProjectObject, this.cancelComment, this.reasons)
          .subscribe(
            (response) => {
              observer.next([]);
              observer.complete();
            },
            (error) => {
              observer.error();
            }
          );
      } else {
        this.disableProjectSave = false;
        observer.next([]);
        observer.complete();
      }
    });
  }

  updateCustomProject(): Observable<Array<any>> {
    this.disableProjectSave = true;
    let projectObj = this.npdProjectService.allProjectFormProperties;

    projectObj.view = 'Overview';

    projectObj.requestID = this.request.requestItemId?.split('.')[1];
    projectObj.projectItemId =
      this.overViewConfig.selectedProject['Project-id'].ItemId;
    let rollsArray = [
      'e2ePMItemId', 'corpPMItemId', 'opsPMItemId', 'regLeadItemId', 'commercialLeadItemId',
      'regionalTechnicalManagerItemId', 'projectSpecialistItemId', 'opsPlannerItemId', 'secondaryAWSpecialist',
      'gfgItemId','fpmsItemId'
    ];
    let projectTeamUpdate = [];
    for (let objKey in projectObj) {
      if (rollsArray.includes(objKey)) {
        projectTeamUpdate.push({ 'userID': projectObj[objKey].split('.')[1] })
      }
      projectObj.projectTeamUpdate = projectTeamUpdate;
    }
    return new Observable((observer) => {
      if (Object.keys(projectObj).length > 2) {
        this.npdProjectService
          .updateCustomNpdProjectDetails(projectObj)
          .subscribe(
            (response) => {
              this.npdProjectService.clearFormProperties();
              observer.next([]);
              observer.complete();
            },
            (error) => {
              observer.error();
            }
          );
      } else {
        this.npdProjectService.clearFormProperties();
        observer.next([]);
        observer.complete();
      }
    });
  }


  updateRequestData(): Observable<Array<any>> {
    return new Observable((observer) => {
      this.monsterUtilService.finalBulkUpdateFormProjectData(
        () => {
          observer.next([]);
          observer.complete();
        },
        () => {
          //error callback
          observer.error();
        }
      );
    });
  }

  createNewProjectUpdate(): ProjectUpdateObj {
    let ProjectObject: ProjectUpdateObj;
    const projectValue = this.projectOverviewForm.getRawValue();
    const projectType = ProjectConstant.PROJECT_TYPE_PROJECT;
    ProjectObject = this.npdProjectService.createNewProjectUpdateObject(
      projectValue,
      projectType,
      this.projectOwnerDetails.userId
    );
    ProjectObject.ProjectId.Id =
      this.overViewConfig.selectedProject?.['Project-id'].Id;
    //ProjectObject.CustomMetadata = this.projectFromTemplateService.getCustomMetadata(this.customFieldsGroup);
    return ProjectObject;
  }

  createNewNpdProjectUpdate(npdCustomFieldsGroup): NpdProjectUpdateObj {
    let npdCustomObj: NpdProjectUpdateObj;
    let obj;
    // npdCustomObj.projectItemId = this.overViewConfig.selectedProject['Project-id'].Id;
    // npdCustomObj.requestID = this.overViewConfig.selectedProject['Project-id'].Id;
    obj = this.npdProjectService.getCustomMetadata(npdCustomFieldsGroup);
    return obj;
  }

  /*    saveCustomProject(ProjectObject,successCallback?, errorCallback?) {
           //this.loaderService.show();
           this.disableProjectSave = true;
           //ProjectObject = this.npdProjectService.compareTwoCustomProjectDetails(ProjectObject, this.oldNpdCustomProjectdata);
           ProjectObject.requestID =  this.request.requestItemId?.split('.')[1];
           ProjectObject.projectItemId = this.overViewConfig.selectedProject['Project-id'].ItemId;
           this.npdProjectService.updateCustomNpdProjectDetails(ProjectObject).subscribe(response => {
                if (successCallback) { successCallback(); }
           }, error => {
                if (errorCallback) { errorCallback(); }
           });
       } */

  /*     saveCustomProject(ProjectObject,successCallback?, errorCallback?) {
            this.disableProjectSave = true;
            let projectObj = this.npdProjectService.allProjectFormProperties;
            projectObj.requestID =  this.request.requestItemId?.split('.')[1];
            projectObj.projectItemId = this.overViewConfig.selectedProject['Project-id'].ItemId;
            this.npdProjectService.updateCustomNpdProjectDetails(projectObj).subscribe(response => {
                 if (successCallback) {
                     this.npdProjectService.clearFormProperties();
                     successCallback(); }
            }, error => {
                 if (errorCallback) { errorCallback(); }
            });
        }
     */

  /*    saveProject(ProjectObject) {

           //this.loadingHandler.next(true);
           let standardStatusValue;
           if (!this.overViewConfig.selectedProject) {
               standardStatusValue = this.projectOverviewForm.value.standardStatus;
           }
           if (this.overViewConfig.selectedProject) {
               this.disableProjectSave = true;
               ProjectObject = this.npdProjectService.compareTwoProjectDetails(ProjectObject, this.oldProjectMetadata);
               const updateCustomProjObj: NpdProjectUpdateObj = this.createNewNpdProjectUpdate(this.npdCustomFieldsGroup);
               let customProjectObject = this.npdProjectService.compareTwoCustomProjectDetails(updateCustomProjObj, this.oldNpdCustomProjectdata);
               if (Object.keys(customProjectObject).length > 0 && Object.keys(ProjectObject).length > 1 ) {
                   this.loaderService.show();
                   this.saveCustomProject(customProjectObject,()=> {
                       ProjectObject.BulkOperation = false;
                       this.loaderService.show();
                       if (ProjectObject?.DueDate && ProjectObject.DueDate != '') {
                           [ , ProjectObject.DueDate] = this.getStartEndDate(ProjectObject.DueDate);
                       }
                       this.projectService.updateProjectDetails(ProjectObject, this.cancelComment, this.reasons).subscribe(response => {
                           this.refreshProjectView(true);
                       }, error => {
                           this.refreshProjectView(false);
                       });
                   },(error)=> {
                       this.refreshProjectView(false);
                   });
               }  else if (Object.keys(customProjectObject).length > 0) {
                   this.loaderService.show();
                   this.saveCustomProject(customProjectObject,()=> {
                       ProjectObject.BulkOperation = false;
                       this.refreshProjectView(true);
                   },(error)=> {
                       this.refreshProjectView(false);
                   });
               } else if (Object.keys(ProjectObject).length > 1) {
                   ProjectObject.BulkOperation = false;
                   this.loaderService.show();
                   if (ProjectObject?.DueDate && ProjectObject.DueDate != '') {
                       [ , ProjectObject.DueDate] = this.getStartEndDate(ProjectObject.DueDate);
                   }
                   this.projectService.updateProjectDetails(ProjectObject, this.cancelComment, this.reasons).subscribe(response => {
                       this.refreshProjectView(true);
                   }, error => {
                       this.refreshProjectView(false);
                   });
               }
               else {
                   this.loadingHandler.next(false);
                   this.loaderService.hide();
                   this.disableProjectSave = false;
               }
           }
       }
    */
  refreshProjectView(isSave, isRefresh?, isRefreshTimeout?) {
    if (isSave) {
      this.loaderService.hide();
      const eventData = {
        message: this.overViewConfig.formType + ' changes updated successfully',
        type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS,
      };
      this.notificationService.success(eventData.message);
      isRefresh && this.ngOnInit();
      /* if(isRefresh) {
                if(isRefreshTimeout) {
                    setTimeout(() => {
                        this.ngOnInit();
                    },3000)
                } else {
                    this.ngOnInit();
                }
            } */
    } else {
      this.loaderService.hide();
      const eventData = {
        message: 'Something went wrong while updating project',
        type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
      };
      this.notificationService.error(eventData.message);
    }
  }

  refreshProject() {
    this.ngOnInit();
  }

  handleProjectNotification(
    response: any,
    eventData: any,
    isCreation: boolean,
    isError: boolean
  ): void {
    this.loadingHandler.next(false);
    if (!isError) {
      if (response && response.APIResponse) {
        if (response.APIResponse.statusCode === '500') {
          eventData = {
            message: response.APIResponse.error.errorMessage,
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.disableProjectSave = false;
          this.notificationHandler.next(eventData);
        } else if (
          response.APIResponse.statusCode === '200' &&
          ((response.APIResponse.data &&
            response.APIResponse.data.Project &&
            response.APIResponse.data.Project['Project-id'] &&
            response.APIResponse.data.Project['Project-id'].Id) ||
            !isCreation ||
            (response.APIResponse.data &&
              response.APIResponse.data.Campaign &&
              response.APIResponse.data.Campaign['Campaign-id'] &&
              response.APIResponse.data.Campaign['Campaign-id'].Id))
        ) {
          this.notificationHandler.next(eventData);
          if (isCreation) {
            this.projectOverviewForm.reset();
            //this.customFieldsGroup?.reset();
            if (response.APIResponse.data.Project) {
              this.saveCallBackHandler.next({
                projectId: response.APIResponse.data.Project['Project-id'].Id,
                isTemplate: this.overViewConfig.isTemplate ? true : false,
              });
            } else {
              this.saveCallBackHandler.next({
                campaignId:
                  response.APIResponse.data.Campaign['Campaign-id'].Id,
              });
            }
          } else {
            this.ngOnInit();
          }
        }
      }
    } else {
      this.disableProjectSave = false;
      this.loadingHandler.next(false);
      this.notificationHandler.next(eventData);
    }
  }

  getFields() {
    this.allProjectFieldsConfig =
      this.fieldConfigService.getFieldsbyFeildCategoryLevel(
        ProjectConstant.CATEGORY_LEVEL_PROJECT
      );
    ///this.allRequestFieldsConfig = this.fieldConfigService.getFieldsbyFeildCategoryLevel(ProjectConstant.CATEGORY_LEVEL_PROJECT);
    //filter custom fields alone out of them
  }

  getFieldConfig(mapperName, isRequest?) {
    if (isRequest) {
      /*  let requestConfig = this.allRequestFieldsConfig.find(requestConfig =>
                 requestConfig.MAPPER_NAME === mapperName
             ); */
      return this.requestData?.[mapperName];
    } else {
      let projectRequestConfig = this.allProjectFieldsConfig.find(
        (projectConfig) => projectConfig.MAPPER_NAME === mapperName
      );
      if (projectRequestConfig?.INDEXER_FIELD_ID) {
        return this.projectRequestConfigDetails?.[
          projectRequestConfig.INDEXER_FIELD_ID
        ];
        //?this.projectRequestConfigDetails[projectRequestConfig.INDEXER_FIELD_ID] :'' ;
      }
    }

    return '';
  }

  getReportingProjectType() {
    return new Observable((observer) => {
      this.monsterConfigApiService.getAllReportingProjectType().subscribe(
        (response) => {
          this.allListConfig['reportingProjectType'] =
            this.monsterConfigApiService.transformResponse(
              response,
              'CP_Project_Type-id',
              true
            );
          observer.next(true);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getRequestIndexerDetailsById(): Observable<any> {
    return new Observable((observer) => {
      this.npdProjectService
        .getRequestDetailsById(this.request?.requestItemId)
        .subscribe(
          (reqResponse) => {
            if (reqResponse?.length > 0) {
              this.requestData = reqResponse[0];
              this.request.requestBusinessId =
                reqResponse[0]?.[
                OverViewConstants.REQUEST_INDEXER_CONFIGS.requestBusinessId
                ];
            }
            observer.next([]);
            observer.complete();
          },
          (error) => {
            observer.error();
          }
        );
    });
  }

  getProjectIndexerDetailsById(): Observable<any> {
    this.getFields();
    return new Observable((observer) => {
      this.npdProjectService.getProjectDetailsById(this.projectId).subscribe(
        (projResponse) => {
          if (projResponse?.length > 0) {
            // this.classificationNumber = this.npdProjectService.getProjectClassificationNumber(projResponse[0]);
            this.projectRequestConfigDetails = projResponse[0];
            this.isBulkEdit =
              this.projectRequestConfigDetails?.NPD_PROJECT_IS_BULK_EDIT &&
              (this.projectRequestConfigDetails?.NPD_PROJECT_IS_BULK_EDIT ==
                true ||
                this.projectRequestConfigDetails?.NPD_PROJECT_IS_BULK_EDIT ==
                'true');
            this.request.requestId = this.getFieldConfig(
              this.overViewConstants.PROJECT_REQUEST.REQUEST_ID
            );
            this.request.requestType = this.getFieldConfig(
              this.overViewConstants.PROJECT_REQUEST.REQUEST_TYPE
            );
            this.request.requestItemId = this.getFieldConfig(
              this.overViewConstants.PROJECT_REQUEST.REQUEST_ITEM_ID
            );
            this.CheckCurrentTimelineCondition();
            this.getRequestIndexerDetailsById().subscribe(
              (response) => {
                observer.next([]);
                observer.complete();
              },
              (error) => {
                observer.error();
              }
            );
          } else {
            observer.next([]);
            observer.complete();
          }
        },
        (error) => {
          observer.error();
        }
      );
    });
  }

  fetchBooleanValue(value) {
    if (value == true) {
      return 'Yes';
    } else if (value == false) {
      return 'No';
    } else {
      return 'NA';
    }
  }

  showEllipses(array, index) {
    if (array === 'RequestOverview') {
      if (!this.requestOverviewEllipses[index]) {
        this.requestOverviewEllipses[index] = {};
      }
      this.requestOverviewEllipses[index]['ellipses'] =
        !this.requestOverviewEllipses[index]['ellipses'];
    } else {
      if (!this.projectCoreData[index]) {
        this.projectCoreData[index] = {};
      }
      this.projectCoreData[index]['ellipses'] =
        !this.projectCoreData[index]['ellipses'];
    }
  }

  onNpdCustomMetadataFieldChange(formValue, group) {
    this.projectOverviewForm.get(group)['controls'].forEach((formGroup) => {
      formGroup['controls']['fieldset'].updateValueAndValidity();

      this.npdProjectService.setCustomFormProperty(
        formValue?.fieldset?.id,
        formValue.value
      );
      if (formValue?.fieldset?.isDynamic) {
        if (formValue?.fieldset?.isUser) {
          let selectedUser = formValue?.fieldset;
          let nameValue = formValue.fieldset?.values.find(
            (lookUpValue) => lookUpValue.value == formValue.value
          );
          this.loaderService.show();
          this.requestService.getUserByID(nameValue?.name).subscribe(
            (response) => {
              selectedUser.userIdentity = response.result.items[0];
              this.loaderService.hide();
              let nameProperty = formValue.fieldset?.name_id;
              let value = selectedUser?.userIdentity.Identity.ItemId;
              this.npdProjectService.setCustomFormProperty(
                nameProperty,
                nameValue?.displayName
              );
              this.npdProjectService.setCustomFormProperty(
                formValue.fieldset?.id,
                value
              );
            },
            (error) => {
              this.loaderService.hide();
            }
          );
        } else if (formValue?.fieldset?.isProgramTag) {
          let pgt;
          const x = formValue?.fieldset?.values.find((a) => {
            if (a.Name === formValue.value) {
              pgt = a?.['Program_Tag-id'].ItemId;
            }
          });
          this.npdProjectService.setCustomFormProperty(
            formValue?.fieldset?.id,
            pgt
          );
          this.npdProjectService.setCustomFormProperty(
            formValue?.fieldset?.name_id,
            formValue.value
          );
        } else {
          let nameValue = formValue.fieldset?.values.find(
            (lookUpValue) => lookUpValue.value == formValue.value
          );
          let nameProperty = formValue.fieldset?.name_id;
          if (nameValue?.displayName && nameProperty) {
            this.npdProjectService.setCustomFormProperty(
              nameProperty,
              nameValue.displayName
            );
          }
        }
      }
    });
    if (formValue?.fieldset?.Mapper_Name == 'NPD_PROJECT_SITE_AUDIT_REQUIRED') {
      let siteReadinessFormGroup = this.projectOverviewForm
        .get(group)
      ['controls'].find((formGroup, index) => {
        return formGroup.name == 'Site Readiness';
      });
      siteReadinessFormGroup?.controls?.fieldset?.controls?.forEach((val) => {
        if (val.fieldset.enableHideOption == true) {
        
        }
      });
    }
  }

  //   updateRequestRelationsIn(formControl,formName,objValue?) {
  //     let relationName;
  //     let objectValue;
  //     let itemId;

  //     if(formName === 'projectClassificationDataForm') {
  //       relationName = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
  //       objectValue = objValue? objValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value['Program_Tag-id'].ItemId
  //       itemId = this.requestDetails && this.requestDetails.requestItemId;

  //       if(formControl === ('earlyProjectClassification' || 'earlyProjectClassificationDesc') ) {
  //         objectValue = this.selectedProjectClassification && this.selectedProjectClassification.ItemId
  //       }
  //     }

  //     this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
  //   }

  getBool(date) {
    this.show[date] = !this.show[date];
  }

  changeTargetDP(weekFormat, date) {
    this.weekYear[date] = weekFormat;
    if (date == 'startDate') {
      this.projectOverviewForm.patchValue({ startDate: weekFormat });
    } else {
      this.projectOverviewForm.patchValue({ [date]: weekFormat });
      this.projectOverviewForm.controls[date].updateValueAndValidity();
    }
    //this.projectOverviewForm.markAsDirty();
    [this.fromDate[date], this.toDate[date]] = this.getWeekFormat(weekFormat);
  }

  getWeekFormat(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates: NgbDate[] = this.calculateDateNgb(week, year);
      return [weekdates[0], weekdates[1]];
    }
  }

  getStartEndDate(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates = this.calculateDate(week, year);
      return [weekdates[0], weekdates[1]];
    }
  }

  calculateDateNgb(week: number, year: number): NgbDate[] {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate, ngbToDate];
  }
  calculateDate(week: number, year: number) {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate() + 1
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    return [fromDate, toDate];
  }

  getAllBool() {
    Object.keys(this.show).map((ele) => {
      this.show[ele] = false;
    });
    this.canSupplierComponent?.getAllBool();
  }

  getStopPropogation(event, date) {
    if (this.show[date]) {
      return event.stopPropagation();
    }
  }

  calculateWeek(date) {
    //: Date
    const time = new Date(date).getTime() + 4 * 24 * 60 * 60 * 1000;
    const firstDay = new Date(new Date(date).getFullYear() + '/1/1'); //NPD1-83 safari issue;
    return (
      Math.floor(Math.round((time - firstDay.getTime()) / 86400000) / 7) + 1
    );
  }

  checkValueIsNullOrEmpty(data: any, defaultValue: any): any {
    if (
      !data ||
      data['@xsi:nil'] === 'true' ||
      data === '' ||
      data['@nil'] === 'true'
    ) {
      return defaultValue;
    }
    return data;
  }

  restartProject() {
    this.statusService.getAllStatusReasons().subscribe((response) => {
      const noteMessage =
        (this.projectOverviewForm && this.projectOverviewForm.dirty) ||
          (this.customFieldsGroup && this.customFieldsGroup.dirty)
          ? 'On restarting project, changes will be discarded.'
          : null;
      const projectStatus = {
        //REQUIRE_REASON: 'true',
        //REQUIRE_COMMENTS: 'true',
        MPM_Status_Reason: '',
      };
      projectStatus.MPM_Status_Reason = response;
      const dialogRef = this.dialog.open(BulkCommentsComponent, {
        width: '40%',
        disableClose: true,
        data: {
          message: 'Restart project',
          hasConfirmationComment: true,
          commentText: 'Comments',
          isCommentRequired: true,
          errorMessage: 'Kindly fill the comments field',
          submitButton: 'Yes',
          cancelButton: 'No',
          statusData: projectStatus,
          status: '',
          note: noteMessage,
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result && result !== 'true') {
          let reason = '';
          result.SelectedReasons.forEach((selectedReason, index) => {
            const selectedStatusReason = response.MPM_Status_Reason.find(
              (statusReason) =>
                statusReason['MPM_Status_Reason-id'].Id === selectedReason.Id
            );
            reason = reason + selectedStatusReason.NAME;
            if (index < result.SelectedReasons.length - 1) {
              reason = reason + ',';
            }
          });
          let newComment;
          if (reason) {
            newComment = reason + ' : ' + result.comment;
          } else {
            newComment = result.comment;
          }
          this.triggerRestartProject(newComment);
        }
      });
    });
  }

  triggerRestartProject(newComment) {
    const comment = newComment;
    const selectedPriority = this.overViewConfig.priorityOptions.find(
      (priority) =>
        priority['MPM_Priority-id'].Id ===
        this.overViewConfig.selectedProject.R_PO_PRIORITY['MPM_Priority-id'].Id
    );
    const selectedTeam = this.overViewConfig.teamsOptions.find(
      (team) =>
        team['MPM_Teams-id'].Id ===
        this.overViewConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id
    );
    const intermediateStatus = this.sharingService
      .getAllStatusConfig()
      .find((status) => status.STATUS_TYPE === StatusTypes.INTERMEDIATE);
    const currentUserObject = this.sharingService.getCurrentUserObject();
    const projectMetaData = {
      PROJECT_IS_ACTIVE: this.overViewConfig.selectedProject.IS_ACTIVE,
      PROJECT_IS_DELETED: false,
      PROJECT_CATEGORY: this.sharingService.selectedCategory.CATEGORY_NAME,
      PROJECT_DESCRIPTION: this.utilService.isNullOrEmpty(
        this.overViewConfig.selectedProject.DESCRIPTION
      ),
      PROJECT_DUE_DATE: this.overViewConfig.selectedProject.DUE_DATE,
      PROJECT_END_DATE: this.overViewConfig.selectedProject.END_DATE,
      PROJECT_NAME: this.overViewConfig.selectedProject.PROJECT_NAME,
      PROJECT_OWNER_NAME: currentUserObject.UserDisplayName,
      PROJECT_PRIORITY: selectedPriority.DESCRIPTION,
      PROJECT_START_DATE: this.overViewConfig.selectedProject.START_DATE,
      PROJECT_STATUS: intermediateStatus.NAME,
      PROJECT_TEAM: selectedTeam.NAME,
      DATA_TYPE: OTMMMPMDataTypes.PROJECT,
      PROJECT_ITEM_ID: this.overViewConfig.selectedProject['Project-id'].ItemId,
      PROJECT_PROGRESS: 0,
      PROJECT_TYPE: this.overViewConfig.selectedProject.PROJECT_TYPE,
      IS_STANDARD_STATUS: this.utilService.isNullOrEmpty(
        this.overViewConfig.selectedProject.IS_STANDARD_STATUS
      ),
      STATUS_ID: intermediateStatus['MPM_Status-id'].Id,
      PROJECT_STATUS_TYPE: intermediateStatus.STATUS_TYPE,
      PROJECT_ID: this.overViewConfig.selectedProject['Project-id'].Id,
      ORIGINAL_PROJECT_START_DATE:
        this.overViewConfig.selectedProject.START_DATE,
      ORIGINAL_PROJECT_DUE_DATE: this.overViewConfig.selectedProject.DUE_DATE,
      EXPECTED_PROJECT_DURATION: this.utilService.isNullOrEmpty(
        this.overViewConfig.selectedProject.EXPECTED_DURATION
      ),
      PROJECT_TIME_SPENT: 0,
      PROJECT_CAMPAIGN_ID:
        this.overViewConfig?.selectedProject?.R_PO_CAMPAIGN &&
        this.overViewConfig.selectedProject.R_PO_CAMPAIGN['Campaign-id'].Id,
    };
    const customMetadata = this.projectFromTemplateService.getCustomMetadata(
      this.customFieldsGroup
    );
    this.projectService
      .restartProject(projectMetaData, customMetadata, comment)
      .subscribe((response) => {
        if (response) {
          this.notificationService.info(
            'Restart project initiated successfully'
          );
        }
      });
  }

  hideVisibilityState(section) {
    if (section == 'opsPMPanel') {
      this.opsPMPanel?.accordion?.closeAll();
    } else if (section == 'e2ePMPanel') {
      this.e2ePMPanel?.accordion?.closeAll();
    } else if (section == 'technicalPanel') {
      this.technicalPanel?.accordion?.closeAll();
    } else if (section == 'secondaryPackaging') {
      this.secondaryPackaging?.accordion?.closeAll();
    } else if (section == 'regulatory') {
      this.regulatory?.accordion?.closeAll();
    } else if (section == 'projCoreData') {
      this.projCoreData?.accordion?.closeAll();
    } else if (section == 'pmPanel') {
      this.pmPanel?.accordion?.closeAll();
    } else if (section == 'projClassificationPanel') {
      this.projClassificationPanel?.accordion?.closeAll();
    } else if (section == 'reqOverviewPanel') {
      this.reqOverviewPanel?.accordion?.closeAll();
    }
  }

  changeVisibilityState(section) {
    this?.[section]?.accordion?.openAll();
  }

  refreshCallBack(event) {
    this.ngOnInit();
  }

  closeAllWeekPicker(item) {
    const projectValue = this.projectOverviewForm.getRawValue();
    for (let [
      j,
      formGroup,
    ] of projectValue.npdCustomE2EPMFieldGroup.entries()) {
      for (let [i, fields] of formGroup['fieldset'].entries()) {
        this.weePickers[j][i] = {};
        if (item.flag) {
          if (i == item.index) {
            this.weePickers[j][i].startDate = true;
          } else {
            this.weePickers[j][i].startDate = false;
          }
        } else {
          this.weePickers[j][i].startDate = false;
        }
      }
    }
  }

  ngOnInit(): void {
    if (navigator.language !== undefined) {
      this.adapter.setLocale(navigator.language);
    }
    this.monsterConfigApiService
      .getAllRolesForCurrentUser()
      .subscribe((response) => {
        const admin = response.find(
          (eachRole) =>
            eachRole['@id'].substring(3, eachRole['@id'].indexOf(',')) ===
            MONSTER_ROLES.NPD_ADMIN
        );
        this.isAdmin = admin != null ? true : false;
        const projectConfig = this.sharingService.getProjectConfig();
        this.appConfig = this.sharingService.getAppConfig();
        this.nameStringPattern = this.utilService.isNullOrEmpty(
          this.appConfig[
          ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN
          ]
        )
          ? '.*'
          : this.appConfig[
          ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN
          ];
        this.enableWorkWeek = this.utilService.getBooleanValue(
          projectConfig[
          ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_WORK_WEEK
          ]
        );
        this.enableCustomWorkflow = this.utilService.getBooleanValue(
          this.appConfig[
          ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CUSTOM_WORKFLOW
          ]
        );
        this.enableProjectRestartConfig = this.utilService.getBooleanValue(
          this.appConfig[
          ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_PROJECT_RESTART
          ]
        );
        if (this.projectId) {
          //this.projectService.getAllCategoryMetadata()
          this.categoryLevel = ProjectConstant.CATEGORY_LEVEL_PROJECT;
          this.refreshView();
          //this.getProjectIndexerDetailsById();
        }
      });
  }

  CheckCurrentTimelineCondition() {
    const governDeliveryWeek =
      this.projectRequestConfigDetails.NPD_PROJECT_GOVERNED_DELIVERY_DATE;
    const currentTimelineWeek =
      this.projectRequestConfigDetails.NPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK;
    this.backgroundCondition = ''

    if ((currentTimelineWeek && currentTimelineWeek !== 'NA') && (governDeliveryWeek && governDeliveryWeek !== 'NA')) {

      let week1 = governDeliveryWeek.split('/')[0];
      let year1 = governDeliveryWeek.split('/')[1];
      let week2 = currentTimelineWeek.split('/')[0];
      let year2 = currentTimelineWeek.split('/')[1];

      const startDate1 = new Date(year1, 0, 1);
      startDate1.setDate(startDate1.getDate() + (week1 - 1) * 7);
      const endDate1 = new Date(startDate1.getTime() + 6 * 24 * 60 * 60 * 1000);
      const endMilli1 = endDate1.getTime() - endDate1.getTimezoneOffset() * 60000

      const startDate2 = new Date(year2, 0, 1);
      startDate2.setDate(startDate2.getDate() + (week2 - 1) * 7);
      const endDate2 = new Date(startDate2.getTime() + 6 * 24 * 60 * 60 * 1000);

      const endMilli2 = endDate2.getTime() - endDate2.getTimezoneOffset() * 60000;

      const diffMillis = endMilli1 - endMilli2;


      // Convert the difference in milliseconds to weeks
      let diffWeeks = Math.floor(diffMillis / (7 * 24 * 60 * 60 * 1000));

      if (diffWeeks < 0) {
        this.backgroundCondition = 'red'
      } else if (diffWeeks > 6) {
        this.backgroundCondition = 'green'
      } else if (0 <= diffWeeks && diffWeeks <= 6) {
        this.backgroundCondition = 'amber'
      }
    }
    return this.backgroundCondition
  }

  // weekDifference(governDeliveryEndDate, currentTimelineEndDate) {
  //   const totalSecDiff =
  //     (governDeliveryEndDate - currentTimelineEndDate) / 1000;
  //   const daysDiff = Math.floor(totalSecDiff / (60 * 60 * 24));
  //   return daysDiff / 7;
  // }

  ngOnDestroy() {
    this.npdProjectService.clearFormProperties();
    this.monsterUtilService.clearFormPropertiesandRelations();
  }

  isIncludeUser(user,roles){
    return roles.includes(user);
  }
}

/*      @ViewChild('parentpanel') parentpanel :ElementRef;

 @ViewChild('mapanel') mapanel//:QueryList<[]>//: ElementRef;
  @ViewChild('accordion',{static:true}) accordion: MatAccordion
this.overViewConstants.ProjectCoreData.GroupArray.forEach(requestconfig => {
              let projectRequestField = this.allProjectFieldsConfig.find(projectConfig =>
                  projectConfig.MAPPER_NAME === requestconfig.Mapper_Name
              );
              if(projectRequestField?.INDEXER_FIELD_ID) {
                 requestconfig.Value =  this.projectRequestConfigDetails[projectRequestField.INDEXER_FIELD_ID]?
                      (this.projectRequestConfigDetails[projectRequestField.INDEXER_FIELD_ID]).toString() :'NA' ;

              } else {
                  requestconfig.Value = 'NA'
              }
          });
          this.overViewConstants.RequestOverview.GroupArray.forEach(requestconfig => {

              if(requestconfig.dataType == 'string') {
                  requestconfig.Value =  this.projectRequestConfigDetails[requestconfig.Indexer_field]?
                  (this.projectRequestConfigDetails[requestconfig.Indexer_field]).toString() :'NA' ;
              } else {
                  requestconfig.Value =  this.projectRequestConfigDetails[requestconfig.Indexer_field]?
                  this.projectRequestConfigDetails[requestconfig.Indexer_field] :'NA' ;
              }
          });
          this.overViewConstants.ProjectCoreData.GroupArray.forEach(requestconfig => {
              requestconfig.Value =  this.projectRequestConfigDetails[requestconfig.Indexer_field]?
              this.projectRequestConfigDetails[requestconfig.Indexer_field] :'NA' ;
          });*/
/*   let projectRequestField = this.allProjectFieldsConfig.find(projectConfig =>
        projectConfig.MAPPER_NAME === requestconfig.Mapper_Name
    ); */
/*  if(projectRequestField?.INDEXER_FIELD_ID) {
     if(requestconfig.dataType == 'string') {
         requestconfig.Value =  this.projectRequestConfigDetails[projectRequestField.INDEXER_FIELD_ID]?
         (this.projectRequestConfigDetails[projectRequestField.INDEXER_FIELD_ID]).toString() :'NA' ;
     } else {
         requestconfig.Value = this.projectRequestConfigDetails[projectRequestField.INDEXER_FIELD_ID]?
          this.projectRequestConfigDetails[projectRequestField.INDEXER_FIELD_ID] :'NA' ;
     }
 } else {
     requestconfig.Value = 'NA'
 }

/*                                 //if (this.projectOverviewForm.status === 'VALID') {
} else {
                     const eventData = { message: 'Kindly fill all mandatory fields', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                     this.notificationHandler.next(eventData);
                     this.disableProjectSave = false;
                     this.loadingHandler.next(false);
                     this.loaderService.hide();
                 } */
