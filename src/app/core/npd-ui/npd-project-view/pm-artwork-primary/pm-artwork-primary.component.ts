import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { LoaderService, NotificationService, SharingService, UtilService } from 'mpm-library';
import { EntityListConstant } from '../../request-view/constants/EntityListConstant';
import { EntityService } from '../../services/entity.service';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { PmSecondaryPackagingComponent } from '../pm-secondary-packaging/pm-secondary-packaging.component';
import { NpdProjectService } from '../services/npd-project.service';
/**
 * @author JeevaR
 */
@Component({
  selector: 'app-pm-artwork-primary',
  templateUrl: './pm-artwork-primary.component.html',
  styleUrls: ['./pm-artwork-primary.component.scss']
})
export class PmArtworkPrimaryComponent extends PmSecondaryPackagingComponent implements OnInit {

  constructor(public loaderService:LoaderService,public entityService: EntityService,public npdProjectService:NpdProjectService,
    public monsterUtilService: MonsterUtilService,public formBuilder: FormBuilder,public adapter: DateAdapter<any>,
    public notificationService:NotificationService,public utilService: UtilService,public monsterConfigService:MonsterConfigApiService,public sharingService:SharingService) {
    super(loaderService,entityService,npdProjectService,monsterUtilService,formBuilder,adapter,notificationService,utilService,monsterConfigService,sharingService);
  }

     @Input() projectEntityItemId;
     @Input() secondaryPackagingConfig;
     renderSPTableRows() {
      this.secondaryPackagingTable && this.secondaryPackagingTable.renderRows ?
        this.secondaryPackagingTable.renderRows() : console.log('No rows in Secondary Packaging.');
  }

  bulkCreateSP() {
    const updateData = [];
    let temp = {};
    for (let i = 0; i < 1; i++) {
        temp = {
          operationType: 'Create',
          parentItemId: this.projectEntityItemId,
          relationName: 'R_PM_SECONDARY_PACK',
          template: null,
          item: {
            Properties: {
              IsArtwork:true
            },
          }
        };
        updateData.push(temp);
    }
    this.loaderService.show();
    this.entityService.bulkCreateEntitywithParent(updateData, null, null).subscribe((response) => {
      const items = response.changeLog.changeEventInfos;
      items.forEach((data) => {
        if (data.changeType !== 'Created') {
          return;
        }
        this.secondaryPackagingFormArray.push(this.formBuilder.group({
          mpmJobNo: [],
          mpmGroup: [],
          isArtwork:[true]
        }));
      });
      this.renderSPTableRows();
      this.refresh()
      this.notificationService.success('Artwork Primary added successfully.');
      this.loaderService.hide();
    }, () => {
      this.loaderService.hide();
      this.notificationService.error('Error while creating Artwork Primary rows.');
    });
  }

 getAllSecondaryPackagingData() {
  // this.loaderService.show();
  this.loadingSecondaryPackaging = true;
  this.entityService.getEntityItemDetails(this.projectEntityItemId,
    EntityListConstant.SECONDARY_PACKAGING_RELATION).subscribe((secondaryPackagingResponse) => {
      if (secondaryPackagingResponse && secondaryPackagingResponse.items) {
        this.mapSecondaryPackagingContentToForm(secondaryPackagingResponse.items.filter(item=>{ return item.Properties.IsArtwork===true }));
        // this.loaderService.hide();
      }
      this.loadingSecondaryPackaging = false;
    }, (error) => {
      // this.loaderService.hide();
      this.loadingSecondaryPackaging = false;
    });
  }
  mapSecondaryPackagingContentToForm(secondaryPackaging) {
    this.packFormats = this.listConfig?.secondaryPackLeadTime;
    this.printSuppliers = this.listConfig?.printSuppliers;
    this.dynamicSecondaryPackagingForm = this.formBuilder.group({
      secondaryPackaging: new FormArray([]),
      // noOfPiecesRequired : new FormControl({ value: this.projectData?.[OverViewConstants.OVERVIEW_MAPPINGS.SECONDARY_PACKAGING.noOfPiecesRequired],disabled:true}),//NPD_PROJECT_NO_OF_PIECES_REQUIRED
    });
    this.spGridColumns =['select','mpmJobNo','mpmGroup','actions'] 
    this.showSecondaryPackagingForm = true;
    for (let i = 0; i < secondaryPackaging.length; i++) {
           this.secondaryPackagingFormArray.push(this.formBuilder.group({
        regsNeeded: [{value:secondaryPackaging[i]?.Properties?.RegsNeeded,disabled: this.secondaryPackagingConfig.regsNeeded.disabled }],
        mpmJobNo: [{value:secondaryPackaging[i]?.Properties?.MPMJobNo,disabled: this.secondaryPackagingConfig.mpmJobNo.disabled },[Validators.maxLength(64)]],
        mpmGroup: [{value:secondaryPackaging[i]?.Properties?.SubRequest,disabled: this.secondaryPackagingConfig.mpmGroup.disabled },[Validators.maxLength(64)]],
        packFormat: [{value: secondaryPackaging[i]?.R_PO_PACK_FORMAT?.Identity?.ItemId,disabled: this.secondaryPackagingConfig.packFormat.disabled }],
        printSupplier: [{value: secondaryPackaging[i]?.R_PO_PRINT_SUPPLIER?.Identity?.ItemId,disabled: this.secondaryPackagingConfig.printSupplier.disabled }],
        approvedDieLineAvailable: [{value:secondaryPackaging[i]?.Properties?.ApprovedDieLineAvailable,disabled: this.secondaryPackagingConfig.approvedDieLineAvailable.disabled }],
        isArtwork: [{value:secondaryPackaging[i]?.Properties?.isArtwork}],

        itemNumber: [{value:secondaryPackaging[i]?.Properties?.ItemNumber,disabled: this.secondaryPackagingConfig.itemNumber.disabled },[Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/)]],
        colourApproval: [{value:secondaryPackaging[i]?.Properties?.ColourApproval,disabled: this.secondaryPackagingConfig.colourApproval.disabled }],
        invoiceApproved: [{value:secondaryPackaging[i]?.Properties?.InvoiceApproved,disabled: this.secondaryPackagingConfig.invoiceApproved.disabled }],
        statusComments: [{value:secondaryPackaging[i]?.Properties?.StatusComments,disabled: this.secondaryPackagingConfig.statusComments.disabled },[Validators.maxLength(2000)]],
        dateDevelopmentCompleted: [{value:secondaryPackaging[i]?.Properties?.DateDevelopmentCompleted,disabled: this.secondaryPackagingConfig.dateDevelopmentCompleted.disabled }],

        dateProofingCompleted: [{value:secondaryPackaging[i]?.Properties?.DateProofingCompleted,disabled: this.secondaryPackagingConfig.dateProofingCompleted.disabled }],
        itemId: [secondaryPackaging[i].Identity.ItemId],
      }));
      this.renderSPTableRows();
    }
  }
  deleteSecondaryPackagingGroup(index?) {
    const formArray = this.dynamicSecondaryPackagingForm.get('secondaryPackaging') as FormArray;
    const deleteItem = formArray.controls[index].value.itemId;
    this.loaderService.show();
    this.entityService.deleteEntityObjects(deleteItem).subscribe(response => {
      this.monsterUtilService.removeFormProperty(deleteItem);
      this.monsterUtilService.removeFormRelations(deleteItem);
      this.secondaryPackagingFormArray.removeAt(index);
      this.loaderService.hide();
      this.renderSPTableRows();
      this.refresh()
      this.notificationService.success('Artwork Primary deleted successfully.');
    }, error => {
      this.loaderService.hide();
      this.notificationService.error('Something went wrong while deleting.');
    }); 
}
bulkDeleteSP() {
  if (this.dynamicSecondaryPackagingForm.value.secondaryPackaging.length === 0) {
    this.notificationService.error('Add at Least one Artwork Primary.');
    return;
  }
  if (this.selection.selected.length < 1) {
    this.notificationService.error('Select Artwork Primary(s) to delete.');
    return;
  }
  this.loaderService.show();
  const formArray = this.dynamicSecondaryPackagingForm.get('secondaryPackaging') as FormArray;
  const itemIds = [];
  for (let i = 0; i < this.selection.selected.length; i++) {
    itemIds.push(this.selection.selected[i].value.itemId);
  }
  const formRowsLenght = formArray.controls.length;
  this.entityService.deleteMultipleEntityObjects(itemIds, EntityListConstant.SECONDARY_PACKAGING, this.utilService.APP_ID).subscribe(() => {
    for (let i = formRowsLenght - 1; i > -1; i--) {
      if (itemIds.indexOf(formArray.controls[i].value.itemId) !== -1) {
        this.monsterUtilService.removeFormProperty(formArray.controls[i].value.itemId);
        this.monsterUtilService.removeFormRelations(formArray.controls[i].value.itemId);
        this.secondaryPackagingFormArray.removeAt(i);
      }
    }
    this.selection = new SelectionModel<Element>(true, []);
    this.renderSPTableRows();
    // this.dynamicSecondaryPackagingForm.patchValue({noOfPiecesRequired: this.secondaryPackagingFormArray.controls.length});
    // this.npdProjectService.setCustomFormProperty(OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.SECONDARY_PACKAGING.noOfPiecesRequired,this.secondaryPackagingFormArray.controls.length) 
    this.loaderService.hide();
    this.notificationService.success('Successfully completed the deletion process.');
    this.refresh()
  }, () => {
    this.notificationService.error('Error while deleting the Artwork Primary.');
  }); 
}
updateFormControlsDisability() {
  for(let i = 0; i < this.secondaryPackagingFormArray.length ;i++ ) {
    this.monsterUtilService.disableFormControls(this.secondaryPackagingFormArray.controls[i],this.secondaryPackagingConfig);
  }
}
refresh(){
  this.ngOnInit()
}

  ngOnInit() {
   super.ngOnInit()
  }


}
