import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService, AssetService, ConfirmationModalComponent, EntityAppDefService, FieldConfigService, IndexerService, LoaderService, NotificationService, OTMMService, ProjectConstant, SharingService, StateService, StatusTypes, TaskService, UtilService, ViewConfigService } from 'mpm-library';
import { forkJoin } from 'rxjs';
import { MPMAssetsViewComponent } from 'src/app/core/mpm-lib/project/assets-view/assets-view.component';
import { AssetsComponent } from 'src/app/core/mpm/project-view/assets/assets.component';
import { RouteService } from 'src/app/core/mpm/route.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { NpdProjectService } from '../services/npd-project.service';
import * as acronui from 'mpm-library';
import { MONSTER_ROLES } from '../../filter-configs/role.config';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';


@Component({
  selector: 'app-npd-assets',
  templateUrl: './npd-assets.component.html',
  styleUrls: ['./npd-assets.component.scss']
})
export class NpdAssetsComponent extends AssetsComponent {
  @ViewChild(MPMAssetsViewComponent, { static: false }) npdAssetComponent: MPMAssetsViewComponent;

  disableCustomReferenceAsset = false;

  public showDeleteIcon: boolean = false;
  public projectTeamIds = [];
  public projectRoles = ['NPD_PROJECT_E2E_PM_ITEM_ID', 'NPD_PROJECT_CORP_PM_ITEM_ID', 'NPD_PROJECT_OPS_PM_ITEM_ID',
    'NPD_PROJECT_REGULATORY_LEAD_ITEM_ID', 'NPD_PROJECT_COMMERCIAL_LEAD_ITEM_ID', 'NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_ITEM_ID',
    'NPD_PROJECT_PR_SPECIALIST_ITEM_ID', 'NPD_PROJECT_OPS_PLANNER_ITEM_ID', 'NPD_PROJECT_SECONDARY_AW_SPECIALIST', 'NPD_PROJECT_GFG_ITEM_ID']

  constructor(
    public activatedRoute: ActivatedRoute,
    public loaderService: LoaderService,
    public notification: NotificationService,
    public dialog: MatDialog,
    public assetService: AssetService,
    public otmmService: OTMMService,
    public taskService: TaskService,
    public sharingService: SharingService,
    public fieldConfigService: FieldConfigService,
    public indexerService: IndexerService,
    public utilService: UtilService,
    public routeService: RouteService,
    public entityAppDefService: EntityAppDefService,
    public router: Router,
    public appService: AppService,
    public stateService: StateService,
    public notificationService: NotificationService,
    public viewConfigService: ViewConfigService,
    public npdProjectService: NpdProjectService,
    public monsterConfigApiService:MonsterConfigApiService,
    public monsterUtilService: MonsterUtilService

  ) {
    super(activatedRoute, loaderService, notification, dialog, assetService, otmmService, taskService, sharingService, fieldConfigService
      , indexerService, utilService, routeService, entityAppDefService, router, appService, stateService, notificationService, viewConfigService,monsterUtilService);
    this.getProjectIdFromRouteParams();
  }

  refresh() {
    if (this.isCampaignAssetView) {
      this.assetService.getCampaignDetails(this.campaignId).subscribe(response => {
        if (response && response.Campaign) {
          this.campaign = response.Campaign;
          this.folderId = response.Campaign.OTMM_FOLDER_ID;
          this.getAssets();
        } else {
          this.loadingHandler(false);
        }
        //  this.enableAddReferenceAsset = (this.project.R_PO_PROJECT_OWNER["Identity-id"].Id === this.sharingService.getCurrentUserItemID()) ? true : false;
      }, error => {
        this.loadingHandler(false);
        const eventData = { message: 'Error while getting project details', type: 'error' };
        this.notificationHandler(eventData);
      });
    } else {
      this.assetService.getProjectDetails(this.projectId).subscribe(response => {
        if (response && response.Project) {
          this.project = response.Project;
          this.folderId = response.Project.OTMM_FOLDER_ID;
          this.entityAppDefService.getStatusById(this.project.R_PO_STATUS['MPM_Status-id'].Id).subscribe(projectStatus => {
            if (projectStatus.STATUS_TYPE === StatusTypes.FINAL_COMPLETED || projectStatus.STATUS_TYPE === StatusTypes.FINAL_CANCELLED || projectStatus.TYPE === StatusTypes.ONHOLD) {
              this.disableReferenceAsset = true;
            }
            if (projectStatus.STATUS_TYPE === StatusTypes.FINAL_COMPLETED || projectStatus.STATUS_TYPE === StatusTypes.FINAL_CANCELLED) {
              this.disableCustomReferenceAsset = true;
            }
            this.getAssets();
          });
        } else {
          this.loadingHandler(false);
        }
        this.enableAddReferenceAsset = (this.project.R_PO_PROJECT_OWNER["Identity-id"].Id === this.sharingService.getCurrentUserItemID()) ? true : false;
      }, error => {
        this.loadingHandler(false);
        const eventData = { message: 'Error while getting project details', type: 'error' };
        this.notificationHandler(eventData);
      });
    }

    this.loadingHandler(true);
  }

  public performDeleteAction(asset_ids, isMultiDelete) {
    let selection_context_param = {
      "selection_context": {
        "asset_ids": asset_ids,
        "asssetContentType": [],
        "assetSubContentType": [],
        "type": "com.artesia.asset.selection.AssetIdsSelectionContext",
        "include_descendants": "NONE"
      }
    }

    let selection_context = { "selection_context_param": selection_context_param }
    let asset_state_options = {
      "asset_state_options_param":
      {
        "asset_state_options":
        {
          "type": "com.artesia.asset.DeleteAssetOptions",
          "apply_to_all_versions": false,
          "delete_only_content": false,
          "hierarchical_processing_options": "APPLY_ONLY_TO_THIS_CONTAINER"
        }
      }
    }

    let params = `action=delete&selection_context=${JSON.stringify(selection_context)}&asset_state_options_param=${JSON.stringify(asset_state_options)}`;

    const httpOptions = {
      headers: new HttpHeaders().set('X-Requested-By', this.userSessionId.toString())
        .set('Content-Type', 'application/x-www-form-urlencoded'),
      withCredentials: true
    };
    this.loaderService.show();

    this.otmmService.http.put('/otmmapi/v6/assets/state', params, httpOptions).subscribe((res: any) => {
      this.loaderService.hide();
      if (res.bulk_asset_result_representation.bulk_asset_result.successful_object_list.length > 0) {
        this.notificationService.success('Deletion  has been initiated ');
        if (isMultiDelete){
          this.selectedAssets.selected.forEach(row => {
            row.selected = false;
            this.selectedAssets.deselect(row);
          });

          if (this.isCampaignAssetView) {
            this.selectedAssetData = this.stateService.selectedCampaignAssetIds = [];
          } else {
            this.stateService.selectedProjectAssetIds = [];
          }
          if (!this.assetMenuconfig?.IS_LIST_VIEW) {
            this.npdAssetComponent.unChekedAllAsets();
          }
        }

      } else {
        this.notificationService.error('Something went wrong while deletion');

      }
    }, err => {
      this.notificationService.error('Something went wrong while deletion');
      this.loaderService.hide();
    })

  }

  public deleteAsset(asset) {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '40%',
      disableClose: true,
      data: {
        message: 'Are you sure you want to delete the Asset?',
        submitButton: 'Yes',
        cancelButton: 'No'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.isTrue) {
          this.performDeleteAction([asset.asset_id], false)
        }
      }
    });
  }

  public deleteSelectedAssets() {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '40%',
      disableClose: true,
      data: {
        message: 'Are you sure you want to delete selected Assets?',
        submitButton: 'Yes',
        cancelButton: 'No'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.isTrue) {
          this.performDeleteAction(this.stateService.getSelectedProjectAssets(), true);
        }
      }
    });

  }


  onChangeView(isListView: boolean) {
    this.selectedAssets?.clear();
    this.selectedDeliverables?.clear();
    this.selectedAssets?.clear();
    this.assetMenuconfig.IS_LIST_VIEW = isListView;
    this.setCurrentView();
  }

  setCurrentView() {
    const currentViewData = {
      sortName: null,
      sortOrder: null,
      pageNumber: this.pageNumber,
      pageSize: this.pageSize,
      isListView: true,
      viewId: this.currentViewConfig['MPM_View_Config-id'].Id,
      viewName: this.currentViewConfig.VIEW
    }
    sessionStorage.setItem('CURRENT_ASSETS_VIEW', JSON.stringify(currentViewData));
  }
  public getProjectIdFromRouteParams() {
    this.readUrlParams((routeParams, queryParams) => {
      if (queryParams && routeParams) {
        this.getProjectDetailsById(routeParams.projectId)
      }
    });

  }
  public readUrlParams(callback) {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.activatedRoute.parent.params.subscribe(parentRouteParams => {
        callback(parentRouteParams, queryParams);
      });
    });
  }
  /**
    * getProjectDetailsById is used to fetch different Users based on Role from  Project Team
    * Author: Venkat Ramana
    */
  public getProjectDetailsById(projectId) {
    this.npdProjectService.getProjectDetailsById(projectId).subscribe(projResponse => {
      let projectData = projResponse[0];
      for (let item of this.projectRoles) {
        if (projectData[item]) {
          this.projectTeamIds.push(projectData[item])
        }

      }
      this.getAllRolesForCurrentUser();
    });
  }

  /**
   * getUserDetails is used to check loginUser is part of Project Team or not to display Delete Icon in View
   * Author: Venkat Ramana
   */

  public getUserDetails() {
    let loginuser = (sessionStorage.getItem('OTDS_USER_DN'))?.split(',')[0]?.split('=')[1];
    const reqObject = {
      'userCN': loginuser
    }
    this.appService.invokeRequest('http://schemas/MPMCustomizedApplicationPackages/Person/operations', 'GetPersonByUserId', reqObject).subscribe((res: any) => {
      (res)
      const loginUserId = acronui.findObjectByProp(acronui.findObjectByProp(acronui.findObjectByProp(res, 'Person'), 'PersonToUser'), 'Identity-id')?.ItemId;
      this.showDeleteIcon = this.projectTeamIds.includes(loginUserId);

    }, err => {
    })

  }



  /**
   * getAllRolesForCurrentUser is to check login user have any Admin Access to show delete Icon
   * Author: Venkat Ramana
   */
  public getAllRolesForCurrentUser() {
    let adminRoles = [MONSTER_ROLES.NPD_ADMIN, MONSTER_ROLES.REGS_TRAFFIC_MANAGER, MONSTER_ROLES.NPD_PM, MONSTER_ROLES.CORP_PROJECT_LEAD];

    let organizations = JSON.parse(sessionStorage.getItem('USER_DETAILS_LDAP'))?.tuple?.old?.user?.organization;
    let currentUserRoles = [];

    if (Array.isArray(organizations)) {
      organizations.map(x => {
        if (x.description == "NPD" || x.description == "NPD Tracker") {
          currentUserRoles = x?.role;
        }
      });
    } else {
      currentUserRoles = organizations.role;
    }
    for (let eachRole of currentUserRoles) {
      if (adminRoles.includes(eachRole['@id'].substring(3, eachRole['@id'].indexOf(',')))) {
        this.showDeleteIcon = true;
        break;
      }
    }
    if (!this.showDeleteIcon) {
      this.getUserDetails();
    }
  }

}
