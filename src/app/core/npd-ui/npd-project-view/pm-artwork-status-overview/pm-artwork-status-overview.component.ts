import { Component, Input, OnInit } from '@angular/core';

import { EntityListConstant } from '../../request-view/constants/EntityListConstant';
import { EntityService } from '../../services/entity.service';

import { NpdProjectService } from '../services/npd-project.service';
import { RouteService } from 'src/app/core/mpm/route.service';
 /**
     * @author JeevaR
     * @purpose Artwork Status from MPM system
     */
@Component({
  selector: 'app-pm-artwork-status-overview',
  templateUrl: './pm-artwork-status-overview.component.html',
  styleUrls: ['./pm-artwork-status-overview.component.scss'],
})
export class PmArtworkStatusOverviewComponent implements OnInit {
  secondaryPackagingResponse: any;
  isDataCollected = false;
  @Input() projectEntityItemId;
  mpmJobList = [];
  mpmJobStatuses = [];
  mpmJobStatusArray = [];
  constructor(
    public entityService: EntityService,
    public npdProjectService: NpdProjectService,
    public routingService: RouteService
  ) {}

  getAllSecondaryPackagingData() {
    let mpmJobs: string = '';
    let mpmGroups: string = '';
    this.entityService
      .getEntityItemDetails(
        this.projectEntityItemId,
        EntityListConstant.SECONDARY_PACKAGING_RELATION
      )
      .subscribe(
        (secondaryPackagingResponse) => {
          if (secondaryPackagingResponse && secondaryPackagingResponse.items) {
            secondaryPackagingResponse.items.forEach((secondaryPack) => {
              let mpmJobNo = secondaryPack?.Properties?.MPMJobNo;
              let mpmGroupName = secondaryPack?.Properties?.SubRequest;
              if (mpmJobNo && mpmGroupName) {
                if (mpmJobs != '' && mpmGroups != '') {
                  mpmJobs = mpmJobs + ',' + mpmJobNo;
                  mpmGroups = mpmGroups + ',' + mpmGroupName;
                } else {
                  mpmJobs = mpmJobNo;
                  mpmGroups = mpmGroupName;
                }
              }
              let Job = {
                JOB_ID: mpmJobNo,
                GROUP_NAME: mpmGroupName,
                jobStatuses: [],
                IS_ARTWORK: secondaryPack?.Properties?.IsArtwork,
                PACK_FORMAT: secondaryPack?.R_PO_PACK_FORMAT?.Properties
                  ?.PackType
                  ? secondaryPack?.R_PO_PACK_FORMAT?.Properties?.PackType
                  : 'PACK FORMAT',
              };
              if (secondaryPack?.Properties?.IsArtwork==false){
                  this.mpmJobList.push(Job);
              }
              else{
                this.mpmJobList.push(Job);
              }
                
            });

            this.npdProjectService
              .getArtworkStatusOverviewByMPMJobsAndMPMGroups(mpmJobs, mpmGroups)
              .subscribe((response: any) => {
                let resp = JSON.parse(JSON.stringify(response));
                let list = this.mpmJobList;
                this.mpmJobList = [];
                list.forEach((mpmJob) => {
                  let isFound = false;
                  if (typeof resp?.tuple == 'object' && !resp?.tuple.length) {
                    if (
                      mpmJob.JOB_ID ==
                        resp?.tuple?.old?.ArtworkJobStatusOverview?.JOB_ID &&
                      mpmJob.GROUP_NAME ===
                        resp?.tuple?.old?.ArtworkJobStatusOverview?.GROUP_NAME
                    ) {
                      isFound = true;
                      let JOB: any = {};
                      JOB.JOB_ID =
                        resp?.tuple?.old?.ArtworkJobStatusOverview?.JOB_ID;
                      JOB.GROUP_NAME =
                        resp?.tuple?.old?.ArtworkJobStatusOverview?.GROUP_NAME;
                      JOB.PROJECT_ID =
                        resp?.tuple?.old?.ArtworkJobStatusOverview?.PROJECT_ID;
                      JOB.jobStatuses = [];
                      JOB.jobStatuses.push({
                        'Artwork Briefed': resp?.tuple?.old
                          ?.ArtworkJobStatusOverview?.ARTWORK_BRIEFED
                          ? resp?.tuple?.old?.ArtworkJobStatusOverview
                              ?.ARTWORK_BRIEFED
                          : 'CANCELLED',
                      });
                      this.mpmJobStatuses.push('Artwork Briefed');
                      JOB.jobStatuses.push({
                        'Proof Available': resp?.tuple?.old
                          ?.ArtworkJobStatusOverview?.PROOF_AVAILABLE
                          ? resp?.tuple?.old?.ArtworkJobStatusOverview
                              ?.PROOF_AVAILABLE
                          : 'CANCELLED',
                      });
                      this.mpmJobStatuses.push('Proof Available');
                      JOB.jobStatuses.push({
                        'NPD Approval': resp?.tuple?.old
                          ?.ArtworkJobStatusOverview?.NPD_APPROVAL
                          ? resp?.tuple?.old?.ArtworkJobStatusOverview
                              ?.NPD_APPROVAL
                          : 'CANCELLED',
                      });
                      this.mpmJobStatuses.push('NPD Approval');
                      if (
                        resp?.tuple?.old?.ArtworkJobStatusOverview?.PEER_REVIEW
                      ) {
                        let peerReviews =
                          resp?.tuple?.old?.ArtworkJobStatusOverview?.PEER_REVIEW.split(
                            ','
                          );

                        peerReviews.forEach((peer) => {
                          JOB.jobStatuses.push({
                            'Peer Review': peer,
                          });
                          this.mpmJobStatuses.push('Peer Review');
                        });
                      } else {
                        JOB.jobStatuses.push({
                          'Peer Review': 'TODO',
                        });
                        this.mpmJobStatuses.push('Peer Review');
                      }

                      JOB.jobStatuses.push({
                        'Approval Routing': resp?.tuple?.old
                          ?.ArtworkJobStatusOverview?.APPROVAL_ROUTING
                          ? resp?.tuple?.old?.ArtworkJobStatusOverview
                              ?.APPROVAL_ROUTING
                          : 'TODO',
                      });
                      this.mpmJobStatuses.push('Approval Routing');
                      JOB.IS_ARTWORK = mpmJob.IS_ARTWORK;
                      JOB.PACK_FORMAT = mpmJob.PACK_FORMAT;
                      this.mpmJobList.push(JOB);
                      this.mpmJobStatusArray.push(this.mpmJobStatuses);
                      this.mpmJobStatuses = [];
                      JOB = {};
                    }
                  } else {
                    if (resp.tuple) {
                      resp?.tuple.forEach((job) => {
                        if (
                          mpmJob.JOB_ID ==
                            job?.old?.ArtworkJobStatusOverview?.JOB_ID &&
                          mpmJob.GROUP_NAME ===
                            job?.old?.ArtworkJobStatusOverview?.GROUP_NAME
                        ) {
                          isFound = true;
                          let JOB = job?.old?.ArtworkJobStatusOverview;
                          JOB.jobStatuses = [];
                          JOB.jobStatuses.push({
                            'Artwork Briefed': job?.old
                              ?.ArtworkJobStatusOverview?.ARTWORK_BRIEFED
                              ? job?.old?.ArtworkJobStatusOverview
                                  ?.ARTWORK_BRIEFED
                              : 'CANCELLED',
                          });
                          this.mpmJobStatuses.push('Artwork Briefed');
                          JOB.jobStatuses.push({
                            'Proof Available': job?.old
                              ?.ArtworkJobStatusOverview?.PROOF_AVAILABLE
                              ? job?.old?.ArtworkJobStatusOverview
                                  ?.PROOF_AVAILABLE
                              : 'CANCELLED',
                          });
                          this.mpmJobStatuses.push('Proof Available');
                          JOB.jobStatuses.push({
                            'NPD Approval': job?.old?.ArtworkJobStatusOverview
                              ?.NPD_APPROVAL
                              ? job?.old?.ArtworkJobStatusOverview?.NPD_APPROVAL
                              : 'CANCELLED',
                          });
                          this.mpmJobStatuses.push('NPD Approval');
                          if (job?.old?.ArtworkJobStatusOverview?.PEER_REVIEW) {
                            let peerReviews =
                              job.old?.ArtworkJobStatusOverview?.PEER_REVIEW.split(
                                ','
                              );

                            peerReviews.forEach((peer) => {
                              JOB.jobStatuses.push({
                                'Peer Review': peer,
                              });
                              this.mpmJobStatuses.push('Peer Review');
                            });
                          } else {
                            JOB.jobStatuses.push({
                              'Peer Review': 'TODO',
                            });
                            this.mpmJobStatuses.push('Peer Review');
                          }

                          JOB.jobStatuses.push({
                            'Approval Routing': job?.old
                              ?.ArtworkJobStatusOverview?.APPROVAL_ROUTING
                              ? job?.old?.ArtworkJobStatusOverview
                                  ?.APPROVAL_ROUTING
                              : 'TODO',
                          });
                          this.mpmJobStatuses.push('Approval Routing');
                          this.mpmJobStatusArray.push(this.mpmJobStatuses);
                          this.mpmJobStatuses = [];
                          JOB.IS_ARTWORK = mpmJob.IS_ARTWORK;
                          JOB.PACK_FORMAT = mpmJob.PACK_FORMAT;
                          this.mpmJobList.push(JOB);
                        }
                      });
                    }
                   
                  }

                  if (!isFound) {
                    let JOB: any = {};
                    JOB.JOB_ID = mpmJob?.JOB_ID ? mpmJob?.JOB_ID : 'JOB NO';
                    JOB.GROUP_NAME = mpmJob?.GROUP_NAME
                      ? mpmJob?.GROUP_NAME
                      : 'GROUP NAME';
                    JOB.IS_ARTWORK = mpmJob?.IS_ARTWORK;
                    JOB.PACK_FORMAT = mpmJob?.PACK_FORMAT
                      ? mpmJob?.PACK_FORMAT
                      : 'PACK FORMAT';
                    JOB.jobStatuses = [];
                    JOB.jobStatuses.push({
                      'Artwork Briefed': 'INDEFINITE',
                    });
                    this.mpmJobStatuses.push('Artwork Briefed');
                    JOB.jobStatuses.push({
                      'Proof Available': 'INDEFINITE',
                    });
                    this.mpmJobStatuses.push('Proof Available');
                    JOB.jobStatuses.push({
                      'NPD Approval': 'INDEFINITE',
                    });
                    this.mpmJobStatuses.push('NPD Approval');
                    JOB.jobStatuses.push({
                      'Peer Review': 'INDEFINITE',
                    });
                    this.mpmJobStatuses.push('Peer Review');
                    JOB.jobStatuses.push({
                      'Approval Routing': 'INDEFINITE',
                    });
                    this.mpmJobStatuses.push('Approval Routing');
                    this.mpmJobStatusArray.push(this.mpmJobStatuses);
                    this.mpmJobStatuses = [];
                    this.mpmJobList.push(JOB);
                  }
                });
                this.isDataCollected = true;
              });
          }
        },
        (error) => {
          console.log('error occured in service')
        }
      );
  }
  openMPMSystem(projectId) {
    this.routingService.goToMPMProjectAssetViewNewTab(projectId);
  }

  ngOnInit(): void {
    this.getAllSecondaryPackagingData();
  }
  refreshStatus(){
    this.isDataCollected=false;
     this.mpmJobList = [];
     this.mpmJobStatuses = [];
     this.mpmJobStatusArray = [];
    this.getAllSecondaryPackagingData();
  }
}
