import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NotificationService, LoaderService } from 'mpm-library';
import { forkJoin, Observable } from 'rxjs';
import { BusinessConstants } from '../../request-view/constants/BusinessConstants';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { RequestService } from '../../services/request.service';
import { OverViewConstants } from '../constants/projectOverviewConstants';
import { NpdProjectService } from '../services/npd-project.service';

@Component({
  selector: 'app-pm-project-classification',
  templateUrl: './pm-project-classification.component.html',
  styleUrls: ['./pm-project-classification.component.scss']
})
export class PmProjectClassificationComponent implements OnInit {
  public allMarkets = [];
  public bussinessUnits = [];
  public finalDraftManufacturingLocation="";
  @Input() leadMarket;
  @Input() bussinessUnit;

  @Input() projectOverviewForm: FormGroup;

  @Input() config;
  @Input() requestDetails;
  @Input() projectClassificationDataConfig;
  @Input() request;
  @Input() projectType;
  @Input() isNewRequest;
  isDraftManufactureDataLoad;

  earlyProjectClassificationVal;
  selectedProjectClassification;
  projectClassificationDataForm: FormGroup;
  readonly radioButtonsList = BusinessConstants.radioButtons;

  requestId: any;
  selectedCorpPM;
  selectedOpsPM;
  corpPMUsers = [];
  opsPMUsers = [];
  allDraftManufacturingLocations:any[];

  earlyManufacturingSites = [];
  draftManufacturingLocations = [];
  projectClassificationParams: any = {
    projectType: null,
    projectClassificationScore: null
  }
  regulatoryTimelineForLocations = {
    market: null,
    manufacturingLocation: null
  }

  constructor(
    public formBuilder: FormBuilder,
    public notificationService: NotificationService,
    public npdProjectService: NpdProjectService,
    public monsterUtilService: MonsterUtilService,
    public monsterConfigApiService: MonsterConfigApiService,
    public loaderService: LoaderService,
  ) {

  }

  get classificationDataForm() {
    return this.projectClassificationDataForm?.controls
  }

  ngOnInit(): void {
    this.getBusinessUnits();
    this.getAllMarkets();
    this.getDraftManufacturingLocation();
    if (this.projectClassificationDataConfig) {//this.request &&
      this.createForm();
      this.getData();
      if (!this.isNewRequest) {
        if (this.projectClassificationDataForm?.controls?.draftManufacturingLocation?.value && this.projectClassificationDataForm?.controls?.draftManufacturingLocation?.value != 'NA'){
          this.finalDraftManufacturingLocation=this.projectClassificationDataForm?.controls?.draftManufacturingLocation?.value;
            this.getEarlyManufacturingSites(this.projectClassificationDataForm.controls.draftManufacturingLocation.value).subscribe();
        }         
      }
    }
    this.disableLeadFormulaField(true);
  }

  getBusinessUnits() {
    this.monsterConfigApiService.getAllBusinessUnits().subscribe((response) => {
      this.bussinessUnits = response;
    }, (error) => {
    });
  }
  getAllMarkets(){
    this.monsterConfigApiService.getAllMarkets().subscribe(res=>{
      this.allMarkets=res;
    })
  }

  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(this.projectClassificationDataForm, this.projectClassificationDataConfig);
  }

  setProjectClassification(property, value) {
    this.projectClassificationParams.projectType = value?.split('.')[1];
    this.disableLeadFormulaField();
  }

  createForm() {

    this.projectClassificationDataForm = this.formBuilder.group({});

    Object.keys(this.projectClassificationDataConfig).filter(formControlConfigName => {
      if (formControlConfigName && this.projectClassificationDataConfig[formControlConfigName].ngIf && 'formControl' in this.projectClassificationDataConfig[formControlConfigName] && this.projectClassificationDataConfig[formControlConfigName]['formControl'] === true) {
        let validators = [];
        if (this.projectClassificationDataConfig[formControlConfigName].validators) {
          if (this.projectClassificationDataConfig[formControlConfigName].validators.required) {
            validators.push(Validators.required);
          }
          if (this.projectClassificationDataConfig[formControlConfigName].validators.length && this.projectClassificationDataConfig[formControlConfigName].validators.length.max) {
            validators.push(Validators.maxLength(this.projectClassificationDataConfig[formControlConfigName].validators.length.max));
          }
        }
        if(formControlConfigName=='registrationClassification'||formControlConfigName=='registrationRequirement'){
          this.projectClassificationDataForm.addControl(formControlConfigName, new FormControl({
            value: this.projectClassificationDataConfig[formControlConfigName].value!='N/A'?this.projectClassificationDataConfig[formControlConfigName].value?'Yes':'No':this.projectClassificationDataConfig[formControlConfigName].value,
            disabled: this.projectClassificationDataConfig[formControlConfigName].disabled
          }, validators));
        }
        if(formControlConfigName=='leadFormula'){
          this.projectClassificationDataForm.addControl(formControlConfigName, new FormControl({
            value: this.projectClassificationDataConfig[formControlConfigName].value==='N/A' || this.projectClassificationDataConfig[formControlConfigName].value===''?false:this.projectClassificationDataConfig[formControlConfigName].value,
            disabled: this.projectClassificationDataConfig[formControlConfigName].disabled
          }, validators));
        }
          else{
          this.projectClassificationDataForm.addControl(formControlConfigName, new FormControl({
            value: this.projectClassificationDataConfig[formControlConfigName].value,
            disabled: this.projectClassificationDataConfig[formControlConfigName].disabled
          }, validators));
        }
       
      }

    });
    if (this.projectClassificationDataForm?.controls?.projectType?.value) {
      this.projectClassificationParams.projectType = this.projectClassificationDataForm.controls.projectType.value?.split('.')[1];
    }
    this.regulatoryTimelineForLocations.market = this.projectClassificationDataConfig?.leadMarketRegulatoryClassification?.value;
    // this.getClassificationChange();
  }

  getData() {
    forkJoin([this.getDraftManufacturingLocation()])
      .subscribe(responseList => {
      }, () => {
        console.log('Error while loading the form configs.')
      });
  }

  getDraftManufacturingLocation(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllDraftManufacturingLocations().subscribe((response) => {
        this.allDraftManufacturingLocations=response;
        let draftManufacturingLocationOptions = [];
        draftManufacturingLocationOptions = response;
        draftManufacturingLocationOptions.forEach(draftManufacturingLocation => {
          let location = {
            ItemId: draftManufacturingLocation?.Identity?.ItemId,
            Id: draftManufacturingLocation?.Identity?.Id,
            displayName: draftManufacturingLocation?.['R_PO_MARKET$Properties']?.DisplayName,
            regulatoryClassification: draftManufacturingLocation?.['R_PO_MARKET$Properties']?.RegulatoryClassification
          }
          this.draftManufacturingLocations.push(location);
        });
        if (this.projectClassificationDataForm?.controls?.draftManufacturingLocation?.value) {
          this.isDraftManufactureDataLoad = true;
          let selectedDraftManufacturingLocation = this.draftManufacturingLocations.find(manufacturingLocation => manufacturingLocation.ItemId == this.projectClassificationDataForm.controls.draftManufacturingLocation.value)
          this.regulatoryTimelineForLocations.manufacturingLocation = selectedDraftManufacturingLocation?.regulatoryClassification;
        }
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  onDraftManufactureLocationChange(draftManufacturingLocation, formName, property) {
    this.earlyManufacturingSites = [];
    this.loaderService.show();
    this.projectClassificationDataForm.patchValue({
      earlyManufacturingSite: ''
    });
    if(draftManufacturingLocation && draftManufacturingLocation != 'NA'){
      this.finalDraftManufacturingLocation=draftManufacturingLocation;
    }
    this.updateRequestRelationsInfo('earlyManufacturingSiteId', 'earlyManufacturingSite');
    this.isDraftManufactureDataLoad = false;
    let selectedDraftManufacturingLocation = this.draftManufacturingLocations.find(manufacturingLocation => manufacturingLocation.ItemId == draftManufacturingLocation)
    let data = { property: 'manufacturingLocation', value: selectedDraftManufacturingLocation?.regulatoryClassification, isDataLoad: false }//draftManufacturingLocation?.split('.')[1]
    // this.getRegulatoryTimeLineLocations(data);
    this.getClassificationChange(false);
    this.getEarlyManufacturingSites(draftManufacturingLocation).subscribe(response => {
      this.loaderService.hide();
    }, (error) => {
      this.loaderService.hide();
    });
  }


  getEarlyManufacturingSites(draftManufacturingLocation): Observable<any> {
    let draftManufacturingRequestParams = {
      'Draft_Manufacturing_Location-id': {
        Id: draftManufacturingLocation && draftManufacturingLocation.split('.')[1],
        ItemId: draftManufacturingLocation
      }
    }
    return new Observable(observer => {
      this.monsterConfigApiService.fetchEarlyManufacturingSitesByDraftManufacturingLocation(draftManufacturingRequestParams).subscribe((response) => {
        this.earlyManufacturingSites = this.monsterConfigApiService.transformResponse(response, "Early_Manufacturing_Site-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }


  onChangeTotal() {
    let projectClasification = this.projectClassificationDataForm.getRawValue();
    const newFg = projectClasification.newFg === true ? 1000 : 0;
    const newRnDFormula = projectClasification.newRnDFormula === true ? 10000 : 0;
    const newHBCFormula = projectClasification.newHBCFormula === true ? 100000 : 0;
    const newPrimaryPackaging = projectClasification.newPrimaryPackaging === true ? 1000000 : 0;
    const secondaryPackaging = projectClasification.secondaryPackaging === true ? 10000000 : 0;
    const registrationDossier = projectClasification.registrationDossier === true ? 100000000 : 0;
    const preProdReg = projectClasification.preProdReg === true ? 1000000000 : 0;
    const postProdReg = projectClasification.postProdReg === true ? 10000000000 : 0;

    return {
      formulaResult: newFg + newRnDFormula + newHBCFormula + newPrimaryPackaging + secondaryPackaging + registrationDossier + preProdReg + postProdReg,
    };


  }

  onValidate() {
    this.projectClassificationDataForm.markAllAsTouched();
    if (this.projectClassificationDataForm.invalid) {
      return true;
    } else {
      return false;
    }
  }

  validateProjClassificationAutoPopulateFields() {
    if (this.projectClassificationDataForm.controls?.draftManufacturingLocation.dirty && this.projectClassificationDataForm.controls?.registrationDossier &&
      this.projectClassificationDataForm.controls?.preProdReg &&
      this.projectClassificationDataForm.controls?.postProdReg) {
      if (this.projectClassificationDataForm.controls.registrationDossier.value == null &&
        this.projectClassificationDataForm.controls.preProdReg.value == null
        && this.projectClassificationDataForm.controls.postProdReg.value == null) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  checkValueIsEmpty(value) {
    if (value == '' || value == null) {
      return true;
    }
    return false
  }

  checkControlTouched() {
    if (this.projectClassificationDataForm.controls?.draftManufacturingLocation.dirty ||
      this.projectClassificationDataForm.controls?.projectType.dirty ||
      this.projectClassificationDataForm.controls?.newFg.dirty ||
      this.projectClassificationDataForm.controls?.newRnDFormula.dirty ||
      this.projectClassificationDataForm.controls?.newHBCFormula.dirty ||
      this.projectClassificationDataForm.controls?.newPrimaryPackaging.dirty ||
      this.projectClassificationDataForm.controls?.registrationDossier.dirty ||
      this.projectClassificationDataForm.controls?.preProdReg.dirty ||
      this.projectClassificationDataForm.controls?.postProdReg.dirty) {
      return true;
    } else {
      return false;
    }
  }

  validateEarlyProjectClassificationFields() {
    if (this.projectClassificationDataForm.controls?.earlyProjectClassification &&
      this.projectClassificationDataForm.controls?.earlyProjectClassificationDesc && this.checkControlTouched()) {
      if (this.checkValueIsEmpty(this.projectClassificationDataForm.controls.earlyProjectClassification.value) ||
        this.checkValueIsEmpty(this.projectClassificationDataForm.controls.earlyProjectClassificationDesc.value)) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  updateRequestInfo(formControl, formName, propertyValue?) {
    let propertyName = OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROJECT_FINAL_CLASSIFICATION[formControl];
    let value;

    /*     if(formControl === ('earlyProjectClassification' || 'earlyProjectClassificationDesc') ) {
          value  = this.selectedProjectClassification && this.selectedProjectClassification.ItemId
        }  else {
          value = propertyValue? propertyValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;
        } */
    if(formControl=='registrationClassification'||formControl=='registrationRequirement'){
      value = propertyValue|| propertyValue=='Yes' ? true:false;

    } else{
      value = propertyValue ? propertyValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;
    }
    if (propertyName) {
      this.npdProjectService.setCustomFormProperty(propertyName, value);
    }
  }

  updateRequestRelationsInfo(relation, formControl) {
    let property;
    let propertyValue;
    let relationProperty;
    let relationvalue;

    relationProperty = OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROJECT_FINAL_CLASSIFICATION[relation];
    relationvalue = this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;//itemId
    property = OverViewConstants.OVERVIEW_OBJECT_CONSTANTS.PROJECT_FINAL_CLASSIFICATION[formControl];
    propertyValue = this.getRelationValue(formControl, relationvalue)//fetch value-display name
    if (property) {
      this.npdProjectService.setCustomFormProperty(property, propertyValue);
      this.npdProjectService.setCustomFormProperty(relationProperty, relationvalue);
    }
  }

  getRelationValue(relation, itemId) {
    let value;
    if (relation == 'projectType') {
      value = this.config.projectTypes?.find(projectType => projectType.ItemId == itemId)?.displayName;
    } else if (relation == 'earlyManufacturingSite') {
      value = this.earlyManufacturingSites?.find(site => site.ItemId == itemId)?.displayName;
    } else if (relation == 'draftManufacturingLocation') {
      value = this.draftManufacturingLocations?.find(loc => loc.ItemId == itemId)?.displayName;
    }
    return value;
  }

  /*   updateRequestInfo(formControl,formName,propertyValue?) {
      let property;
      let value;
      let itemId;
      if(formName === 'projectClassificationDataForm') {
        property = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
        value = propertyValue? propertyValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;
        itemId = this.requestDetails && this.requestDetails.requestItemId;
      }
      this.monsterUtilService.setFormProperty(itemId, property, value);
    } */

  /*   updateRequestInfoByValue(formControl,formName,propertyValue) {
      let property;
      let value;
      let itemId;
      if(formName === 'projectClassificationDataForm') {
        property = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
        value = propertyValue;
        itemId = this.requestDetails && this.requestDetails.requestItemId;
      }
      this.monsterUtilService.setFormProperty(itemId, property, value);
    } */

  getClassificationChange(isThrowableError?) {
    // const totalScore = this.onChangeTotal();


    /**NEW Implementation */
    let formValue = this.projectClassificationDataForm.value;
    let regionId = 0;
    let countryOfSaleId = 0;
    let countryOfSaleRC="";
    let countryOfManufactureRc="";

    this.bussinessUnits.forEach(x => {
      if (x['DISPLAY_NAME'] == this.bussinessUnit) {
        regionId = x['R_PO_REGION']['Region-id']['Id'];

      }
    })
    this.allMarkets.forEach(x => {
      if (x['Name'] == this.leadMarket) {
        countryOfSaleId = x['Markets-id']['Id'];
        countryOfSaleRC=x['RegulatoryClassification'];
      }
    })
    this.allDraftManufacturingLocations.forEach(location=>{
      if(location['Identity']['Id']==this.finalDraftManufacturingLocation.split('.')[1]){
        countryOfManufactureRc=location['R_PO_MARKET$Properties']['RegulatoryClassification'];
      }
    })

    let param = {
      regionId: regionId,
      countryOfSaleId: countryOfSaleId,
      countryOfManufactureId: formValue.draftManufacturingLocation?.split('.')[1],
      projectTypeId: formValue.projectType?.split('.')[1],
      newFG: formValue.newFg,
      newRD: formValue.newRnDFormula,
      newHBC: formValue.newHBCFormula,
      primaryPackaging: formValue.newPrimaryPackaging,
      secondaryPackaging: formValue.secondaryPackaging,
      countryOfSaleRC: countryOfSaleRC,
      countryOfManufactureRC: countryOfManufactureRc,
      leadFormula :formValue.leadFormula

    }
    // if (param.newFG != null && param.newRD != null && param.newHBC != null && param.primaryPackaging != null && param.secondaryPackaging != null) {
    //   this.generateClassificationNumber(param);
    // }
    this.generateClassificationNumber(param,isThrowableError);
  }

  // getRegulatoryTimeLineLocations(data){
  //   this.regulatoryTimelineForLocations[data.property] = data.value;
  //   if(this.regulatoryTimelineForLocations.manufacturingLocation && this.regulatoryTimelineForLocations.market
  //     ) {//&& !this.isDraftManufactureDataLoad && !this.regulatoryTimelineForLocations?.market?.isDataLoad
  //     this.loaderService.show();
  //     this.monsterConfigApiService.fetchRegulatoryTimelineForLocations(this.regulatoryTimelineForLocations).subscribe(
  //       response => {
  //         this.loaderService.hide();
  //         let registrationDossier = false;
  //         let preProdReg = false;
  //         let postProdReg = false;
  //         if(response && response?.['Regulatory_Timeline']){

  //           let value = response['Regulatory_Timeline'];
  //           registrationDossier = (value?.RegistrationPreparationDuration > 0)?true : false;
  //           preProdReg = (value?.PreProductionRegistartionDuration > 0)?true : false;
  //           postProdReg = (value?.PostProductionDuration > 0)? true: false;
  //         } else {
  //           registrationDossier = null;
  //           preProdReg = null;
  //           postProdReg = null;
  //         }
  //         this.projectClassificationDataForm.patchValue(
  //           {
  //             registrationDossier: registrationDossier,
  //             preProdReg: preProdReg,
  //             postProdReg: postProdReg,
  //           });
  //         // this.updateRequestInfo('registrationDossier','projectClassificationDataForm',registrationDossier)
  //         // this.updateRequestInfo('preProdReg','projectClassificationDataForm',preProdReg)
  //         // this.updateRequestInfo('postProdReg','projectClassificationDataForm',postProdReg)
  //         if(this.projectClassificationDataForm?.controls?.registrationDossier && this.projectClassificationDataForm?.controls?.preProdReg
  //           && this.projectClassificationDataForm?.controls?.postProdReg) {
  //           this.getClassificationChange();
  //         }
  //       },(error) => {
  //         this.loaderService.hide();
  //         console.log('Error while loading the Regulatory Timelines.')
  //       })
  //   }
  // }




  // getProjectClassificationNoDescription(parameters) : Observable<any> {
  //   return new Observable(observer => {
  //     this.monsterConfigApiService.fetchProjectClassificationNoDescription(parameters).subscribe((response) => {
  //       if(response.length > 0){
  //         this.projectClassificationDataForm.patchValue({
  //           earlyProjectClassification: response[0].ProjectClassificationNumber,
  //           earlyProjectClassificationDesc: response[0].ProjectClassificationDescription,
  //         });
  //         this.selectedProjectClassification = response[0]['Project_Classifier_Data-id'];
  //       }else {
  //         this.projectClassificationDataForm.patchValue({
  //           earlyProjectClassification: '',
  //           earlyProjectClassificationDesc: '',
  //         });
  //         this.selectedProjectClassification = null;
  //       }
  //       this.updateRequestInfo('earlyProjectClassification','projectClassificationDataForm',null);
  //       this.updateRequestInfo('earlyProjectClassificationDesc','projectClassificationDataForm',null);
  //       observer.next(true);
  //       observer.complete();
  //     },(error)=> {
  //       observer.error(error);
  //     });
  //   });
  // }



  public generateClassificationNumber(parameters,isThrowableError) {
    const notificationValidationArray = [
      parameters.newFG,
      parameters.newHBC,
      parameters.newRD,
      parameters.primaryPackaging,
      parameters.secondaryPackaging,
    ];
    const notificationValidation = notificationValidationArray.every(value => value !== null && value !== undefined && value.toString() !='');

    this.monsterConfigApiService.generateClassificationNumber(parameters).subscribe(response => {
      if (response.length > 0) {
        this.projectClassificationDataForm.patchValue({
          earlyProjectClassification: response[0].CLASSIFICATION_NUMBER,
          earlyProjectClassificationDesc: response[0].CLASSIFICATION_DESCRIPTION,
          registrationClassificationAvailable: response[0]?.REGISTRATION_CLASSIFICATION_AVAILABLE == 'true' ? true : false,
          registrationRequirement: response[0]?.REGISTRATION_REQUIREMENT == 'true' ? true : false
        });
        this.selectedProjectClassification = response[0];
        this.projectClassificationDataForm.patchValue(
          {
            earlyProjectClassification: notificationValidation?response[0]?.CLASSIFICATION_NUMBER:null,
            earlyProjectClassificationDesc: notificationValidation?response[0]?.CLASSIFICATION_DESCRIPTION:null,
            registrationClassification: response[0]?.REGISTRATION_CLASSIFICATION_AVAILABLE == 'true' ? 'Yes' : 'No',
            registrationRequirement: response[0]?.REGISTRATION_REQUIREMENT == 'true' ? 'Yes' : 'No',
          });
        this.updateRequestInfo('earlyProjectClassification', 'projectClassificationDataForm', response[0]?.CLASSIFICATION_NUMBER);
        this.updateRequestInfo('earlyProjectClassificationDesc', 'projectClassificationDataForm', response[0]?.CLASSIFICATION_DESCRIPTION,);
        this.updateRequestInfo('registrationClassification','projectClassificationDataForm',response[0]?.REGISTRATION_CLASSIFICATION_AVAILABLE == 'true' ? true : false);
        this.updateRequestInfo('registrationRequirement','projectClassificationDataForm',response[0]?.REGISTRATION_REQUIREMENT == 'true' ? true : false);

        if(response[0]?.CLASSIFICATION_NUMBER==undefined && isThrowableError && notificationValidation){
          this.notificationService.error(
            'No Project Classification Data Available for the Input'
          );
        }

      }
    }, error => {
      if(isThrowableError && notificationValidation){
        this.notificationService.error(
          'No Project Classification Data Available for the Input'
        );
      }
      this.projectClassificationDataForm.patchValue(
        {
          earlyProjectClassification: '',
          earlyProjectClassificationDesc: '',
          registrationClassification: 'No',
          registrationRequirement: 'No',
        });
      this.updateRequestInfo('earlyProjectClassification', 'projectClassificationDataForm', null);
      this.updateRequestInfo('earlyProjectClassificationDesc', 'projectClassificationDataForm', null);
      this.updateRequestInfo('registrationClassification','projectClassificationDataForm',false);
      this.updateRequestInfo('registrationRequirement','projectClassificationDataForm',false)
    })


  }


  getToolTipDEata(data) {
    if (data && data.length) {
      let msg = "";
      data.forEach(res => {
        msg += res.name + " ";
      })
      return msg;
    }
  }

  /**
   * @author Baba
   * @param onInItCall to check lead formula boolean field during opening of component
   * @returns based on the conditions to be applied to lead formula field returns with or without calling generateClassificationchange API
   */
  disableLeadFormulaField(onInItCall?){
      let projectType=this.getRelationValue('projectType',this.projectClassificationDataForm.value.projectType)
    if(this.projectClassificationDataForm.get('newRnDFormula').value==true 
        && projectType==BusinessConstants.PROJECT_TYPES.COMMERCIAL_NEW_SKU){
          if(this.projectClassificationDataForm.get('newRnDFormula').enabled==true){
            this.projectClassificationDataForm.get('leadFormula').enable();
            this.projectClassificationDataForm.get('leadFormula').updateValueAndValidity()
          }
    }else{
      if(onInItCall){
        this.projectClassificationDataForm.get('leadFormula').disable();
        this.projectClassificationDataForm.get('leadFormula').updateValueAndValidity();
        return;
      }
      this.projectClassificationDataForm.get('leadFormula').setValue(false);
      this.projectClassificationDataForm.get('leadFormula').disable();
      this.projectClassificationDataForm.get('leadFormula').updateValueAndValidity();
      this.updateRequestInfo('leadFormula', 'projectClassificationDataForm', false);
    }
    if(onInItCall){
      return;
    }
    this.getClassificationChange(true)
  }




}
