import { DatePipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { GanttComponent, ContextMenuClickEventArgs, IGanttData, ContextMenuOpenEventArgs, ToolbarItem, ZoomTimelineSettings } from '@syncfusion/ej2-angular-gantt';
import { CommentsUtilService, IndexerService, LoaderService, NotificationService, ProjectConstant, ProjectService, SearchRequest, SearchResponse, SharingService, TaskService } from 'mpm-library';
import { forkJoin, Observable } from 'rxjs';
import { TaskCreationModalComponent } from 'src/app/core/mpm/project-view/task-creation-modal/task-creation-modal.component';
import { MonsterUtilService } from '../../services/monster-util.service';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';
import { GanttConfigConstants } from '../constants/gantt-config';
import { EntityListConstant } from '../../request-view/constants/EntityListConstant';
import { NpdCustomWorkflowModalComponent } from '../../npd-shared/npd-custom-workflow-modal/npd-custom-workflow-modal.component';
import { EntityService } from '../../services/entity.service';
import { NpdTaskService } from '../../npd-task-view/services/npd-task.service';
import { WorkFlowConstants } from '../../npd-shared/constants/workflow-customconfig';
import { NpdResourceManagementService } from '../../npd-resource-management/services/npd-resource-management.service';
import { Internationalization } from '@syncfusion/ej2-base';
import * as moment_ from 'moment';
import { TaskConfigConstant } from 'src/app/core/npd-ui/npd-task-view/constants/TaskConfig';
import { FilterConfigService } from '../../filter-configs/filter-config.service';
import { NpdProjectService } from '../../npd-project-view/services/npd-project.service';

const moment = moment_;
let instance: Internationalization = new Internationalization();
@Component({
  selector: 'app-npd-gantt-chart',
  templateUrl: './npd-gantt-chart.component.html',
  styleUrls: ['./npd-gantt-chart.component.scss'],
})
export class NpdGanttChartComponent implements OnInit {

  isEditModeChecked: boolean = false;
  allTaskPriorToCP1: any[];
  classificationNumber: any = '';

  constructor(private loaderService: LoaderService, private indexerService: IndexerService, private entityService: EntityService,
    private taskService: TaskService, private npdTaskService: NpdTaskService, public activatedRoute: ActivatedRoute, private projectService: ProjectService, private dialog: MatDialog,
    private notificationService: NotificationService, public datepipe: DatePipe, private monsterUtilService: MonsterUtilService,
    private npdResourceManagementService: NpdResourceManagementService, private commentsUtilService: CommentsUtilService,
    private filterConfigService: FilterConfigService, private sharingService: SharingService,
    public npdProjectService: NpdProjectService

  ) {
  }

  @ViewChild('gantt')
  public ganttObj: GanttComponent;
  public data: object[];
  public taskSettings: object;
  public columns: object[];
  public timelineSettings: object;
  public gridLines: string;
  public labelSettings: object;
  public toolbar: string[];
  public editSettings: object;
  editingData = [];
  enableResizing = true;
  selectedProject;
  projectId;
  project;
  allTaskConfigs = []
  allProjectTasks = [];
  allTargetTasks = [];
  ruleList = [];
  taskIdList = [];
  allCheckpoints = [];//gov milestones
  isEditMode;
  checkpointsConfig = JSON.parse(JSON.stringify(GanttConfigConstants.CheckpointConfig));
  public splitterSettings: object;
  public eventMarkers: object[];
  public filterSettings: object;
  public tooltipSettings: object;
  taskList = [];
  allSourceWorkFlowTasks = [];
  isTemplate;
  isActiveProject;
  hasTaskReorderAccess: boolean = false;
  userDisplayName;
  areTasksReordered: boolean = false;
  reorderedTasks = [];
  public zoomLevel = "100%";
  public zoomNum = 100;
  public tableHeight = 'calc(100vh - 151px )';
  //showGantt;
  //public projectStartDate: Date;
  //public projectEndDate: Date;

  editTask(eventData) {
    if (eventData.rowIndex > 0 && this.isEditMode && !(eventData?.data?.taskData?.isCheckPoint)) {
      const editTaskData = {
        TASK_NAME: eventData.data.TaskName,
        TASK_DESCRIPTION: eventData.data.TaskDescription
      }
      this.npdTaskService.setEditTaskHeaderData(editTaskData)
      // this.npdTaskService.setEditTaskHeaderData(eventData)
      this.getProjectDetails(eventData)
    }
  }

  getProjectDetails(eventData) {
    if (eventData.data.taskData.Status === 'Cancelled' || eventData.data.taskData.Status === 'CANCELLED') {
      return;
    }

    this.projectService.getProjectById(this.projectId)
      .subscribe(projectObj => {
        if (projectObj?.R_PO_STATUS["MPM_Status-id"]?.Id == '16399') {
          return
        }
        this.loaderService.show();
        this.loaderService.hide();
        

        if (projectObj) {
          this.selectedProject = projectObj;
          let obj = this.allProjectTasks.find(x => x.ID == eventData.data.TaskID);

          const dialogRef = this.dialog.open(TaskCreationModalComponent, {
            width: '38%',
            disableClose: true,
            data: {
              projectId: this.projectId,
              isTemplate: false,
              taskId: eventData.data?.TaskID,
              taskItemId: eventData.data?.TaskItemId,
              projectObj: this.selectedProject,
              taskConfig: eventData.data?.TaskName,
              isTaskActive: eventData.data?.taskData?.taskActive,//eventData.data?.IS_ACTIVE_TASK,
              isApprovalTask: false,
              modalLabel: 'Edit Task',
              alltask: this.allTargetTasks,
              selectedTask: {
                NPD_TASK_IS_CP1_COMPLETED: eventData.data?.taskData?.isCP1Completed
              },
              selectedtask: obj

            }
          });
          dialogRef.afterClosed().subscribe(result => {
            this.refresh();
            console.log('Task Dialog closed', result);
          });
        }
      }, (error) => {
        this.loaderService.hide();
      });
  }

  /*     rowDragStartHelper(args: any) {
          //var record = args.data[0] ? args.data[0] : args.data;
          //var taskId = record.ganttProperties.taskId;
          if (args.data?.taskData?.Status =='Completed' ) {
              args.cancel = true;
          }
      }; */


  checkIsProjectOwner(projOwner) {
    if (projOwner == this.sharingService.getcurrentUserItemID()) {
      return true;
    }
    return false;
  }

  checkIsTaskOwner(taskOwner) {
    return (taskOwner == this.sharingService.getCurrentUserItemID());
  }

  validateAccess(eventData) {
   
    // const tasks = TaskConfigConstant.allTaskConfig;
    const tasks = TaskConfigConstant.TaskAccessRolesWithTaskRole;
    let no = this.getTaskNo(eventData?.data?.taskData?.TaskName);
    const task = this.allProjectTasks.find(task=>task.ID==eventData?.data?.taskData?.TaskID);
    const taskRole = task?.TASK_ROLE_VALUE;
    const currentTaskIndex = tasks.findIndex(ele => ele.taskOwner[0] == taskRole);
    const roles = [];
    const taskOwner = [];
    let isTaskSuperAccess = (this.filterConfigService.isE2EPMRole() || this.filterConfigService.isProgramManagerRole() || this.filterConfigService.isCorpPMRole() || this.checkIsProjectOwner(eventData?.data?.taskData?.projectOwnerId));
    if (currentTaskIndex != -1) {

      let isUserRoleMatched = false;
      let isTaskUserRoleMatched = false;
      let userDetails = JSON.parse(sessionStorage.getItem('USER_DETAILS_WORKFLOW')).User.ManagerFor.Target;

      tasks[currentTaskIndex].role.forEach((item, i) => {
        roles[i] = userDetails.some(ele => ele.Name == item);
      });
      isUserRoleMatched = roles.some(ele => ele == true);
      tasks[currentTaskIndex].taskOwner.forEach((item, i) => {
        taskOwner[i] = userDetails.some(ele => ele.Name == item);
      });
      isTaskUserRoleMatched = taskOwner.some(ele => ele == true);

      if (isTaskSuperAccess) {
        return true;
      } else if ((eventData?.data?.taskData?.taskActive == 'true' || eventData?.data?.taskData?.taskActive === true) && (isUserRoleMatched || isTaskUserRoleMatched || this.checkIsTaskOwner(eventData?.data?.taskData?.taskOwnerId))) {//this.checkIsTaskOwner()
        return true;
      } else {
        let hasTaskOwnerAccess = this.checkIsTaskOwner(eventData?.data?.taskData?.taskOwnerId);
        return hasTaskOwnerAccess ? true : false;
      }
    } else {
      if (isTaskSuperAccess) {
        return true;
      } else if ((eventData?.data?.taskData?.taskActive == 'true' || eventData?.data?.taskData?.taskActive === true) && (this.checkIsTaskOwner(eventData?.data?.taskData?.taskOwnerId))) {//this.checkIsTaskOwner()
        return true;
      } else {
        let hasTaskOwnerAccess = this.checkIsTaskOwner(eventData?.data?.taskData?.taskOwnerId);
        return hasTaskOwnerAccess ? true : false;
      }
    }
  }

  //enableTaskbarTooltip;
  //enableTaskbarDragTooltip;
  //hideTooltip = false;

  resizeTask(taskargs) {
    
    //this.hideTooltip = true;
   
    //this.enableTaskbarDragTooltip = false;
    //this.enableTaskbarTooltip = false;
    if (taskargs.data?.taskData?.Status == 'Approved/Completed') {
      this.notificationService.warn('Task is in completed state, cannot be updated.')
      this.refresh();
      return;
    }
    if ((taskargs.data?.taskData?.isCheckPoint)) {
      this.notificationService.warn('Check point cannot be updated.')
      this.refresh();
      return;
    }
    if (!this.validateAccess(taskargs)) {
      this.notificationService.warn('User do not have access to perform the operation.');
      this.refresh();
      return;
    }
    if (taskargs.recordIndex > 0 && (taskargs.data?.taskData?.Status != 'Approved/Completed') && !(taskargs.data?.taskData?.isCheckPoint)) {
      let taskRequestObj = {
        TaskObject: {
          TaskId: {
            Id: taskargs.data.TaskID,
          },
        }
      }
      let customTaskRequestObj = {
        Task: {
          Id: taskargs.data.TaskID,
          ItemId: taskargs?.data?.taskData?.TaskItemId,
        },
      }
      let startDate = new Date(taskargs.data?.StartDate)?.toISOString();//(args.data.EndDate).toISOString()
      let startDateWeek = this.monsterUtilService.calculateWeek(startDate);
      let startDateWeekNo = this.monsterUtilService.calculateWeekNo(startDate);

      let dueDate = new Date(taskargs.data?.EndDate)?.toISOString();//(args.data.EndDate).toISOString()
      let dueDateWeek = this.monsterUtilService.calculateWeek(dueDate);
      let dueDateWeekNo = this.monsterUtilService.calculateWeekNo(dueDate);


      let difference1 = (dueDateWeekNo - startDateWeekNo) + 1;
      

      let taskStartWithNoHours = startDate.split('T')[0];
      let taskEndWithNoHours = dueDate.split('T')[0];
      let difference = taskStartWithNoHours && taskEndWithNoHours && this.getDuration(new Date(taskStartWithNoHours), new Date(taskEndWithNoHours));
      

      //taskRequestObj.TaskObject['TaskDuration'] = difference;

      if (taskargs?.taskBarEditAction == "RightResizing") {

        //const [ plannedStartDate,  ] = this.monsterUtilService.getStartEndDate(taskargs.data?.taskData?.plannedStartDate);
        const plannedStartDate = new Date(taskargs.data?.taskData?.plannedStartDate)?.toISOString();
        let plannedEndDate = this.getFinalDate(plannedStartDate, difference);
        let plannedEndDateWeek = this.monsterUtilService.calculateWeek(plannedEndDate);

        [, taskRequestObj.TaskObject['DueDate']] = this.monsterUtilService.getStartEndDate(plannedEndDateWeek);
        [, customTaskRequestObj['PlannedDueDate']] = this.monsterUtilService.getStartEndDate(plannedEndDateWeek);
        taskRequestObj.TaskObject['TaskDuration'] = difference;
        this.updateTask(taskRequestObj, customTaskRequestObj);//updating end date
      } else if (taskargs?.taskBarEditAction == "LeftResizing") {
        //const [ plannedStartDate,  ] = this.monsterUtilService.getStartEndDate(taskargs.data?.taskData?.plannedStartDate);
        const plannedStartDate = new Date(taskargs.data?.taskData.plannedStartDate)?.toISOString();
        let plannedEndDate = this.getFinalDate(plannedStartDate, difference);
        let plannedEndDateWeek = this.monsterUtilService.calculateWeek(plannedEndDate);

        [, taskRequestObj.TaskObject['DueDate']] = this.monsterUtilService.getStartEndDate(plannedEndDateWeek);
        [, customTaskRequestObj['PlannedDueDate']] = this.monsterUtilService.getStartEndDate(plannedEndDateWeek);
        [customTaskRequestObj['ActualStartDate'],] = this.monsterUtilService.getStartEndDate(startDateWeek);
        taskRequestObj.TaskObject['TaskDuration'] = difference;
        this.updateTask(taskRequestObj, customTaskRequestObj);//updating start date */
      } else if (taskargs?.taskBarEditAction == "ChildDrag") {
        [customTaskRequestObj['ActualStartDate'],] = this.monsterUtilService.getStartEndDate(startDateWeek);
        this.updateNpdTask(customTaskRequestObj);
      }
    }
  }

  /*    resizeTask(taskargs) {
          console.log(taskargs);
          if(taskargs.data?.taskData?.Status == 'Approved/Completed' ) {
              this.notificationService.warn('Task is in completed state, cannot be updated.')
              this.refresh();
              return;
          }
          if((taskargs.data?.taskData?.isCheckPoint)) {
              this.notificationService.warn('Check point cannot be updated.')
              this.refresh();
              return;
          }
          if(taskargs.recordIndex > 0 && (taskargs.data?.taskData?.Status !='Approved/Completed') && !(taskargs.data?.taskData?.isCheckPoint) ) {
              let taskRequestObj = {
                  TaskObject : {
                      TaskId : {
                          Id : taskargs.data.TaskID,
                      },
                  }
              }
              let customTaskRequestObj = {
                  Task : {
                      Id : taskargs.data.TaskID,
                      ItemId: taskargs?.data?.taskData?.TaskItemId,
                  },
              }
              let startDate  = new Date(taskargs.data?.StartDate)?.toISOString();//(args.data.EndDate).toISOString()
              let startDateWeek = this.monsterUtilService.calculateWeek(startDate);
              let startDateWeekNo = this.monsterUtilService.calculateWeekNo(startDate);

              let dueDate  = new Date(taskargs.data?.EndDate)?.toISOString();//(args.data.EndDate).toISOString()
              let dueDateWeek = this.monsterUtilService.calculateWeek(dueDate);
              let dueDateWeekNo = this.monsterUtilService.calculateWeekNo(dueDate);


              let difference = (dueDateWeekNo - startDateWeekNo) + 1;
              taskRequestObj.TaskObject['TaskDuration'] = difference;

              if(taskargs?.taskBarEditAction == "RightResizing") {
                 // taskRequestObj.TaskObject['DueDate'] = new Date(taskargs.data?.EndDate)?.toISOString();
                  [ , taskRequestObj.TaskObject['DueDate']] = this.monsterUtilService.getStartEndDate(dueDateWeek);
                  [, customTaskRequestObj['PlannedDueDate']] = this.monsterUtilService.getStartEndDate(dueDateWeek);
                  this.updateTask(taskRequestObj,customTaskRequestObj);//updating end date
              } else if(taskargs?.taskBarEditAction == "LeftResizing") {
                  //taskRequestObj.TaskObject['StartDate'] = new Date(taskargs.data?.StartDate)?.toISOString();

                  [taskRequestObj.TaskObject['StartDate'], ] = this.monsterUtilService.getStartEndDate(startDateWeek);
                  //[customTaskRequestObj['ActualStartDate'],] = this.monsterUtilService.getStartEndDate(startDateWeek);
                  this.updateTask(taskRequestObj,customTaskRequestObj);//updating start date
              } else if(taskargs?.taskBarEditAction == "ChildDrag") {
                  //taskRequestObj.TaskObject['StartDate'] = new Date(taskargs.data?.StartDate)?.toISOString();
                  //taskRequestObj.TaskObject['DueDate'] = new Date(taskargs.data?.EndDate)?.toISOString();

                  [ , taskRequestObj.TaskObject['DueDate']] = this.monsterUtilService.getStartEndDate(dueDateWeek);
                  [taskRequestObj.TaskObject['StartDate'], ] = this.monsterUtilService.getStartEndDate(startDateWeek);
                  //[customTaskRequestObj['ActualStartDate'],] = this.monsterUtilService.getStartEndDate(startDateWeek);
                  [, customTaskRequestObj['PlannedDueDate']] = this.monsterUtilService.getStartEndDate(dueDateWeek);
                  this.updateTask(taskRequestObj,customTaskRequestObj);//updating start date & end date
              }
          }
      } */


  updateTask(requestObj, customTaskRequestObj) {

    this.loaderService.show();
    this.taskService.updateTask(requestObj).subscribe(response => {
      if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
        const eventData = { message: 'Task updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
        //this.notificationService.success(eventData.message);
        //this.refresh();
        //this.ngOnInit();
        this.updateNpdTask(customTaskRequestObj);
      } else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
        && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
        const eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
        this.notificationService.error(eventData.message);
        this.loaderService.hide();
      } else {
        const eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
        this.notificationService.error(eventData.message);
        this.loaderService.hide();
      }
    }, error => {
      this.loaderService.hide();
      const eventData = { message: 'Something went wrong while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
      this.notificationService.error(eventData.message);
    });
  }

  updateNpdTask(customTaskObject) {
    customTaskObject.TaskOperation = 'EDIT'
    const requestObj = {
      afterTaskEditActions: customTaskObject,
    };
    this.npdTaskService.updateTask(requestObj).subscribe(response => {
      this.loaderService.hide();
      this.refresh();
      if (response && response.data) {
        const eventData = { message: 'Task updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
        this.notificationService.success(eventData.message);
      } else {
        const eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
        this.notificationService.success(eventData.message);
      }
    }, error => {
      this.loaderService.hide();
      const eventData = { message: 'Something went wrong while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
      this.notificationService.success(eventData.message);
    });
  }

  /* npd logic */
  // Forming search field for mpm-indexer input
  getSearchField(type, field_id, operator_id, operator_name, value?, field_operator?) {

    let searchField = {
      "type": type,
      "field_id": field_id,
      "relational_operator_id": operator_id,
      "relational_operator_name": operator_name,
      "value": value,
      "relational_operator": field_operator
    }
    return searchField;
  }

  getProjectDetailsById(projectId): Observable<any> {
    let searchConditionList = [
      this.getSearchField("string", "CONTENT_TYPE", "MPM.OPERATOR.CHAR.IS", "is", "MPM_PROJECT"),
      this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", projectId, "AND"),
    ];

    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: "",
      search_condition_list: {
        search_condition: searchConditionList
      },
      facet_condition_list: {
        facet_condition: []
      },
      sorting_list: {
        sort: []
      },
      cursor: {
        page_index: 0,
        page_size: 1
      }
    };
    return new Observable(observer => {
      this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
        if (response.data.length > 0) {
          // this.classificationNumber = this.npdProjectService.getProjectClassificationNumber(response.data[0]);
          this.project = response.data[0];
          this.isActiveProject = this.project.PROJECT_STATUS_VALUE == "Active" ? true : false;
          this.hasTaskReorderAccess = this.hasTaskReorderAccess
            || (this.filterConfigService.isE2EPMRole() && this.userDisplayName == this.project?.NPD_PROJECT_E2E_PM_NAME)
            || (this.filterConfigService.isCorpPMRole() && this.userDisplayName == this.project?.NPD_PROJECT_CORP_PM_NAME)
            || (this.filterConfigService.isOPSPMRole() && this.userDisplayName == this.project?.NPD_PROJECT_OPS_PM_NAME)

        }
        observer.next(response.data);
        observer.complete();
      }, () => {
        observer.error();
      });
    });
  }

  getUniqueArrayValues(arrayList) {
    let uniqueValueList = arrayList.filter((c, index) => {
      return arrayList.indexOf(c) === index;
    });
    return uniqueValueList;
  }

  getTaskDetailsByProjectId(projectItemId, taskIdList) {
    let searchConditionList = [
      this.getSearchField("string", "CONTENT_TYPE", "MPM.OPERATOR.CHAR.IS", "is", "MPM_TASK"),
    ];
    taskIdList = this.getUniqueArrayValues(taskIdList);
    taskIdList.forEach((task, index) => {
      if (index == 0) {
        searchConditionList.push(this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", task, "AND"))
      } else {
        searchConditionList.push(this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", task, "OR"))
      }
    });
    let operation = 'OR';
    (taskIdList?.length) > 0 ? (operation = 'OR') : (operation = 'AND');

    let projectSearchCondition = [
      this.getSearchField("string", "PROJECT_ITEM_ID", "MPM.OPERATOR.CHAR.IS", "is", projectItemId, operation)
    ]
    searchConditionList = searchConditionList.concat(projectSearchCondition);
    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: "",
      search_condition_list: {
        search_condition: searchConditionList
      },
      facet_condition_list: {
        facet_condition: []
      },
      sorting_list: {
        sort: [
          {
            field_id: "TASK_START_DATE",//ID
            order: "ASC"
          }
        ]
      },
      cursor: {
        page_index: 0,
        page_size: 200
      }
    };
    return new Observable(observer => {
      this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
        if (response.data.length > 0) {
          this.allProjectTasks = response.data;
          this.allTargetTasks = [];
          this.allProjectTasks.forEach(task => {
            if (task.PROJECT_ITEM_ID == projectItemId) {
              this.allTargetTasks.push(task);
            }
          });
          /*  this.allProjectTasks.sort((a, b) => {
               let taskNo1 = this.getTaskNo(a?.TASK_NAME);
               let taskNo2 = this.getTaskNo(b?.TASK_NAME);
               return taskNo1 - taskNo2
           }); */
        }
        observer.next(true);
        observer.complete();
      }, () => {
        observer.error();
      });
    });
  }

  getAllGovernanceMilestones(): Observable<any> {
    let requestId = this.project?.PR_REQUEST_ITEM_ID;//?.split('.')[1];
    return new Observable(observer => {
      this.entityService.getEntityItemDetails(requestId,
        EntityListConstant.TECHNICAL_REQUEST_GOVERNANCE_MILESTONE_RELATION).subscribe((governanceMilestoneResponse) => {
          if (governanceMilestoneResponse && governanceMilestoneResponse.items) {
            this.allCheckpoints = governanceMilestoneResponse.items;
          }
          observer.next([]);
          observer.complete();
        }, (error) => {
          observer.error(error);
        });
    });
  }

  getTaskNo(taskName) {
    let taskNo = taskName?.replace(" ", "");
    taskNo = taskNo?.replace("Task", "");
    taskNo = taskNo?.replace("TASK", "");
    taskNo = taskNo?.replace("A", "");
    return taskNo
  }

  getAllWorkflowRules(): Observable<any> {
    return new Observable(observer => {
      this.npdTaskService.getPrimaryWorkflowRulesByProject(this.projectId,this.classificationNumber).subscribe(workflowResponse => {
        this.taskIdList = [];
        this.ruleList = [];
        if (workflowResponse?.tuple) {
          let workflowRules;
          if (workflowResponse?.tuple?.length > 0) {
            workflowRules = workflowResponse.tuple;
          } else {
            workflowRules = [workflowResponse.tuple];
          }
          workflowRules.forEach(workFlowRule => {
            //const currentTask = this.taskData.find(task => task.ID === rule.R_PO_TASK['Task-id'].Id);
            let rule = workFlowRule?.old?.WorkflowRules;
            if (rule && rule.TRIGGER_TYPE === 'STATUS' && rule.TARGET_TYPE === 'TASK') {
              this.ruleList.push({
                currentTask: rule.TASK_ID,
                triggerType: rule.TRIGGER_TYPE,
                triggerData: rule.STATUS_ID,//actionIdrule.TRIGGER_TYPE === 'STATUS' ? rule.STATUS_ID : '',
                targetType: rule.TARGET_TYPE,
                targetData: rule.TARGET_TASK_ID,//rule.TARGET_TYPE === 'TASK' ? rule.R_PO_TARGET_TASK['Task-id'].Id : rule.R_PO_EVENT['MPM_Events-id'].Id,
                workflowRuleId: rule.ID,
              });//fetch rules based on trigger type status alone & TARGET_TYPE === 'TASK'
              this.taskIdList.push(rule.TASK_ID);
            }
          });
          //this.getAllWorkflowTaskIndexerDetails(this.taskIdList).subscribe();
          //this.formProjectSubTasks(project,true);
        } else {
          //this.formProjectSubTasks(project,false);
        }
        observer.next(this.ruleList);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
  }

  /*     getAllWorkflowRules():Observable<any> {
          return new Observable(observer => {
              this.taskService.getWorkflowRulesByProject(this.projectId).subscribe(workflowResponse => {
                  this.taskIdList = [];
                  this.ruleList = [];
                  if (workflowResponse) {
                      let workflowRules;
                      if (workflowResponse && workflowResponse.length > 0) {
                          workflowRules = workflowResponse;
                      } else {
                          workflowRules = [workflowResponse];
                      }
                      workflowRules.forEach(rule => {
                      //const currentTask = this.taskData.find(task => task.ID === rule.R_PO_TASK['Task-id'].Id);
                      if(rule.TRIGGER_TYPE === 'STATUS' && rule.TARGET_TYPE === 'TASK') {
                          this.ruleList.push({
                              currentTask: rule.R_PO_TASK['Task-id'].Id,
                              triggerType: rule.TRIGGER_TYPE,
                              triggerData: rule.TRIGGER_TYPE === 'STATUS' ? rule.R_PO_STATUS['MPM_Status-id'].Id : rule.R_PO_ACTIONS['MPM_Workflow_Actions-id'].Id,
                              targetType: rule.TARGET_TYPE,
                              targetData: rule.TARGET_TYPE === 'TASK' ? rule.R_PO_TARGET_TASK['Task-id'].Id : rule.R_PO_EVENT['MPM_Events-id'].Id,
                              workflowRuleId: rule['Workflow_Rules-id'].Id,
                            });//fetch rules based on trigger type status alone & TARGET_TYPE === 'TASK'
                            this.taskIdList.push(rule.R_PO_TASK['Task-id'].Id);
                      }
                    });
                    //this.getAllWorkflowTaskIndexerDetails(this.taskIdList).subscribe();
                    //this.formProjectSubTasks(project,true);
                  } else {
                    //this.formProjectSubTasks(project,false);
                  }
                  observer.next(this.ruleList);
                  observer.complete();
                },error => {
                  observer.error(error);
              });
          });
      } */

  /*     if(this.project?.PR_REQUEST_ID_string) {
          let startDate =this.datepipe.transform(this.project?.PROJECT_START_DATE, 'MM/dd/yyyy');
          this.eventMarkers = [
              { day:  new Date(this.project?.PROJECT_START_DATE), label: 'Request Id - '+this.project?.PR_REQUEST_ID_string}
          ];
      } */
  getAllTaskConfigs(): Observable<any> {
    this.allTaskConfigs = [];
    return new Observable(observer => {
      this.npdResourceManagementService.fetchAllTaskRolesByClassification(this.classificationNumber).subscribe((response:any) => {
        
        this.allTaskConfigs = response;
        observer.next(this.allTaskConfigs);
        observer.complete();
      }, (error) => {
        observer.error();
      });
    });
  }
  getAllTaskPriorToCP1(projectId): Observable<any> {
    this.allTaskPriorToCP1 = [];
    let parameter = {
      // TaskSequesnce: 7,
      ProjectId: projectId,
      // classificationNumber: this.classificationNumber
    }
    return new Observable(observer => {
      this.npdResourceManagementService.getAllTaskPriorToCP1(parameter).subscribe(response => {
        

        response?.tuple?.forEach(task => {
          this.allTaskPriorToCP1.push(task.old.WorkflowRules.TASK_ID)
        });
        observer.next(this.allTaskPriorToCP1);
        observer.complete();
      }, (error) => {
        observer.error();
      });
    });
  }

  /**
   * @author Manikanta
   * @description positions the new tasks appropriately after task reordering happens.
   * @param newTasksList
   */
  insertNewTasksAtProperIndex(newTasksList) {
    newTasksList.forEach(task => {
      let condition = true;
      let seq = task.sequence;
      while (condition) {
        const index = this.allProjectTasks.findIndex(eachTask => eachTask.sequence == seq);
        if (index >= 0) {
          condition = false;
          if (index + 1 >= this.allProjectTasks.length - 1) {
            this.allProjectTasks.splice(this.allProjectTasks.length, 0, task);
          } else {
            this.allProjectTasks.splice(index + 1, 0, task);
          }
        } else {
          seq = seq - 1;
        }
      }
    });
  }

  sortTasks() {
    this.allProjectTasks.sort((a, b) => {
      let taskNo1 = a?.sequence;
      let taskNo2 = b?.sequence;
      return taskNo1 - taskNo2
    });
  }

  /**
   * @author Manikanta
   * @description Maintains task order based on user preference
   */
  reorderTasks() {
    let newOrderTasks = [];
    let newTasks = []
    this.reorderedTasks.forEach(task => {
      let currentTask = this.allProjectTasks.find(eachTask => task.TaskId == eachTask.ID);
      if (currentTask) {
        newOrderTasks.push(currentTask);
      }
    })
    this.allProjectTasks.forEach(eachTask => {
      let currentTask = this.reorderedTasks.find(task => task.TaskId == eachTask.ID);
      if (!currentTask) {
        newTasks.push(eachTask);
      }
    })
    this.allProjectTasks.length = 0;
    this.allProjectTasks = [...newOrderTasks];
    if (newTasks.length > 0) {
      this.insertNewTasksAtProperIndex(newTasks);
    }


  }

  /**
   * @author Manikanta
   * @param params
   */
  async getProjectTasksSequence(params) {
    let getProjectTaskSequenceRes = await this.npdTaskService.getProjectTaskSequence(params);
    getProjectTaskSequenceRes.subscribe(res => {
      if (res.NPD_Task_Sequence && res.NPD_Task_Sequence.length > 0) {
        this.reorderedTasks.length = 0;
        this.areTasksReordered = true;
        this.reorderedTasks = res.NPD_Task_Sequence;
      } else {
        this.areTasksReordered = false;
        this.reorderedTasks.length = 0;
      }
    })
  }

  getAllDetails() {
    this.loaderService.show();
    this.getAllTaskPriorToCP1(this.projectId).subscribe();
    forkJoin([this.getProjectDetailsById(this.projectId)])
      .subscribe(forkedData => {
        if (forkedData?.[0]) {
          // this.getAllTaskPriorToCP1(this.projectId).subscribe();
          let isCp1ApprovedInGovernance = forkedData[0][0]?.NPD_PROJECT_CP1_DECISION === 'Approved'
          this.loaderService.show();
          //this.getAllGovernanceMilestones().subscribe(response=> {
          const params = { ProjectId: this.projectId };
          this.getProjectTasksSequence(params);
          this.getTaskDetailsByProjectId(this.project?.ITEM_ID, this.taskIdList).subscribe(taskResponse => {
            let taskStartISO = this.project?.PROJECT_START_DATE && new Date(this.project.PROJECT_START_DATE).toISOString();
            let taskEndISO = this.project?.PROJECT_DUE_DATE && new Date(this.project.PROJECT_DUE_DATE).toISOString();
            let taskStartWithNoHours;
            let taskEndWithNoHours;
            if (taskStartISO) {
              taskStartWithNoHours = taskStartISO.split('T')[0];
            }
            if (taskEndISO) {
              taskEndWithNoHours = taskEndISO.split('T')[0];
            }
            let weekDiff = taskStartWithNoHours && taskEndWithNoHours && this.getDuration(new Date(taskStartWithNoHours), new Date(taskEndWithNoHours));
            let project = {
              TaskID: this.project?.ID,
              TaskName: this.project?.PROJECT_NAME,
              StartDate: taskStartWithNoHours && new Date(taskStartWithNoHours),//new Date(this.project?.PROJECT_START_DATE),
              EndDate: taskEndWithNoHours && new Date(taskEndWithNoHours),//new Date(this.project?.PROJECT_END_DATE),
              StartWeek: this.monsterUtilService.calculateWeek(taskStartWithNoHours),//this.monsterUtilService.calculateWeek(this.project?.PROJECT_START_DATE),
              EndWeek: this.monsterUtilService.calculateWeek(taskEndWithNoHours),//this.monsterUtilService.calculateWeek(this.project?.PROJECT_END_DATE),
              CP1Date: this.project?.NPD_PROJECT_CP1_AGREEMENT_DATE,//NPD_PROJECT_CP1_REVISED_DATE,
              currentCommercialAlignedDate: this.project?.NPD_PROJECT_OPS_ALIGNED_DP_WEEK,
              pmSlackTime: this.project?.NPD_PROJECT_PROGRAM_MANAGER_SLACK_TIME,
              TaskDescription: '',
              subtasks: [],
              weekDuration: weekDiff,
              governedDeliveryDate: this.project?.NPD_PROJECT_GOVERNED_DELIVERY_DATE,
              currentTimeline: this.project?.NPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK,
              showCommentsIcon: false
            }
            //sort tasks by sequence
          

            this.classificationNumber=this.npdProjectService.getClassificationNumberByTasksStatus(forkedData?.[0],this.allProjectTasks);
            this.getAllTaskConfigs().subscribe(taskConfigResponse=>{
              let maxSequenceValue = (this.allTaskConfigs &&
                Math.max.apply(Math, this.allTaskConfigs.map(task => { return task.TaskSequence; })))
                + 1;
              this.allProjectTasks.forEach(projectTask => {
                let filteredTask = this.allTaskConfigs.find(task => task.TaskName == projectTask.TASK_NAME)
                if (filteredTask) {
                  projectTask.sequence = filteredTask.TaskSequence;
                } else {
                  projectTask.sequence = maxSequenceValue;
                }
              }
              );
              if (this.areTasksReordered) {
                this.reorderTasks();
              }
              else {
                this.sortTasks();
              }
            })
            this.getAllWorkflowRules().subscribe(response=>{
              if (response) {//
                this.formProjectSubTasks(project, true, isCp1ApprovedInGovernance);
                this.columnsConfig();
              } else {
                this.formProjectSubTasks(project, false, isCp1ApprovedInGovernance);
                this.columnsConfig();
              }
            })

            this.loaderService.hide();
          }, error => {
            this.loaderService.hide();
          });
          //})
        }
      }, error => {
        console.log(error);
        this.loaderService.hide();
        const eventData = { message: 'Something went wrong while fetching details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
        this.notificationService.error(eventData.message);
      });
  }


  columnsConfig() {
    this.ganttObj.hideColumn(['Dependency']); //Hiding predeccesor-dependency columns for gantt chart
    this.ganttObj.hideColumn(['startDate']);
    this.ganttObj.hideColumn(['endDate']);
    this.ganttObj.hideColumn(['isCheckPoint']);
    this.ganttObj.showColumn(['StartWeek']);
    this.ganttObj.showColumn(['EndWeek']);
    //this.ganttObj.renderGantt();
    this.ganttObj.refresh();
  }



  getAllWorkflowTaskIndexerDetails(taskIdList) {//of diff projects too
    this.loaderService.show();
    let searchConditionList = [
      this.getSearchField("string", "CONTENT_TYPE", "MPM.OPERATOR.CHAR.IS", "is", "MPM_TASK"),
      //this.getSearchField("string", "PROJECT_ITEM_ID", "MPM.OPERATOR.CHAR.IS", "is", projectItemId, "AND"),
    ];
    taskIdList.forEach((task, index) => {
      if (index == 0) {
        searchConditionList.push(this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", task, "AND"))
      } else {
        searchConditionList.push(this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", task, "OR"))
      }
    });

    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: "",
      search_condition_list: {
        search_condition: searchConditionList
      },
      facet_condition_list: {
        facet_condition: []
      },
      sorting_list: {
        sort: [
          {
            field_id: "TASK_START_DATE",//ID
            order: "ASC"
          }
        ]
      },
      cursor: {
        page_index: 0,
        page_size: 100
      }
    };
    return new Observable(observer => {
      this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
        if (response.data.length > 0) {
          this.allSourceWorkFlowTasks = response.data;
        }
        this.loaderService.hide();
        observer.next(true);
        observer.complete();
      }, () => {
        this.loaderService.hide();
        observer.error();
      });
    });

  }

  getFinalDate(startDate, duration) {
    let endDateObject = this.commentsUtilService.addUnitsToDate(startDate, duration, 'weeks', true);
    let endDateActual = this.npdTaskService.subTractUnitsToDate(endDateObject, 1, 'days');
    return endDateActual;
  }

  getPlannedStartDate(date) {
    let plannedDate = new Date(date);
    if (new Date().getTimezoneOffset() > 0) {
      plannedDate = new Date(plannedDate.getFullYear(), plannedDate.getMonth(), plannedDate.getDate() + 1);
    }
    return plannedDate;
  }

  formProjectSubTasks(project, isWorkflow, isCp1ApprovedInGovernance) {
    //this.data = [];
    let isCp1Approved: boolean;
    this.checkpointsConfig = JSON.parse(JSON.stringify(GanttConfigConstants.CheckpointConfig));
    this.allProjectTasks.forEach(task => {
      let dependentTasks = [];
      if (isWorkflow) {
        let taskSpecificFilteredRuleDetails = this.ruleList.filter(workflowRule => workflowRule.targetData == task.ID)
        if (taskSpecificFilteredRuleDetails && taskSpecificFilteredRuleDetails.length > 0) {
          taskSpecificFilteredRuleDetails.forEach(workflowRule => {
            dependentTasks.push(workflowRule.currentTask);
          })
        }
      }
      let baselineStartWithNoHours;
      let baselineEndWithNoHours;
      let baseLineStartDate = task.NPD_TASK_BASE_LINE_START_WEEK;
      let baseLineEndDate = task.NPD_TASK_BASE_LINE_END_WEEK;
      if (baseLineStartDate) {
        isCp1Approved = true;
        baselineStartWithNoHours = baseLineStartDate.split('T')[0]
      }
      if (baseLineEndDate) {
        isCp1Approved = true;
        baselineEndWithNoHours = baseLineEndDate.split('T')[0]
      }
      if (task.TASK_NAME != 'CP0') {
        if (this.checkpointsConfig.find(cp => cp.value == task?.TASK_NAME)) {

          let taskStartISO = (task?.NPD_TASK_ACTUAL_START_DATE && task.NPD_TASK_ACTUAL_START_DATE != 'NA' &&
            task.NPD_TASK_ACTUAL_START_DATE != '') ? task?.NPD_TASK_ACTUAL_START_DATE : task.TASK_START_DATE
          let taskEndISO = (task?.NPD_TASK_ACTUAL_DUE_DATE && task.NPD_TASK_ACTUAL_DUE_DATE != 'NA' && task.NPD_TASK_ACTUAL_DUE_DATE != '') ? task?.NPD_TASK_ACTUAL_DUE_DATE : task.TASK_DUE_DATE

          let taskStartWithNoHours;
          let taskEndWithNoHours;

          if (taskStartISO) {
            taskStartWithNoHours = taskStartISO.split('T')[0];
          }
          if (taskEndISO) {
            taskEndWithNoHours = taskEndISO.split('T')[0];
          }
          let weekDiff = this.getDuration(new Date(this.getDateFormat(taskStartWithNoHours)), new Date(this.getDateFormat(taskEndWithNoHours)));
          
          //mapping checkpoints to group of tasks
          project.subtasks.push(
            {
              TaskID: task.ID,//'',
              TaskName: task.TASK_NAME,
              TaskDescription: task.TASK_DESCRIPTION,
              isCheckPoint: true,
              //duration: 0,
              Duration: 0,
              StartDate: new Date(taskStartISO),
              //taskStartWithNoHours && new Date(this.getDateFormat(taskStartWithNoHours)),//taskStartWithNoHours && new Date(taskStartWithNoHours),
              //EndDate: '',//taskEndWithNoHours && new Date(this.getDateFormat(taskEndWithNoHours)),
              StartWeek: taskStartWithNoHours && this.monsterUtilService.calculateWeek(taskStartWithNoHours),//checkPointDetail?.Properties?.Target_Start_Date && this.monsterUtilService.calculateWeek(checkPointDetail?.Properties?.Target_Start_Date),
              EndWeek: taskEndWithNoHours && this.monsterUtilService.calculateWeek(taskEndWithNoHours), //checkPointDetail?.Properties?.Decision_Date && this.monsterUtilService.calculateWeek(checkPointDetail.Properties?.Decision_Date)
              weekDuration: 0,
              Predecessor: dependentTasks?.toString(),
              baseLineStartWeek: this.allTaskPriorToCP1.includes(task.ID) ? baselineStartWithNoHours : baselineStartWithNoHours ? (this.monsterUtilService.calculateWeek(baselineStartWithNoHours)) : (isCp1ApprovedInGovernance ? (task?.NPD_TASK_IS_ADHOC_TASK ? '0' : (isCp1ApprovedInGovernance && !GanttConfigConstants.TASKS_PRIOR_TO_CP1.includes(task.TASK_NAME)) ? 'NA' : '') : ''),
              baseLineEndWeek: this.allTaskPriorToCP1.includes(task.ID) ? baselineEndWithNoHours : baselineEndWithNoHours ? (this.monsterUtilService.calculateWeek(baselineEndWithNoHours)) : (isCp1ApprovedInGovernance ? (task?.NPD_TASK_IS_ADHOC_TASK ? '0' : (isCp1ApprovedInGovernance && !GanttConfigConstants.TASKS_PRIOR_TO_CP1.includes(task.TASK_NAME)) ? 'NA' : '') : ''),
              baseLineDuration: this.allTaskPriorToCP1.includes(task.ID) ? task.NPD_TASK_BASELINE_DURATION : task.NPD_TASK_BASELINE_DURATION == 0 ? 0 : task.NPD_TASK_BASELINE_DURATION ? (task.TASK_NAME.includes('CP') ? 0 : task.NPD_TASK_BASELINE_DURATION < 0 ? 0 : task.NPD_TASK_BASELINE_DURATION) : (isCp1ApprovedInGovernance ? (task?.NPD_TASK_IS_ADHOC_TASK ? '0' : (isCp1ApprovedInGovernance && !GanttConfigConstants.TASKS_PRIOR_TO_CP1.includes(task.TASK_NAME)) ? 'NA' : '') : ''),
              standardDuration: task?.TASK_NAME?.includes('CP') ? 0 : task.NPD_TASK_STANDARD_DURATION ? task.NPD_TASK_STANDARD_DURATION : 'NA',
              taskOwner: task.TASK_OWNER_NAME,
              actualDuration: task?.TASK_NAME?.includes('CP') ? 0 : weekDiff,
              isCP1Completed: task?.NPD_TASK_IS_CP1_COMPLETED,
              showCommentsIcon: true
            },
          );
        } else {
          //if(!(this.checkpointsConfig.find(cp => cp.value == task?.TASK_NAME)) && task.TASK_NAME !='CP0') {//!(this.checkpointsConfig.find(cp => cp.value == task?.TASK_NAME)) && task.TASK_NAME !='CP0'
          let taskStartDate = (task?.NPD_TASK_ACTUAL_START_DATE && task.NPD_TASK_ACTUAL_START_DATE != 'NA' && task.NPD_TASK_ACTUAL_START_DATE != '') ? task?.NPD_TASK_ACTUAL_START_DATE : task.TASK_START_DATE
          //let taskEndDate = (task?.NPD_TASK_ACTUAL_DUE_DATE && task.NPD_TASK_ACTUAL_DUE_DATE !='NA' && task.NPD_TASK_ACTUAL_DUE_DATE !='')?
          //task?.NPD_TASK_ACTUAL_DUE_DATE :task.TASK_DUE_DATE
          let taskEndDate = (task?.NPD_TASK_ACTUAL_DUE_DATE && task.NPD_TASK_ACTUAL_DUE_DATE != 'NA' && task.NPD_TASK_ACTUAL_DUE_DATE != '') ?
            task?.NPD_TASK_ACTUAL_DUE_DATE : ((task?.NPD_TASK_ACTUAL_START_DATE && task.NPD_TASK_ACTUAL_START_DATE != 'NA' && task.NPD_TASK_ACTUAL_START_DATE != '') ? this.getFinalDate(task.NPD_TASK_ACTUAL_START_DATE, task?.TASK_DURATION) : task.TASK_DUE_DATE);
          let taskStartISO = taskStartDate && new Date(taskStartDate).toISOString();//setUtchours(0) or moment date with -1
          let taskEndISO = taskEndDate && new Date(taskEndDate).toISOString();
          let taskStartWithNoHours = taskStartISO?.split('T')[0]; //new Date(taskStartDate).setUTCHours(0);//setUtchours(0) or moment date with -1
          let taskEndWithNoHours = taskEndISO?.split('T')[0];//new Date(taskEndDate).setUTCHours(0);
          let taskDescription;
          let isDifferentProjectTask;
          let weekDiff = this.getDuration(new Date(this.getDateFormat(taskStartWithNoHours)), new Date(this.getDateFormat(taskEndWithNoHours)));
        
          if (task.PROJECT_ITEM_ID == this.project?.ITEM_ID) {
            taskDescription = task.TASK_DESCRIPTION
            isDifferentProjectTask = false;
          } else {
            taskDescription = task?.[WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID] + ' - ' + task.TASK_DESCRIPTION;
            isDifferentProjectTask = true;
          }
          let taskObject;
          taskObject = {
            TaskID: task.ID,
            TaskItemId: task.ITEM_ID,
            TaskName: task.TASK_NAME,//task.PR_REQUEST_ID_string + '-'+ task.TASK_NAME,
            TaskDescription: taskDescription,
            CommentsExist: task?.NPD_TASK_COMMENT_EXIST ? true : false,
            StartDate: new Date(this.getDateFormat(taskStartWithNoHours)),//new Date(this.getDateFormat(taskStartWithNoHours)),//new Date('10/04/2021'),//new Date(taskStartWithNoHours),
            EndDate: new Date(this.getDateFormat(taskEndWithNoHours)),//new Date(this.getDateFormat(taskEndWithNoHours)),//new Date('10/24/2021'),//new Date(taskEndWithNoHours),
            StartWeek: this.monsterUtilService.calculateWeek(taskStartDate),
            EndWeek: this.monsterUtilService.calculateWeek(taskEndDate),
            plannedStartDate: this.getPlannedStartDate(task.TASK_START_DATE).toString(),
            taskNo: task.TASK_NAME,
            Status: task.TASK_STATUS_VALUE,
            taskActive: task.IS_ACTIVE_TASK,
            taskOwnerId: task?.TASK_OWNER_ID,
            projectOwnerId: task?.PROJECT_OWNER_ID,
            CP1Date: null,
            currentCommercialAlignedDate: null,//NPD_PROJECT_CURRENT_COMMERICAL_ALIGNED_DATE,
            pmSlackTime: task?.NPD_PROJECT_PM_SLACK_TIME,
            isCheckPoint: false,
            //Progress: 100,
            Predecessor: dependentTasks?.toString(),
            requestId: task?.[WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID],
            isDifferentProjectTask: isDifferentProjectTask,
            weekDuration: weekDiff,
            baseLineStartWeek: isDifferentProjectTask ? baselineStartWithNoHours ? this.monsterUtilService.calculateWeek(baselineStartWithNoHours) : baselineStartWithNoHours : this.allTaskPriorToCP1.includes(task.ID) ? baselineStartWithNoHours : baselineStartWithNoHours ? (this.monsterUtilService.calculateWeek(baselineStartWithNoHours)) : (isCp1ApprovedInGovernance ? (task?.NPD_TASK_IS_ADHOC_TASK ? '0' : (isCp1ApprovedInGovernance && !GanttConfigConstants.TASKS_PRIOR_TO_CP1.includes(task.TASK_NAME)) ? 'NA' : '') : ''),
            baseLineEndWeek: isDifferentProjectTask ? baselineEndWithNoHours ? this.monsterUtilService.calculateWeek(baselineEndWithNoHours) : baselineEndWithNoHours : this.allTaskPriorToCP1.includes(task.ID) ? baselineEndWithNoHours : baselineEndWithNoHours ? (this.monsterUtilService.calculateWeek(baselineEndWithNoHours)) : (isCp1ApprovedInGovernance ? (task?.NPD_TASK_IS_ADHOC_TASK ? '0' : (isCp1ApprovedInGovernance && !GanttConfigConstants.TASKS_PRIOR_TO_CP1.includes(task.TASK_NAME)) ? 'NA' : '') : ''),
            baseLineDuration: isDifferentProjectTask ? task.NPD_TASK_BASELINE_DURATION : this.allTaskPriorToCP1.includes(task.ID) ? task.NPD_TASK_BASELINE_DURATION : task.NPD_TASK_BASELINE_DURATION == 0 ? 0 : task.NPD_TASK_BASELINE_DURATION ? (task.TASK_NAME.includes('CP') ? 0 : task.NPD_TASK_BASELINE_DURATION < 0 ? 0 : task.NPD_TASK_BASELINE_DURATION) : (isCp1ApprovedInGovernance ? (task?.NPD_TASK_IS_ADHOC_TASK ? '0' : (isCp1ApprovedInGovernance && !GanttConfigConstants.TASKS_PRIOR_TO_CP1.includes(task.TASK_NAME)) ? 'NA' : '') : ''),
            standardDuration: task?.TASK_NAME?.includes('CP') ? 0 : task.NPD_TASK_STANDARD_DURATION ? task.NPD_TASK_STANDARD_DURATION : 'NA',
            taskOwner: task.TASK_OWNER_NAME,
            actualDuration: task?.TASK_NAME?.includes('CP') ? 0 : weekDiff,
            isCP1Completed: task?.NPD_TASK_IS_CP1_COMPLETED,
            showCommentsIcon: true

            /*  Indicators: [
                    {
                        'date': new Date(this.project?.PROJECT_START_DATE),
                        'iconClass': 'e-btn-icon e-notes-info e-icons e-icon-left e-gantt e-notes-info::before',
                        'name': task.PR_REQUEST_ID_string + '-'+ task.TASK_NAME,
                        'tooltip': task.PR_REQUEST_ID_string + '-'+ task.TASK_NAME
                    }
                ] */
          };
          if (this.getTaskNo(task.TASK_NAME) == 30) {
            taskObject.Duration = 0;
          }
          project.subtasks.push(
            taskObject
          )

        }
      }
    });
    this.data.push(project);
    this.tempData = JSON.parse(JSON.stringify(this.data));


  }
  private tempData = [];

  getDateFormat(dateValue) {
    let date1 = new Date(dateValue);
    if (new Date().getTimezoneOffset() > 0) {
      date1 = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate() + 1);
    }
    //new Date('04/02/2019')
    // let date  = new Date().getTimezoneOffset() > 0 ?date1.getDate()+1:date1.getDate();
    let date = date1.getDate()
    let month = date1.getMonth() + 1;
    let year = date1.getFullYear();

    return '' + month + '/' + date + '/' + year;
  }

  public changeTaskBarColor(args: any) {

    if (!(args?.data?.hasChildRecords)) {
      if (args?.data?.taskData?.isCheckPoint) {
        if (args.data.taskData?.TaskName == 'CP1') {//TaskDescription
          args.taskbarBgColor = '#001e8b';//'#c7d8f7';
          args.progressBarBgColor = '#b191d9'; //'#c7d8f7';
          args.milestoneColor = '#001e8b'
        } else if (args.data.taskData?.TaskName == 'CP2') {
          args.taskbarBgColor = '#ffff00b8';
          args.progressBarBgColor = '#b191d9';
          args.milestoneColor = '#ffff00b8'
        } else if (args.data.taskData?.TaskName == 'CP3') {
          args.taskbarBgColor = '#87b4ebfa';
          args.progressBarBgColor = '#b191d9';
          args.milestoneColor = '#87b4ebfa'
        } else if (args.data.taskData?.TaskName == 'CP4') {
          args.taskbarBgColor = '#008b8b';
          args.progressBarBgColor = '#b191d9';
          args.milestoneColor = '#008b8b'
        } //#ffb100f2//orange
      } /* else if(args?.data?.taskData?.isDifferentProjectTask) {
                args.taskbarBgColor = '#b191d9'; //change
                args.progressBarBgColor = '#b191d9';
            } */
      else if (args.data.taskData?.Status == "Defined") {
        if (args.data.taskData?.isDifferentProjectTask) {
          args.taskbarBgColor = '#b268f2';//'#c7d8f7';
          args.progressBarBgColor = '#b268f2'; //'#c7d8f7';
        } else {
          args.taskbarBgColor = '#8602fa';//'#c7d8f7';
          args.progressBarBgColor = '#8602fa'; //'#c7d8f7';
        }
      } else if (args.data.taskData?.Status == "In Progress") {
        if (args.data.taskData?.isDifferentProjectTask) {
          args.taskbarBgColor = '#b268f2';//'#c7d8f7';
          args.progressBarBgColor = '#b268f2'; //'#c7d8f7';
        } else {
          args.taskbarBgColor = '#8602fa';
          args.progressBarBgColor = '#8602fa';
        }
      } else if (args.data.taskData?.Status == "Approved/Completed") {
        if (args.data.taskData?.isDifferentProjectTask) {
          args.taskbarBgColor = '#7fda62';//'#c7d8f7';
          args.progressBarBgColor = '#7fda62'; //'#c7d8f7';
        } else {
          args.taskbarBgColor = '#258f03';
          args.progressBarBgColor = '#258f03';
        }
      } else if (args.data.taskData?.Status === "Denied" || args.data.taskData?.Status === "Requested Changes") {
        args.taskbarBgColor = '#bb0a12';
        args.progressBarBgColor = '#bb0a12';
      }
      else if (args.data.taskData?.Status === "Cancelled") {
        if (args.data.taskData?.isDifferentProjectTask) {
          args.taskbarBgColor = '#989898';//'#c7d8f7';
          args.progressBarBgColor = '#989898'; //'#c7d8f7';
        } else {
          args.taskbarBgColor = '#989898';
          args.progressBarBgColor = '#989898';
        }
      } else {
        args.taskbarBgColor = '#b191d9';
        args.progressBarBgColor = '#b191d9';
      }
    }
    if (!args.data.taskData.subtasks) {
      if (args.data.taskData?.TaskName !== 'CP1' && args.data.taskData?.TaskName !== 'CP2' && args.data.taskData?.TaskName !== 'CP3' && args.data.taskData?.TaskName !== 'CP4') {
        let taskOverDue = this.isOverdue(args.data.taskData);
        if (taskOverDue && (args.data.taskData?.Status !== "Approved/Completed" && args.data.taskData?.Status !== "Cancelled")) {
          if (args.data.taskData?.isDifferentProjectTask) {
            args.taskbarBgColor = '#fc5956';//'#c7d8f7';
            args.progressBarBgColor = '#fc5956'; //'#c7d8f7';
          } else {
            args.taskbarBgColor = '#bb0a12';
            args.progressBarBgColor = '#bb0a12';
          }
        }
      }
    }
  }

  isOverdue(taskData) {
    let currentdate = new Date();
    currentdate.setHours(23, 59, 59, 998);
    return currentdate > taskData.EndDate
  }

  changeTaskRowColor(args) {
    if (args?.data?.taskData?.isDifferentProjectTask)//args?.data?.taskProjectItemId != this.project?.ITEM_ID)
      args.row.style.backgroundColor = "#49008b";
    else if (args?.data?.taskData?.isCheckPoint) {
      if (args?.data?.taskData?.isCheckPoint) {
        if (args.data.taskData?.TaskName == 'CP1') {//TaskDescription
          args.row.style.backgroundColor = "#001e8b";
        } else if (args.data.taskData?.TaskName == 'CP2') {
          args.row.style.backgroundColor = "#ffff00b8";
        } else if (args.data.taskData?.TaskName == 'CP3') {
          args.row.style.backgroundColor = "#87b4ebfa";
        } else if (args.data.taskData?.TaskName == 'CP4') {
          args.row.style.backgroundColor = "#008b8b";
        }
      }
    } else {
      args.row.style.backgroundColor = "#303030";
    }
  }

  changeTaskCellColor(args) {
    if (args.column.field == "TaskDescription") {
      args.cell.style.backgroundColor = "#88c534"//"#3fb5b0"
    }
    if (args.column.field == "actualDuration") {
      
      if (args.data.taskData.baseLineDuration != 0 && args.data.taskData.baseLineDuration && args.data.taskData.actualDuration > args.data.taskData.baseLineDuration) {
        args.cell.style.backgroundColor = "#bb0a12" //"#3fb5b0"
      }

    }

  }

  /*      onTaskBarEditing(args) {
          if(args) {
              console.log(args);
          }
         // args.cancel = true;
         this.hideTooltip = true;
      } */

  setGanttDataConfig() {
    this.data = [];//editingData;
    this.taskSettings = {
      id: 'TaskID',
      TaskItemId: 'TaskItemId',
      name: 'TaskName',//task description
      startDate: 'StartDate',
      endDate: 'EndDate',
      StartWeek: 'StartWeek',
      EndWeek: 'EndWeek',
      taskNo: 'TaskNo',//taskName
      dependency: 'Predecessor',
      child: 'subtasks',
      status: 'Status',
      taskActive: 'taskActive',
      taskOwnerId: 'taskOwnerId',
      projectOwnerId: 'projectOwnerId',
      CP1Date: 'CP1Date',
      currentCommercialAlignedDate: 'currentCommercialAlignedDate',
      pmSlackTime: 'pmSlackTime',
      isCheckPoint: 'isCheckPoint',
      indicators: 'Indicators',
      requestId: 'requestId',
      isDifferentProjectTask: 'isDifferentProjectTask',
      duration: 'Duration',
      weekDuration: 'weekDuration',
      plannedStartDate: 'plannedStartDate',
      comments: 'comments',
      taskOwner: 'TaskOwner',
      actualDuration: 'ActualDuration',
      standardDuration: 'StandardDuration',
      baseLineStartWeek: 'BaseLineStartWeek',
      baseLineEndWeek: 'BaseLineEndWeek',
      governedDeliveryWeek: 'GovernedDeliveryWeek',
      //progress: 'Progress',
    };
    this.editSettings = {
      allowAdding: false,
      allowEditing: false,
      allowDeleting: false,
      allowTaskbarEditing: false,
      showDeleteConfirmDialog: false
    };
    this.toolbar = ['ZoomIn', 'ZoomOut', 'Search'];
    this.columns = [
      { field: 'TaskID', clipMode: 'EllipsisWithTooltip' },// width: 50,
      { field: 'TaskName', headerText: 'Name', clipMode: 'EllipsisWithTooltip' },//, width: 50, clipMode: 'EllipsisWithTooltip'
      //{ field: 'StartDate' },
      { field: 'TaskDescription', headerText: 'Description', clipMode: 'EllipsisWithTooltip' },
      { field: 'comments', headerText: 'Comments', clipMode: 'EllipsisWithTooltip' },
      { field: 'taskOwner', headerText: 'Task Owner', clipMode: 'EllipsisWithTooltip' },
      { field: 'StartWeek', headerText: 'Start Week', clipMode: 'EllipsisWithTooltip' }, //, width: "150"
      { field: 'EndWeek', headerText: 'End Week', clipMode: 'EllipsisWithTooltip' },  //, width: "150"
      { field: 'actualDuration', headerText: 'Actual Duration', clipMode: 'EllipsisWithTooltip' },
      { field: 'standardDuration', headerText: 'Standard Duration', clipMode: 'EllipsisWithTooltip' },
      { field: 'baseLineStartWeek', headerText: 'BaseLine Start Week', clipMode: 'EllipsisWithTooltip' },
      { field: 'baseLineEndWeek', headerText: 'BaseLine End Week', clipMode: 'EllipsisWithTooltip' },
      { field: 'baseLineDuration', headerText: 'BaseLine Duration', clipMode: 'EllipsisWithTooltip' },
      { field: 'governedDeliveryDate', headerText: 'Governed Delivery Date (DP WH)', clipMode: 'EllipsisWithTooltip' },
      { field: 'currentTimeline', headerText: 'Current Timeline (DW)', clipMode: 'EllipsisWithTooltip' },

      // { field: 'CP1Date', headerText: 'Target delivery date agreed at Checkpoint 1',clipMode: 'EllipsisWithTooltip' },
      // { field: 'currentCommercialAlignedDate', headerText: 'Com/ Tech / Ops aligned date (DP WH)',clipMode: 'EllipsisWithTooltip' },
      { field: 'pmSlackTime', headerText: 'Programme Manager Slack Time', clipMode: 'EllipsisWithTooltip' },


    ];
    this.timelineSettings = {
      topTier: {
        unit: 'Week',
        format: 'WW-y',//format: 'MMM dd, y',//'WW ,y'
      },
      timelineUnitSize: '66',//'80'
      weekStartDay: 1,
      /*bottomTier: {
          unit: 'Day',
          count: 1
      }, */
      //showTooltip: true
    };
    this.gridLines = 'Vertical';
    this.labelSettings = {
      //taskLabel: 'TaskName',
      //leftLabel: 'TaskName',
      rightLabel: 'TaskDescription',//TaskName
      showTooltip: true
    };
    this.splitterSettings = {
      position: "50%"
    }
    this.filterSettings = {
      type: 'Menu'
    };
    this.tooltipSettings = {
      showTooltip: true
    };
    //this.hideTooltip = false;
    //this.enableTaskbarDragTooltip = true;
    //this.enableTaskbarTooltip = true;
    //this.projectStartDate= new Date('03/25/2019');
    //this.projectEndDate = new Date('07/28/2019');
    //this.resources = editingResources;
    /*  this.eventMarkers = [
         { day: '4/17/2019', label: 'Request Id' }
     ];
       this.splitterSettings = {
         columnIndex: 2
     }; */

  }

  export(value) {
    if (value == 'excel') {
      this.ganttObj.excelExport();
    } else if (value == 'csv') {
      this.ganttObj.csvExport();
    }
  }


  /*   public toolbarClick(){//args: ClickEventArgs): void {
     this.ganttObj.excelExport();if (args.item.id === 'contextMenu_excelexport') {//ganttDefault_excelexport
          this.ganttObj.excelExport();
      } else if(args.item.id === 'contextMenu_csvexport') {
          this.ganttObj.csvExport();
      } else if(args.item.id === 'contextMenu_search') {
          this.ganttObj.search('t');

      }
  };*/

  onEditModeChange(isEditMode) {
    if (isEditMode.checked) {
      this.isEditModeChecked = true;
      this.changeEditSettings(false, false, false, true, false, true);
    } else {
      this.changeEditSettings(false, false, false, false, false, false);
      this.isEditModeChecked = false;
    }

  }

  changeEditSettings(allowAdding, allowEditing, allowDeleting, allowTaskbarEditing, showDeleteConfirmDialog, enableResizing) {
    this.editSettings = {
      allowAdding: allowAdding,
      allowEditing: allowEditing,
      allowDeleting: allowDeleting,
      allowTaskbarEditing: allowTaskbarEditing,
      showDeleteConfirmDialog: showDeleteConfirmDialog
    };
    this.enableResizing = enableResizing;
  }

  refresh() {
    //this.getData();
    this.data = [];
    this.getAllDetails();
  }

  readUrlParams(callback) {
    this.activatedRoute.params.subscribe(taskparams => {
      this.activatedRoute.queryParams.subscribe(queryParams => {
        this.activatedRoute.parent.parent.params.subscribe(parentRouteParams => {
          callback(parentRouteParams, queryParams, taskparams);
        });
      });
    });
  }


  openCustomWorkflow(eventData?) {
    const dialogRef = this.dialog.open(NpdCustomWorkflowModalComponent, {
      width: '100%',
      minHeight: '25%',
      maxHeight: '100%',
      disableClose: true,
      data: {
        /*currentTaskData: eventData ? eventData : null,
        taskData: this.allProjectTasks,//this.taskList
        sourceTaskData: this.allSourceWorkFlowTasks,
        isTemplate: this.isTemplate, */
        currentTaskData: eventData ? eventData : null,
        taskData: this.allTargetTasks,//this.taskList
        //sourceTaskData: this.allProjectTasks,
        isTemplate: this.isTemplate,
        sequence: this.allTargetTasks.map(x => { return { name: x.TASK_NAME, sequence: x.sequence, taskId: x.ID } })


      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.refresh();
      }
    });
  }

  public format(value: Date): string {
    return instance.formatDate(value, { format: 'MMM dd,yyyy' });
  }

  getNoOfWeeksDiff(taskData) {
    return this.getDuration(taskData.StartDate, taskData.StartDate)
  }

  getDuration(startDate, endDate) {
   
    var a = moment(startDate, 'DD-MM-YYYY').startOf('isoWeek');//.isoWeekday(0);;//.day(0);
    var b = moment(endDate, 'DD-MM-YYYY').startOf('isoWeek');//.isoWeekday(0);;//.day(0);
    var duration = (b.diff(a, 'week'));
    
    if (duration < 0) {
      return 0;
    } else {
      return duration + 1;
    }
    //const minDuration = this.commentsUtilService.getDaysBetweenTwoDates(new Date(taskData.StartDate), new Date(taskData.EndDate), 'week');
  }

  public ngOnInit(): void {
    // this.renderer.addClass(document.body, 'zoom-out');

    this.loaderService.hide();
    this.userDisplayName = JSON.parse(
      sessionStorage.getItem('USER_DETAILS_WORKFLOW')
    )?.User?.UserDisplayName;
    this.hasTaskReorderAccess =
      this.filterConfigService.isProgramManagerRole()
      || this.filterConfigService.isCorpProjectLeader()
      || this.filterConfigService.isAdminRole();
    this.readUrlParams((parentRouteParams, queryParams, taskparams) => {
      if (queryParams && parentRouteParams) {
        //this.queryParams = queryParams;
        this.isTemplate = (queryParams.isTemplate == "true") ? true : false;
        if (parentRouteParams.projectId) {
          this.projectId = parentRouteParams.projectId;
          //this.getData();
          //this.refresh();

          this.getAllDetails();
          this.setGanttDataConfig(); //uncomment these 2 stmts
        }
      }
    });

  }

  /**
   * @author Manikanta
   * @description saves the new order of tasks based on user action.
   * @param event
   */
  rowDrop(args) {
    let index;
    let params = {
      updateNpdTaskSequence: {
        MPMProjectId: this.projectId,
        taskData: []
      }
    };
    let orderedTasksArray = new Array(...this.data[0]['subtasks']);

    if ((args.fromIndex - args.dropIndex) > 0) {
      if (args.dropPosition == 'middleSegment') {
        args.cancel = true;
        this.ganttObj.reorderRows([args.fromIndex], args.dropIndex, 'above');
      }
      if (args.dropPosition == 'topSegment') {
        index = args.dropIndex - 1 < 0 ? orderedTasksArray.length - 1 : args.dropIndex - 1;
      } else if (args.dropPosition == 'bottomSegment') {
        index = args.dropIndex - 1 < 0 ? orderedTasksArray.length - 1 : args.dropIndex;
      } else {
        index = args.dropIndex - 1 < 0 ? orderedTasksArray.length - 1 : args.dropIndex - 1;
      }
    }
    else {
      if (args.dropPosition == 'middleSegment') {
        args.cancel = true;
        this.ganttObj.reorderRows([args.fromIndex], args.dropIndex, 'above');
      }
      if (args.dropPosition == 'bottomSegment') {
        index = args.dropIndex - 1 < 0 ? orderedTasksArray.length - 1 : args.dropIndex - 1;
      } else if (args.dropPosition == 'topSegment') {
        index = args.dropIndex - 1 < 0 ? orderedTasksArray.length - 1 : args.dropIndex - 2;
      } else {
        index = args.dropIndex - 1 < 0 ? orderedTasksArray.length - 1 : args.dropIndex - 2;
      }
    }

    orderedTasksArray.splice(args.fromIndex - 1, 1);
    orderedTasksArray.splice(index, 0, args.data[0]);
    let seq = 1;
    orderedTasksArray.forEach(eachTask => {
      let sequenceObj = {
        taskId: eachTask['TaskID'],
        sequence: seq
      }
      seq++;
      params.updateNpdTaskSequence.taskData.push(sequenceObj);
    })
    this.npdTaskService.updateTaskSequence(params).subscribe(res => {
      this.notificationService.success("New Task Ordered Saved Successfully");
    },
      error => {
        this.notificationService.error("Something Went Wrong While Saving New Task Order");
      })
  }



  public zoomIn() {
    if (this.zoomNum < 100 && this.zoomNum >= 10) {
      this.zoomNum += 10;
      this.zoomLevel = `${this.zoomNum}%`;
      this.setTableHeight();
    }
  }


  public zoomOut() {
    if (this.zoomNum <= 100 && this.zoomNum > 30) {
      this.zoomNum -= 10;
      this.zoomLevel = `${this.zoomNum}%`;
      this.setTableHeight();

    }
  }
  public reset() {
    this.zoomNum = 100;
    this.zoomLevel = '100%';
    this.tableHeight = 'calc(100vh - 151px )';
  }
  public setTableHeight() {

    switch (this.zoomNum) {
      case 100:
        this.tableHeight = 'calc(100vh - 151px)';
        break;
      case 90:
        this.tableHeight = 'calc(109vh - 151px)';
        break;
      case 80:
        this.tableHeight = 'calc(121vh - 162px)';
        break;
      case 70:
        this.tableHeight = 'calc(134vh - 151px)';
        break;
      case 60:
        this.tableHeight = 'calc(153vh - 151px)';
        break;
      case 50:
        this.tableHeight = 'calc(178vh - 151px)';
        break;
      case 40:
        this.tableHeight = 'calc(218vh - 151px)';
        break;
      case 30:
        this.tableHeight = 'calc(281vh - 151px)';
        break;
      default:
        break;


    }
    this.data = [];
    this.data = JSON.parse(JSON.stringify(this.tempData));
  }

  public customZoomingLevels: ZoomTimelineSettings[] = [
    {
      topTier: { unit: 'Year', format: 'MMM, yy', count: 1 },
      bottomTier: { unit: 'Month', format: '', count: 6 }, timelineUnitSize: 200, level: 0,
      timelineViewMode: 'Month', weekStartDay: 1, updateTimescaleView: true, weekendBackground: null, showTooltip: true
    },
    {
      topTier: { unit: 'Year', format: 'yyyy', count: 1 },
      bottomTier: { unit: 'Month', format: '', count: 3 }, timelineUnitSize: 99, level: 1,
      timelineViewMode: 'Year', weekStartDay: 1, updateTimescaleView: true, weekendBackground: null, showTooltip: true
    },
    {
      topTier: { unit: 'Year', format: 'MMM, yy', count: 1 },
      bottomTier: { unit: 'Month', format: 'MMM-yy', count: 1 }, timelineUnitSize: 66, level: 2,
      timelineViewMode: 'Month', weekStartDay: 1, updateTimescaleView: true, weekendBackground: null, showTooltip: true
    },
    {
      topTier: { unit: 'Month', format: 'MMM, yy', count: 1 },
      bottomTier: { unit: 'Week', format: 'WW-y', count: 1 }, timelineUnitSize: 66, level: 3,
      timelineViewMode: 'Month', weekStartDay: 1, updateTimescaleView: true, weekendBackground: null, showTooltip: true
    },
    {
      topTier: { unit: 'Month', format: 'MMM, yy', count: 1 },
      bottomTier: { unit: 'Week', format: 'dd', count: 1 }, timelineUnitSize: 33, level: 4,
      timelineViewMode: 'Month', weekStartDay: 1, updateTimescaleView: true, weekendBackground: null, showTooltip: true
    },
  ];
  public dataBound(event): void {
   
    if (event?.isGanttCreated) {
      this.ganttObj.zoomingLevels = this.customZoomingLevels;
    }
  }


}


/*     getAllDetails2() {
        this.loaderService.show();
        forkJoin([this.getProjectDetailsById(this.projectId),this.getAllWorkflowRules(),this.getAllTaskConfigs()])
        .subscribe(forkedData => {
            if (forkedData?.[0]) {
                this.loaderService.show();
                //this.getAllGovernanceMilestones().subscribe(response=> {
                    this.getTaskDetailsByProjectId(this.project?.ITEM_ID,this.taskIdList).subscribe(taskResponse => {
                        let taskStartISO = this.project?.PROJECT_START_DATE && new Date(this.project.PROJECT_START_DATE).toISOString();
                        let taskEndISO = this.project?.PROJECT_DUE_DATE && new Date(this.project.PROJECT_DUE_DATE).toISOString();
                        let taskStartWithNoHours;
                        let taskEndWithNoHours;
                        if(taskStartISO) {
                            taskStartWithNoHours = taskStartISO.split('T')[0];
                        }
                        if(taskEndISO) {
                            taskEndWithNoHours = taskEndISO.split('T')[0];
                        }
                        let project = {
                            TaskID: this.project?.ID,
                            TaskName: this.project?.PROJECT_NAME,
                            StartDate: taskStartWithNoHours && new Date(taskStartWithNoHours),//new Date(this.project?.PROJECT_START_DATE),
                            EndDate: taskEndWithNoHours && new Date(taskEndWithNoHours),//new Date(this.project?.PROJECT_END_DATE),
                            StartWeek: this.monsterUtilService.calculateWeek(taskStartWithNoHours),//this.monsterUtilService.calculateWeek(this.project?.PROJECT_START_DATE),
                            EndWeek: this.monsterUtilService.calculateWeek(taskEndWithNoHours),//this.monsterUtilService.calculateWeek(this.project?.PROJECT_END_DATE),
                            CP1Date: this.project?.NPD_PROJECT_CP1_REVISED_DATE,
                            currentCommercialAlignedDate: this.project?.NPD_PROJECT_CURRENT_COMMERICAL_ALIGNED_DATE,
                            pmSlackTime: this.project?.NPD_PROJECT_PM_SLACK_TIME,
                            TaskDescription: '',
                            subtasks: [],
                        }
                        if(forkedData?.[1]) {//
                          /*   this.getAllWorkflowTaskIndexerDetails(this.taskIdList).subscribe(response => {
                                this.formProjectSubTasks(project,true);
                                this.columnsConfig();
                                this.showGantt = true;
                            });
                            this.formProjectSubTasks(project,true);
                            this.columnsConfig();
                            //this.showGantt = true;
                        } else {
                            this.formProjectSubTasks(project,false);
                            this.columnsConfig();
                            //this.showGantt = true;
                        }
                        this.loaderService.hide();
                    },error => {
                        this.loaderService.hide();
                    });
                //})
            }
        }, error => {
            console.log(error);
            this.loaderService.hide();
            const eventData = { message: 'Something went wrong while fetching details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            this.notificationService.error(eventData.message);
        });
    }
    formProjectSubTasks2(project,isWorkflow) {
        //this.data = [];
        this.checkpointsConfig = JSON.parse(JSON.stringify(GanttConfigConstants.CheckpointConfig));
        this.allProjectTasks.forEach(task => {

            let dependentTasks = [];
            if(isWorkflow) {
                let taskSpecificFilteredRuleDetails = this.ruleList.filter(workflowRule => workflowRule.targetData == task.ID)
                if(taskSpecificFilteredRuleDetails && taskSpecificFilteredRuleDetails.length > 0) {
                    taskSpecificFilteredRuleDetails.forEach(workflowRule => {
                        dependentTasks.push(workflowRule.currentTask);
                    })
                }
            }
            console.log(task.ID+'->'+task.TASK_NAME+'->');
            console.log(dependentTasks?.toString());
            let taskNo = this.getTaskNo(task?.TASK_NAME);
            if(this.checkpointsConfig.find(cp => !cp.isAdded)) {
                this.checkpointsConfig.forEach(cp => {
                   if(!cp.isAdded && task.PROJECT_ITEM_ID == this.project?.ITEM_ID) {
                    let firstTask = cp.taskList.find(task => task == taskNo);
                    if(firstTask) {
                       /*  let checkPointDetail = this.allCheckpoints.find(checkPoint => checkPoint.Properties.Stage == cp.value)
                        let taskStartISO = checkPointDetail?.Properties?.Target_Start_Date && new Date(checkPointDetail?.Properties?.Target_Start_Date).toISOString();
                        let taskEndISO = checkPointDetail?.Properties?.Decision_Date && new Date(checkPointDetail?.Properties?.Decision_Date).toISOString();
                        let taskStartWithNoHours;
                        let taskEndWithNoHours;

                        let task = this.allProjectTasks.find(task => task.TASK_NAME == cp.value)
                        let taskStartISO =  (task?.NPD_TASK_ACTUAL_START_DATE && task.NPD_TASK_ACTUAL_START_DATE !='NA' &&
                        task.NPD_TASK_ACTUAL_START_DATE !='')? task?.NPD_TASK_ACTUAL_START_DATE : task.TASK_START_DATE
                        let taskEndISO = (task?.NPD_TASK_ACTUAL_DUE_DATE && task.NPD_TASK_ACTUAL_DUE_DATE !='NA' && task.NPD_TASK_ACTUAL_DUE_DATE !='')? task?.NPD_TASK_ACTUAL_DUE_DATE : task.TASK_DUE_DATE
                        let taskStartWithNoHours;
                        let taskEndWithNoHours;
                        if(taskStartISO) {
                            taskStartWithNoHours = taskStartISO.split('T')[0];
                        }
                        if(taskEndISO) {
                            taskEndWithNoHours = taskEndISO.split('T')[0];
                        }
                        console.log('date gantt->'+new Date(this.getDateFormat(taskStartWithNoHours)));
                        console.log('date end gantt->'+new Date(this.getDateFormat(taskEndWithNoHours)));
                        //mapping checkpoints to group of tasks
                        project.subtasks.push(
                            {
                                 TaskID: '',
                                 TaskName: cp.value,
                                 TaskDescription: cp.displayName,
                                 isCheckPoint: true,
                                 duration: 0,
                                 Duration: 0,
                                 StartDate: new Date(taskStartISO),
                                 //taskStartWithNoHours && new Date(this.getDateFormat(taskStartWithNoHours)),//taskStartWithNoHours && new Date(taskStartWithNoHours),

                                 //EndDate: '',//taskEndWithNoHours && new Date(this.getDateFormat(taskEndWithNoHours)),
                                 StartWeek: taskStartWithNoHours &&  this.monsterUtilService.calculateWeek(taskStartWithNoHours),//checkPointDetail?.Properties?.Target_Start_Date && this.monsterUtilService.calculateWeek(checkPointDetail?.Properties?.Target_Start_Date),
                                 EndWeek: taskEndWithNoHours &&  this.monsterUtilService.calculateWeek(taskEndWithNoHours) //checkPointDetail?.Properties?.Decision_Date && this.monsterUtilService.calculateWeek(checkPointDetail.Properties?.Decision_Date)
                                 /* StartDate: checkPointDetail?.Properties?.Target_Start_Date && new Date(checkPointDetail?.Properties?.Target_Start_Date),
                                 EndDate: checkPointDetail?.Properties?.Decision_Date && new Date(checkPointDetail?.Properties?.Decision_Date),
                                 StartWeek: checkPointDetail?.Properties?.Target_Start_Date && this.monsterUtilService.calculateWeek(checkPointDetail?.Properties?.Target_Start_Date),
                                 EndWeek:  checkPointDetail?.Properties?.Decision_Date && this.monsterUtilService.calculateWeek(checkPointDetail.Properties?.Decision_Date)


                                 /* StartDate: new Date(task.TASK_START_DATE),
                                 EndDate: new Date(task.TASK_DUE_DATE),
                                 StartWeek: this.monsterUtilService.calculateWeek(task.TASK_START_DATE),
                                 EndWeek: this.monsterUtilService.calculateWeek(task.TASK_DUE_DATE),
                                 taskNo : task.TASK_NAME,
                                 Status: task.TASK_STATUS_VALUE,
                                 taskActive: task.IS_TASK_ACTIVE,
                                 CP1Date: task?.NPD_PROJECT_CP1_REVISED_DATE,
                                 currentCommercialAlignedDate: task?.NPD_PROJECT_CURRENT_COMMERICAL_ALIGNED_DATE,
                                 pmSlackTime: task?.NPD_PROJECT_PM_SLACK_TIME,

                            },
                        );
                        cp.isAdded = true;
                    }
                   }
                })
            }
            //mapping tasks to project
            //if(task.TASK_NAME !='CP1')//don't push those tasks or push them above
            if(!(this.checkpointsConfig.find(cp => cp.value == task?.TASK_NAME)) && task.TASK_NAME !='CP0') {
                let taskStartDate = (task?.NPD_TASK_ACTUAL_START_DATE && task.NPD_TASK_ACTUAL_START_DATE !='NA' && task.NPD_TASK_ACTUAL_START_DATE !='')? task?.NPD_TASK_ACTUAL_START_DATE : task.TASK_START_DATE
                let taskEndDate = (task?.NPD_TASK_ACTUAL_DUE_DATE && task.NPD_TASK_ACTUAL_DUE_DATE !='NA' && task.NPD_TASK_ACTUAL_DUE_DATE !='')? task?.NPD_TASK_ACTUAL_DUE_DATE : task.TASK_DUE_DATE
                let taskStartISO = taskStartDate && new Date(taskStartDate).toISOString();//setUtchours(0) or moment date with -1
                let taskEndISO = taskEndDate && new Date(taskEndDate).toISOString();
                let taskStartWithNoHours = taskStartISO?.split('T')[0]; //new Date(taskStartDate).setUTCHours(0);//setUtchours(0) or moment date with -1
                let taskEndWithNoHours = taskEndISO?.split('T')[0];//new Date(taskEndDate).setUTCHours(0);
                let taskDescription;
                let isDifferentProjectTask;
                console.log('date gantt->'+new Date(this.getDateFormat(taskStartWithNoHours)));
                console.log('date end gantt->'+new Date(this.getDateFormat(taskEndWithNoHours)));
                if(task.PROJECT_ITEM_ID == this.project?.ITEM_ID) {
                    taskDescription = task.TASK_DESCRIPTION
                    isDifferentProjectTask = false;
                } else {
                    taskDescription = task?.[WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID] + ' - '+task.TASK_DESCRIPTION;
                    isDifferentProjectTask = true;
                }
                project.subtasks.push(
                    {
                         TaskID: task.ID,
                         TaskItemId : task.ITEM_ID,
                         TaskName: task.TASK_NAME,//task.PR_REQUEST_ID_string + '-'+ task.TASK_NAME,
                         TaskDescription: taskDescription,
                         StartDate: new Date(this.getDateFormat(taskStartWithNoHours)),//new Date(this.getDateFormat(taskStartWithNoHours)),//new Date('10/04/2021'),//new Date(taskStartWithNoHours),
                         EndDate: new Date(this.getDateFormat(taskEndWithNoHours)),//new Date(this.getDateFormat(taskEndWithNoHours)),//new Date('10/24/2021'),//new Date(taskEndWithNoHours),
                         StartWeek: this.monsterUtilService.calculateWeek(taskStartDate),
                         EndWeek: this.monsterUtilService.calculateWeek(taskEndDate),
                         taskNo : task.TASK_NAME,
                         Status: task.TASK_STATUS_VALUE,
                         taskActive: task.IS_TASK_ACTIVE,
                         CP1Date: task?.NPD_PROJECT_CP1_REVISED_DATE,
                         currentCommercialAlignedDate: task?.NPD_PROJECT_CURRENT_COMMERICAL_ALIGNED_DATE,
                         pmSlackTime: task?.NPD_PROJECT_PM_SLACK_TIME,
                         isCheckPoint: false,
                         //Progress: 100,
                         Predecessor: dependentTasks?.toString(),
                         requestId: task?.[WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID],
                         isDifferentProjectTask : isDifferentProjectTask
                        /*  Indicators: [
                            {
                                'date': new Date(this.project?.PROJECT_START_DATE),
                                'iconClass': 'e-btn-icon e-notes-info e-icons e-icon-left e-gantt e-notes-info::before',
                                'name': task.PR_REQUEST_ID_string + '-'+ task.TASK_NAME,
                                'tooltip': task.PR_REQUEST_ID_string + '-'+ task.TASK_NAME
                            }
                        ]
                    },
                )
            }

        });

        /*                 taskStartWithNoHours
        '2021-10-04' new Date('04/02/2019'),
        taskEndWithNoHours
        '2021-10-24'
        //this.editingData = [];
        //this.editingData?.push(project);
        this.data.push(project);
    }


getData() {
    this.loaderService.show();
    this.getProjectDetailsById(this.projectId).subscribe(projectResponse => {
        this.getAllGovernanceMilestones().subscribe(checkPointsResponse=> {
            this.getTaskDetailsByProjectId(this.project?.ITEM_ID).subscribe(taskResponse=> {
                let project = {
                    TaskID: this.project?.ID,
                    TaskName: this.project?.PROJECT_NAME,
                    StartDate: new Date(this.project?.PROJECT_START_DATE),
                    EndDate: new Date(this.project?.PROJECT_END_DATE),
                    StartWeek: this.monsterUtilService.calculateWeek(this.project?.PROJECT_START_DATE),
                    EndWeek: this.monsterUtilService.calculateWeek(this.project?.PROJECT_END_DATE),
                    CP1Date: this.project?.NPD_PROJECT_CP1_REVISED_DATE,
                    currentCommercialAlignedDate: this.project?.NPD_PROJECT_CURRENT_COMMERICAL_ALIGNED_DATE,
                    pmSlackTime: this.project?.NPD_PROJECT_PM_SLACK_TIME,
                    TaskDescription: '',
                    subtasks: [],
                    //Duration : 10
                }
                if(this.project?.PR_REQUEST_ID_string) {
                    let startDate =this.datepipe.transform(this.project?.PROJECT_START_DATE, 'MM/dd/yyyy');
                    this.eventMarkers = [
                        { day:  new Date(this.project?.PROJECT_START_DATE), label: 'Request Id - '+this.project?.PR_REQUEST_ID_string}
                    ];
                }
                this.taskService.getWorkflowRulesByProject(this.projectId).subscribe(workflowResponse => {
                    this.loaderService.hide();
                    let taskIdList = [];
                    if (workflowResponse) {
                        let workflowRules;
                        if (workflowResponse && workflowResponse.length > 0) {
                            workflowRules = workflowResponse;
                        } else {
                            workflowRules = [workflowResponse];
                        }
                        workflowRules.forEach(rule => {
                        //const currentTask = this.taskData.find(task => task.ID === rule.R_PO_TASK['Task-id'].Id);
                        if(rule.TRIGGER_TYPE === 'STATUS' && rule.TARGET_TYPE === 'TASK') {
                            this.ruleList.push({
                                currentTask: rule.R_PO_TASK['Task-id'].Id,
                                triggerType: rule.TRIGGER_TYPE,
                                triggerData: rule.TRIGGER_TYPE === 'STATUS' ? rule.R_PO_STATUS['MPM_Status-id'].Id : rule.R_PO_ACTIONS['MPM_Workflow_Actions-id'].Id,
                                targetType: rule.TARGET_TYPE,
                                targetData: rule.TARGET_TYPE === 'TASK' ? rule.R_PO_TARGET_TASK['Task-id'].Id : rule.R_PO_EVENT['MPM_Events-id'].Id,
                                workflowRuleId: rule['Workflow_Rules-id'].Id,
                              });//fetch rules based on trigger type status alone & TARGET_TYPE === 'TASK'
                              taskIdList.push(rule.R_PO_TASK['Task-id'].Id);
                        }
                      });
                      this.getAllWorkflowTaskIndexerDetails(taskIdList).subscribe();
                      this.formProjectSubTasks(project,true);
                    } else {
                      this.formProjectSubTasks(project,false);
                    }
                  },error => {
                    this.loaderService.hide();
                });
                this.ganttObj.hideColumn(['Dependency']); //Hiding predeccesor-dependency columns for gantt chart
                this.ganttObj.hideColumn(['startDate']);
                this.ganttObj.hideColumn(['endDate']);
                this.ganttObj.hideColumn(['isCheckPoint']);
                this.ganttObj.showColumn(['StartWeek']);
                this.ganttObj.showColumn(['EndWeek']);
                this.ganttObj.refresh();
                //this.ganttObj.renderGantt();
            },error => {
                this.loaderService.hide();
            });
        });
    },error => {
        this.loaderService.hide();
    });
}
 */

/*                       this.allProjectTasks.forEach(task => {
                        let dependentTasks = [];
                        let taskSpecificFilteredRuleDetails = this.ruleList.filter(workflowRule => workflowRule.currentTask == task.ID  )
                        if(taskSpecificFilteredRuleDetails && taskSpecificFilteredRuleDetails.length > 0) {
                            taskSpecificFilteredRuleDetails.forEach(workflowRule => {
                                dependentTasks.push(workflowRule.targetData);
                            }
                        )
                        console.log(task.ID+'->'+task.TASK_NAME+'->');
                        console.log(dependentTasks?.toString());
                        project.subtasks.push(
                            {
                                 TaskID: task.ID,
                                 TaskName: task.TASK_NAME,
                                 StartDate: new Date(task.TASK_START_DATE),
                                 EndDate: new Date(task.TASK_DUE_DATE),
                                // Duration: 3,
                                 Predecessor: dependentTasks?.toString(),
                                // Progress: 30
                            },
                        )}
                    });
                     if(taskargs?.taskBarEditAction == "RightResizing") {
                taskRequestObj.TaskObject['DueDate'] = new Date(taskargs.data?.EndDate)?.toISOString();//(args.data.EndDate).toISOString()
                this.updateTask(taskRequestObj);//updating end date
            } else if(taskargs?.taskBarEditAction == "LeftResizing") {
                taskRequestObj.TaskObject['StartDate'] = new Date(taskargs.data?.StartDate)?.toISOString();
                this.updateTask(taskRequestObj);//updating start date
            } else if(taskargs?.taskBarEditAction == "ChildDrag") {
                taskRequestObj.TaskObject['StartDate'] = new Date(taskargs.data?.StartDate)?.toISOString();
                taskRequestObj.TaskObject['DueDate'] = new Date(taskargs.data?.EndDate)?.toISOString();
                this.updateTask(taskRequestObj);//updating start date & end date
            } */
