import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { NpdGanttRoutingModule } from './npd-gantt-routing.module';
import { ExcelExportService, GanttAllModule, GanttModule } from '@syncfusion/ej2-angular-gantt';
import { NpdGanttChartComponent } from './npd-gantt-chart/npd-gantt-chart.component';
import { MaterialModule } from 'src/app/material.module';
import { NpdSharedModule } from '../npd-shared/npd-shared.module';


@NgModule({
  declarations: [NpdGanttChartComponent],
  imports: [
    CommonModule,
    NpdGanttRoutingModule,
    MaterialModule,
    GanttModule,GanttAllModule,
    NpdSharedModule
  ],
  providers: [DatePipe,ExcelExportService]
})
export class NpdGanttModule { }
