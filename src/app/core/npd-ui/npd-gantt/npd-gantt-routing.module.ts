import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NpdGanttChartComponent } from './npd-gantt-chart/npd-gantt-chart.component';


const routes: Routes = [
  {
    path: '',
    component : NpdGanttChartComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NpdGanttRoutingModule { }
