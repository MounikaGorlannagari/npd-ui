import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AppService, EntityAppDefService, GloabalConfig, LoaderService, NotificationService, OTDSTicketService, SessionStorageConstants } from 'mpm-library';
import { Observable } from 'rxjs';
import * as $ from 'jquery';
import { RoutingConstants } from '../../mpm/routingConstants';
import { NPDConfig } from '../utils/config/NPDConfig';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  OTMM_CONFIG_NS = 'http://schemas.acheron.com/psa/custom/1.0';    
  urlToBeloaded;
    urlObtained = false;
    selectedMenu;
    reports = [];
    reportUrl = '';
    reportIframeUrl;
    loadUrl;
  menuItem: any;
  integrationConfig;


  constructor(
    private loaderService: LoaderService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private appService: AppService,
    private entityAppDefService: EntityAppDefService,
    private http: HttpClient,
    private notificationService: NotificationService,
    public otdsTicketService: OTDSTicketService
) { this.loaderService.hide()}

  refreshRequest(){

  }

    getOTDSTicket() {
        const parameters = {
            applicationId: this.entityAppDefService.getApplicationID()
        };
        //this.loadReports();
        /* this.getIHubOTDSTicketForUser(parameters)
            .subscribe(otdsTicketResponse => {
                var token = '';
                var userId = "";
                if (otdsTicketResponse && otdsTicketResponse.tuple && otdsTicketResponse.tuple.old &&
                    otdsTicketResponse.tuple.old.IHUBSessionHelper &&
                    otdsTicketResponse.tuple.old.IHUBSessionHelper.OTDSToken) {
                    token = otdsTicketResponse.tuple.old.IHUBSessionHelper.OTDSToken;
                    userId = otdsTicketResponse.tuple.old.IHUBSessionHelper.UserId;

                }
                if (token) {
                    const httpOptions = {
                        withCredentials: true,
                        headers: new HttpHeaders({
                            'Content-Type': "application/x-www-form-urlencoded",
                        }),
                    };
                    var params = 'userid=' + userId;
                    var loadingReportUrl = 'https://mmdev.monsterenergy.com/iportal/Monster_NPD_Reports/npd_reports.html' + '?__credentials=' + token;
                    //'https://mmdev.monsterenergy.com/iportal/mpm_reports/mpm_reports.html' + '?__credentials=' + token;
                    this.http.post(loadingReportUrl, params, httpOptions)
                        .subscribe(response => {
                            this.loadReports();
                            this.loaderService.hide();
                        }, error => {
                            this.loaderService.hide();
                            if (error.status === 200) {
                                this.loadReports();
                            } else {
                                if (error.status === 502) {
                                    this.notificationService.error("Reports server is not accessible, Kindly check with admin");
                                } else {
                                    this.notificationService.error("You dont have access to see Reports.");
                                }
                            }
                        });
                } else {
                    this.loaderService.hide();
                    this.notificationService.error("You dont have access to see Reports.")
                }

            }); */
        const token = sessionStorage.getItem(SessionStorageConstants.OTDS_TICKET);//this.otdsTicketService.getOTDSTicketForUser();//sessionStorage.getItem(SessionStorageConstants.OTDS_TICKET);
        const userId = (this.integrationConfig?.userId)? this.integrationConfig.userId :sessionStorage.getItem(SessionStorageConstants.OTDS_USER_ID);
                      //sessionStorage.getItem(SessionStorageConstants.OTDS_USER_ID);
        if (token) {
            const httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'Content-Type': "application/x-www-form-urlencoded",
                }),
            };
            var params = 'userid=' + userId;
            var loadingReportUrl = this.reportUrl + '?__credentials=' + token;
            //'https://mmdev.monsterenergy.com/iportal/mpm_reports/mpm_reports.html' + '?__credentials=' + token;
            this.http.post(loadingReportUrl, params, httpOptions)
                .subscribe(response => {
                    this.loadReports();
                    this.loaderService.hide();
                }, error => {
                    this.loaderService.hide();
                    if (error.status === 200) {
                        this.loadReports();
                    } else {
                        if (error.status === 502) {
                            this.notificationService.error("Reports server is not accessible, Kindly check with admin");
                        } else {
                            this.loadReports();
                            this.notificationService.error("You dont have access to see Reports.");
                        }
                    }
                });
        } else {
            this.loaderService.hide();
            this.notificationService.error("You dont have access to see Reports.")
        }

    }

    loadReports() {
        const baseUrl = this.loadUrl ? this.reportUrl:this.reportIframeUrl;
        this.urlToBeloaded = this.sanitizer.bypassSecurityTrustResourceUrl(baseUrl);
        this.urlObtained = true;
    }

    populateProjectFields() {
        this.loaderService.show();
        this.getOTDSTicket();
    }

    ngOnInit() {
        // this.reports = [];
        // this.selectedMenu = '';
        // let isAvailableRoute = false;
        // const matchedReport = this.reports.filter((report) => {
        //     return (report.menu === this.selectedMenu.location);
        // });
        // if (matchedReport.length > 0) {
        //     this.reportUrl = 'https://mmdev.monsterenergy.com/iportal/mpm_reports/mpm_reports.html';
        //     isAvailableRoute = true;
        // }
        // if (!isAvailableRoute) {
        // }
        // this.populateProjectFields();

        // window['handleReportError'] = (errorMsg) => {
        //     this.notificationService.error(errorMsg);
        // };
        this.loaderService.hide();
        this.integrationConfig = NPDConfig.getIntgrationConfig();
        this.reportUrl = this.integrationConfig?.reportsUrl;
        this.reportIframeUrl = this.integrationConfig?.reportsIframeUrl;
        this.loadUrl = this.integrationConfig?.loadUrl;
        this.reports = this.integrationConfig?.reports;// GloabalConfig.config.reports;
        this.selectedMenu = '';//this.menuservice.getSelectedMenuItem();
        let isAvailableRoute = false;
        const matchedReport = this.reports.filter((report) => {
            return (report.menu === this.selectedMenu.location);
        });
        if (matchedReport.length > 0) {
            this.reportUrl = '';//matchedReport[0].url;
            isAvailableRoute = true;
        }
        /* if (!isAvailableRoute) {
            this.router.navigate([RoutingConstants.mpmBaseRoute]);
        } */
        this.populateProjectFields();
        //this.reportUrl = 'https://mmdev.monsterenergy.com/iportal/Monster_NPD_Reports/npd_reports.html' + '?__credentials=';
        //this.loadReports();
        window['handleReportError'] = (errorMsg) => {
            this.notificationService.error(errorMsg);
        };

    }

    getIHubOTDSTicketForUser(parameters) {
      return this.appService.invokeRequest(this.OTMM_CONFIG_NS, 'GetOTDSTicketForUser', parameters);
    }

}
