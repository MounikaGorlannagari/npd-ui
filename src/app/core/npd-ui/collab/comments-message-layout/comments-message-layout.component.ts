import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewChecked, HostListener, Directive } from '@angular/core';
import { CommentsUtilService } from '../services/comments.util.service';
import { UserInfoModal } from '../objects/comment.modal';
import { CommentsService } from '../services/comments.service';

@Component({
  selector: 'npd-comments-message-layout',
  templateUrl: './comments-message-layout.component.html',
  styleUrls: ['./comments-message-layout.component.scss']
})
export class CommentsMessageLayoutComponent implements OnInit, AfterViewChecked {
  @Input() commentListData: Array<any>;
  @Input() pageDetails: any;
  @Input() isScrollDown: boolean;
  @Input() userListData: Array<UserInfoModal>;
  @Output() replyToComment = new EventEmitter<any>();
  @Output() loadingMoreComments = new EventEmitter<any>();
  @ViewChild('scrollToBottom') public myScrollContainer: ElementRef;
  addReplyHeight: boolean;
  loadMore = true;
  navigateToReply = null;
  @HostListener('scroll', ['$event']) onScrollEvent(eventData) {
    this.loadMore = !this.loadMore ? this.isScrollDown : this.loadMore;
    if (this.loadMore && this.myScrollContainer.nativeElement.scrollTop <= 1000 && this.commentListData.length >= 100) {
      this.onloadMoreComments(null);
      this.loadMore = false;
    }
  }
  constructor(
    public commentUtilService: CommentsUtilService,
    private commentsService: CommentsService
  ) { 

    this.commentsService.addReplyHeight.subscribe(flag => {
      this.addReplyHeight = flag;
    })
  }

  ngAfterViewChecked() {
    if (this.isScrollDown && this.myScrollContainer.nativeElement.scrollTop === 0) {
      this.scrollToBottom(this.myScrollContainer.nativeElement.scrollHeight);
      this.loadMore = true;
    }
  }

  onloadMoreComments(eventData): void {
    let tempPageDetails = this.pageDetails;
    if (tempPageDetails) {
      tempPageDetails.page = tempPageDetails.page + 1;
      tempPageDetails.skip = tempPageDetails.skip + 100;
    } else {
      tempPageDetails = this.commentUtilService.resetPageDetails();
    }
    this.loadingMoreComments.emit(tempPageDetails);
  }

  onReplyToComment(parentComment): void {
    this.replyToComment.emit(parentComment);
  }

  scrollToBottom(height): void {
    try {
     // this.myScrollContainer.nativeElement.scrollTop = height;
    } catch (err) { }
  }

  onClickingOnReplyMessage(parentCommentId): void {
    if (document.getElementById('message_' + parentCommentId)) {
      document.getElementById('message_' + parentCommentId).scrollIntoView({ behavior: 'smooth', block: 'center' });
      this.navigateToReply = null;
      this.commentListData.forEach(commentListData=> {
        if(commentListData.commentId === parentCommentId) {
          commentListData.hover = true;
        } else {
          commentListData.hover = false;
        }
      }
      );
    } else {
      this.navigateToReply = parentCommentId;
      this.onloadMoreComments(null);
    }
  }

  ngOnInit(): void {
    this.loadMore = this.isScrollDown;
    if (this.navigateToReply && this.isScrollDown) {
      this.onClickingOnReplyMessage(this.navigateToReply);
    }
  }
}
