import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'npd-comments-modal',
  templateUrl: './comments-modal.component.html',
  styleUrls: ['./comments-modal.component.scss']
})
export class CommentsModalComponent implements OnInit {

  requestId: number;
  taskId: number;
  enableToComment: boolean;
  isGroupFilter : boolean;
  commentType;
  projectId;
  constructor(
    public dialogRef: MatDialogRef<CommentsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  closeCommentsPopup(data) {
    if (this.dialogRef) {
      this.dialogRef.close(data);
    }
  }

  ngOnInit(): void {
    if (this.data) {
      
      this.requestId = this.data.requestId;
      this.isGroupFilter = false;
      this.commentType = this.data.commentType;
      this.projectId = this.data.projectId;
      this.taskId = this.data.taskId
     // this.isExternalUser = this.data.isExternalUser;
    }
  }
}
