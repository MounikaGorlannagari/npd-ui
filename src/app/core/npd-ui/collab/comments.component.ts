import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { CommentsService } from './services/comments.service';
import { CommentsUtilService } from './services/comments.util.service';
import { forkJoin } from 'rxjs';
import { NewCommentModal } from './objects/comment.modal';
import { ProjectService, LoaderService, NotificationService, DeliverableService, SharingService, UtilService } from 'mpm-library';
import { CommentConfig } from './objects/CommentConfig';
import { MonsterRequestService } from '../request-view/services/monster-request.service';
import { RequestService } from '../services/request.service';



@Component({
  selector: 'npd-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CommentsComponent implements OnInit {
  @Input() requestId: number;
  @Input() taskId: number;
  @Input() commentType: string;
  @Input() enableToComment: boolean;
  @Input() showGroupFilter: boolean;
  @Input() projectId;

  selectedGroupFilter;
  commentsConfig = null;
  titleValue = null;
  isScrollDown = false;
  userListData = [];
  isReadonly = false;
  filters = CommentConfig.groupFilters;

  constructor(
    public commentsService: CommentsService,
    public commentUtilService: CommentsUtilService,
    public projectService: ProjectService,
    public loaderService: LoaderService,
    public notification: NotificationService,
    public deliverableService: DeliverableService,
    public sharingService: SharingService,
    public utilService: UtilService,
    public requestService:RequestService
  ) {
    //this.commentsService.clearUserProjectDeliverableMapper();
  }

  initComments(): void {
    if (this.requestId) {
      this.commentsConfig = {
        commentList: [],
        pageDetails: this.commentUtilService.resetPageDetails(),
      };
     
      this.loadDataOnRefresh(this.taskId?this.taskId:this.requestId,false);
    }
  }

  onGroupFilterChange(filter) {
   this.initComments();
  }

  loadDataOnRefresh(requestId: number, isLoadMore: boolean): void {
    this.loaderService.show();
    this.isScrollDown = false;
    const httpArrayList = [
      this.selectedGroupFilter === CommentConfig.ALL_GROUP_FILTER_VALUE ?
        this.commentsService.getComments(requestId,this.commentsConfig.pageDetails.skip) :
        this.commentsService.getCommentsByType(requestId,this.selectedGroupFilter,this.commentsConfig.pageDetails.skip)
    ];
    httpArrayList.push(
      // this.showGroupFilter ?this.commentsService.getAllUsers():this.commentsService.getUsersForProject(this.projectId)
      this.commentsService.getAllUsers()
      );
    forkJoin(httpArrayList).subscribe(responses => {
      this.loaderService.hide();
      if (responses && responses.length >= 2) {
        this.commentsConfig.commentList = !isLoadMore ? responses[0]?.reverse() : [...this.commentsConfig.commentList,...responses[0]?.reverse() ,];
        this.userListData = responses[1];
        this.isScrollDown = responses[0].length > 0 ? true : false;
      }
    }, (errorDetail) => {
      this.notification.error('Something went wrong while getting comments!');
      this.commentsConfig = null;
      this.loaderService.hide();
    });
  }

  onCreatingNewComment(commentDetails: NewCommentModal): void {
      commentDetails = Object.assign(commentDetails, { requestId: this.requestId},{ taskId: this.taskId},{commentType : this.commentType});
      this.commentsService.createNewComment(commentDetails).subscribe(response => {
        if (response) {
          this.commentsConfig.pageDetails = this.commentUtilService.resetPageDetails();
          this.loadDataOnRefresh(this.taskId?this.taskId:this.requestId, false);//currCommentItem.requestId
        }
      });
  }

  onLoadingMoreComments(pageDetails: any): void {
    this.commentsConfig.pageDetails = pageDetails;
    this.loadDataOnRefresh(this.taskId?this.taskId:this.requestId, true);
  }

  updateTheCommentTitle(indexChangeData): void {
   // this.titleValue = indexChangeData.isProject ?
     // ('Project: ' + indexChangeData.titleName) : ('Deliverable: ' + indexChangeData.titleName);
    //this.isReadonly = this.isManager ? !(this.projectOwerId === this.sharingService.getCurrentUserItemID()) : false;
    //this.isReadonly = (indexChangeData.status && (indexChangeData.status === 'COMPLETED' && this.isManager)) ? true : this.isReadonly;
  }

  refreshComments(eventData): void {
    this.loadDataOnRefresh(this.taskId?this.taskId:this.requestId, false);
  }

  getRequestDetails() {
    if(this.requestId) {
      this.loaderService.show();
      this.requestService.fetchRequestDetails(this.requestId).subscribe(request=> {
        if(request?.Properties?.Current_Checkpoint != "0") {//if(request?.Properties?.Current_Checkpoint > 0) {
        this.commentType = CommentConfig.PROJECT_GROUP_FILTER_VALUE;
       } else {
         this.commentType = CommentConfig.REQUEST_GROUP_FILTER_VALUE;
       }
       this.loaderService.hide();
       this.initComments();
      },error=> {
        this.loaderService.hide();
      })
    }
  }

  ngOnInit(): void {
    if(this.showGroupFilter) {
      this.selectedGroupFilter = CommentConfig.ALL_GROUP_FILTER_VALUE;
      this.getRequestDetails();
    } else {
      this.selectedGroupFilter = this.commentType;
      this.initComments();
    }
  }
}
