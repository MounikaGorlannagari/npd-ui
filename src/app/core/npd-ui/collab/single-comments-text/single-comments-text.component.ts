import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MPMFieldConstants, MPMField, MPMFieldKeys, MPM_LEVELS } from 'mpm-library';
import { map, startWith } from 'rxjs/operators';
import { CommentsModalComponent } from '../comments-modal/comments-modal.component';
import { CommentsTextComponent } from '../comments-text/comments-text.component';
import { NewCommentModal } from '../objects/comment.modal';
import { CommentConfig } from '../objects/CommentConfig';

@Component({
  selector: 'app-single-comments-text',
  templateUrl: './single-comments-text.component.html',
  styleUrls: ['./single-comments-text.component.scss']
})
export class SingleCommentsTextComponent extends CommentsTextComponent {
  @Input() taskDetails;
  @Output ()commentChange=new EventEmitter<any>();
  @Input() disableComment:boolean=false;

  @HostListener('window:keyup.enter') onKeyUp() {
    if (!this.commentFormGroup.invalid || this.commentFormGroup.value.commentText.length <= 2001) {

    }
  }
  openCommentDialog() {

    let selectedTask = this.taskDetails.selectedtask;
    const requestId = selectedTask?.PR_REQUEST_ITEM_ID.split('.')[1]
    const taskItemId: string = this.getPropertyByMapper(selectedTask, MPMFieldConstants.MPM_TASK_FIELDS.TASK_ITEM_ID).split('.')[1];
    const projectId = this.getPropertyByMapper(selectedTask, MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID)?.split('.')[1];
    const dialogRef = this.dialog?.open(CommentsModalComponent, {
      width: '75%',
      height: '90vh',
      maxHeight: '90vh',
      minHeight: '90vh',
      panelClass: 'comment-dialog-modal',
      disableClose: true,
      data: {
        requestId: requestId,
        projectId: projectId,
        taskId: taskItemId,
        commentType: CommentConfig.TASK_COMMENT_TYPE_VALUE,
        //isManager: this.isPMView,
        enableToComment: true // this.enableToComment
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('comments module dialog is closed \n', result);
    });
  }
  getProperty(displayColum: MPMField, taskData: any): string {
    return this.fieldConfigService.getFieldValueByDisplayColumn(displayColum, taskData);
  }
  getPropertyByMapper(taskData, mapperName): string {
    const displayColum: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.TASK);
    return this.getProperty(displayColum, taskData);
  }


  createSingleComment(): void {
    if (!this.commentFormGroup.invalid || this.commentFormGroup.value.commentText.length <= 2001) {
      let newComment: NewCommentModal = null;
      newComment = {
        requestId: null,
        taskId: null,
        commentText: '',
        commentType: null,
        refCommentId: (this.parentComment && this.parentComment.commentId) ? this.parentComment.commentId : null,
      };
      if (this.taggedUserList.length > 0) {
        newComment.tagUser = {
          userId: this.taggedUserList.map(data => {
            return data;
          })
        };
      }
      newComment = Object.assign(newComment, this.commentFormGroup.value);
      if (newComment.commentText.trim() && newComment.commentText.trim().length > 0) {
        this.saveComment(newComment)
      }
    }
  }
  public saveComment(commentDetails: NewCommentModal): void {

    const requestId = this.taskDetails.selectedtask.PR_REQUEST_ITEM_ID.split('.')[1]
    commentDetails = Object.assign(commentDetails, { requestId: requestId }, { taskId: this.taskDetails.taskId }, { commentType: 'TASK' });
    this.commentsService.createNewComment(commentDetails).subscribe(response => {
      if (response) {
        console.log('Comments Saved Successfully');
        // this.commentsConfig.pageDetails = this.commentUtilService.resetPageDetails();
        // this.loadDataOnRefresh(this.taskId?this.taskId:this.requestId, false);//currCommentItem.requestId
      }
    });
  }
  onCommentChange(comment) {

    if(comment && typeof(comment)!="object" && comment.trim()?.length == 0) {
      this.taggedUserList = [];
    }
    this.commentChange.emit(comment);
  }
  initalizeCommentText(commentText: string): void {
    this.disableSendIcon = commentText ? false : true;
    this.commentFormGroup = null;
    this.commentFormGroup = this.formBuilder.group({
      commentText: new FormControl({value:commentText,disabled:this.disableComment}, [Validators.maxLength(2000)]),
    });
    if (this.enableToComment == false || this.taskDetails.selectedtask?.IS_MILESTONE=='true') {
      this.commentFormGroup.disable();
    }
    if (this.commentFormGroup.controls.commentText) {
      this.commentFormGroup.controls.commentText.valueChanges.subscribe(data => {
        if (data && typeof data === 'string' && data.trim() === '') {
          this.disableSendIcon = true;
        } else {
          this.disableSendIcon = this.commentFormGroup.controls.commentText.invalid ? true : false;
        }
      });
    }
    this.filterUserListData = this.commentFormGroup.get('commentText').valueChanges
      .pipe(
        startWith(''),
        map(state => this.filterUserDetails(state && typeof state === 'string' ? state.trim() : ''))
      );
  }

}

