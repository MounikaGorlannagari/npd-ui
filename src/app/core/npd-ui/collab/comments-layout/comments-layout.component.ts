import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NewCommentModal, UserInfoModal } from '../objects/comment.modal';

@Component({
  selector: 'npd-comments-layout',
  templateUrl: './comments-layout.component.html',
  styleUrls: ['./comments-layout.component.scss']
})
export class CommentsLayoutComponent implements OnInit {
  @Input() commentDataList: Array<any>;
  @Input() pageDetails: any;
  @Input() isScrollDown: boolean;
  @Input() isReadonly: boolean;
  @Input() userListData: Array<UserInfoModal>;
  @Output() creatingNewComment = new EventEmitter<any>();
  @Output() loadingMoreComments = new EventEmitter<any>();
  @Input() enableToComment: boolean;
  parentComment = null;
  constructor(
  ) {
  }

  ngOnInit(): void {
  }
  onReplyToComment(parentComment): void {
    this.parentComment = { ...parentComment };
  }
  onCreatingNewComment(newComment: NewCommentModal): void {
    this.creatingNewComment.emit(newComment);
  }
  onLoadingMoreComments(pageDetail): void {
    this.loadingMoreComments.emit(pageDetail);
  }
}
