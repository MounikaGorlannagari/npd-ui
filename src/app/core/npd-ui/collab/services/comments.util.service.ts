import { Injectable } from '@angular/core';
import { CommentsModal, UserInfoModal } from '../objects/comment.modal';
import * as moment_ from 'moment';
import { SharingService, StatusLevel, UtilService } from 'mpm-library';


const moment = moment_;

@Injectable({
    providedIn: 'root'
})
export class CommentsUtilService {

    constructor(
        public utilService: UtilService,
        public sharingService: SharingService
    ) { }

    resetPageDetails(): any {
        return {
            page: 1,
            totalCount: 0,
            skip: 0
        };
    }
    getDaysBetweenTwoDates(startDate: Date, endDate: Date, diffType: moment_.unitOfTime.Diff): number {
        const start = moment(startDate);
        const end = moment(endDate);
        return start.diff(end, diffType, true);
    }

    addUnitsToDate(dateObject: Date, units: number, diffType: moment_.unitOfTime.Diff, workweek: boolean): Date {
        const start = moment(dateObject);
        if(!workweek) {
            if(diffType === 'weeks'){
                let days = units*5;
                
                dateObject = this.addBusinessDaysToDate(dateObject, days);
            }else{
                dateObject = this.addBusinessDaysToDate(dateObject, units);
            }
        }else{
            dateObject = start.add(units, diffType).toDate();
        }
        return dateObject;
    }

    addBusinessDaysToDate(date, days) {
        var day = date.getDay();
        date = new Date(date.getTime());
        date.setDate(date.getDate() + days + (day === 6 ? 2 : +!day) + (Math.floor((days - 1 + (day % 6 || 1)) / 5) * 2));
        return date;
      }
      
     

    getDateByGiveFormat(dateObject: Date, formatValue: string): string {
        const dateMoment = moment(dateObject);
        return dateMoment.format(formatValue);
    }

    formCommentsModalStructure(commentList: Array<any>): Array<CommentsModal> {
        let responseList: Array<CommentsModal>;
        responseList = [];
        if (commentList && Array.isArray(commentList) && commentList.length > 0) {
            commentList.forEach((data) => {
                let tempModal: CommentsModal = null;
                tempModal = {
                    commentId: this.getValueFormObjectByKeyList(data, ['Comments-id', 'Id'], 0, 0),
                    commentText: this.checkValueIsNullOrEmpty(data['CommentText'], ''),
                    userInfo: {
                        id: this.getValueFormObjectByKeyList(data, ['Tracking', 'CreatedBy', 'Identity-id', 'Id'], 0, 0),
                        name: '',
                        displayName: this.checkValueIsNullOrEmpty(data['UserName'], 'NA'),
                    },
                    timeStamp: new Date(this.getValueFormObjectByKeyList(data, ['Tracking', 'CreatedDate'], 0, new Date())),
                    projectDetail: this.checkValueIsNullOrEmpty(data['PROJECT_ID'], null),
                    refComment: null,
                    tagUser: [],
                    hover : null
                    //projectStatus: this.checkValueIsNullOrEmpty(data['PROJECT_STATUS'], '')
                };
                if(this.checkValueIsNullOrEmpty(data['RefCommentId'], null)) {
                    tempModal.refComment = {
                        refCommentId: this.checkValueIsNullOrEmpty(data['RefCommentId'], null),
                        refCommentText: this.checkValueIsNullOrEmpty(data['REFCommentText'], ''),

                    };
                }
                responseList.push(tempModal);
            });
        }
        return responseList;
    }

    formUserDetails(taskList: Array<any>): Array<UserInfoModal> {
        let responseList: Array<UserInfoModal>;
        responseList = [];
        const userDuplicate = {};
        if (taskList && Array.isArray(taskList) && taskList.length > 0) {
            taskList.forEach((data) => {
                let tempModal: UserInfoModal = null;
                const userId = this.checkValueIsNullOrEmpty(data['Identity-id']['Id'], null);
                if (!userDuplicate[userId]) {
                    tempModal = {
                        id: this.checkValueIsNullOrEmpty(data['Identity-id']['Id'], null),
                        displayName: this.checkValueIsNullOrEmpty(data["FullName"], 'NA'),//data["FullName"]//data['IdentityDisplayName']['__text']////name: this.checkValueIsNullOrEmpty(data['UserCN'], null),
                        userId : this.checkValueIsNullOrEmpty(data["UserId"], null)
                    };
                    responseList.push(tempModal);
                    userDuplicate[userId] = 1;
                }
            });
        }
        return responseList;
    }


    
    formUserForProjectDetails(taskList: Array<any>): Array<UserInfoModal> {
        let responseList: Array<UserInfoModal>;
        responseList = [];
        const userDuplicate = {};
        if (taskList && Array.isArray(taskList) && taskList.length > 0) {
            taskList.forEach((data) => {
                let tempModal: UserInfoModal = null;
                const userId = this.checkValueIsNullOrEmpty(data['UserID'], null);
                if (!userDuplicate[userId]) {
                    tempModal = {
                        id: this.checkValueIsNullOrEmpty(data['UserID'], null),
                        displayName: this.checkValueIsNullOrEmpty(data['DisplayName'], 'NA'),
                        name: this.checkValueIsNullOrEmpty(data['UserCN'], null)
                    };
                    responseList.push(tempModal);
                    userDuplicate[userId] = 1;
                }
            });
        }
        return responseList;
    }

    getValueFormObjectByKeyList(dataSet: any, keyList: Array<string>, index: number, defaultVale: any): any {
        if (index >= (keyList.length - 1) || !dataSet) {
            return dataSet && dataSet[keyList[index]] ? this.checkValueIsNullOrEmpty(dataSet[keyList[index]], defaultVale) : defaultVale;
        }
        const tempDataSet = JSON.parse(JSON.stringify(dataSet));
        return this.getValueFormObjectByKeyList(tempDataSet[keyList[index]], keyList, (index + 1), defaultVale);
    }

    checkValueIsNullOrEmpty(data: any, defaultVale: any): any {
        if (!data || (data['@xsi:nil'] === 'true') || (data === '') || (data['@nil'] === 'true')) {
            return defaultVale;
        }
        return data;
    }
    getStatusNameById(statusId: string, categoryId: string): string {
        const currStatus = this.sharingService.getAllStatusConfig().find(data => {
            return data['MPM_Status-id']['Id'] === statusId && data['R_PO_CATAGORY_LEVEL']['MPM_Category_Level-id']['Id'] === categoryId;
        });
        return currStatus ? currStatus['NAME'].replace(/ /g, '').toUpperCase() : '';
    }
    getStatusNameByStatusId(statusId: string): string {
        const currStatus = this.sharingService.getAllStatusConfig().find(data => {
            return data['MPM_Status-id']['Id'] === statusId;
        });
        return currStatus ? currStatus['NAME'].replace(/ /g, '').toUpperCase() : '';
    }
    getStatusLevelByStatusId(statusId: string): StatusLevel {
        const currStatus = this.sharingService.getAllStatusConfig().find(data => {
            return data['MPM_Status-id']['Id'] === statusId;
        });
        return currStatus ? currStatus['STATUS_LEVEL'] : null;
    }
}
