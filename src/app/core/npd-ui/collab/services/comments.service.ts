import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { CommentsUtilService } from './comments.util.service';
import * as acronui from 'mpm-library';
import { AppService, NotificationService, UtilService } from 'mpm-library';
import { NewCommentModal, UserInfoModal } from '../objects/comment.modal';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  public addReplyHeight = new Subject<any>();
  SAVE_COMMENT = 'http://schemas.acheron.com/mpm/comments/bpm/1.0';
  constructor(
    public appService: AppService,
    public utilService: UtilService,
    public commentUtilService: CommentsUtilService,
    public notificationService: NotificationService
  ) { }



  COMMENTS_NS = 'http://schemas/NPDSubmission/Comments/operations';
  GET_COMMENTS_WS = 'GetCommentsByRequestID';
  COMMENTS_BY_TYPE_NS = 'http://schemas/NPDSubmission/Comments/operations';
  GET_COMMENTS_BY_TYPE_WS = 'GetRequestCommentsByType';
  DELIVERABLE_OPERATION = 'http://schemas/AcheronMPMCore/Deliverable/operations';
  SAVE_COMMENT_NS = 'http://schemas.monster.npd.com/comments/bpm/1.0';
  SAVE_COMMENT_WS = 'SaveComments';
  CURSOR_NS = 'http://schemas.opentext.com/bps/entity/core';
  USER_PROJECT_DELIVERABLE_MAPPER = {};

  getAddReplyHeight(change: any) {
    this.addReplyHeight.next(change);
  }

  createNewComment(newComment: NewCommentModal): Observable<any> {
    const param = {
      comments : newComment
    };
    return new Observable(observer => {
      this.appService.invokeRequest(this.SAVE_COMMENT_NS, this.SAVE_COMMENT_WS, param)
        .subscribe(response => {
          observer.next(true);
          observer.complete();
        }, error => {
          observer.next(false);
          observer.complete();
        });
    });
  }

  getComments(requestId, skip: number): Observable<any> {
    const param = {
      requestId : requestId,
      Cursor: {
        '@xmlns': this.CURSOR_NS,
        '@offset': skip,
        '@limit': 100
      }
    };
    return new Observable(observer => {
      this.appService.invokeRequest(this.COMMENTS_NS, this.GET_COMMENTS_WS, param)
        .subscribe(response => {
          const statuses = acronui.findObjectsByProp(response, 'Comments');
          const commentList= this.commentUtilService.formCommentsModalStructure(statuses);
          observer.next(commentList);
          observer.complete();
        }, error => {
          observer.error(error);
        });
    });
  }

  getCommentsByType(requestId,commentType,skip: number): Observable<any> {
    const param = {
      requestId : requestId,
      commentType : commentType,
      Cursor: {
        '@xmlns': this.CURSOR_NS,
        '@offset': skip,
        '@limit': 100
      }
    };
    return new Observable(observer => {
      this.appService.invokeRequest(this.COMMENTS_BY_TYPE_NS, this.GET_COMMENTS_BY_TYPE_WS, param)
        .subscribe(response => {
          const statuses = acronui.findObjectsByProp(response, 'Comments');
          const commentList= this.commentUtilService.formCommentsModalStructure(statuses);
          observer.next(commentList);
          observer.complete();
        }, error => {
          observer.error(error);
        });
    });
  }

  getAllUsers() : Observable<any> {
    return new Observable(observer => {
    this.appService.getAllUsers().subscribe(allUsers => {
        const users = acronui.findObjectsByProp(allUsers, 'User');
        const userList: Array<UserInfoModal> = this.commentUtilService.formUserDetails(users);
              observer.next(userList);
              observer.complete();
          }, error => {
              observer.error(error);
        });
    });
  }

  getUsersForProject(projectId: number): Observable<any> {
    const param = {
      ProjectID: projectId
    };
    return new Observable(observer => {
        this.appService.invokeRequest(this.SAVE_COMMENT, 'GetUsersForProject', param)
          .subscribe(response => {
            const users = acronui.findObjectsByProp(response, 'User');
            const userList: Array<UserInfoModal> = this.commentUtilService.formUserForProjectDetails(users);
            //this.setUserProjectDeliverableMapper(projectId, userList);
            observer.next(userList);
            observer.complete();
          }, error => {
            console.error(error);
            this.notificationService.error('Error while getting users for the project.');
            observer.next([]);
            observer.complete();
            // observer.error(error);
          });
    });
  }



/*   clearUserProjectDeliverableMapper() {
    this.USER_PROJECT_DELIVERABLE_MAPPER = {};
  } */

}
