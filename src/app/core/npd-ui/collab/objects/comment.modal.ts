export interface UserInfoModal {
    id: string;
    displayName: string;
    name?: string;
    userId?: string;
    commentText?: string;
}

export interface CommentsModal {
    commentId: number;
    commentText: string;
    userInfo: UserInfoModal;
    timeStamp: Date;
    projectDetail: number;
    refComment: {
        refCommentId: number;
        refCommentText: string;
    };
    tagUser?: Array<UserInfoModal>;
    hover : boolean;
}

export interface NewCommentModal {
    requestId: number;
    taskId?:number;
    commentText: string;
    commentType : string;
    refCommentId : number;
    tagUser?: {
        userId: Array<any>;
    };
}

/* export interface ProjectDeliverableModal {
    titleName: string;
    icon: string;
    isActive: boolean;
    isProject: boolean;
    requestId: number;
    status?: string;
    owner?: string;
} */

