export const CommentConfig = {

    groupFilters : [
        {
          displayName: 'Request',
          value: 'REQUEST'
        },
        {
          displayName: 'Project',
          value: 'PROJECT'
        },
        {
          displayName: 'All',
          value: 'ALL'
        }
    ],
    REQUEST_GROUP_FILTER_VALUE :'REQUEST',
    PROJECT_GROUP_FILTER_VALUE :'PROJECT',
    TASK_COMMENT_TYPE_VALUE :'TASK',
    ALL_GROUP_FILTER_VALUE :'ALL'
}