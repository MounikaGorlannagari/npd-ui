import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NewCommentModal, CommentsModal, UserInfoModal } from '../objects/comment.modal';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { SharingService, UtilService, ProjectService, EntityAppDefService, ApplicationConfigConstants, ProjectConstant, FieldConfigService } from 'mpm-library';
import { MatDialog } from '@angular/material/dialog';
import { CommentsService } from '../services/comments.service';

@Component({
  selector: 'npd-comments-text',
  templateUrl: './comments-text.component.html',
  styleUrls: ['./comments-text.component.scss']
})
export class CommentsTextComponent implements OnInit {
  @Input() parentComment: CommentsModal;
  @Input() userListData: Array<UserInfoModal>;
  @Output() creatingNewComment = new EventEmitter<any>();
  @Input() enableToComment: boolean;

  commentFormGroup: FormGroup;
  disableSendIcon = true;
  filterUserListData: Observable<Array<UserInfoModal>>;
  isTagUser = false;
  taggedUserList = [];

  commentConfig: any;
  defaultComment: string;

  constructor(
    public formBuilder: FormBuilder,
    public sharingService: SharingService,
    public utilService: UtilService,
    public projectService: ProjectService,
    public entityAppDefService: EntityAppDefService,
    public fieldConfigService: FieldConfigService,
    public dialog: MatDialog,
    public commentsService: CommentsService,

  ) { }

  initalizeCommentText(commentText: string): void {
    this.disableSendIcon = commentText ? false : true;
    this.commentFormGroup = null;
    this.commentFormGroup = this.formBuilder.group({
      commentText: new FormControl(commentText, [Validators.maxLength(2000)]),
    });
    if (this.enableToComment == false) {
      this.commentFormGroup.disable();
    }
    if (this.commentFormGroup.controls.commentText) {
      this.commentFormGroup.controls.commentText.valueChanges.subscribe(data => {
        if (data && typeof data === 'string' && data.trim() === '') {
          this.disableSendIcon = true;
        } else {
          this.disableSendIcon = this.commentFormGroup.controls.commentText.invalid ? true : false;
        }
      });
    }
    this.filterUserListData = this.commentFormGroup.get('commentText').valueChanges
      .pipe(
        startWith(''),
        map(state => this.filterUserDetails(state && typeof state === 'string' ? state.trim() : ''))
      );
  }

  tagUserForComment(changeEvent): void {
    this.isTagUser = true;
  }

  onCommentChange(comment) {
    if(comment && typeof(comment)!="object" && comment.trim()?.length == 0) {
      this.taggedUserList = [];
    }
  }

  createNewComment(clickEvent): void {
    let newComment: NewCommentModal = null;
    newComment = {
      requestId: null,
      taskId:null,
      commentText : '',
      commentType : null,
      refCommentId: (this.parentComment && this.parentComment.commentId) ? this.parentComment.commentId : null,
    };
    if(this.taggedUserList.length > 0) {
      newComment.tagUser = {
        userId: this.taggedUserList.map(data => {
          return data;
        })
      };
    }

    newComment = Object.assign(newComment, this.commentFormGroup.value);
    if (newComment.commentText.trim() && newComment.commentText.trim().length > 0) {
      this.taggedUserList = [];
      this.removeParentComment(null);
      this.initalizeCommentText('');
      this.creatingNewComment.emit(newComment);
    } else {
      this.disableSendIcon = true;
    }
  }
  removeParentComment(removeEvent): void {
    this.parentComment = null;
  }
  manageFiltervalue(filterValue: string): string {
    return null;
  }
  filterUserDetails(filterValue: string): Array<UserInfoModal> {
    // this.disableSendIcon = filterValue && filterValue.length > 0 ? false : true;
    if (filterValue && typeof filterValue === 'string' && filterValue.indexOf('@') >= 0) {
      const filterValueTemp = filterValue.substr(filterValue.lastIndexOf('@') + 1).toLowerCase();
      const filteredResult = this.userListData.filter(data => {
        return data.displayName.toLowerCase().indexOf(filterValueTemp) >= 0 && data.id !== this.sharingService.getCurrentUserItemID();
      }).map(data => {
        return Object.assign(data, { commentText: filterValue });
      });
      return filteredResult;
    } /* else {
      this.disableSendIcon = this.commentFormGroup.value.commentText ? false : true;
    } */
    /*     if (this.commentFormGroup.invalid) {
          this.disableSendIcon = true;
        } */
    return [];
  }

  @HostListener('window:keyup.enter') onKeyUp() {
    if (!this.commentFormGroup.invalid || this.commentFormGroup.value.commentText.length <= 2001) {
      this.createNewComment(null);
    }
  }
  onOptionSelected(eventData): void {
    if (eventData && eventData.option && eventData.option.value && this.commentFormGroup) {
      const subjectData = eventData.option.value;
      if (this.taggedUserList.indexOf(eventData.option.value.id) < 0) {
        this.taggedUserList.push(eventData.option.value.id);
      }
      this.initalizeCommentText(subjectData.commentText.substr(0, subjectData.commentText.lastIndexOf('@')) + ('@' + subjectData.displayName + ' '));
    }
  }
  displayFunction(subjectData: UserInfoModal): string {
    if (subjectData && subjectData.commentText) {
      return subjectData.commentText.substr(0, subjectData.commentText.lastIndexOf('@')) + ('@' + subjectData.displayName + ' ');
    } else if (typeof subjectData === 'string') {
      return subjectData;
    }
    return '';
  }
  ngOnInit(): void {
    this.taggedUserList = [];
    this.commentConfig = this.sharingService.getCommentConfig();
    this.defaultComment = this.commentConfig[ApplicationConfigConstants.MPM_COMMENTS_CONFIG.DEFAULT_COMMENT];
      this.commentConfig[ApplicationConfigConstants.MPM_COMMENTS_CONFIG.EXTERNAL_INTERNAL_COMMENT_SWITCH_LABEL];
    this.initalizeCommentText('');
  }
}
