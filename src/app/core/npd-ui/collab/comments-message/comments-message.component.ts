import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { CommentsUtilService } from '../services/comments.util.service';
import { UserInfoModal, CommentsModal } from '../objects/comment.modal';
import { CommentsService } from '../services/comments.service';
import { SharingService } from 'mpm-library';
import { trigger, state, style, transition, animate } from '@angular/animations';


@Component({
  selector: 'npd-comments-message',
  templateUrl: './comments-message.component.html',
  styleUrls: ['./comments-message.component.scss'],
  /* animations: [
    trigger('fadeIn', [
   /*    state('true', style({ opacity: '2' })),
      state('false', style({ opacity: '1' })),
      transition('* => *', [animate(1000)])
          state('false', style({
        'background': '*'
      })), 
      state('true', style({
        transform: 'rotateY(360deg)'
      })),
      transition('false => true', [animate(1000)]),
    ])
   
  ] html
  [@fadeIn]="commentData.hover"*/

})
export class CommentsMessageComponent implements OnInit, OnChanges {
  @Input() commentData: CommentsModal;
  @Input() isReply: boolean;
  @Input() userListData: Array<UserInfoModal>;
  @Output() replyToComment = new EventEmitter<any>();
  @Output() clickingOnReplyMessage = new EventEmitter<any>();
  commentTextTemp = null;
  currUserId = null;
  hover: boolean;
  arrayInfo = [];
  reasonInfo = [];
  restartReasonInfo = [];
  
  constructor(
    public commentUtilService: CommentsUtilService,
    public sharingService: SharingService,
    public commentsService: CommentsService,

  ) {
    this.currUserId = this.sharingService.getCurrentUserItemID();
  }

  ngOnInit(): void {
    this.hover = false;
    this.commentTextTemp = this.handleUserInfoDetailsTemp(this.commentData.commentText, this.userListData.map(data => data.displayName));
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.isReply.currentValue != changes.isReply.firstChange){
      this.setAddReplyHeight(changes.isReply.firstChange);
    };
  }
  setAddReplyHeight(reply){
    this.commentsService.getAddReplyHeight(reply);
  }

  getFirstLetterFromUserName(name) {
    if(name && name.length > 0) {
      return name.charAt(0);
    } else {
      return '';
    }
  }
  onclickToReply(data) {
    if (this.replyToComment && typeof this.replyToComment.emit === 'function') {
      this.replyToComment.emit(data);
    }
  }
  toggleEvent(event) {
    this.hover = event;
  }
  handleDateManipulation(dateString): string {
    if (!dateString) {
      return '';
    }
    if (dateString && !(dateString instanceof Date)) {
      dateString = new Date(dateString);
    }
    let numericValueDiff = 0;
    numericValueDiff = this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'day');
    if (numericValueDiff < 1) {
      numericValueDiff = this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'hours');
      let calculateDateTime;
      if (numericValueDiff < 1) {
        calculateDateTime = Math.floor(this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'minutes'))
        return calculateDateTime + (calculateDateTime == 1 ?' min ' : ' mins ')+  'ago.' ;
      } else {
        calculateDateTime =  Math.floor(this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'hours'))
        return calculateDateTime + (calculateDateTime == 1 ?' hour ' : ' hours ')+  'ago.' ;
      }
    }
    return this.commentUtilService.getDateByGiveFormat(dateString, 'MMM D Y, h:mm a');
  }
  onParentCommentNavigation(parentCommentId) {
    this.clickingOnReplyMessage.emit(parentCommentId);
  }

  handleUserInfoDetailsTemp(commentText: string, displayName: Array<any>) {
    if (this.commentData.commentId) {
      this.reasonInfo = [];
      // removed for code hardening
      //    this.commentsService.getReasonsByCommentId(this.commentData.commentId).subscribe(reasonResponse => {
      /*    console.log(reasonResponse);
         const reasons = reasonResponse && reasonResponse.MPM_Status_Reason ? reasonResponse.MPM_Status_Reason : null;
         if (reasons) {
           if (Array.isArray(reasons)) {
             reasons.forEach(reason => {
               this.reasonInfo.push(reason.NAME);
             });
           } else {
             this.reasonInfo.push(reasons.NAME);
           }
         }
         console.log(this.reasonInfo); */

      if (!commentText) {
        return 'SUCCESS';
      }
      const positionofAt = commentText.indexOf('@');
      if (positionofAt >= 0) {
        this.arrayInfo.push({ text: commentText.substr(0, (positionofAt)), isUser: false });
        commentText = commentText.substr(positionofAt + 1);
        let isUserThere = false;
        displayName.forEach(data => {
          if (commentText.indexOf(data) === 0) {
            isUserThere = true;
            this.arrayInfo.push({ text: commentText.substr(0, data.length), isUser: true });
            commentText = commentText.substr(data.length);
          }
        });
        if (!isUserThere) {
          this.arrayInfo[this.arrayInfo.length - 1].text = (this.arrayInfo[this.arrayInfo.length - 1].text + '@');
        }
      } else {
        if (commentText && commentText.split(':') && commentText.split(':').length > 1) {
          this.restartReasonInfo = [];
          this.arrayInfo.push({ text: commentText.split(':')[1], isUser: false });

          let reasons;
          reasons = commentText.split(':') && commentText.split(':')[0] && commentText.split(':')[0].split(',') ? commentText.split(':')[0].split(',') : '';
          if (reasons) {
            reasons.forEach(reason => {
              this.restartReasonInfo.push(reason);
            });
          }
          commentText = '';

        } else {
          this.arrayInfo.push({ text: commentText, isUser: false });
          commentText = '';
        }
      }
      return this.handleUserInfoDetailsTemp(commentText, displayName);
      //  });
    }
    /*    if (!commentText) {
         return 'SUCCESS';
       }
       const positionofAt = commentText.indexOf('@');
       if (positionofAt >= 0) {
         this.arrayInfo.push({ text: commentText.substr(0, (positionofAt)), isUser: false });
         commentText = commentText.substr(positionofAt + 1);
         let isUserThere = false;
         displayName.forEach(data => {
           if (commentText.indexOf(data) === 0) {
             isUserThere = true;
             this.arrayInfo.push({ text: commentText.substr(0, data.length), isUser: true });
             commentText = commentText.substr(data.length);
           }
         });
         if (!isUserThere) {
           this.arrayInfo[this.arrayInfo.length - 1].text = (this.arrayInfo[this.arrayInfo.length - 1].text + '@');
         }
       } else {
         this.arrayInfo.push({ text: commentText, isUser: false });
         commentText = '';
       }
       return this.handleUserInfoDetailsTemp(commentText, displayName); */
  }

  ngOnDestroy(){
    this.setAddReplyHeight(false);
  }
}
