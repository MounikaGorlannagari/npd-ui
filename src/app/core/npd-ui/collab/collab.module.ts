import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsLayoutComponent } from './comments-layout/comments-layout.component';
import { CommentsMessageComponent } from './comments-message/comments-message.component';
import { CommentsTextComponent } from './comments-text/comments-text.component';
import { CommentsComponent } from './comments.component';
import { CommentsMessageLayoutComponent } from './comments-message-layout/comments-message-layout.component';
import { MaterialModule } from 'src/app/material.module';
import { MpmLibraryModule } from 'mpm-library';
import { CommentsModalComponent } from './comments-modal/comments-modal.component';
import { SingleCommentsTextComponent } from './single-comments-text/single-comments-text.component';

@NgModule({
  declarations: [
    CommentsLayoutComponent,
    CommentsMessageComponent,
    CommentsTextComponent,
    CommentsComponent,
    CommentsMessageLayoutComponent,
    CommentsModalComponent,
    SingleCommentsTextComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MpmLibraryModule
  ],
  exports: [
    CommentsLayoutComponent,
    CommentsMessageComponent,
    CommentsTextComponent,
    CommentsComponent,
    CommentsMessageLayoutComponent,
    SingleCommentsTextComponent
  ]
})
export class CollabModule { }
