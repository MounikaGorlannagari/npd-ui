export class NPDConfig {

    public static _instance = new NPDConfig();
    public static integrationConfig: any = null;


    constructor() {
        NPDConfig._instance = this;
    }

    public static getInstance() {
        return NPDConfig._instance;
    }

    public static setIntegrationConfig(integrationConfigObj) {
        NPDConfig.integrationConfig = integrationConfigObj;
    }

    public static getIntgrationConfig() {
        return NPDConfig.integrationConfig;
    }
}