export interface NpdPageObjectInterface {
    length: any;
    pageIndex: any;
    pageSize: any;
    totalPages: any;
}
