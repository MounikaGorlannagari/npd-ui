import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { NotificationService } from 'mpm-library';
import { NpdPageObjectInterface } from '../npd-page.object.interface';

@Component({
  selector: 'app-npd-pagination',
  templateUrl: './npd-pagination.component.html',
  styleUrls: ['./npd-pagination.component.scss']
})
export class NpdPaginationComponent implements OnInit, OnChanges {

  @Input() length;
  @Input() pageSize;
  @Input() pageIndex;
  @Output() paginator = new EventEmitter<any>();

  @ViewChild(MatPaginator, { static: true }) matPaginator: MatPaginator;

  pageSizeOptions: number[] = [5, 10, 15 ,30, 50];
  totalPageNumber = 1;
  manualPageChangeControl: FormControl;

  constructor(
      public notificationService: NotificationService
  ) { }

  calculateTotalPageSize() {
      this.pageIndex = this.pageIndex ? Number(this.pageIndex) : (this.matPaginator.pageIndex + 1);
      const pageSize = this.pageSize ? Number(this.pageSize) : this.matPaginator.pageSize;
      this.matPaginator.pageIndex = this.pageIndex - 1;
      this.matPaginator.pageSize = pageSize;

      this.totalPageNumber = Math.ceil(this.length / pageSize);
  }

  changePaginator() {
      if (this.pageSize !== this.matPaginator.pageSize) {
          this.pageIndex = 1;
          this.matPaginator.pageIndex = 0;
      } else {
          this.pageIndex = this.matPaginator.pageIndex + 1;
      }
      this.pageSize = this.matPaginator.pageSize;

      this.initializeForm();
      const pageObject: NpdPageObjectInterface = {
          length: this.matPaginator.length,
          pageIndex: this.matPaginator.pageIndex,
          pageSize: this.matPaginator.pageSize,
          totalPages: this.totalPageNumber
      };
      this.paginator.next(pageObject);
  }

  onpageValueChange(event) {
      if (event.target && event.target.value && (event.relatedTarget == null || event.relatedTarget == undefined)) {
          if (Number(event.target.value) <= this.totalPageNumber) {
              this.matPaginator.pageIndex = Number(event.target.value) - 1;
              this.changePaginator();
          } else {
              this.notificationService.error('Current page exceeds total no. of pages');
          }
      }
  }

  initializeForm() {
      this.calculateTotalPageSize();
      this.manualPageChangeControl = new FormControl(this.pageIndex,
          [Validators.max(this.totalPageNumber), Validators.min(1), Validators.required]);
  }

  ngOnChanges() {
      this.initializeForm();
  }

  numberOnly(event): boolean {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;

  }

  ngOnInit() {
      this.initializeForm();
      this.matPaginator.page.subscribe(() => {
          this.changePaginator();
      });
  }
}
