import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NpdPaginationComponent } from './npd-pagination/npd-pagination.component';
import { MaterialModule } from 'src/app/material.module';



@NgModule({
  declarations: [NpdPaginationComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    NpdPaginationComponent
  ]
  
})
export class NpdPaginationModule { }
