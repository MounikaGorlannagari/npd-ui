import { Injectable } from '@angular/core';
import { ApplicationConfigConstants, AppService, NotificationService, UtilService } from 'mpm-library';
import { Observable } from 'rxjs';
import * as acronui from 'mpm-library';
import { EntityService } from './entity.service';
import { EntityListConstant } from '../request-view/constants/EntityListConstant';
import { NpdSessionStorageConstants } from '../request-view/constants/sessionStorageConstants';
import { MONSTER_ROLES } from '../filter-configs/role.config';

@Injectable({
  providedIn: 'root',
})
export class MonsterConfigApiService {
  MARKET_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Markets/operations';
  GET_MARKET_DETAILS_WS_METHOD_NAME = 'GetActiveMarkets';

  BUSINESS_UNIT_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Business_Unit/operations';
  GET_BUSINESS_UNIT_DETAILS_WS_METHOD_NAME = 'GetActiveBusinessUnits';

  BRAND_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Brands/operations';
  GET_BRAND_DETAILS_WS_METHOD_NAME = 'GetActiveBrandDetails';

  PLATFORM_DETAILS_BY_BRAND_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Brands/operations';
  GET_PLATFORM_DETAILS_BY_BRAND_WS_METHOD_NAME = 'GetR_PM_PLATFORMS';

  VARIANT_DETAILS_BY_PLATFORM_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Platforms/operations';
  GET_VARIANT_DETAILS_BY_PLATFORM_WS_METHOD_NAME = 'GetR_PM_VARIANT_SKU';

  PROJECT_TYPE_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Project_Type/operations';
  GET_PROJECT_TYPE_DETAILS_WS_METHOD_NAME = 'GetActiveProjectType';

  PACKAGING_TYPE_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Packaging_Type/operations';
  GET_PACKAGING_TYPE_DETAILS_WS_METHOD_NAME = 'GetActivePackagingType';

  COMMERCIAL_CATEGORIZATION_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Commercial_Categorisation/operations';
  GET_COMMERCIAL_CATEGORIZATION_DETAILS_WS_METHOD_NAME =
    'GetActiveCommercialCatergorisation';

  GOVERNANCE_APPROACH_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Governance_Approach/operations';
  GET_GOVERNANCE_APPROACH_DETAILS_WS_METHOD_NAME =
    'GetActiveGovernanceApproachDetails';

  CASE_PACK_SIZE_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Case_Pack_Size/operations';
  CASE_PACK_SIZE_DETAILS_WS_METHOD_NAME = 'GetActiveCasePackSizes';

  CONSUMER_UNIT_SIZE_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Consumer_Unit_Size/operations';
  CONSUMER_UNIT_SIZE_DETAILS_WS_METHOD_NAME = 'GetActiveConsumerUnitSizes';

  //PRODUCTION_SITE_DETAILS_METHOD_NS = 'http://schemas/NPDLookUpDomainModel/Production_Site/operations';
  //PRODUCTION_SITE_DETAILS_WS_METHOD_NAME = 'GetActiveProductionSites';

  BOTTLER_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Bottler/operations';
  BOTTLER_DETAILS_WS_METHOD_NAME = 'GetActiveBottler';

  SKU_DETAILS_METHOS_NS =
    'http://schemas/NPDLookUpDomainModel/Sku_Details/operations';
  SKU_DETAILS_WS_METHOD_NAME = 'GetActiveSKUDetails';

  //EARLY_MANUFACTURING_DETAILS_METHOD_NS = 'http://schemas/NPDLookUpDomainModel/Early_Manufacturing_Site/operations';
  //GET_EARLY_MANUFACTURING_DETAILS_WS_METHOD_NAME = 'GetActiveEarlyManufacturingSites';

  EARLY_MANUFACTURING_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Draft_Manufacturing_Location/operations';
  GET_EARLY_MANUFACTURING_DETAILS_WS_METHOD_NAME = 'GetR_PM_MANUFACTURINGSITE';

  PROGRAM_TAG_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Program_Tag/operations';
  GET_PROGRAM_TAG_DETAILS_WS_METHOD_NAME = 'GetActiveProgramDetails';

  DRAFT_MANUFACTURING_LOCATION_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Draft_Manufacturing_Location/operations';
  GET_DRAFT_MANUFACTURING_LOCATION_DETAILS_WS_METHOD_NAME =
    'GetActiveDraftManufacturingLocation';

  THRESHOLD_VALUES_FOR_MARKET_BRAND =
    'http://schemas.npd.monster.com/jobrequest/1.0';
  GET_THRESHOLD_VALUES = 'GetThresholdValues';

  PROJECT_CLASSIFIER_DATA =
    'http://schemas/NPDLookUpDomainModel/Project_Classifier_Data/operations';
  GENERATE_CLASSIFICATION_DATA='http://schemas.npd.monster.com/jobrequest/1.0';
  GET_PROJECT_CLASSIFICATION = 'GetProjectClassification';
  GENERATE_CLASSIFICATION_NUMBER = 'GenerateClassificationNumber';


  REQUEST_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Request_Type/operations';
  GET_REQUEST_DETAILS_WS_METHOD_NAME = 'GetActiveRequestTypes';

  TASK_MENU_CLAIM_REVOKE = 'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  Task_Operation = 'TaskActionHandler';

  REPORTING_PROJECT_TYPE =
    'http://schemas/NPDLookUpDomainModel/CP_Project_Type/operations';
  Get_Active_CP_Project_Types = 'GetActiveCPProjectTypes';

  PROJECT_SUB_TYPE =
    'http://schemas/NPDLookUpDomainModel/CP_Project_Type/operations';
  GetR_PM_PROJECT_SUB_TYPE = 'GetR_PM_PROJECT_SUB_TYPE';

  PROJECT_DETAIL =
    'http://schemas/NPDLookUpDomainModel/Project_Sub_Type/operations';
  GetR_PM_PROJECT_DETAIL = 'GetR_PM_PROJECT_DETAIL';

  DELIVERY_QUARTERS =
    'http://schemas/NPDLookUpDomainModel/PM_Delivery_Quarter/operations';
  GetDELIVERY_QUARTERS = 'GetAllDeliveryQuarters';

  REGULATORY_TIMELINE_METHOS_NS =
    'http://schemas/NPDLookUpDomainModel/Regulatory_Timeline/operations';
  REGULATORY_TIMELINE_WS_METHOD_NAME = 'GetRegulatoryTimelineForLocations';

  HandleSecondaryPackLeadTimeChanges_WS_NAME =
    'HandleSecondaryPackLeadTimeChanges';
  HandleSecondaryPackLeadTimeChanges_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';

  GET_ROLES_FOR_USER_NS = 'http://schemas.cordys.com/1.0/ldap';
  GET_ROLES_FOR_USER_WS_METHOD_NAME = 'GetRoles';

  UPDATE_GOVERNANCE_MILESTONE_METHOD_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  UPDATE_GOVERNANCE_MILESTONE_METHOD_NAME = 'UpdateGovernanceMilestones';

  UPDATE_SECONDARY_PACK_METHOD_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  UPDATE_SECONDARY_PACK_METHOD_NAME = 'UpdateSecondaryPackage';
  PLATFORM_DETAILS_METHOD_NS =
    'http://schemas/NPDLookUpDomainModel/Platforms/operations';
  GET_PLATFORM_DETAILS_WS_METHOD_NAME = 'GetActivePlatforms';

  GET_ALL_EARLY_MANUFACTURING_SITES_NS =
    'http://schemas/NPDLookUpDomainModel/Early_Manufacturing_Site/operations';
  GET_ALL_EARLY_MANUFACTURING_SITES_WS = 'GetActiveEarlyManufacturingSites';
  GET_ALL_ACTIVE_VARIANT_NS =
    'http://schemas/NPDLookUpDomainModel/Variant_SKU/operations';
  GET_ALL_ACTIVE_VARIANT_WS = 'GetAllActiveVariants';
  GET_ALL_ACTIVE_PROJECT_SUBTYPE_NS =
    'http://schemas/NPDLookUpDomainModel/Project_Sub_Type/operations';
  GET_ALL_ACTIVE_PROJECT_SUBTYPE_WS = 'GetAllActiveProjectSubType';
  GET_ALL_ACTIVE_PROJECT_DETAILS_NS =
    'http://schemas/NPDLookUpDomainModel/Project_Detail/operations';
  GET_ALL_ACTIVE_PROJECT_DETAILS_WS = 'GetAllActiveProjectDetail';
  constructor(
    private appService: AppService,
    private notificationService: NotificationService,
    private entityService: EntityService,
    private utilService: UtilService
  ) {}

  // to clear all cached configs in local storage.
  clearLocalStorageConfigs() {
    //window.localStorage.removeItem('allBrands');
    //window.localStorage.removeItem('allLanguages');
  }

  // to parse stored data in local storage
  parseStorageValue(data: string) {
    const json = JSON.parse(data);
    return json;
  }

  // to get property node from appworks response
  getPropertyListFromResponse(response, property) {
    const propertyList = acronui.findObjectsByProp(response, property);
    return propertyList;
  }

  getListAsResponse(options) {
    let arrayList = [];
    if (options) {
      if (Array.isArray(options)) {
        arrayList = options;
      } else {
        arrayList.push(options);
      }
    }
    return arrayList;
  }

  transformResponse(list, propertyId, isSort?, isActive?, isMarket?) {
    let allList = [];
    if (isMarket) {
      for (var i = 0; i < list.length; i++) {
        let options = {
          displayName: list[i].DisplayName || list[i].DISPLAY_NAME,
          value: list[i].Value || list[i].VALUE,
          ItemId: list[i][propertyId].ItemId,
          Id: list[i][propertyId].Id,
          sequence: list[i].Sequence || list[i].SEQUENCE, //to sort dropdown values based on it
          businessUnit:
            list[i]?.R_PO_BUSINESS_UNIT &&
            list[i]?.R_PO_BUSINESS_UNIT['Business_Unit-id'],
          regulatoryClassification: list[i]?.RegulatoryClassification,
          ConversionRateEURO: list[i]?.ConversionRateEURO,
        };
        allList.push(options);
      }
    } else {
      for (var i = 0; i < list.length; i++) {
        let options = {
          displayName: list[i].DisplayName || list[i].DISPLAY_NAME,
          value: list[i].Value || list[i].VALUE,
          ItemId: list[i][propertyId].ItemId,
          Id: list[i][propertyId].Id,
          isActive: list[i].IsActive,
          sequence: list[i].Sequence || list[i].SEQUENCE, //to sort dropdown values based on it
        };
        allList.push(options);
      }
    }
    if (isActive) {
      allList = allList.filter((list) => list.isActive);
    }
    if (isSort) {
      allList.sort((a, b) => {
        return a.sequence - b.sequence;
      });
    }
    return allList;
  }
  transformResponseForLookup(list, propertyId, isSort?, isActive?, isMarket?) {
    let allList = [];
    if (isMarket) {
      for (var i = 0; i < list.length; i++) {
        let options = {
          displayName: list[i].DisplayName || list[i].DISPLAY_NAME,
          value: list[i].Value || list[i].VALUE,
        };
        allList.push(options);
      }
    } else {
      for (var i = 0; i < list.length; i++) {
        if (list[i].IsActive) {
          let options = {
            displayName: list[i].DisplayName || list[i].DISPLAY_NAME,
            value: list[i].Value || list[i].VALUE,
          };
          allList.push(options);
        }
      }
    }
    return allList;
  }

  getAllBrands(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_BRANDS) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_BRANDS)
          )
        );
      } else {
        this.fetchAllBrands(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_BRANDS)
            )
          );
        });
      }
    });
  }

  getAllReportingProjectType(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_REPORTING_TYPES
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_REPORTING_TYPES
            )
          )
        );
      } else {
        this.fetchReportingProjectType(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_REPORTING_TYPES
              )
            )
          );
        });
      }
    });
  }

  getAllMarkets(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_MARKETS) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_MARKETS)
          )
        );
      } else {
        this.fetchAllMarkets(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_MARKETS)
            )
          );
        });
      }
    });
  }

  getAllBusinessUnits(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_BUSINESS_UNITS
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_BUSINESS_UNITS
            )
          )
        );
      } else {
        this.fetchAllBusinessUnits(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_BUSINESS_UNITS
              )
            )
          );
        });
      }
    });
  }

  getAllProjectTypes(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_PROJECT_TYPES) !==
        null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_PROJECT_TYPES)
          )
        );
      } else {
        this.fetchAllProjectTypes(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_PROJECT_TYPES
              )
            )
          );
        });
      }
    });
  }

  getAllPackagingTypes(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_PACKAGING_TYPES
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_PACKAGING_TYPES
            )
          )
        );
      } else {
        this.fetchAllPackagingTypes(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_PACKAGING_TYPES
              )
            )
          );
        });
      }
    });
  }

  getAllCommercialCategorization(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_COMMERCIAL_CATEGORIES
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_COMMERCIAL_CATEGORIES
            )
          )
        );
      } else {
        this.fetchAllCommercialCategorization(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_COMMERCIAL_CATEGORIES
              )
            )
          );
        });
      }
    });
  }

  getAllGovernanceApproaches(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_GOVERNANCE_APPROACHES
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_GOVERNANCE_APPROACHES
            )
          )
        );
      } else {
        this.fetchAllGovernanceApproaches(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_GOVERNANCE_APPROACHES
              )
            )
          );
        });
      }
    });
  }

  getAllCasePackSizes(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_CASE_PACK_SIZES
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_CASE_PACK_SIZES
            )
          )
        );
      } else {
        this.fetchAllCasePackSizes(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_CASE_PACK_SIZES
              )
            )
          );
        });
      }
    });
  }

  /*     getAllProductionSites(): Observable<any> {
          return new Observable((observer) => {
              if (window.localStorage.allProductionSites) {
                  observer.next(this.parseStorageValue(window.localStorage.allProductionSites));
              }
              else {
                  this.fetchAllProductionSites(() => {
                      observer.next(this.parseStorageValue(window.localStorage.allProductionSites));
                  });
              }
          });
      } */

  getAllConsumerUnits(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_CONSUMER_UNITS
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_CONSUMER_UNITS
            )
          )
        );
      } else {
        this.fetchAllConsumerUnits(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_CONSUMER_UNITS
              )
            )
          );
        });
      }
    });
  }

  getAllBottlers(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_BOTTLERS) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_BOTTLERS)
          )
        );
      } else {
        this.fetchAllBottlers(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_BOTTLERS)
            )
          );
        });
      }
    });
  }

  getAllSKUDetails(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_SKU_DETAILS) !==
        null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_SKU_DETAILS)
          )
        );
      } else {
        this.fetchAllSKUDetails(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_SKU_DETAILS)
            )
          );
        });
      }
    });
  }

  /*  getAllDraftManufacturingLocations(): Observable<any> {
       return new Observable((observer) => {
           if (window.localStorage.allDraftManufacturingLocations) {
               observer.next(this.parseStorageValue(window.localStorage.allDraftManufacturingLocations));
           }
           else {
               this.fetchAllDraftManufacturingLocations(() => {
                   observer.next(this.parseStorageValue(window.localStorage.allDraftManufacturingLocations));
               });
           }
       });
   }
*/
  getAllDraftManufacturingLocations(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_DRAFT_MANUFACTURING_LOCATIONS
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_DRAFT_MANUFACTURING_LOCATIONS
            )
          )
        );
      } else {
        this.fetchAllDraftManufacturingLocations(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_DRAFT_MANUFACTURING_LOCATIONS
              )
            )
          );
        });
      }
    });
  }

  getAllRequestTypes(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_REQUEST_TYPES) !==
        null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_REQUEST_TYPES)
          )
        );
      } else {
        this.fetchAllRequestTypes(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_REQUEST_TYPES
              )
            )
          );
        });
      }
    });
  }

  getAllDeliveryQuarters(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_DELIVERY_QUARTERS
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_DELIVERY_QUARTERS
            )
          )
        );
      } else {
        this.fetchAllDeliveryQuarters(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_DELIVERY_QUARTERS
              )
            )
          );
        });
      }
    });
  }

  getAllProgramTags(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_PROGRAM_TAG) !==
        null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_PROGRAM_TAG)
          )
        );
      } else {
        this.fetchAllProgramTags(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_PROGRAM_TAG)
            )
          );
        });
      }
    });
  }

  // api call methods

  fetchAllBrands(callBack) {
    this.appService
      .invokeRequest(
        this.BRAND_DETAILS_METHOD_NS,
        this.GET_BRAND_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let brandOptions = [];
          const options = this.getPropertyListFromResponse(response, 'Brands');
          brandOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_BRANDS,
            JSON.stringify(brandOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_BRANDS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error('Unable to get all the brands.');
        }
      );
  }

  fetchReportingProjectType(callBack) {
    this.appService
      .invokeRequest(
        this.REPORTING_PROJECT_TYPE,
        this.Get_Active_CP_Project_Types,
        ''
      )
      .subscribe(
        (response) => {
          let projectTypeOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'CP_Project_Type'
          );
          projectTypeOptions = this.getListAsResponse(options);
          //window.localStorage.allReportingProjectTypes = JSON.stringify(projectTypeOptions);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_REPORTING_TYPES,
            JSON.stringify(projectTypeOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          //window.localStorage.allReportingProjectTypes = JSON.stringify([]);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_REPORTING_TYPES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get Reporting Project Type.'
          );
        }
      );
  }

  fetchReportingProjectSubType(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.PROJECT_SUB_TYPE,
          this.GetR_PM_PROJECT_SUB_TYPE,
          parameters
        )
        .subscribe(
          (response) => {
            let projectTypeOptions = [];
            const options = this.getPropertyListFromResponse(
              response,
              'Project_Sub_Type'
            );
            projectTypeOptions = this.getListAsResponse(options);
            observer.next(projectTypeOptions);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            this.notificationService.error(
              'Unable to get Reporting Sub Project Type.'
            );
          }
        );
    });
  }

  fetchReportingProjectDetail(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.PROJECT_DETAIL,
          this.GetR_PM_PROJECT_DETAIL,
          parameters
        )
        .subscribe(
          (response) => {
            let projectTypeOptions = [];
            const options = this.getPropertyListFromResponse(
              response,
              'Project_Detail'
            );
            projectTypeOptions = this.getListAsResponse(options);
            observer.next(projectTypeOptions);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            // this.notificationService.error('Unable to get Project Detail.');
          }
        );
    });
  }

  fetchAllDeliveryQuarters(callBack) {
    this.appService
      .invokeRequest(this.DELIVERY_QUARTERS, this.GetDELIVERY_QUARTERS, '')
      .subscribe(
        (response) => {
          let deliveryQuarterOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'PM_Delivery_Quarter'
          );
          deliveryQuarterOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_DELIVERY_QUARTERS,
            JSON.stringify(deliveryQuarterOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_DELIVERY_QUARTERS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error('Unable to get Delivery Quarters.');
        }
      );
  }

  fetchAllMarkets(callBack) {
    this.appService
      .invokeRequest(
        this.MARKET_DETAILS_METHOD_NS,
        this.GET_MARKET_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let marketOptions = [];
          const options = this.getPropertyListFromResponse(response, 'Markets');
          marketOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_MARKETS,
            JSON.stringify(marketOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_MARKETS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error('Unable to get all the markets.');
        }
      );
  }

  fetchAllBusinessUnits(callBack) {
    this.appService
      .invokeRequest(
        this.BUSINESS_UNIT_DETAILS_METHOD_NS,
        this.GET_BUSINESS_UNIT_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let businessUnitOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Business_Unit'
          );
          businessUnitOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_BUSINESS_UNITS,
            JSON.stringify(businessUnitOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_BUSINESS_UNITS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the business units.'
          );
        }
      );
  }

  fetchAllProjectTypes(callBack) {
    this.appService
      .invokeRequest(
        this.PROJECT_TYPE_DETAILS_METHOD_NS,
        this.GET_PROJECT_TYPE_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let projectTypeOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Project_Type'
          );
          projectTypeOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PROJECT_TYPES,
            JSON.stringify(projectTypeOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PROJECT_TYPES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the project types.'
          );
        }
      );
  }

  fetchAllPackagingTypes(callBack) {
    this.appService
      .invokeRequest(
        this.PACKAGING_TYPE_DETAILS_METHOD_NS,
        this.GET_PACKAGING_TYPE_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let packagingTypeOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Packaging_Type'
          );
          packagingTypeOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PACKAGING_TYPES,
            JSON.stringify(packagingTypeOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PACKAGING_TYPES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the packaging types.'
          );
        }
      );
  }

  fetchAllCommercialCategorization(callBack) {
    this.appService
      .invokeRequest(
        this.COMMERCIAL_CATEGORIZATION_DETAILS_METHOD_NS,
        this.GET_COMMERCIAL_CATEGORIZATION_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let commercialCategoryOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Commercial_Categorisation'
          );
          commercialCategoryOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_COMMERCIAL_CATEGORIES,
            JSON.stringify(commercialCategoryOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_COMMERCIAL_CATEGORIES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the Commercial Categories.'
          );
        }
      );
  }

  fetchAllCasePackSizes(callBack) {
    this.appService
      .invokeRequest(
        this.CASE_PACK_SIZE_DETAILS_METHOD_NS,
        this.CASE_PACK_SIZE_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let casePackSizeOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Case_Pack_Size'
          );
          casePackSizeOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_CASE_PACK_SIZES,
            JSON.stringify(casePackSizeOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_CASE_PACK_SIZES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the case pack sizes.'
          );
        }
      );
  }

  fetchAllConsumerUnits(callBack) {
    this.appService
      .invokeRequest(
        this.CONSUMER_UNIT_SIZE_DETAILS_METHOD_NS,
        this.CONSUMER_UNIT_SIZE_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let consumerUnitsOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Consumer_Unit_Size'
          );
          consumerUnitsOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_CONSUMER_UNITS,
            JSON.stringify(consumerUnitsOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_CONSUMER_UNITS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the consumer units.'
          );
        }
      );
  }

  fetchAllGovernanceApproaches(callBack) {
    this.appService
      .invokeRequest(
        this.GOVERNANCE_APPROACH_DETAILS_METHOD_NS,
        this.GET_GOVERNANCE_APPROACH_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let governanceApproachOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Governance_Approach'
          );
          governanceApproachOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_GOVERNANCE_APPROACHES,
            JSON.stringify(governanceApproachOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_GOVERNANCE_APPROACHES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the governance approaches.'
          );
        }
      );
  }

  /*     fetchAllProductionSites(callBack) {
          this.appService.invokeRequest(this.PRODUCTION_SITE_DETAILS_METHOD_NS,
              this.PRODUCTION_SITE_DETAILS_WS_METHOD_NAME, '').subscribe(response => {
                let productionSitessOptions = [];
                const options = this.getPropertyListFromResponse(response, 'Production_Site');
                productionSitessOptions = this.getListAsResponse(options);
                window.localStorage.allProductionSites = JSON.stringify(productionSitessOptions);
                if (callBack) {
                      callBack();
                }
              }, error => {
                window.localStorage.allProductionSites = JSON.stringify([]);
                if (callBack) {
                      callBack();
                }
                this.notificationService.error('Unable to get all the production sites.');
          });
      } */

  fetchAllBottlers(callBack) {
    this.appService
      .invokeRequest(
        this.BOTTLER_DETAILS_METHOD_NS,
        this.BOTTLER_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let bottlersOptions = [];
          const options = this.getPropertyListFromResponse(response, 'Bottler');
          bottlersOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_BOTTLERS,
            JSON.stringify(bottlersOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_BOTTLERS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error('Unable to get all the bottlers.');
        }
      );
  }

  fetchAllSKUDetails(callBack) {
    this.appService
      .invokeRequest(
        this.SKU_DETAILS_METHOS_NS,
        this.SKU_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let skuDetailssOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Sku_Details'
          );
          skuDetailssOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_SKU_DETAILS,
            JSON.stringify(skuDetailssOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_SKU_DETAILS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error('Unable to get all the sku details.');
        }
      );
  }

  fetchAllRequestTypes(callBack) {
    this.appService
      .invokeRequest(
        this.REQUEST_DETAILS_METHOD_NS,
        this.GET_REQUEST_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let requestTypeOptions = [];
          if (response) {
            let requestTypes = [];
            if (response && response.Request_Type) {
              if (Array.isArray(response.Request_Type)) {
                requestTypes = response.Request_Type;
              } else {
                requestTypes.push(response.Request_Type);
              }
            }
            for (var i = 0; i < requestTypes.length; i++) {
              let options = {
                name: requestTypes[i].DISPLAY_NAME,
                value: requestTypes[i].VALUE,
                appDynamicConfigId:
                  ApplicationConfigConstants.MPM_PROJECT_CONFIG
                    .ENABLE_CREATE_PROJECT,
                isShown: requestTypes[i].Is_Active,
                icon: 'description',
                IS_REQUEST_TYPE: true,
                itemId: requestTypes[i]['Request_Type-id'].ItemId,
              };
              requestTypeOptions.push(options);
            }
          }
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_REQUEST_TYPES,
            JSON.stringify(requestTypeOptions)
          );
          //window.localStorage.allRequestTypes = JSON.stringify(requestTypeOptions);
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          //window.localStorage.allRequestTypes = JSON.stringify([]);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_REQUEST_TYPES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the Request Types.'
          );
        }
      );
  }

  fetchAllThresholdValuesForMarketBrand(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.THRESHOLD_VALUES_FOR_MARKET_BRAND,
          this.GET_THRESHOLD_VALUES,
          parameters
        )
        .subscribe(
          (response) => {
            let thresholdValuesForMarketBrand = [];
            const options = this.getPropertyListFromResponse(
              response?.tuple?.old,
              'ThresholdData'
            );
            thresholdValuesForMarketBrand = this.getListAsResponse(options);
            observer.next(thresholdValuesForMarketBrand);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            this.notificationService.error('Unable to get Threshold values.');
          }
        );
    });
  }

  fetchProjectClassificationNoDescription(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.PROJECT_CLASSIFIER_DATA,
          this.GET_PROJECT_CLASSIFICATION,
          parameters
        )
        .subscribe(
          (response) => {
            let projectClassificationData = [];
            const options = this.getPropertyListFromResponse(
              response,
              'Project_Classifier_Data'
            );
            projectClassificationData = this.getListAsResponse(options);
            observer.next(projectClassificationData);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            this.notificationService.error(
              'Unable to get Project Classification Data.'
            );
          }
        );
    });
  }
  public generateClassificationNumber(parameters,isThrowableError?): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.GENERATE_CLASSIFICATION_DATA,
          this.GENERATE_CLASSIFICATION_NUMBER,
          parameters
        )
        .subscribe(
          (response) => {
            let projectClassificationData = [];
            const options = this.getPropertyListFromResponse(
              response,
              'ProjectClassification'
            );
            projectClassificationData = this.getListAsResponse(options);
            observer.next(projectClassificationData);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            // if(isThrowableError || isThrowableError==undefined || isThrowableError==null){
            //   this.notificationService.error(
            //     'No Project Classification Data Available for the Input'
            //   );
            // }
            
          }
        );
    });
  }

  fetchTaskMenuClaimRevoke(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.TASK_MENU_CLAIM_REVOKE,
          this.Task_Operation,
          parameters
        )
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  /* fetchAllDraftManufacturingLocations(callBack) {
      this.appService.invokeRequest(this.DRAFT_MANUFACTURING_LOCATION_DETAILS_METHOD_NS,
          this.GET_DRAFT_MANUFACTURING_LOCATION_DETAILS_WS_METHOD_NAME, '').subscribe(response => {
            let draftManufacturingLocationOptions = [];
            const options = this.getPropertyListFromResponse(response, 'Draft_Manufacturing_Location');
            draftManufacturingLocationOptions = this.getListAsResponse(options);
            window.localStorage.allDraftManufacturingLocations = JSON.stringify(draftManufacturingLocationOptions);
            if (callBack) {
                  callBack();
            }
          }, error => {
            window.localStorage.allDraftManufacturingLocations = JSON.stringify([]);
            if (callBack) {
                  callBack();
            }
            this.notificationService.error('Unable to get all the Draft Manufacturing Locations.');
      });
  } */

  fetchAllDraftManufacturingLocations(callBack) {
    this.entityService
      .getEntityObjects(
        EntityListConstant.DRAFT_MANUFACTURING_LOCATION,
        null,
        null,
        null,
        null,
        null,
        this.utilService.APP_ID
      )
      .subscribe(
        (response) => {
          const draftManufacturingLocationOptions = acronui.findObjectsByProp(
            response,
            'items'
          );
          //window.localStorage.allDraftManufacturingLocations = JSON.stringify(draftManufacturingLocationOptions);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_DRAFT_MANUFACTURING_LOCATIONS,
            JSON.stringify(draftManufacturingLocationOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          //window.localStorage.allDraftManufacturingLocations = JSON.stringify([]);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_DRAFT_MANUFACTURING_LOCATIONS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the Draft Manufacturing Locations.'
          );
        }
      );
  }

  fetchAllProgramTags(callBack) {
    this.appService
      .invokeRequest(
        this.PROGRAM_TAG_DETAILS_METHOD_NS,
        this.GET_PROGRAM_TAG_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let programTagOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Program_Tag'
          );
          programTagOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PROGRAM_TAG,
            JSON.stringify(programTagOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PROGRAM_TAG,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error('Unable to get all the Program Tags.');
        }
      );
  }

  fetchEarlyManufacturingSitesByDraftManufacturingLocation(
    parameters
  ): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.EARLY_MANUFACTURING_DETAILS_METHOD_NS,
          this.GET_EARLY_MANUFACTURING_DETAILS_WS_METHOD_NAME,
          parameters
        )
        .subscribe(
          (response) => {
            let earlyManufacturingSiteOptions = [];
            const options = this.getPropertyListFromResponse(
              response,
              'Early_Manufacturing_Site'
            );
            earlyManufacturingSiteOptions = this.getListAsResponse(options);
            observer.next(earlyManufacturingSiteOptions);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            // this.notificationService.error('Unable to get the Early Manufacturing Sites.');
          }
        );
    });
  }

  fetchRegulatoryTimelineForLocations(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.REGULATORY_TIMELINE_METHOS_NS,
          this.REGULATORY_TIMELINE_WS_METHOD_NAME,
          parameters
        )
        .subscribe(
          (response) => {
            //const regulatoryTimeLineLocationOptions = this.getPropertyListFromResponse(response, 'Regulatory_Timeline');
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            this.notificationService.error(
              'Unable to get the Regulatory TimeLine Locations.'
            );
          }
        );
    });
  }

  fetchPlatformsByBrand(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.PLATFORM_DETAILS_BY_BRAND_METHOD_NS,
          this.GET_PLATFORM_DETAILS_BY_BRAND_WS_METHOD_NAME,
          parameters
        )
        .subscribe(
          (response) => {
            let platformOptions = [];
            const options = this.getPropertyListFromResponse(
              response,
              'Platforms'
            );
            platformOptions = this.getListAsResponse(options);
            observer.next(platformOptions);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            this.notificationService.error('Unable to get all the platforms.');
          }
        );
    });
  }

  fetchVariantsByPlatform(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.VARIANT_DETAILS_BY_PLATFORM_METHOD_NS,
          this.GET_VARIANT_DETAILS_BY_PLATFORM_WS_METHOD_NAME,
          parameters
        )
        .subscribe(
          (response) => {
            let variantOptions = [];
            const options = this.getPropertyListFromResponse(
              response,
              'Variant_SKU'
            );
            variantOptions = this.getListAsResponse(options);
            observer.next(variantOptions);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            this.notificationService.error('Unable to get all the variants.');
          }
        );
    });
  }

  handleSecondaryPackChange(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.HandleSecondaryPackLeadTimeChanges_NS,
          this.HandleSecondaryPackLeadTimeChanges_WS_NAME,
          parameters
        )
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            this.notificationService.error(
              'Unable to handle Secondary Pack Change.'
            );
          }
        );
    });
  }

  updateGovernanceMilestone(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.UPDATE_GOVERNANCE_MILESTONE_METHOD_NS,
          this.UPDATE_GOVERNANCE_MILESTONE_METHOD_NAME,
          parameters
        )
        .subscribe(
          (res) => {
            observer.next(res);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            this.notificationService.error(
              'Something went Wrong while Updating Governance Milestone'
            );
          }
        );
    });
  }

  updateSeconaryPackaging(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.UPDATE_SECONDARY_PACK_METHOD_NS,
          this.UPDATE_SECONDARY_PACK_METHOD_NAME,
          parameters
        )
        .subscribe(
          (res) => {
            observer.next(res);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            this.notificationService.error(
              'Something went Wrong while Updating Governance Milestone'
            );
          }
        );
    });
  }

  getAllRolesForCurrentUser(): Observable<any> {
    const parameters = {
      depth: '0',
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.GET_ROLES_FOR_USER_NS,
          this.GET_ROLES_FOR_USER_WS_METHOD_NAME,
          parameters
        )
        .subscribe(
          (response) => {
            let isAdmin;
            if (sessionStorage.getItem('IS_ADMIN') == null) {
              const admin = response.tuple.old.user.role.find(
                (eachRole) =>
                  eachRole['@id'].substring(3, eachRole['@id'].indexOf(',')) ===
                  MONSTER_ROLES.NPD_ADMIN
              );
              isAdmin = admin != null ? true : false;
              sessionStorage.setItem('IS_ADMIN', isAdmin);
            }
            observer.next(response.tuple.old.user.role);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }
  getAllPlatforms(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_PLATFORMS) !==
        null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_PLATFORMS)
          )
        );
      } else {
        this.fetchAllPlatforms(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_PLATFORMS)
            )
          );
        });
      }
    });
  }
  fetchAllPlatforms(callBack) {
    this.appService
      .invokeRequest(
        this.PLATFORM_DETAILS_METHOD_NS,
        this.GET_PLATFORM_DETAILS_WS_METHOD_NAME,
        ''
      )
      .subscribe(
        (response) => {
          let brandOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Platforms'
          );
          const platformOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PLATFORMS,
            JSON.stringify(platformOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PLATFORMS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error('Unable to get all the platforms.');
        }
      );
  }
  getAllEarlyManufacturingSites(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_EARLY_MANUFACTURING_SITES
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_EARLY_MANUFACTURING_SITES
            )
          )
        );
      } else {
        this.fetchAllEarlyManufacturingSites(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_EARLY_MANUFACTURING_SITES
              )
            )
          );
        });
      }
    });
  }
  fetchAllEarlyManufacturingSites(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_EARLY_MANUFACTURING_SITES_NS,
        this.GET_ALL_EARLY_MANUFACTURING_SITES_WS,
        ''
      )
      .subscribe(
        (response) => {
          let earlyManufacturingSItesOption = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Early_Manufacturing_Site'
          );
          earlyManufacturingSItesOption = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_EARLY_MANUFACTURING_SITES,
            JSON.stringify(earlyManufacturingSItesOption)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_EARLY_MANUFACTURING_SITES,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the manufacturing sites.'
          );
        }
      );
  }
  getAllVariants(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(NpdSessionStorageConstants.ALL_VARIANTS) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(NpdSessionStorageConstants.ALL_VARIANTS)
          )
        );
      } else {
        this.fetchAllVariants(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(NpdSessionStorageConstants.ALL_VARIANTS)
            )
          );
        });
      }
    });
  }
  fetchAllVariants(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_ACTIVE_VARIANT_NS,
        this.GET_ALL_ACTIVE_VARIANT_WS,
        ''
      )
      .subscribe(
        (response) => {
          let varianOptions = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Variant_SKU'
          );
          varianOptions = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_VARIANTS,
            JSON.stringify(varianOptions)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_VARIANTS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error('Unable to get all the variants.');
        }
      );
  }
  getAllProjectSubtype(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_PROJECT_SUBTYPE
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_PROJECT_SUBTYPE
            )
          )
        );
      } else {
        this.fetchAllProjectSubtype(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_PROJECT_SUBTYPE
              )
            )
          );
        });
      }
    });
  }
  fetchAllProjectSubtype(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_ACTIVE_PROJECT_SUBTYPE_NS,
        this.GET_ALL_ACTIVE_PROJECT_SUBTYPE_WS,
        ''
      )
      .subscribe(
        (response) => {
          let projectSubtype = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Project_Sub_Type'
          );
          projectSubtype = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PROJECT_SUBTYPE,
            JSON.stringify(projectSubtype)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PROJECT_SUBTYPE,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the project subtypes.'
          );
        }
      );
  }
  getAllProjectDetail(): Observable<any> {
    return new Observable((observer) => {
      if (
        sessionStorage.getItem(
          NpdSessionStorageConstants.ALL_PROJECT_DETAILS
        ) !== null
      ) {
        observer.next(
          this.parseStorageValue(
            sessionStorage.getItem(
              NpdSessionStorageConstants.ALL_PROJECT_DETAILS
            )
          )
        );
      } else {
        this.fetchAllProjectDetail(() => {
          observer.next(
            this.parseStorageValue(
              sessionStorage.getItem(
                NpdSessionStorageConstants.ALL_PROJECT_DETAILS
              )
            )
          );
        });
      }
    });
  }
  fetchAllProjectDetail(callBack) {
    this.appService
      .invokeRequest(
        this.GET_ALL_ACTIVE_PROJECT_DETAILS_NS,
        this.GET_ALL_ACTIVE_PROJECT_DETAILS_WS,
        ''
      )
      .subscribe(
        (response) => {
          let projectDetails = [];
          const options = this.getPropertyListFromResponse(
            response,
            'Project_Detail'
          );
          projectDetails = this.getListAsResponse(options);
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PROJECT_DETAILS,
            JSON.stringify(projectDetails)
          );
          if (callBack) {
            callBack();
          }
        },
        (error) => {
          sessionStorage.setItem(
            NpdSessionStorageConstants.ALL_PROJECT_DETAILS,
            JSON.stringify([])
          );
          if (callBack) {
            callBack();
          }
          this.notificationService.error(
            'Unable to get all the project details.'
          );
        }
      );
  }
}
