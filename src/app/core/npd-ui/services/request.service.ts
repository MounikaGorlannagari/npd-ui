import { Injectable } from '@angular/core';
import { AppService, NotificationService, SharingService, UtilService } from 'mpm-library';
import { Observable, Subject } from 'rxjs';
import { InboxService } from '../request-management/requestor-dashboard/inbox/inbox.service';
import { BusinessConstants } from '../request-view/constants/BusinessConstants';
import { EntityListConstant } from '../request-view/constants/EntityListConstant';
import { HistoryConstant } from '../request-view/constants/HistoryConstants';
import { EntityService } from './entity.service';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  goBackSubject = new Subject();
  relationshipEntityPropsIdMapper = [];
  constructor(private appService:AppService,private sharingService:SharingService,private notificationService: NotificationService,
    private entityService: EntityService,private utilService: UtilService,private inboxService: InboxService) { }


  hasRole(roleName): boolean {
      const allRoles = this.utilService.getUserRoles();
      let hasRole = false;
      allRoles.forEach(roles => {
        hasRole = (!hasRole) ? (roles.Name === roleName) : hasRole;
      });
      return hasRole;
  }
    
    setBackNavigation(bool){
      this.goBackSubject.next(bool);
    }
  
  getRoleByName(roleName: string) {
    return this.sharingService.getAllRoles().find(role => role.ROLE_NAME === roleName);
  }


  getUsersByRole(): Observable<any> {
    return new Observable(observer => {
      const parameters = {
        role: this.getRoleByName(BusinessConstants.ROLE_E2EPM) && this.getRoleByName(BusinessConstants.ROLE_E2EPM).ROLE_DN,
        depth: '1',
        sort: 'ascending'
      };
      this.appService.getUsersForRole(parameters).subscribe(response => {
          const users = [];
          if (response.tuple && response.tuple.length > 0) {
            response.tuple.map(user => {
              users.push({
                name: user.old.entry.cn.string,
                value: user.old.entry.authenticationuser.string,
                displayName: user.old.entry.description.string,
                isActive: user.old.entry.enable?.string=='true'?'true':'false'
              });
            });
          

          }
          observer.next(users);
          observer.complete();
        }, error => {
          observer.error(error);
        });
    });
  }

  getUsersByRoleForCorpPM(): Observable<any> {
    return new Observable(observer => {
      const parameters = {
        role: this.getRoleByName(BusinessConstants.ROLE_CORPPM) && this.getRoleByName(BusinessConstants.ROLE_CORPPM).ROLE_DN,
        depth: '1',
        sort: 'ascending'
      };
      this.appService.getUsersForRole(parameters).subscribe(response => {
          const users = [];
          if (response.tuple && response.tuple.length > 0) {
            response.tuple.map(user => {
              users.push({
                name: user.old.entry.cn.string,
                value: user.old.entry.authenticationuser.string,
                displayName: user.old.entry.description.string,
                isActive: user.old.entry.enable?.string === 'true'?'true':'false'
              });
            });

          }
          observer.next(users);
          observer.complete();
        }, error => {
          observer.error(error);
        });
    });
  }

  getUsersByRoleForOpsPM(): Observable<any> {
    return new Observable(observer => {
      const parameters = {
        role: this.getRoleByName(BusinessConstants.ROLE_OPSPM) && this.getRoleByName(BusinessConstants.ROLE_OPSPM).ROLE_DN,
        depth: '1',
        sort: 'ascending'
      };
      this.appService.getUsersForRole(parameters).subscribe(response => {
          const users = [];
          if (response.tuple && response.tuple.length > 0) {
            response.tuple.map(user => {
              users.push({
                name: user.old.entry.cn.string,
                value: user.old.entry.authenticationuser.string,
                displayName: user.old.entry.description.string,
                isActive: user.old.entry.enable?.string === 'true'?'true':'false'
              });
            });

          }
          observer.next(users);
          observer.complete();
        }, error => {
          observer.error(error);
        });
    });
  }

  getUserByID(userId): Observable<any> {
    const filters = [];
    filters.push({
      'name': EntityListConstant.MPM_ENTITY_USER_ID,
      'operator': 'eq',
      'value': userId
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.MPM_ENTITY_USER, filters, '', this.utilService.APP_ID);
    return new Observable(observer => {
      this.entityService.getEntityObjects(EntityListConstant.MPM_ENTITY_USER, filterParam, null, null, null, null, this.utilService.APP_ID)
        .subscribe(userResponse => {
          observer.next(userResponse);
          observer.complete();
        }, error => {
          observer.error(new Error(error));
        });
    });
  }

  triggerActionFlow(action,requestObject,taskId) : Observable<any> {

    // createNotification = {
    //   TITLE : ''
    // }
    const taskFormData = {
      formoutputdata: {
        freeformcontrols: {
          Request_ID: requestObject.requestId,
          Project_Name:requestObject.projectName,
          select_action: action,
          button1: 'Submit',//''
          button2: 'Cancel'//''
        }
      }
    };
    const taskDetails = [];
    const taskObject = {
      taskId: taskId,
      taskData: {
        Data: taskFormData
      }
    };
    taskDetails.push(taskObject);
    const reqObj = {
      taskOperation: {
        applicationId: this.utilService.APP_ID,
        operation: 'COMPLETE',
        tasks: {
          task: [{
            taskId: taskDetails[0].taskId,
            taskData: taskDetails[0].taskData.Data
          }]
        }
      }
    };
    return new Observable(observer => {
      this.inboxService.taskOperation(reqObj).subscribe((response) => {
          observer.next(response);
          // this.appService.invokeRequest('http://schemas/AcheronMPMCore/Notification/operations','CreateNotification',).subscribe{

          // }
          observer.complete();
        }, (error) => {
          observer.error(new Error(error));
        });
    });
  }


  formatHistoryLog(historyLogs, entityName, entityObject, relationshipEntityPropsIdMapper) {
    if (historyLogs.length === 0) {
      return historyLogs;
    }
    let resultHistoryLogs = [];
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < historyLogs.length; i++) {

      if (historyLogs[i].EventType === HistoryConstant.HISTORY_EVENTS.propertyChange) {
        if (entityName === EntityListConstant.REQUEST && entityObject.REQUEST_ID$Properties && entityObject.Tracking.LastModifiedDate &&
          entityObject.Tracking.LastModifiedDate != null &&
          (new Date(historyLogs[i].When) >= new Date(entityObject.Tracking.LastModifiedDate))) {

          if (HistoryConstant.REQUEST_CONSTANTS.indexOf(historyLogs[i].Options.n) !== -1) {

            // tslint:disable-next-line: no-shadowed-variable
            /*const descArray = historyLogs[i].Description.split('\'');
            descArray[1] = this.formatPropertyString(historyLogs[i].Options.n);
            historyLogs[i].Description = descArray.join(' ');*/
            historyLogs[i].Description = historyLogs[i].Description.replace('Property', 'The');
            if (this.utilService.isValid(historyLogs[i].Options.o)
              && this.utilService.isValid(historyLogs[i].Options.n)) {
              if (historyLogs[i].Options.o !== historyLogs[i].Options.v) {
                resultHistoryLogs.push(historyLogs[i]);
              }
            } else if (historyLogs[i].Options.n === 'CAMPAIGN_ITEM_ID' || historyLogs[i].Options.n === 'PROJECT_ITEM_ID') {
              historyLogs[i].Description = historyLogs[i].Description.replace(historyLogs[i].Options.v,
                relationshipEntityPropsIdMapper[historyLogs[i].Options.v]);
              if (historyLogs[i].Options.o) {
                historyLogs[i].Description = historyLogs[i].Description.replace(historyLogs[i].Options.o,
                  relationshipEntityPropsIdMapper[historyLogs[i].Options.o]);
              }
              historyLogs[i].Description = historyLogs[i].Description.replace(' Item ID', '');
              resultHistoryLogs.push(historyLogs[i]);
            } else {
              resultHistoryLogs.push(historyLogs[i]);
            }
          }

        } 
      } else if (historyLogs[i].EventType === HistoryConstant.HISTORY_EVENTS.relationshipChange) {
        if (entityName === EntityListConstant.REQUEST && entityObject.REQUEST_ID$Properties && entityObject.Tracking.LastModifiedDate &&
          entityObject.Tracking.LastModifiedDate != null &&
          (new Date(historyLogs[i].When) >= new Date(entityObject.Tracking.LastModifiedDate))) {
          // tslint:disable-next-line: no-shadowed-variable
          const relationship = this.getRelationshipEntityName(historyLogs[i]);
          // tslint:disable-next-line: no-shadowed-variable
          const toRtoReplaceString = '\'' + relationship + '\'';

          if (relationship !== '' && HistoryConstant.REQUEST_CONSTANTS.indexOf(relationship) !== -1) {
            historyLogs[i].Description = historyLogs[i].Description.replace(historyLogs[i].ri.split('.')[1],
              relationshipEntityPropsIdMapper[historyLogs[i].ri]);
            if (historyLogs[i].rio) {
              historyLogs[i].Description = historyLogs[i].Description.replace(historyLogs[i].rio.split('.')[1],
                relationshipEntityPropsIdMapper[historyLogs[i].rio]);
            }
            historyLogs[i].Description = historyLogs[i].Description.replace('relationship ', '');
            historyLogs[i].Description = historyLogs[i].Description.replace(toRtoReplaceString, '');
            historyLogs[i].Description = historyLogs[i].Description.replace(toRtoReplaceString, '');
            historyLogs[i].Description = historyLogs[i].Description.replace(' ID', '');
            resultHistoryLogs.push(historyLogs[i]);
          }

        } 
      }
    }

    // callback function to sort api:to sort array in desc order based on date
    const calcDate: any = (a, b) => {
      return new Date(b.When).valueOf() - new Date(a.When).valueOf();
    };
    resultHistoryLogs = resultHistoryLogs.sort(calcDate);

    return resultHistoryLogs;
  }

  getRelationshipEntityName(historyLog) {
    let relationship = '';
    if (historyLog.Description.indexOf('Request Priority ID') > -1) {
      relationship = 'Request Priority';
    } else if (historyLog.Description.indexOf('Language ID') > -1) {
      relationship = 'Language';
    } else if (historyLog.Description.indexOf('Vendor ID') > -1) {
      relationship = 'Vendor';
    } else if (historyLog.Description.indexOf('Licensed Partner ID') > -1) {
      relationship = 'Licensed Partner';
    }
    return relationship;
  }


  fetchRequestDetails(requestId) :Observable<any> {
    return new Observable(observer => {
      const filters = [];
    filters.push({
      'name': EntityListConstant.REQUEST_ENTITY_PARAMS.ID,
      'operator': 'eq',
      'value': requestId
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.REQUEST, filters, '', this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.REQUEST, filterParam, null, null, null, null, this.utilService.APP_ID)
      .subscribe((requestResponse) => {
        let techReq = [];
        if (requestResponse?.result?.items?.length === 1) {
          techReq = requestResponse.result.items[0];
        } /*  else {
          this.notificationService.error('No Request found.')
        } */
        observer.next(techReq);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
    
  }


/*   triggerActionFlow2(action?: string,requestObject?,taskId?) : Observable<any> {
    const taskFormData = {
      formoutputdata: {
        '@xmlns': 'http://schemas.cordys.com/1.0/xforms/processapi',
        freeformcontrols: {
          Request_ID : requestObject.requestId,
          Project_Name : requestObject.Project_Name ,
       
          button1  : 'Submit',
          button2: 'Cancel',
        }
      }
    };
    if (action) {
     // taskFormData.formoutputdata.freeformcontrols.select_action.__text = action;
    }
    const taskDetails = [];
    const taskObject = {
      taskId: taskId,
      taskData: {
        Data: taskFormData
      }
    };
    taskDetails.push(taskObject);
    const reqObj = {
      taskOperation: {
        applicationId: this.utilService.APP_ID,
        Action: 'COMPLETE',
        tasks: {
          task: [{
            taskId: taskDetails[0].taskId,
            taskData: taskDetails[0].taskData.Data
          }]
        }
      }
    };
    return new Observable(observer => {
      //PerformTaskOperation
      this.inboxService.taskOperation(reqObj).subscribe((response) => {
          observer.next(response);
          observer.complete();
        }, (error) => {
          observer.error(new Error(error));
        });
    });
  }

  
  triggerActionFlow(action,requestObject,taskId) : Observable<any> {
    const taskFormData = {
      formoutputdata: {
        '@xmlns': 'http://schemas.cordys.com/1.0/xforms/processapi',

        freeformcontrols: {
          op_Request_ID: {//Request_ID//op_requestId
            '@xmlns:ns1': 'http://schemas.cordys.com/1.0/xforms/processapi',
            'ns1:display_name': 'Request ID',
            __text:requestObject.requestId
          },
          op_Project_Name: {//Project_Name 
            '@xmlns:ns2': 'http://schemas.cordys.com/1.0/xforms/processapi',
            '@ns2:display_name': 'Project Name ',
            __text: requestObject.ProjectName 
          },
          select_action: {
            '@xmlns:ns3': 'http://schemas.cordys.com/1.0/xforms/processapi',
            '@ns3:display_name': 'Action',
            __text: action
          },
          cancel_btn: {
            '@xmlns:ns4': 'http://schemas.cordys.com/1.0/xforms/processapi',
            '@ns4:display_name': 'Cancel',
            __text: 'Cancel'
          },
          submit_btn: {
            '@xmlns:ns5': 'http://schemas.cordys.com/1.0/xforms/processapi',
            '@ns5:display_name': 'Submit',
            __text: 'Submit'
          }
        }
      }
    };
   /*  if (action) {
      taskFormData.formoutputdata.freeformcontrols.select_action.__text = action;
    } 
    const taskDetails = [];
    const taskObject = {
      taskId: taskId,
      taskData: {
        Data: taskFormData
      }
    };
    taskDetails.push(taskObject);
    const reqObj = {
      taskOperation: {
        applicationId: this.utilService.APP_ID,
        operation: 'COMPLETE',
        tasks: {
          task: [{
            taskId: taskDetails[0].taskId,
            taskData: taskDetails[0].taskData.Data
          }]
        }
      }
    };
    return new Observable(observer => {
      this.inboxService.taskOperation(reqObj).subscribe((response) => {
          observer.next(response);
          observer.complete();
        }, (error) => {
          observer.error(new Error(error));
        });
    });
  }


  triggerActionFlow3(action,requestObject,taskId) : Observable<any> {
    const taskFormData = {
      formoutputdata: {
        '@xmlns': 'http://schemas.cordys.com/1.0/xforms/processapi',

        freeformcontrols: {
           Request_ID: {//Request_ID//op_requestId
            '@xmlns:ns1': 'http://schemas.cordys.com/1.0/xforms/processapi',
            'ns1:display_name': 'Request ID',
            __text:requestObject.requestId
          },
          Project_Name: {//Project_Name 
            '@xmlns:ns2': 'http://schemas.cordys.com/1.0/xforms/processapi',
            '@ns2:display_name': 'Project Name ',
            __text: requestObject.ProjectName 
          },
          select_action: {
            '@xmlns:ns3': 'http://schemas.cordys.com/1.0/xforms/processapi',
            '@ns3:display_name': 'Action',
            __text: action
          },
          button1: {
            '@xmlns:ns4': 'http://schemas.cordys.com/1.0/xforms/processapi',
            '@ns4:display_name': 'Submit',
            __text: 'Submit'
          },
          button2: {
            '@xmlns:ns5': 'http://schemas.cordys.com/1.0/xforms/processapi',
            '@ns5:display_name': 'Cancel',
            __text: 'Cancel'
          },
        }
      }
    };
   /*  if (action) {
      taskFormData.formoutputdata.freeformcontrols.select_action.__text = action;
    }
    const taskDetails = [];
    const taskObject = {
      taskId: taskId,
      taskData: {
        Data: taskFormData
      }
    };
    taskDetails.push(taskObject);
    const reqObj = {
      taskOperation: {
        applicationId: this.utilService.APP_ID,
        operation: 'COMPLETE',
        tasks: {
          task: [{
            taskId: taskDetails[0].taskId,
            taskData: taskDetails[0].taskData.Data
          }]
        }
      }
    };
    return new Observable(observer => {
      this.inboxService.taskOperation(reqObj).subscribe((response) => {
          observer.next(response);
          observer.complete();
        }, (error) => {
          observer.error(new Error(error));
        });
    });
  } */

   /*
  * To form data from relationship entites to show History Log.
  */
   formRelationshipEntityPropsIdMapper(relationshipEntity, type?) {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < relationshipEntity.length; i++) {
      let ItemId;
      let displayName;
      if (relationshipEntity[i]['REQUEST_PRIORITY-id'] && relationshipEntity[i]['REQUEST_PRIORITY-id'].ItemId
        && relationshipEntity[i].DISPLAY_NAME) {
        ItemId = relationshipEntity[i]['REQUEST_PRIORITY-id'].ItemId;
        displayName = relationshipEntity[i].DISPLAY_NAME;
      } else if (relationshipEntity[i]['LONG_FORM_STATUS-id'] && relationshipEntity[i]['LONG_FORM_STATUS-id'].ItemId
        && relationshipEntity[i].DISPLAY_NAME) {
        ItemId = relationshipEntity[i]['LONG_FORM_STATUS-id'].ItemId;
        displayName = relationshipEntity[i].DISPLAY_NAME;
      }
      else if (relationshipEntity[i]['LANGUAGE-id'] && relationshipEntity[i]['LANGUAGE-id'].ItemId
        && relationshipEntity[i].DISPLAY_NAME) {
        ItemId = relationshipEntity[i]['LANGUAGE-id'].ItemId;
        displayName = relationshipEntity[i].DISPLAY_NAME;
      } else if (type === 'CAMPAIGN') {
        ItemId = relationshipEntity[i].ITEM_ID;
        displayName = relationshipEntity[i].CAMPAIGN_NAME;
      } else if (type === 'PROJECT') {
        ItemId = relationshipEntity[i].ITEM_ID;
        displayName = relationshipEntity[i].PROJECT_NAME;
      }
      if (ItemId && this.relationshipEntityPropsIdMapper[ItemId] === undefined) {
        this.relationshipEntityPropsIdMapper[ItemId] = displayName;
      }
    }
  }
}
