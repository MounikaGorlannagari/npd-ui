import { Injectable, isDevMode } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'mpm-library';

@Injectable({
  providedIn: 'root'
})
export class DefaultColumnPreferenceService {

  constructor(private appService: AppService) { }

  userCN ;
  //= "SystemPreference";
  userId;
  systemDefaultPreference = [];

  integrationConfig;
  //envConfig = integrationConfig;
  envConfig = null;

  public MPM_USER_PREFERENCE_NS = 'http://schemas/AcheronMPMCore/MPM_User_Preference/operations';
  public GET_MPM_USER_PREFERENCE_BY_USER_WS = 'GetMPMUserPreferenceByUser';

  getsystemDefaultPreference() {
    return this.systemDefaultPreference;
  }

  setsystemDefaultPreference(preference) {
    this.systemDefaultPreference = preference;
  }

  getSystemPreference(): Observable<any> {
    const parameter = {
      UserId: this.userId
    };
    return new Observable(observer => {
        this.appService.invokeRequest(this.MPM_USER_PREFERENCE_NS, this.GET_MPM_USER_PREFERENCE_BY_USER_WS, parameter).subscribe(response => {
                const userPreferenceResponse = response.MPM_User_Preference ? response.MPM_User_Preference : [];
                let defaultPreference = [];
                if(Array.isArray(userPreferenceResponse)) {
                  defaultPreference = userPreferenceResponse;
                }
                else {
                  defaultPreference.push(userPreferenceResponse);
                }
                this.setsystemDefaultPreference(defaultPreference);
                observer.next(userPreferenceResponse);
                observer.complete();
            },(error) =>{
              observer.error(error);
            });
    });
  }

  getSystemUserId(): Observable<any> {
    this.userCN = isDevMode()?this.envConfig.user_preference_cn: this.integrationConfig.user_preference_cn;
    let parameters = {
      userCN : this.userCN
    }
    return new Observable(observer => {
      this.appService.getPersonDetailsByUserCN(parameters)
        .subscribe(userDetailsResponse => {
          this.userId =  userDetailsResponse.Person.PersonToUser['Identity-id'].Id;
          observer.next(userDetailsResponse);
          observer.complete();
          //this.sharingService.setCurrentUserID(currentUser.UserDN.split('=')[1].split(',')[0]);//split('=')[1].split(',')[0]
      },(error) => {
        observer.error(error);
      });
    });
  }

  getDefaultPreference() {
    this.getSystemUserId().subscribe(userResponse => {
      this.getSystemPreference().subscribe(systemPreferenceResponse => {
      });
    });
  }

  getDefaultPreferenceByViewId(viewId) {
   return this.systemDefaultPreference.find(defaultPreference=>
      defaultPreference.R_PO_MPM_VIEW_CONFIG && defaultPreference.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'] && defaultPreference.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id 
      && defaultPreference.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id == viewId
    )
  }
}
