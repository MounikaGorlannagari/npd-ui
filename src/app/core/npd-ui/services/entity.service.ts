import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { AppService, GloabalConfig } from 'mpm-library';
import { EntityHelperService } from './entity-helper.service';

@Injectable({
  providedIn: 'root'
})
export class EntityService {

  Entities;
  cordysPlatformVersion;

  TARGET_USER = 'user';
  TARGET_ROLE = 'role';
  TARGET_TEAM = 'team';
  TARGET_WORKLIST = 'worklist';

  defaultSkip = 0;
  defaultTop = 200;

  constructor(
      private http: HttpClient,
      private entityHelperService: EntityHelperService,
      private appService: AppService
  ) { }

  getServerURL(): string {
      return GloabalConfig.webServiceUrl + '/';
  }

  formRestUrl(apiurl, urlparam): string {
        return this.getServerURL() + apiurl + (urlparam != null ? this.formRestUrlParams(urlparam) : '');
  }

  fireGetRestRequest(url, parameters): Observable<any> {
      return new Observable(observer => {
          this.http.get(this.formRestUrl(url, parameters)).subscribe((response) => {
              observer.next(response);
              observer.complete();
          }, (error) => {
              this.appService.checkPSSession(() => {
                  observer.error(new Error(error));
              });
          });
      });
  }

  firePostRestRequest(url, parameters, contentType, headerObject): Observable<any> {
    
      let postReq: Observable<any>;
      const httpHeaderOptions = {
          withCredentials: true,
          headers: new HttpHeaders({
              'Content-Type': 'application/json'
          })
      };
      if (contentType && contentType != null) {
          httpHeaderOptions.headers['Content-Type'] = contentType;
      }
      if (headerObject && headerObject && headerObject != null) {
          postReq = this.http.post(this.formRestUrl(url, null), parameters, httpHeaderOptions);
      } else {
          postReq = this.http.post(this.formRestUrl(url, null), parameters, httpHeaderOptions);
      }
      return new Observable(observer => {
          postReq.subscribe((response) => {
              observer.next(response);
              observer.complete();
          }, (error) => {
              this.appService.checkPSSession(() => {
                  observer.error(new Error(error));
              });
          });
      });
  }

  firePutRestRequest(url, parameters, contentType, headerObject: HttpHeaders): Observable<any> {
      let postReq: Observable<any>;
      const httpHeaderOptions = {
          withCredentials: true,
          headers: new HttpHeaders({
              'Content-Type': 'application/json'
          })
      };
      if (contentType && contentType != null) {
          httpHeaderOptions.headers['Content-Type'] = contentType;
      }
      if (headerObject && headerObject && headerObject != null) {
          httpHeaderOptions.headers = headerObject;
          postReq = this.http.put(this.formRestUrl(url, null), parameters, httpHeaderOptions);
      } else {
          postReq = this.http.put(this.formRestUrl(url, null), parameters);
      }
      return new Observable(observer => {
          postReq.subscribe((response) => {
              observer.next(response);
              observer.complete();
          }, (error) => {
              this.appService.checkPSSession(() => {
                  observer.error(error);
              });
          });
      });
  }

  fireDeleteRestRequest(url, parameters, contentType) {
      let deleteReq;
      const httpHeaderOptions = {
          withCredentials: true,
          headers: new HttpHeaders({
              'Content-Type': 'application/json'
          }),
          params: parameters
      };
      if (contentType && contentType != null) {
          httpHeaderOptions.headers['Content-Type'] = contentType;
      }
      deleteReq = this.http.delete(this.formRestUrl(url, null), httpHeaderOptions);
      return new Observable(observer => {
          deleteReq.subscribe((response) => {
              observer.next(response);
              observer.complete();
          }, (error) => {
              this.appService.checkPSSession(() => {
                  observer.error(new Error(error));
              });
          });
      });
  }

  fireGetSessionWithOTDSTicket(url, otdsTicket, parameters) {
      const httpHeaderOptions = {
          withCredentials: true,
          headers: new HttpHeaders({
              'OTDSTicket': otdsTicket
          })
      };
      return new Observable(observer => {
          this.http.get(this.formRestUrl(url, parameters)).subscribe((response) => {
              observer.next(response);
              observer.complete();
          }, (error) => {
              this.appService.checkPSSession(() => {
                  observer.error(new Error(error));
              });
          });
      });
  }

  formRestParams(parameters) {
      let param = '?';
      for (let i = 0; i < parameters.length; i++) {
          if (parameters[i]['key'] === 'json') {
              param += i === 0 ? parameters[i].value.key + '=' + encodeURIComponent(JSON.stringify(parameters[i].value.value)) :
                  '&' + parameters[i].value.key + '=' + encodeURIComponent(JSON.stringify(parameters[i].value.value));
          } else {
              param += i === 0 ? parameters[i].key + '=' + parameters[i].value : '&' + parameters[i].key + '=' + parameters[i].value;
          }
      }
      return param;
  }

  formRestUrlParams(params) {
      if (params != null && Array.isArray(params) && params.length > 0) {
          let parameter = '';
          for (let i = 0; i < params.length; i++) {
              parameter += '/' + params[i];
          }
          return parameter;
      } else {
          return '';
      }
  }

  getPostRequestObject(url, parameter, headers: HttpHeaders): Observable<any> {
      return this.firePostRestRequest(url, parameter, 'application/json', headers);
  }

  getGetRequestObject(url, parameter): Observable<any> {
      return this.fireGetRestRequest(url, parameter);
  }

  getPutRequestObject(url, parameter, headers: HttpHeaders): Observable<any> {
      return this.firePutRestRequest(url, parameter, 'application/json', headers);
  }

  getDeleteRequestObject(url, parameter, headers): Observable<any> {
      return this.fireDeleteRestRequest(url, parameter, 'application/json');
  }

  getEntity(entity_key, appId) {
      this.Entities = this.entityHelperService.getEntityInfo(appId);
      return this.Entities.filter(e => {
          return e['listId'] === entity_key;
      })[0];
  }

  getParameter(selectedEntity, parametername): any {
      selectedEntity.entityListParameters = selectedEntity.parameters;
      if (Array.isArray(selectedEntity.entityListParameters)) {
          return selectedEntity.entityListParameters.filter(e => e['listParameterId'] === parametername)[0];
      } else if (selectedEntity.entityListParameters['listParameterId'] === parametername) {
          return selectedEntity.entityListParameters;
      }
  }
  getEntityObjects(entity_key, filterParameters, orderBy, selectedEntity, skip, top, appId): Observable<any> {
      if (!selectedEntity) { selectedEntity = this.getEntity(entity_key, appId); }
      if (!selectedEntity) {
          console.warn('Entity is not available for ' + entity_key);
          return;
      }
      if (!skip) {
          skip = this.defaultSkip;
      }
      if (!top) {
          top = this.defaultTop;
      }
      const parameter: any = {
          'viewId': selectedEntity.viewId,
          'versionId': selectedEntity.versionId,
          'dataSourceId': selectedEntity.dataSourceId,
          'dataSourceIds': [selectedEntity.dataSourceId],
          'parameters': {

          },
          'resultOption': 'Items',
          'skip': skip,
          'top': top
      };
      if (filterParameters) {
          parameter.parameters = filterParameters;
      }
      if (orderBy) {
          parameter.orderBy = orderBy;
      }

      const url = 'app/entityRestService/Elements(' + parameter.versionId + '.' + selectedEntity.viewId +
          ')/ResultItems?language=en-US&' + this.getCookie();
      return this.getPostRequestObject(url, parameter,null);
  }

  getEntityObjectsWithCount(entity_key, filterParameters, orderBy, selectedEntity, skip, top, appId): Observable<any> {
    if (!selectedEntity) { selectedEntity = this.getEntity(entity_key, appId); }
    if (!selectedEntity) {
        console.warn('Entity is not available for ' + entity_key);
        return;
    }
    if (!skip) {
        skip = this.defaultSkip;
    }
    if (!top) {
        top = this.defaultTop;
    }
    const parameter: any = {
        'viewId': selectedEntity.viewId,
        'versionId': selectedEntity.versionId,
        'dataSourceId': selectedEntity.dataSourceId,
        'dataSourceIds': [selectedEntity.dataSourceId],
        'parameters': {

        },
        'resultOption': "ItemsAndTotalCount",
        'skip': skip,
        'top': top
    };
    if (filterParameters) {
        parameter.parameters = filterParameters;
    }
    if (orderBy) {
        parameter.orderBy = orderBy;
    }

    const url = 'app/entityRestService/Elements(' + parameter.versionId + '.' + selectedEntity.viewId +
        ')/ResultItems?include=PropDescs,Rules,Usage&language=en-US&' + this.getCookie();

    return this.getPostRequestObject(url, parameter, null);
}

  
  perpareFilterParameters(entity_key, filters, selectedEntity, appId) {
      if (!filters) { return null; }
      if (!selectedEntity) { selectedEntity = this.getEntity(entity_key, appId); }
      const filterParameters = {};
      $.each(filters, (index, filter) => {
          const selectedParameter = this.getParameter(selectedEntity, filter.name);
          if (filter.operator === 'Between') {
              filterParameters[selectedParameter.parameterName] = {
                  'parameterId': selectedParameter.parameterId,
                  'name': selectedParameter.parameterName,
                  'propertyId': selectedParameter.propertyId,
                  'comparison': {
                      'operator': filter.operator,
                      'from': filter.from,
                      'to': filter.to
                  }
              };
          } else {
              filterParameters[selectedParameter.parameterName] = {
                  'parameterId': selectedParameter.parameterId,
                  'name': selectedParameter.parameterName,
                  'propertyId': selectedParameter.propertyId,
                  'comparison': {
                      'operator': filter.operator,
                      'value': filter.value,
                      'values': filter.values
                  }
              };
          }
      });
      return filterParameters;
  }


 
  perpareFilterParametersone(entity_key, filters, selectedEntity, appId) {
      if (!filters) { return null; }
      if (!selectedEntity) { selectedEntity = this.getEntity(entity_key, appId); }
      const filterParameters = {};
      $.each(filters, (index, filter) => {
          const selectedParameter = this.getParameter(selectedEntity, filter.name);
          filterParameters[selectedParameter.parameterName] = {
              'parameterId': selectedParameter.parameterId,
              'name': selectedParameter.parameterName,
              'propertyId': selectedParameter.propertyId,
              'comparison': {
                  'operator': filter.operator,
                  'value': filter.value,
                  'values': filter.values,
                  'to': filter.to,
                  'from': filter.from,
              }
          };
      });
      return filterParameters;
  }

  updateEntityObjects(itemId, updateData) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId || !updateData) {
          return;
      }
      const parameter = [{
          'itemId': itemId,
          'item': updateData,
          'operationType': 'Update',
          'containerVersionId': this.Entities.versionId
      }];
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  updateBulkEntityRelationObjects(isRelation: boolean, itemId: string | number, relationObjects: any[],
      isUpdateData: boolean, updateData: any) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      const parameter = [];
      if (isRelation) {
          for (let i = 0; i < relationObjects.length; i++) {
              let paramObj = {};
              if (relationObjects[i].itemId && relationObjects[i].itemId != null && relationObjects[i].itemId !== '') {
                  paramObj['itemId'] = relationObjects[i].itemId;
              } else {
                  paramObj['itemId'] = itemId;
              }
              paramObj['relationName'] = relationObjects[i].relationName;
              if (relationObjects[i].operationType) {
                  paramObj['operationType'] = relationObjects[i].operationType;
              } else {
                  paramObj['operationType'] = 'Update';
              }
              paramObj['targetItemId'] = relationObjects[i].targetItemId;
              parameter.push(paramObj);
              paramObj = {};
          }
      }
      if (isUpdateData) {
          let paramObj = {};
          paramObj['itemId'] = itemId;
          paramObj['item'] = updateData;
          paramObj['operationType'] = 'Update';
          paramObj['containerVersionId'] = this.Entities.versionId;
          parameter.push(paramObj);
          paramObj = {};
      }
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  updateEntityRelationObjects(itemId, relationName, targetItemId) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId || !targetItemId || !relationName) {
          return;
      }
      const parameter = [{
          'itemId': itemId,
          'relationName': relationName,
          'operationType': 'Update',
          'targetItemId': targetItemId
      }];
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  // delete entity relation objects
  deleteEntityRelationObjects(itemId, relationName, targetItemId) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId || !relationName) {
          return;
      }
      const parameter = [{
          'itemId': itemId,
          'relationName': relationName,
          'operationType': 'delete',
          'targetItemId': targetItemId
      }];
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  updateMultipleEntityObjects(itemIds, updateData) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemIds || !updateData || !Array.isArray(itemIds)) {
          return;
      }
      const parameter = [];
      for (let i = 0; i < itemIds.length; i++) {
          parameter.push({
              'itemId': itemIds[i],
              'item': updateData,
              'operationType': 'Update',
              'containerVersionId': this.Entities.versionId
          });
      }
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  updateMultipleEntityObjectsProperty(objects) {
    if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
    if (!objects || !Array.isArray(objects)) {
        return;
    }
    const parameter = [];
    for (let i = 0; i < objects.length; i++) {
        parameter.push({
            'itemId': objects[i].ItemId,
            'item': objects[i].updateData,
            'operationType': 'Update',
            'containerVersionId': this.Entities.versionId
        });
    }
    const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
    return this.getPutRequestObject(url, parameter, null);
}

  updateEntityObjectsAndVerifyConflicts(itemId, updateData, oldData, entity_key?, appId?) {
      if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      let versionId = this.Entities.versionId;
      if (this.Entities && !this.Entities.versionId && entity_key && appId) {
          const selectedEntity = this.getEntity(entity_key, appId);
          if (!selectedEntity) {
              console.warn('Entity is not available for ' + entity_key);
              return;
          }
          versionId = selectedEntity.versionId;
      }
      if (!itemId || !updateData) {
          return;
      }
      const parameter = [{
          'itemId': itemId,
          'item': updateData,
          'operationType': 'Update',
          'originalItem': oldData,
          'containerVersionId': this.Entities.versionId
      }];
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  updateEntityObjectsWithProperties(itemId, updateData, conflictData) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId || !updateData) {
          return;
      }
      const parameter: any = [{
          'itemId': itemId,
          'item': updateData,
          'operationType': 'Update',
          'containerVersionId': this.Entities.versionId
      }];
      if (conflictData && conflictData.length && conflictData.length > 0) {
          for (let i = 0; i < conflictData.length; i++) {
              parameter.push({
                  'itemId': conflictData[i].itemId,
                  'item': conflictData[i].item,
                  'operationType': conflictData[i].operationType,
                  'originalItem': conflictData[i].originalItem
              });
          }
      }
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  updateEntityRelationObjectsWithProperties(itemId, relationName, targetItemId, conflictData) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId || !targetItemId || !relationName) {
          return;
      }
      const parameter: any = [{
          'itemId': itemId,
          'relationName': relationName,
          'operationType': 'Update',
          'targetItemId': targetItemId
      }];
      if (conflictData && conflictData.length && conflictData.length > 0) {
          for (let i = 0; i < conflictData.length; i++) {
              const obj = {
                  'itemId': conflictData[i].itemId,
                  'item': conflictData[i].item,
                  'operationType': conflictData[i].operationType,
                  'originalItem': conflictData[i].originalItem
              };
              parameter.push(obj);
          }
      }
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  deleteEntityRelationObjectsWithProperties(itemId, relationName, targetItemId, conflictData) {
     // if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId || !relationName) {
          return;
      }
      const parameter: any = [{
          'itemId': itemId,
          'relationName': relationName,
          'operationType': 'delete',
          'targetItemId': targetItemId
      }];
      if (conflictData && conflictData.length && conflictData.length > 0) {
          for (let i = 0; i < conflictData.length; i++) {
              parameter.push({
                  'itemId': conflictData[i].itemId,
                  'item': conflictData[i].item,
                  'operationType': conflictData[i].operationType,
                  'originalItem': conflictData[i].originalItem
              });
          }
      }
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  updateMultipleEntityObjectsPropertyWithProperties(objects, conflictData, entity_key?, appId?) {
      if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      let versionId = this.Entities.versionId;
      if (this.Entities && !this.Entities.versionId && entity_key && appId) {
          const selectedEntity = this.getEntity(entity_key, appId);
          if (!selectedEntity) {
              console.warn('Entity is not available for ' + entity_key);
              return;
          }
          versionId = selectedEntity.versionId;
      }
      if (!objects || !Array.isArray(objects) || !this.Entities) {
          return;
      }
      const parameter = [];
      if (conflictData && conflictData.length && conflictData.length > 0) {
          for (let i = 0; i < conflictData.length; i++) {
              parameter.push({
                  'itemId': conflictData[i].itemId,
                  'item': conflictData[i].item,
                  'operationType': conflictData[i].operationType,
                  'originalItem': conflictData[i].originalItem
              });
          }
      }
      for (let i = 0; i < objects.length; i++) {
          parameter.push({
              'itemId': objects[i].ItemId,
              'item': objects[i].updateData,
              'operationType': 'Update',
              'containerVersionId': versionId
          });
      }
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  updateBulkEntityRelationObjectsWithProperties(isRelation, itemId, relationObjects, isUpdateData, updateData, conflictData) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId) {
          return;
      }
      const parameter = [];
      if (isRelation) {
          for (let i = 0; i < relationObjects.length; i++) {
              let paramObj = {};
              if (relationObjects[i].itemId && relationObjects[i].itemId != null && relationObjects[i].itemId !== '') {
                  paramObj['itemId'] = relationObjects[i].itemId;
              } else {
                  paramObj['itemId'] = itemId;
              }
              paramObj['relationName'] = relationObjects[i].relationName;
              if (relationObjects[i].operationType) {
                  paramObj['operationType'] = relationObjects[i].operationType;
              } else {
                  paramObj['operationType'] = 'Update';
              }
              paramObj['targetItemId'] = relationObjects[i].targetItemId;
              parameter.push(paramObj);
              paramObj = {};
          }
      }
      if (isUpdateData) {
          let paramObj = {};
          paramObj['itemId'] = itemId;
          paramObj['item'] = updateData;
          paramObj['operationType'] = 'Update';
          paramObj['containerVersionId'] = this.Entities.versionId;
          parameter.push(paramObj);
          paramObj = {};
      }
      if (conflictData && conflictData.length && conflictData.length > 0) {
          for (let i = 0; i < conflictData.length; i++) {
              parameter.push({
                  'itemId': conflictData[i].itemId,
                  'item': conflictData[i].item,
                  'operationType': conflictData[i].operationType,
                  'originalItem': conflictData[i].originalItem
              });
          }
      }
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  deleteEntityObjectsWithProperties(itemId, conflictData) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId) {
          return;
      }
      const parameter: any = [{
          'itemId': itemId,
          'operationType': 'Delete',
          'containerVersionId': this.Entities.versionId
      }];
      if (conflictData && conflictData.length && conflictData.length > 0) {
          for (let i = 0; i < conflictData.length; i++) {
              parameter.push({
                  'itemId': conflictData[i].itemId,
                  'item': conflictData[i].item,
                  'operationType': conflictData[i].operationType,
                  'originalItem': conflictData[i].originalItem
              });
          }
      }
      const url = 'app/entityRestService/Items(' + itemId +
          ')?action=DeleteItem&changeLog=true&include=PropDescs,Elements,Actions&language=en-US&' + this.getCookie();
      return this.getDeleteRequestObject(url, null, null);
  }

  // delete multiple entity objects
  deleteMultipleEntityObjects(itemIds, entity_key, appId) {
      if (!itemIds || !Array.isArray(itemIds) || !appId) {
          return;
      }
      const selectedEntity = this.getEntity(entity_key, appId);
      if (!selectedEntity) {
          console.warn('Entity is not available for ' + entity_key);
          return;
      }
      const parameter = [];
      for (let i = 0; i < itemIds.length; i++) {
          parameter.push({
              'itemId': itemIds[i],
              'operationType': 'Delete',
              'containerVersionId': selectedEntity.versionId
          });
      }
      const url = 'app/entityRestService/Items?changeLog=true&forceUpdate=true&permitSavingErrors=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }
  // delete multiple objects of different entities
  deleteMultipleDifferentEntityObjects(deleteItemIds, appId) {
      if (!deleteItemIds || !Array.isArray(deleteItemIds) || !appId) {
          return;
      }
      const parameter = [];
      for (let k = 0; k < deleteItemIds.length; k++) {
          const selectedEntity = this.getEntity(deleteItemIds[k].entity_key, appId);
          if (!selectedEntity) {
              console.warn('Entity is not available for ' + deleteItemIds[k].entity_key);
              return;
          }
          if (Array.isArray(deleteItemIds[k].itemIds)) {
              for (let i = 0; i < deleteItemIds[k].itemIds.length; i++) {
                  parameter.push({
                      'itemId': deleteItemIds[k].itemIds[i],
                      'operationType': 'Delete',
                      'containerVersionId': selectedEntity.versionId
                  });
              }
          }
      }
      const url = 'app/entityRestService/Items?changeLog=true&forceUpdate=true&permitSavingErrors=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  deleteEntityObjects(itemId) {
      if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId) {
          return;
      }
      const parameter = [{
          'itemId': itemId,
          'operationType': 'Delete',
          'containerVersionId': this.Entities.versionId
      }];
      const url = 'app/entityRestService/Items(' + itemId +
          ')?action=DeleteItem&changeLog=true&include=PropDescs,Elements,Actions&language=en-US&' + this.getCookie();
      return this.getDeleteRequestObject(url, null, null);
  }

  createEntityObjects(elementId, parentItemId, updateData, entity_key) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!elementId) {
          if (entity_key) {
              const selectedEntity = this.getEntity(entity_key, null);
              if (!selectedEntity) {
                  console.warn('Entity is not available for ' + entity_key);
              } else {
                  elementId = selectedEntity.dataSourceId;
              }
          }
      }
      if (!elementId || !parentItemId) {
          return;
      }
      if (!updateData) { updateData = {}; }
      const parameter = [{
          'operationType': 'Create',
          'elementId': elementId,
          'parentItemId': parentItemId,
          'template': null,
          'item': updateData
      }];
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }




  createEntitywithOutParent(entity_key, updateData, APP_ID) {
     const selectedEntity = this.getEntity(entity_key, APP_ID);
      if (!selectedEntity) {
          console.warn('Entity is not available for ' + entity_key);
          return;
      }
      if (!updateData) { updateData = {}; }
      const parameter = { 'item': updateData };
      const url = 'app/entityRestService/Items?elementId=' + selectedEntity.dataSourceId + '&containerVersionId=' +
          selectedEntity.versionId + '&changeLog=true&include=All,PropDescs,Elements,Actions&language=en-GB&' + this.getCookie();
      return this.getPostRequestObject(url, parameter, null);
  }

  createEntitywithParent(parentItemId, updateData, relationShipName, entityKey, APP_ID) {
       // const selectedEntity = this.getEntity(entity_key, APP_ID);
      if (!relationShipName || !parentItemId) {
          return;
      }
      if (!updateData) { updateData = {}; }
      const parameter = [{
          'operationType': 'Create',
          'parentItemId': parentItemId,
          'relationName': relationShipName,
          'template': null,
          'item': updateData
      }];
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }

  bulkCreateEntitywithParent(updateData, entityKey, APP_ID) {
      if (!updateData) {
          return;
      }
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, updateData, null);
  }

  getEntityItems(itemId) {
      const url = 'app/entityRestService/Items(' + itemId + ')?language=en-US&include=All';
      return this.getGetRequestObject(url, null);
  }

  getEntityList(callbackHandler) {
      let url = 'app/entityRestService/Items($user)?language=en-GB&userId=%24me&include=PropDescs%2CElements%2CActions';
      const req = this.getGetRequestObject(url, null);
      const entityService = this;
      req.subscribe((response) => {
          const versionId = response.data.item.$meta.$containerVersionId;
          let elementId;
          $.each(response.data.dictionary, (key, value) => {
              if (value.usage && value.usage.homePages) {
                  $.each(value.usage.homePages = (innerKey, innerValue) => {
                      if (innerValue.name === 'HomePage') {
                          elementId = innerValue.id;
                      }
                  });
              }
          });
          url = 'app/entityRestService/Elements(' + versionId + '.' + elementId +
              ')?language=en-GB&include=Usage&ChildLevels=1&childDetails=true&ChildFilter=Panel';
          const childReq = entityService.getGetRequestObject(url, null);
          childReq.subscribe((res) => {
              $.each(res.data.children, (key, value) => {
                  if (value.name === 'Views' && value.usage.lists) {
                      callbackHandler(value.usage.lists);
                  }
              });
          });
      });
  }

  getEntityObjectsByfilter(item_ID) {
      const url = 'app/entityRestService/Items(' + item_ID + ')?language=en-US&include=All';
      return this.getGetRequestObject(url, null);
  }

  forwardTask(taskItemId, targetType, forwardTo, reason) {
      if (this.TARGET_USER === targetType ||
          this.TARGET_ROLE === targetType ||
          this.TARGET_TEAM === targetType ||
          this.TARGET_WORKLIST === targetType) {
          let url = 'app/entityRestService/Items(' + taskItemId + ')?action=FORWARD&';
          if (this.TARGET_USER === targetType) {
              url = url.concat('userDN=').concat(forwardTo);
          } else {
              url = url.concat('targetId=').concat(forwardTo).concat('&targetType=').concat(targetType);
          }

          url = url.concat('&memoData=').concat(reason).concat('&language=en-US&').concat(this.getCookie());

          return this.getPostRequestObject(url, null, null);

      } else {
          console.warn('Invalid target type - ' + targetType + '. Allowed types are user, role, team and worklist.');
      }
  }

  doDynamicAction(entityInstanceId, actionName, extraParameters) {
      let url = 'app/entityRestService/Items(' + entityInstanceId + ')?action=' + actionName + '&language=en-US&' + this.getCookie();
      if (extraParameters) { url = url + '&' + extraParameters; }
      return this.getPostRequestObject(url, null, null);
  }

  getCookie() {
      const ca = document.cookie.split(';');
      for (let i = 0; i < ca.length; i++) {
          let cookie = ca[i];
          const cookieNameIndex = cookie.indexOf('=');
          const cookieName = cookie.substring(0, cookieNameIndex);
          if (cookieName.endsWith('_ct')) {
              if (cookie.startsWith(' ')) {
                  cookie = cookie.substring(1);
              }
              return cookie;
          }
      }
      return '';
  }

  getHistoryByItemId(taskItemId) {
      if (taskItemId && taskItemId != null) {
          const url = 'app/entityRestService/Items(' + taskItemId + ')/History';
          return this.getGetRequestObject(url, null);
      } else {
          console.warn('Something went wrong while getting History.');
      }
  }

  getLifecycleByItemId(ItemId) {
      if (ItemId && ItemId != null) {
          const url = 'app/entityRestService/Items(' + ItemId + ')/Lifecycle';
          return this.getGetRequestObject(url, null);
      } else {
          console.warn('Something went wrong while getting Lifecycle.');
      }
  }

  addTask(ItemId, parameter) {
      if (ItemId && ItemId != null) {
          const url = 'app/entityRestService/Items(' + ItemId +
              ')/Lifecycle?action=AddActivities&changeLog=true&include=All&language=en-US&' + this.getCookie();
          return this.getPostRequestObject(url, parameter, null);
      } else {
          console.warn('Something went wrong while adding task.');
      }
  }

  getEntityItemDetails(itemId, relationName,sortField?, sortorder?) {
      if (itemId != null && itemId !== '') {
          let url = 'app/entityRestService/Items(' + itemId + ')';

          if (relationName != null && relationName !== '') {
              url += '/' + relationName;
          }
          url += '?language=en-US&include=All&max=300&%24skip=0'
          if(sortField){
              url += '&%24orderby=' + sortField;
          }
          if(sortorder){
              url += '%20' + sortorder;
          }
          return this.getGetRequestObject(url, null);
      }
  }

  getEntityObjectsCount(entity_key, filterParameters, selectedEntity, appId) {
      if (!selectedEntity) { selectedEntity = this.getEntity(entity_key, appId); }
      if (!selectedEntity) {
          console.warn('Entity is not available for ' + entity_key);
          return;
      }
      const parameter: any = {
          'viewId': selectedEntity.viewId,
          'versionId': selectedEntity.versionId,
          'dataSourceId': selectedEntity.dataSourceId,
          'dataSourceIds': [selectedEntity.dataSourceId],
          'resultOption': 'TotalCount'
      };
      if (filterParameters) {
          parameter.parameters = filterParameters;
      }
      const url = 'app/entityRestService/Elements(' + parameter.versionId + '.' + selectedEntity.viewId +
          ')/ResultItems?language=en-US&' + this.getCookie();
      return this.getPostRequestObject(url, parameter, null);
  }

  setCordysPlatformVersion(version) {
      if (version && version != null && version !== '') {
          this.cordysPlatformVersion = parseFloat(version);
      }
  }
}


 


/*    getEntityObjectsHC(entity_key, filterParameters, orderBy, selectedEntity, skip, top, appId): Observable<any> {
      if (!selectedEntity) { selectedEntity = this.getEntity(entity_key, appId); }
      if (!selectedEntity) {
          console.warn('Entity is not available for ' + entity_key);
          return;
      }
      if (!skip) {
          skip = this.defaultSkip;
      }
      if (!top) {
          top = this.defaultTop;
      }
      const parameter: any = {
          'viewId': selectedEntity.viewId,
          'versionId': selectedEntity.versionId,
          'dataSourceId': selectedEntity.dataSourceId,
          'dataSourceIds': [selectedEntity.dataSourceId],
          'parameters': {

          },
          'resultOption': 'Items',
          'skip': skip,
          'top': top
      };
      if (filterParameters) {
          parameter.parameters = filterParameters;
      }
      if (orderBy) {
          parameter.orderBy = orderBy;
      }

      const url = 'app/entityRestService/Elements(' + parameter.versionId + '.' + selectedEntity.viewId +
          ')/ResultItems?language=en-US&' + this.getCookie();
      return this.getPostRequestObject(url, parameter, null);
  }
deleteEntityObjectsHC(itemId,versionId) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId) {
          return;
      }
      const parameter = [{
          'itemId': itemId,
          'operationType': 'Delete',
          'containerVersionId': versionId
      }];
      const url = 'app/entityRestService/Items(' + itemId +
          ')?action=DeleteItem&changeLog=true&include=PropDescs,Elements,Actions&language=en-US&' + this.getCookie();
      return this.getDeleteRequestObject(url, null, null);
  }createEntitywithOutParentHC(entity_key, updateData, selectedEntity) {

       if (!selectedEntity) {
           console.warn('Entity is not available for ' + entity_key);
           return;
       }
       if (!updateData) { updateData = {}; }
       const parameter = { 'item': updateData };
       const url = 'app/entityRestService/Items?elementId=' + selectedEntity.dataSourceId + '&containerVersionId=' +
           selectedEntity.versionId + '&changeLog=true&include=All,PropDescs,Elements,Actions&language=en-GB&' + this.getCookie();
       return this.getPostRequestObject(url, parameter, null);
   }   

createEntitywithParentHC(parentItemId, updateData, relationShipName, entityKey, APP_ID) {
     
     if (!relationShipName || !parentItemId) {
         return;
     }
     if (!updateData) { updateData = {}; }
     const parameter = [{
         'operationType': 'Create',
         'parentItemId': parentItemId,
         'relationName': relationShipName,
         'template': null,
         'item': updateData
     }];
     const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
     return this.getPutRequestObject(url, parameter, null);
 } 
   // delete entity relation objects
   deleteEntityRelationObjectsHC(itemId, relationName, targetItemId) {
      //if (!this.Entities) { this.Entities = this.entityHelperService.getEntityInfo(null); }
      if (!itemId || !relationName) {
          return;
      }
      const parameter = [{
          'itemId': itemId,
          'relationName': relationName,
          'operationType': 'delete',
          'targetItemId': targetItemId
      }];
      const url = 'app/entityRestService/Items?changeLog=true&language=en-US&' + this.getCookie();
      return this.getPutRequestObject(url, parameter, null);
  }*/