import { Injectable } from '@angular/core';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { findObjects, findObjectsByProp } from 'mpm-library';

@Injectable({
  providedIn: 'root'
})
export class EntityHelperService {

    Entities: Array<any> = [];
    ENTITY_CONTAINER: Array<any> = [];
    APP_ID: string;

    getEntityInfo(appId): Array<any> {
        const applicationId = appId ? appId : this.APP_ID;
        const appObj = findObjects(this.ENTITY_CONTAINER, 'appId', applicationId);
        if (appObj != null && appObj[0] && appObj[0].entities) {
            return appObj[0].entities;
        }
        return this.Entities;
    }

    triggerSOAPRequest(namespace, method, parameters): Observable<any> {
        if (parameters == null || parameters === '' || typeof (parameters) === 'undefined') {
            parameters = '';
        }
        return new Observable(observer => {
                //setTimeout(() => {
                $['soap']({
                    namespace: namespace,
                    method: method,
                    parameters: parameters

                }).then(function (data) {
                    observer.next(data);
                    observer.complete();
                }, function (error) {
                    observer.error(new Error(error));
                });
           // },100000);
            });
       
    }

    initializeAppEntityLists(appId): Observable<any> {
      return new Observable(observer => {
          if (typeof (appId) === 'undefined') {
              observer.error(new Error('Something went wrong while retrieving entity lists for application.'));
              return;
          } else {
              this.APP_ID = appId;
              const ENTITY_LIST_SERVICE_NS = 'http://schemas.acheron.com/mpm/ideation/util/1.0';
              const ENTITY_LIST_METHOD = 'GetAllEntityListDetails';
              const existingObjs = findObjects(this.ENTITY_CONTAINER, 'appId', appId);
              if (existingObjs.length > 0) {
                  observer.next(existingObjs);
                  observer.complete();
                  return;
              }
              const params = {
                  'applicationId': appId
              };
              this.triggerSOAPRequest(ENTITY_LIST_SERVICE_NS, ENTITY_LIST_METHOD, params)
                  .subscribe(response => {
                      const entityList = findObjectsByProp(response, 'EntityList');
                      this.ENTITY_CONTAINER.push({
                          'appId': appId,
                          'entities': entityList
                      });

                      this.Entities = entityList;
                      observer.next(entityList);
                      observer.complete();
                  }, error => {
                      console.error("Failed to load the entity lists for application");
                      observer.error(new Error('Something went wrong while retrieving entity lists for application.'));
                  });
          }
      });
  }

    setEntityHelperAppId(appId): void {
        if (typeof (appId) !== 'undefined' && appId !== '') {
            this.APP_ID = appId;
        }
    }
}
