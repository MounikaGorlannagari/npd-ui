import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class WeekPickerService {

  constructor() { }


  calculateTaskWeek(date, dateType) {
    if (dateType == 'NPD_TASK_ESTIMATED_COMPLETION_DATE' || dateType == 'TASK_DUE_DATE') {
      let abc = new Date(date);
      abc.setDate(abc.getDate() - 1);
      const timeStamp = abc.getTime();
      const year = moment(new Date(timeStamp)).isoWeekYear();
      let weekNo;
      weekNo = moment(new Date(timeStamp)).isoWeek();
      return '' + weekNo + '/' + year
    } else if (dateType == 'NPD_TASK_ESTIMATED_START_DATE' || dateType == 'TASK_START_DATE' || dateType == 'PROJECT_START_DATE' || dateType == 'REQUEST_CREATED_DATE' || dateType == 'NPD_PROJECT_DATE_DEVELOPMENT_COMPLETED_S1') {
      const currentdate = new Date(date);

      const timeStamp = currentdate.getTime();
      const year = moment(new Date(timeStamp)).isoWeekYear();
      let weekNo;
      weekNo = moment(new Date(timeStamp)).isoWeek();
      return '' + weekNo + '/' + year
    }

  }

  getWeekFormat(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates: NgbDate[] = this.calculateDateNgb(week, year);
      return [weekdates[0], weekdates[1]];
    }
  }

  calculateDateNgb(week: number, year: number): NgbDate[] {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate, ngbToDate];
  }

  getStartEndDate(weekFormat, dateType?) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];
      let weekdates 

      if(!dateType){
        weekdates =  this.calculateStartAndEndDates(week, year)
      }else{
       weekdates = this.calculateDate(week, year, dateType);
      }
      return [weekdates[0], weekdates[1]];
    }
  }

  calculateDate(week: number, year: number, dateType) {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );

    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );

    const selectFromDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate() + 1
    );

    let fDate = new Date(
      `${selectFromDate.year.toString()}-${selectFromDate.month
        .toString()
        .padStart(2, '0')}-${selectFromDate.day.toString().padStart(2, '0')}`
    );

    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    if (dateType == 'NPD_TASK_ESTIMATED_COMPLETION_DATE' || dateType == 'TASK_DUE_DATE') {
      const toDate = new Date(fDate.getTime() + 6 * 24 * 60 * 60 * 1000);
      return [fromDate, toDate];

    } else if (dateType == 'NPD_TASK_ESTIMATED_START_DATE' || dateType == 'TASK_START_DATE') {
      const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
      return [fromDate, toDate];
    }
  }

  calculateStartAndEndDates(week: number, year: number) {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + "/1/4"); //NPD1-83 Safari Browser issue
    const date = new Date(
        firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
        date.getFullYear(),
        date.getMonth() + 1,
        date.getDate() + 1
    );
    let fromDate = new Date(`${selectDate.year.toString()}-${selectDate.month.toString().padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`);
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    return [fromDate, toDate];
}
}
