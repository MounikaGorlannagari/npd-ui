import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import $ from 'jquery';
import {
  AppService,
  IndexerDataTypes,
  LoaderService,
  MPMField,
  NotificationService,
  SharingService,
  UtilService,
} from 'mpm-library';
import { BusinessConstants } from '../request-view/constants/BusinessConstants';
import { EntityService } from './entity.service';
import * as moment from 'moment';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { I } from '@angular/cdk/keycodes';
import { Subject } from 'rxjs';

/* moment.updateLocale('en', {
  week: {
    dow : 1, // Monday is the first day of the week.
  }
}); */
@Injectable({
  providedIn: 'root',
})
export class MonsterUtilService {
  allFormProperties: any = {};
  allFormRelations: any = {};

  propertyUpdateErrorRetryCount = 0;
  relationUpdateErrorRetryCount = 0;

  allStatus = BusinessConstants.REQUEST_STATUS;

  constructor(
    private utilService: UtilService,
    private sharingService: SharingService,
    private entityService: EntityService,
    private notificationService: NotificationService,
    private loaderService: LoaderService,
    private appService: AppService,
    private dialog: MatDialog,
    private datePipe: DatePipe
  ) {}

  checkElementEventPosition(panelHTML, event) {
    const position = $(panelHTML).offset();
    const height = $(panelHTML).height();
    const width = $(panelHTML).width();
    if (
      event.pageY < position.top ||
      event.pageY > position.top + height ||
      event.pageY < position.bottom ||
      event.pageY > position.bottom + height ||
      event.pageX < position.left ||
      event.pageX > position.left + width ||
      event.pageX < position.right ||
      event.pageX > position.right + width
    ) {
      return true;
    }
  }

  disableFormControls(form, conditionObj) {
    Object.keys(form.controls).forEach((key) => {
      conditionObj[key] && conditionObj[key].disabled
        ? form.get(key).disable()
        : form.get(key).enable();
    });
  }

  disableFormControlsForDeliverable(form, conditionObj) {
    let count = 0;
    Object.keys(form.controls).forEach((key) => {
      if (key == 'volume') {
        if (count == 0) {
          conditionObj[key] && conditionObj[key].disabled
            ? form.get(key).disable()
            : form.get(key).enable();
          // form.get(key).enable()
          count++;
          return;
        }
      }
    });
  }

  setFormProperty(itemId, property, value) {
    if (
      this.allFormProperties[itemId] &&
      this.allFormProperties[itemId].Properties
    ) {
      this.allFormProperties[itemId].Properties[property] = value;
    }
    // else if(this.allFormProperties[itemId] && this.allFormProperties[itemId].Properties==){

    // }
    else {
      this.allFormProperties[itemId] = {};
      this.allFormProperties[itemId].Properties = {
        [property]: value,
      };
    }
  }

  setFormRelations(itemId, relation, targetItemId) {
    if (this.allFormRelations[itemId]) {
      this.allFormRelations[itemId][relation] = targetItemId;
    } else {
      this.allFormRelations[itemId] = {
        [relation]: targetItemId,
      };
    }
  }

  updateFormProperty(isAppUpdate?, callback?, errorCallBack?) {
    if (isAppUpdate) {
      this.relationUpdateErrorRetryCount = 0;
      this.propertyUpdateErrorRetryCount = 0;
    }
    const updateObjects = [];
    // tslint:disable-next-line: forin
    for (const prop in this.allFormProperties) {
      updateObjects.push({
        ItemId: prop,
        updateData: this.allFormProperties[prop],
      });
    }
    this.loaderService.show();
    if (updateObjects.length === 0) {
      this.loaderService.hide();
      this.updateFormRelations(callback, errorCallBack, false);
      return;
    }

    this.notificationService.info(
      'Please do not close the browser window. Updating the form changes to server.'
    );
    this.entityService
      .updateMultipleEntityObjectsProperty(updateObjects)
      .subscribe(
        (response) => {
          this.allFormProperties = {};
          this.loaderService.hide();
          this.updateFormRelations(callback, errorCallBack, true);
          this.propertyUpdateErrorRetryCount = 0;
          //this.notificationService.success('Form changes are saved successfully.');
        },
        (error) => {
          this.loaderService.hide();
          this.notificationService.error(
            'Error while saving the form changes : Properties. Retrying update again. Please wait for some time.'
          );
          if (this.propertyUpdateErrorRetryCount < 5) {
            this.propertyUpdateErrorRetryCount += 1;
            this.updateFormProperty();
          } else {
            //this.allFormProperties = {};
            if (errorCallBack) {
              errorCallBack(false);
            }
            this.notificationService.error(
              'Please check your internet connection or contact admin.'
            );
          }
        }
      );
  }

  updateFormRelations(callback?, errorCallBack?, isNested?) {
    const updateObjects = [];
    // tslint:disable-next-line: forin
    for (const prop in this.allFormRelations) {
      for (const relation in this.allFormRelations[prop]) {
        if (this.utilService.isValid(this.allFormRelations[prop][relation])) {
          updateObjects.push({
            itemId: prop,
            relationName: relation,
            operationType: 'Update',
            targetItemId: this.allFormRelations[prop][relation],
          });
        } else {
          updateObjects.push({
            itemId: prop,
            relationName: relation,
            operationType: 'Delete',
            targetItemId: null,
          });
        }
      }
    }
    this.loaderService.show();
    if (updateObjects.length === 0) {
      this.loaderService.hide();
      if (callback) {
        callback(isNested);
      }
      return;
    }
    this.notificationService.info(
      'Please do not close the browser window. Updating the form changes to server.'
    );
    this.entityService
      .updateBulkEntityRelationObjects(true, 'true', updateObjects, false, '')
      .subscribe(
        () => {
          this.loaderService.hide();
          this.allFormRelations = {};
          this.relationUpdateErrorRetryCount = 0;
          //this.notificationService.success('Form changes are saved successfully.');
          if (callback) {
            callback(true);
          }
        },
        () => {
          // tslint:disable-next-line: max-line-length
          this.notificationService.error(
            'Error while saving the form changes: Relations. Retrying update again. Please wait for some time.'
          );
          this.loaderService.hide();
          if (this.relationUpdateErrorRetryCount < 5) {
            this.relationUpdateErrorRetryCount += 1;
            this.updateFormRelations();
          } else {
            ///this.allFormRelations = {};
            if (errorCallBack) {
              errorCallBack(false);
            }
            this.notificationService.error(
              'Please check your internet connection or contact admin.'
            );
          }
        }
      );
  }

  finalBulkUpdateFormData(callback?, errorcallback?) {
    const updateObjects = [];
    for (const prop in this.allFormProperties) {
      updateObjects.push({
        ItemId: prop,
        updateData: this.allFormProperties[prop],
      });
    }
    this.loaderService.show();
    if (updateObjects.length === 0) {
      this.loaderService.hide();
      this.finalupdateFormRelations(callback, errorcallback);
      return;
    }
    this.entityService
      .updateMultipleEntityObjectsProperty(updateObjects)
      .subscribe(
        (response) => {
          this.allFormProperties = {};
          this.loaderService.hide();
          this.finalupdateFormRelations(callback, errorcallback);
        },
        (error) => {
          this.loaderService.hide();
          if (errorcallback) {
            errorcallback();
          }
        }
      );
  }

  finalBulkUpdateFormProjectData(callback?, errorcallback?) {
    const updateObjects = [];
    for (const prop in this.allFormProperties) {
      updateObjects.push({
        ItemId: prop,
        updateData: this.allFormProperties[prop],
      });
    }
    if (updateObjects.length === 0) {
      this.finalupdateFormRelations(callback, errorcallback);
      return;
    }
    this.entityService
      .updateMultipleEntityObjectsProperty(updateObjects)
      .subscribe(
        (response) => {
          this.allFormProperties = {};
          this.finalupdateFormRelations(callback, errorcallback);
        },
        (error) => {
          if (errorcallback) {
            errorcallback();
          }
        }
      );
  }

  clearFormPropertiesandRelations() {
    this.allFormProperties = {};
    this.allFormRelations = {};
  }

  finalupdateFormRelations(callback?, errorcallback?) {
    const updateObjects = [];
    for (const prop in this.allFormRelations) {
      for (const relation in this.allFormRelations[prop]) {
        if (this.utilService.isValid(this.allFormRelations[prop][relation])) {
          updateObjects.push({
            itemId: prop,
            relationName: relation,
            operationType: 'Update',
            targetItemId: this.allFormRelations[prop][relation],
          });
        } else {
          updateObjects.push({
            itemId: prop,
            relationName: relation,
            operationType: 'Delete',
            targetItemId: null,
          });
        }
      }
    }
    this.loaderService.show();
    if (updateObjects.length === 0) {
      if (callback) {
        callback();
      }
      this.loaderService.hide();
      return;
    }
    this.entityService
      .updateBulkEntityRelationObjects(true, 'true', updateObjects, false, '')
      .subscribe(
        () => {
          this.loaderService.hide();
          this.allFormRelations = {};
          if (callback) {
            callback();
          }
        },
        () => {
          this.loaderService.hide();
          if (errorcallback) {
            errorcallback();
          }
        }
      );
  }

  removeFormProperty(itemId) {
    delete this.allFormProperties[itemId];
  }

  removeFormRelations(itemId) {
    delete this.allFormRelations[itemId];
  }

  changeStatusColor(status) {
    let style;
    if (status === 'InProgress' || status === 'In Progress') {
      style = 'progress-status';
    } else if (status === 'Completed' || status === 'Approved/Completed') {
      style = 'completed-status';
    } else if (status === 'InDraft' || status === 'Defined') {
      style = 'draft-status';
    } else if (status === 'Deleted' || status === 'Deleted') {
      style = 'delete-status';
    } else if (
      status === 'Denied' ||
      status === 'Cancelled' ||
      status === 'Requested Changes'
    ) {
      style = 'denied-status';
    } else {
      style = 'default-status';
    }
    return style;
  }

  changeTaskStatusColor(status) {
    let style;
    if (status === 'InProgress' || status === 'In Progress') {
      style = 'task-progress-status';
    } else if (status === 'Completed' || status === 'Approved/Completed') {
      style = 'task-completed-status';
    } else if (status === 'InDraft' || status === 'Defined') {
      style = 'task-progress-status';
    } else if (status === 'Deleted' || status === 'Deleted') {
      style = 'delete-status';
    } else if (
      status == 'OnHold' ||
      status === 'Denied' ||
      status === 'Requested Changes'
    ) {
      style = 'denied-status';
    } else if (status === 'Cancelled') {
      style = 'cancelled-status';
    } else {
      style = 'default-status';
    }
    return style;
  }

  changeProjectStatusColor(status) {
    let style;
    if (status === 'In Progress') {
      style = 'progress-status';
    } else if (status === 'Completed') {
      style = 'completed-status';
    } else if (
      status === 'Draft' ||
      status === 'Defined' ||
      status === 'Active'
    ) {
      style = 'draft-status';
    } else if (
      status === 'Cancelled' ||
      status === 'Blocked' ||
      status === 'At Risk' ||
      status === 'OnHold'
    ) {
      style = 'denied-status';
    } else {
      style = 'default-status';
    }
    return style;
  }

  /*    changeProjectStatusColor(status) {
      let style;
      if(status === "Intermediate") {
       style = "progress-status";
      }
      else if(status === "Completed") {
        style = "completed-status";
      } 
      else if(status === "Final Cancelled" || status === "Rejected" || status === "Cancelled" || status === "OnHold") {
        style = "denied-status";
      } 
      else {
        style = "default-status";
      }
      return style;
     }  */

  getStatusDisplayName(value) {
    return this.allStatus.find((status) => status.VALUE === value);
  }

  transformDate(date, format) {
    return this.datePipe.transform(date, format);
  }

  getFieldValueByDisplayColumn(displayColumn: MPMField, dataSet: any): any {
    if (displayColumn && dataSet && displayColumn.INDEXER_FIELD_ID) {
      const indexerId = this.formIndexerIdFromField(displayColumn);
      let currValue = dataSet[indexerId];
      return currValue;
    }
    return 'NA';
  }

  formIndexerIdFromField(displayColumn: MPMField): string {
    let indexerId = displayColumn.INDEXER_FIELD_ID;
    return indexerId;
  }
  /* 
    convertDate(date) {
      return new Date(date).setUTCHours(0);
    }
  
    calculateWeek(weekDate) {//: Date
      let date = this.convertDate(weekDate);
      const currentdate = new Date(date);
      const timeStamp = currentdate.getTime();
      const year = new Date(date).getFullYear();
      let weekDay = this.getDayOfWeek(new Date(timeStamp));
      let weekNo ;
      if( weekDay == 'Su') {
      weekNo = moment(new Date(timeStamp)).week() - 2;//this.dateArray[i - 1]['weekNo'];
      } else {
        weekNo = moment(new Date(timeStamp)).week();//- 1;
      }
      return ''+weekNo + '/'+ year
    }    */
  /*   calculateWeek(date) {//: Date
      const currentdate = new Date(date);
      const timeStamp = currentdate.getTime();
      const year = new Date(date).getFullYear();
      let weekDay = this.getDayOfWeek(new Date(timeStamp));
      let weekNo ;
      if( weekDay == 'Su') {
        weekNo = moment(new Date(timeStamp)).week() - 2;//this.dateArray[i - 1]['weekNo'];
      } else {
        weekNo = moment(new Date(timeStamp)).week() - 1;
      }
      return ''+weekNo + '/'+ year
    }  */

  calculateWeek(date) {
    //: Date
    let currentdate = new Date(date);
    if (new Date().getTimezoneOffset() > 0) {
      currentdate = new Date(
        currentdate.getFullYear(),
        currentdate.getMonth(),
        currentdate.getDate() + 1
      );
    }
    currentdate.setHours(0);
    currentdate.setMinutes(0);
    const timeStamp = currentdate.getTime();
    const year = moment(new Date(timeStamp)).isoWeekYear();
    let weekNo;
    //moment().weekday(0);
    //moment().isoWeekYear();
    weekNo = moment(new Date(timeStamp)).isoWeek();
    return '' + weekNo + '/' + year;
  }

  calculateResourceWeek(date) {
    //: Date
    let currentdate = new Date(date);
    const timeStamp = currentdate.getTime();
    const year = moment(new Date(timeStamp)).isoWeekYear();
    let weekNo;
    //moment().weekday(0);
    //moment().isoWeekYear();
    weekNo = moment(new Date(timeStamp)).isoWeek();
    return '' + weekNo + '/' + year;
  }

  calculateWeekNo(date) {
    //: Date
    const currentdate = new Date(date);
    const timeStamp = currentdate.getTime();
    let weekNo;
    weekNo = moment(new Date(timeStamp)).isoWeek();
    return weekNo;
  }

  calculateTaskWeek(date) {
    //: Date
    let currentdate = new Date(date);
    const timeStamp = currentdate.getTime();
    const year = moment(new Date(timeStamp)).isoWeekYear();
    let weekNo;
    //moment().weekday(0);
    //moment().isoWeekYear();
    weekNo = moment(new Date(timeStamp)).isoWeek();
    return '' + weekNo + '/' + year;
  }

  calculateTaskPlannedWeek(date) {
    //: Date
    let currentdate = new Date(date);
    if (new Date().getTimezoneOffset() > 0) {
      currentdate = new Date(
        currentdate.getFullYear(),
        currentdate.getMonth(),
        currentdate.getDate() + 1
      );
    }
    currentdate.setHours(0);
    currentdate.setMinutes(0);
    const timeStamp = currentdate.getTime();
    const year = moment(new Date(timeStamp)).isoWeekYear();
    let weekNo;
    //moment().weekday(0);
    //moment().isoWeekYear();
    weekNo = moment(new Date(timeStamp)).isoWeek();
    return '' + weekNo + '/' + year;
  }

  calculateTaskWeekNo(date) {
    //: Date
    const currentdate = new Date(date);
    const timeStamp = currentdate.getTime();
    let weekNo;
    weekNo = moment(new Date(timeStamp)).isoWeek();
    return weekNo;
  }

  /*   calculateWeekNo(date) {//: Date
      const currentdate = new Date(date);
      const timeStamp = currentdate.getTime();
      const year = new Date(date).getFullYear();
      let weekDay = this.getDayOfWeek(new Date(timeStamp));
      let weekNo ;
      if( weekDay == 'Su') {
        weekNo = moment(new Date(timeStamp)).week() - 2;//this.dateArray[i - 1]['weekNo'];
      } else {
        weekNo = moment(new Date(timeStamp)).week() - 1;
      }
      return weekNo;
    }  */
  /*   calculateWeek(date) {//: Date
      //const currentdate = new Date(date);
      //const timeStamp = currentdate.getTime();
      const year = new Date(date).getFullYear();
      let timeStamp = new Date(+`${new Date(date).getTime() + (new Date().getTimezoneOffset() * 60 * 1000)} `).getTime();
      let weekDay = this.getDayOfWeek(new Date(timeStamp));
      let weekNo ;
      if( weekDay == 'Su') {
        weekNo = moment(new Date(timeStamp)).week() - 2;//this.dateArray[i - 1]['weekNo'];
      } else {
        weekNo = moment(new Date(timeStamp)).week();
      }
      return ''+weekNo + '/'+ year
    }   */

  /*    calculateWeek(date) {//: Date
      const currentdate = new Date(date);
      const timeStamp = currentdate.getTime();
      const year = new Date(date).getFullYear();
      let weekDay = this.getDayOfWeek(new Date(timeStamp));
      let weekNo ;
      if( weekDay == 'Su') {
       weekNo = moment(new Date(timeStamp)).week() - 2;//this.dateArray[i - 1]['weekNo'];
      } else {
        weekNo = moment(new Date(timeStamp)).week() - 1;
      }
      return ''+weekNo + '/'+ year
    }  */

  getDayOfWeek(date) {
    const dayOfWeek = new Date(date).getDay();
    return isNaN(dayOfWeek)
      ? null
      : ['Su', 'M', 'T', 'W', 'Th', 'F', 'Sa'][dayOfWeek];
  }

  getStartEndDate(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];
      let weekdates = this.calculateDate(week, year);
      // weekdates[0].setHours(0);
      // weekdates[0].setMinutes(0);
      // weekdates[1].setHours(0);
      // weekdates[1].setMinutes(0);
      return [weekdates[0], weekdates[1]];
    }
  }

  calculateDate(week, year) {
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year?.toString()}-${selectDate.month
        ?.toString()
        .padStart(2, '0')}-${selectDate.day?.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    return [fromDate, toDate];
  }

  /*subject declaration to use in the layout component to change the status 
  of assets menu when deletion happens from anywhere in the project page*/
  public overViewProjectAssetCount$ = new Subject<any>();

  /* When ever deletion of assets happen it will trigger the OTMM 
  after user refreshes the page which will get the specified assets 
  after update it will set the subject value*/
  sendAssetCount(count: number) {
    if (count != null) {
      this.overViewProjectAssetCount$.next(count);
    }
  }

  isDateStringMatches(indexerField: string): boolean {
    if (
      indexerField === 'NPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK' ||
      indexerField === 'NPD_PROJECT_CP2_REVISED_DATE' ||
      indexerField === 'NPD_PROJECT_CP1_REVISED_DATE' ||
      indexerField === 'NPD_PROJECT_EAN_TARGET_WEEK' ||
      indexerField === 'NPD_PROJECT_EAN_ACTUAL_WEEK' ||
      indexerField === 'NPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK' ||
      indexerField === 'NPD_PROJECT_PALLET_LABEL_RECEIVED_ACTUAL_WEEK' ||
      indexerField === 'NPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK' ||
      indexerField === 'NPD_PROJECT_PALLET_LABEL_SENT_ACTUAL_WEEK' ||
      indexerField === 'NPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER' ||
      indexerField === 'NPD_PROJECT_TRIAL_MATERIALS_CONFIRMED_DELIVERY_WEEK' ||
      indexerField === 'NPD_PROJECT_EARLIEST_TRIAL_OR_1ST_PROD_DATE' ||
      indexerField === 'NPD_PROJECT_OPS_ALIGNED_DP_WEEK' ||
      indexerField === 'RT_GOVERNANCE_APPROACH' ||
      indexerField === 'RQ_GOVERNANCE_APPROACH' ||
      indexerField === 'NPD_PROJECT_GOVERNANCE_APPROACH' ||
      indexerField === 'NPD_PROJECT_NEXT_GOVERNANCE_DUE' ||
      indexerField === 'NPD_PROJECT_CP1_TARGET_START_DATE' ||
      indexerField === 'NPD_PROJECT_CP1_LIVE_DATE' ||
      indexerField === 'NPD_PROJECT_CP1_DECISION_DATE' ||
      indexerField === 'NPD_PROJECT_CP1_DECISION' ||
      indexerField === 'NPD_PROJECT_CP2_REVISED_DATE' ||
      indexerField === 'NPD_PROJECT_CP2_TARGET_START_DATE' ||
      indexerField === 'NPD_PROJECT_CP2_LIVE_DATE' ||
      indexerField === 'NPD_PROJECT_CP2_DECISION_DATE' ||
      indexerField === 'NPD_PROJECT_CP2_DECISION' ||
      indexerField === 'NPD_PROJECT_CP3_REVISED_DATE' ||
      indexerField === 'NPD_PROJECT_CP3_TARGET_START_DATE' ||
      indexerField === 'NPD_PROJECT_CP3_LIVE_DATE' ||
      indexerField === 'NPD_PROJECT_CP3_DECISION_DATE' ||
      indexerField === 'NPD_PROJECT_CP3_DECISION' ||
      indexerField === 'NPD_PROJECT_CP4_REVISED_DATE' ||
      indexerField === 'NPD_PROJECT_CP4_TARGET_START_DATE' ||
      indexerField === 'NPD_PROJECT_CP4_LIVE_DATE' ||
      indexerField === 'NPD_PROJECT_CP4_DECISION_DATE' ||
      indexerField === 'NPD_PROJECT_CP4_DECISION' ||
      indexerField === 'NPD_PROJECT_GOVERNED_DELIVERY_DATE' ||
      indexerField === 'NPD_PROJECT_PROD_INFO_TARGET_WEEK' ||
      indexerField === 'NPD_PROJECT_PROD_INFO_ACTUAL_WEEK' ||
      indexerField ===
        'NPD_PROJECT_FINAL_AGREED_1ST_PROD_CONFIRMED_WITH_BOTTLER' ||
      indexerField ===
        'NPD_PROJECT_1ST_PROD_MATERIALS_CONFIRMED_DELIVERY_WEEK' ||
      indexerField === 'NPD_PROJECT_LABEL_UPLOADED_ACTUAL_DATE' ||
      indexerField === 'NPD_PROJECT_PALLET_LABEL_RECEIVED_TARGET_WEEK' ||
      indexerField === 'NPD_PROJECT_PALLET_LABEL_RECEIVED_ACTUAL_WEEK' ||
      indexerField === 'NPD_PROJECT_PALLET_LABEL_SENT_TARGET_WEEK' ||
      indexerField === 'NPD_PROJECT_PALLET_LABEL_SENT_ACTUAL_WEEK' ||
      indexerField === 'NPD_PROJECT_BOTTLER_CAN_TARGET_WEEK' ||
      indexerField === 'NPD_PROJECT_BOTTLER_CAN_ACTUAL_WEEK' ||
      indexerField === 'NPD_PROJECT_PO_RECEIVED_ACTUAL_WEEK' ||
      indexerField === 'NPD_PROJECT_CP1_AGREEMENT_DATE' ||
      indexerField === 'RT_TASK_COMPLETED_BY' ||
      indexerField === 'NPD_PROJECT_TECH_QUAL_COMPLETED' ||
      indexerField === 'NPD_PROJECT_TECH_QUAL_WEEK_DUE' ||
      indexerField === 'NPD_PROJECT_AUDIT_DATE' ||
      indexerField === 'NPD_PROJECT_HBC_RPE_MSEB_CREATED' ||
      indexerField === 'NPD_PROJECT_QA_QC_RELEASE_DATE' ||
      indexerField === 'NPD_PROJECT_FG_CODE_VOLUME_TRANSFER_WEEK' ||
      indexerField === 'NPD_PROJECT_BOM_SENT_ACTUAL_DATE' ||
      indexerField === 'RT_TASK_START_DATE' ||
      indexerField === 'RT_TASK_DUE_DATE' ||
      indexerField === 'RT_TASK_COMPLETION_DATE' ||
      indexerField === 'NPD_PROJECT_ON_TRACK_ORIGINAL_CP1_DATE' ||
      indexerField === 'NPD_PROJECT_TARGET_CP2_DATE' ||
      indexerField === 'NPD_PROJECT_CP1_REVISED_DATE'
    ) {
      return true;
    }
    return false;
  }
}

/*    calculateWeek2(date) {//: Date
    const time = new Date(date).getTime() + 4 * 24 * 60 * 60 * 1000;
    const firstDay = new Date( new Date(date).getFullYear() + "/1/1");
    const year =new Date(date).getFullYear()
    return (
      Math.floor(Math.round((time - firstDay.getTime()) / 86400000) / 7) + 1
    ) +'/'+year
 
} 

calculateWeek3(date) {//: Date
  const currentdate = new Date(date);
  const oneJan = new Date(currentdate.getFullYear(),0,1);
  const year =new Date(date).getFullYear()
  const numberOfDays = Math.floor((currentdate.valueOf() - oneJan.valueOf()) / (24 * 60 * 60 * 1000));
  let result = Math.ceil(( currentdate.getDay() + 1 + numberOfDays) / 7);
  return result + '/'+year
} 
*/

/*
updateFormProperty(isAppUpdate?, callback?,isSuccessCallback?,isErrorCallback?,){
  if (isAppUpdate){
     this.relationUpdateErrorRetryCount = 0;
     this.propertyUpdateErrorRetryCount = 0;
  }
  const updateObjects = [];
  // tslint:disable-next-line: forin
  for (const prop in this.allFormProperties){
    updateObjects.push({
        ItemId: prop,
        updateData: this.allFormProperties[prop]
    });
  }
  this.loaderService.show();
  if (updateObjects.length === 0){
      this.loaderService.hide();
      this.updateFormRelations(callback);
      return;
  }
  if(!isSuccessCallback) {
    this.notificationService.info('Please do not close the browser window. Updating the form changes to server.');
  }
  this.entityService.updateMultipleEntityObjectsProperty(updateObjects).subscribe(response => {
        this.allFormProperties = {};
        this.loaderService.hide();
        this.updateFormRelations(()=> {

        });
        this.propertyUpdateErrorRetryCount = 0;
        if(!isSuccessCallback) {
          this.notificationService.success('Form changes are saved successfully.');
        }
      
  }, error => {
        this.loaderService.hide();
        if(!isErrorCallback) {
          this.notificationService.error('Error while saving the form changes : Properties. Retrying update again. Please wait for some time.');
            if (this.propertyUpdateErrorRetryCount < 5){
              this.propertyUpdateErrorRetryCount += 1;
              this.updateFormProperty();
          }
          else{
              if (callback){
                  callback();
              }
              this.notificationService.error('Please check your internet connection or contact admin.');
          }
        }
        else {
          isErrorCallback();
        }
       
  });
}

updateFormRelations(callback?,isSuccessCallback?,isErrorCallback?){
 const updateObjects = [];
 // tslint:disable-next-line: forin
 for (const prop in this.allFormRelations){
      for (const relation in this.allFormRelations[prop]) {
          if (this.utilService.isValid(this.allFormRelations[prop][relation])){
                 updateObjects.push({
                     itemId : prop,
                     relationName: relation,
                     operationType : 'Update',
                     targetItemId: this.allFormRelations[prop][relation]
                 });
          }
          else{
                updateObjects.push({
                    itemId : prop,
                    relationName: relation,
                    operationType : 'Delete',
                    targetItemId: null
                });
          }
      }
 }
 this.loaderService.show();
 if (updateObjects.length === 0){
    this.loaderService.hide();
    if (callback){
        callback();
    }
    return;
 }
 if(!isSuccessCallback) {
  this.notificationService.info('Please do not close the browser window. Updating the form changes to server.');
 }
 this.entityService.updateBulkEntityRelationObjects(true, 'true', updateObjects, false, '').subscribe(() => {
      this.loaderService.hide();
      this.allFormRelations = {};
      this.relationUpdateErrorRetryCount = 0;
      this.notificationService.success('Form changes are saved successfully.');
      if (callback){
        callback();
      }
 }, () => {
      if(isErrorCallback) {
        isErrorCallback();
      } else {
           // tslint:disable-next-line: max-line-length
        this.notificationService.error('Error while saving the form changes: Relations. Retrying update again. Please wait for some time.');
        this.loaderService.hide();
        if (this.relationUpdateErrorRetryCount < 5){
          this.relationUpdateErrorRetryCount += 1;
          this.updateFormRelations();
        }
        else{
            if (callback){
              callback();
            }
            this.notificationService.error('Please check your internet connection or contact admin.');
        }
      }
 });
}
*/
