import { Injectable } from '@angular/core';
import { SharingService, UtilService } from 'mpm-library';
import { MonsterConfigApiService } from '../services/monster-config-api.service';
import { MONSTER_ROLES } from './role.config';

@Injectable({
  providedIn: 'root'
})
export class FilterConfigService {

  constructor( 
    private utilService: UtilService,
    ) { }

  isCommercialRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isCommercialAccess = false;
    allRoles.forEach(roles => {
        isCommercialAccess = (!isCommercialAccess) ? (roles.Name === (MONSTER_ROLES.MONSTER_COMMERCIAL)) : isCommercialAccess;
    });
    return isCommercialAccess;
  }

  isProgramManagerRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isProgramManagerAccess = false;
    allRoles.forEach(roles => {
      isProgramManagerAccess = (!isProgramManagerAccess) ? (roles.Name === (MONSTER_ROLES.NPD_PM)) : isProgramManagerAccess;
    });
    return isProgramManagerAccess;
  }

  isProjectSpecialistRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isProjectSpecialstAccess = false;
    allRoles.forEach(roles => {
      isProjectSpecialstAccess = (!isProjectSpecialstAccess) ? (roles.Name === (MONSTER_ROLES.NPD_PROjECT_SPECIALIST)) : isProjectSpecialstAccess;
    });
    return isProjectSpecialstAccess;
  }

  isGFGRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isGFGRoleAccess = false;
    allRoles.forEach(roles => {
      isGFGRoleAccess = (!isGFGRoleAccess) ? (roles.Name === (MONSTER_ROLES.GFG)) : isGFGRoleAccess;
    });
    return isGFGRoleAccess;
  }

  isRegionalTechnicalManagerRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isRTMAccess = false;
    allRoles.forEach(roles => {
      isRTMAccess = (!isRTMAccess) ? (roles.Name === (MONSTER_ROLES.REGIONAL_TECHNICAL_MANAGER)) : isRTMAccess;
    });
    return isRTMAccess;
  }

  isCorpProjectLeader(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isCorpProjectLeaderAccess = false;
    allRoles.forEach(roles => {
      isCorpProjectLeaderAccess = (!isCorpProjectLeaderAccess) ? (roles.Name === (MONSTER_ROLES.CORP_PROJECT_LEAD)) : isCorpProjectLeaderAccess;
    });
    return isCorpProjectLeaderAccess;
  }

  isRegsTrafficManager(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isRegsTrafficManagerAccess = false;
    allRoles.forEach(roles => {
      isRegsTrafficManagerAccess = (!isRegsTrafficManagerAccess) ? (roles.Name === (MONSTER_ROLES.REGS_TRAFFIC_MANAGER)) : isRegsTrafficManagerAccess;
    });
    return isRegsTrafficManagerAccess;
  }
  
  isAdminRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isAdminAccess = false;
    allRoles.forEach(roles => {
      isAdminAccess = (!isAdminAccess) ? (roles.Name === (MONSTER_ROLES.NPD_ADMIN)) : isAdminAccess;
    });
    return isAdminAccess;
  }

  isE2EPMRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isE2EPMAccess = false;
    allRoles.forEach(roles => {
      isE2EPMAccess = (!isE2EPMAccess) ? (roles.Name === (MONSTER_ROLES.NPD_E2EPM)) : isE2EPMAccess;
    });
    return isE2EPMAccess;
  }

  isCorpPMRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let iscorpPMAccess = false;
    allRoles.forEach(roles => {
      iscorpPMAccess = (!iscorpPMAccess) ? (roles.Name === (MONSTER_ROLES.NPD_CORPPM)) : iscorpPMAccess;
    });
    return iscorpPMAccess;
  }

  isOPSPMRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isOPSPMAccess = false;
    allRoles.forEach(roles => {
      isOPSPMAccess = (!isOPSPMAccess) ? (roles.Name === (MONSTER_ROLES.NPD_OPS_PM)) : isOPSPMAccess;
    });
    return isOPSPMAccess;
  }

    
  isRegulatoryRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isRegulatoryAccess = false;
    allRoles.forEach(roles => {
      isRegulatoryAccess = (!isRegulatoryAccess) ? (roles.Name === (MONSTER_ROLES.NPD_REGULATORY)) : isRegulatoryAccess;
    });
    return isRegulatoryAccess;
  }

  
  isSecondaryPackagingRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isSecondaryPackagingAccess = false;
    allRoles.forEach(roles => {
      isSecondaryPackagingAccess = (!isSecondaryPackagingAccess) ? (roles.Name === (MONSTER_ROLES.NPD_SECONDARY_PACKAGING)) : isSecondaryPackagingAccess;
    });
    return isSecondaryPackagingAccess;
  }


  
  isTechnicalQualRole(): boolean {
    const allRoles = this.utilService.getUserRoles();
    let isTechnicalQualAccess = false;
    allRoles.forEach(roles => {
      isTechnicalQualAccess = (!isTechnicalQualAccess) ? (roles.Name === (MONSTER_ROLES.NPD_TECHNICAL_QUAL)) : isTechnicalQualAccess;
    });
    return isTechnicalQualAccess;
  }

  isFPMSRole(): boolean{
    const allRoles=this.utilService.getUserRoles();
    let isFPMSAccess= false;
    allRoles.forEach(role=>{
      isFPMSAccess = (!isFPMSAccess) ? (role.Name === (MONSTER_ROLES.NPD_FPMS)) : isFPMSAccess;
    });
    return isFPMSAccess;
  }
  
}
