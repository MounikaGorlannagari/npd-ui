export enum MONSTER_ROLES {
    MONSTER_COMMERCIAL = 'Commercial',
    NPD_E2EPM = 'E2E PM',
    NPD_CORPPM ='Corp PM',
    NPD_PM = 'Programme Manager',
    NPD_OPS_PM = 'Ops PM',
    NPD_PROjECT_SPECIALIST = 'Project Specialist',
    NPD_REGULATORY= 'Regulatory Affairs',//CHANGE VALUES HERE
    NPD_SECONDARY_PACKAGING = 'Secondary AW Specialist',
    NPD_TECHNICAL_QUAL = 'Tech and Qual',
    NPD_ADMIN = 'NPD Admin',
    CORP_PROJECT_LEAD = 'Corp Project Leader',
    REGS_TRAFFIC_MANAGER = 'Regs Traffic Manager',
    GFG = 'GFG',
    REGIONAL_TECHNICAL_MANAGER = 'Regional Technical Manager',
    NPD_FPMS = 'FPMS'


}