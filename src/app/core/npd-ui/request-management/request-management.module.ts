import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../material.module';
import { RequestorDashboardComponent } from './requestor-dashboard/requestor-dashboard.component';
import { MpmLibraryModule, CustomSelectModule, FacetModule, PaginationModule, ColumnChooserModule } from 'mpm-library';
import { InboxComponent } from './requestor-dashboard/inbox/inbox.component';
import { InboxAssignmentModalComponent } from './requestor-dashboard/inbox/inbox-assignment-modal/inbox-assignment-modal.component';
import { MpmLibModule } from '../../mpm-lib/mpm-lib.module';
import { RequestManagementRouting } from './request-management-routing';




@NgModule({
  declarations: [RequestorDashboardComponent, InboxComponent, InboxAssignmentModalComponent],
  imports: [
    CommonModule,
    RequestManagementRouting,
    MaterialModule,
    MpmLibraryModule,
    PaginationModule,
    FacetModule,
    MpmLibModule,
    CustomSelectModule,
    ColumnChooserModule
  ]
})
export class RequestManagementModule { }
