import { RequestConstant } from '../constants/request-constant';


interface RequestDashboardFilterObj {
    IS_DEFAULT: boolean;
    //PROJECT_TYPE: ProjectType;
    VALUE: string;
    NAME: string;
    SEARCH_CONDITION: Array<any>;
    IS_PROJECT_TYPE?: boolean;
    IS_SHOWN?:boolean
    TYPE: string;
    VIEW: string;
    IS_INBOX: boolean;
    ACCESS_ROLE: string;
}

export enum RequestDashboardFilterValues {
    MY_REQUESTS = 'MY_REQUESTS',
    ALL_REQUESTS = 'ALL_REQUESTS',
    MY_TASKS = 'MY_TASKS',
    E2E_PM_CLASSIFICATION_TEAM_TASKS = 'E2E_PM_CLASSIFICATION_TEAM_TASKS',
    BUSINESS_DEVELOPMENT_APPROVAL_TEAM_TASKS = 'BUSINESS_DEVELOPMENT_APPROVAL_TEAM_TASKS',
    PROGRAM_MANAGER_APPROVAL_TEAM_TASKS = 'PROGRAM_MANAGER_APPROVAL_TEAM_TASKS',
}

export const RequestDashboardFilters: Array<RequestDashboardFilterObj> = [{
    IS_DEFAULT: false,
    VALUE: RequestDashboardFilterValues.MY_REQUESTS,
    NAME: 'My Requests',
    TYPE: 'myRequest',
    SEARCH_CONDITION: RequestConstant.GET_REQUEST_SEARCH_CONDITION_LIST, 
    IS_SHOWN: true,
    VIEW: 'MY_REQUEST_VIEW',
    IS_INBOX: false,
    ACCESS_ROLE: "All"
}, {
    IS_DEFAULT: true,
    VALUE: RequestDashboardFilterValues.ALL_REQUESTS,
    NAME: 'All Requests',
    TYPE: 'allRequest',
    SEARCH_CONDITION: RequestConstant.GET_REQUEST_SEARCH_CONDITION_LIST,
    IS_SHOWN: true,
    VIEW: 'ALL_REQUEST_VIEW',
    IS_INBOX: false,
    ACCESS_ROLE: "All"
}, {
    IS_DEFAULT: false,
    //PROJECT_TYPE: ProjectTypes.TEMPLATE,
    VALUE: RequestDashboardFilterValues.MY_TASKS,
    NAME: 'My Tasks',
    TYPE: 'myTasks',
    SEARCH_CONDITION: RequestConstant.GET_REQUEST_TASK_SEARCH_CONDITION_LIST, 
    IS_SHOWN: true,
    VIEW: 'REQUEST_TASK_VIEW',
    IS_INBOX: true,
    ACCESS_ROLE: "All"
}
, {
    IS_DEFAULT: false,
    //PROJECT_TYPE: ProjectTypes.TEMPLATE,
    VALUE: RequestDashboardFilterValues.E2E_PM_CLASSIFICATION_TEAM_TASKS,
    NAME: 'E2E PM Classification',
    TYPE: 'e2ePMClassification',
    SEARCH_CONDITION: RequestConstant.GET_REQUEST_TASK_SEARCH_CONDITION_LIST, 
    IS_SHOWN: true,
    VIEW: 'REQUEST_TASK_VIEW',
    IS_INBOX: true,
    ACCESS_ROLE: "E2E PM"
}
, {
    IS_DEFAULT: false,
    //PROJECT_TYPE: ProjectTypes.TEMPLATE,
    VALUE: RequestDashboardFilterValues.PROGRAM_MANAGER_APPROVAL_TEAM_TASKS,
    NAME: 'Program Manager Approval',
    TYPE: 'programManagerApproval',
    SEARCH_CONDITION: RequestConstant.GET_REQUEST_TASK_SEARCH_CONDITION_LIST, 
    IS_SHOWN: true,
    VIEW: 'REQUEST_TASK_VIEW',
    IS_INBOX: true,
    ACCESS_ROLE: "Program Manager"
},
{
    IS_DEFAULT: false,
    VALUE: RequestDashboardFilterValues.BUSINESS_DEVELOPMENT_APPROVAL_TEAM_TASKS,
    NAME: 'Business Development Approval',
    TYPE: 'businessDevlopmentApproval',
    SEARCH_CONDITION: RequestConstant.GET_REQUEST_TASK_SEARCH_CONDITION_LIST, 
    IS_SHOWN: true,
    VIEW: 'REQUEST_TASK_VIEW',
    IS_INBOX: true,
    ACCESS_ROLE: "Business Development"
}

];