
interface RequestDashboardFilterObj {
    IS_DEFAULT: boolean;
    //PROJECT_TYPE: ProjectType;
    VALUE: string;
    NAME: string;
    SEARCH_CONDITION: Array<any>;
    IS_PROJECT_TYPE?: boolean;
    IS_SHOWN?: boolean
    TYPE: string;
    VIEW: string;
    IS_INBOX: boolean;
    ACCESS_ROLE: string;
}

export enum RequestDashboardFilterValues {
    MY_REQUESTS = 'MY_REQUESTS',
    ALL_REQUESTS = 'ALL_REQUESTS',
    MY_TASKS = 'MY_TASKS',
    INTERNATIONAL_MARKETING_TEAM_TASKS = 'INTERNATIONAL_MARKETING_TEAM_TASKS',
    TRAFFIC_TEAM_TASKS = 'TRAFFIC_TEAM_TASKS',
    LOCALIZATION_TEAM_TASKS = 'LOCALIZATION_TEAM_TASKS',
    //LOCALIZATION_PO_TEAM_TASKS = 'LOCALIZATION_PO_TEAM_TASKS',
    QC_REVIEW_TASKS = 'QC_REVIEW_TASKS',
    PROXY_GENERATION = 'PROXY_GENERATION',
    MATERIAL_VENDOR = 'MATERIAL_VENDOR'
}



export const RequestDashboardFilters: Array<RequestDashboardFilterObj> = [{
    IS_DEFAULT: false,
    VALUE: RequestDashboardFilterValues.MY_REQUESTS,
    NAME: 'My Requests',
    TYPE: 'myRequest',
    SEARCH_CONDITION: [],
    IS_SHOWN: true,
    VIEW: 'MY_REQUEST_VIEW',
    IS_INBOX: false,
    ACCESS_ROLE: "Request Access"
}, {
    IS_DEFAULT: false,
    VALUE: RequestDashboardFilterValues.ALL_REQUESTS,
    NAME: 'All Requests',
    TYPE: 'allRequest',
    SEARCH_CONDITION: [],
    IS_SHOWN: true,
    VIEW: 'ALL_REQUEST_VIEW',
    IS_INBOX: false,
    ACCESS_ROLE: "All Request Access"
}, {
    IS_DEFAULT: true,
    //PROJECT_TYPE: ProjectTypes.TEMPLATE,
    VALUE: RequestDashboardFilterValues.MY_TASKS,
    NAME: 'My Tasks',
    TYPE: 'myTasks',
    SEARCH_CONDITION: [],
    IS_SHOWN: true,
    VIEW: 'REQUEST_TASK_VIEW',
    IS_INBOX: true,
    ACCESS_ROLE: "All"
}



];
