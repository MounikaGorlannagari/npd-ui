import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { SearchRequest, ApplicationConfigConstants, LoaderService, SearchDataService, NotificationService, SharingService, SearchChangeEventService, FacetChangeEventService, FieldConfigService, ViewConfigService, IndexerService, UtilService, AppService, FacetConditionList, ConfirmationModalComponent, MPMFieldConstants, SearchResponse, SearchConfigConstants, ViewConfig, MPMSearchOperators, MPMSearchOperatorNames, ExportDataComponent } from 'mpm-library';
import { RouteService } from 'src/app/core/mpm/route.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { RequestDashboardFilters, RequestDashboardFilterValues } from '../filter/request-dashboard-filters';
import { TaskConstant } from '../constants/task-constant';
import { getTaskListDependentFilters } from 'src/app/core/mpm/deliverables-task-view/taskListfilters/TaskListDependentFilter';
import { RequestDashboardControlConstant } from '../constants/request-dashboard-control-constants';
import { InboxAssignmentModalComponent } from './inbox/inbox-assignment-modal/inbox-assignment-modal.component';
import { InboxService } from './inbox/inbox.service';
import { DefaultColumnPreferenceService } from '../../services/default-column-preference.service';
import { MONSTER_ROLES } from '../../filter-configs/role.config';
import { FilterConfigService } from '../../filter-configs/filter-config.service';
import { BusinessConstants } from '../../request-view/constants/BusinessConstants';
import { I } from '@angular/cdk/keycodes';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { takeUntil } from 'rxjs/operators'
import { InboxComponent } from './inbox/inbox.component';
import { TechnicalRequestService } from '../../request-view/services/technical-request.service';
import { RequestsCopyConstants } from '../../request-view/constants/RequestsCopyConstants';
import { NPDFacetService } from 'src/app/core/mpm-lib/facet/facet.service';
@Component({
    selector: 'app-requestor-dashboard',
    templateUrl: './requestor-dashboard.component.html',
    styleUrls: ['./requestor-dashboard.component.scss'],
    providers: []
})
export class RequestorDashboardComponent implements OnInit {
    private unsubscribe$: Subject<any> = new Subject<any>();
    mobileQuery: MediaQueryList;
    private mobileQueryListener: () => void;
    displayableFields: any[];
    columnChooserFields: any;
    userPreference: { USER_ID: any; PREFERENCE_TYPE: string; VIEW_ID: any; FIELD_DATA: any[]; };
    pageNumber: any;
    //isCommercial;
    constructor(
        private technicalRequestService: TechnicalRequestService,
        private loaderService: LoaderService,
        private routeService: RouteService,
        private searchDataService: SearchDataService,
        private changeDetectorRef: ChangeDetectorRef,
        private media: MediaMatcher,
        private notificationService: NotificationService,
        private sharingService: SharingService,
        private searchChangeEventService: SearchChangeEventService,
        private facetChangeEventService: FacetChangeEventService,
        private fieldConfigService: FieldConfigService,
        private viewConfigService: ViewConfigService,
        private indexerService: IndexerService,
        private utilService: UtilService,
        private appService: AppService,
        private sharingservice: SharingService,
        private inboxService: InboxService,
        private dialog: MatDialog,
        private route: ActivatedRoute,
        private defaultColumnPreferenceService: DefaultColumnPreferenceService,
        private filterConfigService: FilterConfigService,
        private monsterConfigApiService: MonsterConfigApiService,
        private facetService: NPDFacetService
    ) {
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
    }
    @ViewChild('inbox') inboxComponent: InboxComponent;

    isCopyRequest: boolean
    dashboardMenuConfig;
    searchConditions = [];
    //pageSize = 10;
    page = 1;
    skip = 0;
    // top = 10;
    pageSize: number;
    top: number;
    tempsearchConditionsSubscribe: any;
    searchConfigId = -1;

    facetExpansionState = false;
    facetRestrictionList: FacetConditionList;
    requestConfig: any;
    appConfig: any;
    optionDropdown = false;
    userPreferenceFreezeCount;
    isColumnsFreezable = true;
    showInboxControls = false;
    taskOperations = {
        enableClaim: false,
        enableRevoke: false,
        enableAssign: false,
        enableDelegate: false
    }
    selectedTasksStates = [];
    selectedTasksID = [];
    selectedTasksAssignee = [];
    selectedTasksDelegatedTo = [];
    selectedTasksSubject = [];
    selectedTasks = [];
    enableClaim = false;
    enableRevoke = false;
    enableAssign = false;
    enableDelegate = false;
    APP_ID = '';
    isNotificationAdmin = false;
    userRoles = [];
    notificationAdminRoleName = "Notification Admin";
    canCreateRequest = false;
    menu;
    listDependentFilters = [];
    selectedListDependentFilter;
    defaultViewFilterName;
    hasPagination = null;
    selectedSortOption;
    selectedSortOrder;
    exportDataSearchRequest: SearchRequest;

    REQUEST_DETAILS_METHOD_NS = 'http://schemas/NPDLookUpDomainModel/Request_Type/operations';
    GET_REQUEST_DETAILS_WS_METHOD_NAME = 'GetActiveRequestTypes';


    facetToggled(event) {
        this.facetExpansionState = event;
        if (event) {
            // this.refreshRequest();
        }
    }

    exportData() {
        // this.previousCampaignRequest = null;
        // this.previousProjectRequest = null;
        // this.selectedProjectCounts = 0;
        let mapperNames: String = "";
        let index = 0;
        this.displayableFields.forEach(field => {
            if (field.MAPPER_NAME.length > 0) {
                mapperNames = mapperNames.concat(field.MAPPER_NAME);
                if (index != (this.columnChooserFields.length - 1)) {
                    mapperNames = mapperNames.concat(",");
                }
            }
            index++;
        });
        // this.projectBulkCountService.getProjectBulkCount(true);
        const dialogRef = this.dialog.open(ExportDataComponent, {
            width: '40%',
            disableClose: true,
            data:
            {
                // exportData: this.exportTableData,
                searchRequest: this.exportDataSearchRequest,
                columns: mapperNames,
                totalCount: this.dashboardMenuConfig.totalProjectCount
            }
        });
    }

    resetView() {
        sessionStorage.removeItem('USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW);
        const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
        const userPreferenceId = userPreference && userPreference['MPM_User_Preference-id'] && userPreference['MPM_User_Preference-id'].Id ?
            userPreference['MPM_User_Preference-id'].Id : null;
        const defaultPreferenceView = this.defaultColumnPreferenceService.getDefaultPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
        let isDefaultPreference = false;
        if (userPreferenceId) {
            this.loaderService.show();
            this.appService.deleteUserPreference(userPreferenceId).subscribe(response => {
                this.loaderService.hide();
                if (response) {
                    this.notificationService.success('User preference have been reset successfully');
                }
            });

        } else {
            if (defaultPreferenceView) {
                isDefaultPreference = true;
            }
            this.notificationService.success('User preference have been reset successfully');
        }

        this.resetCurrentView(isDefaultPreference);
    }

    resetToDefaultView(currentDefaultViewFieldData) {
        /*   currentDefaultViewFieldData.sort((a, b) => {
              const firstKey: any = Object.keys(a);
              const firstValue = a[firstKey];
              const secondKey: any = Object.keys(b);
              const secondValue = b[secondKey];
              return firstValue.POSITION - secondValue.POSITION;
          }); */
        const displayableFields = [];
        const listFieldOrder = [];
        const nonDisplayableFields = [];
        currentDefaultViewFieldData.forEach(viewField => {
            const viewFieldId = Object.keys(viewField)[0];
            if (viewField[viewFieldId].IS_DISPLAY === true) {
                const selectedField = this.dashboardMenuConfig.projectViewConfig.displayFields.find(field => field.INDEXER_FIELD_ID === viewFieldId);
                if (selectedField) {
                    selectedField.width = viewField[viewFieldId].WIDTH;
                    displayableFields.push(selectedField);
                    listFieldOrder.push(selectedField.MAPPER_NAME);
                }
            }
            else {
                const selectedField = this.dashboardMenuConfig.projectViewConfig.displayFields.find(field => field.INDEXER_FIELD_ID === viewFieldId);
                if (selectedField) {
                    selectedField.width = viewField[viewFieldId].WIDTH;
                    nonDisplayableFields.push(selectedField);
                    //listFieldOrder.push(selectedField.MAPPER_NAME);
                }
            }
        });

        this.dashboardMenuConfig.projectViewConfig.listFieldOrder = this.dashboardMenuConfig.projectViewConfig.LIST_FIELD_ORDER.toString();
        const order = this.dashboardMenuConfig.projectViewConfig.listFieldOrder.split(",");
        //filtering out non-displayable element list from all fields list
        let defaultDisplayableFields = this.dashboardMenuConfig.projectViewConfig.displayFields.filter(elm => !nonDisplayableFields.map(elm => JSON.stringify(elm)).includes(JSON.stringify(elm)));
        this.dashboardMenuConfig.projectViewConfig.displayFields.sort(function (a, b) {
            return order.indexOf(a.MAPPER_NAME) - order.indexOf(b.MAPPER_NAME);
        });
        if (defaultDisplayableFields) {
            defaultDisplayableFields.sort(function (a, b) {
                return order.indexOf(a.MAPPER_NAME) - order.indexOf(b.MAPPER_NAME);
            })
            this.displayableFields = defaultDisplayableFields;//displayableFields;
            this.columnChooserFields = this.formColumnChooserField(defaultDisplayableFields);//displayableFields;
        } else {
            this.displayableFields = this.dashboardMenuConfig.projectViewConfig.displayFields;
            this.columnChooserFields = this.formColumnChooserField(this.dashboardMenuConfig.projectViewConfig.displayFields);
        }
    }

    resetCurrentView(defaultSystemPreference?) {
        if (defaultSystemPreference) {
            this.dashboardMenuConfig.projectViewConfig.displayFields.forEach(displayField => {
                displayField.width = '';
            });
            const defaultSystemPreference = this.defaultColumnPreferenceService.getsystemDefaultPreference();
            const defaultPreferenceView = this.defaultColumnPreferenceService.getDefaultPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
            const currentDefaultViewFieldData = defaultPreferenceView && !this.utilService.isNullOrEmpty(defaultPreferenceView.FIELD_DATA) ? JSON.parse(defaultPreferenceView.FIELD_DATA).data : null;
            this.resetToDefaultView(currentDefaultViewFieldData);
            const userPreferenceData = this.formViewData();
            this.setCurrentViewData(userPreferenceData);
            this.defaultViewFilterName = null;
            this.refreshRequest();
        }
        else {
            this.dashboardMenuConfig.projectViewConfig.displayFields.forEach(displayField => {
                displayField.width = '';
            });
            this.userPreferenceFreezeCount = MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT;
            this.dashboardMenuConfig.projectViewConfig.listFieldOrder = this.dashboardMenuConfig.projectViewConfig.LIST_FIELD_ORDER;
            this.displayableFields = this.dashboardMenuConfig.projectViewConfig.displayFields;
            this.columnChooserFields = this.formColumnChooserField(this.dashboardMenuConfig.projectViewConfig.displayFields);
            const userPreferenceData = this.formViewData();
            this.setCurrentViewData(userPreferenceData);
            this.defaultViewFilterName = null;
            this.refreshRequest();
        }
    }

    setViewData(userPreference) {
        sessionStorage.setItem('USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW, userPreference);
    }

    setCurrentViewData(userPreference) {
        sessionStorage.setItem('CURRENT_USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW, userPreference);
    }

    formViewData() {
        const userPreference = {
            USER_ID: this.sharingService.getCurrentPerson().Id,
            PREFERENCE_TYPE: 'VIEW',
            VIEW_ID: this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id,
            FIELD_DATA: [],
            FREEZE_COLUMN_COUNT: 0,

        };
        this.dashboardMenuConfig.projectViewConfig.displayFields.forEach(viewField => {
            if (viewField.INDEXER_FIELD_ID) {
                const fieldIndex = this.displayableFields.findIndex(displayableField => displayableField.INDEXER_FIELD_ID === viewField.INDEXER_FIELD_ID);
                userPreference.FIELD_DATA.push({
                    [viewField.INDEXER_FIELD_ID]: {
                        POSITION: fieldIndex >= 0 ? fieldIndex : null,
                        WIDTH: viewField && viewField.width ? viewField.width : '',
                        IS_DISPLAY: fieldIndex >= 0
                    }
                });
            }
        });
        if (this.userPreferenceFreezeCount === undefined) {
            userPreference.FREEZE_COLUMN_COUNT =
                MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT;
        }
        userPreference.FREEZE_COLUMN_COUNT = this.userPreferenceFreezeCount;
        return JSON.stringify(userPreference);
    }

    updateDisplayColumns(displayColumnFields) {
        this.displayableFields = [];
        const listFieldOrder = [];
        displayColumnFields.column.sort((a, b) => a.POSITION - b.POSITION);
        displayColumnFields.column.forEach(displayColumnField => {
            if (displayColumnField.selected === true) {
                const selectedField = this.dashboardMenuConfig.projectViewConfig.displayFields.find(field => field.INDEXER_FIELD_ID === displayColumnField.INDEXER_FIELD_ID);
                if (selectedField) {
                    this.displayableFields.push(selectedField);
                    listFieldOrder.push(selectedField.MAPPER_NAME);
                }
            }
        });
        this.userPreferenceFreezeCount = displayColumnFields.frezeCount;
        this.dashboardMenuConfig.projectViewConfig.freezeCount = displayColumnFields.frezeCount;
        this.dashboardMenuConfig.projectViewConfig.listFieldOrder = listFieldOrder.toString();
        const userPreferenceData = this.formViewData();
        this.setViewData(userPreferenceData);
        this.setCurrentViewData(userPreferenceData);
        //   this.facetService.updateOrderdDisplayableFields(this.displayableFields);
        this.refreshRequest();
    }

    formColumnChooserField(listFields) {
        const columnChooserFields = [];
        this.dashboardMenuConfig.projectViewConfig.displayFields.forEach((displayField, index) => {
            const selectedFieldIndex = listFields.findIndex(listField => listField.INDEXER_FIELD_ID === displayField.INDEXER_FIELD_ID);
            if (selectedFieldIndex >= 0) {
                columnChooserFields.push({
                    INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
                    DISPLAY_NAME: displayField.DISPLAY_NAME,
                    POSITION: selectedFieldIndex,
                    selected: true
                });
            } else {
                columnChooserFields.push({
                    INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
                    DISPLAY_NAME: displayField.DISPLAY_NAME,
                    POSITION: index,
                    selected: false
                });
            }
        });
        return columnChooserFields;
    }

    getOrderedDisplayableFields(displayableFields) {
        this.columnChooserFields = this.formColumnChooserField(displayableFields);
        this.updateDisplayColumns({ 'column': this.columnChooserFields, 'frezeCount': this.userPreferenceFreezeCount });
        this.facetService.updateOrderdDisplayableFields(displayableFields);
        //   this.updateDisplayColumns({'column':this.columnChooserFields,'frezeCount':4});
    }

    resizeDisplayableFields() {
        const userPreferenceData = this.formViewData();
        this.setViewData(userPreferenceData);
        this.setCurrentViewData(userPreferenceData);
    }

    setUserPreference() {
        let userPreferenceSessionData: any = sessionStorage.getItem('USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW);
        userPreferenceSessionData = userPreferenceSessionData ? JSON.parse(userPreferenceSessionData) : null;
        const userPreferenceViewField = userPreferenceSessionData && userPreferenceSessionData.FIELD_DATA ? userPreferenceSessionData.FIELD_DATA : null;
        const defaultSystemPreference = this.defaultColumnPreferenceService.getsystemDefaultPreference();
        const defaultPreferenceView = this.defaultColumnPreferenceService.getDefaultPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
        if (userPreferenceViewField) {
            userPreferenceViewField.sort((a, b) => {
                const firstKey: any = Object.keys(a);
                const firstValue = a[firstKey];
                const secondKey: any = Object.keys(b);
                const secondValue = b[secondKey];
                return firstValue.POSITION - secondValue.POSITION;
            });
            const displayableFields = [];
            const listFieldOrder = [];
            userPreferenceViewField.forEach(viewField => {
                const viewFieldId = Object.keys(viewField)[0];
                if (viewField[viewFieldId].IS_DISPLAY === true) {
                    const selectedField = this.dashboardMenuConfig.projectViewConfig.displayFields.find(field => field.INDEXER_FIELD_ID === viewFieldId);
                    if (selectedField) {
                        selectedField.width = viewField[viewFieldId].WIDTH;
                        displayableFields.push(selectedField);
                        listFieldOrder.push(selectedField.MAPPER_NAME);
                    }
                }
            });
            if (defaultPreferenceView) {
                this.dashboardMenuConfig.projectViewConfig.listFieldOrder = this.dashboardMenuConfig.projectViewConfig.LIST_FIELD_ORDER.toString();
            } else {
                this.dashboardMenuConfig.projectViewConfig.listFieldOrder = listFieldOrder.toString();
            }
            const order = this.dashboardMenuConfig.projectViewConfig.listFieldOrder.split(",");
            this.dashboardMenuConfig.projectViewConfig.displayFields.sort(function (a, b) {
                return order.indexOf(a.MAPPER_NAME) - order.indexOf(b.MAPPER_NAME);
            })
            this.displayableFields = displayableFields;
            this.columnChooserFields = this.formColumnChooserField(displayableFields);
            this.userPreferenceFreezeCount =
                userPreferenceSessionData.FREEZE_COLUMN_COUNT;
        } else {
            const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
            const currentViewFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
            const currentDefaultViewFieldData = defaultPreferenceView && !this.utilService.isNullOrEmpty(defaultPreferenceView.FIELD_DATA) ? JSON.parse(defaultPreferenceView.FIELD_DATA).data : null;
            this.dashboardMenuConfig.projectViewConfig.freezeCount =
                this.userPreferenceFreezeCount;
            if (currentViewFieldData?.data && this.utilService.getBooleanValue(userPreference.IS_LIST_VIEW)) {
                this.userPreferenceFreezeCount =
                    currentViewFieldData.FREEZE_COLUMN_COUNT;
                currentViewFieldData.data.sort((a, b) => {
                    const firstKey: any = Object.keys(a);
                    const firstValue = a[firstKey];
                    const secondKey: any = Object.keys(b);
                    const secondValue = b[secondKey];
                    return firstValue.POSITION - secondValue.POSITION;
                });
                const displayableFields = [];
                const listFieldOrder = [];
                currentViewFieldData.data.forEach(viewField => {
                    const viewFieldId = Object.keys(viewField)[0];
                    if (viewField[viewFieldId].IS_DISPLAY === true) {
                        const selectedField = this.dashboardMenuConfig.projectViewConfig.displayFields.find(field => field.INDEXER_FIELD_ID === viewFieldId);
                        if (selectedField) {
                            selectedField.width = viewField[viewFieldId].WIDTH;
                            displayableFields.push(selectedField);
                            listFieldOrder.push(selectedField.MAPPER_NAME);
                        }
                    }
                });
                this.dashboardMenuConfig.projectViewConfig.listFieldOrder = listFieldOrder.toString();
                this.displayableFields = displayableFields;
                this.columnChooserFields = this.formColumnChooserField(displayableFields);
            } else if (defaultSystemPreference && defaultPreferenceView) {
                this.resetToDefaultView(currentDefaultViewFieldData);
            } else {
                this.dashboardMenuConfig.projectViewConfig.listFieldOrder = this.dashboardMenuConfig.projectViewConfig.LIST_FIELD_ORDER;
                const order = this.dashboardMenuConfig.projectViewConfig.listFieldOrder.split(",");
                this.dashboardMenuConfig.projectViewConfig.displayFields.sort(function (a, b) {
                    return order.indexOf(a.MAPPER_NAME) - order.indexOf(b.MAPPER_NAME);
                })
                this.displayableFields = this.dashboardMenuConfig.projectViewConfig.displayFields;
                this.columnChooserFields = this.formColumnChooserField(this.dashboardMenuConfig.projectViewConfig.displayFields);
                this.userPreferenceFreezeCount =
                    MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT;
            }
        }
        this.facetService.updateOrderdDisplayableFields(this.displayableFields);
    }

    setCurrentView() {
        const currentViewData = {
            sortName: this.dashboardMenuConfig.selectedSortOption.option.indexerId,
            sortOrder: this.dashboardMenuConfig.selectedSortOption.sortType,
            pageNumber: this.pageNumber,
            pageSize: this.pageSize,
            isListView: this.dashboardMenuConfig.IS_LIST_VIEW,
            viewId: this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id,
            viewName: this.dashboardMenuConfig.projectViewConfig.VIEW,
            listfilter: this.dashboardMenuConfig.selectedViewByName,
            isPMView: true
        }
        sessionStorage.setItem('CURRENT_VIEW', JSON.stringify(currentViewData));
    }

    setDefaultView() {

        const menu = this.sharingService.getCurrentMenu();
        const defaultView = this.sharingService.getDefaultCurrentUserPreferenceByMenu(menu['MPM_Menu-id'].Id);
        if (defaultView || this.defaultViewFilterName) {
            if (defaultView.FILTER_NAME !== this.dashboardMenuConfig.selectedViewByName ||
                this.defaultViewFilterName !== this.dashboardMenuConfig.selectedViewByName) {
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                    width: '40%',
                    disableClose: true,
                    data: {
                        message: 'Are you sure you want to override default view?<br /> <b>Current Default View: </b>' + defaultView.FILTER_NAME,
                        submitButton: 'Yes',
                        cancelButton: 'No'
                    }
                });
                dialogRef.afterClosed().subscribe(result => {
                    if (result && result.isTrue) {
                        this.appService.updateDefaultUserPreference(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id, this.dashboardMenuConfig.selectedViewByName, menu['MPM_Menu-id'].Id, null).
                            subscribe(response => {
                                if (response) {
                                    this.notificationService.success('Default view has been saved successfully');
                                    this.defaultViewFilterName = this.dashboardMenuConfig.selectedViewByName;
                                } else {
                                    this.notificationService.error('Something went wrong while updating default view');
                                }
                            });
                    }
                });
            }
        } else {
            this.appService.updateDefaultUserPreference(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id, this.dashboardMenuConfig.selectedViewByName, menu['MPM_Menu-id'].Id, null).
                subscribe(response => {
                    if (response) {
                        this.notificationService.success('Default view has been saved successfully');
                        this.defaultViewFilterName = this.dashboardMenuConfig.selectedViewByName;
                    } else {
                        this.notificationService.error('Something went wrong while updating default view');
                    }
                });
        }
    }

    setPageSetting(viewId?) {
        const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(viewId ? viewId : this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
        const userPreferenceFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
        if (userPreferenceFieldData) {
            this.pageSize = userPreferenceFieldData.pageSize;
            this.top = this.pageSize;
            this.pageNumber = userPreferenceFieldData.pageNumber;
            this.page = this.pageNumber
            this.skip = (this.pageNumber - 1) * this.pageSize;
            //this.isdefaultPagination = false;
            return true;
        }
        return false;
    }

    openRequestDetails(requestDetails) {
        let params = {
            menu: this.dashboardMenuConfig.selectedDashboardFilter.TYPE,
            listDependentFilter: null
        }
        if (this.selectedListDependentFilter && this.selectedListDependentFilter.value) {
            params.listDependentFilter = this.selectedListDependentFilter.value;
        }
        let requestId;
        let requestType;
        let requestSubjectType;

        if (this.dashboardMenuConfig.selectedDashboardFilter.TYPE === 'programManagerApproval'
            || this.dashboardMenuConfig.selectedDashboardFilter.TYPE === 'e2ePMClassification'
            || this.dashboardMenuConfig.selectedDashboardFilter.TYPE === 'myTasks'
            || this.dashboardMenuConfig.selectedDashboardFilter.TYPE === 'businessDevlopmentApproval') {//|| this.dashboardMenuConfig.selectedDashboardFilter.TYPE === 'businessDevApproval'
            params["taskId"] = requestDetails.request.RT_PARENT_TASK_ID;
            requestId = requestDetails.request.RT_REQUEST_ENTITY_ITEM_ID;
            requestType = requestDetails.request.RT_REQUEST_TYPE && requestDetails.request.RT_REQUEST_TYPE.toLowerCase();
            requestSubjectType = requestDetails.request.RT_TASK_SUBJECT;
            //params["requestSubjectType"] = requestDetails.request.RT_TASK_SUBJECT;
            // requestDetails.request.ITEM_ID
        }
        else if (this.dashboardMenuConfig.selectedDashboardFilter.TYPE === 'materialVendor') {
            params["taskId"] = requestDetails.request.RT_PARENT_TASK_ID;
            requestId = requestDetails.request.RT_MATERIAL_PLANNER_REQUEST_ITEM_ID;
            requestType = requestDetails.request.RT_REQUEST_TYPE_VALUE;
            requestSubjectType = requestDetails.request.RT_SUBJECT;
        }
        else {
            requestId = requestDetails.request.RQ_REQUEST_ENTITY_ITEM_ID;//'005056AACEB9A1EBA6DD27D3623FE821.327732';
            requestType = (requestDetails.request.RQ_REQUEST_TYPE) && (requestDetails.request.RQ_REQUEST_TYPE).toLowerCase();
        }
        //this.routeService.goToRequestDetailView(requestDetails.requestId, params);
        this.routeService.goToRequestDetailView(requestId, params, requestType, requestSubjectType);

    }

    pagination(pagination: any) {
        this.pageSize = pagination.pageSize;
        this.top = pagination.pageSize;
        this.skip = (pagination.pageIndex * pagination.pageSize);
        this.page = 1 + pagination.pageIndex;
        this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_PAGE_LIMIT] = (pagination.pageSize).toString();
        this.pageNumber = this.page;
        this.loaderService.show();
        this.handleRequestServiceCall();
        this.setCurrentView();
    }

    defaultPagination() {
        this.top = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.pageSize = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.skip = 0;
        this.page = 1;
        this.pageNumber = this.page;
        this.setCurrentView();
    }

    onDashBoardFilterChange(event) {
        this.facetRestrictionList = {};
        this.facetChangeEventService.resetFacet();
        this.dashboardMenuConfig.projectDashBoadFilters.map(filter => {
            if (filter.VALUE === event.currentTarget.value) {
                this.dashboardMenuConfig.selectedViewByName = filter.NAME;
                this.dashboardMenuConfig.selectedDashboardFilter = filter;
                this.getFilters(true);
                this.dashboardMenuConfig.searchCondition = this.formFilterCondition(filter.VALUE, filter.SEARCH_CONDITION);
                this.dashboardMenuConfig.projectViewName = filter.VIEW
                if (filter.IS_INBOX) {
                    this.showInboxControls = true;
                }
                else {
                    this.showInboxControls = false;
                }
            }
        });
        this.defaultPagination();
        this.loaderService.show();
        this.getMPMDataFields().subscribe(data => {
            this.updateSearchConfigId();
            this.handleRequestServiceCall();
        }, mpmFieldError => {
            this.loaderService.hide();
            this.notificationService.error(mpmFieldError);
        });
    }


    onSelectionChange(event, field) {
        if (!field || !event) {
            return;
        }
        this.isCopyRequest = RequestsCopyConstants.COPY_REQUEST_FALSE;
        this.technicalRequestService.setCopyCondtion(this.isCopyRequest);
        if (field.fieldName === 'newRequestType') {
            if (event.currentTarget.value === BusinessConstants.TECHNICAL_REQUEST) {
                this.routeService.goToRequestView((event.currentTarget.value).toLowerCase(), this.dashboardMenuConfig.selectedDashboardFilter.TYPE);
                return;
            } else if (event.currentTarget.value === BusinessConstants.COMMERCIAL_REQUEST) {
                this.routeService.goToRequestView((event.currentTarget.value).toLowerCase(), this.dashboardMenuConfig.selectedDashboardFilter.TYPE);
                return;
            } else if (event.currentTarget.value === BusinessConstants.OPERATIONAL_REQUEST) {
                this.routeService.goToRequestView((event.currentTarget.value).toLowerCase(), this.dashboardMenuConfig.selectedDashboardFilter.TYPE);
                return;
            }
        }
    }

    copyProject(requestDetails) {
        this.isCopyRequest = RequestsCopyConstants.COPY_REQUEST_TRUE;
        this.technicalRequestService.setCopyCondtion(this.isCopyRequest);
        this.technicalRequestService.sendData(requestDetails.request.ITEM_ID);
        if (requestDetails.request.RQ_REQUEST_TYPE.toUpperCase() === BusinessConstants.TECHNICAL_REQUEST) {
            this.routeService.goToRequestView((requestDetails.request.RQ_REQUEST_TYPE).toLowerCase(), this.dashboardMenuConfig.selectedDashboardFilter.TYPE);
            return;
        } else if (requestDetails.request.RQ_REQUEST_TYPE.toUpperCase() === BusinessConstants.COMMERCIAL_REQUEST) {
            this.routeService.goToRequestView((requestDetails.request.RQ_REQUEST_TYPE).toLowerCase(), this.dashboardMenuConfig.selectedDashboardFilter.TYPE);
            return;
        } else if (requestDetails.request.RQ_REQUEST_TYPE.toUpperCase() === BusinessConstants.OPERATIONAL_REQUEST) {
            this.routeService.goToRequestView((requestDetails.request.RQ_REQUEST_TYPE).toLowerCase(), this.dashboardMenuConfig.selectedDashboardFilter.TYPE);
            return;
        }
    }

    refreshRequest() {
        this.loaderService.show();
        this.handleRequestServiceCall();
        //   this.updateDisplayColumns({'column':this.columnChooserFields,'frezeCount':0});

    }

    //bulk delete from grid view
    bulkDelete() {
        if (this.inboxComponent) {
            this.inboxComponent.deletedMultipleReq();
        }
    }
    getAllRequests(): Observable<any> {
        return new Observable(observer => {
            this.dashboardMenuConfig.projectData = [];
            let keyWord = '';
            let sortTemp = [];
            if (this.dashboardMenuConfig.selectedSortOption) {
                const sortOption = this.dashboardMenuConfig.projectSortOptions.find(option => option.name === this.dashboardMenuConfig.selectedSortOption.option.name);
                sortTemp = sortOption ? [{
                    field_id: sortOption.indexerId,
                    order: this.dashboardMenuConfig.selectedSortOption.sortType
                }] : [];
            }
            let searchConditions = this.dashboardMenuConfig.searchCondition;
            if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
                keyWord = this.searchConditions[0].keyword;
            } else if (this.searchConditions && this.searchConditions.length > 0) {
                searchConditions = searchConditions.concat(this.searchConditions);
            }
            const parameters: SearchRequest = {
                search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
                keyword: keyWord,
                search_condition_list: {
                    search_condition: searchConditions
                },
                facet_condition_list: {
                    facet_condition: (this.facetRestrictionList
                        && this.facetRestrictionList.facet_condition) ? this.facetRestrictionList.facet_condition : []
                },
                sorting_list: {
                    sort: sortTemp
                },
                cursor: {
                    page_index: this.skip,
                    page_size: this.top
                }
            };
            this.exportDataSearchRequest = parameters;
            this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
                this.dashboardMenuConfig.projectData = response.data;
                this.dashboardMenuConfig.totalProjectCount = response.cursor.total_records;
                this.facetService.updateOrderdDisplayableFields(this.displayableFields);
                this.facetChangeEventService.updateFacetFilter(response.facet_field_response_list ? response.facet_field_response_list : []);
                observer.next(true);
                observer.complete();
            }, error => {
                this.loaderService.hide();
                observer.error('Something went wrong while getting requests.');
            });
        });
    }

    getMPMDataFields(): Observable<any> {
        return new Observable(observer => {
            let viewConfig;

            if (this.dashboardMenuConfig.projectViewName || this.dashboardMenuConfig.templateViewName) {
                viewConfig = this.dashboardMenuConfig.IS_PROJECT_TYPE ? this.dashboardMenuConfig.projectViewName ? this.dashboardMenuConfig.projectViewName : SearchConfigConstants.SEARCH_NAME.PROJECT : this.dashboardMenuConfig.templateViewName ? this.dashboardMenuConfig.templateViewName : SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE; //this.sharingService.getViewConfigById(this.dashboardMenuConfig.projectViewId).VIEW;
            } else {
                viewConfig = this.dashboardMenuConfig.IS_PROJECT_TYPE ? SearchConfigConstants.SEARCH_NAME.PROJECT : SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE;
            }
            this.viewConfigService.getAllDisplayFeildForViewConfig(
                viewConfig).subscribe((viewDetails: ViewConfig) => {
                    this.updateViewDetailsBasedonViewChange(viewDetails);
                    observer.next(true);
                    observer.complete();
                }, metadataError => {
                    observer.error(metadataError);
                });
        });
    }

    changeView(isListView: boolean) {
        this.dashboardMenuConfig.IS_LIST_VIEW = isListView;
        this.updateViewDetailsBasedonViewChange(this.dashboardMenuConfig.projectViewConfig);
    }

    updateViewDetailsBasedonViewChange(viewDetails: ViewConfig) {
        if (!this.dashboardMenuConfig.projectViewConfig || viewDetails.VIEW !== this.dashboardMenuConfig.projectViewConfig.VIEW) {
            this.dashboardMenuConfig.projectViewConfig = viewDetails;
            this.updateSearchConfigId();
        }
        const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
        const userPreferenceFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
        this.dashboardMenuConfig.projectViewConfig = viewDetails;
        this.dashboardMenuConfig.projectViewConfig.displayFields = viewDetails.R_PM_LIST_VIEW_MPM_FIELDS
        this.columnChooserFields = this.formColumnChooserField(viewDetails.R_PM_LIST_VIEW_MPM_FIELDS)
        this.dashboardMenuConfig.projectViewConfig.listFieldOrder = viewDetails.LIST_FIELD_ORDER;
        this.dashboardMenuConfig.metaDataFields = this.fieldConfigService.getFieldsbyFeildCategoryLevel('REQUEST');
        const dataSet = this.fieldConfigService.formrequiredDataForLoadView(viewDetails, this.dashboardMenuConfig.IS_LIST_VIEW);
        const compDataSet = this.fieldConfigService.formrequiredDataForLoadView(viewDetails, !this.dashboardMenuConfig.IS_LIST_VIEW);

        let arrayList = this.dashboardMenuConfig.IS_LIST_VIEW ? viewDetails.R_PM_LIST_VIEW_MPM_FIELDS : viewDetails.R_PM_CARD_VIEW_MPM_FIELDS;
        arrayList = arrayList ? arrayList : [];
        this.setUserPreference();

        this.dashboardMenuConfig.projectSortOptions = dataSet.SORTABLE_FIELDS;
        this.dashboardMenuConfig.projectSearchableFields = dataSet.SEARCHABLE_FIELDS;
        let existingSortColumn: boolean;
        const sortInput = compDataSet.SORTABLE_FIELDS.find(option => option.displayName === this.selectedSortOption);
        if (sortInput === undefined) {
            existingSortColumn = true;
        } else {
            existingSortColumn = false;
        }
        const sortableFields = existingSortColumn === false ? compDataSet.SORTABLE_FIELDS : dataSet.SORTABLE_FIELDS;
        let isDefaultSort = true;
        if (sortableFields.length > 0) {
            let sortOptions = {
                option: sortableFields[0],
                //sortType: MatSortDirections.ASC
                sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc'
            };
            sortOptions = dataSet.DEFAULT_SORT_FIELD ? dataSet.DEFAULT_SORT_FIELD : sortOptions;

            if (this.selectedSortOrder || this.selectedSortOption) {
                const sortData = sortableFields.find(option => option.displayName === this.selectedSortOption);
                const selectedSort = {
                    option: sortData,
                    sortType: this.selectedSortOrder ? this.selectedSortOrder : sortOptions.sortType
                };
                isDefaultSort = false;
                this.dashboardMenuConfig.selectedSortOption = sortData ? JSON.parse(JSON.stringify(selectedSort)) : JSON.parse(JSON.stringify(sortOptions));
                this.dashboardMenuConfig.sortFieldName = this.dashboardMenuConfig.selectedSortOption.option.name;
                this.dashboardMenuConfig.sortOrder = this.dashboardMenuConfig.selectedSortOption.sortType;
            } else {
                if (userPreferenceFieldData) {
                    userPreferenceFieldData.data.forEach(field => {
                        const fieldId = Object.keys(field)[0];
                        if (field[fieldId].IS_SORTABLE === true) {
                            const selectedField = sortableFields.find(field => field.indexerId === fieldId);
                            if (selectedField) {
                                const selectedSort = {
                                    option: selectedField,
                                    sortType: field[fieldId].SORT_ORDER ? field[fieldId].SORT_ORDER : (this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc')
                                };
                                isDefaultSort = false;
                                this.dashboardMenuConfig.selectedSortOption = JSON.parse(JSON.stringify(selectedSort));
                                this.dashboardMenuConfig.sortFieldName = this.dashboardMenuConfig.selectedSortOption.option.name;
                                this.dashboardMenuConfig.sortOrder = this.dashboardMenuConfig.selectedSortOption.sortType;
                            }
                        }
                    });
                }
            }
            if (isDefaultSort) {
                this.dashboardMenuConfig.selectedSortOption = JSON.parse(JSON.stringify(sortOptions));
                this.dashboardMenuConfig.sortFieldName = this.dashboardMenuConfig.selectedSortOption.option.name;
                this.dashboardMenuConfig.sortOrder = this.dashboardMenuConfig.selectedSortOption.sortType;
            }
        }
        if (!this.hasPagination) {
            this.setPageSetting();
        }
        this.setCurrentView();
        const userPreferenceData = this.formViewData();
        this.setCurrentViewData(userPreferenceData);
    }

    initializeSearchCondition(filter) {
        this.dashboardMenuConfig.selectedViewByName = filter.NAME;
        this.dashboardMenuConfig.selectedDashboardFilter = filter;
        this.getFilters();
        this.dashboardMenuConfig.searchCondition = this.formFilterCondition(filter.VALUE, filter.SEARCH_CONDITION);
        this.dashboardMenuConfig.projectViewName = filter.VIEW
        if (filter.IS_INBOX) {
            this.showInboxControls = true;
        }
    }

    initializeRequestFilters() {
        this.dashboardMenuConfig.projectDashBoadFilters = RequestDashboardFilters;
        this.dashboardMenuConfig.projectDashBoadFilters.forEach(filter => {
            if (this.menu) {
                if (this.menu == filter.TYPE) {
                    this.initializeSearchCondition(filter)
                }
            }
            else if (filter.IS_DEFAULT) {
                this.initializeSearchCondition(filter)
            }
        });
    }
    // Adding Search Conditions to the filter
    formFilterCondition(filterValues: any, defaultSearchCondition: any): Array<any> {
        const searchCondition = this.fieldConfigService.formIndexerIdFromMapperId(defaultSearchCondition);
        /* condition = this.fieldConfigService.createConditionObject(RequestConstant.CATEGORY_LEVEL_REQUEST,
            MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, RequestConstant.CATEGORY_LEVEL_REQUEST);
        if (condition) {
            searchCondition.push(condition);
        } */
        if (filterValues === RequestDashboardFilterValues.ALL_REQUESTS) {

            let statusCondition1 = this.fieldConfigService.createConditionObject('REQUEST_STATUS',
                MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, 'Draft');

            let statusCondition2 = this.fieldConfigService.createConditionObject('REQUEST_STATUS',
                MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, 'Planning');

            let statusCondition3 = this.fieldConfigService.createConditionObject('REQUEST_STATUS',
                MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, 'Deleted');

            if (statusCondition1) {
                searchCondition.push(statusCondition1);
            }
            if (statusCondition2) {
                searchCondition.push(statusCondition2);
            }
            if (statusCondition3) {
                searchCondition.push(statusCondition3);
            }
        }
        if (filterValues === RequestDashboardFilterValues.MY_REQUESTS) {
            let ownerCondition = this.fieldConfigService.createConditionObject('REQUESTOR',
                MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, this.sharingService.getCurrentUserDisplayName());
            let statusCondition2 = this.fieldConfigService.createConditionObject('REQUEST_STATUS',
                MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, 'Planning');
            let statusCondition3 = this.fieldConfigService.createConditionObject('REQUEST_STATUS',
                MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, 'Deleted');

            if (ownerCondition) {
                searchCondition.push(ownerCondition);
            }
            if (statusCondition2) {
                searchCondition.push(statusCondition2);
            }
            if (statusCondition3) {
                searchCondition.push(statusCondition3);
            }
        }
        if (filterValues === RequestDashboardFilterValues.MY_TASKS) {
            let ownerCondition = this.fieldConfigService.createConditionObject('TASK_OWNER_CN',
                MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, this.sharingService.getCurrentUserCN());
            let taskStateCondition;
            /*if(this.selectedListDependentFilter) {
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                this.selectedListDependentFilter.searchOperator, this.selectedListDependentFilter.searchOperatorName, MPMSearchOperators.AND, TaskConstant.TASK_STATE.COMPLETED);
            }
            else {*/
            taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, TaskConstant.TASK_STATE.COMPLETED);

            let statusCondition1 = BusinessConstants.searchConditionForDeleteAfterSubmit

            if (statusCondition1) {
                searchCondition.push(statusCondition1);
            }

            if (ownerCondition) {
                searchCondition.push(ownerCondition);
            }
            if (taskStateCondition) {
                searchCondition.push(taskStateCondition);
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, TaskConstant.TASK_STATE.OBSOLETE);
                searchCondition.push(taskStateCondition);
            }
        }
        if (filterValues === RequestDashboardFilterValues.E2E_PM_CLASSIFICATION_TEAM_TASKS) {
            let subjectCondition = this.fieldConfigService.createConditionObject('TASK_SUBJECT',
                'MPM.OPERATOR.CHAR.CONTAINS', 'contains', MPMSearchOperators.AND, 'E2E Review');

            let statusCondition1 = BusinessConstants.searchConditionForDeleteAfterSubmit

            let taskStateCondition;
            if (this.selectedListDependentFilter) {
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    this.selectedListDependentFilter.searchOperator, this.selectedListDependentFilter.searchOperatorName, MPMSearchOperators.AND, TaskConstant.TASK_STATE.COMPLETED);
            }
            else {
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, TaskConstant.TASK_STATE.COMPLETED);
            }
            if (subjectCondition) {
                searchCondition.push(subjectCondition);
            }
            if (statusCondition1) {
                searchCondition.push(statusCondition1);
            }
            if (taskStateCondition) {
                searchCondition.push(taskStateCondition);
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, TaskConstant.TASK_STATE.OBSOLETE);
                searchCondition.push(taskStateCondition);
            }
        }
        if (filterValues === RequestDashboardFilterValues.BUSINESS_DEVELOPMENT_APPROVAL_TEAM_TASKS) {
            let subjectCondition = this.fieldConfigService.createConditionObject('TASK_SUBJECT',
                'MPM.OPERATOR.CHAR.CONTAINS', 'contains', MPMSearchOperators.AND, 'Business Dev Approval');

            let statusCondition1 = BusinessConstants.searchConditionForDeleteAfterSubmit

            let taskStateCondition;
            if (this.selectedListDependentFilter) {
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    this.selectedListDependentFilter.searchOperator, this.selectedListDependentFilter.searchOperatorName, MPMSearchOperators.AND, TaskConstant.TASK_STATE.COMPLETED);
            }
            else {
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, TaskConstant.TASK_STATE.COMPLETED);
            }
            if (subjectCondition) {
                searchCondition.push(subjectCondition);
            }

            if (statusCondition1) {
                searchCondition.push(statusCondition1);
            }
            if (taskStateCondition) {
                searchCondition.push(taskStateCondition);
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, TaskConstant.TASK_STATE.OBSOLETE);
                searchCondition.push(taskStateCondition);
            }
        }
        if (filterValues === RequestDashboardFilterValues.PROGRAM_MANAGER_APPROVAL_TEAM_TASKS) {
            let subjectCondition = this.fieldConfigService.createConditionObject('TASK_SUBJECT',
                'MPM.OPERATOR.CHAR.CONTAINS', 'contains', MPMSearchOperators.AND, 'PM Review');

            let statusCondition1 = BusinessConstants.searchConditionForDeleteAfterSubmit;

            let taskStateCondition;
            if (this.selectedListDependentFilter) {
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    this.selectedListDependentFilter.searchOperator, this.selectedListDependentFilter.searchOperatorName, MPMSearchOperators.AND, TaskConstant.TASK_STATE.COMPLETED);
            }
            else {
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, TaskConstant.TASK_STATE.COMPLETED);
            }
            if (subjectCondition) {
                searchCondition.push(subjectCondition);
            }
            if (statusCondition1) {
                searchCondition.push(statusCondition1);
            }

            if (taskStateCondition) {
                searchCondition.push(taskStateCondition);
                taskStateCondition = this.fieldConfigService.createConditionObject('TASK_STATE',
                    MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT, MPMSearchOperators.AND, TaskConstant.TASK_STATE.OBSOLETE);
                searchCondition.push(taskStateCondition);
            }
        }
        return searchCondition;

    }

    handleRequestServiceCall() {
        this.getAllRequests()
            .subscribe(projectsResponse => {
                this.loaderService.hide();
            }, requestsError => {
                this.loaderService.hide();
                this.notificationService.error(requestsError);
            });
    }

    updateSearchConfigId() {
        if (this.dashboardMenuConfig.projectViewConfig) {
            this.searchConfigId = this.dashboardMenuConfig.projectViewConfig.R_PO_ADVANCED_SEARCH_CONFIG;
            this.searchChangeEventService.update(this.dashboardMenuConfig.projectViewConfig);
        }
    }

    getRequestConfig() {
        const newRequestType = this.dashboardMenuConfig.NEW_REQUEST_TYPES;
        this.requestConfig = this.sharingService.getProjectConfig();
        this.appConfig = this.sharingService.getAppConfig();
        newRequestType.isShown = this.canCreateRequest;

        newRequestType.options.map(option => {
            option.isShown = this.utilService.getBooleanValue(this.requestConfig[option.appDynamicConfigId]);
            this.optionDropdown = this.optionDropdown || option.isShown;
        });
        //this.isCommercial = this.filterConfigService.isCommercialRole();
        this.dashboardMenuConfig.NEW_REQUEST_TYPES = newRequestType;
    }



    getRequestDetails(): Observable<any> {
        return new Observable(observer => {
            this.appService.invokeRequest(this.REQUEST_DETAILS_METHOD_NS, this.GET_REQUEST_DETAILS_WS_METHOD_NAME, '').subscribe(response => {
                if (response) {
                    let requestTypes = [];
                    if (response && response.Request_Type) {
                        if (Array.isArray(response.Request_Type)) {
                            requestTypes = response.Request_Type;
                        }
                        else {
                            requestTypes.push(response.Request_Type);
                        }
                    }
                    RequestDashboardControlConstant.newRequestType.options = [];
                    for (var i = 0; i < requestTypes.length; i++) {
                        let options = {
                            name: requestTypes[i].DISPLAY_NAME,
                            value: requestTypes[i].VALUE,
                            appDynamicConfigId: ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_PROJECT,
                            isShown: requestTypes[i].Is_Active,
                            icon: 'description',
                            IS_REQUEST_TYPE: true,
                            itemId: requestTypes[i]["Request_Type-id"].ItemId
                        }
                        RequestDashboardControlConstant.newRequestType.options.push(options);
                    }
                }
                if (RequestDashboardControlConstant.newRequestType.options.length > 0) {
                    this.canCreateRequest = true;
                }
                this.getRequestConfig();
            });
            observer.next(true);
            observer.complete();
        });
    }

    onSortChange(event) {
        this.dashboardMenuConfig.selectedSortOption = event;
        this.selectedSortOption = this.dashboardMenuConfig.selectedSortOption.option.displayName;
        this.selectedSortOrder = this.dashboardMenuConfig.selectedSortOption.sortType;
        this.refreshRequest();
    }

    getSelectedRequest(event) {
        this.selectedTasks = event;

        //console.log(this.utilService.getUserRoles());

        if (this.dashboardMenuConfig.selectedDashboardFilter.IS_INBOX) {
            this.enableInboxOperationControls();
        }
    }

    disableAllControls() {
        this.enableClaim = false;
        this.enableRevoke = false;
        this.enableAssign = false;
        this.enableDelegate = false;
    }

    enableInboxOperationControls() {
        this.selectedTasksStates = [];
        this.selectedTasksID = [];
        this.selectedTasksAssignee = [];
        this.selectedTasksDelegatedTo = [];
        this.selectedTasksSubject = [];
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.selectedTasks.length; i++) {
            this.selectedTasksStates.push(this.selectedTasks[i].RT_TASK_STATE);
            this.selectedTasksID.push(this.selectedTasks[i].RT_PARENT_TASK_ID);
            this.selectedTasksAssignee.push(this.selectedTasks[i].RT_TASK_OWNER_CN);
            this.selectedTasksDelegatedTo.push(this.selectedTasks[i].RT_DELEGATED_TO);
            this.selectedTasksSubject.push(this.selectedTasks[i].RT_TASK_SUBJECT);
        }

        const containsDelegatedTasks = this.selectedTasksDelegatedTo.some((el) => {
            return el && el !== 'NA';
        });

        // If any one Task Status is CREATED or If it in collaboration then disable all the actions
        if (this.selectedTasksStates.indexOf('1') !== -1) {
            this.enableClaim = true;
            this.enableRevoke = false;
            this.enableAssign = true;
            this.enableDelegate = false;
        }
        // If any one Task Status is CREATED or If it in collaboration then disable all the actions
        if (this.selectedTasksStates.indexOf('5') !== -1) {
            this.disableAllControls();
        } else if (this.selectedTasksStates.indexOf('2') !== -1 && this.selectedTasksStates.indexOf('1') !== -1) {
            this.disableAllControls();
        } else {
            // Check if all Task are assigned to same user
            if (this.selectedTasksStates.indexOf('2') !== -1) {
                const arryString = this.selectedTasksStates.toString();
                // tslint:disable-next-line: no-shadowed-variable
                const regExp = new RegExp('2', 'g');
                const StatusData = arryString.match(regExp);
                if ((StatusData != null) && (StatusData.length === this.selectedTasksStates.length)) {
                    const arrayUserString = this.selectedTasksAssignee.toString();
                    const regExp1 = new RegExp(this.sharingService.getCurrentUserID(), 'g');
                    const AssigneeData = arrayUserString.match(regExp1);
                    // Check if The task belong to him or not
                    if (((AssigneeData != null) && (AssigneeData.length === this.selectedTasksAssignee.length))) { // || (this.isNotificationAdmin)
                        this.enableClaim = false;
                        this.enableAssign = false;
                        this.enableRevoke = true;

                        if (containsDelegatedTasks) {
                            this.enableDelegate = false;
                        } else {
                            this.enableDelegate = true;
                        }

                    } else {
                        this.disableAllControls();
                    }
                }
            }
            // Check all Task for CREATED state
            else {
                const arryStateString = this.selectedTasksStates.toString();
                const regExp = new RegExp('1', 'g');
                const TaskStatusData = arryStateString.match(regExp);
                // Check if all the selected task are in CREATED state
                if ((TaskStatusData != null) && (TaskStatusData.length === this.selectedTasksStates.length)) {
                    this.enableClaim = true;
                    this.enableRevoke = false;
                    // Enable assign button if he is Notification admin
                    /* if (this.isNotificationAdmin) {
                        this.enableAssign = true;
                    } else {
                        this.enableAssign = false;
                    } */
                    this.enableAssign = true;
                    this.enableDelegate = false;
                } else {
                    this.disableAllControls();
                }
            }
            const containsNonRevokableTasks = this.selectedTasksSubject.some((el) => {
                return el && (el == BusinessConstants.TASK_ACTIVITY_E2EPM || el == BusinessConstants.TASK_ACTIVITY_REQUEST_REWORK);
            });
            if (containsNonRevokableTasks) {
                this.enableRevoke = false;
            }


        }


    }

    getInboxOperationTaskObj() {
        return {
            'taskId': '',
            'taskData': {
                'data': {
                    'operation': ''
                }
            },
            'customData': {
                'data': {
                    'applicationId': ''
                }
            }
        };
    }

    claim() {
        const taskToSend = [];
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.selectedTasks.length; i++) {
            let inboxOperationTaskObject: any = this.getInboxOperationTaskObj();
            inboxOperationTaskObject.taskId = this.selectedTasks[i].RT_PARENT_TASK_ID;
            inboxOperationTaskObject.taskData.data.operation = '';
            inboxOperationTaskObject.customData.data.applicationId = '';
            taskToSend.push(inboxOperationTaskObject);
        }
        this.loaderService.show();
        this.inboxService.claim(this.APP_ID, this.sharingservice.currentUserDN, taskToSend, (response) => {
            this.disableAllControls();
            if (response != null) {
                setTimeout(() => {
                    this.notificationService.success('Task(s) claimed successfully');
                    this.refreshRequest();
                }, 3000)
            } else {
                this.loaderService.hide();
                this.notificationService.error('Error while claiming the task(s). Please try again.');
            }
        });
    }

    revoke() {
        //console.log("Revoke")
        const taskToSend = [];
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < this.selectedTasks.length; i++) {
            let inboxOperationTaskObject: any = this.getInboxOperationTaskObj();
            inboxOperationTaskObject.taskId = this.selectedTasks[i].RT_PARENT_TASK_ID;
            inboxOperationTaskObject.taskData.data.operation = '';
            inboxOperationTaskObject.customData.data.applicationId = '';
            taskToSend.push(inboxOperationTaskObject);
        }
        this.loaderService.show();
        this.inboxService.revoke(this.APP_ID, taskToSend, (response) => {
            this.disableAllControls();
            if (response != null) {
                setTimeout(() => {
                    this.notificationService.success('User is revoked from the Task(s)');
                    this.refreshRequest();
                }, 3000)

            } else {
                this.loaderService.hide();
                this.notificationService.error('Error while revoking the Task(s). Please try again.');
            }
        });
    }

    userAssignment(operation: string) {
        //console.log("User Assignment "+operation)
        const dialogRef = this.dialog.open(InboxAssignmentModalComponent, {
            // tslint:disable-next-line: object-literal-shorthand
            width: '25%',
            //height: '35%',
            data: { tasks: this.selectedTasksID, operation: operation }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result.userId === '') {
                return;
            }
            const taskToSend = [];
            const selectedTasksAssignees = [];
            // tslint:disable-next-line: prefer-for-of
            for (let i = 0; i < this.selectedTasks.length; i++) {
                let inboxOperationTaskObject: any = this.getInboxOperationTaskObj();
                inboxOperationTaskObject.taskId = this.selectedTasks[i].RT_PARENT_TASK_ID;
                inboxOperationTaskObject.taskData.data.operation = '';
                inboxOperationTaskObject.customData.data.applicationId = this.APP_ID;
                inboxOperationTaskObject.customData.data.reviewState =
                    this.selectedTasks[i].RT_TASK_SUBJECT ? this.selectedTasks[i].RT_TASK_SUBJECT : '';
                inboxOperationTaskObject.customData.data.requestId = this.selectedTasks[i].RT_REQUEST_ENTITY_ITEM_ID;
                taskToSend.push(inboxOperationTaskObject);
                selectedTasksAssignees.push(this.selectedTasks[i].RT_TASK_OWNER_NAME);
            }
            if (result.operation === 'Assign') {
                this.loaderService.show();
                this.inboxService.assign(this.APP_ID, result.userId, taskToSend, (response) => {
                    this.disableAllControls();
                    if (response === null) {
                        this.loaderService.hide();
                        this.notificationService.error('Error while assigning the user. Please try again.');
                    }
                    else {
                        setTimeout(() => {
                            //this.notificationService.success('Task(s) is delegated to the User');
                            this.notificationService.success('User is assigned for the task(s)');
                            this.refreshRequest();
                        }, 3000)
                        // this.notificationService.success('User is assigned for the task(s)');

                    }
                });
            }
            else {
                this.loaderService.show();
                this.inboxService.delegate(this.APP_ID, result.userId, result.transferOwnership, taskToSend, (response) => {
                    this.disableAllControls();
                    if (response === null) {
                        this.loaderService.hide();
                        this.notificationService.error('Error while delegating the user. Please try again.');
                    }
                    else {
                        setTimeout(() => {
                            this.notificationService.success('Task(s) is delegated to the User');
                            // this.notificationService.success('User is delegated for the task(s)');
                            this.refreshRequest();
                        }, 3000)

                    }
                });
            }
        });
    }

    hasRole(roleName: string) {
        let hasRole = false;
        for (let i = 0; i < this.userRoles.length; i++) {
            if (roleName === this.userRoles[i].Name) {
                hasRole = true;
                break;
            }
        }
        return hasRole
    }

    displayFilterOption(filter) {
        if (filter.VALUE === RequestDashboardFilterValues.MY_TASKS || filter.VALUE === RequestDashboardFilterValues.ALL_REQUESTS || filter.VALUE === RequestDashboardFilterValues.MY_REQUESTS) {
            return true;
        }

        return this.hasRole(filter.ACCESS_ROLE);
    }

    listDependentFilterChange(option) {
        this.selectedListDependentFilter = option;
        this.facetRestrictionList = {};
        this.facetChangeEventService.resetFacet();
        this.dashboardMenuConfig.searchCondition = this.formFilterCondition(this.dashboardMenuConfig.selectedDashboardFilter.VALUE,
            this.dashboardMenuConfig.selectedDashboardFilter.SEARCH_CONDITION);
        this.defaultPagination();
        this.loaderService.show();
        this.getMPMDataFields().subscribe(data => {
            this.updateSearchConfigId();
            this.handleRequestServiceCall();
        }, mpmFieldError => {
            this.loaderService.hide();
            this.notificationService.error(mpmFieldError);
        });
    }


    getFilters(isDashboardFilterChange?) {
        const allListDependentFilters = getTaskListDependentFilters();
        let routedListDependentFilter = this.route.snapshot.queryParamMap.get("listDependentFilter");
        let routedListDependentFilters;
        this.listDependentFilters = allListDependentFilters[this.dashboardMenuConfig.selectedDashboardFilter.VALUE];
        if (isDashboardFilterChange) {
            this.selectedListDependentFilter = (this.listDependentFilters && this.listDependentFilters.length > 0 && this.listDependentFilters[0]);
        } else {
            if (routedListDependentFilter) {
                routedListDependentFilters = this.listDependentFilters.find(filter => {
                    return filter.value === routedListDependentFilter;
                });
            }
            this.selectedListDependentFilter = this.selectedListDependentFilter || routedListDependentFilters || (this.listDependentFilters && this.listDependentFilters.length > 0 && this.listDependentFilters[0]);
        }

    }

    getAllRequestTypes(): Observable<any> {
        return new Observable(observer => {
            this.monsterConfigApiService.getAllRequestTypes().subscribe((response) => {
                RequestDashboardControlConstant.newRequestType.options = response;
                if (RequestDashboardControlConstant.newRequestType.options.length > 0) {
                    this.canCreateRequest = true;
                }
                this.getRequestConfig();
                observer.next(true);
                observer.complete();
            }, (error) => {
                observer.error(error);
            });
        });
    }



    ngOnInit() {

        this.loaderService.show();
        this.monsterConfigApiService.getAllRolesForCurrentUser();
        this.appConfig = this.sharingService.getAppConfig();
        this.top = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.pageSize = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.pageNumber = 1;
        //this.getRequestDetails().subscribe(response => { });
        // this.menu = this.route.snapshot.paramMap.get("menu")
        this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_PAGE_LIMIT] = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION];
        this.menu = this.route.snapshot.queryParamMap.get("menu");
        //this.listDependentFilter = this.route.snapshot.queryParamMap.get("listDependentFilter");
        /* this.dashboardMenuConfig = {
          FILTER_LIST: [],
          APPLIED_FILTER: [],
          SERACH_FILTER: [],
          IS_REQUEST_TYPE: true,
          SELECTED_SORT: [],
          NEW_REQUEST_TYPES: RequestDashboardControlConstant.newRequestType,//this.dashboardControlConstant.newRequestType,
          requestFiltersList: [],
          requestDashboardFilters: [],
          selectedViewByName: 'LO_REQUEST_VIEW',
          applyFilterList: [],
          totalRequestCount: 0,
          requestData: [],
          isViewTypeChanged: false,
          isAdvancedSearchOpen: false,
          searchInputValue: '',
          searchCondition: [],
          requestSortOptions: [],
          requestSearchableFields: [],
          requestViewConfig: null,
      }; */
        this.dashboardMenuConfig = {
            FILTER_LIST: [],
            APPLIED_FILTER: [],
            SERACH_FILTER: [],
            IS_PROJECT_TYPE: true,
            SELECTED_SORT: [],
            IS_LIST_VIEW: true,
            NEW_REQUEST_TYPES: RequestDashboardControlConstant.newRequestType,
            projectFilterList: [],
            projectDashBoadFilters: [],
            selectedViewByName: '',
            applyFilterList: [],
            totalProjectCount: 0,
            projectData: [],
            isViewTypeChanged: false,
            isAdvancedSearchOpen: false,
            searchInputValue: '',
            searchCondition: [],
            projectSortOptions: [],
            projectSearchableFields: [],
            projectViewConfig: null,
            projectViewName: '',
            templateViewName: null,
            selectedDashboardFilter: '',
        };
        this.userRoles = this.utilService.getUserRoles();
        this.isNotificationAdmin = this.hasRole(this.notificationAdminRoleName);
        this.APP_ID = this.utilService.APP_ID;
        this.getAllRequestTypes().subscribe();
        this.getRequestConfig();
        this.initializeRequestFilters();

        this.getMPMDataFields()
            .subscribe(metadataResponse => {
                this.tempsearchConditionsSubscribe = this.searchDataService.searchData.subscribe(data => {
                    this.loaderService.show();
                    this.searchConditions = data;
                    this.skip = (data && data.length > 0) ? 0 : this.skip;
                    this.page = (data && data.length > 0) ? 1 : this.page;
                    this.handleRequestServiceCall();
                });
                this.facetChangeEventService.onFacetCondidtionChange.pipe(
                    takeUntil(this.unsubscribe$)
                ).subscribe(facetRestrictionList => {
                    if (JSON.stringify(facetRestrictionList) !== '{}') {
                        this.facetRestrictionList = facetRestrictionList;
                        this.handleRequestServiceCall();
                    }
                });
            }, metadataError => {
                this.loaderService.hide();
                this.notificationService.error(metadataError);
            });

        this.sharingService.resetViewData().subscribe(viewId => {
            if (viewId && this.dashboardMenuConfig && this.dashboardMenuConfig.projectViewConfig && this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'] && this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id && viewId === this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id) {
                const defaultPreferenceView = this.defaultColumnPreferenceService.getDefaultPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
                this.resetCurrentView(defaultPreferenceView);
            }
        });
    }

    //Task Operations - NPD1-67
    //Claiming task
    claimIndividualTask(taskDetails) {
        const taskToSend = [];

        // console.log(taskDetails);
        let inboxOperationTaskObject: any = this.getInboxOperationTaskObj();
        inboxOperationTaskObject.taskId = taskDetails.RT_PARENT_TASK_ID;
        inboxOperationTaskObject.taskData.data.operation = '';
        inboxOperationTaskObject.customData.data.applicationId = '';
        taskToSend.push(inboxOperationTaskObject);
        this.loaderService.show();
        this.inboxService.claim(this.APP_ID, this.sharingservice.currentUserDN, taskToSend, (response) => {
            this.disableAllControls();
            if (response != null) {
                setTimeout(() => {
                    this.notificationService.success('Task claimed successfully');
                    this.refreshRequest();
                }, 3000)
            } else {
                this.loaderService.hide();
                this.notificationService.error('Error while claiming the task. Please try again.');
            }
        });

    }

    revokeIndividualTask(taskDetails) {
        const taskToSend = [];

        // console.log(taskDetails);
        let inboxOperationTaskObject: any = this.getInboxOperationTaskObj();
        inboxOperationTaskObject.taskId = taskDetails.RT_PARENT_TASK_ID;
        inboxOperationTaskObject.taskData.data.operation = '';
        inboxOperationTaskObject.customData.data.applicationId = '';
        taskToSend.push(inboxOperationTaskObject);

        this.loaderService.show();
        this.inboxService.revoke(this.APP_ID, taskToSend, (response) => {
            this.disableAllControls();
            if (response != null) {
                setTimeout(() => {
                    this.notificationService.success('User is revoked from the Task');
                    this.refreshRequest();
                }, 3000)

            } else {
                this.loaderService.hide();
                this.notificationService.error('Error while revoking the Task. Please try again.');
            }
        });

    }

    assignIndividualTask(taskDetails) {
        //console.log("User Assignment "+taskDetails.operation)
        let selectedTask = [];
        selectedTask.push(taskDetails.RT_PARENT_TASK_ID);
        const dialogRef = this.dialog.open(InboxAssignmentModalComponent, {
            // tslint:disable-next-line: object-literal-shorthand
            width: '25%',
            data: { tasks: selectedTask, operation: taskDetails.operation }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result.userId === '') {
                return;
            }
            const taskToSend = [];
            const selectedTasksAssignees = [];
            // tslint:disable-next-line: prefer-for-of

            let inboxOperationTaskObject: any = this.getInboxOperationTaskObj();
            inboxOperationTaskObject.taskId = taskDetails.RT_PARENT_TASK_ID;
            inboxOperationTaskObject.taskData.data.operation = '';
            inboxOperationTaskObject.customData.data.applicationId = this.APP_ID;
            inboxOperationTaskObject.customData.data.reviewState =
                taskDetails.RT_TASK_SUBJECT ? taskDetails.RT_TASK_SUBJECT : '';
            inboxOperationTaskObject.customData.data.requestId = taskDetails.RT_REQUEST_ENTITY_ITEM_ID;
            taskToSend.push(inboxOperationTaskObject);
            selectedTasksAssignees.push(taskDetails.RT_TASK_OWNER_NAME);

            if (result.operation === 'Assign') {
                this.loaderService.show();
                this.inboxService.assign(this.APP_ID, result.userId, taskToSend, (response) => {
                    if (response === null) {
                        this.loaderService.hide();
                        this.notificationService.error('Error while assigning the user. Please try again.');
                    }
                    else {
                        setTimeout(() => {
                            this.notificationService.success('User is assigned for the task');
                            this.refreshRequest();
                        }, 3000)
                    }
                });
            }
            else {
                this.loaderService.show();
                this.inboxService.delegate(this.APP_ID, result.userId, result.transferOwnership, taskToSend, (response) => {
                    if (response === null) {
                        this.loaderService.hide();
                        this.notificationService.error('Error while delegating the task to user. Please try again.');
                    }
                    else {
                        setTimeout(() => {
                            this.notificationService.success('Task is delegated to the User');
                            this.refreshRequest();
                        }, 3000)

                    }
                });
            }
        });
    }

    ngOnDestroy() {
        if (this.tempsearchConditionsSubscribe) {
            this.searchChangeEventService.update(null);
            this.tempsearchConditionsSubscribe.unsubscribe();
            //   this.facetChangeEventService.facetCondidtion.next(null);
            //   this.facetChangeEventService.facetCondidtion.complete();
            this.unsubscribe$.next();
            this.unsubscribe$.complete();
        }
    }

}
