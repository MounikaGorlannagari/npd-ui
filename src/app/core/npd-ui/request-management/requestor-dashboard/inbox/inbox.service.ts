import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { AppService, GloabalConfig } from 'mpm-library';
import * as acronui from 'mpm-library';

@Injectable({
    providedIn: 'root'
})

export class InboxService {

    constructor(
        private appService: AppService
    ) { }

    GET_TASK_NS = 'http://schemas.cordys.com/notification/workflow/1.0';
    GET_TASK_WS_METHOD_NAME = 'GetTask';
    TO_WS_METHOD_NAME = 'TaskOperation';
    TO_WS_NS = 'http://schemas.acheron.com/mpm/inbox/bpm/1.0';
    GET_USERS_BY_TASK_NS = 'http://schemas.cordys.com/notification/workflow/1.0';
    GET_USERS_BY_TASK_METHOD_NAME = 'GetUsersByTask';

    selectedTaskObject: any = {};

    inboxOperationTaskObject: any = {
        'taskId': '',
        'taskData': {
            'data': {
                'operation': ''
            }
        },
        'customData': {
            'data': {
                'applicationId': ''
            }
        }
    };

    getTaskObjStructure(): any {
        return this.inboxOperationTaskObject;
    }

    setSelectedTaskObject(obj): any {
        this.selectedTaskObject = obj;
    }

    getSelectedTaskObject(): any {
        return this.selectedTaskObject;
    }

    getTaskDetails(taskId, callback?): Observable<any> {
        const requestObject = {
            'TaskId': taskId,
            'RetrievePossibleActions': true,
            'ReturnTaskData': true
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.GET_TASK_NS, this.GET_TASK_WS_METHOD_NAME, requestObject)
                .subscribe(response => {
                    const rxdTask = acronui.findObjectsByProp(response, 'old');
                    observer.next(rxdTask);
                    observer.complete();
                }, error => {
                    observer.error(new Error(error));
                });
        });
    }

    claim(applicationID, assigneeDN, tasks, callback) {
        if ((applicationID) && Array.isArray(tasks) && tasks.length > 0) {
            const requestObj = {
                'taskOperation': {
                    'applicationId': '',
                    'operation': 'CLAIM',
                    'assigneeDN': assigneeDN,
                    'tasks': {
                        'task': [],
                    }
                }
            };
            requestObj.taskOperation.applicationId = applicationID;
            requestObj.taskOperation.tasks.task = tasks;
            this.appService.invokeRequest(this.TO_WS_NS, this.TO_WS_METHOD_NAME, requestObj)
                .subscribe(response => {
                    if (callback) {
                        callback(response);
                    }
                }, error => {
                    if (callback) {
                        callback(null);
                    }
                });
        } else {
            console.warn('Mandatory parameters are missing for claim operation.');
        }
    }

    revoke(applicationID, tasks, callback) {
        if ((applicationID) && Array.isArray(tasks) && tasks.length > 0) {
            const requestObj = {
                'taskOperation': {
                    'applicationId': applicationID,
                    'operation': 'REVOKE',
                    'tasks': {
                        'task': [],
                    }
                }
            };
            requestObj.taskOperation.applicationId = applicationID;
            requestObj.taskOperation.tasks.task = tasks;
            this.appService.invokeRequest(this.TO_WS_NS, this.TO_WS_METHOD_NAME, requestObj)
                .subscribe(response => {
                    if (callback) {
                        callback(response);
                    }
                }, error => {
                    if (callback) {
                        callback(null);
                    }
                });
        } else {
            console.warn('Mandatory parameters are missing for revoke operation.');
        }
    }

    assign(applicationID, assigneeDN, tasks, callback) {
        if ((applicationID) && (assigneeDN) && Array.isArray(tasks) && tasks.length > 0) {
            const requestObj = {
                'taskOperation': {
                    'applicationId': applicationID,
                    'operation': 'ASSIGN',
                    'assigneeDN': assigneeDN,
                    'tasks': {
                        'task': [],
                    }
                }
            };
            requestObj.taskOperation.applicationId = applicationID;
            requestObj.taskOperation.tasks.task = tasks;
            this.appService.invokeRequest(this.TO_WS_NS, this.TO_WS_METHOD_NAME, requestObj)
                .subscribe(response => {
                    if (callback) {
                        callback(response);
                    }
                }, error => {
                    if (callback) {
                        callback(null);
                    }
                });
        } else {
            console.warn('Mandatory parameters are missing for assign operation.');
        }
    }

    delegate(applicationID, assigneeDN, transferOwnership, tasks, callback) {
        if ((applicationID) && (assigneeDN) && Array.isArray(tasks) && tasks.length > 0) {
            const requestObj = {
                'taskOperation': {
                    'applicationId': applicationID,
                    'operation': 'DELEGATE',
                    'transferOwnership': (typeof transferOwnership === 'boolean') ? transferOwnership : true,
                    'assigneeDN': assigneeDN,
                    'tasks': {
                        'task': [],
                    },
                }
            };
            requestObj.taskOperation.applicationId = applicationID;
            requestObj.taskOperation.tasks.task = tasks;
            this.appService.invokeRequest(this.TO_WS_NS, this.TO_WS_METHOD_NAME, requestObj)
                .subscribe(response => {
                    if (callback) {
                        callback(response);
                    }
                }, error => {
                    if (callback) {
                        callback(null);
                    }
                });
        } else {
            console.warn('Mandatory parameters are missing for delegate operation.');
        }
    }

    getCompleteTaskReqObj(appId, itemId, id, status, parentTaskId, operationName): any {
        if (!itemId || !id || !status || !parentTaskId) {
            return;
        }
        return {
            'taskOperation': {
                'applicationId': appId,
                'operation': operationName,
                'tasks': {
                    'task': {
                        'taskId': parentTaskId,
                        'taskData': {
                            'formoutputdata': {
                                'freeformcontrols': {
                                    'input_request_item_id': {
                                        '@display_name': 'Request ItemId',
                                        '__text': itemId
                                    },
                                    'input_id': {
                                        '@display_name': 'Id',
                                        '__text': id
                                    },
                                    'select_status': {
                                        '@display_name': 'Status',
                                        '__text': status
                                    },
                                    'btn_submit': {
                                        '@display_name': 'Ok',
                                        '__text': 'Ok'
                                    },
                                    'btn_cancel': {
                                        '@display_name': 'Cancel',
                                        '__text': 'Cancel'
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
    }

    taskOperation(requestObj): Observable<any> {
        return new Observable(taskOpObserver => {
            this.appService.invokeRequest(this.TO_WS_NS, this.TO_WS_METHOD_NAME, requestObj)
                .subscribe(response => {
                    taskOpObserver.next(response);
                    taskOpObserver.complete();
                }, error => {
                    taskOpObserver.error(new Error());
                });
        });
    }

    fetchUsersForTask(taskID, callback) {
        if (Array.isArray(taskID) && taskID.length > 0) {
            const requestObject = {
                'TaskId': taskID,
                'TargetType': 'role',
            };
            this.appService.invokeRequest(this.GET_USERS_BY_TASK_NS, this.GET_USERS_BY_TASK_METHOD_NAME, requestObject)
                .subscribe(response => {
                    if (callback) {
                        const users = acronui.findObjectsByProp(response, 'PotentialOwner');
                        callback(users);
                    }
                }, error => {
                    if (callback) {
                        callback(null);
                    }
                });
        }
    }

}
