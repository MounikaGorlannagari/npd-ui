import { Component, OnInit, OnDestroy, ChangeDetectorRef, SimpleChanges, Renderer2, EventEmitter, Output, Input } from '@angular/core';
import { ProjectListComponent, MPMFieldKeys, MPMField, AppService, ProjectService, 
    TaskService, UtilService, NotificationService, LoaderService, FormatToLocalePipe, 
    OtmmMetadataService, EntityAppDefService, FieldConfigService, SharingService, 
    ProjectBulkCountService, ConfirmationModalComponent, IndexerDataTypes, GetPropertyPipe } from 'mpm-library';
import * as $ from 'jquery';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { RouteService } from 'src/app/core/mpm/route.service';
import { RequestDashboardFilterValues } from '../../filter/request-dashboard-filters';
import { FieldConstants } from '../../constants/request-field-constant';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../../request-view/constants/BusinessConstants';
import { InboxService } from './inbox.service';
import { TechnicalBusinessRequestComponent } from '../../../request-view/request-types/technical-business-request/technical-business-request.component';
import { TechnicalRequestService } from '../../../request-view/services/technical-request.service';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { MONSTER_ROLES } from '../../../filter-configs/role.config';
import { FilterConfigService } from '../../../filter-configs/filter-config.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
})
export class InboxComponent extends ProjectListComponent implements OnInit {
  selectedDashboardFilter = '';
  isCopy = false;
  isCopyRequest: boolean = false;
  isMyTasks = false;
  isMyRequest = false;
  isAllRequests = false;
  isE2ETasks = false;
  isPMTasks = false;
  isBDATasks = false;
  currentUserDisplayName = this.sharingService.getCurrentUserDisplayName();

  @Input() customFrezze;

  @Input() userPreferenceFreezeCount;
  //event emiters for Task Operations- : EventEmitter<any> - NPD1-67
  @Output() claimTask = new EventEmitter<any>();
  @Output() revokeTask = new EventEmitter<any>();
  @Output() assignTask = new EventEmitter<any>();
  /*isSFL = false;
    isLFL = false;
    isVFL = false;*/
  @Output() refreshPage: EventEmitter<any> = new EventEmitter<any>();
  @Output() copyRequestButton: EventEmitter<any> = new EventEmitter<any>();
  // @Output() isCopyRequest: EventEmitter<any> = new EventEmitter<any>();

  DELETE_REQUEST_METHOD_NS = 'http://schemas.starz.com/csc/localization/1.0';
  DELETE_REQUEST_METHOD_NAME = 'DeleteRequest';
  currentUser;
  selectedRows = [];
  conditionCount: number = 0;
  requestFieldConstants = FieldConstants;
  dashboardMenuConfig;
  isAdmin: boolean;
  isDeleteAccess: any;
  // isAdmin: boolean;

  constructor(
    private appService: AppService,
    private monsterConfigApiService: MonsterConfigApiService,
    private mpmRouteService: RouteService,
    dialog: MatDialog,
    projectService: ProjectService,
    taskService: TaskService,
    utilService: UtilService,
    notificationService: NotificationService,
    loaderService: LoaderService,
    formatToLocalePipe: FormatToLocalePipe,
    otmmMetadataService: OtmmMetadataService,
    entityAppDefService: EntityAppDefService,
    fieldConfigService: FieldConfigService,
    sharingService: SharingService,
    projectBulkCountService: ProjectBulkCountService,
    router: Router,
    renderer: Renderer2,
    private monsterUtilService: MonsterUtilService,
    public getPropertyPipe: GetPropertyPipe,
    private routeService: RouteService,
    private technicalService: TechnicalRequestService,
    private filterConfigService: FilterConfigService
  ) {
    super(
      dialog,
      projectService,
      taskService,
      utilService,
      notificationService,
      loaderService,
      formatToLocalePipe,
      otmmMetadataService,
      entityAppDefService,
      fieldConfigService,
      sharingService,
      projectBulkCountService,
      router,
      renderer,
      getPropertyPipe
    );
    this.currentUser = this.sharingService.getCurrentUserDisplayName();
  }

  tableDrop(event: CdkDragDrop<string[]>) {
    let count = 0;
    if (this.displayColumns.find((column) => column === 'Select')) {
      count = count + 1;
    }
    if (this.displayColumns.find((column) => column === 'Expand')) {
      count = count + 1;
    }
    moveItemInArray(
      this.displayColumns,
      event.previousIndex + count,
      event.currentIndex + count
    );
    const orderedFields = [];
    this.displayColumns.forEach((column) => {
      if (
        !(column === 'Select' || column === 'Expand' || column === 'Actions')
      ) {
        orderedFields.push(
          this.displayableFields.find(
            (displayableField) => displayableField.INDEXER_FIELD_ID === column
          )
        );
      }
    });
    this.orderedDisplayableFields.next(orderedFields);
  }
  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.displayableFields &&
      !changes.displayableFields.firstChange &&
      JSON.stringify(changes.displayableFields.currentValue) !==
        JSON.stringify(changes.displayableFields.previousValue)
    ) {
      this.refreshData();
    }
  }
  getColumnStyle(column) {
    const style = [];
    if (column.DATA_TYPE === 'string' || column.DATA_TYPE === 'decimal') {
      // Added this becasue Bussiness Id is decimal NPD1-83
      style.push('wrap-text-custom');
    } else if (column.DATA_TYPE === 'dateTime') {
      style.push('gap-date-custom');
    }
    return style;
  }
  onResizeMouseDown(event, column) {
    event.stopPropagation();
    event.preventDefault();
    const start = event.target;
    const pressed = true;
    const startX = event.x;
    const startWidth = $(start).parent().width();
    this.initResizableColumns(start, pressed, startX, startWidth, column);
  }

  initResizableColumns(start, pressed, startX, startWidth, column) {
    this.renderer.listen('body', 'mousemove', (event) => {
      if (pressed) {
        const width = startWidth + (event.x - startX);
        column.width = width;
      }
    });
    this.renderer.listen('body', 'mouseup', (event) => {
      if (pressed) {
        pressed = false;
        this.resizeDisplayableFields.next();
      }
    });
  }
  refreshData() {
    // this.displayableMetadataFields = this.projectConfig.projectViewConfig ? this.utilService.getDisplayOrderData(this.projectConfig.projectViewConfig.R_PM_LIST_VIEW_MPM_FIELDS, this.projectConfig.projectViewConfig.LIST_FIELD_ORDER) : [];
    this.displayableMetadataFields = this.displayableFields;
    this.displayColumns = this.displayableMetadataFields.map(
      (column: MPMField) => column.INDEXER_FIELD_ID
    );
    this.isSelect = this.isCampaign
      ? !this.isCampaign
      : !this.isCampaignDashboard || !this.projectConfig.IS_CAMPAIGN_TYPE;
    /* if ((this.isCampaign && !this.isCampaign) || (this.isCampaignDashboard && !this.isCampaignDashboard || !this.projectConfig.IS_CAMPAIGN_TYPE)) {
     */
    if (this.isSelect) {
      this.displayColumns.unshift('Select');
    }
    if (this.projectConfig.IS_PROJECT_TYPE) {
      this.displayColumns.unshift('Expand');
    }
    this.selectedDashboardFilter =
      this.projectConfig.selectedDashboardFilter.VALUE;
    if (
      this.selectedDashboardFilter === RequestDashboardFilterValues.MY_TASKS
    ) {
      this.isMyTasks = true;
    }
    if (
      this.selectedDashboardFilter === RequestDashboardFilterValues.MY_REQUESTS
    ) {
      this.isMyRequest = true;
    }
    if (
      this.selectedDashboardFilter === RequestDashboardFilterValues.ALL_REQUESTS
    ) {
      this.isAllRequests = true;
    }
    if (
      this.selectedDashboardFilter ===
      RequestDashboardFilterValues.E2E_PM_CLASSIFICATION_TEAM_TASKS
    ) {
      this.isE2ETasks = true;
    }
    if (
      this.selectedDashboardFilter ===
      RequestDashboardFilterValues.PROGRAM_MANAGER_APPROVAL_TEAM_TASKS
    ) {
      this.isPMTasks = true;
    }
    if (
      this.selectedDashboardFilter ===
      RequestDashboardFilterValues.BUSINESS_DEVELOPMENT_APPROVAL_TEAM_TASKS
    ) {
      this.isBDATasks = true;
    }

    if (
      this.isMyTasks ||
      this.isBDATasks ||
      this.isPMTasks ||
      this.isE2ETasks ||
      this.isAllRequests ||
      this.isMyRequest
    ) {
      //|| this.isMyRequest
      this.displayColumns.push('Actions');
    }

    this.projectDataDataSource.data = [];
    this.projectDataDataSource.data = this.projectConfig.projectData;
    this.sort.active = this.projectConfig.selectedSortOption.option.name;
    this.sort.direction =
      this.projectConfig.selectedSortOption.sortType.toLowerCase();
    this.projectDataDataSource.sort = this.sort;

    this.monsterConfigApiService
      .getAllRolesForCurrentUser()
      .subscribe((response) => {
        const admin = response.find(
          (eachRole) =>
            eachRole['@id'].substring(3, eachRole['@id'].indexOf(',')) ===
            MONSTER_ROLES.NPD_ADMIN
        );
        this.isAdmin = admin != null ? true : false;
      });
  }

  openProject(event, requestDetails) {
    event.stopPropagation();
    let projectId = this.getpropertyValue(
      requestDetails.PLANNER_TEMP_PROJECT_ID
    )
      ? this.getpropertyValue(requestDetails.PLANNER_TEMP_PROJECT_ID)
      : this.getpropertyValue(requestDetails.RT_LOCALIZATION_PROJECT_ID)
      ? this.getpropertyValue(requestDetails.RT_LOCALIZATION_PROJECT_ID)
      : this.getpropertyValue(requestDetails.LOCALIZATION_PROJECT_ID);
    if (!projectId) {
      this.notificationService.info('Project Details not found.');
      return;
    }
    //this.mpmRouteService.goToProjectDetailView(projectId, false, null);
    this.mpmRouteService.goToTaskView(
      false,
      projectId,
      true,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    );
  }
  getpropertyValue(propertyValue) {
    if (propertyValue && propertyValue !== '0' && propertyValue !== 'NA')
      return propertyValue;
    else return null;
  }

  changeStatusColor(status) {
    return this.monsterUtilService.changeStatusColor(status);
  }

  getStatus(displayColumn, data) {
    let status = this.monsterUtilService.getStatusDisplayName(
      this.getProperty(displayColumn, data)
    ); //this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, data)
    return status && status.DISPLAY_NAME;
  }

  checkDateType(displayColumn) {
    return (
      displayColumn?.DATA_TYPE?.toLowerCase() ===
      IndexerDataTypes.DATETIME?.toLowerCase()
    );
  }

  getWeek(column, row) {
    let date = this.monsterUtilService.getFieldValueByDisplayColumn(
      column,
      row
    );
    if (
      date &&
      column?.DATA_TYPE.toLowerCase() ===
        IndexerDataTypes.DATETIME.toLowerCase()
    ) {
      return this.monsterUtilService.calculateWeek(date);
    }
  }

  tableData(): MatTableDataSource<any> {
    let result = new MatTableDataSource(this.projectDataDataSource.data);
    result.sortingDataAccessor = (item, header) => {
      if (this.monsterUtilService.isDateStringMatches(header)) {
        const weekFormat = item[header];
        if (
          weekFormat &&
          weekFormat != undefined &&
          weekFormat != '' &&
          weekFormat != 'NA'
        ) {
          const [week, year] = weekFormat.split('/').map(Number);
          return new Date(year, 0, (week - 1) * 7); // Convert to a date for proper sorting
        } else {
          const [week, year] = [0, 0];
          return new Date(year, 0, (week - 1) * 7); // Convert to a date for proper sorting
        }
      } else {
        return item[header];
      }
    };
    result.sort = this.sort;
    return result;
  }
  /* localizationConst = LocalizationConstants;
    isDeleteDisable(requestDetails){
        let isLFLRequest = false;
        let isSFLRequest = false;
        if(requestDetails.RT_REQUEST_TYPE_VALUE === LocalizationConstants.LOCALIZATION_REQUEST_TYPE_LFL ||
            requestDetails.REQUEST_TYPE_VALUE === LocalizationConstants.LOCALIZATION_REQUEST_TYPE_LFL){
            isLFLRequest = true;
        } else if(requestDetails.RT_REQUEST_TYPE_VALUE === LocalizationConstants.LOCALIZATION_REQUEST_TYPE_SFL ||
            requestDetails.REQUEST_TYPE_VALUE === LocalizationConstants.LOCALIZATION_REQUEST_TYPE_SFL){
            isSFLRequest = true;
        }
        let requestStatus = requestDetails.RT_REQUEST_STATUS ? requestDetails.RT_REQUEST_STATUS : requestDetails.REQUEST_STATUS;
        if (isLFLRequest || isSFLRequest) {
            if (isSFLRequest && !(requestStatus === LocalizationConstants.REQUEST_STATUS_NEW ||
                requestStatus === LocalizationConstants.REQUEST_STATUS_DRAFT || 
                requestStatus === LocalizationConstants.REQUEST_STATUS_INPROGRESS ||
                requestStatus === LocalizationConstants.REQUEST_STATUS_SOURCE_APPROVED ||
                requestStatus === LocalizationConstants.REQUEST_STATUS_SOURCE_REJECTED ||
                requestStatus === LocalizationConstants.REQUEST_STATUS_SOURCE_NOT_AVAILABLE ||
                requestStatus === LocalizationConstants.REQUEST_STATUS_PO_INPROGRESS)) {
                    this.notificationService.info("Request deletion is denied.");
                    return true;
            } else if(isLFLRequest && !(requestStatus === LocalizationConstants.REQUEST_STATUS_NEW)){
                this.notificationService.info("Request deletion is denied.");
                return true;
            }
            return false;
        } else {
            return true;
        }
    } */
  /* getDeleteColumnStyle(column) {
        const style = [];
        if(((column.RT_REQUEST_TYPE_VALUE === this.localizationConst.LOCALIZATION_REQUEST_TYPE_LFL ||
            column.REQUEST_TYPE_VALUE === this.localizationConst.LOCALIZATION_REQUEST_TYPE_LFL) &&
            !(column.RT_REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_NEW || column.REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_NEW)) ||
            ((column.RT_REQUEST_TYPE_VALUE === this.localizationConst.LOCALIZATION_REQUEST_TYPE_SFL ||
                column.REQUEST_TYPE_VALUE === this.localizationConst.LOCALIZATION_REQUEST_TYPE_SFL) &&
            !(column.RT_REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_DRAFT || column.REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_DRAFT ||
                column.RT_REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_NEW || column.REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_NEW ||
                column.RT_REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_INPROGRESS || column.REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_INPROGRESS ||
                column.RT_REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_SOURCE_APPROVED || column.REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_SOURCE_APPROVED ||
                column.RT_REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_SOURCE_REJECTED || column.REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_SOURCE_REJECTED ||
                column.RT_REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_SOURCE_NOT_AVAILABLE || column.REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_SOURCE_NOT_AVAILABLE ||
                column.RT_REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_PO_INPROGRESS || column.REQUEST_STATUS === this.localizationConst.REQUEST_STATUS_PO_INPROGRESS)) ||
            !(column.RT_REQUEST_TYPE_VALUE === this.localizationConst.LOCALIZATION_REQUEST_TYPE_LFL ||
                column.REQUEST_TYPE_VALUE === this.localizationConst.LOCALIZATION_REQUEST_TYPE_LFL ||
                column.RT_REQUEST_TYPE_VALUE === this.localizationConst.LOCALIZATION_REQUEST_TYPE_SFL ||
                column.REQUEST_TYPE_VALUE === this.localizationConst.LOCALIZATION_REQUEST_TYPE_SFL) ||
            !(this.currentUser === column.REQUESTOR_NAME || this.currentUser === column.RT_TASK_OWNER)) {
                style.push('delete-btn-change');
        }
        return style;
    }
    deleteRequest(event, requestDetails) {
        event.stopPropagation();
        if (!this.isDeleteDisable(requestDetails)) {
            this.confirmDelete((result) => {
                if (result) {
                    this.loaderService.show();
                    let LCRequestItemId = (requestDetails.RT_LOCALIZATION_REQUEST_ITEM_ID) ? requestDetails.RT_LOCALIZATION_REQUEST_ITEM_ID :
                        requestDetails.LOCALIZATION_REQUEST_ITEM_ID;
                    const parameters = {
                        localizationRequestItemId: LCRequestItemId
                    };
                    this.appService.invokeRequest(this.DELETE_REQUEST_METHOD_NS,
                        this.DELETE_REQUEST_METHOD_NAME, parameters).subscribe(response => {
                            if (response) {
                                this.notificationService.success('Request Deleted sucessfully.');
                                this.loaderService.hide();
                                this.refreshEmitter.emit();
                            }
                        }, error => {
                            this.notificationService.error('Error while deleting request');
                            this.loaderService.hide();
                        });
                }
            });
        }
    }
    */
  userDetails = JSON.parse(sessionStorage.getItem('USER_DETAILS_WORKFLOW')).User
    .ManagerFor.Target;
  isPM = this.filterConfigService.isProgramManagerRole();

  // Updating a request status to deleted.
  deleteReq(event, requestDetails) {
    event.stopPropagation();
    this.confirmDelete((result) => {
      if (result) {
        let requestIdSplit = requestDetails.ITEM_ID.split('.');
        let requestId = requestIdSplit[0] + '.' + requestIdSplit[1];
        let status = this.monsterUtilService.getStatusDisplayName(
          BusinessConstants.REQUEST_STATUS_DELETED
        );
        this.monsterUtilService.setFormProperty(
          requestId,
          'Request_Status',
          status.VALUE
        );
        this.monsterUtilService.finalBulkUpdateFormData(
          () => {
            this.refreshPage.emit();
            this.notificationService.success('Request deleted');
          },
          () => {
            this.notificationService.error(
              'Something went wrong while deleting the request.'
            );
          }
        );
      }
    });
  }

  //Bulk deleting from the gridview
  deletedMultipleReq() {
    this.selectedRows = [];
    this.selectedRows = this.projectDataDataSource.filteredData.filter(
      (request: any) => request.selected
    );
    if (this.selectedRows.length > 0) {
      this.confirmDelete((result) => {
        if (result) {
          this.selectedRows.forEach((element) => {
            // !((((row.RQ_REQUEST_STATUS || row.RT_REQUEST_STATUS) ===('InProgress' || 'Denied || Completed' ) || row.RQ_REQUEST_STATUS==='InDraft') && (isPM||isAdmin)) ||((row.RQ_REQUEST_STATUS==='InDraft' && row.RQ_REQUESTER===currentUser )))
            // if((((element.RQ_REQUEST_STATUS || element.RT_REQUEST_STATUS) ===('InProgress' || 'Denied' ) ||element.RQ_REQUEST_STATUS==="InDraft")&& (this.isPM || this.isAdmin))||
            // ((element.RQ_REQUEST_STATUS==="InDraft" && element.RQ_REQUESTER===this.currentUser))){
            // }
            if (
              ((element.RQ_REQUEST_STATUS === 'InProgress' ||
                element.RQ_REQUEST_STATUS === 'Denied' ||
                element.RQ_REQUEST_STATUS === 'Indraft' ||
                element.RT_REQUEST_STATUS === 'InProgress') &&
                (this.isPM || this.isAdmin)) ||
              (element.RQ_REQUEST_STATUS === 'InDraft' &&
                (element.RQ_REQUESTER === this.currentUser ||
                  this.isPM ||
                  this.isAdmin))
            ) {
            } else {
              this.conditionCount++;
            }
          });
          if (this.conditionCount === 0) {
            let status = this.monsterUtilService.getStatusDisplayName(
              BusinessConstants.REQUEST_STATUS_DELETED
            );
            this.selectedRows.forEach((element) => {
              let requestIdSplit = element.ITEM_ID.split('.');
              let requestId = requestIdSplit[0] + '.' + requestIdSplit[1];
              this.monsterUtilService.setFormProperty(
                requestId,
                'Request_Status',
                status.VALUE
              );
            });
            this.monsterUtilService.finalBulkUpdateFormData(
              () => {
                this.refreshPage.emit();
                this.notificationService.success('Request(s) deleted');
              },
              () => {
                this.notificationService.error(
                  'Something went wrong while deleting the request.'
                );
                this.monsterUtilService.clearFormPropertiesandRelations();
              }
            );
          } else if (this.conditionCount !== 0) {
            this.refreshPage.emit();
            this.notificationService.error(
              'Delete access denied for some request(s)!'
            );
          }
        }
      });
      this.conditionCount = 0;
    } else {
      this.notificationService.error('No requests selected!');
    }
  }

  confirmDelete(callback) {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '40%',
      disableClose: true,
      data: {
        message: 'Are you sure you want to delete the Request?',
        submitButton: 'Yes',
        cancelButton: 'No',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (callback) {
        callback(result ? result.isTrue : null);
      }
    });
  }

  // Copying an existing request
  copyReq(event, requestdetails) {
    event.stopPropagation();
    const projectId = this.getProperty(
      this.fieldConfigService.getFieldByKeyValue(
        MPMFieldKeys.MAPPER_NAME,
        this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID
      ),
      requestdetails
    );
    if (projectId && projectId !== 'NA') {
      this.loaderService.show();
      let data = {
        requestId: projectId.split('.')[1],
        request: requestdetails,
      };
      this.copyRequestButton.emit(data);
    } else {
      this.notificationService.error('Invalid Project Id.');
    }
  }

  // overrriding default implemnation to return the row object
  openProjectDetails(project) {
    const projectId = this.getProperty(
      this.fieldConfigService.getFieldByKeyValue(
        MPMFieldKeys.MAPPER_NAME,
        this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID
      ),
      project
    );
    if (projectId && projectId !== 'NA') {
      this.loaderService.show();
      let data = {
        requestId: projectId.split('.')[1],
        request: project,
      };
      this.viewProjectDetails.emit(data);
      this.technicalService.sendData(data);
    } else {
      this.notificationService.error('Invalid Project Id.');
    }
  }

  //Individual Task Operations - NPD1-67
  claimIndividualTask(event, taskDetails: any) {
    event.stopPropagation();
    this.claimTask.emit(taskDetails);
  }

  revokeIndividualTask(event, taskDetails: any) {
    event.stopPropagation();
    this.revokeTask.emit(taskDetails);
  }

  userAssignment(event, taskDetails: any, operation) {
    event.stopPropagation();
    taskDetails.operation = operation;
    this.assignTask.emit(taskDetails);
  }

  showDelegate(taskDetails) {
    //RT_TASK_OWNER !== currentUserDisplayName
    if (
      taskDetails &&
      taskDetails.RT_TASK_OWNER &&
      taskDetails.RT_TASK_OWNER &&
      taskDetails.RT_TASK_OWNER != 'NA'
    ) {
      //taskDetails.RT.TASK_OWNER == this.sharingService.getCurrentUserCN())
      if (taskDetails.RT_DELEGATED_TO && taskDetails.RT_DELEGATED_TO != 'NA') {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  //NPD1-83
  getColumnMaxWidth(column) {
    if (column.width) {
      return { 'max-width.px': column.width };
    } else {
      return { 'max-width.em': '8' };
    }
  }

  disableRevoke(row) {
    if (
      row?.RT_TASK_SUBJECT === BusinessConstants.TASK_ACTIVITY_E2EPM ||
      row?.RT_TASK_SUBJECT === BusinessConstants.TASK_ACTIVITY_REQUEST_REWORK
    ) {
      return true;
    }
    return false;
  }
}
