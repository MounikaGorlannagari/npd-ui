import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NotificationService, LoaderService, UtilService, SharingService } from 'mpm-library';
import { InboxService } from '../inbox.service';

@Component({
  selector: 'app-inbox-assignment-modal',
  templateUrl: './inbox-assignment-modal.component.html',
  styleUrls: ['./inbox-assignment-modal.component.scss']
})
export class InboxAssignmentModalComponent implements OnInit {

  selectedUser = '';
  transferOwnership = true;
  userSource = [];
  users = [];
  actualUsers = [];
  userId = '';

  constructor( @Inject(MAT_DIALOG_DATA) public data: any,
               private inboxService: InboxService,
               private notificationService: NotificationService, private loaderService: LoaderService,
               public dialogRef: MatDialogRef<InboxAssignmentModalComponent>,
               private utilService: UtilService,private sharingService: SharingService) { }

  fetchUsers(){
    this.loaderService.show();
    this.inboxService.fetchUsersForTask(this.data.tasks,  (response) => {
       if (response == null){
            this.notificationService.error('Error while retrieving users for the selected Task(s), Please try again.');
            this.loaderService.hide();
       }
       else{
           this.userSource = response;
          //  console.log('currentuserId',this.sharingService.getCurrentUserDN())
           const currentUserCN = this.sharingService.getCurrentUserDN();
           response.forEach(user => {
            if(currentUserCN !== user.Id){
              this.actualUsers.push(user.Description + ' (' + user.Id.split(',')[0].split('=')[1] + ')');
            }
           });
           this.users = JSON.parse(JSON.stringify(this.actualUsers));
           this.loaderService.hide();
           if(this.userSource.length === 0){
              this.notificationService.error('No users found for the selected Task. Unable to assign or delegate.');
           }
       }
    });
  }

  filterUsers(){
     this.users = this.actualUsers.filter((user) => {
       return user.toLowerCase().includes(this.selectedUser.toLowerCase());
     });
  }

  validateUser(){
      if (this.userSource.length && this.userSource.length > 0){
           this.userSource.forEach((user) => {
               if (this.selectedUser.indexOf('(') !== -1 && user.Id.indexOf(',') !== -1
               && user.Id.indexOf('=') !== -1 && user.Id.split(',')[0].split('=')[1] === this.selectedUser.split('(')[1].split(')')[0]){
                    this.userId = user.Id;
               }
           });
           if (!this.utilService.isValid(this.userId)){
             return false;
           }
           else{
             return true;
           }
      }
      else{
        return false;
      }
  }

  completeOperation(){
      if (!this.validateUser()){
           this.notificationService.error('Please select a valid user from dropdown');
           return;
      }
      this.dialogRef.close({
        operation : this.data.operation,
        userId : this.userId,
        transferOwnership : this.transferOwnership
      });
  }

  cancelOperation(){
    this.dialogRef.close({
      operation : this.data.operation,
      userId : '',
      transferOwnership : this.transferOwnership
    });
  }

  ngOnInit(): void {
    this.fetchUsers();
  }

}
