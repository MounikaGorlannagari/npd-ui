import { ApplicationConfigConstants } from 'mpm-library';

export const RequestDashboardControlConstant = {
    dashboardControls: [
        'newRequestType'
    ],
    newRequestType: {
        fieldName: 'newRequestType',
        inputType: 'singleSelect',
        label: 'New',
        placeholder: 'Create new Request',
        defaultValue: '',
        appDynamicConfigId:  ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_CREATE_PROJECT,
        isShown: '',
        options: []
    }
};
