import { IndexerDataTypes, MPMSearchOperators, MPMSearchOperatorNames, CustomSelect } from 'mpm-library';
import { TaskListDependentFilterTypes } from 'src/app/core/mpm/deliverables-task-view/taskListfilters/TaskListDependentFilterTypes';

export const RequestConstant = {

    CATEGORY_LEVEL_REQUEST: 'REQUEST',
    CATEGORY_LEVEL_REQUEST_TASK: 'REQUEST_TASK',
    STATUS_TYPE_DRAFT: 'DRAFT',
    NAME_STRING_PATTERN: '^[a-zA-Z0-9 _().-]*$',
    REQUEST_METADATA_GROUP: '',
    METADATA_EDIT_TYPES: {
        SIMPLE: 'SIMPLE',
        DATE: 'DATE',
        TEXTAREA: 'TEXTAREA',
        COMBO: 'COMBO',
        NUMBER: 'NUMBER',
    },
    GET_REQUEST_SEARCH_CONDITION_LIST: [
    {
        type: IndexerDataTypes.STRING,
        field_id: 'CONTENT_TYPE',
        relational_operator_id: MPMSearchOperators.IS,
        relational_operator_name: MPMSearchOperatorNames.IS,
        value: 'REQUEST',
    },
    {
        type: IndexerDataTypes.STRING,
        field_id: 'REQUEST_ENTITY_ITEM_ID',
        relational_operator_id: MPMSearchOperators.IS_NOT,
        relational_operator_name: MPMSearchOperatorNames.IS_NOT,
        relational_operator: MPMSearchOperatorNames.AND,
        value: 'NA',
    },
    {
        type: IndexerDataTypes.STRING,
        field_id: 'REQUEST_ENTITY_ITEM_ID',
        relational_operator_id: MPMSearchOperators.IS_NOT_EMPTY,
        relational_operator_name: MPMSearchOperatorNames.IS_NOT_EMPTY,
        relational_operator: MPMSearchOperatorNames.AND,
        value: '',
    }
],
    GET_REQUEST_TASK_SEARCH_CONDITION_LIST: [{
        type: IndexerDataTypes.STRING,
        field_id: 'CONTENT_TYPE',
        relational_operator_id: MPMSearchOperators.IS,
        relational_operator_name: MPMSearchOperatorNames.IS,
        value: 'REQUEST_TASK'
    }],
    GET_MATERIAL_REQUEST_TASK_SEARCH_CONDITION_LIST: [{
        type: IndexerDataTypes.STRING,
        field_id: 'RT_SUBJECT',
        relational_operator_id: MPMSearchOperators.IS,
        relational_operator_name: MPMSearchOperatorNames.IS,
        value: 'Materials Vendor Task'
    }],
    
    SFL_REQUEST_TYPE_VALUE : 'SHORT_FORM_LOCALIZATION',
    LFL_REQUEST_TYPE_VALUE : 'LONG_FORM_LOCALIZATION',
    VERSION_REQUEST_TYPE_VALUE : 'VERSIONING_REQUEST',
    MATERIAL_REQUEST_TYPE_VALUE :'MATERIAL'
};


export function getTaskListDependentFilters(): any {
    const TO_DO_FILTER = {
        value: TaskListDependentFilterTypes.TO_DO_FILTER,
        name: 'To Do',
        default: true,
        searchOperatorName : MPMSearchOperatorNames.IS_NOT,
        searchOperator : MPMSearchOperators.IS_NOT
    };

    const COMPLETED = {
        value: TaskListDependentFilterTypes.COMPLETED,
        name: 'Completed',
        default: false,
        searchOperatorName : MPMSearchOperatorNames.IS,
        searchOperator : MPMSearchOperators.IS
    };

    type TaskListDependentFilter = { [key in FilterTypes]?: Array<CustomSelect> };

    const TASK_LIST_DEPENDENT_FILTER: TaskListDependentFilter = {};

    TASK_LIST_DEPENDENT_FILTER[FilterTypes.MY_TASKS] = [TO_DO_FILTER, COMPLETED];
    TASK_LIST_DEPENDENT_FILTER[FilterTypes.PROGRAM_MANAGER_APPROVAL] = [TO_DO_FILTER, COMPLETED];
    TASK_LIST_DEPENDENT_FILTER[FilterTypes.E2E_PM_CLASSIFICATION] = [TO_DO_FILTER, COMPLETED];
    TASK_LIST_DEPENDENT_FILTER[FilterTypes.BUSINESS_DEVELOPMENT_APPROVAL] = [TO_DO_FILTER, COMPLETED];
    return TASK_LIST_DEPENDENT_FILTER;
}

export enum FilterTypes {
    MY_TASKS = 'MY_TASKS',
    PROGRAM_MANAGER_APPROVAL = 'PROGRAM_MANAGER_APPROVAL',
    E2E_PM_CLASSIFICATION = 'E2E_PM_CLASSIFICATION',
    BUSINESS_DEVELOPMENT_APPROVAL = 'BUSINESS_DEVELOPMENT_APPROVAL'
}

