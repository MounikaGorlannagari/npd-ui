export const TaskConstant = {

    TASK_STATE: {
        CREATED: "1",
        ASSIGNED: "2",
        INPROGRESS: "3",
        SUSPENDED: "4",
        COMPLETED: "5",
        PAUSED: "7",
        OBSOLETE: "8",
        INTERNAL_PAUSE: "10"
    }
}