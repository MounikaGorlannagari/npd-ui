import { Routes, Router, RouterModule } from '@angular/router';
import { RequestorDashboardComponent } from './requestor-dashboard/requestor-dashboard.component';
import { NgModule } from '@angular/core';


const routes: Routes = [
    {
        path: '',
        component: RequestorDashboardComponent

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RequestManagementRouting {    
}
