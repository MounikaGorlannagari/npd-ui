import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { AppService, SharingService, UtilService, TaskService, EntityAppDefService, LoaderService, ProjectUtilService, NotificationService , CommentsUtilService, ConfirmationModalComponent} from 'mpm-library';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MPMTaskCreationComponent } from 'src/app/core/mpm-lib/project/tasks/task-creation/task-creation.component';
import { MonsterUtilService } from '../../services/monster-util.service';
// import { CommentsUtilService } from 'mpm-library';
import { NpdTaskService } from '../services/npd-task.service';
// import { MPMTaskCreationComponent } from 'src/app/core/mpm-lib/project/tasks/task-creation/task-creation.component';

@Component({
  selector: 'app-bulk-edit-tasks',
  templateUrl: './bulk-edit-tasks.component.html',
  styleUrls: ['./bulk-edit-tasks.component.scss']
})
export class BulkEditTasksComponent extends MPMTaskCreationComponent implements OnInit {
  abcd: boolean=true;
  isFormValid: boolean =false;

  constructor(
    public dialogRef : MatDialogRef<BulkEditTasksComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    public appService: AppService,
    public sharingService: SharingService,
    public adapter: DateAdapter<any>,
    public utilService: UtilService,
    public taskService: TaskService,
    public entityAppdefService: EntityAppDefService,
    public loaderService: LoaderService,
    public dialog: MatDialog,
    public projectUtilService: ProjectUtilService,
    public commentsUtilService: CommentsUtilService,
    public notificationService: NotificationService,
    public npdTaskService: NpdTaskService,
    public monsterUtilService :MonsterUtilService
  ) {
    super(
      appService,
      sharingService,
      adapter,
      utilService,
      taskService,
      entityAppdefService,
      loaderService,
      dialog,
      projectUtilService,
      commentsUtilService,
      notificationService
    );
  }

  TaskBulkOperations_IM = {
    TaskBulkOperations:{
      Identity: [],
      ActualStartDate:null,
      ActualCompletionDate:'',
      Duration:'',
      TaskOwner:null,
      TaskOperation:'',
      TaskRMRole:''
    }
  }  

  taskOwnerList=[];
  @Output() reloadPage: EventEmitter<any>= new EventEmitter<any>();
  filteredOptions: Observable<string[]>;

  taskOwnersObject=[]

  ngOnInit(): void {
    this.npdTaskService.getUsersForRole(this.data.taskRole).subscribe((response)=>{               
    response.tuple.forEach((item)=>{
      const DisplayName=item.old.entry.description.string
      const value=item.old.entry.cn.string
      const taskOwnerObject={
          DisplayName :DisplayName,
          Value: value
      }
      this.taskOwnerList.push(DisplayName);
      this.taskOwnersObject.push(taskOwnerObject)
    });

    this.filteredOptions = this.bulkEditTaskForm.controls.taskOwner.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value || '')),
    );    
  }) 
  this.data.reload='reload'
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.taskOwnersObject.filter(option => option.DisplayName.toLowerCase().includes(filterValue));
  }


  bulkEditTaskForm = new FormGroup({
    actualStartDate: new FormControl(),
    duration:new FormControl(),
    taskOwner: new FormControl()
  });

  // maxDuration=10;

  weekYear = {
    actualStartDate: null,
  };

  fromDate = {
    actualStartDate: null,
  };

  toDate = {
    actualStartDate: null,
  }

  disable ={
    actualStartDate: null,
  }


  show ={
    actualStartDate: false,
  }

  closeWeekPicker(){
    this.show.actualStartDate = false;
  }

  changeTargetDP(weekFormat,date){
    this.weekYear[date]=weekFormat;
    if(date == 'actualStartDate'){
      this.bulkEditTaskForm.patchValue({actualStartDate:weekFormat});
    }else{
      this.bulkEditTaskForm.patchValue({[date]:weekFormat});
      this.bulkEditTaskForm.controls[date].updateValueAndValidity();
    }
    this.bulkEditTaskForm.markAsDirty();
    [this.fromDate[date], this.toDate[date]]= this.getWeekFormat(weekFormat)
    this.show[date] = !this.show[date];
  }

  getWeekFormat(weekFormat){
    if(weekFormat && weekFormat !==undefined){
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates: NgbDate[] = this.calculateDateNgb(week, year);
      return [weekdates[0], weekdates[1]];
    }
  }

  calculateDateNgb(week: number, year: number): NgbDate[] {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate, ngbToDate];
  }

  getBool(date) {
    this.show[date] = !this.show[date];
  }

  getAllBool() {
    Object.keys(this.show).map((ele) => {
      this.show[ele] = false;
    });
  }

  openWeekPicker(date) {
    this.getAllBool();
    this.show[date] = !this.show[date];
  }

  getStopPropogation(event,date){
    if(this.show[date]){
      return event.stopPropagation();
    }
  }

  
  onFocusOut(event) {
    if (
      event.target.value.trim() === '' &&
      (!this.bulkEditTaskForm.get('taskOwner') || !this.bulkEditTaskForm.get('taskOwner')?.errors)
    ) { 
      return;
    } else {
      if (this.taskOwnerList) {
        if (!this.taskOwnerList.some((e) => e === event.target.value)) {
          this.bulkEditTaskForm.get('taskOwner').setErrors({ incorrect: true });
        } else {
          this.bulkEditTaskForm.get('taskOwner').setErrors(null);
        }
      }
      // this.isFormValid=false

    }
  }



  onClickSubmit(formData){
    this.confirmBulkEdit((result)=>{
      if(result){
        this.loaderService.show();
        if(formData.actualStartDate){
          let startDate = this.monsterUtilService.getStartEndDate(formData.actualStartDate);
          const isoStartDate =  this.npdTaskService.getIsoFormatDateString(startDate[0]);
          this.TaskBulkOperations_IM.TaskBulkOperations.ActualStartDate = isoStartDate;
         }
         if(formData.taskOwner){
          const taskOwnerValue= this.taskOwnersObject.filter(a=>a.DisplayName == formData.taskOwner);
          this.TaskBulkOperations_IM.TaskBulkOperations.TaskOwner = taskOwnerValue[0].Value;
         }
         this.TaskBulkOperations_IM.TaskBulkOperations.Duration = formData.duration;
        this.TaskBulkOperations_IM.TaskBulkOperations.TaskOperation=this.data.tasksSelected.TaskBulkOperations.TaskOperation;
        this.TaskBulkOperations_IM.TaskBulkOperations.Identity = this.data.tasksSelected.TaskBulkOperations.Identity;
        this.TaskBulkOperations_IM.TaskBulkOperations.TaskRMRole = this.data.tasksSelected.TaskBulkOperations.TaskRMRole;
        this.npdTaskService.taskBulkEdit(this.TaskBulkOperations_IM).subscribe(()=>{
        this.notificationService.success("Task Bulk Operation has been triggered")
        },()=>{
          this.notificationService.error('Something went wrong during Task Bulk Operation.');
          this.TaskBulkOperations_IM=null;
        })
        this.closeDialog();
        this.loaderService.hide();
      }
    })
  }


  confirmBulkEdit(callback) {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '40%',
      disableClose: true,
      data: {
        message: 'Do you want to take this action on all tasks selected?',
        submitButton: 'Yes',
        cancelButton: 'No'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (callback) { callback(result ? result.isTrue : null) };
    });
  }

  closeDialog() {
    this.dialogRef.close(this.data.reload); 
  }
  numericPattern = /^[0-9]*$/;
  numericOnly(event){
     return this.numericPattern.test(event.key);
  }

  validateForm(formControl){
    let validTaskOwner =''
    let validDuration =null
    let validActualStartDate =''
    if(formControl=='taskOwner'){
     validTaskOwner =  this.bulkEditTaskForm?.controls?.[formControl]?.value;
    }else if (formControl=='duration'){
       validDuration =  this.bulkEditTaskForm?.controls?.[formControl]?.value;
    }else if(formControl=='actualStartDate'){
       validActualStartDate =  this.bulkEditTaskForm?.controls?.[formControl]?.value;
    }
   if(validDuration==0){
      this.isFormValid = false
    }else  if(validTaskOwner.length>0 || validDuration!=null || validActualStartDate.length >0 ){
      this.isFormValid = true
    }else{
      this.isFormValid =false
    }
  }
}

