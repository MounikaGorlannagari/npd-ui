import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoaderService, NotificationService } from 'mpm-library';
import { NpdProjectService } from '../../npd-project-view/services/npd-project.service';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { TaskConfigConstant } from '../constants/TaskConfig';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-missing-tasks',
  templateUrl: './missing-tasks.component.html',
  styleUrls: ['./missing-tasks.component.scss']
})
export class MissingTasksComponent implements OnInit {

  constructor(private notificationService: NotificationService, private loaderService: LoaderService,
    private npdProjectService: NpdProjectService, @Inject(MAT_DIALOG_DATA) public dialogData: any,
    public dialogRef: MatDialogRef<MissingTasksComponent>, private monsterConfigApiService: MonsterConfigApiService
  ) { }



  modalLabel;
  selectedTasks;
  notCreatedTask;
  projectId;
  taskList = [];
  classificationNumber: string = '';



  closeDialog() {
    this.dialogRef.close();
  }

  getTaskName(task) {
    return task.taskName + ' - ' + task.taskDescription
  }
  addTasks() {
    if (!(this.selectedTasks?.length > 0)) {
      this.notificationService.warn('Please select a task to create');
      return;
    };
    let tasks = [];
    this.selectedTasks.forEach(element => {
      tasks.push(
        {
          TaskName: element.taskName
        }
      )
    });
    this.loaderService.show();
    this.npdProjectService.initiateTasksCreationWorkflow(tasks, this.projectId, this.classificationNumber).subscribe(response => {
      this.notificationService.success('Task(s) creation is initiated');
      this.dialogRef.close(true);
    }, (error) => {
      this.loaderService.hide();
      this.notificationService.error('Something went wrong while task(s) creation')
      this.dialogRef.close();
    });
  }

  // fetchnotCreatedTask() {
  //   this.loaderService.show();
  //   this.npdProjectService.getTasksNotCreatedDetails(this.projectId).subscribe(response => {
  // this.notCreatedTask = this.monsterConfigApiService.getListAsResponse(response);
  //     this.loaderService.hide();
  //   }, (error) => {
  //     this.loaderService.hide();
  //   });
  // }



  mapData() {
    this.modalLabel = this.dialogData.modalLabel;
    this.projectId = this.dialogData.projectId;
    this.notCreatedTask = this.dialogData.taskList;
    this.classificationNumber = this.dialogData.classificationNumber;

  }


  ngOnInit(): void {
    this.mapData();
    // this.fetchnotCreatedTask();
  }

}
