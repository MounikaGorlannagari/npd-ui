import { I } from '@angular/cdk/keycodes';
import { Component, OnInit, Renderer2 } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AssetService, ConfirmationModalComponent, FieldConfigService, FormatToLocalePipe, IndexerDataTypes, LoaderService, MPMField, MPMFieldConstants, MPMFieldKeys, MPM_LEVELS, NotificationService, ProjectService, SharingService, TaskService, UtilService } from 'mpm-library';
import { MPMTaskListViewComponent } from 'src/app/core/mpm-lib/project/tasks/task-list-view/task-list-view.component';
import { CommentsModalComponent } from '../../collab/comments-modal/comments-modal.component';
import { CommentConfig } from '../../collab/objects/CommentConfig';
import { FilterConfigService } from '../../filter-configs/filter-config.service';
import { WorkflowRuleService } from '../../npd-shared/services/workflow-rule.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { NpdTaskService } from '../services/npd-task.service';

@Component({
  selector: 'app-npd-task-list-view',
  templateUrl: './npd-task-list-view.component.html',
  styleUrls: ['./npd-task-list-view.component.scss']
})
export class NpdTaskListViewComponent extends MPMTaskListViewComponent {

  constructor(
    public sharingService: SharingService,
    public taskService: TaskService,
    public formatToLocalePipe: FormatToLocalePipe,
    public utilService: UtilService,
    public fieldConfigService: FieldConfigService,
    public assetService: AssetService,
    public loaderService: LoaderService,
    public notificationService: NotificationService,
    public projectService: ProjectService,
    public dialog: MatDialog,
    public renderer: Renderer2,
    public monsterUtilService:MonsterUtilService,
    public npdTaskService:NpdTaskService,
    public filterConfigService:FilterConfigService

  ) {
  super(sharingService,taskService,formatToLocalePipe,utilService,
     fieldConfigService,assetService,loaderService,notificationService,projectService,dialog,renderer);
  }
  cancelledProject:boolean
  getSpecifiedRoleAccess() {
    return this.filterConfigService.isCorpPMRole() || this.filterConfigService.isE2EPMRole() || this.filterConfigService.isOPSPMRole() || this.filterConfigService.isProgramManagerRole();
  }

  openCommentDialog(selectedTask) {
    // console.log('selectedTask',selectedTask)
    //this.enableToComment = this.getProperty(this.deliverableData, this.MPMFeildConstantMapper.DELIVERABLE_STATUS) !== this.assetStatusConstants.COMPLETED;
    // const taskId: string = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ID);
    const requestId=selectedTask.PR_REQUEST_ITEM_ID.split('.')[1]
    const taskItemId: string = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID).split('.')[1];
    const projectId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_PROJECT_ID)?.split('.')[1];//selectedTask['PROJECT_ITEM_ID']?.split('.')[1];
    //this.getPropertyByMapper(selectedTask, this.mpmCustomFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID)?.split('.')[1];
    // console.log('proId'+projectId);
    const dialogRef = this.dialog.open(CommentsModalComponent, {
        width: '75%',
        height: '90vh',
        maxHeight: '90vh',
        minHeight: '90vh',
        panelClass: 'comment-dialog-modal',
        disableClose: true,
        data: {
            requestId: requestId,
            projectId: projectId,
            taskId:taskItemId,
            commentType:  CommentConfig.TASK_COMMENT_TYPE_VALUE,
            //isManager: this.isPMView,
            enableToComment: true // this.enableToComment
        }
    });
    dialogRef.afterClosed().subscribe(result => {
        // console.log('comments module dialog is closed \n', result);
    });
  }

  changeStatusColor(task,status) {
    let style;
    let isOverdue = this.isOverdue(task);
    if(status=== "Cancelled"){
      style = "cancelled-status"
      return style;
    }else if(isOverdue &&  status!=="Approved/Completed") {
      style = "delete-status";
      return style;
    }
    else{
    return this.monsterUtilService.changeTaskStatusColor(status);
    }
  }
  //To check if the task duration
  isOverdue(taskData) {
    let currentdate = new Date();
    let estimatedEndDate= new Date(taskData.NPD_TASK_ESTIMATED_COMPLETION_DATE)
    currentdate.setHours(23,59,59,998);
    return currentdate>estimatedEndDate;
}
  getStatus(displayColumn,data) {
    let status = this.monsterUtilService.getStatusDisplayName(this.getProperty(displayColumn, data));//this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, data)
    return status && status.DISPLAY_NAME;
  }

   openTaskDetailsPopup(task) {
    // if(this.taskConfig.isReadOnly || this.isOnHoldProject) {
    //   return;
    // }
    // console.log(this.taskConfig)
    if(this.taskConfig?.projectStatusId=='16399'){
      this.cancelledProject=true;
      return
    }
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",task);

    this.npdTaskService.setEditTaskHeaderData(task)
    this.editTask(task);
  }

  editTask(selectedTask){
    const taskItemId: string = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
        const isApprovalTask = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_IS_APPROVAL);
        this.taskConfig.isApprovalTask = isApprovalTask === 'true' ? true : false;
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            this.taskConfig.taskId = taskItemId.split('.')[1];
            this.editTaskHandler.next(this.taskConfig);
            // this.refresh();
        } else if (taskItemId && !(taskItemId.includes('.'))) {
            this.taskConfig.taskId = taskItemId;
            this.editTaskHandler.next(this.taskConfig);
            // this.refresh();
        }
  }

  checkDateType(displayColumn) {
    return displayColumn?.DATA_TYPE?.toLowerCase() === IndexerDataTypes.DATETIME?.toLowerCase()
  }

  getWeek(column, row) {
    let date = this.monsterUtilService.getFieldValueByDisplayColumn(column,row);
    if (date && column?.DATA_TYPE.toLowerCase() === IndexerDataTypes.DATETIME.toLowerCase()) {
        return this.monsterUtilService.calculateWeek(date);
    }
  }

  refreshData(taskConfig) {
    this.displayableMPMFields = this.taskConfig.viewConfig ? this.utilService.getDisplayOrderData(this.displayableFields, this.taskConfig.viewConfig.listFieldOrder) : [];
    this.displayColumns = this.displayableMPMFields.map((column: MPMField) => column.INDEXER_FIELD_ID);
    this.displayColumns = ['Active'].concat(this.displayColumns);

    /* if (this.isProjectOwner || (this.currentUserId && this.taskConfig.selectedProject &&
        this.currentUserId === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id)) {
        this.displayColumns.push('Actions');
    } else if (!this.taskConfig.selectedProject) {
        this.displayColumns.push('Actions');
    } */
    this.displayColumns.push('Actions');

    if (this.taskConfig.selectedProject && this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW) {
        this.isCustomWorkflow = this.utilService.getBooleanValue(this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW);
    }
    // this.taskConfig = taskConfig;
    this.taskListDataSource.data = [];
    this.taskListDataSource.data = this.taskConfig.taskList;
    if (this.sort && this.taskConfig.taskListSortFieldName && this.taskConfig.taskListSortFieldName != 'IS_ACTIVE_TASK') {
      this.sort.active = this.taskConfig.taskListSortFieldName;
      this.sort.direction = this.taskConfig.taskListSortOrder.toLowerCase();
    }
    this.taskListDataSource.sort = this.sort;
   }

    deleteTask(selectedTask,isForceDelete:boolean) {
        const taskId = selectedTask.ID;
        let projectId = selectedTask?.PROJECT_ITEM_ID?.split('.')[1]
        let msg;
        if (this.isCustomWorkflow) {
            msg = 'Are you sure you want to delete the tasks and reconfigure the workflow rule engine?';
        } else {
            msg = 'Are you sure you want to delete the tasks and Exit?';
        }
        //  MPMV3-2094
        this.taskService.getAllApprovalTasks(taskId).subscribe(approvalTask => {
            // console.log(approvalTask)
            if (approvalTask.tuple != undefined) {
                this.forceDeletion = true;
                this.affectedTask = [];
                let tasks;
                if (!Array.isArray(approvalTask.tuple)) {
                    tasks = [approvalTask.tuple];
                } else {
                    tasks = approvalTask.tuple;
                }
                tasks.forEach(task => {
                    if(task.old.TaskView.isDeleted === 'false') {
                        this.affectedTask.push(task.old.TaskView.name);
                    }
                });
            }
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    name: this.affectedTask,
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    if(isForceDelete) {
                        this.npdTaskService.forceDeleteTask(taskId, this.isCustomWorkflow).subscribe(
                            response => {
                                this.notificationService.success('Task has been deleted successfully');
                                this.refresh();
                            }, error => {
                                this.notificationService.error('Delete Task operation failed, try again later');
                            });
                    } else {
                            this.taskService.softDeleteTask(taskId, this.isCustomWorkflow).subscribe(
                                response => {
                                    this.npdTaskService.triggerNPDWorkflow(projectId).subscribe(response => {
                                      this.notificationService.success('Task has been deleted successfully');
                                      this.refresh();
                                    }, error => {
                                      this.notificationService.error('Delete Task operation failed, try again later');
                                    });
                                }, error => {
                                    this.notificationService.error('Delete Task operation failed, try again later');
                                });
                    }
                }
            });
        });
    }
    isCancelledProject(): boolean {
        return this.taskConfig?.projectStatusId=='16399'
    }

    openEditTaskAction(selectedTask){
      this.npdTaskService.setEditTaskHeaderData(selectedTask);
      this.editTask(selectedTask)
    }
}
