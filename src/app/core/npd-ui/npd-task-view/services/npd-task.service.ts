import { I } from '@angular/cdk/keycodes';
import { ObserversModule } from '@angular/cdk/observers';
import { Injectable } from '@angular/core';
import * as moment_ from 'moment';
import { ApplicationConfigConstants, AppService, FieldConfigService, SessionStorageConstants, SharingService, TaskConstants } from 'mpm-library';
import { Observable, Subject } from 'rxjs';
import { OverViewConstants } from '../../npd-project-view/constants/projectOverviewConstants';
import { NpdResourceManagementService } from '../../npd-resource-management/services/npd-resource-management.service';
import { NpdTaskUpdateObj } from '../constants/npd-task.update';

const moment = moment_;
@Injectable({
  providedIn: 'root',
})
export class NpdTaskService {
  TASK_ACTION_STATUS_HANDLER_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  TASK_ACTION_STATUS_HANDLER_WS_METHOD_NAME = 'TaskStatusHandler';

  TASK_BPM_NS = 'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  EDIT_TASK = 'AfterTaskEditActions';

  NPDTask_BY_MPMTask_NS = 'http://schemas/NPDSubmission/NPD_Task/operations';
  NPDTask_BY_MPMTask = 'GetNPDTaskByMPMTaskId';

  GET_PRIMARY_DEPENDENCY_RULES_NS =
    'http://schemas.npd.monster.com/jobrequest/1.0';
  GET_PRIMARY_DEPENDENCY_RULES = 'GetPrimaryDependencyRulesByProject';

  HANDLE_WORKFLOW_RULES_INITIATION_METHOD_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  HANDLE_WORKFLOW_RULES_INITIATION_METHOD_WS = 'HandleCustomWorkflowRules';

  TASK_BULK_EDIT_OPERATIONS_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  TASK_BULK_EDIT_OPERATIONS_WS = 'TaskBulkOperations';
  // CREATE_NPD_TASK_NS = 'http://schemas/NPDSubmission/NPD_Task/operations';
  // CREATE_NPD_TASK = 'CreateNPD_Task';

  UPDATE_NPD_TASK_SEQUENCE_NS =
    'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  UPDATE_NPD_TASK_SEQUENCE_WS = 'UpdateNpdTaskSequence';

  GET_NPD_TASK_SEQUENCE_NS =
    'http://schemas/NPDSubmission/NPD_Task_Sequence/operations';
  GET_NPD_TASK_SEQUENCE_WS = 'GetNPDTaskSequenceByProjectId';

  DELETE_WORKFLOW_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
  FORCE_DELETE_TASK = 'ForceDeleteTask';

  GET_WORKFLOW_RULES_BY_TARGET_TASK_ID_NS =
    'http://schemas.npd.monster.com/jobrequest/1.0';
  GET_WORKFLOW_RULES_BY_TARGET_TASK_ID_WS = 'GetNpdWorkflowRulesByTargetTaskId';
  appConfig;
  projectTaskViewFields=null;

  private editTaskHeaderData = new Subject<any>();

  constructor(
    private appService: AppService,
    private npdResourceManagementService: NpdResourceManagementService,
    private sharingService: SharingService,
    private fieldConfigService: FieldConfigService
  ) { }

  taskStatusChange(taskId, action): Observable<any> {
    return new Observable((observer) => {
      const taskStatusObj = {
        taskStatusHandler: {
          taskId: taskId,
          action: action,
        },
      };
      //this.loaderService.show();
      this.appService
        .invokeRequest(
          this.TASK_ACTION_STATUS_HANDLER_NS,
          this.TASK_ACTION_STATUS_HANDLER_WS_METHOD_NAME,
          taskStatusObj
        )
        .subscribe(
          (response) => {
            if (response && response.Task) {
              const taskObj = response.Task;
            }
            //this.loaderService.hide();
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
            //this.loaderService.hide();
          }
        );
    });
  }

  updateTask(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(this.TASK_BPM_NS, this.EDIT_TASK, parameters)
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  fetchCustomTaskDetails(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.NPDTask_BY_MPMTask_NS,
          this.NPDTask_BY_MPMTask,
          parameters
        )
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getPrimaryWorkflowRulesByProject(projectId,classificationNumber): Observable<any> {
    let parameters = {
      ProjectId: projectId,
      classificationNumber:classificationNumber
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.GET_PRIMARY_DEPENDENCY_RULES_NS,
          this.GET_PRIMARY_DEPENDENCY_RULES,
          parameters
        )
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  checkValueIsNullOrEmpty(data: any, defaultValue: any): any {
    if (
      !data ||
      data['@xsi:nil'] === 'true' ||
      data === '' ||
      data['@nil'] === 'true'
    ) {
      return defaultValue;
    }
    return data;
  }

  checkDataValueIsNullOrEmpty(data: any, defaultValue: any): any {
    if (
      !data ||
      data['@xsi:nil'] === 'true' ||
      data === '' ||
      data['@nil'] === 'true' ||
      data === 'NA'
    ) {
      return defaultValue;
    }
    return data;
  }

  checkBooleanValueIsNullOrEmpty(data: any, defaultValue: any): any {
    if (
      !data ||
      data['@xsi:nil'] === 'true' ||
      data === '' ||
      data['@nil'] === 'true'
    ) {
      return defaultValue;
    } else if (data == 'true') {
      return true;
    } else {
      return false;
    }
  }

  checkIndexerBooleanValueIsNullOrEmpty(data: any, defaultValue: any): any {
    if (data == 'NA' || !data) {
      return defaultValue;
    } else if (data == 'true') {
      return true;
    } else if (data == 'false') {
      return false;
    } else {
      return null;
    }
  }

  checkClassificationIndexerBooleanValueIsNullOrEmpty(
    data: any,
    defaultValue: any
  ): any {
    if (data == 'NA' || data == '') {
      return defaultValue;
    } else if (data == 'true' || data == true) {
      return true;
    } else if (data == 'false' || data == false) {
      return false;
    } else {
      return null;
    }
  }

  validateClassificationIndexerBooleanValueIsNullOrEmpty(
    data: any,
    defaultValue: any
  ): any {
    if (data == 'true' || data == true) {
      return true;
    } else if (data == 'false' || data == false) {
      return true;
    } else {
      return null;
    }
  }

  getIsoFormatDateString(dateObject) {
    let dateString;
    const newDate = dateObject;
    const year = newDate.getFullYear();
    const actualMonth = newDate.getMonth() + 1;
    const month =
      actualMonth.toString().length <= 1 ? '0' + actualMonth : actualMonth;
    const date =
      newDate.getDate().toString().length <= 1
        ? '0' + newDate.getDate()
        : newDate.getDate();
    return year + '-' + month + '-' + date + 'T00:00:00.000Z';
  }

  createNewTaskUpdateObject(
    taskValue: any,
    taskType,
    isApprovalTask,
    projectId,
    reviewers
  ): NpdTaskUpdateObj {
    const newTask: NpdTaskUpdateObj = {
      TaskId: {
        Id: null,
      },
      TaskName: taskValue.taskName
        ? taskValue.taskName.replace(/\s+/g, ' ')
        : taskValue.taskName,
      Description: taskValue.description,
      /* StartDate: taskValue.startDate,
    DueDate: taskValue.endDate,
    OriginalStartDate: taskValue.OriginalStartDate,
    OriginalDueDate: taskValue.OriginalEndDate, */
      StartDate: this.getIsoFormatDateString(
        new Date(taskValue.OriginalStartDate)
      ),
      DueDate: this.getIsoFormatDateString(new Date(taskValue.OriginalEndDate)),
      //ActualStartDate: taskValue.startDate,
      //ActualDueDate: taskValue.endDate,
      TaskDuration: taskValue.duration,
      TaskDurationType: taskValue.durationType,
      ParentTaskID:
        taskValue.predecessor && taskValue.predecessor.value
          ? taskValue.predecessor.value
          : null,
      RevisionReviewRequired: taskValue.revisionReviewRequired,
      IsMilestone: taskValue.isMilestone,
      IsTaskRequiredonRejection: null,
      IsApprovalTask: isApprovalTask,
      AssignmentType: taskValue.allocation,
      TaskType: taskType,
      RPOStatus: {
        StatusID: {
          Id: taskValue.status,
        },
      },
      RPOPriority: {
        PriorityID: {
          Id: taskValue.priority,
        },
      },
      RPOProject: {
        ProjectID: {
          Id: projectId,
        },
      },
      RPOTeamRole: {
        TeamRole: {
          Id:
            taskValue.allocation === TaskConstants.ALLOCATION_TYPE_ROLE
              ? taskValue.role
                ? taskValue.role.name
                : ''
              : null,
        },
      },
      RPOOwnerUser: {
        User: {
          userID: isApprovalTask
            ? taskValue.deliverableApprover
              ? taskValue.deliverableApprover.value
              : ''
            : taskValue.owner
              ? taskValue.owner.value
              : '',
        },
      },
      Reviewers: {
        Reviewer: reviewers,
      },
    };

    return newTask;
  }

  createCustomTaskUpdateObject(taskValue: any) {
    const newTask = {
      Task: {
        Id: null,
        ItemId: null,
      },
      //Site: taskValue?.earlyManufacturingSite,
      //CountryofManufacture: taskValue?.draftManufacturingLocation,
      //TrialDateConfirmedWithBottler: taskValue?.trialDateConfirmed,
      FinalClassification: taskValue?.finalClassification,
      DraftClassification: taskValue?.draftClassification,
      CanStart: taskValue?.canStart,
      ScopingDocumentRequirement: taskValue?.scopingDocumentRequirement,
      RequiredComplete: taskValue?.requiredComplete,
      IssuanceType: taskValue?.issuanceType,
      Pallet: taskValue?.pallet,
      Tray: taskValue?.tray,
      IsThisRequired: taskValue?.isRequired,
      BOMSentActual: taskValue?.BOMSentActual,
      NPD_Project_Trial_Date_Confirmed_With_Bottler:
        taskValue?.trialDateConfirmed,
      TrialVolume24EQCases: taskValue?.trialVolume24EQCases,
      FirstProductionVolume24EQCases: taskValue?.firstProductionVolume24EQCases,
      ProcessAuthorityApprovalRequired: taskValue?.processAuthorityApproval,
      TransactionSchemeAnalysisRequired:
        taskValue?.transactionSchemeAnalysisRequired,
      NonProprietaryMaterialCostingRequired:
        taskValue?.nonProprietaryMaterialCostingRequired,
      ActualStartDate: taskValue.startDate,
      ActualDueDate: taskValue.endDate,
      PlannedDueDate: taskValue.OriginalEndDate,
      QCReleaseOrProdScheduleDiff: taskValue.noofWeeks,
      AllowProdScheduleLessThanEightWeeks: taskValue.productionSchedule,
      RMUserId: taskValue.rmUserId,
      RMUserCN: taskValue.rmUserCN,
      ProjectFields: {
        FinalCountryOfManufactureId: taskValue?.finalManufacturingLocation,
        FinalCountryOfManufacture: taskValue?.finalManufacturingLocationName,
        FinalManufacturingSiteId: taskValue?.finalManufacturingSite,
        FinalManufacturingSite: taskValue?.finalManufacturingSiteName,
        FinalProjectType: taskValue?.projectTypeName,
        FinalProjectTypeId: taskValue?.projectType,
        NewFG: taskValue?.newFg,
        NewRandD: taskValue?.newRnDFormula,
        NewHBC: taskValue?.newHBCFormula,
        NewPrimaryPack: taskValue?.newPrimaryPackaging,
        NewSecondaryPack: taskValue?.secondaryPackaging,
        LeadFormula: taskValue?.leadFormula,
        NewRegistrationDossier: taskValue?.registrationDossier,
        NewPreProduction: taskValue?.preProdReg,
        NewPostProduction: taskValue?.postProdReg,
        FinalProjectClassificationDescription:
          taskValue?.finalClassificationDesc,
        WinshuttleProjectNumber: taskValue?.winshuttleProject,
        FinalAgreed1stProdConfirmedWithBottler:
          taskValue?.finalAgreed1stProdConfirmedWithBottler,
        RegistrationClassificationAvailable: taskValue?.registrationClassificationAvailable,
        RegistrationRequirement: taskValue?.registrationRequirement
      },
      isNormalClassificationChange: false,
      RestrictTask18n33Deletion: false,
      DeleteTask18n33: false,
    };
    return newTask;
  }
  createCustomTaskUpdateObjectDuringTaskInAvailabilty(
    projectindexerDetails: any,
    requestIndexerDetails: any,
    flag4: boolean,
    flag5: boolean
  ) {
    if (!(flag4 == true && flag5 == true)) {
      if (flag4 == true && flag5 == false) {
        const newTask = {
          Task: {
            Id: null,
            ItemId: null,
          },
          ProjectFields: {
            FinalCountryOfManufactureId:
              projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyManufacturingLocationId
              ],
            FinalCountryOfManufacture:
              projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyManufacturingLocation
              ],
            FinalManufacturingSiteId:
              projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyManufacturingSiteId
              ],
            FinalManufacturingSite:
              projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyManufacturingSite
              ],
            NewRegistrationDossier:
              requestIndexerDetails?.RQ_REGISTRATION_DOSSIER,
            NewPreProduction:
              requestIndexerDetails?.RQ_PRE_PRODUCTION_REGISTRATION,
            NewPostProduction:
              requestIndexerDetails?.RQ_POST_PRODUCTION_REGISTRATION,
            FinalProjectClassificationDescription:
              requestIndexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyManufacturingDescription
              ],
          },
        };
        return newTask;
      }
      if (flag5 == true && flag4 == false) {
        const newTask = {
          Task: {
            Id: null,
            ItemId: null,
          },
          ProjectFields: {
            FinalProjectType:
              projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyProjectType
              ],
            FinalProjectTypeId:
              projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyProjectTypeId
              ],
            NewFG: requestIndexerDetails?.RQ_NEW_FG_SAP,
            NewRandD: requestIndexerDetails?.RQ_NEW_RD_FORMULA,
            NewHBC: requestIndexerDetails?.RQ_NEW_HBC_FORMULA,
            NewPrimaryPack: requestIndexerDetails?.RQ_NEW_PRIMARY_PACKAGING,
            NewSecondaryPack: requestIndexerDetails?.RQ_IS_SECONDARY_PACKAGING,
            FinalProjectClassificationDescription:
              requestIndexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyManufacturingDescription
              ],
          },
        };
        return newTask;
      }
    }
  }

  /*   createCustomRequestUpdateObject(taskValue: any) {
    const request = {
      Site: taskValue?.site,
      //FinalClassification: taskValue?.finalClassification,
      //DraftClassification: taskValue?.draftClassification,
      CountryofManufacture: taskValue?.countryofManufacture,
      newFg: taskValue?.newFg,
      newRnDFormula: taskValue?.newRnDFormula,
      newHBCFormula: taskValue?.newHBCFormula,
      newPrimaryPackaging: taskValue?.newPrimaryPackaging,
      secondaryPackaging: taskValue?.secondaryPackaging,
      registrationDossier: taskValue?.registrationDossier,
      preProdReg: taskValue?.preProdReg,
      postProdReg: taskValue?.postProdReg,
    }
    return request;
  }
 */

  compareTwoProjectDetails(updatedProject, oldProject) {
    if (oldProject) {
      const comparedObject = Object.assign(updatedProject);
      const excludedObject: Array<string> = [
        'ProjectId',
        'TaskId',
        'DeliverableId',
        'CustomMetadata',
      ];
      let projectFields;
      //const comparedObject = Object.assign(updatedProject['ProjectFields']);
      for (const keys in updatedProject) {
        if (
          excludedObject.indexOf(keys) < 0 &&
          this.isEqual(updatedProject[keys], oldProject[keys])
        ) {
          delete comparedObject[keys];
        } else if (
          keys === 'CustomMetadata' &&
          comparedObject[keys]['Metadata']['metadataFields'].length === 0
        ) {
          delete comparedObject[keys];
        } else if (keys === 'ProjectFields') {
          // && this.isEqual(updatedProject['ProjectFields'], oldProject['ProjectFields']
          projectFields = this.compareTwoProjectDetails(
            updatedProject['ProjectFields'],
            oldProject['ProjectFields']
          );
          /* for (const keys in updatedProject['ProjectFields']) {
             if (excludedObject.indexOf(keys) < 0 && this.isEqual(updatedProject['ProjectFields'][keys], oldProject['ProjectFields'][keys])) {
              delete comparedObject['ProjectFields'][keys];
            }
          } */
        }
      }
      return comparedObject;
    }
    return updatedProject;
  }

  /*   compareDates(updatedDates,oldDates) {
    if (oldDates) {
      const comparedObject = Object.assign(oldDates);
      const excludedObject: Array<string> = ['ProjectId', 'TaskId', 'DeliverableId', 'CustomMetadata'];
      for (const keys in updatedDates) {
        if (excludedObject.indexOf(keys) < 0 && this.isEqual(updatedDates[keys], oldDates[keys])) {
          delete comparedObject[keys];
        }
      }
      return comparedObject;
    }
    return updatedDates;
  } */

  isEqual(newObject: any, oldObject: any): boolean {
    if (newObject && oldObject) {
      if (typeof newObject === 'string') {
        return newObject === oldObject;
      } else {
        return JSON.stringify(newObject) === JSON.stringify(oldObject);
      }
    } else if (typeof newObject === 'boolean') {
      return newObject === oldObject;
    } else if ((newObject && !oldObject) || (!newObject && oldObject)) {
      return false;
    } else if (!newObject && !oldObject) {
      return true;
    }
    return false;
  }

  subTractUnitsToDate(
    dateObject: Date,
    units: number,
    diffType: moment_.unitOfTime.Diff
  ): Date {
    const start = moment(dateObject);
    dateObject = start.subtract(units, diffType).toDate();
    return dateObject;
  }

  sortTasksBySequence(taskList,classificationNumber): Observable<any> {
    // console.log(taskList);
    return new Observable((observer) => {
      this.npdResourceManagementService.fetchAllTaskRolesByClassification(classificationNumber).subscribe(
        (response:any) => {
          // console.log(response);
          let allTaskConfigs = response;
          let maxSequenceValue =
            (allTaskConfigs &&
              Math.max.apply(
                Math,
                allTaskConfigs.map((task) => {
                  return task.TaskSequence;
                })
              )) + 1;
          taskList.forEach((projectTask) => {
            let filteredTask = allTaskConfigs.find(
              (task) => task.TaskName == projectTask.TASK_NAME
            );
            if (filteredTask) {
              projectTask.sequence = filteredTask.TaskSequence;
            } else {
              projectTask.sequence = maxSequenceValue;
            }
          });
          taskList.sort((a, b) => {
            let taskNo1 = a?.sequence;
            let taskNo2 = b?.sequence;
            return taskNo1 - taskNo2;
          });
          observer.next(taskList);
          observer.complete();
        },
        (error) => {
          observer.error();
        }
      );
    });
  }

  formrequiredDataForLoadView(viewConfig, isListView) {
    const data = {
      SORTABLE_FIELDS: [],
      SEARCHABLE_FIELDS: [],
      DEFAULT_SORT_FIELD: null,
      DISPLAY_DEFAULT_FIELDS: [],
      DISPLAY_COLUMNS: [],
      ALL_SORTABLE_FIELDS: [],
      ALL_DISPLAY_DEFAULT_FIELDS: [],
      ALL_DISPLAY_COLUMNS: [],
    };
    if (viewConfig) {
      this.appConfig = this.sharingService.getAppConfig();
      const arrayList = isListView
        ? this.fieldConfigService.getDisplayOrderData(
          viewConfig.R_PM_LIST_VIEW_MPM_FIELDS,
          viewConfig.LIST_FIELD_ORDER
        )
        : viewConfig.R_PM_CARD_VIEW_MPM_FIELDS;
      data.DISPLAY_DEFAULT_FIELDS = arrayList ? arrayList : [];
      data.DISPLAY_DEFAULT_FIELDS.forEach((eachMetadata) => {
        const tempMetadata = {
          displayName: eachMetadata.DISPLAY_NAME,
          name: eachMetadata.INDEXER_FIELD_ID,
          fieldId: eachMetadata['MPM_Fields_Config-id'].Id,
          indexerId:
            this.fieldConfigService.formIndexerIdFromField(eachMetadata),
        };
        data.DISPLAY_COLUMNS.push(eachMetadata.INDEXER_FIELD_ID);
        if (eachMetadata.SORTABLE === 'true') {
          data.SORTABLE_FIELDS.push(tempMetadata);
        }
        if (eachMetadata.SEARCHABLE === 'true') {
          data.SEARCHABLE_FIELDS.push(tempMetadata);
        }
      });
      const cardList = viewConfig.R_PM_CARD_VIEW_MPM_FIELDS;
      data.ALL_DISPLAY_DEFAULT_FIELDS = cardList ? cardList : [];
      data.ALL_DISPLAY_DEFAULT_FIELDS.forEach((eachMetadata) => {
        const tempCardMetadata = {
          displayName: eachMetadata.DISPLAY_NAME,
          name: eachMetadata.INDEXER_FIELD_ID,
          fieldId: eachMetadata['MPM_Fields_Config-id'].Id,
          indexerId:
            this.fieldConfigService.formIndexerIdFromField(eachMetadata),
        };
        data.ALL_DISPLAY_COLUMNS.push(eachMetadata.INDEXER_FIELD_ID);
        if (eachMetadata.SORTABLE === 'true') {
          data.ALL_SORTABLE_FIELDS.push(tempCardMetadata);
        }
        if (eachMetadata.SEARCHABLE === 'true') {
          data.ALL_SORTABLE_FIELDS.push(tempCardMetadata);
        }
      });
      let allSortableFields =
        isListView && data.SORTABLE_FIELDS.concat(data.ALL_SORTABLE_FIELDS);

      if (
        viewConfig.R_PO_DEFAULT_SORT_FIELD &&
        viewConfig.R_PO_DEFAULT_SORT_FIELD['MPM_Fields_Config-id'].Id
      ) {
        const deafultSort = allSortableFields.find(
          (dataSort) =>
            dataSort.fieldId ===
            viewConfig.R_PO_DEFAULT_SORT_FIELD['MPM_Fields_Config-id'].Id
        );
        if (deafultSort) {
          data.DEFAULT_SORT_FIELD = {
            option: deafultSort,
            //sortType: MatSortDirections.ASC
            sortType:
              this.appConfig[
                ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER
              ] === 'ASC'
                ? 'asc'
                : 'desc',
          };
        }
      }
    }
    return data;
  }

  triggerNPDWorkflow(projectId) {
    let projectUpdateObj = {
      ProjectId: projectId,
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.HANDLE_WORKFLOW_RULES_INITIATION_METHOD_NS,
          this.HANDLE_WORKFLOW_RULES_INITIATION_METHOD_WS,
          projectUpdateObj
        )
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  setEditTaskHeaderData(editTaskHeaderData) {
    this.editTaskHeaderData = editTaskHeaderData;
  }

  getEditTaskHeaderData(): Observable<any> {
    return new Observable((observer) => {
      observer.next(this.editTaskHeaderData);
    });
    // this.editTaskHeaderData.next(editTaskHeader)
  }

  /**
   * @author Manikanta
   * @param parameters
   * @returns
   */
  taskBulkEdit(parameters): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.TASK_BULK_EDIT_OPERATIONS_NS,
          this.TASK_BULK_EDIT_OPERATIONS_WS,
          parameters
        )
        .subscribe(
          (res) => {
            observer.next(res);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  /**
   * @author Manikanta
   * @param params
   * @returns
   */
  getProjectTaskSequence(params): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.GET_NPD_TASK_SEQUENCE_NS,
          this.GET_NPD_TASK_SEQUENCE_WS,
          params
        )
        .subscribe((res) => {
          observer.next(res);
          observer.complete();
        });
    });
  }

  /**
   * @author Manikanta
   * @param params
   * @returns
   */
  updateTaskSequence(params): Observable<any> {
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.UPDATE_NPD_TASK_SEQUENCE_NS,
          this.UPDATE_NPD_TASK_SEQUENCE_WS,
          params
        )
        .subscribe(
          (res) => {
            observer.next(res);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getRoleByName(roleName: string) {
    if (roleName === 'Corp PM') {
      roleName = 'CORP_PM';
    } else if (roleName === 'Ops PM') {
      roleName = 'OPS_PM';
    } else if (roleName === 'Project Specialist') {
      roleName = 'PROJECT_SPECIALIST';
    } else if (roleName === 'Secondary AW Specialist') {
      roleName = 'SECONDARY_PACKAGING';
    } else if (roleName === 'RTM') {
      roleName = 'REGIONAL_TECHNICAL_MANAGER';
    }
    return this.sharingService
      .getAllRoles()
      .find((role) => role.ROLE_NAME === roleName);
  }

  getUsersForRole(roleName) {
    const parameters = {
      role: this.getRoleByName(roleName)?.ROLE_DN,
      depth: '1',
      sort: 'ascending',
    };
    return this.appService.getUsersForRole(parameters);
  }

  getPersonDetailsByUserCN(user) {
    let parameters = {
      userCN: user,
    };
    // this.appService.getPersonDetailsByUserCN(parameters).subscribe((res)=>{
    //   console.log(res);
    //   return res.Person.PersonToUser['Identity-id'].ItemId;
    // })
    return this.appService.getPersonDetailsByUserCN(parameters);
  }
  forceDeleteTask(Id: string, isCustomWorkflow: boolean): Observable<any> {
    const parameter = {
      id: Id,
      isCustomWorkflow: isCustomWorkflow,
    };
    return new Observable((observer) => {
      this.appService
        .invokeRequest(
          this.DELETE_WORKFLOW_NS,
          this.FORCE_DELETE_TASK,
          parameter
        )
        .subscribe(
          (response) => {
            observer.next(response);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  getDependentTasksForTargetTaskId(taskId: number, classificationNumber: string) {
    let parameter = {
      targetTaskId: taskId,
      classificationNumber: classificationNumber
    };
    return this.appService.invokeRequest(
      this.GET_WORKFLOW_RULES_BY_TARGET_TASK_ID_NS,
      this.GET_WORKFLOW_RULES_BY_TARGET_TASK_ID_WS,
      parameter
    );
  }

  getProjectTaskViewDisplayableFields(){
    if(this.projectTaskViewFields==null){
      this.projectTaskViewFields=[];
      let projectTaskView=
        JSON.parse(sessionStorage.getItem('ALL_MPM_VIEW_CONFIG')).find(view=>view.VIEW==='PM_TASK_VIEW')?.R_PM_LIST_VIEW_MPM_FIELDS;
      projectTaskView.forEach(field=>{
        this.projectTaskViewFields.push(
          {
            field_id : field.INDEXER_FIELD_ID
          });
      })
    }
    return this.projectTaskViewFields;
  }
}
