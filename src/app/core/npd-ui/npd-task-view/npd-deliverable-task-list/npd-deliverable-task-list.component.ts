import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Renderer2,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  ApplicationConfigConstants,
  AssetService,
  ConfirmationModalComponent,
  DeliverableBulkCountService,
  FieldConfigService,
  FormatToLocalePipe,
  IndexerDataTypes,
  LoaderService,
  MPMField,
  MPMFieldConstants,
  MPMFieldKeys,
  MPM_LEVELS,
  NotificationService,
  OTMMService,
  ProjectService,
  SharingService,
  StatusTypes,
  UtilService,
} from 'mpm-library';
import { MPMDeliverableTaskListComponent } from 'src/app/core/mpm-lib/deliverable-task/deliverable-task-list/deliverable-task-list.component';
import { TaskCreationModalComponent } from 'src/app/core/mpm/project-view/task-creation-modal/task-creation-modal.component';
import { RouteService } from 'src/app/core/mpm/route.service';
import { CommentsModalComponent } from '../../collab/comments-modal/comments-modal.component';
import { CommentConfig } from '../../collab/objects/CommentConfig';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { OverViewConstants } from '../../npd-project-view/constants/projectOverviewConstants';
import { NpdTaskService } from '../services/npd-task.service';
import { BulkEditTasksComponent } from '../bulk-edit-tasks/bulk-edit-tasks.component';
// import { BulkTaskEditComponent } from 'src/app/core/mpm/project-view/task/bulk-edit-task/bulk-task-edit/bulk-task-edit.component';
import { SelectionModel } from '@angular/cdk/collections';
import { TaskConfigConstant } from '../constants/TaskConfig';
import { WeekPickerService } from '../../services/week-picker.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-npd-deliverable-task-list',
  templateUrl: './npd-deliverable-task-list.component.html',
  styleUrls: ['./npd-deliverable-task-list.component.scss'],
})
export class NpdDeliverableTaskListComponent extends MPMDeliverableTaskListComponent {
  selectedProject: any;
  taskConfig: any;
  taskCR = {
    taskId: '',
    taskItemId: '',
  };
  requestItemId;
  public loggedInUserId: string;
  public isRevokeEnabled: boolean;
  taskEditHeaderData;
  overViewConstants = OverViewConstants;
  @Input() selectedGroupByFilter: any;

  selectedTasksIDList = [];
  isAllSameTasks: any[];
  taskOwnerRole: any[];
  allIds: { TaskId: any; TaskItemId: any; ProjectId: any; RequestId: any };

  TaskBulkOperations_IM = {
    TaskBulkOperations: {
      Identity: [],
      ActualStartDate: '',
      ActualCompletionDate: '',
      Duration: '',
      TaskOwner: '',
      TaskOperation: '',
      TaskRMRole: '',
    },
  };
  taskChecked: boolean;
  // myTasksFilter: any;
  @Input() myTasksFilter;

  @Output() refreshPage: EventEmitter<any> = new EventEmitter<any>();
  selectAllChecked: any;
  reload: any;

  selectAllTaskChecked: boolean;
  @Output() selectedTaskCallBackHandler = new EventEmitter<any>();
  selectedTasks = new SelectionModel(true, []);
  backgroundCondition: string;
  constructor(
    private mpmRouteService: RouteService,
    utilService: UtilService,
    projectService: ProjectService,
    formatToLocalePipe: FormatToLocalePipe,
    element: ElementRef,
    fieldConfigService: FieldConfigService,
    sharingService: SharingService,
    deliverableBulkCountService: DeliverableBulkCountService,
    dialog: MatDialog,
    assetService: AssetService,
    otmmService: OTMMService,
    router: Router,
    renderer: Renderer2,
    private monsterConfigApiService: MonsterConfigApiService,
    private notificationService: NotificationService,
    private loaderService: LoaderService,
    public monsterUtilService: MonsterUtilService,
    private npdTaskService: NpdTaskService,
    private weekPickerService: WeekPickerService
  ) {
    super(
      utilService,
      projectService,
      formatToLocalePipe,
      element,
      fieldConfigService,
      sharingService,
      deliverableBulkCountService,
      dialog,
      assetService,
      otmmService,
      router,
      renderer
    );
  }

  // isAllSelected() {
  //   const numSelected = this.selectedTasks.selected.length;
  //   const numRows = this.deliverableTaskDataSource.data.length;
  //   return numSelected === numRows;
  // }

  // masterSelect() {
  //   this.selectAllTaskChecked ? this.checkAllTasks() : this.uncheckAllTasks();
  // }

  // checkAllTasks() {
  //   this.deliverableTaskDataSource.data.forEach(row => {
  //     row.selected = true;
  //     this.selectedTasks.select(row);
  //   });
  //   this.selectedTaskCallBackHandler.emit(this.selectedTasks.selected);

  // }

  // uncheckAllTasks() {
  //   this.deliverableTaskDataSource.data.forEach(row => {
  //     row.selected = false;
  //     this.selectedTasks.deselect(row);
  //   });
  //   this.selectedTaskCallBackHandler.emit(this.selectedTasks.selected);

  // }

  // checkTask(taskData, event) {
  //   if (event.checked) {
  //     taskData.selected = true;
  //     this.selectedTasks.select(taskData);
  //     this.selectedTaskCallBackHandler.emit(this.selectedTasks.selected);
  //   }
  //   else {
  //     this.selectedTasks.deselect(taskData);
  //     this.selectedTaskCallBackHandler.emit(this.selectedTasks.selected);
  //   }
  // }

  getFinalStatusCheck(selectedTask): boolean {
    return (
      this.getPropertyByMapper(
        selectedTask,
        this.MPMFieldConstants.MPM_TASK_FIELDS.TASK_STATUS_TYPE
      ) === StatusTypes.FINAL_APPROVED ||
      this.getPropertyByMapper(
        selectedTask,
        this.MPMFieldConstants.MPM_TASK_FIELDS.TASK_STATUS_TYPE
      ) === StatusTypes.FINAL_COMPLETED ||
      this.getPropertyByMapper(
        selectedTask,
        this.MPMFieldConstants.MPM_TASK_FIELDS.TASK_STATUS_TYPE
      ) === StatusTypes.FINAL_REJECTED
    );
  }

  isOnHoldProject(selectedTask): boolean {
    let isOnHoldProject = selectedTask.IS_ON_HOLD === 'true' ? true : false;
    return isOnHoldProject;
  }

  getPropertyByMapper(taskData, mapperName): string {
    const displayColumn: MPMField = this.fieldConfigService.getFieldByKeyValue(
      MPMFieldKeys.MAPPER_NAME,
      mapperName,
      MPM_LEVELS.TASK
    );
    return this.getProperty(taskData, displayColumn);
  }

  getProjectPropertyByMapper(taskData, mapperName): string {
    const displayColum: MPMField = this.fieldConfigService.getFieldByKeyValue(
      MPMFieldKeys.MAPPER_NAME,
      mapperName,
      MPM_LEVELS.PROJECT
    );
    return this.getProperty(displayColum, taskData);
  }

  openCommentDialog(selectedTask) {
    // console.log('selectedTask',selectedTask)
    const taskId: string = this.getPropertyByMapper(
      selectedTask,
      this.MPMFieldConstants.MPM_TASK_FIELDS.TASK_ID
    );
    const projectId = this.getPropertyByMapper(
      selectedTask,
      this.MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID
    )?.split('.')[1]; //selectedTask['PROJECT_ITEM_ID']?.split('.')[1];
    const taskItemId: string = this.getPropertyByMapper(
      selectedTask,
      this.MPMFieldConstants.MPM_TASK_FIELDS.TASK_ITEM_ID
    ).split('.')[1];
    const requestId = selectedTask.PR_REQUEST_ITEM_ID.split('.')[1];
    this.requestItemId = requestId;

    const dialogRef = this.dialog.open(CommentsModalComponent, {
      width: '75%',
      height: '90vh',
      maxHeight: '90vh',
      minHeight: '90vh',
      panelClass: 'comment-dialog-modal',
      disableClose: true,
      data: {
        requestId: requestId,
        commentType: CommentConfig.TASK_COMMENT_TYPE_VALUE,
        projectId: projectId,
        taskId: taskItemId,
        enableToComment: true, // this.enableToComment
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('comments module dialog is closed \n', result);
    });
  }

  openEditDialog(eventData) {
    this.npdTaskService.setEditTaskHeaderData(eventData);
    this.getProjectDetails(eventData.PROJECT_ITEM_ID.split('.')[1], eventData);
  }
  getProjectDetails(projectId, eventData) {
    this.projectService.getProjectById(projectId).subscribe((projectObj) => {
      if (projectObj) {
        this.selectedProject = projectObj;
        const dialogRef = this.dialog.open(TaskCreationModalComponent, {
          width: '38%',
          disableClose: true,
          data: {
            projectId: eventData.PROJECT_ITEM_ID.split('.')[1],
            isTemplate: false,
            taskId: eventData.ID,
            projectObj: this.selectedProject,
            selectedtask: eventData,
            isTaskActive: eventData.IS_ACTIVE_TASK,
            isApprovalTask: eventData.IS_APPROVAL_TASK,
            modalLabel: 'Edit Task',
          },
        });
      }
    });
  }

  ngOnInit() {
    super.ngOnInit();

    this.loggedInUserId = this.sharingService.getCurrentUserItemID();
    this.isRevokeEnabled = this.utilService.getBooleanValue(
      this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_REVOKE]
    );
    if (this.level == 'TASK' && this.displayColumns.indexOf('Actions') < 0) {
      this.displayColumns[this.displayColumns.length] = 'Actions';
    }
    this.loaderService.hide();
  }

  checkCanClaim(selectedTask) {
    let role_to_match;
    let matched_role;
    role_to_match = selectedTask.TASK_ROLE_ID;
    let role = this.utilService.getTeamRoleDNByRoleId(role_to_match);
    if (role) {
      matched_role = this.utilService.getUserRoles().filter((role1) => {
        return role1.Id == role.ROLE_DN;
      });
      // console.log(matched_role);
    }
    return matched_role ? true : false;
  }

  showClaim(row) {
    let showClaim =
      this.getPropertyByMapper(
        row,
        this.MPMFieldConstants.MPM_TASK_FIELDS.IS_TASK_ACTIVE
      ) == '0';
    return showClaim;
  }

  // enableClaim(row) {
  //   let isTaskClaim = (!this.checkCanClaim(row) ||
  //                     !(this.getPropertyByMapper(row, this.MPMFieldConstants.MPM_TASK_FIELDS.IS_TASK_ACTIVE) === 'true' ? true : false) ||
  //                     this.isOnHoldProject(row)) && this.getPropertyByMapper(row,this.MPMFieldConstants.MPM_TASK_FIELDS.TASK_ACTIVE_USER_ID) == '0';
  //   return isTaskClaim;
  // }
  enableClaim(row) {
    let isTaskClaim = true;

    if (
      !this.isOnHoldProject(row) &&
      this.getPropertyByMapper(
        row,
        this.MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_NAME
      ) === 'NA'
    ) {
      isTaskClaim = false;
    }
    return isTaskClaim;
  }

  showRevoke(row) {
    let showRevoke =
      this.isRevokeEnabled &&
      this.getPropertyByMapper(
        row,
        this.MPMFieldConstants.MPM_TASK_FIELDS.TASK_ACTIVE_USER_NAME
      ) !== 'NA';
    return showRevoke;
  }

  enableRevoke(row) {
    let enableRevoke =
      !(this.getPropertyByMapper(
        row,
        MPMFieldConstants.MPM_TASK_FIELDS.IS_TASK_ACTIVE
      ) === 'true'
        ? true
        : false) ||
      this.loggedInUserId !==
        this.getPropertyByMapper(
          row,
          MPMFieldConstants.MPM_TASK_FIELDS.TASK_ACTIVE_USER_ID
        ) ||
      this.isOnHoldProject(row);
    return enableRevoke;
  }

  claim(eventData) {
    // console.log('eventData', eventData)
    const taskOperationParams = {
      taskAction: {
        userCN: this.sharingService.getCurrentUserCN(),
        task: {
          Id: eventData.ID,
          ItemId: eventData.ITEM_ID,
        },
        actionId: 'CLAIM',
        requestId: eventData?.PR_REQUEST_ITEM_ID?.split('.')[1],
        MakeTaskOwnerAsRMUser:
          eventData.TASK_ROLE_VALUE != 'GFG' ? 'true' : 'false',
        isTaskOwnerChange:
          eventData.TASK_ROLE_VALUE === 'GFG' ? 'true' : 'false',
        //eventData?.PR_REQUEST_ITEM_ID?.split('.')[1]////720909
      },
    };
    this.loaderService.show();
    this.monsterConfigApiService
      .fetchTaskMenuClaimRevoke(taskOperationParams)
      .subscribe((response) => {
        if (response != null) {
          setTimeout(() => {
            this.loaderService.hide();
            this.notificationService.success('Task claimed successfully');
          }, 3000);
        } else {
          this.loaderService.hide();
          this.notificationService.error(
            'Error while claiming the task. Please try again.'
          );
        }
      });
  }

  revoke(eventData) {
    const taskOperationParams = {
      taskAction: {
        //userCN: this.sharingService.getCurrentUserCN(),
        task: {
          Id: eventData.ID,
          ItemId: eventData.ITEM_ID,
        },
        actionId: 'REVOKE',
      },
    };
    this.loaderService.show();
    this.monsterConfigApiService
      .fetchTaskMenuClaimRevoke(taskOperationParams)
      .subscribe((response) => {
        if (response != null) {
          setTimeout(() => {
            this.loaderService.hide();
            this.notificationService.success('User is revoked from the Task');
          }, 3000);
        } else {
          this.loaderService.hide();
          this.notificationService.error(
            'Error while revoking the Task. Please try again.'
          );
        }
      });
  }
  checkDateType(displayColumn) {
    return (
      displayColumn?.DATA_TYPE?.toLowerCase() ===
      IndexerDataTypes.DATETIME?.toLowerCase()
    );
  }

  getWeek(column, row) {
    let date = this.monsterUtilService.getFieldValueByDisplayColumn(
      column,
      row
    );
    if (
      date &&
      column?.DATA_TYPE.toLowerCase() ===
        IndexerDataTypes.DATETIME.toLowerCase()
    ) {
      return this.monsterUtilService.calculateWeek(date);
    }
  }

  onRowClick(project) {
    if (project.PROJECT_ITEM_ID) {
      this.mpmRouteService.goToProjectDetailViewNewTab(
        project.PROJECT_ITEM_ID.split('.')[1],
        false,
        null
      );
    } else {
      this.mpmRouteService.goToProjectDetailViewNewTab(
        project.ITEM_ID.split('.')[1],
        false,
        null
      );
    }
  }
  // bulk task operations
  openBulkEditDialog(operationName) {
    this.selectedTasksIDList = [];
    this.taskOwnerRole = [];
    this.isAllSameTasks = [];
    let sameTasksSet;
    let taskOwnerRole;
    let selectedRows = [];
    selectedRows = this.selection['_selected'];

    if (selectedRows == null || selectedRows.length == 0) {
      // to check if tasks are  selected/not
      this.notificationService.error('Please select tasks for bulk operation');
    } else {
      let number = selectedRows[0].TASK_NAME.split(' ')[1];
      let taskDetails = TaskConfigConstant.allTaskConfig.find(
        (task) => task.taskNumber == number
      );

      let allTaskRoles = [];
      let selectedTaskNames = [];
      selectedRows.forEach((task) => {
        allTaskRoles.push(task?.TASK_ROLE_VALUE);
        selectedTaskNames.push(task?.TASK_NAME);
      });
      if (!selectedTaskNames.every((name) => name == selectedTaskNames[0])) {
        this.notificationService.error('Please select tasks of same type');
        return;
      }
      let taskRole = selectedRows[0].TASK_ROLE_VALUE;
      if (!allTaskRoles.every((role) => role == taskRole)) {
        this.notificationService.error('Each task is having different role');
        return;
      }
      // let taskRole = taskDetails.taskOwner[0];
      selectedRows.forEach((task) => {
        if (!(task?.NPD_TASK_IS_BULK_UPDATE_INPROGRESS == 'true')) {
          //to check if bulk operation is already in process
          this.allIds = {
            TaskId: task.ID,
            TaskItemId: task.ITEM_ID,
            ProjectId: task.PROJECT_ITEM_ID.split('.')[1],
            RequestId: task.PR_REQUEST_ITEM_ID.split('.')[1],
          };
          this.selectedTasksIDList.push(this.allIds);
        }
        this.isAllSameTasks.push(task.TASK_NAME);
        this.taskOwnerRole.push(task.TASK_ROLE_VALUE);
      });
      this.TaskBulkOperations_IM.TaskBulkOperations.Identity =
        this.selectedTasksIDList;
      sameTasksSet = new Set(this.isAllSameTasks);
      taskOwnerRole = new Set(this.taskOwnerRole);
      if (sameTasksSet.size > 1) {
        // to check if tasks are of same type
        this.notificationService.error('Please select tasks of same type');
      } else if (this.selectedTasksIDList.length == 0) {
        this.notificationService.warn(
          'Task Bulk Operation in Progress, Kindly wait..'
        );
        this.allIds = null;
      } else {
        this.TaskBulkOperations_IM.TaskBulkOperations.TaskOperation =
          operationName;
        this.TaskBulkOperations_IM.TaskBulkOperations.TaskOperation =
          operationName;
        this.TaskBulkOperations_IM.TaskBulkOperations.TaskRMRole =
          selectedRows[0]?.NPD_TASK_RM_ROLE;
        if (operationName == 'EDIT') {
          const dialogRef = this.dialog.open(BulkEditTasksComponent, {
            maxHeight: '150vh',
            minHeight: '30vh',
            disableClose: true,
            data: {
              tasksSelected: this.TaskBulkOperations_IM,
              taskRole: taskRole,
              reload: this.reload,
            },
          });
          dialogRef.afterClosed().subscribe((result) => {
            // console.log('The dialog was closed');
            this.reload = result;
            if (this.reload == 'reload') {
              this.refreshPage.emit();
            }
          });
        } else if (operationName == 'COMPLETE') {
          this.confirmBulkEdit((result) => {
            if (result) {
              const day = new Date();
              const completionWeek = this.monsterUtilService.calculateWeek(
                new Date(day)
              );
              let completionDate =
                this.monsterUtilService.getStartEndDate(completionWeek);
              const isoCompletionDate =
                this.npdTaskService.getIsoFormatDateString(completionDate[1]);
              this.TaskBulkOperations_IM.TaskBulkOperations.ActualCompletionDate =
                isoCompletionDate;
              this.npdTaskService
                .taskBulkEdit(this.TaskBulkOperations_IM)
                .subscribe(
                  () => {
                    this.notificationService.success(
                      'Task Bulk Operation has been triggered'
                    );
                  },
                  () => {
                    this.notificationService.error(
                      'Something went wrong during Task Bulk Operation.'
                    );
                  }
                );
              this.refreshPage.emit();
            } else {
              this.refreshPage.emit();
            }
          });
        }
      }
    }
  }

  confirmBulkEdit(callback) {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '40%',
      disableClose: true,
      data: {
        message: 'Do you want to take this action on all tasks selected?',
        submitButton: 'Yes',
        cancelButton: 'No',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (callback) {
        callback(result ? result.isTrue : null);
      }
    });
  }

  routeToProjectDashboardView() {
    let selectedTaskData = this.selection['_selected'];
    let projectBusinessID = [];
    if (selectedTaskData == null || selectedTaskData.length == 0) {
      this.notificationService.error(
        'Please select tasks to open in new dashboard'
      );
    } else {
      const duplicateBusinessID = new Set();
      for (const task of selectedTaskData) {
        duplicateBusinessID.add(task.NPD_PROJECT_BUSINESS_ID);
      }
      projectBusinessID = Array.from(duplicateBusinessID);
      const facetData = [
        {
          fieldId: 'NPD_PROJECT_BUSINESS_ID',
          type: 'SimpleFieldRestriction',
          values: JSON.stringify(projectBusinessID),
        },
      ];
      this.mpmRouteService.goToProjectDashBoardInNewTab(
        'My Projects',
        true,
        null,
        null,
        '1',
        '25',
        null,
        null,
        null,
        JSON.stringify(facetData)
      );
    }
  }

  expandTaskRow(taskData) {
    this.expandRow(taskData);
    this.taskEditHeaderData = taskData;
    // this.npdTaskService.setEditTaskHeaderData(this.taskEditHeaderData)
  }

  changeStatusColor(task, status) {
    let style;
    let isOverdue = this.isOverdue(task);
    if (status === 'Cancelled') {
      style = 'cancelled-status';
      return style;
    } else if (isOverdue && status !== 'Approved/Completed') {
      style = 'delete-status';
      return style;
    } else {
      return this.monsterUtilService.changeTaskStatusColor(status);
    }
  }

  isOverdue(taskData) {
    let currentdate = new Date();
    let estimatedEndDate = new Date(
      taskData.NPD_TASK_ESTIMATED_COMPLETION_DATE
    );
    currentdate.setHours(23, 59, 59, 998);
    return currentdate > estimatedEndDate;
  }

  getCurrentTimelineStatus(rowData, ColumnData) {
    // console.log('row', rowData);
    // console.log('clolumn', ColumnData);
    // if()
    return true;
  }

  getCurrentTimelineCondition(rowData, ColumnData) {
    this.backgroundCondition = '';
    const governDeliveryWeek = rowData.NPD_PROJECT_GOVERNED_DELIVERY_DATE;
    const currentTimelineWeek = rowData.NPD_PROJECT_COM_TECH_OPS_AGREED_DP_WEEK;
    if (
      currentTimelineWeek &&
      currentTimelineWeek !== 'NA' &&
      governDeliveryWeek &&
      governDeliveryWeek !== 'NA'
    ) {
      const getDateFromWeek = (weekString) => {
        const [week, year] = weekString.split('/');
        const startDate = new Date(year, 0, 1);
        startDate.setDate(startDate.getDate() + (week - 1) * 7);
        return new Date(startDate.getTime() + 6 * 24 * 60 * 60 * 1000);
      };

      // Convert the difference in milliseconds to weeks
      const endDateGovernDeliveryWeek = getDateFromWeek(governDeliveryWeek);
      const endDateCurrentTimelineWeek = getDateFromWeek(currentTimelineWeek);

      //Time zone
      const endMilliGovernDeliveryWeek =
        endDateGovernDeliveryWeek.getTime() -
        endDateGovernDeliveryWeek.getTimezoneOffset() * 60000;
      const endMilliCurrentTimelineWeek =
        endDateCurrentTimelineWeek.getTime() -
        endDateCurrentTimelineWeek.getTimezoneOffset() * 60000;

      const diffMillis =
        endMilliGovernDeliveryWeek - endMilliCurrentTimelineWeek;

      let diffWeeks = Math.floor(diffMillis / (7 * 24 * 60 * 60 * 1000));

      if (diffWeeks < 0) {
        this.backgroundCondition = 'red';
      } else if (diffWeeks > 6) {
        this.backgroundCondition = 'green';
      } else if (0 <= diffWeeks && diffWeeks <= 6) {
        this.backgroundCondition = 'amber';
      }
    }
    return this.backgroundCondition;
  }
  tableData(): MatTableDataSource<any> {
    let result = new MatTableDataSource(this.deliverableTaskDataSource.data);
    result.sortingDataAccessor = (item, header) => {
      if (this.monsterUtilService.isDateStringMatches(header)) {
        const weekFormat = item[header];
        if (
          weekFormat &&
          weekFormat != undefined &&
          weekFormat != '' &&
          weekFormat != 'NA'
        ) {
          const [week, year] = weekFormat.split('/').map(Number);
          return new Date(year, 0, (week - 1) * 7); // Convert to a date for proper sorting
        } else {
          const [week, year] = [0, 0];
          return new Date(year, 0, (week - 1) * 7); // Convert to a date for proper sorting
        }
      } else {
        return item[header];
      }
    };
    result.sort = this.sortControl;
    return result;
  }
}
