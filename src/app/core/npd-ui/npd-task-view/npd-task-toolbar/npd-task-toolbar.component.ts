import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SharingService, CommentsUtilService, UtilService, AssetService, LoaderService, NotificationService } from 'mpm-library';
import { MPMTaskToolbarComponent } from 'src/app/core/mpm-lib/project/tasks/task-toolbar/task-toolbar.component';
import { FilterConfigService } from '../../filter-configs/filter-config.service';
import { MissingTasksComponent } from '../missing-tasks/missing-tasks.component';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { NpdProjectService } from '../../npd-project-view/services/npd-project.service';
import { TaskConfigConstant } from '../constants/TaskConfig';

@Component({
  selector: 'app-npd-task-toolbar',
  templateUrl: './npd-task-toolbar.component.html',
  styleUrls: ['./npd-task-toolbar.component.scss']
})
export class NpdTaskToolbarComponent extends MPMTaskToolbarComponent {

  definedTaskAccess = false;
  constructor(public sharingService: SharingService,
    public commentUtilService: CommentsUtilService,
    public utilService: UtilService,
    public assetService: AssetService, private dialog: MatDialog, private filterConfigService: FilterConfigService,
    private npdProjectService: NpdProjectService, private loaderService: LoaderService, private notificationService: NotificationService) {
    super(sharingService, commentUtilService, utilService, assetService,);
  }
  createMissingTasks() {
    /*   if(this.validateAssignment()) {
        return;
      } */
    this.loaderService.show();
    const taskList = this.taskConfig.taskList;
    const classificationTask = taskList.find(task => !TaskConfigConstant.nonClassficationTaskNames.includes(task?.TASK_NAME));
    const classificationNumber = this.npdProjectService.getProjectClassificationNumber(classificationTask);
    if (classificationNumber) {
      this.npdProjectService.getProjectTasksConfigsByClassificationNumber(classificationNumber).subscribe((taskConfigs: any) => {
        const notCreatedTask = taskConfigs.filter((config: any) => !taskList.some(taskList => taskList?.TASK_NAME === config.taskName && taskList?.TASK_DESCRIPTION === config.taskDescription));
        const sortedNotCreatedTask = notCreatedTask.sort((a, b) => a.taskName.localeCompare(b.taskName));

        if (sortedNotCreatedTask.length) {
          const dialogRef = this.dialog.open(MissingTasksComponent, {
            width: '35%',
            disableClose: true,
            data: {
              projectId: this.taskConfig.projectId,
              modalLabel: 'Add Defined Task(s)',
              taskList: sortedNotCreatedTask,
              classificationNumber: classificationNumber
            }
          });
          this.loaderService.hide();
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              this.refresh();
            }
          });
        }
        else {
          this.loaderService.hide();
          this.notificationService.warn("There is no defined task to add here.");
        }
      }, (error) => {
        this.loaderService.hide();
      });
    }
    else {
      this.loaderService.hide();
      this.notificationService.warn("There is no defined task to add here.");
    }
    // const dialogRef = this.dialog.open(MissingTasksComponent, {
    //   width: '35%',
    //   disableClose: true,
    //   data: {
    //     projectId: this.taskConfig.projectId,
    //     modalLabel: 'Add Defined Task(s)',
    //     // taskList: this.taskConfig.taskList
    //   }
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.refresh();
    //   }
    // });
  }

  ngOnInit() {
    super.ngOnInit();
    this.definedTaskAccess = this.filterConfigService.isCorpPMRole() || this.filterConfigService.isE2EPMRole() || this.filterConfigService.isOPSPMRole() || this.filterConfigService.isProgramManagerRole();
  }
}
