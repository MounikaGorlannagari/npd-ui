import {
  Component,
  ChangeDetectorRef,
  Input,
  EventEmitter,
  Output,
  ViewChild,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
  ValidatorFn,
} from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import {
  ProjectConstant,
  MPM_LEVELS,
  TaskConstants,
  AppService,
  CommentsUtilService,
  EntityAppDefService,
  LoaderService,
  NotificationService,
  ProjectUtilService,
  SharingService,
  TaskService,
  UtilService,
  MPMFieldConstants,
  StatusTypes,
  SearchRequest,
  SearchResponse,
  IndexerService,
  ConfirmationModalComponent,
  FieldConfigService,
  MPMField,
  MPMFieldKeys,
} from 'mpm-library';
import { TaskUpdateObj } from 'mpm-library/lib/project/tasks/task-creation/task.update';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { startWith, map, distinctUntilChanged } from 'rxjs/operators';
import { MPMTaskCreationComponent } from 'src/app/core/mpm-lib/project/tasks/task-creation/task-creation.component';
import { FilterConfigService } from '../../filter-configs/filter-config.service';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { TaskConfigConstant } from '../constants/TaskConfig';
import { NpdTaskService } from '../services/npd-task.service';
import * as moment from 'moment';
import { BusinessConstants } from '../../request-view/constants/BusinessConstants';
import { RequestService } from '../../services/request.service';
import { EntityListConstant } from '../../request-view/constants/EntityListConstant';
import { EntityService } from '../../services/entity.service';
import { I } from '@angular/cdk/keycodes';
import { OverViewConstants } from '../../npd-project-view/constants/projectOverviewConstants';
import { NpdProjectService } from '../../npd-project-view/services/npd-project.service';
import $ from 'jquery';
import { DatePipe } from '@angular/common';
import { MONSTER_ROLES } from '../../filter-configs/role.config';
import { NewCommentModal, UserInfoModal } from '../../collab/objects/comment.modal';
import { CommentsService } from '../../collab/services/comments.service';
import { CommentConfig } from '../../collab/objects/CommentConfig';
import { CommentsModalComponent } from '../../collab/comments-modal/comments-modal.component';
import { SingleCommentsTextComponent } from '../../collab/single-comments-text/single-comments-text.component';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-npd-task-edit',
  templateUrl: './npd-task-edit.component.html',
  styleUrls: ['./npd-task-edit.component.scss'],
})
export class NpdTaskEditComponent extends MPMTaskCreationComponent {
  public bussinessUnit = '';
  public finalDraftManufacturingLocation = '';
  public bussinessUnits = [];
  public allMarkets = [];
  public leadMarket = '';
  public userListData: Array<UserInfoModal>;
  @Input() selectedTask;
  @ViewChild('singleCommentcomponent')
  singleCommentcomponent: SingleCommentsTextComponent;

  show = {
    plannedStartDate: false,
    plannedEndDate: false,
    actualStartDate: false,
    actualEndDate: false,
    BOMSentActual: false,
    trialDateConfirmed: false,
    finalAgreed1stProdConfirmedWithBottler: false,
    firstProductionVolume24EQCases: false,
    EstimatedCompletionDate: false,
  };
  showFields = {
    canStart: false,
    draftClassification: false,
    finalClassification: false,
    site: false,
    scopingDocumentRequirement: false,
    winshuttleProject: false,
    requiredComplete: false,
    issuanceType: false,
    pallet: false,
    tray: false,
    isRequired: false,
    trialDateConfirmed: false,
    BOMSentActual: false,
    countryofManufacture: false,
    processAuthorityApproval: false,
    transactionSchemeAnalysisRequired: false,
    nonProprietaryMaterialCostingRequired: false,
    newFg: false,
    newRnDFormula: false,
    newHBCFormula: false,
    newPrimaryPackaging: false,
    secondaryPackaging: false,
    leadFormula:false,
    productionSchedule: false,
    noofWeeks: false,
    finalAgreed1stProdConfirmedWithBottler: false,
    firstProductionVolume24EQCases: false,
    projectType: false,
    earlyProjectType: false,
    earlyManufacturingLocation: false,
    earlyManufacturingSite: false,
    rmRole: false,
    taskOwnerRMUser: false,
    trialVolume24EQCases: false,
  };
  disableFields = {
    canStart: true,
    draftClassification: true,
    finalClassification: true,
    site: true,
    scopingDocumentRequirement: true,
    winshuttleProject: true,
    requiredComplete: true,
    issuanceType: true,
    pallet: true,
    tray: true,
    isRequired: true,
    trialDateConfirmed: true,
    trialVolume24EQCases: true,
    BOMSentActual: true,
    countryofManufacture: true,
    processAuthorityApproval: true,
    transactionSchemeAnalysisRequired: true,
    nonProprietaryMaterialCostingRequired: true,
    newFg: true,
    newRnDFormula: true,
    newHBCFormula: true,
    newPrimaryPackaging: true,
    secondaryPackaging: true,
    productionSchedule: true,
    noofWeeks: true,
    finalAgreed1stProdConfirmedWithBottler: true,
    firstProductionVolume24EQCases: true,
    projectType: true,
    earlyProjectType: true,
    earlyManufacturingLocation: true,
    earlyManufacturingSite: true,
    rmUser: true,
    taskOwnerRMUser: true,
    leadFormula: true
  };
  validationRequiredFields = {
    scopingDocumentRequirement: false,
    processAuthorityApproval: false,
    transactionSchemeAnalysisRequired: false,
    nonProprietaryMaterialCostingRequired: false,
    requiredComplete: false,
    productionSchedule: false,
  };
  //requiredComp = false;
  fromDate = {
    plannedStartDate: null,
    plannedEndDate: null,
    actualStartDate: null,
    actualEndDate: null,
    BOMSentActual: null,
    trialDateConfirmed: null,
    finalAgreed1stProdConfirmedWithBottler: null,
    EstimatedCompletionDate: null,
  };
  toDate = {
    plannedStartDate: null,
    plannedEndDate: null,
    actualStartDate: null,
    actualEndDate: null,
    BOMSentActual: null,
    trialDateConfirmed: null,
    finalAgreed1stProdConfirmedWithBottler: null,
    EstimatedCompletionDate: null,
  };
  weekYear = {
    plannedStartDate: null,
    plannedEndDate: null,
    actualStartDate: null,
    actualEndDate: null,
    BOMSentActual: null,
    trialDateConfirmed: null,
    finalAgreed1stProdConfirmedWithBottler: null,
    EstimatedCompletionDate: null,
  };
  disable = {
    plannedStartDate: true,
    plannedEndDate: true,
    actualStartDate: true,
    actualEndDate: true,
    duration: true,
    role: true,
    status: true,
    taskOwner: true,
    description: true,
    taskAction: true,
    EstimatedCompletionDate: true,
  };
  /*  { actualStartDate: false,
    actualEndDate: false,
    duration: false,
    role: false,
    status: false,
    taskOwner: false,
    description: false,
    taskAction: false }*/
  requiredComplete = [
    { value: 'Pre-Costing (default)' },
    { value: 'Pre-Trial' },
  ];
  oldTaskCustomObject;
  newTaskCustomObject;
  draftManufacturingLocations = [];
  earlyManufacturingSites = [];
  projectTypes = [];
  rmRoleUsers = [];
  taskCustomConfig;
  weekPickerMaxDate;
  readonly radioButtonsList = BusinessConstants.radioButtons;
  requestId;
  taskIndexerDetails;
  projectindexerDetails;
  requestIndexerDetails;
  requestDetails;
  request;
  regulatoryTimelineForLocations = {
    market: null,
    manufacturingLocation: null,
  };
  projectClassificationParams: any = {
    projectType: null,
    projectClassificationScore: null,
  };
  taskNumber;
  filterUserListData: Observable<any[]>;
  selectedRMUser;
  taskObjFinalClassification;
  totalScore: number = 0;
  isTask4Available: boolean = false;
  isTask5Available: boolean = false;
  selectedTaskIndexer: any;
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = [
    'projectBusinessId',
    'taskName',
    'taskDescription',
  ];
  panelOpenState = false;
  tableFooterColumns: string[] = ['noData'];
  @Output() editTaskHeaderData = new EventEmitter<any>();
  @Input() taskData;
  private subscription: Subscription;
  validation_msgs = {
    rmUserAutocompleteControl: [
      { type: 'invalidAutocompleteObject', message: 'No user matches found.' },
      //{ type: 'required', message: 'Contact is required.' }
    ],
  };
  @Input('detectChanges') observable$: Observable<any>;
  classificationNumber: string = '';
  allDraftManufacturingLocations: any[];

  constructor(
    public appService: AppService,
    public sharingService: SharingService,
    public adapter: DateAdapter<any>,
    public utilService: UtilService,
    public taskService: TaskService,
    public entityAppdefService: EntityAppDefService,
    public loaderService: LoaderService,
    public dialog: MatDialog,
    public projectUtilService: ProjectUtilService,
    public commentsUtilService: CommentsUtilService,
    public notificationService: NotificationService,
    public npdTaskService: NpdTaskService,
    public monsterConfigApiService: MonsterConfigApiService,
    public monsterUtilService: MonsterUtilService,
    public filterConfigService: FilterConfigService,
    public requestService: RequestService,
    public indexerService: IndexerService,
    public entityService: EntityService,
    public npdProjectService: NpdProjectService,
    public commentsService: CommentsService,
    private cd: ChangeDetectorRef,
    public fieldConfigService: FieldConfigService
  ) {
    super(
      appService,
      sharingService,
      adapter,
      utilService,
      taskService,
      entityAppdefService,
      loaderService,
      dialog,
      projectUtilService,
      commentsUtilService,
      notificationService
    );
  }

  getBool(date) {
    this.show[date] = !this.show[date];
  }

  openWeekPicker(date) {
    this.getAllBool();
    this.show[date] = !this.show[date];
  }

  changeTargetDP(weekFormat, date) {
    this.weekYear[date] = weekFormat;
    if (date == 'startDate') {
      this.taskForm.patchValue({ startDate: weekFormat });
      //this.taskForm.patchValue({endDate: `${+weekFormat.split("/")[0] + +this.taskForm?.controls?.duration?.value}/${weekFormat.split("/")[1]}`});
      /*   else if(date == "endDate"){
        this.taskForm.patchValue({endDate: weekFormat});
        }  */
    } else {
      this.taskForm.patchValue({ [date]: weekFormat });
      this.taskForm.controls[date].updateValueAndValidity();
    }
    this.taskForm.markAsDirty();
    [this.fromDate[date], this.toDate[date]] = this.getWeekFormat(weekFormat);
  }

  getWeekFormat(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates: NgbDate[] = this.calculateDateNgb(week, year);
      return [weekdates[0], weekdates[1]];
    }
  }

  getStartEndDate(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];
      let weekdates = this.calculateDate(week, year);
      weekdates[0].setHours(0);
      weekdates[0].setMinutes(0);
      weekdates[1].setHours(0);
      weekdates[1].setMinutes(0);
      return [weekdates[0], weekdates[1]];
    }
  }

  // getStartEndDateForUpdateTask(weekFormat) {
  //   if (weekFormat && weekFormat !== undefined) {
  //     let week = weekFormat.split('/')[0];
  //     let year = weekFormat.split('/')[1];
  //     let weekdates = this.calculateDate(week, year);
  //     weekdates[0].setHours(12)
  //     weekdates[0].setMinutes(0);
  //     weekdates[1].setHours(12);
  //     weekdates[1].setMinutes(0);
  //     return [weekdates[0], weekdates[1]];
  //   }
  // }

  calculateDateNgb(week: number, year: number): NgbDate[] {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate, ngbToDate];
  }
  calculateDate(week: number, year: number) {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    return [fromDate, toDate];
  }
  getAllBool() {
    Object.keys(this.show).map((ele) => {
      this.show[ele] = false;
    });
  }

  isDateValid(date) {
    if (this.taskForm.get(date).touched) {
      if (
        this.taskForm.get(date).value == undefined ||
        this.taskForm.get(date).value.trim() === '' ||
        this.taskForm.get(date).value == null
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  checkDatePattern(date) {
    let isValid = true;

    if (
      this.taskForm.get(date).touched &&
      this.taskForm.get(date).value &&
      this.taskForm.get(date).value.trim().length > 0
    ) {
      let regexp = new RegExp('^([1-9]|[1-4][0-9]|5[1-2])/[0-9]{4}$');
      isValid = regexp.test(this.taskForm.get(date).value);
    }
    return !isValid;
  }

  getStopPropogation(event, date) {
    if (this.show[date]) {
      return event.stopPropagation();
    }
  }

  getCustomTaskDetailsAndRMRoles(isApprovalTask: boolean): Observable<any> {
    return new Observable((observer) => {
      const requestParameters = {
        ProjectID: this.taskConfig.projectId,
        IsApprovalTask: isApprovalTask,
        AllowActionTask: this.allowActionTask,
      };
      // this.taskService
      //   .getDependentTask(requestParameters)
      //   .subscribe((dependentTaskResponse) => {
          // if (dependentTaskResponse) {
          //   if (!Array.isArray(dependentTaskResponse)) {
          //     dependentTaskResponse = [dependentTaskResponse];
          //   }
          //   this.taskModalConfig.taskList = dependentTaskResponse;
          //   if (this.taskModalConfig.selectedTask) {
          //     // removing the selected task from task list
          //     this.taskModalConfig.taskList =
          //       this.taskModalConfig.taskList.filter((task) => {
          //         return task !== this.taskModalConfig.selectedTask;
          //       });
          //   }
          //   const predecessorFilterList = [];
          //   this.taskModalConfig.taskList.map((task) => {
          //     if (
          //       this.taskModalConfig.taskId &&
          //       task['Task-id'].Id === this.taskModalConfig.taskId
          //     ) {
          //       this.taskModalConfig.selectedTask = task;
          //     }
          //     const value = this.formPredecessorValue(task);
          //     if (task.NAME && task['Task-id'] && task['Task-id'].Id) {
          //       predecessorFilterList.push({
          //         name: task.NAME,
          //         value: task['Task-id'].Id,
          //         displayName: value,
          //       });
          //     }
          //   });
          //   this.taskModalConfig.deliveryPackageConfig.filterOptions =
          //     predecessorFilterList;
          //   this.taskModalConfig.predecessorConfig.filterOptions =
          //     predecessorFilterList;
          // } else {
          //   if (this.taskForm) {
          //     this.taskForm.controls.predecessor.patchValue('');
          //     this.taskModalConfig.taskList = [];
          //     if (this.taskConfig.isApprovalTask) {
          //       this.taskModalConfig.deliveryPackageConfig.filterOptions = [];
          //     } else {
          //       this.taskModalConfig.predecessorConfig.filterOptions = [];
          //     }
          //   } else {
          //     this.taskModalConfig.taskList = [];
          //     this.taskModalConfig.deliveryPackageConfig.filterOptions = [];
          //     this.taskModalConfig.predecessorConfig.filterOptions = [];
          //   }
          // }
          let parameters = {
            MPMTaskId: this.taskModalConfig.taskId,
          };
          // console.log('taskModalConfig.selectedTask',this.taskModalConfig.selectedTask)
          this.npdTaskService.fetchCustomTaskDetails(parameters).subscribe(
            (taskResponse) => {
              this.taskCustomConfig = taskResponse?.['NPD_Task'];
              if (
                this.npdTaskService.checkValueIsNullOrEmpty(
                  this.taskCustomConfig?.RMRoleName,
                  null
                ) &&
                this.taskCustomConfig?.R_PO_RM_ROLE?.['Identity-id']?.Id
              ) {
                // let roleDn = this.utilService.GetRoleDNByRoleId(this.taskCustomConfig.R_PO_RM_ROLE['Identity-id'].Id);
                let roleDn = undefined;
                if (roleDn == undefined) {
                  let roleName = this.taskCustomConfig?.RMRoleName;
                  // let roleName = this.taskModalConfig.selectedTask.NPD_TASK_RM_ROLE;
                  if (roleName === 'Corp PM') {
                    roleName = 'CORP_PM';
                  } else if (roleName === 'Ops PM') {
                    roleName = 'OPS_PM';
                  } else if (roleName === 'Project Specialist') {
                    roleName = 'PROJECT_SPECIALIST';
                  } else if (roleName === 'Secondary AW Specialist') {
                    roleName = 'SECONDARY_PACKAGING';
                  } else if (roleName === 'RTM') {
                    roleName = 'REGIONAL_TECHNICAL_MANAGER';
                  }
                  roleDn = this.sharingService.getRoleByName(roleName);
                }
                this.npdProjectService
                  .getAllUsersByRole(roleDn?.ROLE_DN)
                  .subscribe(
                    (response) => {
                      this.rmRoleUsers = response;
                      this.loaderService.hide();
                      observer.next(true);
                      observer.complete();
                    },
                    (error) => {
                      observer.error();
                    }
                  );
              } else {
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
              }
            },
            (error) => {
              observer.error();
            }
          );
          //*********** */
          //this.loaderService.hide();
          //observer.next(true);
          //observer.complete();
          /************** */
        // });
    });
  }

  onRMUserChange(userValue) {
    if (
      (userValue != undefined &&
        typeof userValue === 'string' &&
        userValue?.trim() === '') ||
      !this.rmRoleUsers.some((e) => e.displayName === userValue?.displayName)
    ) {
      this.taskForm.patchValue({
        rmUserId: '',
        rmUserCN: '',
      });
    }
  }

  onFocusOut(event) {
    if (
      event.target.value.trim() === '' &&
      (!this.taskForm.get('rmUser') || !this.taskForm.get('rmUser')?.errors)
    ) {
      //this.taskForm.get('rmUser').setErrors(null);
      return;
    } else {
      if (this.rmRoleUsers) {
        if (
          !this.rmRoleUsers.some((e) => e.displayName === event.target.value)
        ) {
          this.taskForm.get('rmUser').setErrors({ incorrect: true });
        } else {
          this.taskForm.get('rmUser').setErrors(null);
        }
      }
    }
  }

  formRMuserValue(task) {
    //combining user-name,.. & identity -id
    if (task?.RMUserCN) {
      const userIdentity = {
        Identity: {
          Id: task?.R_PO_RM_USER?.['Identity-id']?.Id,
        },
      };
      const selectedRole = this.rmRoleUsers.find(
        (element) => element.name === task.RMUserCN
      );
      if (selectedRole) {
        selectedRole.userIdentity = userIdentity;
      }
      return selectedRole ? selectedRole : { userIdentity: userIdentity };
    }
    return {};
  }

  displayFn(user?: any): string | undefined {
    return user ? user.displayName || user.name : '';
  }

  filterUserList() {
    this.filterUserListData = this.taskForm.controls.rmUser.valueChanges.pipe(
      startWith(''),
      // map(value => this._filter(value))
      map((value) => this.filterUser(value))
    );
  }

  public filterUser(value: string): string[] {
    let filterValue = value;
    try {
      filterValue = value.toLowerCase();
    } catch (exception) {
      filterValue = value;
    }
    if (this.rmRoleUsers) {
      return this.rmRoleUsers.filter((option) => {
        return (
          option.name && option.name.toLowerCase().indexOf(filterValue) >= 0
        );
      });
    } else {
      return [];
    }
  }

  /* private _filter(value: string): string[] {
    const filterValue = value?.toLowerCase();
    return this.rmRoleUsers.filter(option => option.name?.toLowerCase().includes(filterValue));
  } */

  onRMUserSelection(selectedUser) {
    this.taskForm.get('rmUser')?.setErrors(null);
    if (selectedUser?.option?.value) {
      this.loaderService.show();
      this.requestService
        .getUserByID(selectedUser.option.value?.name)
        .subscribe(
          (response) => {
            //this.selectedRMUser.userIdentity = response.result.items[0];
            let user = response.result.items[0];
            this.selectedRMUser.userIdentity = response.result.items[0];
            //this.selectede2EPM.userIdentity.Identity.Id
            this.taskForm.patchValue({
              rmUserId: user?.Identity?.Id,
              rmUserCN: selectedUser.option.value?.name,
            });
            this.loaderService.hide();
          },
          (error) => {
            this.loaderService.hide();
          }
        );
    }
    /*   if(this.selectede2EPM) {
          this.requestService.getUserByID(this.selectede2EPM.name).subscribe(response => {
            this.selectede2EPM.userIdentity = response.result.items[0];
            this.updateRequestRelationsInfo(formControl,formName);
          }, error => {
          });
        } */
  }

  autocompleteObjectValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (typeof control.value === 'string') {
        return { invalidAutocompleteObject: { value: control.value } };
      }
      return null; /* valid option selected */
    };
  }

  initialiseTaskForm() {
    // console.log(this.taskModalConfig);
    const disableField = false;
    this.taskForm = null;
    this.taskModalConfig.minDate =
      this.taskModalConfig.selectedProject.START_DATE;
    this.taskModalConfig.maxDate =
      this.taskModalConfig.selectedProject.DUE_DATE;
    const pattern = ProjectConstant.NAME_STRING_PATTERN;
    if (!this.taskModalConfig.isTemplate) {
      if (this.taskModalConfig.isNewTask) {
        this.taskStatus = this.taskModalConfig.taskInitialStatusList;
        this.taskForm = new FormGroup({
          taskName: new FormControl({ value: '', disabled: disableField }, [
            Validators.required,
            Validators.maxLength(120),
            Validators.pattern(this.nameStringPattern),
          ]),
          duration: new FormControl(
            { value: '', disabled: disableField || !this.enableDuration },
            [Validators.required, Validators.max(999), Validators.min(1)]
          ),
          durationType: new FormControl({
            value: this.DurationType[0].value,
            disabled: disableField || !this.enableDuration,
          }),
          status: new FormControl({
            value:
              this.taskModalConfig.taskInitialStatusList &&
                this.taskModalConfig.taskInitialStatusList[0] &&
                this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id']
                ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id']
                  .Id
                : '',
            disabled: true,
          }),
          owner: new FormControl({ value: '', disabled: disableField }),
          role: new FormControl({ value: '', disabled: disableField }),
          priority: new FormControl(
            {
              value:
                this.taskModalConfig.taskPriorityList &&
                  this.taskModalConfig.taskPriorityList[0] &&
                  this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id']
                  ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id']
                    .Id
                  : '',
              disabled: disableField,
            },
            [Validators.required]
          ),
          startDate: new FormControl(
            {
              value: this.taskModalConfig.selectedProject.START_DATE,
              disabled: disableField,
            },
            [Validators.required]
          ),
          endDate: new FormControl(
            {
              value: this.taskModalConfig.selectedProject.DUE_DATE,
              disabled: disableField || this.enableDuration,
            },
            [Validators.required]
          ),
          description: new FormControl({ value: '', disabled: disableField }, [
            Validators.maxLength(1000),
          ]),
          revisionReviewRequired: new FormControl({
            value: this.taskConfig.isApprovalTask
              ? this.defaultRevisionRequiredStatus
              : '',
            disabled: disableField,
          }),
          isMilestone: new FormControl({
            value: false,
            disabled: disableField,
          }),
          viewOtherComments: new FormControl({
            value: '',
            disabled: disableField,
          }),
          predecessor: new FormControl({ value: '', disabled: disableField }),
          deliverableApprover: new FormControl({
            value: '',
            disabled: disableField,
          }),
          deliverableReviewers: new FormControl({
            value: '',
            disabled: disableField,
          }),
          allocation: new FormControl({ value: '', disabled: disableField }),
          comments: new FormControl({ value: '' }),
        });
        this.taskForm.updateValueAndValidity();
      } else if (this.taskModalConfig.selectedTask) {
        this.taskStatus = this.projectUtilService.filterStatusOptions(
          this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id,
          this.taskModalConfig.taskStatusList,
          MPM_LEVELS.TASK
        );
        this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(
          this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id,
          this.taskModalConfig.taskStatusList
        );
        // if (selectedStatusInfo && (selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.FINAL)) {
        // this.overViewConfig.isReadOnly = true;
        // disableFields =  true;
        // this.disableStatusOptions = true;
        // }
        const task = this.taskModalConfig.selectedTask;
        const taskField = this.getTaskFields(task);
        const taskRoleId =
          task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id']
            ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id
            : '';
        // const taskOwnerId = task.R_PO_OWNER_ID && task.R_PO_OWNER_ID['Identity-id'] ? task.R_PO_OWNER_ID['Identity-id'].Id : '';
        task.predecessor =
          task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object'
            ? task.PARENT_TASK_ID
            : null;
        //const startDateWeekYearValue = ''+(this.calculateWeek(task.START_DATE)-1)+'/'+ new Date(task.START_DATE).getFullYear();
        //const endDateWeekYearValue = ''+(this.calculateWeek(task.DUE_DATE)-1)+'/'+ new Date(task.DUE_DATE).getFullYear();

        const OriginalStartDateWeekYearValue =
          this.monsterUtilService.calculateTaskPlannedWeek(task.START_DATE);
        const OriginalEndDateWeekYearValue =
          this.monsterUtilService.calculateTaskPlannedWeek(task.DUE_DATE);
          let EstimatedCompletionDateWeekYearValue;
          if(this.selectedTask && this.selectedTask.selectedtask && this.selectedTask.selectedtask.NPD_TASK_ESTIMATED_COMPLETION_DATE){
            EstimatedCompletionDateWeekYearValue=
            this.monsterUtilService.calculateTaskPlannedWeek(
              this.selectedTask?.selectedtask?.NPD_TASK_ESTIMATED_COMPLETION_DATE
            );
          }else{
            EstimatedCompletionDateWeekYearValue=
            this.monsterUtilService.calculateTaskPlannedWeek(
              this.taskModalConfig.selectedTask.NPD_TASK_ESTIMATED_COMPLETION_DATE
            );
          }

        let startDateWeekYearValue = '';
        let endDateWeekYearValue = '';
        let BOMSentActualValue = '';

        /*  if(this.npdTaskService.checkValueIsNullOrEmpty(task.ACTUAL_START_DATE, '')!= '') {
                startDateWeekYearValue = ''+(this.calculateWeek(task.ACTUAL_START_DATE)-1)+'/'+ new Date(task.ACTUAL_START_DATE).getFullYear();
            }
            if(this.npdTaskService.checkValueIsNullOrEmpty(task.ACTUAL_DUE_DATE, '')!= '') {
                endDateWeekYearValue = ''+(this.calculateWeek(task.ACTUAL_DUE_DATE)-1)+'/'+ new Date(task.ACTUAL_DUE_DATE).getFullYear();
            } */
        if (
          this.npdTaskService.checkValueIsNullOrEmpty(
            this.taskCustomConfig?.ActualStartDate,
            ''
          ) != ''
        ) {
          startDateWeekYearValue =
            this.monsterUtilService.calculateTaskPlannedWeek(
              this.taskCustomConfig?.ActualStartDate
            );
          //''+(this.calculateWeek(this.taskCustomConfig?.ActualStartDate)-1)+'/'+ new Date(this.taskCustomConfig?.ActualStartDate).getFullYear();
        }
        if (
          this.npdTaskService.checkValueIsNullOrEmpty(
            this.taskCustomConfig?.ActualDueDate,
            ''
          ) != ''
        ) {
          endDateWeekYearValue =
            this.monsterUtilService.calculateTaskPlannedWeek(
              this.taskCustomConfig?.ActualDueDate
            );
          //''+(this.calculateWeek(this.taskCustomConfig?.ActualDueDate)-1)+'/'+ new Date(this.taskCustomConfig?.ActualDueDate).getFullYear();
        }
        // if (
        //   this.npdTaskService.checkValueIsNullOrEmpty(
        //     this.taskCustomConfig?.BOMSentActual,
        //     ''
        //   ) != ''
        // ) {
        //   BOMSentActualValue = this.monsterUtilService.calculateTaskWeek(
        //     this.taskCustomConfig?.BOMSentActual
        //   );
        //   // ''+(this.calculateWeek(this.taskCustomConfig?.BOMSentActual)-1)+'/'+ new Date(this.taskCustomConfig?.BOMSentActual).getFullYear();
        // }
        if (
          this.npdTaskService.checkValueIsNullOrEmpty(
            this.projectindexerDetails?.NPD_PROJECT_BOM_SENT_ACTUAL_DATE,
            ''
          ) != ''
        ) {
          BOMSentActualValue =
            this.projectindexerDetails?.NPD_PROJECT_BOM_SENT_ACTUAL_DATE;
          //this.monsterUtilService.calculateTaskWeek(this.projectindexerDetails?.NPD_PROJECT_BOM_SENT_ACTUAL_DATE);
        }
        let finalTaskStatus = this.taskModalConfig.taskStatusList.find(
          (status) => status.STATUS_TYPE == StatusTypes.FINAL_APPROVED
        );
        let isFinalTask;
        if (
          finalTaskStatus &&
          finalTaskStatus['MPM_Status-id']?.Id ==
          this.taskModalConfig.selectedTask?.R_PO_STATUS['MPM_Status-id']?.Id
        ) {
          isFinalTask = true;
        }
        let string = task.NAME;
        string = string.replace(' ', '');
        string = string.replace('Task', ''); //string = string.replace("Task","");
        string = string.replace('TASK', '');
        this.taskNumber = string;
        let taskOwner=this.selectedTask?.selectedtask?.TASK_ROLE_VALUE
        this.getTaskNo(string, isFinalTask,taskOwner);

        if (
          this.selectedTaskIndexer?.NPD_TASK_IS_CP1_COMPLETED === 'true' ||
          this.selectedTaskIndexer?.NPD_TASK_IS_CP1_COMPLETED == true
        ) {
          this.disable.taskAction = true;
        }
        if (
          this.projectindexerDetails?.PROJECT_STATUS_VALUE == 'OnHold' ||
          this.projectindexerDetails?.PROJECT_STATUS_VALUE == 'On Hold'
        ) {
          this.disable.actualEndDate = true;
          this.disable.taskAction = true;
        }
        this.taskForm = new FormGroup({
          taskName: new FormControl(
            { value: task.NAME, disabled: disableField },
            [
              Validators.required,
              Validators.maxLength(120),
              Validators.pattern(this.nameStringPattern),
            ]
          ),
          duration: new FormControl(
            {
              value:
                typeof task.TASK_DURATION === 'string'
                  ? task.TASK_DURATION
                  : '',
              disabled: this.disable.duration,
            },
            [Validators.required, Validators.max(999), Validators.min(1)]
          ),
          durationType: new FormControl({
            value:
              typeof task.TASK_DURATION_TYPE === 'string'
                ? task.TASK_DURATION_TYPE
                : this.DurationType[0].value,
            disabled: this.disable.duration,
          }),
          status: new FormControl({
            value:
              task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id']
                ? task.R_PO_STATUS['MPM_Status-id'].Id
                : '',
            disabled: task.IS_ACTIVE == 'true' ? this.disable.status : true,
          }),
          owner: new FormControl({
            value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
            disabled: this.disable.taskOwner,
          }),
          role: new FormControl({
            value: this.formRoleValue(taskRoleId),
            disabled: this.disable.role,
          }),
          priority: new FormControl(
            {
              value:
                task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id']
                  ? task.R_PO_PRIORITY['MPM_Priority-id'].Id
                  : '',
              disabled: disableField,
            },
            [Validators.required]
          ),
          // startDate: new FormControl({
          //     value: this.commentsUtilService.addUnitsToDate(new Date(task.START_DATE), 0, 'days', this.enableWorkWeek), disabled: (disableField ||
          //         (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL))
          // }, [Validators.required]),
          //startDate,endDate-Planned; OriginalStartDate,OriginalEndDate-Actual
          startDate: new FormControl({
            value: startDateWeekYearValue,
            disabled: this.disable.actualStartDate,
          }),
          endDate: new FormControl({
            value: endDateWeekYearValue,
            disabled: this.disable.actualEndDate,
          }),
          OriginalStartDate: new FormControl(
            {
              value: OriginalStartDateWeekYearValue,
              disabled: this.disable.plannedStartDate,
            },
            [Validators.required]
          ),
          OriginalEndDate: new FormControl(
            {
              value: OriginalEndDateWeekYearValue,
              disabled: this.disable.plannedEndDate,
            },
            [Validators.required]
          ),
          EstimatedCompletionDate: new FormControl({
            value: EstimatedCompletionDateWeekYearValue,
            disabled: this.disable.EstimatedCompletionDate,
          }),
          description: new FormControl(
            {
              value: this.utilService.isNullOrEmpty(task.DESCRIPTION)
                ? ''
                : task.DESCRIPTION,
              disabled: this.disable.description,
            },
            [Validators.maxLength(1000)]
          ),
          revisionReviewRequired: new FormControl({
            value: this.utilService.getBooleanValue(
              task.REVISION_REVIEW_REQUIRED
            ),
            disabled: true,
          }),
          isMilestone: new FormControl({
            value: this.utilService.getBooleanValue(task.IS_MILESTONE),
            disabled: disableField,
          }),
          viewOtherComments: new FormControl({
            value: taskField.showOtherComments,
            disabled: disableField,
          }),
          predecessor: new FormControl(this.formPredecessor(task.predecessor)),
          // disabled: taskField.isApprovalTaskSelected || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL
          deliverableApprover: new FormControl({
            value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
            disabled:
              disableField ||
              (this.selectedStatusInfo &&
                this.selectedStatusInfo.STATUS_TYPE !==
                this.taskConstants.STATUS_TYPE_INITIAL),
          }),
          deliverableReviewers: new FormControl({ value: this.savedReviewers }),
          allocation: new FormControl({
            value: task.ASSIGNMENT_TYPE,
            disabled:
              disableField ||
              (this.selectedStatusInfo &&
                this.selectedStatusInfo.STATUS_TYPE !==
                this.taskConstants.STATUS_TYPE_INITIAL),
          }),
          comments: new FormControl(''),
          canStart: new FormControl({
            //this.taskCustomConfig?.CanStart
            value: this.npdTaskService.checkBooleanValueIsNullOrEmpty(
              task.IS_ACTIVE,
              false
            ),
            disabled: this.disableFields.canStart,
          }),
          earlyProjectType: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyProjectType
              ],
              ''
            ),
            disabled: this.disableFields.earlyProjectType,
          }),
          earlyManufacturingLocation: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyManufacturingLocation
              ],
              ''
            ),
            disabled: this.disableFields.earlyManufacturingLocation,
          }),
          earlyManufacturingSite: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_EARLY_CLASSIFICATION
                .earlyManufacturingSite
              ],
              ''
            ),
            disabled: this.disableFields.earlyManufacturingSite,
          }),
          draftClassification: new FormControl({
            value: this.projectindexerDetails?.PR_EARLY_PROJECT_CLASSIFICATION, //this.request?.draftClassification,
            disabled: this.disableFields.draftClassification,
          }),
          finalClassification: new FormControl(
            {
              value: this.npdTaskService.checkValueIsNullOrEmpty(
                this.projectindexerDetails?.[
                OverViewConstants.OVERVIEW_MAPPINGS
                  .PROJECT_FINAL_CLASSIFICATION.finalProjectClassification
                ],
                ''
              ),
              disabled: this.disableFields.finalClassification,
            },
            [Validators.required]
          ),
          finalClassificationDesc: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
                .finalProjectClassificationDesc
              ],
              ''
            ),
            disabled: this.disableFields.finalClassification,
          }),
          finalManufacturingSite: new FormControl({
            value:
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
                .finalManufacturingSiteId
              ],
            //this.request?.earlyManufacturingSite,
            disabled: this.disableFields.site,
          }),
          finalManufacturingSiteName: new FormControl({
            value:
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
                .finalManufacturingSite
              ],
            disabled: this.disableFields.site,
          }),
          finalManufacturingLocation: new FormControl({
            value:
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
                .finalManufacturingLocationId
              ],
            //value: this.request?.draftManufacturingLocation,
            disabled: this.disableFields.countryofManufacture,
          }),
          finalManufacturingLocationName: new FormControl({
            value:
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
                .finalManufacturingLocation
              ],
            disabled: this.disableFields.countryofManufacture,
          }),
          projectType: new FormControl({
            value:
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
                .projectType
              ],
            disabled: this.disableFields.projectType,
          }),
          projectTypeName: new FormControl({
            value:
              this.projectindexerDetails?.[
              OverViewConstants.OVERVIEW_MAPPINGS.PROJECT_FINAL_CLASSIFICATION
                .projectTypeName
              ],
            disabled: this.disableFields.projectType,
          }),
          scopingDocumentRequirement: new FormControl(
            {
              value: this.npdTaskService.checkValueIsNullOrEmpty(
                this.taskCustomConfig?.ScopingDocumentRequirement,
                ''
              ),
              disabled: this.disableFields.scopingDocumentRequirement,
            },
            this.getValidation('scopingDocumentRequirement')
          ),
          winshuttleProject: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_WINSHUTTLE_NUMBER,
              ''
            ),
            disabled: this.disableFields.winshuttleProject,
          }),
          requiredComplete: new FormControl(
            {
              value: this.taskCustomConfig?.RequiredComplete,
              disabled: this.disableFields.requiredComplete,
            },
            this.getValidation('requiredComplete')
          ),
          issuanceType: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.taskCustomConfig?.IssuanceType,
              ''
            ),
            disabled: this.disableFields.issuanceType,
          }),
          pallet: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_PALLET_LABEL_FORMAT,
              ''
            ),
            disabled: this.disableFields.pallet,
          }),
          tray: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_TRAY_LABEL_FORMAT,
              ''
            ),
            disabled: this.disableFields.tray,
          }),
          isRequired: new FormControl({
            value: this.npdTaskService.checkBooleanValueIsNullOrEmpty(
              this.taskCustomConfig?.IsThisRequired,
              false
            ),
            disabled: this.disableFields.isRequired,
          }),
          BOMSentActual: new FormControl({
            value: BOMSentActualValue,
            //this.npdTaskService.checkValueIsNullOrEmpty(this.taskCustomConfig?.BOMSentActualweek, ''),
            disabled: this.disableFields.BOMSentActual,
          }),
          trialDateConfirmed: new FormControl({
            value:
              this.projectindexerDetails
                ?.NPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER,
            disabled: this.disableFields.trialDateConfirmed,
          }),
          trialVolume24EQCases: new FormControl({
            value:
              this.projectindexerDetails?.NPD_PROJECT_TRIAL_VOLUME_24_EQ_CASES,
            disabled: false,
          }),
          // firstProductionVolume24EQCases: new FormControl({
          //   value:
          //     "424,424,4",
          //   disabled: false,
          // }),
          transactionSchemeAnalysisRequired: new FormControl(
            {
              value: this.npdTaskService.checkBooleanValueIsNullOrEmpty(
                this.taskCustomConfig?.TransactionSchemeAnalysisRequired,
                null
              ),
              disabled: this.disableFields.transactionSchemeAnalysisRequired,
            },
            this.getValidation('transactionSchemeAnalysisRequired')
          ),
          processAuthorityApproval: new FormControl(
            {
              value: this.npdTaskService.checkBooleanValueIsNullOrEmpty(
                this.taskCustomConfig
                  ?.ProcessAuthorityApprovalEarlyRDTrialRequired,
                null
              ),
              disabled: this.disableFields.processAuthorityApproval,
            },
            this.getValidation('processAuthorityApproval')
          ),
          nonProprietaryMaterialCostingRequired: new FormControl(
            {
              value: this.npdTaskService.checkBooleanValueIsNullOrEmpty(
                this.taskCustomConfig?.NonproprietaryMaterialCostingRequired,
                null
              ),
              disabled:
                this.disableFields.nonProprietaryMaterialCostingRequired,
            },
            this.getValidation('nonProprietaryMaterialCostingRequired')
          ),

          newFg: new FormControl({
            value: this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_NEW_FG,
              null
            ), //this.request?.newFg,
            disabled: this.disableFields.newFg,
          }),
          newRnDFormula: new FormControl({
            value: this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_NEW_RD_FORMULA,
              null
            ), //this.request?.newFg,
            disabled: this.disableFields.newRnDFormula,
          }),
          newHBCFormula: new FormControl({
            value: this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_NEW_HBC_FORMULA,
              null
            ), //this.request?.newFg,
            disabled: this.disableFields.newHBCFormula,
          }),
          newPrimaryPackaging: new FormControl({
            value: this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_NEW_PRIMARY_PACK,
              null
            ), //this.request?.newFg,
            disabled: this.disableFields.newPrimaryPackaging,
          }),
          secondaryPackaging: new FormControl({
            value: this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_NEW_SECONDARY_PACK,
              null
            ), //this.request?.newFg,
            disabled: this.disableFields.secondaryPackaging,
          }),
          registrationClassificationAvailable: new FormControl(
            this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_REGISTRATION_CLASSIFICATION,
              null
            ) //this.request?.newFg,
          ),
          registrationRequirement: new FormControl(
            this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_REGISTRATION_REQUIREMENT,
              null
            )
          ),
          leadFormula: new FormControl({
            value: this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_LEAD_FORMULA,
              null
            ), //this.request?.newFg,
            disabled: this.disableFields.leadFormula,
          }),

          // registrationDossier: new FormControl(
          //   this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
          //     this.projectindexerDetails?.NPD_PROJECT_NEW_REGISTRATION_DOSSIER,
          //     null
          //   ) //this.request?.newFg,
          // ),
          // preProdReg: new FormControl(
          //   this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
          //     this.projectindexerDetails?.NPD_PROJECT_NEW_PRE_PRODUCTION,
          //     null
          //   ) //this.request?.newFg,
          // ),
          // postProdReg: new FormControl(
          //   this.npdTaskService.checkIndexerBooleanValueIsNullOrEmpty(
          //     this.projectindexerDetails?.NPD_PROJECT_NEW_POST_PRODUCTION,
          //     null
          //   ) //this.request?.newFg,
          // ),
          productionSchedule: new FormControl({
            //this.taskCustomConfig?.CanStart
            value: this.npdTaskService.checkBooleanValueIsNullOrEmpty(
              this.projectindexerDetails
                ?.NPD_PROJECT_ALLOW_PROD_SCHEDULE_LESS_THAN_8,
              false
            ),
            disabled: this.disableFields.productionSchedule,
          }),
          noofWeeks: new FormControl({
            //this.taskCustomConfig?.CanStart//NPD_PROJECT_LOCAL_BOM_IN_SAP//QCReleaseOrInitialProductionScheduleDifference
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails?.NPD_PROJECT_LOCAL_BOM_IN_SAP,
              null
            ),
            disabled: this.disableFields.noofWeeks,
          }),
          finalAgreed1stProdConfirmedWithBottler: new FormControl({
            //this.taskCustomConfig?.CanStart
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails
                ?.NPD_PROJECT_FINAL_AGREED_1ST_PROD_CONFIRMED_WITH_BOTTLER,
              null
            ),
            disabled: this.disableFields.finalAgreed1stProdConfirmedWithBottler,
          }),
          firstProductionVolume24EQCases: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.projectindexerDetails
                ?.NPD_PROJECT_FIRST_PRODUCTION_VOLUME_24_EQ_CASES,
              null
            ),
            disabled: this.disableFields.firstProductionVolume24EQCases,
          }),
          rmRole: new FormControl({
            ////this.taskCustomConfig?.R_PO_RM_ROLE?.['Identity-id']?.Id
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.taskCustomConfig?.RMRoleName === 'Secondary AW Specialist'
                ? 'Secondary AW Manager'
                : this.taskCustomConfig?.RMRoleName,
              null
            ),
            disabled: true,
          }),
          rmUser: new FormControl({
            value: this.npdTaskService.checkValueIsNullOrEmpty(
              this.taskCustomConfig?.R_PO_RM_ROLE?.['Identity-id']?.Id,
              null
            ),
            disabled:
              this.disableFields.rmUser ||
              (this.npdTaskService.checkValueIsNullOrEmpty(
                this.taskCustomConfig?.RMRoleName,
                null
              )
                ? false
                : true),
          }),
          rmUserId: new FormControl(
            this.npdTaskService.checkValueIsNullOrEmpty(
              this.taskCustomConfig?.R_PO_RM_USER?.['Identity-id']?.Id,
              null
            )
          ),
          rmUserCN: new FormControl(
            this.npdTaskService.checkValueIsNullOrEmpty(
              this.taskCustomConfig?.RMUserCN,
              null
            )
          ),
        });
        if (
          this.taskForm?.controls?.finalManufacturingLocation?.value &&
          this.taskForm?.controls?.finalManufacturingLocation?.value != '' &&
          this.taskForm?.controls?.finalManufacturingLocation?.value != 'NA' &&
          this.showFields.site &&
          !this.showFields.countryofManufacture
        ) {
          this.getEarlyManufacturingSites(
            this.taskForm.controls.finalManufacturingLocation.value
          ).subscribe();
        }

        if (
          this.npdTaskService.checkValueIsNullOrEmpty(
            this.taskCustomConfig?.RMRoleName,
            null
          )
        ) {
          this.showFields.rmRole = true;
          this.filterUserList();
          if (
            this.npdTaskService.checkValueIsNullOrEmpty(
              this.taskCustomConfig?.RMUserCN,
              null
            )
          ) {
            this.selectedRMUser = this.formRMuserValue(this.taskCustomConfig);
          }
          //this.taskForm.controls.rmUser.setValidators([this.autocompleteObjectValidator()]);
        }
        this.taskModalConfig.selectedpredecessor = this.taskModalConfig.selectedPredecessor ? this.getPredecessortask(
          task.predecessor
        ) : null;
        this.taskForm.controls.predecessor.disable();
        if (this.taskModalConfig.selectedpredecessor) {
          const minDateObject = this.commentsUtilService.addUnitsToDate(
            new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE),
            1,
            'days',
            this.enableWorkWeek
          );
          this.taskModalConfig.minDate = minDateObject;
        }
        this.getTaskOwnerAssignmentCount();
        // this.onTaskSelection();
        this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
        this.oldTaskObject = this.npdTaskService.createNewTaskUpdateObject(
          this.taskForm.getRawValue(),
          this.taskConfig.taskType,
          this.taskConfig.isApprovalTask,
          this.taskModalConfig.projectId,
          this.savedReviewers
        );
        /*  this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject
                (this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers); */
        let formValues = this.npdTaskService.createCustomTaskUpdateObject(
          this.taskForm.getRawValue()
        );
        this.oldTaskCustomObject = $.extend(true, {}, formValues);

        if (isFinalTask) {
          this.taskForm.disable();
          this.disableAllControls(true);
        }
        if (!this.taskConfig['isTaskActive']) {
          this.disable.actualEndDate = true;
          this.taskForm.controls?.endDate?.disable();
          /* this.disable.actualStartDate = true;
                this.taskForm.controls?.startDate?.disable(); */
        }
        this.taskForm.updateValueAndValidity();
      }
    } else {
      if (this.taskModalConfig.isNewTask) {
        this.taskStatus = this.taskModalConfig.taskInitialStatusList;
        this.taskForm = new FormGroup({
          taskName: new FormControl({ value: '', disabled: disableField }, [
            Validators.required,
            Validators.maxLength(120),
            Validators.pattern(this.nameStringPattern),
          ]),
          // 205
          /*   duration: new FormControl({ value: '', disabled: (disableField || !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                  durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || !this.enableDuration) }),
                  */
          duration: new FormControl(
            {
              value: '',
              disabled:
                disableField || this.taskModalConfig.isTemplate
                  ? disableField
                  : !this.enableDuration,
            },
            [Validators.required, Validators.max(999), Validators.min(1)]
          ),
          durationType: new FormControl({
            value: this.DurationType[0].value,
            disabled:
              disableField || this.taskModalConfig.isTemplate
                ? disableField
                : !this.enableDuration,
          }),
          status: new FormControl(
            this.taskModalConfig.taskInitialStatusList &&
              this.taskModalConfig.taskInitialStatusList[0] &&
              this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id']
              ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id']
                .Id
              : ''
          ),
          owner: new FormControl({ value: '', disabled: disableField }),
          role: new FormControl({ value: '', disabled: disableField }),
          priority: new FormControl(
            {
              value:
                this.taskModalConfig.taskPriorityList &&
                  this.taskModalConfig.taskPriorityList[0] &&
                  this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id']
                  ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id']
                    .Id
                  : '',
              disabled: disableField,
            },
            [Validators.required]
          ),
          description: new FormControl({ value: '', disabled: disableField }, [
            Validators.maxLength(1000),
          ]),
          revisionReviewRequired: new FormControl({
            value: this.taskConfig.isApprovalTask
              ? this.defaultRevisionRequiredStatus
              : '',
            disabled: disableField,
          }),
          isMilestone: new FormControl({
            value: false,
            disabled: disableField,
          }),
          viewOtherComments: new FormControl({
            value: '',
            disabled: disableField,
          }),
          predecessor: new FormControl({ value: '', disabled: disableField }),
          deliverableApprover: new FormControl({
            value: '',
            disabled: disableField,
          }),
          deliverableReviewers: new FormControl({
            value: '',
            disabled: disableField,
          }),
          allocation: new FormControl({ value: '', disabled: disableField }),
          comments: new FormControl({ value: '', disabled: disableField }),
        });
      } else if (this.taskModalConfig.selectedTask) {
        this.taskStatus = this.projectUtilService.filterStatusOptions(
          this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id,
          this.taskModalConfig.taskStatusList,
          MPM_LEVELS.TASK
        );
        this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(
          this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id,
          this.taskModalConfig.taskStatusList
        );
        //check selected status info-completed/or any other
        const task = this.taskModalConfig.selectedTask;
        const taskField = this.getTaskFields(task);
        const taskRoleId =
          task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id']
            ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id
            : '';
        task.predecessor =
          task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object'
            ? task.PARENT_TASK_ID
            : null;
        this.taskForm = new FormGroup({
          taskName: new FormControl(
            { value: task.NAME, disabled: disableField },
            [
              Validators.required,
              Validators.maxLength(120),
              Validators.pattern(this.nameStringPattern),
            ]
          ),
          // 205
          /* duration: new FormControl({
                    value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                    disabled: (disableField || !this.enableDuration)
                }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({
                    value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                    disabled: (disableField || !this.enableDuration)
                }), */
          duration: new FormControl(
            {
              value:
                typeof task.TASK_DURATION === 'string'
                  ? task.TASK_DURATION
                  : '',
              disabled:
                disableField || this.taskModalConfig.isTemplate
                  ? disableField
                  : !this.enableDuration,
            },
            [Validators.required, Validators.max(999), Validators.min(1)]
          ),
          durationType: new FormControl({
            value:
              typeof task.TASK_DURATION_TYPE === 'string'
                ? task.TASK_DURATION_TYPE
                : this.DurationType[0].value,
            disabled:
              disableField || this.taskModalConfig.isTemplate
                ? disableField
                : !this.enableDuration,
          }),
          status: new FormControl({
            value:
              task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id']
                ? task.R_PO_STATUS['MPM_Status-id'].Id
                : '',
            disabled: true,
          }),
          owner: new FormControl({
            value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
            disabled:
              disableField ||
              (this.selectedStatusInfo &&
                this.selectedStatusInfo.STATUS_TYPE !==
                this.taskConstants.STATUS_TYPE_INITIAL),
          }),
          role: new FormControl({
            value: this.formRoleValue(taskRoleId),
            disabled:
              disableField ||
              (this.selectedStatusInfo &&
                this.selectedStatusInfo.STATUS_TYPE !==
                this.taskConstants.STATUS_TYPE_INITIAL),
          }),
          priority: new FormControl(
            {
              value:
                task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id']
                  ? task.R_PO_PRIORITY['MPM_Priority-id'].Id
                  : '',
              disabled: disableField,
            },
            [Validators.required]
          ),
          description: new FormControl(
            {
              value: this.utilService.isNullOrEmpty(task.DESCRIPTION)
                ? ''
                : task.DESCRIPTION,
              disabled: disableField,
            },
            [Validators.maxLength(1000)]
          ),
          revisionReviewRequired: new FormControl({
            value: this.utilService.getBooleanValue(
              task.REVISION_REVIEW_REQUIRED
            ),
            disabled: true,
          }),
          isMilestone: new FormControl({
            value: this.utilService.getBooleanValue(task.IS_MILESTONE),
            disabled: disableField,
          }),
          viewOtherComments: new FormControl({
            value: taskField.showOtherComments,
            disabled: disableField,
          }),
          predecessor: new FormControl(this.formPredecessor(task.predecessor)),
          deliverableApprover: new FormControl({
            value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
            disabled:
              disableField ||
              (this.selectedStatusInfo &&
                this.selectedStatusInfo.STATUS_TYPE !==
                this.taskConstants.STATUS_TYPE_INITIAL),
          }),
          deliverableReviewers: new FormControl({ value: this.savedReviewers }),
          allocation: new FormControl({
            value: task.ASSIGNMENT_TYPE,
            disabled:
              disableField ||
              (this.selectedStatusInfo &&
                this.selectedStatusInfo.STATUS_TYPE !==
                this.taskConstants.STATUS_TYPE_INITIAL),
          }),
          comments: new FormControl({ value: '', disabled: disableField }),
        });
        this.taskModalConfig.selectedpredecessor = this.taskModalConfig.selectedPredecessor ? this.getPredecessortask(
          task.predecessor
        ) : null;
        this.taskForm.controls.predecessor.disable();
        if (this.taskModalConfig.selectedpredecessor) {
          const minDateObject = this.commentsUtilService.addUnitsToDate(
            new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE),
            1,
            'days',
            this.enableWorkWeek
          );
          this.taskModalConfig.minDate = minDateObject;
        }
        this.getTaskOwnerAssignmentCount();
        // this.onTaskSelection();
        this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
        this.oldTaskObject = this.npdTaskService.createNewTaskUpdateObject(
          this.taskForm.getRawValue(),
          this.taskConfig.taskType,
          this.taskConfig.isApprovalTask,
          this.taskModalConfig.projectId,
          this.savedReviewers
        );
        /*  this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject
                (this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers); */
      }
    }

    this.filteredReviewers = this.taskForm
      .get('deliverableReviewers')
      .valueChanges.pipe(
        startWith(''),
        map((reviewer: string | null) =>
          reviewer ? this.filterReviewers(reviewer) : this.allReviewers.slice()
        )
      );

    this.taskModalConfig.selectedStatusItemId = this.taskForm.value.status;
    // this.taskForm.controls.status.disable();
    this.taskModalConfig.predecessorConfig.formControl =
      this.taskForm.controls.predecessor;
    this.taskModalConfig.taskOwnerConfig.formControl =
      this.taskForm.controls.owner;
    this.taskModalConfig.taskRoleConfig.formControl =
      this.taskForm.controls.role;
    this.taskModalConfig.deliverableApprover.formControl =
      this.taskForm.controls.deliverableApprover;
    this.taskModalConfig.deliverableReviewer.formControl =
      this.taskForm.controls.deliverableReviewer;
    this.taskModalConfig.deliveryPackageConfig.formControl =
      this.taskForm.controls.predecessor;
    if (
      this.taskForm.controls.allocation.value ===
      TaskConstants.ALLOCATION_TYPE_ROLE
    ) {
      if (this.taskForm.controls.owner && !this.taskModalConfig.selectedTask) {
        this.taskForm.controls.owner.disable();
      }
      if (
        this.taskForm.controls.deliverableApprover &&
        !this.taskModalConfig.selectedTask
      ) {
        this.taskForm.controls.deliverableApprover.disable();
      }
      if (
        this.taskForm.controls.deliverableReviewer &&
        !this.taskModalConfig.selectedTask
      ) {
        this.taskForm.controls.deliverableReviewer.disable();
      }
      /* if (this.taskForm.controls.role.value !== '') {
            this.getUsersByRole().subscribe(() => {
                this.taskModalConfig.taskOwnerConfig.filterOptions = this.teamRoleUserOptions;
                this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
            });
        } */
    }

    this.taskForm.controls.predecessor.valueChanges.subscribe((predecessor) => {
      // TODO predecessor change method
      if (
        !this.taskModalConfig.selectedTask &&
        typeof predecessor === 'object' &&
        predecessor &&
        predecessor.value
      ) {
        this.taskModalConfig.taskList.map((task) => {
          if (task['Task-id'].Id === predecessor.value) {
            /*if (task.REVISION_REVIEW_REQUIRED) {
                        this.taskForm.controls.revisionReviewRequired.patchValue(true);
                    }*/
            const minDateObject = this.commentsUtilService.addUnitsToDate(
              new Date(task.DUE_DATE),
              1,
              'days',
              this.enableWorkWeek
            );
            this.taskModalConfig.minDate = minDateObject;
            this.taskModalConfig.maxDate =
              this.taskModalConfig.selectedProject.DUE_DATE;
            if (!this.taskModalConfig.isTemplate) {
              this.taskForm.controls.startDate.setValue(minDateObject);
              this.taskForm.controls.endDate.setValue(
                this.taskModalConfig.selectedProject.DUE_DATE
              );
            }
          }
        });
      } else {
        this.taskModalConfig.minDate = this.taskModalConfig.selectedpredecessor
          ? this.commentsUtilService.addUnitsToDate(
            new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE),
            1,
            'days',
            this.enableWorkWeek
          )
          : this.taskModalConfig.selectedProject.START_DATE;
        this.taskModalConfig.maxDate =
          this.taskModalConfig.selectedProject.DUE_DATE;
        let selectedPredecessor;
        if (this.taskConfig.isApprovalTask) {
          selectedPredecessor =
            this.taskModalConfig.deliveryPackageConfig.filterOptions.find(
              (predecessorValue) => predecessorValue.displayName === predecessor
            );
        } else {
          selectedPredecessor =
            this.taskModalConfig.predecessorConfig.filterOptions.find(
              (predecessorValue) => predecessorValue.displayName === predecessor
            );
        }
        if (selectedPredecessor) {
          this.taskForm.controls.predecessor.setValue(selectedPredecessor);
        }
        // if (!this.taskModalConfig.isTemplate) {
        //     this.taskForm.controls.startDate.setValue(this.taskModalConfig.selectedProject.START_DATE);
        //     this.taskForm.controls.endDate.setValue(this.taskModalConfig.selectedProject.DUE_DATE);
        // }
      }
    });

    if (this.taskForm.controls.owner) {
      this.taskForm.controls.owner.valueChanges.subscribe((ownerValue) => {
        if (
          ownerValue &&
          typeof ownerValue === 'object' &&
          !this.taskModalConfig.isTemplate
        ) {
          this.taskModalConfig.taskOwnerConfig.filterOptions.map(
            (taskOwnerName) => {
              if (taskOwnerName.value === ownerValue.value) {
                this.getTaskOwnerAssignmentCount();
              }
            }
          );
        } else {
          this.ownerProjectCountMessage = '';
          const selectedOwner =
            this.taskModalConfig.taskOwnerConfig.filterOptions.find(
              (owner) => owner.displayName === ownerValue
            );
          if (selectedOwner) {
            this.taskForm.controls.owner.setValue(selectedOwner);
          }
        }
      });
    }
    this.taskForm.controls.allocation.valueChanges.subscribe(
      (allocationTypeValue) => {
        if (this.taskForm.controls.role) {
          this.taskForm.controls.role.setValue('');
        }
        if (this.taskForm.controls.owner) {
          this.taskForm.controls.owner.setValue('');
        }
        if (this.taskForm.controls.deliverableApprover) {
          this.taskForm.controls.deliverableApprover.setValue('');
        }
        if (this.taskForm.controls.deliverableReviewers) {
          this.taskForm.controls.deliverableReviewers.setValue('');
        }
        this.onChangeofAllocationType(allocationTypeValue, null, null, true);
      }
    );

    /* this.taskForm.controls.deliverableReviewers.valueChanges.subscribe(deliverableReviewers => {
        console.log(deliverableReviewers);
    }); */

    this.taskForm.controls.role.valueChanges.subscribe((role) => {
      if (role && typeof role === 'object') {
        this.onChangeofRole(role);
      } else {
        const selectedRole =
          this.taskModalConfig.taskRoleConfig.filterOptions.find(
            (taskRole) => taskRole.displayName === role
          );
        if (selectedRole) {
          this.taskForm.controls.role.setValue(selectedRole);
        }
      }
    });
    /*
    if (this.taskForm.controls.startDate) {
        this.taskForm.controls.startDate.valueChanges.subscribe(() => {
            if (this.taskForm.value.owner && this.taskForm.value.owner.value) {
                this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                    if (taskOwnerName.value === this.taskForm.value.owner.value) {
                        this.getTaskOwnerAssignmentCount();
                    }
                });
            } else {
                this.ownerProjectCountMessage = '';
            }
            if (this.dateValidation && this.dateValidation.startMaxDate && new Date(this.taskForm.controls.startDate.value) > new Date(this.dateValidation.startMaxDate)) {
                this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            } else {
                this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
            }

            if (new Date(this.taskForm.controls.endDate.value) > new Date(this.taskModalConfig.selectedProject.DUE_DATE)) {
                this.durationErrorMessage = this.taskConstants.DURATION_ERROR_MESSAGE;
            }

            if (this.enableDuration) {
                this.validateDuration();
            }

            if (this.dateValidation && this.dateValidation.startMaxDate && new Date(this.taskForm.controls.startDate.value) > new Date(this.dateValidation.startMaxDate)) {
                this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            } else {
                this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
            }


        });




        this.taskForm.controls.endDate.valueChanges.subscribe(() => {
            if (this.taskForm.value.owner && this.taskForm.value.owner.value) {
                this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                    if (taskOwnerName.value === this.taskForm.value.owner.value) {
                        this.getTaskOwnerAssignmentCount();
                    }
                });
            } else {
                this.ownerProjectCountMessage = '';
            }
            if (this.dateValidation && this.dateValidation.endMinDate && new Date(this.taskForm.controls.endDate.value) < new Date(this.dateValidation.endMinDate)) {
                this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            } else if (new Date(this.taskForm.controls.endDate.value) < new Date(this.taskForm.controls.startDate.value)) {
                this.dateErrorMessage = this.taskConstants.START_DATE_ERROR_MESSAGE;
            } else {
                this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
            }



        });
    } */
    if (this.enableDuration) {
      this.taskForm.controls.duration.valueChanges
        .pipe(distinctUntilChanged())
        .subscribe(() => {
          if (
            this.taskForm.controls.OriginalStartDate &&
            this.taskForm.controls.OriginalStartDate.value &&
            this.taskForm.controls.duration &&
            this.taskForm.controls.duration.value
          ) {
            this.validateDuration();
          }
        });
      this.taskForm.controls.durationType.valueChanges
        .pipe(distinctUntilChanged())
        .subscribe(() => {
          if (
            this.taskForm.controls.OriginalStartDate &&
            this.taskForm.controls.OriginalStartDate.value &&
            this.taskForm.controls.duration &&
            this.taskForm.controls.duration.value
          ) {
            this.validateDuration();
          }
        });
    }

    this.taskForm.addControl('comments', null);

    this.taskModalConfig.hasAllConfig = true;
    this.disableLeadFormulaField(true);
    // console.log("now" + this.taskForm.controls.finalManufacturingSite.value);
    // console.log(this.earlyManufacturingSites);
  }

  calculateWeek(weekDate: Date) {
    //let timeStamp = new Date(+`${new Date(weekDate).getTime() + (new Date().getTimezoneOffset() * 60 * 1000)} `).getTime();
    let date = new Date(weekDate);
    const timeStamp = date.getTime();
    let weekDay = this.monsterUtilService.getDayOfWeek(new Date(timeStamp));
    let weekNo;
    if (weekDay == 'Su') {
      weekNo = moment(new Date(timeStamp)).week() - 1; //this.dateArray[i - 1]['weekNo'];
    } else {
      weekNo = moment(new Date(timeStamp)).week();
    }
    return weekNo;
  }

  /*  convertDate(date) {
    return new Date(date).setUTCHours(0);
}
calculateWeek(weekDate: Date) {
    let date = this.convertDate(weekDate);
    const time = new Date(date).getTime() + 4 * 24 * 60 * 60 * 1000;
    const firstDay = new Date( new Date(date).getFullYear() + "/1/1"); //NPD1-83 safari issue;
    return (
      Math.floor(Math.round((time - firstDay.getTime()) / 86400000) / 7) + 1
    );
}  */

  /*   calculateWeek(date: Date) {
  const time = new Date(date).getTime() + 4 * 24 * 60 * 60 * 1000;
  const firstDay = new Date( new Date(date).getFullYear() + "/1/1"); //NPD1-83 safari issue;
  return (
    Math.floor(Math.round((time - firstDay.getTime()) / 86400000) / 7) + 1
  );
}   */
  /* calculateWeek(date: Date) {
    let timeStamp = new Date(date).getTime();
    return (moment(new Date(timeStamp)).week());//-1
}  */
  getCurrentNpdTaskFormValues() {
    const taskFormValues = this.taskForm?.getRawValue();
    if (taskFormValues != undefined) {
      // const taskFormValues = this.taskForm.getRawValue();
      if (taskFormValues?.startDate != '') {
        [taskFormValues.startDate] = this.getStartEndDate(
          taskFormValues.startDate
        );
      }
      if (taskFormValues?.endDate != '') {
        [, taskFormValues.endDate] = this.getStartEndDate(
          taskFormValues.endDate
        );
      }
      [taskFormValues.OriginalStartDate] = this.getStartEndDate(
        taskFormValues.OriginalStartDate
      );
      [, taskFormValues.OriginalEndDate] = this.getStartEndDate(
        taskFormValues.OriginalEndDate
      );

      // if (this.show.BOMSentActual && !this.disableFields.BOMSentActual) {
      //   [taskFormValues.BOMSentActual] = this.getStartEndDate(
      //     taskFormValues?.BOMSentActual
      //   );
      // }
      /* if (this.show.trialDateConfirmed && !this.disableFields.trialDateConfirmed) {
        [ taskFormValues.trialDateConfirmed,] = this.getStartEndDate(taskFormValues.trialDateConfirmed);
    } */

      this.formReviewerObj();
      const TaskObject: TaskUpdateObj =
        this.npdTaskService.createNewTaskUpdateObject(
          taskFormValues,
          this.taskConfig.taskType,
          this.taskConfig.isApprovalTask,
          this.taskModalConfig.projectId,
          this.selectedReviewersObj
        );
      //this.projectUtilService.createNewTaskUpdateObject(taskFormValues, this.taskConfig.taskType, this.taskConfig.isApprovalTask,
      //this.taskModalConfig.projectId, this.selectedReviewersObj);

      let customTaskObject: any =
        this.npdTaskService.createCustomTaskUpdateObject(taskFormValues);

      if (
        taskFormValues.taskName === TaskConfigConstant.allTaskName[0].task04
      ) {
        customTaskObject = this.checkClassificationSimilarity(
          customTaskObject,
          this.requestIndexerDetails,
          this.projectindexerDetails
        );
      }

      if (
        this.taskModalConfig.selectedTask &&
        this.taskModalConfig.selectedTask['Task-id'] &&
        this.taskModalConfig.selectedTask['Task-id'].Id
      ) {
        TaskObject.TaskId.Id = this.taskModalConfig.selectedTask['Task-id'].Id;
        customTaskObject.Task.Id =
          this.taskModalConfig.selectedTask['Task-id'].Id;
        customTaskObject.Task.ItemId =
          this.taskModalConfig.selectedTask['Task-id'].ItemId;
        TaskObject.RevisionReviewRequired = this.utilService.isNullOrEmpty(
          this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED
        )
          ? ''
          : this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED;
        TaskObject.IsMilestone = this.utilService.isNullOrEmpty(
          this.taskModalConfig.selectedTask.IS_MILESTONE
        )
          ? ''
          : this.taskModalConfig.selectedTask.IS_MILESTONE;
        TaskObject.IsMilestone = this.utilService.isNullOrEmpty(
          taskFormValues.isMilestone
        )
          ? ''
          : taskFormValues.isMilestone;
      }
      let object = {
        customTaskObject: customTaskObject,
        TaskObject: TaskObject,
      };
      return object;
    }
  }

  updateTaskDetails(saveOption) {
    const taskFormValues = this.taskForm.getRawValue();
    if (taskFormValues?.startDate != '') {
      [taskFormValues.startDate] = this.getStartEndDate(
        taskFormValues.startDate
      );
    }
    if (taskFormValues?.endDate != '') {
      [, taskFormValues.endDate] = this.getStartEndDate(taskFormValues.endDate);
    }
    [taskFormValues.OriginalStartDate] = this.getStartEndDate(
      taskFormValues.OriginalStartDate
    );
    [, taskFormValues.OriginalEndDate] = this.getStartEndDate(
      taskFormValues.OriginalEndDate
    );

    // if (this.show.BOMSentActual && !this.disableFields.BOMSentActual) {
    //   [taskFormValues.BOMSentActual] = this.getStartEndDate(
    //     taskFormValues.BOMSentActual
    //   );
    // }
    /* if (this.show.trialDateConfirmed && !this.disableFields.trialDateConfirmed) {
        [ taskFormValues.trialDateConfirmed,] = this.getStartEndDate(taskFormValues.trialDateConfirmed);
    }  */

    this.formReviewerObj();
    const TaskObject: TaskUpdateObj =
      this.npdTaskService.createNewTaskUpdateObject(
        taskFormValues,
        this.taskConfig.taskType,
        this.taskConfig.isApprovalTask,
        this.taskModalConfig.projectId,
        this.selectedReviewersObj
      );
    //this.projectUtilService.createNewTaskUpdateObject(taskFormValues, this.taskConfig.taskType, this.taskConfig.isApprovalTask,
    //this.taskModalConfig.projectId, this.selectedReviewersObj);
    const customTaskObject =
      this.npdTaskService.createCustomTaskUpdateObject(taskFormValues);
    if (
      this.taskModalConfig.selectedTask &&
      this.taskModalConfig.selectedTask['Task-id'] &&
      this.taskModalConfig.selectedTask['Task-id'].Id
    ) {
      TaskObject.TaskId.Id = this.taskModalConfig.selectedTask['Task-id'].Id;
      customTaskObject.Task.Id =
        this.taskModalConfig.selectedTask['Task-id'].Id;
      customTaskObject.Task.ItemId =
        this.taskModalConfig.selectedTask['Task-id'].ItemId;
      TaskObject.RevisionReviewRequired = this.utilService.isNullOrEmpty(
        this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED
      )
        ? ''
        : this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED;
      TaskObject.IsMilestone = this.utilService.isNullOrEmpty(
        this.taskModalConfig.selectedTask.IS_MILESTONE
      )
        ? ''
        : this.taskModalConfig.selectedTask.IS_MILESTONE;
      TaskObject.IsMilestone = this.utilService.isNullOrEmpty(
        taskFormValues.isMilestone
      )
        ? ''
        : taskFormValues.isMilestone;
    }
    this.saveNpdTask(TaskObject, customTaskObject, saveOption);
  }

  validateDuration() {
    if (
      this.taskForm.controls.duration &&
      this.taskForm.controls.duration.value
    ) {
      if (
        this.taskForm.controls.duration.value < 1 ||
        this.taskForm.controls.duration.value > 999
      ) {
        this.durationErrorMessage =
          this.taskConstants.DURATION_MIN_ERROR_MESSAGE;
      } else {
        //plannedEndDate change based on duration
        if (this.taskForm.controls.durationType.value === 'weeks') {
          /*   const [ startDate,  ] = this.getStartEndDate(this.taskForm.controls.OriginalStartDate.value);
                let endDateObject = this.commentsUtilService.addUnitsToDate(startDate, this.taskForm.controls.durationType.value === 'weeks' ? (this.taskForm.controls.duration.value) : (this.taskForm.controls.duration.value - 1), this.taskForm.controls.durationType.value, this.enableWorkWeek);
                let endDateActual = this.npdTaskService.subTractUnitsToDate(endDateObject,1,'days');
                const endDateWeek = ''+(this.calculateWeek(endDateActual)-1)+'/'+ new Date(endDateActual).getFullYear();
                this.taskForm.controls.OriginalEndDate.setValue(endDateWeek);
                this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(1)]);
                this.taskForm.controls.duration.updateValueAndValidity();
                if (this.taskModalConfig && this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) {
                    if (new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) >= new Date(this.taskForm.controls.endDate.value)) {
                        const minDuration = this.commentsUtilService.getDaysBetweenTwoDates(new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE), new Date(this.taskForm.controls.endDate.value), this.taskForm.controls.durationType.value);
                        this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(minDuration + 1)]);
                        this.taskForm.controls.duration.updateValueAndValidity();
                        this.durationErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                    }
                } */
          const [startDate] = this.getStartEndDate(
            this.taskForm.controls.OriginalStartDate.value
          );
          //let startdateWithNoHours = new Date(startDate).setUTCHours(0);
          //let startDateValue = new Date(startdateWithNoHours);
          let endDateObject = this.commentsUtilService.addUnitsToDate(
            startDate,
            this.taskForm.controls.durationType.value === 'weeks'
              ? this.taskForm.controls.duration.value
              : this.taskForm.controls.duration.value - 1,
            this.taskForm.controls.durationType.value,
            this.enableWorkWeek
          );
          let endDateActual = this.npdTaskService.subTractUnitsToDate(
            endDateObject,
            1,
            'days'
          );
          const endDateWeek =
            this.monsterUtilService.calculateTaskWeek(endDateActual); //''+(this.calculateWeek(endDateActual)-1)+'/'+ new Date(endDateActual).getFullYear();
          this.taskForm.controls.OriginalEndDate.setValue(endDateWeek);
          this.taskForm.controls.duration.setValidators([
            Validators.required,
            Validators.max(999),
            Validators.min(1),
          ]);
          this.taskForm.controls.duration.updateValueAndValidity();
          if (
            this.taskModalConfig &&
            this.taskModalConfig.selectedTask &&
            this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE
          ) {
            if (
              new Date(
                this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE
              ) >= new Date(this.taskForm.controls.endDate.value)
            ) {
              const minDuration =
                this.commentsUtilService.getDaysBetweenTwoDates(
                  new Date(
                    this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE
                  ),
                  new Date(this.taskForm.controls.endDate.value),
                  this.taskForm.controls.durationType.value
                );
              this.taskForm.controls.duration.setValidators([
                Validators.required,
                Validators.max(999),
                Validators.min(minDuration + 1),
              ]);
              this.taskForm.controls.duration.updateValueAndValidity();
              this.durationErrorMessage =
                this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            }
          }
        } else {
          const [startDate] = this.getStartEndDate(
            this.taskForm.controls.OriginalStartDate.value
          );
          let endDateObject = this.commentsUtilService.addUnitsToDate(
            startDate,
            this.taskForm.controls.durationType.value === 'weeks'
              ? this.taskForm.controls.duration.value
              : this.taskForm.controls.duration.value - 1,
            this.taskForm.controls.durationType.value,
            this.enableWorkWeek
          );
          const endDateWeek =
            '' +
            (this.calculateWeek(endDateObject) - 1) +
            '/' +
            new Date(endDateObject).getFullYear();
          this.taskForm.controls.OriginalEndDate.setValue(endDateWeek);
          this.taskForm.controls.duration.setValidators([
            Validators.required,
            Validators.max(999),
            Validators.min(1),
          ]);
          this.taskForm.controls.duration.updateValueAndValidity();
          if (
            this.taskModalConfig &&
            this.taskModalConfig.selectedTask &&
            this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE
          ) {
            if (
              new Date(
                this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE
              ) >= new Date(this.taskForm.controls.endDate.value)
            ) {
              const minDuration =
                this.commentsUtilService.getDaysBetweenTwoDates(
                  new Date(
                    this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE
                  ),
                  new Date(this.taskForm.controls.endDate.value),
                  this.taskForm.controls.durationType.value
                );
              this.taskForm.controls.duration.setValidators([
                Validators.required,
                Validators.max(999),
                Validators.min(minDuration + 1),
              ]);
              this.taskForm.controls.duration.updateValueAndValidity();
              this.durationErrorMessage =
                this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            }
          }
        }
      }

      /* else {//Actual Comp Date change based on duration
            const [ startDate,  ] = this.getStartEndDate(this.taskForm.controls.startDate.value);
            let endDateObject = this.commentsUtilService.addUnitsToDate(new Date(startDate), this.taskForm.controls.durationType.value === 'weeks' ? (this.taskForm.controls.duration.value) : (this.taskForm.controls.duration.value - 1), this.taskForm.controls.durationType.value, this.enableWorkWeek);
            const endDateWeek = ''+(this.calculateWeek(endDateObject)-1)+'/'+ new Date(endDateObject).getFullYear();
            this.taskForm.controls.endDate.setValue(endDateWeek);
            this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(1)]);
            this.taskForm.controls.duration.updateValueAndValidity();
            if (this.taskModalConfig && this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) {
                if (new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) >= new Date(this.taskForm.controls.endDate.value)) {
                    const minDuration = this.commentsUtilService.getDaysBetweenTwoDates(new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE), new Date(this.taskForm.controls.endDate.value), this.taskForm.controls.durationType.value);
                    this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(minDuration + 1)]);
                    this.taskForm.controls.duration.updateValueAndValidity();
                    this.durationErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                }
            }
        }  */
    }
  }

  /* validateDuration() {
    if (this.taskForm.controls.duration && this.taskForm.controls.duration.value) {
        if (this.taskForm.controls.duration.value < 1 || this.taskForm.controls.duration.value > 999) {
            this.durationErrorMessage = this.taskConstants.DURATION_MIN_ERROR_MESSAGE;
        }
        else {//plannedEndDate change based on duration
            const [ startDate,  ] = this.getStartEndDate(this.taskForm.controls.OriginalStartDate.value);
            let endDateObject = this.commentsUtilService.addUnitsToDate(startDate, this.taskForm.controls.durationType.value === 'weeks' ? (this.taskForm.controls.duration.value) : (this.taskForm.controls.duration.value - 1), this.taskForm.controls.durationType.value, this.enableWorkWeek);
            const endDateWeek = ''+(this.calculateWeek(endDateObject)-1)+'/'+ new Date(endDateObject).getFullYear();
            this.taskForm.controls.OriginalEndDate.setValue(endDateWeek);
            this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(1)]);
            this.taskForm.controls.duration.updateValueAndValidity();
            if (this.taskModalConfig && this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) {
                if (new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) >= new Date(this.taskForm.controls.endDate.value)) {
                    const minDuration = this.commentsUtilService.getDaysBetweenTwoDates(new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE), new Date(this.taskForm.controls.endDate.value), this.taskForm.controls.durationType.value);
                    this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(minDuration + 1)]);
                    this.taskForm.controls.duration.updateValueAndValidity();
                    this.durationErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                }
            }
        }
        /* else {//Actual Comp Date change based on duration
            const [ startDate,  ] = this.getStartEndDate(this.taskForm.controls.startDate.value);
            let endDateObject = this.commentsUtilService.addUnitsToDate(new Date(startDate), this.taskForm.controls.durationType.value === 'weeks' ? (this.taskForm.controls.duration.value) : (this.taskForm.controls.duration.value - 1), this.taskForm.controls.durationType.value, this.enableWorkWeek);
            const endDateWeek = ''+(this.calculateWeek(endDateObject)-1)+'/'+ new Date(endDateObject).getFullYear();
            this.taskForm.controls.endDate.setValue(endDateWeek);
            this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(1)]);
            this.taskForm.controls.duration.updateValueAndValidity();
            if (this.taskModalConfig && this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) {
                if (new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) >= new Date(this.taskForm.controls.endDate.value)) {
                    const minDuration = this.commentsUtilService.getDaysBetweenTwoDates(new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE), new Date(this.taskForm.controls.endDate.value), this.taskForm.controls.durationType.value);
                    this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(minDuration + 1)]);
                    this.taskForm.controls.duration.updateValueAndValidity();
                    this.durationErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                }
            }
        }
    }
} */

  /* getTaskNo(no){
    const tasks = TaskConfigConstant.allTaskConfig;
    const currentTaskIndex = tasks.findIndex(ele => ele.taskNumber == no);
    const roles = [];
    const taskOwner = [];
    let isTaskSuperAccess = (this.filterConfigService.isE2EPMRole() || this.filterConfigService.isProgramManagerRole() || this.checkIsProjectOwner());
    if(currentTaskIndex != -1) {

        let isUserRoleMatched = false;
        const isProjectView = this.taskConfig['router'].includes("project") || this.taskConfig['router'].includes("ResourceManagement")
        if(isProjectView) {
            tasks[currentTaskIndex].role.forEach((item,i) => {
                let userDetails = JSON.parse(sessionStorage.getItem('USER_DETAILS_WORKFLOW')).User.ManagerFor.Target;
                roles[i] = userDetails.some(ele => ele.Name == item);
                });
                isUserRoleMatched = roles.some(ele => ele == true);
        } else {
            tasks[currentTaskIndex].taskOwner.forEach((item,i) => {
                let userDetails = JSON.parse(sessionStorage.getItem('USER_DETAILS_WORKFLOW')).User.ManagerFor.Target;
                taskOwner[i] = userDetails.some(ele => ele.Name == item);
                });
                isUserRoleMatched = taskOwner.some(ele => ele == true);
                // let userDetails = JSON.parse(sessionStorage.getItem('USER_DETAILS_WORKFLOW')).User.ManagerFor.Target;
                // flag = userDetails.some(ele => ele.Name == tasks[currentTaskIndex].taskOwner);
        }

        if(isTaskSuperAccess) {
            this.getfieldAccessByRoles(tasks,currentTaskIndex,false);
        } else if(this.taskConfig['isTaskActive'] && isUserRoleMatched) {
            this.getfieldAccessByRoles(tasks,currentTaskIndex,false)
        } else {
            this.getfieldAccessByRoles(tasks,currentTaskIndex,true);
        }
    }
}
 */
  getTaskNo(no, isFinalTask,taskRole?) {
    const tasks = TaskConfigConstant.allTaskConfig;
    const currentTaskIndex = tasks.findIndex((ele) => ele.taskNumber == no);
    const tasksAccessWithRoles = TaskConfigConstant.TaskAccessRolesWithTaskRole;
    const currentTaskAccessIndex = tasksAccessWithRoles.findIndex((ele) => ele.taskOwner[0] == taskRole);
    let userDisplayName: string = JSON.parse(
      sessionStorage.getItem('USER_DETAILS_WORKFLOW')
    )?.User?.UserDisplayName;
    let isTaskSuperAccess: boolean = this.filterConfigService.isProgramManagerRole() || this.checkIsProjectOwner(); this.filterConfigService.isCorpProjectLeader()
    isTaskSuperAccess = isTaskSuperAccess || (this.filterConfigService.isCorpPMRole() && userDisplayName.includes(this.projectindexerDetails?.NPD_PROJECT_CORP_PM_NAME));
    isTaskSuperAccess = isTaskSuperAccess || (this.filterConfigService.isE2EPMRole() && userDisplayName.includes(this.projectindexerDetails?.NPD_PROJECT_E2E_PM_NAME));


    if (no === 'CP1' || no === 'CP2' || no === 'CP3' || no === 'CP4') {
      this.showFields.rmRole = false;
      return;
    }
    if (currentTaskAccessIndex != -1 && currentTaskIndex != -1) {
      let roles = [];
      let taskOwner = [];
      const tasksWithRoles = TaskConfigConstant.TaskAccessRolesWithTaskRole;
      let isUserRoleMatched = false;
      let isTaskUserRoleMatched = false;
      let userDetails = JSON.parse(
        sessionStorage.getItem('USER_DETAILS_WORKFLOW')
      ).User.ManagerFor.Target;
      let userNameOfRole;
      let isUserRoleNameMatched;

      // console.log(userDisplayName);
      tasksWithRoles[currentTaskAccessIndex].role.forEach((item, i) => {
        if (item == 'Corp PM') {
          roles[i] = userDetails.some(
            (ele) =>
              ele.Name == item &&
              userDisplayName.includes(
                this.projectindexerDetails.NPD_PROJECT_CORP_PM_NAME
              )
          );
        } else if (item == 'Project Specialist') {
          roles[i] =
            userDetails.some((ele) => ele.Name == item) &&
            userDisplayName.includes(
              this.projectindexerDetails.PR_PROJECT_SPECIALIST_NAME
            );
        } else if (item == 'Regulatory Affairs') {
          roles[i] =
            userDetails.some((ele) => ele.Name == item) &&
            userDisplayName.includes(
              this.projectindexerDetails.NPD_PROJECT_REGULATORY_LEAD_NAME
            );
        } else {
          roles[i] = userDetails.some((ele) => ele.Name == item);
        }
      });
      console.log('roles', roles);
      console.log('this.projectindexerDetails', this.projectindexerDetails);
      isTaskSuperAccess =
        isTaskSuperAccess ||
        (this.filterConfigService.isCorpPMRole() &&
          userDisplayName.includes(
            this.projectindexerDetails?.NPD_PROJECT_CORP_PM_NAME
          ));
      roles.forEach((item, i) => {
        if (item == true) {
          if (tasksWithRoles[currentTaskAccessIndex].role[i] === 'Corp PM') {
            userNameOfRole =
              this.projectindexerDetails?.NPD_PROJECT_CORP_PM_NAME;
            isTaskSuperAccess =
              isTaskSuperAccess ||
              this.filterConfigService.isCorpProjectLeader() ||
              (this.filterConfigService.isCorpPMRole() &&
                userDisplayName.includes(userNameOfRole));
            // console.log('isTaskSuperAccess',this.projectindexerDetails.NPD_PROJECT_CORP_PM_NAME)
          } else if (
            tasksWithRoles[currentTaskAccessIndex].role[i] === 'E2E PM'
          ) {
            userNameOfRole = this.projectindexerDetails.NPD_PROJECT_E2E_PM_NAME;
            isTaskSuperAccess =
              isTaskSuperAccess ||
              (this.filterConfigService.isE2EPMRole() &&
                userDisplayName.includes(userNameOfRole));
            // console.log('isTaskSuperAccess',this.projectindexerDetails.NPD_PROJECT_E2E_PM_NAME)
          } else if (tasksWithRoles[currentTaskAccessIndex].role[i] === 'GFG') {
            userNameOfRole = this.projectindexerDetails.NPD_PROJECT_GFG_NAME;
            // console.log('GFG',this.projectindexerDetails.NPD_PROJECT_GFG_NAME)
          } else if (
            tasksWithRoles[currentTaskAccessIndex].role[i] ===
            'Project Specialist'
          ) {
            userNameOfRole =
              this.projectindexerDetails.PR_PROJECT_SPECIALIST_NAME;
            isTaskSuperAccess =
              isTaskSuperAccess ||
              (this.filterConfigService.isProjectSpecialistRole() &&
                userDisplayName.includes(userNameOfRole));
          } else if (
            tasksWithRoles[currentTaskAccessIndex].role[i] ===
            'Corp Project Leader'
          ) {
            isTaskSuperAccess =
              isTaskSuperAccess ||
              this.filterConfigService.isCorpProjectLeader();
          } else if (
            tasksWithRoles[currentTaskAccessIndex].role[i] ===
            'Regs Traffic Manager'
          ) {
            isTaskSuperAccess =
              isTaskSuperAccess ||
              this.filterConfigService.isRegsTrafficManager();
          } else if (
            tasksWithRoles[currentTaskAccessIndex].role[i] ===
            'Regulatory Affairs'
          ) {
            userNameOfRole =
              this.projectindexerDetails.NPD_PROJECT_REGULATORY_LEAD_NAME;
            isTaskSuperAccess =
              isTaskSuperAccess ||
              (this.filterConfigService.isRegulatoryRole() &&
                userDisplayName.includes(userNameOfRole)) ||
              this.filterConfigService.isRegsTrafficManager();
            // userDisplayName.includes(userNameOfRole);
          } // will work only in QA due to indexer mapping
          //else if (tasks[currentTaskIndex].role[i] === 'Ops PM') {
          //   isTaskSuperAccess =
          //     this.filterConfigService.isOPSPMRole() &&
          //     userDisplayName.includes(userNameOfRole);
          // } // not a super user
        }

        isUserRoleMatched = roles.some((ele) => ele == true);
        tasksWithRoles[currentTaskAccessIndex].taskOwner.forEach((item, i) => {
          taskOwner[i] = userDetails.some((ele) => ele.Name == item);
        });
        isTaskUserRoleMatched = taskOwner.some((ele) => ele == true);

        isUserRoleNameMatched =
          userNameOfRole &&
          userDisplayName &&
          userNameOfRole === userDisplayName
            ? true
            : false;
      });
      if (isTaskSuperAccess) {
        this.getfieldAccessByRoles(tasks, currentTaskIndex, false, false);
      } else if (
        (isUserRoleNameMatched && isUserRoleMatched) ||
        (isTaskUserRoleMatched && this.checkIsTaskOwner())
      ) {
        this.getfieldAccessByRoles(tasks, currentTaskIndex, false, false);
      } else {
        let isTaskActionDisable = this.checkIsTaskOwner();
        this.getfieldAccessByRoles(
          tasks,
          currentTaskIndex,
          true,
          !isTaskActionDisable
        );
      }
    } else {
      if ((isTaskSuperAccess || this.checkIsTaskOwner()) && !isFinalTask) {
        this.disable.actualStartDate = false;
        this.disable.actualEndDate = false;
        this.disable.duration = false;
        this.disable.role = false;
        this.disable.status = false;
        this.disable.taskOwner = false;
        this.disable.description = true;

        let isTaskActionDisable = isTaskSuperAccess || this.checkIsTaskOwner();
        this.disable.taskAction = !isTaskActionDisable;
        this.disableCustomConfiguredFields(null, false);
      }
      let isTaskActionDisable = isTaskSuperAccess || this.checkIsTaskOwner();
      this.disable.taskAction = !isTaskActionDisable;
    }
  }

  checkIsProjectOwner() {
    if (
      this.taskConfig?.projectObj?.R_PO_PROJECT_OWNER?.['Identity-id']?.Id ==
      this.sharingService.getcurrentUserItemID()
    ) {
      return true;
    }
    return false;
  }

  disableAllControls(isDisable) {
    (this.disable.actualStartDate = isDisable ? true : false),
      (this.disable.actualEndDate = isDisable ? true : false),
      (this.disable.duration = isDisable ? true : false),
      (this.disable.role = isDisable ? true : false),
      (this.disable.status = isDisable ? true : false);
    this.disable.taskOwner = isDisable ? true : false;
    this.disable.description = isDisable ? true : true;
    this.disableCustomConfiguredFields(null, isDisable);
  }

  checkIsTaskOwner() {
    return (
      this.taskModalConfig?.selectedTask?.R_PO_OWNER_ID?.['Identity-id']?.Id ==
      this.sharingService.getCurrentUserItemID()
    );
  }

  getfieldAccessByRoles(
    tasks,
    currentTaskIndex,
    isDisable,
    isTaskActionDisable
  ) {
    //let isActionAccess = (this.filterConfigService.isCorpPMRole() || this.filterConfigService.isE2EPMRole() || this.filterConfigService.isProgramManagerRole() || this.checkIsProjectOwner() || this.checkIsTaskOwner());
    let isCancelledProject =
      this.projectindexerDetails['PROJECT_STATUS_VALUE'] === 'Cancelled' ||
      this.projectindexerDetails['PROJECT_STATUS_VALUE'] === 'CANCELLED';
    this.disable.actualStartDate = isDisable
      ? true
      : !tasks[currentTaskIndex].actualStartDate;
    this.disable.actualEndDate = isDisable
      ? true
      : !tasks[currentTaskIndex].actualEndDate;
    this.disable.duration = isDisable
      ? true
      : !tasks[currentTaskIndex].duration;
    this.disable.role = isDisable ? true : false;
    this.disable.status = isDisable ? true : false;
    this.disable.taskOwner = isDisable ? true : false;
    this.disable.description = isDisable ? true : true;
    this.disable.taskAction =
      isTaskActionDisable || isCancelledProject ? true : false; //isDisable
    this.showCustomConfiguredFields(tasks[currentTaskIndex]); //npd custom fields
    this.disableCustomConfiguredFields(tasks[currentTaskIndex], isDisable);
    this.getRequiredCustomConfiguredFields(tasks[currentTaskIndex]);
  }

  getValidation(formControlName) {
    let validators = [];
    if (
      this.showFields?.[formControlName] &&
      this.validationRequiredFields?.[formControlName]
    ) {
      validators.push(Validators.required);
    }
    return validators;
  }

  showCustomConfiguredFields(taskConfig) {
    this.showFields = {
      canStart: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'canStart'
      )
        ? true
        : false,
      draftClassification: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'draftClassification'
      )
        ? true
        : false,
      finalClassification: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'finalClassification'
      )
        ? true
        : false,
      site: taskConfig?.show?.find((fieldConfig) => fieldConfig == 'site')
        ? true
        : false,
      projectType: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'projectType'
      )
        ? true
        : false,
      scopingDocumentRequirement: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'scopingDocumentRequirement'
      )
        ? true
        : false,
      winshuttleProject: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'winshuttleProject'
      )
        ? true
        : false,
      requiredComplete: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'requiredComplete'
      )
        ? true
        : false,
      issuanceType: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'issuanceType'
      )
        ? true
        : false,
      pallet: taskConfig?.show?.find((fieldConfig) => fieldConfig == 'pallet')
        ? true
        : false,
      tray: taskConfig?.show?.find((fieldConfig) => fieldConfig == 'tray')
        ? true
        : false,
      isRequired: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'isRequired'
      )
        ? true
        : false,
      trialDateConfirmed: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'trialDateConfirmed'
      )
        ? true
        : false,
      trialVolume24EQCases: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'trialVolume24EQCases'
      )
        ? true
        : false,
      BOMSentActual: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'BOMSentActual'
      )
        ? true
        : false,
      countryofManufacture: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'countryofManufacture'
      )
        ? true
        : false,
      processAuthorityApproval: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'processAuthorityApproval'
      )
        ? true
        : false,
      transactionSchemeAnalysisRequired: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'transactionSchemeAnalysisRequired'
      )
        ? true
        : false,
      nonProprietaryMaterialCostingRequired: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'nonProprietaryMaterialCostingRequired'
      )
        ? true
        : false,
      newFg: taskConfig?.show?.find((fieldConfig) => fieldConfig == 'newFg')
        ? true
        : false,
      newRnDFormula: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'newRnDFormula'
      )
        ? true
        : false,
      newHBCFormula: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'newHBCFormula'
      )
        ? true
        : false,
      newPrimaryPackaging: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'newPrimaryPackaging'
      )
        ? true
        : false,
      secondaryPackaging: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'secondaryPackaging'
      )
        ? true
        : false,
      leadFormula: taskConfig?.show?.find(
          (fieldConfig) => fieldConfig == 'leadFormula'
        )
          ? true
          : false,
      productionSchedule: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'productionSchedule'
      )
        ? true
        : false,
      noofWeeks: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'noofWeeks'
      )
        ? true
        : false,
      finalAgreed1stProdConfirmedWithBottler: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'finalAgreed1stProdConfirmedWithBottler'
      )
        ? true
        : false,
      firstProductionVolume24EQCases: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'firstProductionVolume24EQCases'
      )
        ? true
        : false,
      earlyProjectType: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'earlyProjectType'
      )
        ? true
        : false,
      earlyManufacturingLocation: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'earlyManufacturingLocation'
      )
        ? true
        : false,
      earlyManufacturingSite: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'earlyManufacturingSite'
      )
        ? true
        : false,
      rmRole: false,
      taskOwnerRMUser: taskConfig?.show?.find(
        (fieldConfig) => fieldConfig == 'taskOwnerRMUser'
      ),
    };
  }

  disableCustomConfiguredFields(taskConfig, disable) {
    this.disableFields = {
      canStart: true, //disable? true: false,
      draftClassification: true, //disable? true: false,
      finalClassification: true, //disable? true: false,
      site:
        disable ||
          taskConfig?.taskNumber == '06' ||
          taskConfig?.taskNumber == '04'
          ? true
          : false,
      countryofManufacture:
        disable || taskConfig?.taskNumber == '04' ? true : false,
      projectType: disable ? true : false,
      scopingDocumentRequirement: true, //disable? true: false,
      winshuttleProject: disable ? true : false,
      requiredComplete: disable ? true : false,
      issuanceType: true, //disable? true: false,
      pallet: true, //disable? true: false,
      tray: true, //disable? true: false,
      isRequired: disable ? true : false,
      trialDateConfirmed: disable ? true : false,
      BOMSentActual: disable ? true : false,
      processAuthorityApproval: disable ? true : false,
      transactionSchemeAnalysisRequired: disable
        ? true
        : this.projectindexerDetails.NPD_PROJECT_IS_FG_PROJECT === 'true' ||
          this.projectindexerDetails.NPD_PROJECT_IS_FG_PROJECT === true
          ? true
          : false,
      nonProprietaryMaterialCostingRequired: disable ? true : false,
      newFg: disable ? true : false,
      newRnDFormula: disable ? true : false,
      newHBCFormula: disable ? true : false,
      newPrimaryPackaging: disable ? true : false,
      secondaryPackaging: disable ? true : false,
      leadFormula: disable ? true : false,
      productionSchedule: disable ? true : false,
      noofWeeks: true,
      finalAgreed1stProdConfirmedWithBottler: disable ? true : false, //true//disable? true: false,
      firstProductionVolume24EQCases: disable ? true : false,
      earlyProjectType: true,
      earlyManufacturingLocation: true,
      earlyManufacturingSite: true,
      rmUser: disable ? true : false,
      taskOwnerRMUser: disable ? true : false,
      trialVolume24EQCases: disable ? true : false,
    };
  }

  getRequiredCustomConfiguredFields(taskConfig) {
    this.validationRequiredFields = {
      scopingDocumentRequirement: taskConfig?.required?.find(
        (fieldConfig) => fieldConfig == 'scopingDocumentRequirement'
      )
        ? true
        : false,
      processAuthorityApproval: taskConfig?.required?.find(
        (fieldConfig) => fieldConfig == 'processAuthorityApproval'
      )
        ? true
        : false,
      transactionSchemeAnalysisRequired: taskConfig?.required?.find(
        (fieldConfig) => fieldConfig == 'transactionSchemeAnalysisRequired'
      )
        ? true
        : false,
      nonProprietaryMaterialCostingRequired: taskConfig?.required?.find(
        (fieldConfig) => fieldConfig == 'nonProprietaryMaterialCostingRequired'
      )
        ? true
        : false,
      requiredComplete: taskConfig?.required?.find(
        (fieldConfig) => fieldConfig == 'requiredComplete'
      )
        ? true
        : false,
      productionSchedule: taskConfig?.required?.find(
        (fieldConfig) => fieldConfig == 'productionSchedule'
      )
        ? true
        : false,
    };
  }

  getTaskNumberFromTask() {
    if (this.taskModalConfig) {
      const task = this.taskModalConfig.selectedTask;
      let taskNo = task?.NAME;
      if (taskNo) {
        taskNo = taskNo.replace(' ', '');
        taskNo = taskNo.replace('Task', ''); //string = string.replace("Task","");
        taskNo = taskNo.replace('TASK', '');
      }
      return taskNo;
    }
  }

  checkTaskDetails() {
    const task = this.taskModalConfig.selectedTask;
    let taskNo = task?.NAME;
    taskNo = taskNo.replace(' ', '');
    taskNo = taskNo.replace('Task', ''); //string = string.replace("Task","");
    taskNo = taskNo.replace('TASK', '');
    const tasks = TaskConfigConstant.allTaskConfig;
    const currentTaskIndex = tasks.findIndex((ele) => ele.taskNumber == taskNo);
    if (currentTaskIndex != -1 && (taskNo == '04' || taskNo == '05')) {
      return true;
    }
    return false;
  }
  loadData() {
    this.taskModalConfig.taskAssignmentTypes = TaskConstants.ALLOCATION_TYPE;
    this.taskModalConfig.taskTypes = TaskConstants.TASK_TYPE_LIST;
    if (this.taskModalConfig.isNewTask) {
      this.loaderService.hide();
      this.initialiseTaskForm();
    } else {
      const ownerId =
        this.taskModalConfig.selectedTask &&
          this.taskModalConfig.selectedTask.R_PO_OWNER_ID &&
          this.taskModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id']
          ? this.taskModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id'].Id
          : '';
      forkJoin([
        this.taskService.getTaskDataValues(this.taskConfig.taskId),
        this.formOwnerValue(ownerId),
        //this.getDeliverableReviewByTaskId(this.taskConfig.taskId),
      ]).subscribe(
        (response) => {
          this.loaderService.hide();
          this.dateValidation = response[0];
          TaskConstants.TASK_TYPE_LIST.forEach((element) => {
            if (
              this.taskModalConfig &&
              this.taskModalConfig.selectedTask &&
              this.taskModalConfig.selectedTask.TASK_TYPE &&
              element.NAME === this.taskModalConfig.selectedTask.TASK_TYPE
            ) {
              this.existingTaskTypeDetails = element;
            }
          });
          TaskConstants.ALLOCATION_TYPE.forEach((element) => {
            if (
              this.taskModalConfig &&
              this.taskModalConfig.selectedTask &&
              this.taskModalConfig.selectedTask.ALLOCATION_TYPE &&
              element.NAME === this.taskModalConfig.selectedTask.ALLOCATION_TYPE
            ) {
              this.existingTaskTypeDetails = element;
            }
          });

          this.initialiseTaskForm();
        },
        () => {
          this.loaderService.hide();
        }
      );
    }
  }

  assignMPMTaskValuesToSelectedTask(key,value){
    this.taskModalConfig.selectedTask[key] = value;
  }

//   formRoleValue(taskRoleID) {
//     if (taskRoleID) {
//         const selectedRole = this.teamRolesOptions.find(element => element.name === taskRoleID);
//         return (selectedRole) ? selectedRole : '';
//     } else {
//         return '';
//     }
// }

formOwnerValue(taskUserID): Observable<any> {
  return new Observable(observer => {
      if (!this.utilService.isNullOrEmpty(taskUserID)) {
          const parameters = {
              userId: taskUserID
          };
          this.appService.getPersonByIdentityUserId(parameters).subscribe(response => {
              const selectedUser = this.teamRoleUserOptions.find(element => element.value === response.userCN);
              if (selectedUser) {
                  this.selectedOwner = selectedUser;
              }
              observer.next(true);
              observer.complete();
          }, error => {
              const eventData = { message: '', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
              this.notificationHandler.next(eventData);
              observer.next(false);
              observer.complete();
          });
      } else {
          observer.next('');
          observer.complete();
      }
  });
}
/************************************* */
formRoleValue(taskRoleID) {
  if (taskRoleID) {
      const selectedRole = this.teamRolesOptions.find(element => element.name === taskRoleID);
      return (selectedRole) ? selectedRole : '';
  } else {
      return '';
  }
}
/************************************* */

  getPreDetails() {
    this.selectedTaskIndexer = this.taskModalConfig.selectedTask;
    this.getProjectDetails(this.taskConfig.projectObj).subscribe(
      () => {
        this.getCustomTaskDetailsAndRMRoles(this.taskConfig.isApprovalTask).subscribe(
          () => {
            if (
              this.taskModalConfig.categoryId &&
              this.taskModalConfig.teamId
            ) {
              /*************************************************************************************** */
              let currSelectedTask = this.taskModalConfig.selectedTask;
              this.assignMPMTaskValuesToSelectedTask("R_PO_TEAM_ROLE", {
                "MPM_Team_Role_Mapping-id": {
                  "Id": currSelectedTask.TASK_ROLE_ID?.toString()
              }
              })
              this.assignMPMTaskValuesToSelectedTask("R_PO_OWNER_ID",{
                "Identity-id": {
                    "Id": currSelectedTask.TASK_OWNER_ID?.toString()
                }
              })
              this.assignMPMTaskValuesToSelectedTask("R_PO_STATUS",{
                "MPM_Status-id": {
                  "Id": currSelectedTask.TASK_STATUS_ID?.toString()
              }
              })
              this.assignMPMTaskValuesToSelectedTask("Task-id",{
                  "Id": currSelectedTask.ID,
                  "ItemId": currSelectedTask.ITEM_ID
              })
              this.assignMPMTaskValuesToSelectedTask("R_PO_PRIORITY",{
                "MPM_Priority-id": {
                    "Id": currSelectedTask.TASK_PRIORITY_ID
                }
            })
              this.assignMPMTaskValuesToSelectedTask("ASSIGNMENT_TYPE", currSelectedTask.TASK_ASSIGNMENT_TYPE);
              this.assignMPMTaskValuesToSelectedTask("NAME",currSelectedTask.TASK_NAME);
              this.assignMPMTaskValuesToSelectedTask("DESCRIPTION",currSelectedTask.TASK_DESCRIPTION);
              this.assignMPMTaskValuesToSelectedTask("START_DATE",currSelectedTask.TASK_START_DATE);
              this.assignMPMTaskValuesToSelectedTask("ORIGINAL_DUE_DATE", currSelectedTask?.ORIGINAL_TASK_DUE_DATE.toString());
              this.assignMPMTaskValuesToSelectedTask("EXPECTED_DURATION",currSelectedTask.EXPECTED_TASK_DURATION);
              this.assignMPMTaskValuesToSelectedTask("TASK_DURATION",currSelectedTask.TASK_DURATION?.toString());
              this.assignMPMTaskValuesToSelectedTask("DUE_DATE",currSelectedTask.TASK_DUE_DATE);
              this.assignMPMTaskValuesToSelectedTask("IS_ACTIVE",currSelectedTask.IS_ACTIVE_TASK);
              /***************************************************************************************** */
              this.getAllRoles().subscribe(() => {
                //let isClassificationTask = this.checkTaskDetails();
                forkJoin([
                  this.getDraftManufacturingLocation(),
                  this.getPriorities(),
                  this.getStatus(),
                  this.getInitialStatus(),
                  this.getUsersByRole(),

                  //this.getReviewers(),
                ]) //,isClassificationTask ? this.getTaskRequestDetailsById():[]
                  .subscribe(() => {
                    /*  if(isClassificationTask) {
                                        this.loaderService.show();
                                        this.getTaskRequestDetailsById().subscribe(requestResponse => {
                                          this.loadData();
                                        },(error)=> {
                                            const eventData = { message: 'Something went wrong while fetching classification details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                                            this.notificationHandler.next(eventData);
                                            this.loaderService.hide();
                                        })
                                    } else  */

                    // let currentTaskId =
                    //   this.taskModalConfig.selectedTask['Task-id'].Id;
                    // let projectId = this.taskModalConfig['projectId'];
                    // let otherTasks: any = [];
                    // let adhocTasks: any = [];
                    // let adhocTasksForOtherProject: any = [];
                    //service to get all the workflow rules for the given target task id
                    // this.npdTaskService
                    //   .getDependentTasksForTargetTaskId(currentTaskId, this.classificationNumber)
                    //   .subscribe((response) => {
                    //     let dependencyResponse = response?.tuple;
                    //     if (dependencyResponse?.length) {
                    //       dependencyResponse?.forEach((dependency) => {
                    //         //condition to segregrate current project and other project tasks
                    //         if (
                    //           dependency?.old?.WorkflowRules?.PROJECT_ID ==
                    //           projectId
                    //         ) {
                    //           if (
                    //             dependency?.old?.WorkflowRules?.TASK_NAME.includes(
                    //               '~'
                    //             )
                    //           ) {
                    //             adhocTasks.push({
                    //               projectBusinessId:
                    //                 dependency?.old?.WorkflowRules
                    //                   ?.PROJECT_BUSINESS_ID,
                    //               taskId:
                    //                 dependency?.old?.WorkflowRules?.TASK_ID,
                    //               taskName:
                    //                 dependency?.old?.WorkflowRules?.TASK_NAME,
                    //               projectId:
                    //                 dependency?.old?.WorkflowRules?.PROJECT_ID,
                    //               taskDescription:
                    //                 dependency?.old?.WorkflowRules
                    //                   ?.TASK_DESCRIPTION,
                    //             });
                    //           } else {
                    //             this.dataSource.push({
                    //               projectBusinessId:
                    //                 dependency?.old?.WorkflowRules
                    //                   ?.PROJECT_BUSINESS_ID,
                    //               taskId:
                    //                 dependency?.old?.WorkflowRules?.TASK_ID,
                    //               taskName:
                    //                 dependency?.old?.WorkflowRules?.TASK_NAME,
                    //               projectId:
                    //                 dependency?.old?.WorkflowRules?.PROJECT_ID,
                    //               taskDescription:
                    //                 dependency?.old?.WorkflowRules
                    //                   ?.TASK_DESCRIPTION,
                    //             });
                    //           }
                    //         } else {
                    //           if (
                    //             dependency?.old?.WorkflowRules?.TASK_NAME.includes(
                    //               '~'
                    //             )
                    //           ) {
                    //             adhocTasksForOtherProject.push({
                    //               projectBusinessId:
                    //                 dependency?.old?.WorkflowRules
                    //                   ?.PROJECT_BUSINESS_ID,
                    //               taskId:
                    //                 dependency?.old?.WorkflowRules?.TASK_ID,
                    //               taskName:
                    //                 dependency?.old?.WorkflowRules?.TASK_NAME,
                    //               projectId:
                    //                 dependency?.old?.WorkflowRules?.PROJECT_ID,
                    //               taskDescription:
                    //                 dependency?.old?.WorkflowRules
                    //                   ?.TASK_DESCRIPTION,
                    //             });
                    //           } else {
                    //             otherTasks.push({
                    //               projectBusinessId:
                    //                 dependency?.old?.WorkflowRules
                    //                   ?.PROJECT_BUSINESS_ID,
                    //               taskId:
                    //                 dependency?.old?.WorkflowRules?.TASK_ID,
                    //               taskName:
                    //                 dependency?.old?.WorkflowRules?.TASK_NAME,
                    //               projectId:
                    //                 dependency?.old?.WorkflowRules?.PROJECT_ID,
                    //               taskDescription:
                    //                 dependency?.old?.WorkflowRules
                    //                   ?.TASK_DESCRIPTION,
                    //             });
                    //           }
                    //         }
                    //       });
                    //     } else {
                    //       if (
                    //         dependencyResponse?.old?.WorkflowRules
                    //           ?.PROJECT_ID == projectId
                    //       ) {
                    //         if (
                    //           dependencyResponse?.old?.WorkflowRules?.TASK_NAME.includes(
                    //             '~'
                    //           )
                    //         ) {
                    //           adhocTasks.push({
                    //             projectBusinessId:
                    //               dependencyResponse?.old?.WorkflowRules
                    //                 ?.PROJECT_BUSINESS_ID,
                    //             taskId:
                    //               dependencyResponse?.old?.WorkflowRules
                    //                 ?.TASK_ID,
                    //             taskName:
                    //               dependencyResponse?.old?.WorkflowRules
                    //                 ?.TASK_NAME,
                    //             projectId:
                    //               dependencyResponse?.old?.WorkflowRules
                    //                 ?.PROJECT_ID,
                    //             taskDescription:
                    //               dependencyResponse?.old?.WorkflowRules
                    //                 ?.TASK_DESCRIPTION,
                    //           });
                    //         }
                    //         this.dataSource.push({
                    //           projectBusinessId:
                    //             dependencyResponse?.old?.WorkflowRules
                    //               ?.PROJECT_BUSINESS_ID,
                    //           taskId:
                    //             dependencyResponse?.old?.WorkflowRules?.TASK_ID,
                    //           taskName:
                    //             dependencyResponse?.old?.WorkflowRules
                    //               ?.TASK_NAME,
                    //           projectId:
                    //             dependencyResponse?.old?.WorkflowRules
                    //               ?.PROJECT_ID,
                    //           taskDescription:
                    //             dependencyResponse?.old?.WorkflowRules
                    //               ?.TASK_DESCRIPTION,
                    //         });
                    //       } else {
                    //         if (dependencyResponse) {
                    //           if (
                    //             dependencyResponse?.old?.WorkflowRules?.TASK_NAME.includes(
                    //               '~'
                    //             )
                    //           ) {
                    //             adhocTasksForOtherProject.push({
                    //               projectBusinessId:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.PROJECT_BUSINESS_ID,
                    //               taskId:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.TASK_ID,
                    //               taskName:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.TASK_NAME,
                    //               projectId:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.PROJECT_ID,
                    //               taskDescription:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.TASK_DESCRIPTION,
                    //             });
                    //           } else {
                    //             otherTasks.push({
                    //               projectBusinessId:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.PROJECT_BUSINESS_ID,
                    //               taskId:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.TASK_ID,
                    //               taskName:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.TASK_NAME,
                    //               projectId:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.PROJECT_ID,
                    //               taskDescription:
                    //                 dependencyResponse?.old?.WorkflowRules
                    //                   ?.TASK_DESCRIPTION,
                    //             });
                    //           }
                    //         }
                    //       }
                    //     }
                    //     //concatinating both current project and other project's task
                    //     this.dataSource = this.dataSource.concat(adhocTasks);
                    //     this.dataSource = this.dataSource.concat(otherTasks);
                    //     this.dataSource = this.dataSource.concat(
                    //       adhocTasksForOtherProject
                    //     );

                        if (
                          this.getTaskNumberFromTask() == 39 ||
                          this.getTaskNumberFromTask() == '09' ||
                          this.getTaskNumberFromTask() == 42 ||
                          this.getTaskNumberFromTask() == 36 ||
                          this.getTaskNumberFromTask() == 39 ||
                          this.getTaskNumberFromTask() == 39 ||
                          this.getTaskNumberFromTask() == 39 ||
                          this.getTaskNumberFromTask() == '04' ||
                          this.getTaskNumberFromTask() == '05' ||
                          this.getTaskNumberFromTask() == '06'
                        ) {
                          //
                          // this.loadData();

                          //

                          this.getProjectIndexerDetails().subscribe(
                            (projectResponse) => {
                            if (
                                this.getTaskNumberFromTask() == '04' ||
                                this.getTaskNumberFromTask() == '05'
                              ) {
                                // this.getRequestIndexerDetails(
                                //   this.projectindexerDetails.PR_REQUEST_ITEM_ID
                                // ).subscribe((response) => {

                                    this.getEarlyClassificationForFinalClassification();



                                // });
                              }
                              this.loadData();
                          //   },
                          //   (error) => {
                          //     const eventData = {
                          //       message:
                          //         'Something went wrong while fetching details',
                          //       type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
                          //     };
                          //     this.notificationHandler.next(eventData);
                          //     this.loaderService.hide();
                          //   }
                          // );
                          //
                            });

                        } else {
                          this.loadData();
                        }
                      });
                  });
              // });
            }
          },
          () => {
            this.loaderService.hide();
            const eventData = {
              message: 'Something went wrong while fetching Tasks',
              type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
            };
            this.notificationHandler.next(eventData);
          }
        );
      },
      () => {
        this.loaderService.hide();
        const eventData = {
          message: 'Something went wrong while fetching Project Details',
          type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
        };
        this.notificationHandler.next(eventData);
      }
    );
  }
  getDependentTasksForCurrentTask() { }

  openDependencyPanel(){
    if(this.dataSource.data.length == 0){
    this.loaderService.show();
    let currentTaskId = this.taskModalConfig.selectedTask['Task-id'].Id;
                    let projectId = this.taskModalConfig['projectId'];
                    let otherTasks: any = [];
                    let adhocTasks: any = [];
                    let adhocTasksForOtherProject: any = [];
    this.npdTaskService
                      .getDependentTasksForTargetTaskId(currentTaskId, this.classificationNumber)
                      .subscribe((response) => {
                        let dependencyResponse = response?.tuple;
                        if (dependencyResponse?.length) {
                          dependencyResponse?.forEach((dependency) => {
                            //condition to segregrate current project and other project tasks
                            if (
                              dependency?.old?.WorkflowRules?.PROJECT_ID ==
                              projectId
                            ) {
                              if (
                                dependency?.old?.WorkflowRules?.TASK_NAME.includes(
                                  '~'
                                )
                              ) {
                                adhocTasks.push({
                                  projectBusinessId:
                                    dependency?.old?.WorkflowRules
                                      ?.PROJECT_BUSINESS_ID,
                                  taskId:
                                    dependency?.old?.WorkflowRules?.TASK_ID,
                                  taskName:
                                    dependency?.old?.WorkflowRules?.TASK_NAME,
                                  projectId:
                                    dependency?.old?.WorkflowRules?.PROJECT_ID,
                                  taskDescription:
                                    dependency?.old?.WorkflowRules
                                      ?.TASK_DESCRIPTION,
                                });
                              } else {
                                this.dataSource.data.push({
                                  projectBusinessId:
                                    dependency?.old?.WorkflowRules
                                      ?.PROJECT_BUSINESS_ID,
                                  taskId:
                                    dependency?.old?.WorkflowRules?.TASK_ID,
                                  taskName:
                                    dependency?.old?.WorkflowRules?.TASK_NAME,
                                  projectId:
                                    dependency?.old?.WorkflowRules?.PROJECT_ID,
                                  taskDescription:
                                    dependency?.old?.WorkflowRules
                                      ?.TASK_DESCRIPTION,
                                });
                              }
                            } else {
                              if (
                                dependency?.old?.WorkflowRules?.TASK_NAME.includes(
                                  '~'
                                )
                              ) {
                                adhocTasksForOtherProject.push({
                                  projectBusinessId:
                                    dependency?.old?.WorkflowRules
                                      ?.PROJECT_BUSINESS_ID,
                                  taskId:
                                    dependency?.old?.WorkflowRules?.TASK_ID,
                                  taskName:
                                    dependency?.old?.WorkflowRules?.TASK_NAME,
                                  projectId:
                                    dependency?.old?.WorkflowRules?.PROJECT_ID,
                                  taskDescription:
                                    dependency?.old?.WorkflowRules
                                      ?.TASK_DESCRIPTION,
                                });
                              } else {
                                otherTasks.push({
                                  projectBusinessId:
                                    dependency?.old?.WorkflowRules
                                      ?.PROJECT_BUSINESS_ID,
                                  taskId:
                                    dependency?.old?.WorkflowRules?.TASK_ID,
                                  taskName:
                                    dependency?.old?.WorkflowRules?.TASK_NAME,
                                  projectId:
                                    dependency?.old?.WorkflowRules?.PROJECT_ID,
                                  taskDescription:
                                    dependency?.old?.WorkflowRules
                                      ?.TASK_DESCRIPTION,
                                });
                              }
                            }
                          });
                        } else {
                          if (
                            dependencyResponse?.old?.WorkflowRules
                              ?.PROJECT_ID == projectId
                          ) {
                            if (
                              dependencyResponse?.old?.WorkflowRules?.TASK_NAME.includes(
                                '~'
                              )
                            ) {
                              adhocTasks.push({
                                projectBusinessId:
                                  dependencyResponse?.old?.WorkflowRules
                                    ?.PROJECT_BUSINESS_ID,
                                taskId:
                                  dependencyResponse?.old?.WorkflowRules
                                    ?.TASK_ID,
                                taskName:
                                  dependencyResponse?.old?.WorkflowRules
                                    ?.TASK_NAME,
                                projectId:
                                  dependencyResponse?.old?.WorkflowRules
                                    ?.PROJECT_ID,
                                taskDescription:
                                  dependencyResponse?.old?.WorkflowRules
                                    ?.TASK_DESCRIPTION,
                              });
                            }
                            this.dataSource.data.push({
                              projectBusinessId:
                                dependencyResponse?.old?.WorkflowRules
                                  ?.PROJECT_BUSINESS_ID,
                              taskId:
                                dependencyResponse?.old?.WorkflowRules?.TASK_ID,
                              taskName:
                                dependencyResponse?.old?.WorkflowRules
                                  ?.TASK_NAME,
                              projectId:
                                dependencyResponse?.old?.WorkflowRules
                                  ?.PROJECT_ID,
                              taskDescription:
                                dependencyResponse?.old?.WorkflowRules
                                  ?.TASK_DESCRIPTION,
                            });
                          } else {
                            if (dependencyResponse) {
                              if (
                                dependencyResponse?.old?.WorkflowRules?.TASK_NAME.includes(
                                  '~'
                                )
                              ) {
                                adhocTasksForOtherProject.push({
                                  projectBusinessId:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.PROJECT_BUSINESS_ID,
                                  taskId:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.TASK_ID,
                                  taskName:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.TASK_NAME,
                                  projectId:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.PROJECT_ID,
                                  taskDescription:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.TASK_DESCRIPTION,
                                });
                              } else {
                                otherTasks.push({
                                  projectBusinessId:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.PROJECT_BUSINESS_ID,
                                  taskId:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.TASK_ID,
                                  taskName:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.TASK_NAME,
                                  projectId:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.PROJECT_ID,
                                  taskDescription:
                                    dependencyResponse?.old?.WorkflowRules
                                      ?.TASK_DESCRIPTION,
                                });
                              }
                            }
                          }
                        }
                        //concatinating both current project and other project's task
                        this.dataSource.data = this.dataSource.data.concat(adhocTasks);
                        this.dataSource.data = this.dataSource.data.concat(otherTasks);
                        this.dataSource.data = this.dataSource.data.concat(
                          adhocTasksForOtherProject
                        );
                        this.loaderService.hide();
                        });
                      }
  }

  getPreDetailsForTask() {
    this.getProjectDetails(this.taskConfig.projectObj).subscribe(
      () => {
        this.getDependentTasks(this.taskConfig.isApprovalTask).subscribe(
          () => {
            if (
              this.taskModalConfig.categoryId &&
              this.taskModalConfig.teamId
            ) {
              this.getAllRoles().subscribe(() => {
                //let isClassificationTask = this.checkTaskDetails();
                forkJoin([
                  this.getDraftManufacturingLocation(),
                  this.getPriorities(),
                  this.getStatus(),
                  this.getInitialStatus(),
                  this.getUsersByRole(),
                  //this.getReviewers(),
                ]) //,isClassificationTask ? this.getTaskRequestDetailsById():[]
                  .subscribe(() => {
                    /*  if(isClassificationTask) {
                                        this.loaderService.show();
                                        this.getTaskRequestDetailsById().subscribe(requestResponse => {
                                          this.loadData();
                                        },(error)=> {
                                            const eventData = { message: 'Something went wrong while fetching classification details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                                            this.notificationHandler.next(eventData);
                                            this.loaderService.hide();
                                        })
                                    } else  */
                    if (
                      this.getTaskNumberFromTask() == 39 ||
                      this.getTaskNumberFromTask() == '09' ||
                      this.getTaskNumberFromTask() == 42 ||
                      this.getTaskNumberFromTask() == 36 ||
                      this.getTaskNumberFromTask() == 39 ||
                      this.getTaskNumberFromTask() == 39 ||
                      this.getTaskNumberFromTask() == 39 ||
                      this.getTaskNumberFromTask() == '04' ||
                      this.getTaskNumberFromTask() == '05' ||
                      this.getTaskNumberFromTask() == '06'
                    ) {
                      this.getProjectIndexerDetails().subscribe(
                        (projectResponse) => {
                          if (
                            this.getTaskNumberFromTask() == '04' ||
                            this.getTaskNumberFromTask() == '05'
                          ) {
                            this.getRequestIndexerDetails(
                              this.projectindexerDetails.PR_REQUEST_ITEM_ID
                            ).subscribe((response) => {
                              this.getEarlyClassificationForFinalClassification();
                            });
                          }
                          this.loadData();
                        },
                        (error) => {
                          const eventData = {
                            message:
                              'Something went wrong while fetching details',
                            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
                          };
                          this.notificationHandler.next(eventData);
                          this.loaderService.hide();
                        }
                      );
                    } else {
                      this.loadData();
                    }
                  });
              });
            }
          },
          () => {
            this.loaderService.hide();
            const eventData = {
              message: 'Something went wrong while fetching Tasks',
              type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
            };
            this.notificationHandler.next(eventData);
          }
        );
      },
      () => {
        this.loaderService.hide();
        const eventData = {
          message: 'Something went wrong while fetching Project Details',
          type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
        };
        this.notificationHandler.next(eventData);
      }
    );
  }
  getDraftManufacturingLocation(): Observable<any> {
    return new Observable((observer) => {
      this.monsterConfigApiService
        .getAllDraftManufacturingLocations()
        .subscribe(
          (response) => {
            this.allDraftManufacturingLocations = response;
            let draftManufacturingLocationOptions = [];
            draftManufacturingLocationOptions = response;
            draftManufacturingLocationOptions.forEach(
              (draftManufacturingLocation) => {
                let location = {
                  ItemId: draftManufacturingLocation?.Identity?.ItemId,
                  Id: draftManufacturingLocation?.Identity?.Id,
                  displayName:
                    draftManufacturingLocation?.['R_PO_MARKET$Properties']
                      ?.DisplayName,
                  regulatoryClassification:
                    draftManufacturingLocation?.['R_PO_MARKET$Properties']
                      ?.RegulatoryClassification,
                };
                this.draftManufacturingLocations.push(location);
              }
            );
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  checkValidations(isSave) {
    if (!this.validateClassification(isSave)) {
      return true;
    }
    if (
      this.taskForm.getRawValue().deliverableApprover &&
      this.taskForm.getRawValue().deliverableApprover.value
    ) {
      const data = this.savedReviewers.find(
        (reviewer) =>
          reviewer.value ===
          this.taskForm.getRawValue().deliverableApprover.value
      );
      if (data) {
        const eventData = {
          message: 'Selected reviewer is already the approver',
          type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
        };
        this.notificationHandler.next(eventData);
        this.disableTaskSave = false;
        return true;
      }
    }

    if (isSave && this.taskForm.pristine && !this.chipChanged) {
      const eventData = {
        message: 'Kindly make changes to update the task',
        type: ProjectConstant.NOTIFICATION_LABELS.WARN,
      };
      this.notificationHandler.next(eventData);
      this.disableTaskSave = false;
      return true;
    }

    const nameSplit = this.taskForm.value.taskName.split('_');
    if (nameSplit[0].length === 0) {
      const eventData = {
        message: 'Name should not start with "_"',
        type: ProjectConstant.NOTIFICATION_LABELS.WARN,
      };
      this.notificationHandler.next(eventData);
      return true;
    }
    this.disableTaskSave = true;
    const taskEndDateWarning = false;

    if (taskEndDateWarning) {
      this.taskForm.patchValue({
        endDate: this.taskModalConfig.selectedTask.DUE_DATE,
      });
      const eventData = {
        message:
          "Task's end date should not be less than the deliverables' due date",
        type: ProjectConstant.NOTIFICATION_LABELS.WARN,
      };
      this.notificationHandler.next(eventData);
      this.disableTaskSave = false;
      return true;
    }
    if (this.taskForm.status === 'VALID') {
      if (this.taskForm.controls.taskName.value.trim() === '') {
        const eventData = {
          message: 'Please provide a valid task name',
          type: ProjectConstant.NOTIFICATION_LABELS.INFO,
        };
        this.notificationHandler.next(eventData);
        this.disableTaskSave = false;
        return true;
      } else {
        this.taskForm.controls.taskName.setValue(
          this.taskForm.controls.taskName.value.trim()
        );
        this.disableTaskSave = true;
        //this.updateTaskDetails(saveOption);
        return false;
      }
    }
    if (
      this.projectindexerDetails?.NPD_TASK_IS_BULK_UPDATE_INPROGRESS === 'true'
    ) {
      const eventData = {
        message: 'Task Bulk Operation in Progress, Kindly wait..',
        type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
      };
      this.disableTaskSave = true;
      //this.updateTaskDetails(saveOption);
      return false;
    } else {
      const eventData = {
        message: 'Kindly fill all mandatory fields',
        type: ProjectConstant.NOTIFICATION_LABELS.INFO,
      };
      this.notificationHandler.next(eventData);
      this.disableTaskSave = false;
      return true;
    }
  }
  updateTask(saveOption) {
    if (this.checkValidations(true)) {
      return;
    }
    this.taskForm.controls.taskName.setValue(
      this.taskForm.controls.taskName.value.trim()
    );
    this.disableTaskSave = true;
    this.updateTaskDetails(saveOption);
  }

  //check is task active-action COMPLETE
  //check if task is completed-action REVOKE
  getFinalStatusCheck(selectedTask): boolean {
    let finalTaskStatus = this.taskModalConfig.taskStatusList.find(
      (status) => status.STATUS_TYPE == StatusTypes.FINAL_APPROVED
    );
    //   console.log(finalTaskStatus)
    if (
      finalTaskStatus &&
      finalTaskStatus['MPM_Status-id']?.Id ==
      selectedTask?.R_PO_STATUS['MPM_Status-id']?.Id
    ) {
      return true;
    } else {
      return false;
    }
  }

  performTaskAction(action) {
    let statusType;
    let actionType;
    let notifyMessage;
    let dueDateWeek;
    let requestObj;
    let loggedInUserName: string =
      this.sharingService.getCurrentUserDisplayName();
    if (action === 'Complete') {
      statusType = 'COMPLETE';
      actionType = 'COMPLETE';
      notifyMessage = 'Completed';
      if (!this.taskForm.controls?.endDate?.value) {
        dueDateWeek = this.monsterUtilService.calculateTaskWeek(new Date());
      } else {
        dueDateWeek = this.taskForm.controls?.endDate?.value;
      }
      let dueDate;
      if (dueDateWeek) {
        [, dueDate] = this.getStartEndDate(dueDateWeek);
      }
      let taskObj = this.getCurrentNpdTaskFormValues();
      //let currentTaskFormValues = taskObj.customTaskObject;
      //let customTaskObj;//= this.npdTaskService.compareTwoProjectDetails(currentTaskFormValues, this.oldTaskCustomObject);
      /*   customTaskObj.ActualDueDate = dueDate;
            customTaskObj.TaskOperation = actionType */
      if (
        taskObj.customTaskObject &&
        taskObj.customTaskObject.ActualStartDate
      ) {
        taskObj.customTaskObject.ActualStartDate = this.getIsoFormatDateString(
          taskObj.customTaskObject.ActualStartDate
        );
      }
      if (taskObj.customTaskObject && taskObj.customTaskObject.PlannedDueDate) {
        taskObj.customTaskObject.PlannedDueDate = this.getIsoFormatDateString(
          taskObj.customTaskObject.PlannedDueDate
        );
      }
      requestObj = {
        afterTaskEditActions: {
          ActualDueDate: this.getIsoFormatDateString(dueDate),
          TaskOperation: actionType,
        },
      };
      if (this.checkValidations(false)) {
        return;
      }
      this.saveNpdTaskDetails(
        taskObj?.TaskObject,
        taskObj?.customTaskObject,
        null,
        'COMPLETE',
        true,
        requestObj,
        notifyMessage,
        statusType
      );
    } else {
      statusType = 'REVOKE';
      actionType = 'REVOKE';
      notifyMessage = 'Reverted back';
      //dueDateWeek = this.taskForm.controls?.OriginalEndDate?.value;
      dueDateWeek = this.taskForm.controls?.endDate?.value;
      // console.log(this.getCurrentNpdTaskFormValues());
      if (this.projectindexerDetails.PROJECT_STATUS_VALUE == 'Completed') {
        const dialogRef = this.dialog.open(ConfirmationModalComponent, {
          width: '40%',
          disableClose: true,
          data: {
            message: `REVOKING THIS TASK WILL MOVE THE COMPLETED PROJECT BACK INTO AN ACTIVE STATE - ARE YOU SURE YOU WANT TO PROCEED?
            IF YES PLEASE ADD THE COMMENT`,
            submitButton: 'Yes',
            cancelButton: 'No',
            //confirmationHeading: '',
            isCommentRequired: true,
            errorMessage: 'Please add Comment',
            hasConfirmationComment: true,
          },
        });
        dialogRef.afterClosed().subscribe((result) => {
          if (result && result.isTrue) {
            console.log(result);
            console.log(
              this.projectindexerDetails?.NPD_PROJECT_E2E_PM_ITEM_ID?.split(
                '.'
              )[1]
            );
            // let messagePrefix = "Task " + this.getCurrentNpdTaskFormValues().TaskObject.TaskName + ' has been revoked by user ' +loggedInUserName+ ' on a Completed Project '+this.projectindexerDetails?.PR_REQUEST_ID_string + "Therefore this project is now moved back to Active" + ' - ';
            let message = result.confirmationComment;
            let commentDetails: NewCommentModal = {
              requestId:
                this.projectindexerDetails?.PR_REQUEST_ITEM_ID?.split('.')[1],
              commentText: message,
              commentType: 'PROJECT',
              refCommentId: null,
              //   tagUser: {
              //     userId: [this.projectindexerDetails?.NPD_PROJECT_E2E_PM_ITEM_ID?.split(".")[1]]
              // }
            };
            // console.log(commentDetails);
            this.commentsService.createNewComment(commentDetails).subscribe();
            let dueDate;
            if (dueDateWeek) {
              [, dueDate] = this.getStartEndDate(dueDateWeek);
            }

            requestObj = {
              afterTaskEditActions: {
                Task: {
                  Id: this.taskModalConfig.taskId,
                  ItemId: this.taskModalConfig.selectedTask['Task-id'].ItemId,
                },
                ActualDueDate: this.getIsoFormatDateString(dueDate),
                TaskOperation: actionType,
              },
            };
            this.actionTaskOperation(requestObj, notifyMessage, statusType);
          }
        });
      } else {
        let dueDate;
        if (dueDateWeek) {
          [, dueDate] = this.getStartEndDate(dueDateWeek);
        }

        requestObj = {
          afterTaskEditActions: {
            Task: {
              Id: this.taskModalConfig.taskId,
              ItemId: this.taskModalConfig.selectedTask['Task-id'].ItemId,
            },
            ActualDueDate: this.getIsoFormatDateString(dueDate),
            TaskOperation: actionType,
          },
        };
        this.actionTaskOperation(requestObj, notifyMessage, statusType);
      }
    }
  }

  actionTaskOperation(requestObj, notifyMessage, statusType) {
    this.loaderService.show();
    this.npdTaskService.updateTask(requestObj).subscribe(
      (response) => {
        this.loaderService.hide();
        this.disableTaskSave = false;
        if (response && response.data) {
          this.taskStatusChange(notifyMessage, statusType);
          //action === 'Complete' && this.updateRequestData();
        } else {
          const eventData = {
            message: 'Something went wrong on while updating task',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
        }
      },
      (error) => {
        this.loaderService.hide();
        const eventData = {
          message: 'Something went wrong while updating task',
          type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
        };
        this.notificationHandler.next(eventData);
      }
    );
  }

  getFinalCustomTaskObject(customTaskObject, TaskOperation) {
    let taskNo = this.getTaskNumberFromTask();
    let taskFormValue = this.taskForm.getRawValue();
    if (
      (taskNo == '04' || taskNo == '05') &&
      taskFormValue?.finalClassification != 'NA' &&
      taskFormValue?.finalClassification != ''
    ) {
      customTaskObject.FinalClassification = taskFormValue?.finalClassification;
      customTaskObject.RegistrationClassificationAvailable = taskFormValue?.registrationClassificationAvailable;
      customTaskObject.RegistrationRequirement = taskFormValue?.registrationRequirement;
    } else if (taskNo == '07') {
      customTaskObject.ProcessAuthorityApprovalRequired =
        taskFormValue?.processAuthorityApproval;
    } else if (taskNo == 13) {
      customTaskObject.TransactionSchemeAnalysisRequired =
        taskFormValue?.transactionSchemeAnalysisRequired;
      customTaskObject.NonProprietaryMaterialCostingRequired =
        taskFormValue?.nonProprietaryMaterialCostingRequired;
    } else if (taskNo == 10) {
      customTaskObject.RequiredComplete = taskFormValue?.requiredComplete;
    } else if (taskNo == 36) {
      customTaskObject.AllowProdScheduleLessThanEightWeeks =
        taskFormValue?.productionSchedule;
    }
    return customTaskObject;
  }

  saveNpdTaskDetails(
    TaskObject,
    customTaskObject,
    saveOption,
    TaskOperation,
    isActionComplete?,
    requestObj?,
    notifyMessage?,
    statusType?
  ) {
    // conditions for task 4 and task 5
    if (this.taskModalConfig.selectedTask) {
      TaskObject = this.projectUtilService.compareTwoProjectDetails(
        TaskObject,
        this.oldTaskObject
      );
      customTaskObject = this.npdTaskService.compareTwoProjectDetails(
        customTaskObject,
        this.oldTaskCustomObject
      );
      if (customTaskObject) {
        if (
          this.taskForm?.getRawValue()?.startDate ==
          this.oldTaskCustomObject?.ActualStartDate
        ) {
          delete customTaskObject['ActualStartDate'];
        }
        if (
          this.taskForm?.getRawValue()?.endDate ==
          this.oldTaskCustomObject?.ActualDueDate
        ) {
          delete customTaskObject['ActualDueDate'];
        }
        if (
          this.taskForm?.getRawValue()?.OriginalEndDate ==
          this.oldTaskCustomObject?.PlannedDueDate
        ) {
          delete customTaskObject['PlannedDueDate'];
        }
      }
      if (TaskObject) {
        if (
          this.taskForm?.getRawValue()?.OriginalStartDate ==
          this.oldTaskObject?.StartDate
        ) {
          delete TaskObject['StartDate'];
        }
        if (
          this.taskForm?.getRawValue()?.OriginalEndDate ==
          this.oldTaskObject?.DueDate
        ) {
          delete TaskObject['DueDate'];
        }
      }
      if (requestObj) {
        customTaskObject.ActualDueDate =
          requestObj.afterTaskEditActions?.ActualDueDate;
      }
      customTaskObject = this.getFinalCustomTaskObject(
        customTaskObject,
        TaskOperation
      );
      this.loaderService.show();

      forkJoin([
        this.updateCustomDetails(customTaskObject, saveOption, TaskOperation),
        this.updateMpmTaskDetails(TaskObject, saveOption),
      ]).subscribe(
        (response) => {
          if (isActionComplete) {
            //this.updateRequestData()
            //this.actionTaskOperation(requestObj,notifyMessage,statusType);
            this.taskStatusChange(notifyMessage, statusType);
          } else {
            this.loaderService.hide();
          }
        },
        (error) => {
          this.loaderService.hide();
          this.notificationService.error(
            'Something went wrong while saving details.'
          );
        }
      );
    }
  }

  updateCustomDetails(
    customTaskObject,
    saveOption,
    taskOperation,
    isNotify?
  ): Observable<any> {
    return new Observable((observer) => {
      if (Object.keys(customTaskObject).length > 1) {
        customTaskObject.TaskOperation = taskOperation; //'EDIT
        const requestObj = {
          afterTaskEditActions: customTaskObject,
        };
        this.npdTaskService.updateTask(requestObj).subscribe(
          (response) => {
            this.disableTaskSave = false;
            if (response && response.data) {
              if (isNotify) {
                const eventData = {
                  message: 'Task updated successfully',
                  type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS,
                };
                this.notificationHandler.next(eventData);
                this.saveCallBackHandler.next({
                  taskId: this.taskModalConfig?.selectedTask?.['Task-id'].Id,
                  projectId: this.taskModalConfig.projectId,
                  isTemplate: this.taskModalConfig?.isTemplate,
                  saveOption: saveOption,
                });
              }
            } else {
              if (isNotify) {
                const eventData = {
                  message: 'Something went wrong on while updating task',
                  type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
                };
                this.notificationHandler.next(eventData);
              }
            }
            observer.next([]);
            observer.complete();
          },
          (error) => {
            if (isNotify) {
              const eventData = {
                message: 'Something went wrong while updating task',
                type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
              };
              this.notificationHandler.next(eventData);
            }
            observer.error();
          }
        );
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  updateMpmTaskDetails(TaskObject, saveOption, isNotify?): Observable<any> {
    return new Observable((observer) => {
      if (
        Object.keys(TaskObject).length > 1 ||
        JSON.stringify(this.deliverableReviewers) !==
        JSON.stringify(this.savedReviewers)
      ) {
        TaskObject.BulkOperation = false;
        const requestObj = {
          TaskObject: TaskObject,
          DeliverableReviewers: {
            DeliverableReviewer: this.reviewersObj,
          },
        };
        this.taskService.updateTask(requestObj).subscribe(
          (response) => {
            this.disableTaskSave = false;
            if (
              response &&
              response.APIResponse &&
              response.APIResponse.statusCode === '200'
            ) {
              if (isNotify) {
                const eventData = {
                  message: 'Task updated successfully',
                  type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS,
                };
                this.singleCommentcomponent.createSingleComment();
                this.notificationHandler.next(eventData);
                this.saveCallBackHandler.next({
                  taskId: response.APIResponse.data.Task['Task-id'].Id,
                  projectId: this.taskModalConfig.projectId,
                  isTemplate: this.taskModalConfig.isTemplate,
                  saveOption: saveOption,
                });
              }
            } else if (
              response &&
              response.APIResponse &&
              response.APIResponse.statusCode === '500' &&
              response.APIResponse.error &&
              response.APIResponse.error.errorCode &&
              response.APIResponse.error.errorMessage
            ) {
              if (isNotify) {
                const eventData = {
                  message: response.APIResponse.error.errorMessage,
                  type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
                };
                this.notificationHandler.next(eventData);
              }
            } else {
              if (isNotify) {
                const eventData = {
                  message: 'Something went wrong on while updating task',
                  type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
                };
                this.notificationHandler.next(eventData);
              }
            }
            observer.next([]);
            observer.complete();
          },
          (error) => {
            if (isNotify) {
              const eventData = {
                message: 'Something went wrong while updating task',
                type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
              };
              this.notificationHandler.next(eventData);
            }
            observer.error();
          }
        );
      } else {
        //this.loaderService.hide();
        observer.next([]);
        observer.complete();
        this.disableTaskSave = false;
      }
    });
  }

  /*  updateRequestData() : Observable<any> {
        return new Observable(observer => {
            if(this.checkTaskDetails()) {
                this.monsterUtilService.finalBulkUpdateFormData(() => {
                    //this.loaderService.hide();
                    observer.next([]);
                    observer.complete();
                    },() =>{
                    observer.error();
                    this.notificationService.error('Something went wrong while updating classification details.');
                });
            } else {
                observer.next([]);
                observer.complete();
            }
        });

    } */

  taskStatusChange(notifyMessage, statusType) {
    this.loaderService.show();
    this.npdTaskService
      .taskStatusChange(this.taskConfig.taskId, statusType)
      .subscribe(
        (response) => {
          this.loaderService.hide();
          //this.notificationService.success('Task has been ' + statusType);
          this.notificationService.success('Task has been ' + notifyMessage);
          this.saveCallBackHandler.next({
            taskId: this.taskConfig.taskId,
            projectId: this.taskModalConfig.projectId,
            isTemplate: this.taskModalConfig.isTemplate,
            saveOption: this.saveOptions.find(
              (option) => option.value == 'SAVE'
            )?.value,
          });
        },
        (actionError) => {
          this.loaderService.hide();
          this.notificationService.error(
            'Something went wrong while performing the Task action'
          );
        }
      );
  }

  getIsoFormatDateString(dateObject) {
    const newDate = new Date(dateObject);
    const year = newDate.getFullYear();
    const actualMonth = newDate.getMonth() + 1;
    const month =
      actualMonth.toString().length <= 1 ? '0' + actualMonth : actualMonth;
    const date =
      newDate.getDate().toString().length <= 1
        ? '0' + newDate.getDate()
        : newDate.getDate();
    return year + '-' + month + '-' + date + 'T00:00:00.000Z';
  }

  saveNpdTask(TaskObject, customTaskObject, saveOption) {
    this.loaderService.show();
    if (this.taskModalConfig.selectedTask) {
      TaskObject = this.projectUtilService.compareTwoProjectDetails(
        TaskObject,
        this.oldTaskObject
      );
      if (TaskObject) {
        if (
          this.taskForm?.getRawValue()?.OriginalStartDate ==
          this.oldTaskObject?.StartDate
        ) {
          delete TaskObject['StartDate'];
        }
        if (
          this.taskForm?.getRawValue()?.OriginalEndDate ==
          this.oldTaskObject?.DueDate
        ) {
          delete TaskObject['DueDate'];
        }
      }
      this.updateMpmTaskDetails(TaskObject, saveOption).subscribe(
        (response) => {
          customTaskObject = this.npdTaskService.compareTwoProjectDetails(
            customTaskObject,
            this.oldTaskCustomObject
          );
          if (customTaskObject) {
            if (
              this.taskForm?.getRawValue()?.startDate ==
              this.oldTaskCustomObject?.ActualStartDate
            ) {
              delete customTaskObject['ActualStartDate'];
            } else {
              // customTaskObject['ActualStartDate'] = customTaskObject['ActualStartDate'].getFullYear()+ "-" + customTaskObject['ActualStartDate'].getMonth() + "-" +customTaskObject['ActualStartDate'].getDate()+"T00:00:00.0";
              if (customTaskObject['ActualStartDate']) {
                customTaskObject['ActualStartDate'] =
                  this.getIsoFormatDateString(
                    customTaskObject['ActualStartDate']
                  );
              } else {
                customTaskObject['ActualStartDate'] =
                  customTaskObject['ActualStartDate'].toString();
              }
            }
            if (
              this.taskForm?.getRawValue()?.endDate ==
              this.oldTaskCustomObject?.ActualDueDate
            ) {
              delete customTaskObject['ActualDueDate'];
            } else {
              if (customTaskObject['ActualDueDate']) {
                customTaskObject['ActualDueDate'] = this.getIsoFormatDateString(
                  customTaskObject['ActualDueDate']
                );
              } else {
                customTaskObject['ActualDueDate'] =
                  customTaskObject['ActualDueDate'].toString();
              }
            }
            if (
              this.taskForm?.getRawValue()?.OriginalEndDate ==
              this.oldTaskCustomObject?.PlannedDueDate
            ) {
              delete customTaskObject['PlannedDueDate'];
            } else {
              customTaskObject['PlannedDueDate'] = this.getIsoFormatDateString(
                customTaskObject['PlannedDueDate']
              );
            }
          }
          // conditions for task 4 and task 5
          forkJoin([
            this.updateCustomDetails(customTaskObject, saveOption, 'EDIT'),
          ]).subscribe(
            (response) => {
              this.loaderService.hide();
              const eventData = {
                message: 'Task updated successfully',
                type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS,
              };
              this.singleCommentcomponent.createSingleComment();
              this.notificationHandler.next(eventData);
              this.saveCallBackHandler.next({
                taskId: this.taskModalConfig?.selectedTask?.['Task-id'].Id,
                projectId: this.taskModalConfig.projectId,
                isTemplate: this.taskModalConfig?.isTemplate,
                saveOption: saveOption,
              });
            },
            (error) => {
              this.loaderService.hide();
              this.notificationService.error(
                'Something went wrong while saving details.'
              );
            }
          );
        }
      );

      /*  this.monsterUtilService.finalBulkUpdateFormData(() => {
                this.loaderService.hide();
                    const eventData = { message: 'Task updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    this.notificationHandler.next(eventData);
                    this.saveCallBackHandler.next({
                        taskId: this.taskModalConfig?.selectedTask?.['Task-id'].Id, projectId: this.taskModalConfig.projectId,
                        isTemplate: this.taskModalConfig?.isTemplate, saveOption: saveOption
                    });
                },() =>{
                this.notificationService.error('Something went wrong while updating classification details.');
            });
            if (Object.keys(customTaskObject).length > 1 ) {
                customTaskObject.TaskOperation = 'EDIT';
                const requestObj = {
                    afterTaskEditActions : customTaskObject,
                };
                this.npdTaskService.updateTask(requestObj).subscribe(response => {
                    this.loaderService.hide();
                    this.disableTaskSave = false;
                    if (response && response.data) {
                        const eventData = { message: 'Task updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                        this.notificationHandler.next(eventData);
                        this.saveCallBackHandler.next({
                            taskId: this.taskModalConfig?.selectedTask?.['Task-id'].Id, projectId: this.taskModalConfig.projectId,
                            isTemplate: this.taskModalConfig?.isTemplate, saveOption: saveOption
                        });
                    } else {
                        const eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                        this.notificationHandler.next(eventData);
                    }
                }, error => {
                    const eventData = { message: 'Something went wrong while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                });
            }
            if (Object.keys(TaskObject).length > 1 || (JSON.stringify(this.deliverableReviewers) !== JSON.stringify(this.savedReviewers))) {
                TaskObject.BulkOperation = false;
                const requestObj = {
                    TaskObject: TaskObject,
                    DeliverableReviewers: {
                        DeliverableReviewer: this.reviewersObj
                    }
                };
                this.taskService.updateTask(requestObj).subscribe(response => {
                    this.loaderService.hide();
                    this.disableTaskSave = false;
                    if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
                        const eventData = { message: 'Task updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                        this.notificationHandler.next(eventData);
                        this.saveCallBackHandler.next({
                            taskId: response.APIResponse.data.Task['Task-id'].Id, projectId: this.taskModalConfig.projectId,
                            isTemplate: this.taskModalConfig.isTemplate, saveOption: saveOption
                        });
                    } else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                        && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                        const eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                        this.notificationHandler.next(eventData);
                    } else {
                        const eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                        this.notificationHandler.next(eventData);
                    }
                }, error => {
                    const eventData = { message: 'Something went wrong while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                });
            } else {
                this.loaderService.hide();
                this.disableTaskSave = false;
            } */
    } else {
      const requestObj = {
        TaskObject: TaskObject,
      };
      this.taskService.createTask(requestObj).subscribe(
        (response) => {
          this.loaderService.hide();
          this.disableTaskSave = false;
          if (
            response &&
            response.APIResponse &&
            response.APIResponse.statusCode === '200' &&
            response.APIResponse.data &&
            response.APIResponse.data.Task &&
            response.APIResponse.data.Task['Task-id'] &&
            response.APIResponse.data.Task['Task-id'].Id
          ) {
            const eventData = {
              message: 'Task saved successfully',
              type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS,
            };
            this.notificationHandler.next(eventData);
            this.disableTaskSave = false;
            this.saveCallBackHandler.next({
              taskId: response.APIResponse.data.Task['Task-id'].Id,
              projectId: this.taskModalConfig.projectId,
              isTemplate: this.taskModalConfig.isTemplate,
              saveOption: saveOption,
            });
          } else if (
            response &&
            response.APIResponse &&
            response.APIResponse.statusCode === '500' &&
            response.APIResponse.error &&
            response.APIResponse.error.errorCode &&
            response.APIResponse.error.errorMessage
          ) {
            const eventData = {
              message: response.APIResponse.error.errorMessage,
              type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
            };
            this.notificationHandler.next(eventData);
          } else {
            const eventData = {
              message: 'Something went wrong on while creating task',
              type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS,
            };
            this.notificationHandler.next(eventData);
          }
        },
        () => {
          this.loaderService.hide();
          const eventData = {
            message: 'Something went wrong on while updating task',
            type: ProjectConstant.NOTIFICATION_LABELS.ERROR,
          };
          this.notificationHandler.next(eventData);
          this.disableTaskSave = false;
        }
      );
    }
  }

  /*  onDraftManufactureLocationChange(draftManufacturingLocation) {
        if(draftManufacturingLocation?.isUserInput) {
            this.earlyManufacturingSites = [];
            // this.taskForm.patchValue({
            //     site : ''
            // });
            //     this.updateRequestRelationsInfo('earlyManufacturingSite','taskForm');

            let selectedDraftManufacturingLocation = this.draftManufacturingLocations.find(manufacturingLocation=> manufacturingLocation.ItemId == draftManufacturingLocation?.source?.value)
            let data = {property :'manufacturingLocation',value:selectedDraftManufacturingLocation?.regulatoryClassification,isDataLoad : false };
            this.getRegulatoryTimeLineLocations(data);
            this.loaderService.show();
            this.getEarlyManufacturingSites(draftManufacturingLocation?.source?.value).subscribe(response => {

            this.loaderService.hide();
            },(error) => {
            this.loaderService.hide();
            });
        }
    } */

  onDraftManufactureLocationChange(draftManufacturingLocation) {
    //if(draftManufacturingLocation?.isUserInput) {
    this.earlyManufacturingSites = [];
    this.taskForm.patchValue({
      finalManufacturingSite: '',
    });
    let selectedDraftManufacturingLocation =
      this.draftManufacturingLocations.find(
        (manufacturingLocation) =>
          manufacturingLocation.ItemId == draftManufacturingLocation
      );
    this.taskForm.patchValue({
      finalManufacturingLocationName:
        selectedDraftManufacturingLocation?.displayName,
    });
    let data = {
      property: 'manufacturingLocation',
      value: selectedDraftManufacturingLocation?.regulatoryClassification,
      isDataLoad: false,
    };
    // this.getRegulatoryTimeLineLocations(data);

    this.loaderService.show();
    if (draftManufacturingLocation && draftManufacturingLocation != 'NA') {
      this.finalDraftManufacturingLocation=draftManufacturingLocation;
      this.getClassificationChange(false);
      this.getEarlyManufacturingSites(draftManufacturingLocation).subscribe(
        (response) => {
          this.loaderService.hide();
        },
        (error) => {
          this.loaderService.hide();
        }
      );
    } else {
      this.loaderService.hide();
    }
    //}
  }

  onEarlyManufacturingSiteChange(earlyManufacturingSiteValue) {
    let selectedEarlyManufacturingSite = this.earlyManufacturingSites.find(
      (earlyManufacturingSite) =>
        earlyManufacturingSite.ItemId == earlyManufacturingSiteValue
    );
    this.taskForm.patchValue({
      finalManufacturingSiteName: selectedEarlyManufacturingSite?.displayName,
    });
  }

  // getRegulatoryTimeLineLocations(data) {
  //   this.regulatoryTimelineForLocations[data.property] = data.value;
  //   if (
  //     this.regulatoryTimelineForLocations.manufacturingLocation &&
  //     this.regulatoryTimelineForLocations.market
  //   ) {
  //     this.loaderService.show();
  //     this.monsterConfigApiService
  //       .fetchRegulatoryTimelineForLocations(
  //         this.regulatoryTimelineForLocations
  //       )
  //       .subscribe(
  //         (response) => {
  //           this.loaderService.hide();
  //           let registrationDossier = false;
  //           let preProdReg = false;
  //           let postProdReg = false;
  //           if (response && response?.['Regulatory_Timeline']) {
  //             let value = response['Regulatory_Timeline'];
  //             registrationDossier =
  //               value?.RegistrationPreparationDuration > 0 ? true : false;
  //             preProdReg =
  //               value?.PreProductionRegistartionDuration > 0 ? true : false;
  //             postProdReg = value?.PostProductionDuration > 0 ? true : false;
  //           } else {
  //             registrationDossier = null;
  //             preProdReg = null;
  //             postProdReg = null;
  //           }
  //           this.taskForm.patchValue({
  //             registrationDossier: registrationDossier,
  //             preProdReg: preProdReg,
  //             postProdReg: postProdReg,
  //           });
  //           //this.updateRequestInfoByValue('registrationDossier','taskForm',registrationDossier)
  //           //this.updateRequestInfoByValue('preProdReg','taskForm',preProdReg)
  //           //this.updateRequestInfoByValue('postProdReg','taskForm',postProdReg)
  //           if (
  //             this.taskForm?.controls?.registrationDossier &&
  //             this.taskForm?.controls?.preProdReg &&
  //             this.taskForm?.controls?.postProdReg &&
  //             !this.validateClassificationRadioFields()
  //           ) {
  //             //(this.checkClassificationRadioFields()
  //             this.getClassificationChange();
  //           }
  //         },
  //         (error) => {
  //           this.loaderService.hide();
  //           console.log('Error while fetching Regulatory Timelines.');
  //         }
  //       );
  //   }
  // }

  validateClassificationRadioFields() {
    if (
      this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.newFg?.value,
        null
      ) == null ||
      this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.newRnDFormula?.value,
        null
      ) == null ||
      this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.newHBCFormula?.value,
        null
      ) == null ||
      this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.newPrimaryPackaging?.value,
        null
      ) == null ||
      this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.secondaryPackaging?.value,
        null
      ) == null
      // this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
      //   this.taskForm.controls?.leadFormula?.value,
      //   null
      // ) == null
      // this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
      //   this.taskForm.controls?.postProdReg?.value,
      //   null
      // ) == null ||
      // this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
      //   this.taskForm.controls?.preProdReg?.value,
      //   null
      // ) == null ||
      // this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
      //   this.taskForm.controls?.registrationDossier?.value,
      //   null
      // ) == null
    ) {
      return false;
    } else {
      return true;
    }
  }

  checkClassificationRadioFields() {
    if (
      this.npdTaskService.checkClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.newFg?.value,
        null
      ) == null ||
      this.npdTaskService.checkClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.newRnDFormula?.value,
        null
      ) == null ||
      this.npdTaskService.checkClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.newHBCFormula?.value,
        null
      ) == null ||
      this.npdTaskService.checkClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.newPrimaryPackaging?.value,
        null
      ) == null ||
      this.npdTaskService.checkClassificationIndexerBooleanValueIsNullOrEmpty(
        this.taskForm.controls?.secondaryPackaging?.value,
        null
      ) == null
    ) {
      return false;
    } else {
      return true;
    }
  }

  /*       updateRequestInfoByValue(formControl,formName,propertyValue) {
        let property;
        let value;
        let itemId;

        if(formName === 'taskForm') {
          property = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
          value = propertyValue;
          itemId = this.request?.requestItemId;
        }
        this.monsterUtilService.setFormProperty(itemId, property, value);
      } */

  getEarlyManufacturingSites(draftManufacturingLocation): Observable<any> {
    let draftManufacturingRequestParams = {
      'Draft_Manufacturing_Location-id': {
        Id:
          draftManufacturingLocation &&
          draftManufacturingLocation.split('.')[1],
        ItemId: draftManufacturingLocation,
      },
    };
    return new Observable((observer) => {
      this.monsterConfigApiService
        .fetchEarlyManufacturingSitesByDraftManufacturingLocation(
          draftManufacturingRequestParams
        )
        .subscribe(
          (response) => {
            this.earlyManufacturingSites =
              this.monsterConfigApiService.transformResponse(
                response,
                'Early_Manufacturing_Site-id',
                true
              );
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }

  setProjectClassification(value) {
    let selectedProjectType = this.projectTypes.find(
      (type) => type.ItemId == value
    );
    this.taskForm.patchValue({
      projectTypeName: selectedProjectType?.displayName,
    });
    this.projectClassificationParams.projectType = value?.split('.')[1];
    // this.getClassificationChange(true);
    this.disableLeadFormulaField();
  }

  getBusinessUnits() {
    this.monsterConfigApiService.getAllBusinessUnits().subscribe((response) => {
      // console.log("Bussiness UNIT", response);
      this.bussinessUnits = response;
    }, (error) => {
    });
  }
  getAllMarkets() {
    this.monsterConfigApiService.getAllMarkets().subscribe(res => {
      // console.log("allMarkets", res);
      this.allMarkets = res;
    })
  }
  getClassificationChange(isThrowableError?) {
    // let total = this.onChangeTotal();
    // this.getProjectClassification({
    //   property: 'projectClassificationScore',
    //   value: total.formulaResult,
    // });


    /**NEW Implementation */
    // console.log(this.taskForm);
    let formValue = this.taskForm.value;
    let regionId = 0;
    let countryOfSaleId = 0;
    let countryOfSaleRC="";
    let countryOfManufactureRc="";

    this.bussinessUnits.forEach(x => {
      if (x['DISPLAY_NAME'] == this.bussinessUnit) {
        regionId = x['R_PO_REGION']['Region-id']['Id'];

      }
    })
    this.allMarkets.forEach(x => {
      if (x['Markets-id']['Id'] == this.leadMarket) {
        countryOfSaleId = x['Markets-id']['Id'];
        countryOfSaleRC = x['RegulatoryClassification'];
      }
    })
    this.allDraftManufacturingLocations.forEach(location=>{
      if(location['Identity']['Id']==this.taskForm.controls.finalManufacturingLocation.value?.split('.')[1]){
        countryOfManufactureRc=location['R_PO_MARKET$Properties']['RegulatoryClassification'];
      }
    })

    let param = {
      regionId: regionId,
      countryOfSaleId: countryOfSaleId,
      countryOfManufactureId: this.taskForm.controls.finalManufacturingLocation.value?.split('.')[1],
      projectTypeId: this.taskForm.controls.projectType.value?.split('.')[1],
      newFG: formValue.newFg,
      newRD: formValue.newRnDFormula,
      newHBC: formValue.newHBCFormula,
      primaryPackaging: formValue.newPrimaryPackaging,
      secondaryPackaging: formValue.secondaryPackaging,
      countryOfSaleRC: countryOfSaleRC,
      countryOfManufactureRC: countryOfManufactureRc,
      leadFormula: formValue.leadFormula

    }
    // console.log(param)
    // if (param.newFG != null && param.newRD != null && param.newHBC != null && param.primaryPackaging != null && param.secondaryPackaging != null) {
    //   this.generateClassificationNumber(param);
    // }
    this.generateClassificationNumber(param, isThrowableError);
  }

  // onChangeTotal() {
  //   let projectClasification = this.taskForm.getRawValue();
  //   // console.log('this.taskForm.getRawValue();', this.taskForm.getRawValue());
  //   const newFg = projectClasification.newFg === true ? 1000 : 0; //this.taskForm.value.newFg === true ? 1000 : 0;
  //   const newRnDFormula =
  //     projectClasification.newRnDFormula === true ? 10000 : 0;
  //   const newHBCFormula =
  //     projectClasification.newHBCFormula === true ? 100000 : 0;
  //   const newPrimaryPackaging =
  //     projectClasification.newPrimaryPackaging === true ? 1000000 : 0;
  //   const secondaryPackaging =
  //     projectClasification.secondaryPackaging === true ? 10000000 : 0;
  //   let registrationDossier;
  //   let preProdReg;
  //   let postProdReg;

  //   if (this.isTask4Available == true && this.isTask5Available == true) {
  //     if (
  //       this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
  //         projectClasification.postProdReg,
  //         null
  //       ) == null ||
  //       this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
  //         projectClasification.preProdReg,
  //         null
  //       ) == null ||
  //       this.npdTaskService.validateClassificationIndexerBooleanValueIsNullOrEmpty(
  //         projectClasification.registrationDossier,
  //         null
  //       ) == null
  //     ) {
  //       return { formulaResult: -1 };
  //     }
  //     registrationDossier =
  //       projectClasification.registrationDossier === true ? 100000000 : 0;
  //     preProdReg = projectClasification.preProdReg === true ? 1000000000 : 0;
  //     postProdReg = projectClasification.postProdReg === true ? 10000000000 : 0;
  //   }
  //   if (!(this.isTask4Available == true && this.isTask5Available == true)) {
  //     let projectClassificationConfig =
  //       this.taskObjFinalClassification?.ProjectFields;
  //     registrationDossier =
  //       projectClassificationConfig?.NewRegistrationDossier === true
  //         ? 100000000
  //         : 0;
  //     preProdReg =
  //       projectClassificationConfig?.NewPreProduction === true ? 1000000000 : 0;
  //     postProdReg =
  //       projectClassificationConfig?.NewPostProduction === true
  //         ? 10000000000
  //         : 0;
  //   }

  //   return {
  //     formulaResult:
  //       newFg +
  //       newRnDFormula +
  //       newHBCFormula +
  //       newPrimaryPackaging +
  //       secondaryPackaging +
  //       registrationDossier +
  //       preProdReg +
  //       postProdReg,
  //   };
  // }

  // getProjectClassification(data) {
  //   this.projectClassificationParams[data.property] = +data.value;
  //   this.getProjectClassificationNoDescription(
  //     this.projectClassificationParams
  //   ).subscribe(
  //     (response) => {
  //       response;
  //     },
  //     () => {
  //       console.log('Error while loading the form configs.');
  //     }
  //   );
  // }

  // getProjectClassificationNoDescription(parameters): Observable<any> {
  //   return new Observable((observer) => {
  //     this.monsterConfigApiService
  //       .fetchProjectClassificationNoDescription(parameters)
  //       .subscribe(
  //         (response) => {
  //           if (response.length > 0) {
  //             this.taskForm.patchValue({
  //               finalClassification: response[0].ProjectClassificationNumber,
  //               finalClassificationDesc:
  //                 response[0].ProjectClassificationDescription,
  //             });
  //           } else {
  //             this.taskForm.patchValue({
  //               finalClassification: '',
  //               finalClassificationDesc: '',
  //             });
  //           }
  //           observer.next(true);
  //           observer.complete();
  //         },
  //         (error) => {
  //           observer.error(error);
  //         }
  //       );
  //   });
  // }
  /*
    updateRequestInfo(formControl,formName,propertyValue?) {
        let property;
        let value;
        let itemId;

        if(formName === 'taskForm') {
          property = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
          value = propertyValue? propertyValue : this.taskForm.controls[formControl] && this.taskForm.controls[formControl].value;
          itemId = this.request && this.request.requestItemId;
        }
        this.monsterUtilService.setFormProperty(itemId, property, value);
    } */

  /*     updateRequestRelationsInfo(formControl,formName,objValue?) {
        let relationName;
        let objectValue;
        let itemId;

        if(formName === 'taskForm') {
          relationName = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
          objectValue = objValue? objValue : this.taskForm.controls[formControl] && this.taskForm.controls[formControl].value;
          itemId = this.request && this.request.requestItemId;
        }
        this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
    }
 */
  // Forming search field for mpm-indexer input
  getSearchField(
    type,
    field_id,
    operator_id,
    operator_name,
    value?,
    field_operator?
  ) {
    let searchField = {
      type: type,
      field_id: field_id,
      relational_operator_id: operator_id,
      relational_operator_name: operator_name,
      value: value,
      relational_operator: field_operator,
    };
    return searchField;
  }

  /*     getTaskRequestDetailsById(): Observable<any> {
        let taskId = this.taskModalConfig.selectedTask?.['Task-id']?.Id
        let searchConditionList = [
            this.getSearchField("string", "CONTENT_TYPE", "MPM.OPERATOR.CHAR.IS", "is", "MPM_TASK"),
            this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", taskId, "AND"),
        ];

        const parameters: SearchRequest = {
            search_config_id: null,
            keyword: "",
            search_condition_list: {
                search_condition: searchConditionList
            },
            facet_condition_list: {
                facet_condition: []
            },
            sorting_list: {
                sort: []
            },
            cursor: {
                page_index: 0,
                page_size: 1
            }
        };
        return new Observable(observer => {
            this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
                if (response.data.length > 0) {
                    this.requestId = response.data[0]?.['PR_REQUEST_ITEM_ID']?.split('.')[1];//'005056AACEB9A1EBA6DD27D3623FE821.65623'; //response.data[0];//REFERENCE_LINK_ID
                                                                                           //response.data[0]['PR_REQUEST_TYPE'];

                    this.taskIndexerDetails = response.data[0];
                    if(this.requestId && this.isLoadRequestDetails()) {
                        this.requestService.fetchRequestDetails(this.requestId).subscribe(request=> {
                            if(this.getTaskNumberFromTask() == 5) {
                                this.regulatoryTimelineForLocations.market = this.projectindexerDetails?.NPD_PROJECT_LEAD_MARKET;
                                if(this.projectindexerDetails?.NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID !='NA') {
                                    this.getEarlyManufacturingSites(this.request.draftManufacturingLocation).subscribe();
                                }
                            } else {
                                observer.next([]);
                                observer.complete();
                            }
                        },error=> {
                            observer.error();
                        })

                        this.requestService.fetchRequestDetails(this.requestId).subscribe(request=> {
                            this.requestDetails = request;
                            this.requestDetails.requestItemId = request;
                            this.mapRequestDetails(response.data[0]);
                            if(this.getTaskNumberFromTask() == 5) {
                                this.regulatoryTimelineForLocations.market = this.projectindexerDetails?.NPD_PROJECT_LEAD_MARKET;
                                if(this.projectindexerDetails?.NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID !='NA') {
                                    this.getEarlyManufacturingSites(this.request.draftManufacturingLocation).subscribe();
                                }

                                this.retrieveMarketScopeContents().subscribe(response=> {
                                    observer.next([]);
                                    observer.complete();
                                },(error)=> {
                                    observer.error();
                                });
                            } else {
                                observer.next([]);
                                observer.complete();
                            }
                        },error=> {
                            observer.error();
                        })
                    }
                    else {
                        observer.next([]);
                        observer.complete();
                    }
                } else {
                    observer.next([]);
                    observer.complete();
                }
            }, () => {
                observer.error();
            });
        });
    } */

  isLoadRequestDetails() {
    let taskNo = this.getTaskNumberFromTask();
    return taskNo == '04' || taskNo == '05';
  }

  /*     retrieveMarketScopeContents() : Observable<any> {
        return new Observable(observer => {
            this.entittyService.getEntityItemDetails(this.request.requestItemId,
                EntityListConstant[this.request.requestMarketScopeRelation]).subscribe((marketScopeResponse) => {
                  if (marketScopeResponse && marketScopeResponse.items) {
                      for (let i = 0; i < marketScopeResponse.items.length; i++) {
                          if(marketScopeResponse.items[i].Properties.LEAD_MARKET) {
                              this.regulatoryTimelineForLocations.market =
                              marketScopeResponse.items[i]?.['R_PO_MARKET']?.Properties?.RegulatoryClassification;
                              break;
                          }
                      }
                  }
                  observer.next([]);
                  observer.complete();
                },error=> {
                    observer.error();
                })
        });
    }
 */

  mapRequestDetails(requestIndexer) {
    this.request = {
      /*  newFg: this.requestDetails?.Properties?.NewFGSAP,
            newRnDFormula:this.requestDetails?.Properties?.NewRDFormula,
            newHBCFormula:this.requestDetails?.Properties?.NewHBCFormula,
            newPrimaryPackaging:this.requestDetails?.Properties?.NewPrimaryPackaging,
            secondaryPackaging:this.requestDetails?.Properties?.IsSecondaryPackaging,
            registrationDossier:this.requestDetails?.Properties?.RegistrationDossier,
            preProdReg:this.requestDetails?.Properties?.PreProductionRegistartion,
            postProdReg:this.requestDetails?.Properties?.PostProductionRegistartion,
            draftClassification: this.requestDetails?.R_PO_PROJECT_CLASSIFICATION$Properties?.ProjectClassificationNumber,
            projectType: this.requestDetails?.R_PO_PROJECT_TYPE$Identity?.Id,
            draftManufacturingLocation : this.requestDetails?.R_PO_DRAFT_MANUFACTURING_LOCATION$Identity?.ItemId,
            earlyManufacturingSite :  this.requestDetails?.R_PO_EARLY_MANUFACTURING_SITE$Identity?.ItemId,
            requestMarketScopeRelation: this.getMarketRelation(requestIndexer?.PR_REQUEST_TYPE),
            requestItemId: requestIndexer?.PR_REQUEST_ITEM_ID,
            requestType: requestIndexer?.PR_REQUEST_TYPE , */
    };
    //this.projectClassificationParams.projectType = this.requestDetails?.R_PO_PROJECT_TYPE$Identity?.Id;
  }

  getMarketRelation(requestType) {
    if (
      requestType == BusinessConstants.TECHNICAL_REQUEST ||
      requestType == BusinessConstants.TECHNICAL_REQUEST2
    ) {
      return 'TECHNICAL_REQUEST_MARKET_SCOPE_RELATION';
    } else if (
      requestType == BusinessConstants.COMMERCIAL_REQUEST ||
      requestType == BusinessConstants.COMMERCIAL_REQUEST2
    ) {
      return 'COMMERCIAL_REQUEST_MARKET_SCOPE_RELATION';
    } else if (
      requestType == BusinessConstants.OPERATIONAL_REQUEST ||
      requestType == BusinessConstants.OPERATIONAL_REQUEST2
    ) {
      return 'OPERATIONS_REQUEST_MARKET_SCOPE_RELATION';
    }
    return '';
  }

  /*     validateClassification() {
        if(this.taskNumber == "4") {//&& (this.taskForm.controls.projectType.dirty || this.taskForm.controls.newFg.dirty || this.taskForm.controls.newRnDFormula.dirty|| this.taskForm.controls.newHBCFormula.dirty || this.taskForm.controls.newPrimaryPackaging.dirty|| this.taskForm.controls.secondaryPackaging.dirty)
                if(this.npdTaskService.checkValueIsNullOrEmpty(this.taskForm.controls.finalClassification.value,null)) {
                    return true;
                } else {
                    this.notificationService.error('Final Classification is mandatory.Please Check if the configuration is available.')
                    return false;
                }
        }
        if(this.taskNumber == "5") {//&& this.taskForm.controls.finalManufacturingLocation.dirty
            if(this.taskForm.controls?.registrationDossier &&
                this.taskForm.controls?.preProdReg &&
                this.taskForm.controls?.postProdReg ) {
                  if(this.taskForm.controls.registrationDossier.value == null &&
                    this.taskForm.controls.preProdReg.value == null
                    && this.taskForm.controls.postProdReg.value == null  ) {
                      this.notificationService.error('Please check if the selected Country of Sales & Country of origin values are configured.');
                      return false;
                  } else {
                    return true;
                  }
            } else {
                //this.notificationService.error('Please check if the selected Country of Sales & Country of origin values are configured.');
                return true;
            }
        }
        return true;
    } */

  checkFinalClassification(isSave) {
    if (this.npdTaskService.checkValueIsNullOrEmpty(this.taskForm.controls.finalClassification.value, null) &&
      this.taskForm.controls.finalClassification.value != 'NA'
    ) {
      if (isSave || this.taskForm.controls?.registrationClassificationAvailable?.value == 'true' || this.taskForm.controls?.registrationClassificationAvailable?.value == true) {
        return true
      } else {
        this.notificationService.error('Registration Classification not available cannot complete the task');
        return false;
      }
    } else {
      this.notificationService.error(
        'Final Classification is mandatory.Please Check if the configuration is available.'
      );
      return false;
    }
  }

  // checkMarketLocationConfig() {
  //   if (
  //     this.taskForm.controls?.registrationDossier &&
  //     this.taskForm.controls?.preProdReg &&
  //     this.taskForm.controls?.postProdReg
  //   ) {
  //     if (
  //       this.taskForm.controls.registrationDossier.value == null &&
  //       this.taskForm.controls.preProdReg.value == null &&
  //       this.taskForm.controls.postProdReg.value == null
  //     ) {
  //       this.notificationService.error(
  //         'Please check if the selected Country of Sales & Country of origin values are configured.'
  //       );
  //       return false;
  //     } else {
  //       return true;
  //     }
  //   } else {
  //     //this.notificationService.error('Please check if the selected Country of Sales & Country of origin values are configured.');
  //     return true;
  //   }
  // }

  validateManufacturingLocationAndSite() {
    if (
      this.npdTaskService.checkDataValueIsNullOrEmpty(
        this.taskForm.controls?.finalManufacturingLocation?.value,
        null
      ) == null ||
      this.npdTaskService.checkDataValueIsNullOrEmpty(
        this.taskForm.controls?.finalManufacturingSite?.value,
        null
      ) == null
    ) {
      return false;
    } else {
      return true;
    }
  }

  validateClassification(isSave) {
    if (this.taskNumber == '04') {
      if (!isSave && (!this.validateClassificationRadioFields() || !this.npdTaskService.checkDataValueIsNullOrEmpty(this.taskForm.controls?.projectType?.value, null))) {
        this.notificationService.error('Please Fill Mandatory Classification fields.');
        return false;
      }
      if (
        isSave &&
        (this.taskForm.controls.projectType.dirty ||
          this.taskForm.controls.newFg.dirty ||
          this.taskForm.controls.newRnDFormula.dirty ||
          this.taskForm.controls.newHBCFormula.dirty ||
          this.taskForm.controls.newPrimaryPackaging.dirty ||
          this.taskForm.controls.secondaryPackaging.dirty)
      ) {
        return this.checkFinalClassification(isSave); //&& this.checkMarketLocationConfig();
      } else if (!isSave) {
        return (
          this.checkFinalClassification(isSave) && this.isSiteAvailable()
        );
        // console.log('completed');
        // return false;
      }
    }
    if (this.taskNumber == '05') {
      //&& this.taskForm.controls.finalManufacturingLocation.dirty
      if (!isSave && !this.validateManufacturingLocationAndSite()) {
        this.notificationService.error(
          'Please Fill Mandatory Classification fields.'
        );
        return false;
      }
      if (!isSave && !(this.taskForm.controls?.registrationClassificationAvailable?.value == 'true' || this.taskForm.controls?.registrationClassificationAvailable?.value == true)) {
        this.notificationService.error('Registration Classification not available cannot complete the task');
        return false;
      }
      // if (isSave && this.taskForm.controls.finalManufacturingLocation.dirty) {
      //   //return this.checkMarketLocationConfig();
      // } else if (!isSave) {
      //   return this.checkMarketLocationConfig();
      // }
    }
    return true;
  }
  isSiteAvailable(): boolean {
    let projectClasification = this.taskForm.getRawValue();
    if (
      projectClasification.finalManufacturingSite === '' ||
      projectClasification.finalManufacturingSite === 'NA' ||
      projectClasification.finalManufacturingSiteName === '' ||
      projectClasification.finalManufacturingSiteName === 'NA'
    ) {
      this.notificationService.error('Please provide the Site');
      return false;
    }
    return true;
  }
  getProjectTypes(): Observable<any> {
    return new Observable((observer) => {
      this.monsterConfigApiService.getAllProjectTypes().subscribe(
        (response) => {
          this.projectTypes = this.monsterConfigApiService.transformResponse(
            response,
            'Project_Type-id',
            true
          );
          observer.next(true);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }

  getProjectIndexerDetails(): Observable<any> {
    let projectId = this.taskModalConfig['projectId'];
    let searchConditionList = [
      this.getSearchField(
        'string',
        'CONTENT_TYPE',
        'MPM.OPERATOR.CHAR.IS',
        'is',
        'MPM_PROJECT'
      ),
      this.getSearchField(
        'string',
        'ID',
        'MPM.OPERATOR.CHAR.IS',
        'is',
        projectId,
        'AND'
      ),
    ];
    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: '',
      search_condition_list: {
        search_condition: searchConditionList,
      },
      facet_condition_list: {
        facet_condition: [],
      },
      sorting_list: {
        sort: [],
      },
      cursor: {
        page_index: 0,
        page_size: 1,
      },
    };
    return new Observable((observer) => {
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          if (response.data.length > 0) {
            this.projectindexerDetails = response.data[0];
            this.classificationNumber = this.npdProjectService.getProjectClassificationNumber(response.data[0]);
            //['NPD_PROJECT_TRIAL_DATE_CONFIRMED_WITH_BOTTLER']
            /*  if(this.getTaskNumberFromTask() == 5) {
                        this.regulatoryTimelineForLocations.market = this.projectindexerDetails?.NPD_PROJECT_LEAD_MARKET;
                        if(this.projectindexerDetails?.NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID !='NA') {
                            this.getEarlyManufacturingSites(this.projectindexerDetails.NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID).subscribe();
                        }
                    }  */
            if (this.getTaskNumberFromTask() == '04') {
              this.getProjectTypes().subscribe();
            }
            if (
              this.getTaskNumberFromTask() == '04' ||
              this.getTaskNumberFromTask() == '05' ||
              this.getTaskNumberFromTask() == '06'
            ) {
              if (
                this.projectindexerDetails
                  ?.NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID &&
                this.projectindexerDetails
                  .NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID != 'NA'
              ) {
                this.getEarlyManufacturingSites(
                  this.projectindexerDetails
                    .NPD_PROJECT_FINAL_MANUFACTURING_LOCATION_ITEM_ID
                ).subscribe();
              }
            }
            if (
              this.getTaskNumberFromTask() == '04' ||
              this.getTaskNumberFromTask() == '05'
            ) {
              this.regulatoryTimelineForLocations.market =
                this.projectindexerDetails?.NPD_PROJECT_LEAD_MARKET;
              let type =
                this.projectindexerDetails
                  ?.NPD_PROJECT_NEW_PROJECT_TYPE_ITEM_ID;
              if (type && type != 'NA') {
                this.projectClassificationParams.projectType =
                  type?.split('.')[1];
              }
            }
          }
          observer.next(this.projectindexerDetails);
          observer.complete();
        },
        () => {
          observer.error();
        }
      );
    });
  }
  getRequestIndexerDetails(requestId): Observable<any> {
    let requestResponse: any;
    let searchConditionList = [
      this.getSearchField(
        'string',
        'CONTENT_TYPE',
        'MPM.OPERATOR.CHAR.IS',
        'is',
        'REQUEST'
      ),
      this.getSearchField(
        'string',
        'ITEM_ID',
        'MPM.OPERATOR.CHAR.IS',
        'is',
        requestId,
        'AND'
      ),
    ];

    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: '',
      search_condition_list: {
        search_condition: searchConditionList,
      },
      facet_condition_list: {
        facet_condition: [],
      },
      sorting_list: {
        sort: [],
      },
      cursor: {
        page_index: 0,
        page_size: 1,
      },
    };
    return new Observable((observer) => {
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          if (response.data.length > 0) {
            this.requestIndexerDetails = response.data[0];
            requestResponse = response.data[0];
          }
          observer.next(requestResponse);
          observer.complete();
        },
        () => {
          observer.error();
        }
      );
    });
  }

  /**
   * @author JeevaR
   */
  getEarlyClassificationForFinalClassification() {
    let taskId;
    let taskItemId;
    if (
      this.taskConfig['totalData'] &&
      this.taskConfig['totalData']?.taskList
    ) {
      this.isTask4Available = this.taskConfig['totalData'].taskList?.some(
        (ele) => {
          if (
            ele['TASK_NAME'] == TaskConfigConstant.allTaskName[0].task04 ||
            ele['TASK_NAME'] == 'TASK04' ||
            ele['TASK_NAME'].toLowerCase() == 'task 04' ||
            ele['TASK_NAME'].toLowerCase() == 'task04' ||
            ele['TASK_NAME'] == 'TASK 04' ||
            ele['TASK_NAME'] == 'TASK04' ||
            ele['TASK_NAME'].toLowerCase() == 'task 04' ||
            ele['TASK_NAME'].toLowerCase() == 'task04'
          ) {
            taskId = ele['ID'];
            taskItemId = ele['ITEM_ID'];
            // console.log(TaskConfigConstant.allTaskName[0].task04);
            return true;
          } else {
            // console.log(TaskConfigConstant.allTaskName[0].task04);
            return false;
          }
        }
      );

      this.isTask5Available = this.taskConfig['totalData'].taskList?.some(
        (ele) => {
          if (
            ele['TASK_NAME'] == 'TASK 05' ||
            ele['TASK_NAME'] == 'TASK05' ||
            ele['TASK_NAME'].toLowerCase() == 'task 05' ||
            ele['TASK_NAME'].toLowerCase() == 'task05' ||
            ele['TASK_NAME'] == 'TASK 05' ||
            ele['TASK_NAME'] == 'TASK05' ||
            ele['TASK_NAME'].toLowerCase() == 'task 05' ||
            ele['TASK_NAME'].toLowerCase() == 'task05'
          ) {
            taskId = ele['ID'];
            taskItemId = ele['ITEM_ID'];
            return true;
          } else {
            return false;
          }
        }
      );
    } else if (this.taskConfig['allTask']) {
      this.isTask4Available = this.taskConfig['allTask']?.some((ele) => {
        if (
          ele['TASK_NAME'] == 'TASK 04' ||
          ele['TASK_NAME'] == 'TASK04' ||
          ele['TASK_NAME'].toLowerCase() == 'task 04' ||
          ele['TASK_NAME'].toLowerCase() == 'task04' ||
          ele['TASK_NAME'] == 'TASK 04' ||
          ele['TASK_NAME'] == 'TASK04' ||
          ele['TASK_NAME'].toLowerCase() == 'task 04' ||
          ele['TASK_NAME'].toLowerCase() == 'task04'
        ) {
          taskId = ele['ID'];
          taskItemId = ele['ITEM_ID'];
          return true;
        } else {
          return false;
        }
      });

      this.isTask5Available = this.taskConfig['allTask']?.some((ele) => {
        if (
          ele['TASK_NAME'] == 'TASK 05' ||
          ele['TASK_NAME'] == 'TASK05' ||
          ele['TASK_NAME'].toLowerCase() == 'task 05' ||
          ele['TASK_NAME'].toLowerCase() == 'task05' ||
          ele['TASK_NAME'] == 'TASK 05' ||
          ele['TASK_NAME'] == 'TASK05' ||
          ele['TASK_NAME'].toLowerCase() == 'task 05' ||
          ele['TASK_NAME'].toLowerCase() == 'task05'
        ) {
          taskId = ele['ID'];
          taskItemId = ele['ITEM_ID'];
          return true;
        } else {
          return false;
        }
      });
    }

    // console.log('isTask4Available', this.isTask4Available);
    // console.log('isTask5Available', this.isTask5Available);
    if (!(this.isTask4Available == true && this.isTask5Available == true)) {
      if (this.isTask4Available == true && this.isTask5Available == false) {
        this.taskObjFinalClassification =
          this.npdTaskService.createCustomTaskUpdateObjectDuringTaskInAvailabilty(
            this.projectindexerDetails,
            this.requestIndexerDetails,
            this.isTask4Available,
            this.isTask5Available
          );
        this.taskObjFinalClassification.Task.Id = taskId;
        this.taskObjFinalClassification.Task.ItemId = taskItemId;
      }
      if (this.isTask4Available == false && this.isTask5Available == true) {
        this.taskObjFinalClassification =
          this.npdTaskService.createCustomTaskUpdateObjectDuringTaskInAvailabilty(
            this.projectindexerDetails,
            this.requestIndexerDetails,
            this.isTask4Available,
            this.isTask5Available
          );
        this.taskObjFinalClassification.Task.Id = taskId;
        this.taskObjFinalClassification.Task.ItemId = taskItemId;
      }
      if (
        this.taskObjFinalClassification &&
        Object.keys(this.taskObjFinalClassification).length > 1
      ) {
        this.taskObjFinalClassification.TaskOperation = 'SAVE'; //'EDIT
        const requestObj = {
          afterTaskEditActions: this.taskObjFinalClassification,
        };
        this.npdTaskService.updateTask(requestObj).subscribe((response) => {
          this.taskForm?.patchValue({
            finalManufacturingLocation:
              this.taskObjFinalClassification?.ProjectFields
                ?.FinalCountryOfManufactureId,
            finalManufacturingLocationName:
              this.taskObjFinalClassification?.ProjectFields
                ?.FinalCountryOfManufacture,
            finalManufacturingSite:
              this.taskObjFinalClassification?.ProjectFields
                ?.FinalManufacturingSiteId,
            finalManufacturingSiteName:
              this.taskObjFinalClassification?.ProjectFields
                ?.FinalManufacturingSite
          });
          console.log('updated');
          // this.loadData();
        });
      }
    }
  }

  /**
   * @author JeevaR
   */
  changeTaskOwnerAsRMUser() {
    let selectedTaskOwner = this.taskForm.controls.owner.value;
    if (selectedTaskOwner === '' || selectedTaskOwner == undefined) {
      this.notificationService.error('Please add Task Owner !');
      return;
    }
    let selectedUser = {
      option: {
        value: {
          actualValue: selectedTaskOwner.name,
          displayName: selectedTaskOwner.displayName,
          isActive: 'true',
          name: selectedTaskOwner.value,
          value: selectedTaskOwner.displayName,
        },
      },
    };
    this.selectedRMUser = selectedUser.option.value;
    this.onRMUserSelection(selectedUser);
    this.taskForm.get('rmUserId').markAsDirty();
  }
  checkClassificationSimilarity(
    customTaskObject,
    requestIndexerDetails,
    projectindexerDetails
  ) {
    let isNormalClassificationChange = false;
    let RestrictTask18n33Deletion = false;
    let DeleteTask18n33 = false;

    if (projectindexerDetails?.NPD_PROJECT_CURRENT_FINAL_CLASSIFICATION) {
      if (
        customTaskObject?.ProjectFields?.NewFG !=
        projectindexerDetails?.NPD_PROJECT_CURRENT_NEW_FG
      ) {
        isNormalClassificationChange = true;
      } else if (
        !isNormalClassificationChange &&
        customTaskObject?.ProjectFields?.NewRandD !=
        projectindexerDetails?.NPD_PROJECT_CURRENT_NEW_RD_FORMULA
      ) {
        isNormalClassificationChange = true;
      } else if (
        !isNormalClassificationChange &&
        customTaskObject?.ProjectFields?.NewHBC !=
        projectindexerDetails?.NPD_PROJECT_CURRENT_NEW_HBC_FORMULA
      ) {
        isNormalClassificationChange = true;
      } else if (
        !isNormalClassificationChange &&
        customTaskObject?.ProjectFields?.NewPrimaryPack !=
        projectindexerDetails?.NPD_PROJECT_CURRENT_NEW_PRIMARY_PACK
      ) {
        isNormalClassificationChange = true;
      } 
      else if(!isNormalClassificationChange && customTaskObject?.ProjectFields?.LeadFormula != projectindexerDetails?.NPD_PROJECT_CURRENT_LEAD_FORMULA){
        isNormalClassificationChange = true;
      }
      else if (
        !isNormalClassificationChange &&
        customTaskObject?.ProjectFields?.NewSecondaryPack !=
        projectindexerDetails?.NPD_PROJECT_CURRENT_NEW_SECONDARY_PACK
      ) {
        if (
          (projectindexerDetails?.NPD_PROJECT_CURRENT_NEW_SECONDARY_PACK ==
            'false' ||
            projectindexerDetails?.NPD_PROJECT_CURRENT_NEW_SECONDARY_PACK ==
            false) &&
          (customTaskObject?.ProjectFields?.NewSecondaryPack == true ||
            customTaskObject?.ProjectFields?.NewSecondaryPack === 'true')
        ) {
          RestrictTask18n33Deletion = true;
        } else if (
          (projectindexerDetails?.NPD_PROJECT_CURRENT_NEW_SECONDARY_PACK ==
            'true' ||
            projectindexerDetails?.NPD_PROJECT_CURRENT_NEW_SECONDARY_PACK ==
            true) &&
          (customTaskObject?.ProjectFields?.NewSecondaryPack == false ||
            customTaskObject?.ProjectFields?.NewSecondaryPack === 'false')
        ) {
          DeleteTask18n33 = true;
        }
      } else {
        if (
          projectindexerDetails?.NPD_PROJECT_CURRENT_FINAL_CLASSIFICATION !=
          customTaskObject?.FinalClassification
        )
          isNormalClassificationChange = true;
      }
    } else {
      if (
        customTaskObject?.ProjectFields?.NewFG !=
        requestIndexerDetails?.RQ_NEW_FG_SAP
      ) {
        isNormalClassificationChange = true;
      } else if (
        !isNormalClassificationChange &&
        customTaskObject?.ProjectFields?.NewRandD !=
        requestIndexerDetails?.RQ_NEW_RD_FORMULA
      ) {
        isNormalClassificationChange = true;
      } else if (
        !isNormalClassificationChange &&
        customTaskObject?.ProjectFields?.NewHBC !=
        requestIndexerDetails?.RQ_NEW_HBC_FORMULA
      ) {
        isNormalClassificationChange = true;
      } else if (
        !isNormalClassificationChange &&
        customTaskObject?.ProjectFields?.NewPrimaryPack !=
        requestIndexerDetails?.RQ_NEW_PRIMARY_PACKAGING
      ) {
        isNormalClassificationChange = true;
      }
      else if (
        !isNormalClassificationChange &&
        customTaskObject?.ProjectFields?.LeadFormula !=
          requestIndexerDetails?.RQ_LEAD_FORMULA
      ) {
        isNormalClassificationChange = true;
      } else if (
        !isNormalClassificationChange &&
        customTaskObject?.ProjectFields?.NewSecondaryPack !=
          requestIndexerDetails?.RQ_IS_SECONDARY_PACKAGING
      ) {
        if (
          (requestIndexerDetails?.RQ_IS_SECONDARY_PACKAGING == 'false' ||
            requestIndexerDetails?.RQ_IS_SECONDARY_PACKAGING == false) &&
          (customTaskObject?.ProjectFields?.NewSecondaryPack == true ||
            customTaskObject?.ProjectFields?.NewSecondaryPack === 'true')
        ) {
          RestrictTask18n33Deletion = true;
        } else if (
          (requestIndexerDetails?.RQ_IS_SECONDARY_PACKAGING == 'true' ||
            requestIndexerDetails?.RQ_IS_SECONDARY_PACKAGING == true) &&
          (customTaskObject?.ProjectFields?.NewSecondaryPack == false ||
            customTaskObject?.ProjectFields?.NewSecondaryPack === 'false')
        ) {
          DeleteTask18n33 = true;
        }
      } else {
        if (
          requestIndexerDetails?.RQ_EARLY_PROJECT_CLASSIFICATION !=
          customTaskObject?.FinalClassification
        )
          isNormalClassificationChange = true;
      }
    }
    customTaskObject.isNormalClassificationChange =
      isNormalClassificationChange;
    customTaskObject.RestrictTask18n33Deletion = RestrictTask18n33Deletion;
    customTaskObject.DeleteTask18n33 = DeleteTask18n33;
    return customTaskObject;
  }
  ngOnInit() {
    this.getAllMarkets();
    this.getBusinessUnits();
    this.getDraftManufacturingLocation();
    super.ngOnInit();
    // console.log('this.taskConfig', this.taskConfig);

    this.getProjectIndexerDetails().subscribe((response) => {
      // console.log(response);
      this.bussinessUnit = response?.PR_BUSINESS_UNIT;
      this.leadMarket = response?.NPD_PROJECT_LEAD_MARKET_ID;
      this.finalDraftManufacturingLocation = response?.NPD_PROJECT_EARLY_MANUFACTURING_LOCATION_ITEM_ID;
      this.getRequestIndexerDetails(response.PR_REQUEST_ITEM_ID).subscribe(
        (resp) => {
          this.projectindexerDetails = response;
          this.editTaskHeaderData.emit(this.projectindexerDetails);
          // console.log(' this.projectindexerDetails = response;', response);
          this.requestIndexerDetails = resp;
        }
      );
    });
    this.getAllUsers();
    //this.weekPickerMaxDate = {year: 2021, month: 9, day: 10};
    //uncomment this code after UAT
    /*  let dateVal = new Date();
        let weekYearValue = this.monsterUtilService.calculateWeek(dateVal);//''+(this.calculateWeek(date)+'/'+ new Date(date).getFullYear());
        [ , dateVal] = this.getStartEndDate(weekYearValue);
        this.weekPickerMaxDate = {year: dateVal.getFullYear(), month: dateVal.getMonth() + 1, day: dateVal.getDate()}; */
    // console.log(this.taskModalConfig.taskOwnerConfig);
  }

  numericPattern = /^[0-9]*$/;
  numericOnly(event) {
    return this.numericPattern.test(event.key);
  }

  ngOnDestroy() {
    this.monsterUtilService.clearFormPropertiesandRelations();
  }

  public getAllUsers() {
    this.commentsService.getAllUsers().subscribe((res) => {
      this.userListData = res;
    });
  }

  public onCommentChange(event) {
    // console.log(event);
    this.taskForm.markAsDirty(); // this Statement is used to mak Form pristine to true
  }
  getColumnMaxWidth(column) {
    if (column?.width) {
      return { 'max-width.px': column.width };
    } else {
      return {
        'max-width.em': '8', 'white-space': 'nowrap',
        'width': '70px',
        'overflow': 'hidden',
        'text-overflow': 'ellipsis',
      };
    }
  }



  public generateClassificationNumber(parameters, isThrowableError) {
    const notificationValidationArray = [
      parameters.newFG,
      parameters.newHBC,
      parameters.newRD,
      parameters.primaryPackaging,
      parameters.secondaryPackaging,
    ];
    const notificationValidation = notificationValidationArray.every(value => value !== null && value !== undefined);

    this.monsterConfigApiService.generateClassificationNumber(parameters).subscribe(response => {
      // console.log(response);
      if (response.length > 0) {
        this.taskForm.patchValue({
          finalClassification: notificationValidation ? response[0]?.CLASSIFICATION_NUMBER : null,
          finalClassificationDesc: notificationValidation ? response[0]?.CLASSIFICATION_DESCRIPTION : null,
          registrationClassificationAvailable: response[0]?.REGISTRATION_CLASSIFICATION_AVAILABLE == 'true' ? true : false,
          registrationRequirement: response[0]?.REGISTRATION_REQUIREMENT == 'true' ? true : false
        });
        if (response[0]?.CLASSIFICATION_NUMBER == undefined && isThrowableError && notificationValidation) {
          this.notificationService.error(
            'No Project Classification Data Available for the Input'
          );
        }
      }
    }, error => {
      // console.log("CalssificationError");
      if(isThrowableError && notificationValidation){
        this.notificationService.error(
          'No Project Classification Data Available for the Input'
        );
      }
      this.taskForm.patchValue({
        finalClassification: '',
        finalClassificationDesc: '',
        registrationClassificationAvailable: 'No',
        registrationRequirement: 'No'
      });
    })


  }


  /**
   * @author Baba
   * @param onInItCall to check lead formula boolean field during opening of component
   * @returns based on the conditions to be applied to lead formula field returns with or without calling generateClassificationchange API
   */
  disableLeadFormulaField(onInItCall?){
    if(this.taskForm.controls.leadFormula.value==null || this.taskForm.controls.leadFormula.value==undefined){
      this.taskForm.controls.leadFormula.setValue(false);
    }
    if(this.taskForm.controls.newRnDFormula.enabled==true){
      let projectType= this.taskForm.controls.projectTypeName.value;
    if(this.taskForm.controls.newRnDFormula.value==true
        && projectType==BusinessConstants.PROJECT_TYPES.COMMERCIAL_NEW_SKU){
      this.taskForm.controls.leadFormula.enable();
      this.taskForm.controls.leadFormula.updateValueAndValidity()
    }else{
      this.taskForm.controls.leadFormula.setValue(false);
      this.taskForm.controls.leadFormula.disable();
      this.taskForm.controls.leadFormula.updateValueAndValidity();
    }
    if(onInItCall){
      return;
    }
    this.getClassificationChange(true)
    }
  }

}



/*     const tasks = [
        {taskNumber: 2, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM','Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager'],
            show: ['canStart','draftClassification','finalClassification','site','scopingDocumentRequirement',
                   'winshuttleProject','requiredComplete','issuanceType','pallet','tray','isRequired',
                'trialDateConfirmed','BOMSentActual','countryofManufacture','processAuthorityApproval','transactionSchemeAnalysisRequired','nonProprietaryMaterialCostingRequired'
             ]
            ,//show: ['canStart'],
            disable: []
        },
        {taskNumber: 3, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM','Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager'],
            show: ['canStart'], disable: []
        },
        {taskNumber: 6, actualStartDate: true, actualEndDate: false, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']
            ,show: ['canStart'], disable: []},
        {taskNumber: 10, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager']
            ,show: ['canStart'], disable: []},
        {taskNumber: 15, actualStartDate: true, actualEndDate: false, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']},
        {taskNumber: 18, actualStartDate: true, actualEndDate: false, duration: true, role: ['E2E PM','Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager']},
        {taskNumber: 27, actualStartDate: true, actualEndDate: true, duration: true, role:  ['E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']},
        {taskNumber: 28, actualStartDate: true, actualEndDate: false, duration: true, role: ['E2E PM','Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager']},
        {taskNumber: 29, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM','Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager']},
        {taskNumber: 33, actualStartDate: true, actualEndDate: false, duration: true, role: ['E2E PM','Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager']},
        {taskNumber: 35, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM','Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager']},
        {taskNumber: 39, actualStartDate: true, actualEndDate: true, duration: true, role:  ['E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']},
        {taskNumber: 40, actualStartDate: true, actualEndDate: true, duration: true, role:  ['E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']},
        //role-(Role 1,Role 2,Role 3) project tasks menu, taskOwner(functional role,Role3)-Task dashboard
        {taskNumber: 4, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager'],
         show: ['canStart','draftClassification','finalClassification'], disable: ['draftClassification','finalClassification']
        },
        {taskNumber: 7, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager']},
        {taskNumber: 11, actualStartDate: true, actualEndDate: true, duration: true, role: ['GFG','E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager']},
        {taskNumber: 12, actualStartDate: true, actualEndDate: true, duration: true, role: ['GFG','E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager']},
        {taskNumber: 13, actualStartDate: true, actualEndDate: true, duration: true, role: ['GFG','E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager']},
        {taskNumber: 16, actualStartDate: true, actualEndDate: true, duration: true, role: ['GFG','E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager']},
        {taskNumber: 25, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager']},
        {taskNumber: 26, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager']},
        {taskNumber: 34, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Corp PM', 'Programme Manager']},

        {taskNumber: '13A', actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']},
        {taskNumber: 17, actualStartDate: true, actualEndDate: false, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']},
        {taskNumber: 20, actualStartDate: true, actualEndDate: false, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']},
        {taskNumber: 21, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']},
        {taskNumber: 22, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']},
        {taskNumber: 41, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']},

        {taskNumber: 9, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Project Specialist', 'Programme Manager']},
        {taskNumber: 14, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Project Specialist', 'Programme Manager']},
        {taskNumber: 19, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Project Specialist', 'Programme Manager']},
        {taskNumber: 30, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Project Specialist', 'Programme Manager']},
        {taskNumber: 31, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM','E2E PM', 'Programme Manager'], taskOwner: ['Project Specialist', 'Programme Manager']},

        {taskNumber: 18, actualStartDate: false, actualEndDate: false, duration: false, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Secondary AW Specialist', 'Programme Manager']},
        {taskNumber: 33, actualStartDate: false, actualEndDate: false, duration: false, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Secondary AW Specialist', 'Programme Manager']},

        {taskNumber: 5, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']},
        {taskNumber: 23, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']},
        {taskNumber: 24, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']},
        {taskNumber: 32, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']},
        {taskNumber: 36, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']},
        {taskNumber: 37, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']},
        {taskNumber: 38, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']},
        {taskNumber: 39, actualStartDate: false, actualEndDate: false, duration: false, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
        },

        {taskNumber: 42, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
        ,show: ['canStart'], disable: []},
    ];    if(tasks[currentTaskIndex]?.taskNumber == 10){
         this.requiredComp = true;
    } else {
        this.requiredComp = false;
    }   /*  if(this.taskModalConfig?.IS_ACTIVE == "false") {
                        let finalTaskStatus = this.taskModalConfig.taskStatusList.find(status => status.STATUS_TYPE == StatusTypes.FINAL_APPROVED);
                        if(finalTaskStatus && finalTaskStatus["MPM_Status-id"]?.Id == this.taskModalConfig.selectedTask?.R_PO_STATUS["MPM_Status-id"]?.Id) {
                            this.disable.taskAction = false;//enabling revoke when task completed & is in inactive state
                        }
                    }

                    /* checkIsFinalTaskStatus() {
    let finalTaskStatus = this.taskModalConfig.taskStatusList.find(status => status.STATUS_TYPE == StatusTypes.FINAL_APPROVED);
    if(finalTaskStatus && finalTaskStatus["MPM_Status-id"]?.Id == this.taskModalConfig.selectedTask?.R_PO_STATUS["MPM_Status-id"]?.Id) {
        return true;//enabling revoke when task completed & is in inactive state
    }
    return false;
}
 if(isUserRoleMatched) {
            if(this.taskConfig['isTaskActive'] || isTaskSuperAccess) {//||(this.filterConfigService.isE2EPMRole() || this.filterConfigService.isProgramManagerRole() || this.checkIsProjectOwner)
                this.enableFieldAccessToRoles(tasks,currentTaskIndex)
            } else {
                if(isTaskSuperAccess) {// && !this.getFinalStatusCheck(this.taskModalConfig?.selectedTask)
                    this.enableFieldAccessToRoles(tasks,currentTaskIndex);
                } else {
                    this.disableFieldAccess(tasks,currentTaskIndex,true);
                }
            }
        } else {
                if(isTaskSuperAccess) {
                    this.enableFieldAccessToRoles(tasks,currentTaskIndex);
                } else {
                    this.disableFieldAccess(tasks,currentTaskIndex,true);
                }
        }

enableFieldAccessToRoles(tasks,currentTaskIndex,isDisable) {
    this.disable.actualStartDate = isDisable? true :!tasks[currentTaskIndex].actualStartDate;
    this.disable.actualEndDate =  isDisable? true :!tasks[currentTaskIndex].actualEndDate;
    this.disable.duration =  isDisable? true :!tasks[currentTaskIndex].duration;
    this.disable.role =  isDisable? true :false;
    this.disable.status =  isDisable? true :false;
    this.disable.taskOwner =  isDisable? true :false;
    this.disable.description =  isDisable? true : true;
    this.disable.taskAction =  isDisable? true :false;
    this.showConfiguredFields(tasks[currentTaskIndex])
    this.disableConfiguredFields(tasks[currentTaskIndex],isDisable);
}

disableFieldAccess(tasks,currentTaskIndex,disableTaskAction) {
    this.disable.actualStartDate = true;
    this.disable.actualEndDate = true;
    this.disable.duration = true;
    this.disable.role = true;
    this.disable.status = true;
    this.disable.taskOwner = true;
    this.disable.description = true;
    this.disable.taskAction = disableTaskAction;
    this.showConfiguredFields(tasks[currentTaskIndex])
    this.disableConfiguredFields(tasks[currentTaskIndex],true);
}*/
