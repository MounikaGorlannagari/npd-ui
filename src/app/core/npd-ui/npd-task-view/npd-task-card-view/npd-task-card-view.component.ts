import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationModalComponent, FieldConfigService, FormatToLocalePipe, IndexerDataTypes, LoaderService, MPMField, MPMFieldConstants, MPMFieldKeys, MPM_LEVELS, NotificationService, OtmmMetadataService, ProjectConstant, SearchConfigConstants, SharingService, StatusTypes, TaskConstants, TaskService, UtilService, ViewConfig, ViewConfigService } from 'mpm-library';
import { MPMTaskCardViewComponent } from 'src/app/core/mpm-lib/project/tasks/task-card-view/task-card-view.component';
import { CommentsModalComponent } from '../../collab/comments-modal/comments-modal.component';
import { CommentConfig } from '../../collab/objects/CommentConfig';
import { FilterConfigService } from '../../filter-configs/filter-config.service';
import { WorkflowRuleService } from '../../npd-shared/services/workflow-rule.service';
import { MonsterUtilService } from '../../services/monster-util.service';
import { NpdTaskService } from '../services/npd-task.service';
@Component({
  selector: 'app-npd-task-card-view',
  templateUrl: './npd-task-card-view.component.html',
  styleUrls: ['./npd-task-card-view.component.scss']
})
export class NpdTaskCardViewComponent extends MPMTaskCardViewComponent {
  
  constructor(
    public taskService: TaskService,
    public sharingService: SharingService,
    public otmmMetadataService: OtmmMetadataService,
    public formatToLocalePipe: FormatToLocalePipe,
    public utilService: UtilService,
    public viewConfigService: ViewConfigService,
    public fieldConfigService: FieldConfigService,
    public loaderService: LoaderService,
    public notificationService: NotificationService,
    public dialog: MatDialog,
    public monsterUtilService:MonsterUtilService,
    public npdTaskService:NpdTaskService,
    public filterConfigService:FilterConfigService
  ) {
  super(taskService,sharingService,otmmMetadataService,formatToLocalePipe,utilService,
    viewConfigService,fieldConfigService,loaderService,notificationService,dialog);
  }

  /*getDeleteAccess() {
    return this.isFinalTask || !this.getSpecifiedRoleAccess();
  } */

  getSpecifiedRoleAccess() {
    return this.filterConfigService.isCorpPMRole() || this.filterConfigService.isE2EPMRole() || this.filterConfigService.isOPSPMRole() || this.filterConfigService.isProgramManagerRole();
  }


  openCommentDialog() {
    // console.log('this.taskData',this.taskData)
    const taskItemId: string = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID).split('.')[1];
    const requestId: string = this.taskData.PR_REQUEST_ITEM_ID.split('.')[1];
    const taskId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ID);
    const projectId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_PROJECT_ID)?.split('.')[1];//selectedTask['PROJECT_ITEM_ID']?.split('.')[1];
    

    const dialogRef = this.dialog.open(CommentsModalComponent, {
        width: '75%',
        height: '90vh',
        maxHeight: '90vh',
        minHeight: '90vh',
        panelClass: 'comment-dialog-modal',
        disableClose: true,
        data: {
            requestId: requestId,
            commentType:  CommentConfig.TASK_COMMENT_TYPE_VALUE,
            projectId: projectId,
            taskId:taskItemId,
            //isManager: this.isPMView,
            enableToComment: true // this.enableToComment
        }
    });
    dialogRef.afterClosed().subscribe(result => {
        // console.log('comments module dialog is closed \n', result);
    });
  }
  
  getProjectPropertyByMapper(taskData, mapperName): string {
    const displayColum: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.PROJECT);
    return this.getProperty(displayColum, taskData);
  }

  openTaskDetailsPopup(taskData) {
    // if(this.taskConfig.isReadOnly || this.isOnHoldProject) {
    //   return;
    // } 
    if(this.isCancelledTask || this.projectCancelledStatus){
        return;
    }
    this.npdTaskService.setEditTaskHeaderData(taskData);
    this.editTask();
  }
  checkDateType(displayColumn) {
    return displayColumn?.DATA_TYPE?.toLowerCase() === IndexerDataTypes.DATETIME?.toLowerCase()
  }
  
  getWeek(column, dateValue) {
    //let date = this.monsterUtilService.getFieldValueByDisplayColumn(column,row);
    if (dateValue && dateValue != 'NA' && column?.DATA_TYPE.toLowerCase() === IndexerDataTypes.DATETIME.toLowerCase()) {
        return this.monsterUtilService.calculateWeek(dateValue);
    }
  }

  getWeekValue(taskData,columnName) {
    if (taskData?.[columnName] && taskData?.[columnName]!='NA' ) {
      return this.monsterUtilService.calculateWeek(taskData[columnName]);
    }
  }

  deleteTask(isForceDelete: boolean) {
        const taskId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ID);
        let projectId = this.taskData?.PROJECT_ITEM_ID?.split('.')[1]
        let msg;
        const isCustomWorkflow = this.utilService.getBooleanValue(this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW);
        if (isCustomWorkflow) {
            msg = 'Are you sure you want to delete the tasks and reconfigure the workflow rule engine?';
        } else {
            msg = 'Are you sure you want to delete the tasks and Exit?';
        }

        //  MPMV3-2094
        this.taskService.getAllApprovalTasks(taskId).subscribe(approvalTask => {
            // console.log(approvalTask)
            if (approvalTask.tuple != undefined) {
                this.forceDeletion = true;
                this.affectedTask = [];
                let tasks;
                if (!Array.isArray(approvalTask.tuple)) {
                    tasks = [approvalTask.tuple];
                } else {
                    tasks = approvalTask.tuple;
                }
                tasks.forEach(task => {
                    if(task.old.TaskView.isDeleted === 'false') {
                        this.affectedTask.push(task.old.TaskView.name);
                    }                    
                });
            }
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    //name: this.isTemplate ? [] : this.affectedTask,
                    name: this.affectedTask,
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    if(isForceDelete) {
                        this.npdTaskService.forceDeleteTask(taskId, isCustomWorkflow).subscribe(
                            response => {
                              this.notificationService.success('Task has been deleted successfully');
                              this.refresh();
                            }, error => {
                                this.notificationService.error('Delete Task operation failed, try again later');
                            });
                    } else {
                        this.taskService.softDeleteTask(taskId, isCustomWorkflow).subscribe(
                            response => {
                                this.npdTaskService.triggerNPDWorkflow(projectId).subscribe(response => {
                                  this.notificationService.success('Task has been deleted successfully');
                                  this.refresh();
                                }, error => {
                                  this.notificationService.error('Delete Task operation failed, try again later');
                                });
                            }, error => {
                                this.notificationService.error('Delete Task operation failed, try again later');
                            });
                    }                    
                }
            });
        });
    }
  
  ngOnInit() {
    let viewConfig;
    // console.log(this.taskConfig)
    const currentUSerID = this.sharingService.getCurrentUserItemID();
    const customMetadataFields = this.otmmMetadataService.getCustomTaskMetadataFields();

    this.projectCancelledStatus = this.projectStatusType === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? true : false;

    if (this.taskData.STATUS && this.taskData.STATUS.TYPE === TaskConstants.StatusConstant.TASK_STATUS.CANCELLED || this.projectCancelledStatus) {
        this.isCancelledTask = true;
    }
    if (currentUSerID && this.taskConfig.selectedProject && this.taskConfig.selectedProject.R_PO_PROJECT_OWNER &&
        this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'] && currentUSerID === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
        this.isProjectOwner = true;
    }
    this.taskStatusType = StatusTypes.FINAL_REJECTED;
    this.handleActionsOnTaskView();
    if (this.taskData && this.taskData.STATUS_STATE === 'FINAL') {
        this.isFinalStatus = true;
    }
    //this.enableDeleteForTask = this.taskData.TASK_STATUS_TYPE === StatusTypes.INITIAL ? true : this.taskData.TASK_STATUS_TYPE === StatusTypes.INTERMEDIATE ? true : false;

    if (this.taskConfig.taskViewName) {
        viewConfig = this.taskConfig.taskViewName;
    } else {
        viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
    }
    this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetailsParent: ViewConfig) => {
        const viewDetails: ViewConfig = viewDetailsParent ? JSON.parse(JSON.stringify(viewDetailsParent)) : null;
        if (viewDetails && viewDetails.R_PM_CARD_VIEW_MPM_FIELDS) {
            viewDetails.R_PM_CARD_VIEW_MPM_FIELDS.forEach((mpmField: MPMField) => {
                if (this.taskConstants.DEFAULT_CARD_FIELDS.indexOf(mpmField.MAPPER_NAME) < 0) {
                    mpmField.VALUE = this.fieldConfigService.getFieldValueByDisplayColumn(mpmField, this.taskData);
                    if(!(mpmField.MAPPER_NAME == MPMFieldConstants.MPM_TASK_FIELDS.IS_TASK_ACTIVE)) {
                      this.displayableMPMFields.push(mpmField);
                    }
                }
            });
        }
    });
    if (this.taskConfig.currentProjectId && this.taskConfig.projectData && this.taskConfig.projectData.length > 0) {
        this.taskConfig.selectedProject = this.taskConfig.projectData.find(project => project.ID === this.taskConfig.currentProjectId);
    }
    if (this.taskConfig.selectedProject && this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW === 'true' && this.taskConfig.workflowActionRules && this.taskConfig.workflowActionRules.length > 0) {
        const taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
        if (taskItemId && taskItemId.split('.')[1]) {
            const taskId = taskItemId.split('.')[1];
            const currentWorkflowActionRules = this.taskConfig.workflowActionRules.filter(workflowActionRule => workflowActionRule.TaskId === taskId);
            this.currentWorkflowActions = [];
            currentWorkflowActionRules.forEach(rule => {
                if (rule && rule.Action) {
                    if (rule.Action.length > 0) {
                        rule.Action.forEach(action => {
                            const selectedAction = this.currentWorkflowActions.find(currentAction => currentAction.Id === action.Id);
                            if (!selectedAction) {
                                this.currentWorkflowActions.push(action);
                            }
                        });
                    } else {
                        this.currentWorkflowActions.push(rule.Action);
                    }
                }
            });
        }
    }
}
openEditTaskAction(){
    const editTaskData={
    TASK_NAME:  this.getProperty(this.taskData, this.MPMFeildConstants.TASK_NAME),
     TASK_DESCRIPTION: this.getProperty(this.taskData, this.MPMFeildConstants.TASK_DESCRIPTION)
    }
    // const taskName: string = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_NAME);
    // const taskDescription: string = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_DESCRIPTION);
    this.npdTaskService.setEditTaskHeaderData(editTaskData);
    this.editTask()
}
}
