
//check here
export const TaskConfigConstant = {
    allTaskConfig: [
        {
            taskNumber: '02', actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager'],
         /*    show: ['canStart','draftClassification','finalClassification','site','scopingDocumentRequirement',
                   'winshuttleProject','requiredComplete','issuanceType','pallet','tray','isRequired',
                'trialDateConfirmed','BOMSentActual','countryofManufacture','processAuthorityApproval','transactionSchemeAnalysisRequired','nonProprietaryMaterialCostingRequired'
             ] ,*/show: ['canStart', , 'taskOwnerRMUser'],
            disable: []
        },
        {
            taskNumber: '03', actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager'],
            show: ['canStart', , 'taskOwnerRMUser'], disable: []
        },
        {
            taskNumber: '04', actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Corp PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager'],
            show: ['canStart', 'draftClassification', 'finalClassification', 'newFg', 'newRnDFormula', 'newHBCFormula', 'newPrimaryPackaging', 'secondaryPackaging', 'projectType','leadFormula', 'site', 'countryofManufacture', 'earlyProjectType', , 'taskOwnerRMUser'], disable: ['draftClassification', 'finalClassification', 'site', 'countryofManufacture']
        },
        {
            taskNumber: '05', actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Ops PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'site', 'countryofManufacture', 'earlyManufacturingLocation', 'earlyManufacturingSite', , 'taskOwnerRMUser']
        }, //'site','countryofManufacture'//nable

        {
            taskNumber: '06', actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'RTM', 'E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']
            , show: ['canStart', 'site', , 'taskOwnerRMUser'], disable: []
        },
        {
            taskNumber: '07', actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager']
            , show: ['canStart', 'scopingDocumentRequirement', 'processAuthorityApproval', , 'taskOwnerRMUser'],
            required: ['scopingDocumentRequirement', 'processAuthorityApproval', , 'taskOwnerRMUser']
        },
        {
            taskNumber: '09', actualStartDate: true, actualEndDate: true, duration: true, role: ['Project Specialist', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Project Specialist', 'Programme Manager']
            , show: ['canStart', 'winshuttleProject', , 'taskOwnerRMUser'], disable: []
        },

        {
            taskNumber: 10, actualStartDate: true, actualEndDate: true, duration: true, role: ['E2E PM', 'Corp PM', 'Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager']//
            , show: ['canStart', 'requiredComplete', , 'taskOwnerRMUser'],
            required: ['requiredComplete'], disable: []
        },

        //role-(Role 1,Role 2,Role 3) project tasks menu, taskOwner(functional role,Role3)-Task dashboard

        {
            taskNumber: 11, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager']
            , show: ['canStart', , 'taskOwnerRMUser']
        },
        {
            taskNumber: 12, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager']
            , show: ['canStart','taskOwnerRMUser']
        },
        {
            taskNumber: 13, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager']
            , show: ['canStart', 'transactionSchemeAnalysisRequired', 'nonProprietaryMaterialCostingRequired'],
            required: ['transactionSchemeAnalysisRequired', 'nonProprietaryMaterialCostingRequired']
        },

        {
            taskNumber: '13A', actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager']
            , show: ['canStart', , 'taskOwnerRMUser']
        },//'transactionSchemeAnalysisRequired','nonProprietaryMaterialCostingRequired'

        {
            taskNumber: 14, actualStartDate: true, actualEndDate: true, duration: true, role: ['Project Specialist', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Project Specialist', 'Programme Manager']
            , show: ['canStart']
        },

        {
            taskNumber: 15, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'RTM', 'E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']
            , show: ['canStart', , 'taskOwnerRMUser']
        },
        {
            taskNumber: 16, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager']
            , show: ['canStart', 'issuanceType','taskOwnerRMUser']
        },
        {
            taskNumber: 17, actualStartDate: true, actualEndDate: true, duration: true, role: ['Regulatory Affairs', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader', 'Regs Traffic Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },

        {
            taskNumber: 18, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Secondary AW Specialist', 'E2E PM', 'Programme Manager'], taskOwner: ['Secondary AW Specialist', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        //{taskNumber: 18, actualStartDate: false, actualEndDate: false, duration: false, role: ['E2E PM', 'Programme Manager'], taskOwner: ['Secondary AW Specialist', 'Programme Manager']},

        {
            taskNumber: 19, actualStartDate: true, actualEndDate: true, duration: true, role: ['Project Specialist', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Project Specialist', 'Programme Manager']
            , show: ['canStart',]
        },
        {
            taskNumber: 20, actualStartDate: true, actualEndDate: true, duration: true, role: ['Regulatory Affairs', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader', 'Regs Traffic Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 21, actualStartDate: true, actualEndDate: true, duration: true, role: ['Regulatory Affairs', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader', 'Regs Traffic Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 22, actualStartDate: true, actualEndDate: true, duration: true, role: ['Regulatory Affairs', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader', 'Regs Traffic Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 23, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 24, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 25, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 26, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },

        {
            taskNumber: 27, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'RTM', 'E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 28, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },

        {
            taskNumber: 29, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager'],
            show: ['canStart', 'taskOwnerRMUser']
        },

        {
            taskNumber: 30, actualStartDate: true, actualEndDate: true, duration: true, role: ['Project Specialist', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Project Specialist', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 31, actualStartDate: true, actualEndDate: true, duration: true, role: ['Project Specialist', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Project Specialist', 'Programme Manager']
            , show: ['canStart', 'pallet', 'tray', 'taskOwnerRMUser']
        },
        {
            taskNumber: 32, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'isRequired', 'taskOwnerRMUser']
        },
        {
            taskNumber: 33, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Secondary AW Specialist', 'E2E PM', 'Programme Manager'], taskOwner: ['Secondary AW Specialist', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        //{taskNumber: 33, actualStartDate: true, actualEndDate: false, duration: true, role: ['E2E PM','Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager']},

        {
            taskNumber: 34, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader'], taskOwner: ['Corp PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 35, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'E2E PM', 'Programme Manager'], taskOwner: ['E2E PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },

        {
            taskNumber: 36, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'BOMSentActual', 'productionSchedule', 'noofWeeks', 'taskOwnerRMUser'], required: ['productionSchedule']
        },
        {
            taskNumber: 37, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },

        {
            taskNumber: 38, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 39, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'RTM', 'E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']
            , show: ['canStart', 'trialDateConfirmed', 'trialVolume24EQCases', 'taskOwnerRMUser']
        },//trialDateConfirmed
        {
            taskNumber: 40, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'RTM', 'E2E PM', 'Programme Manager'], taskOwner: ['RTM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 41, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Regulatory Affairs', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader', 'Regs Traffic Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 42, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'isRequired', 'finalAgreed1stProdConfirmedWithBottler', 'taskOwnerRMUser', 'firstProductionVolume24EQCases'], disable: ['finalAgreed1stProdConfirmedWithBottler']
        },
        //  Adding Tasks for the UAE Related Tempaltes
         {
            taskNumber: 43, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 44, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 45, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 46, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 47, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Regulatory Affairs', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 48, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
        {
            taskNumber: 49, actualStartDate: true, actualEndDate: true, duration: true, role: ['Corp PM', 'Ops PM', 'E2E PM', 'Programme Manager'], taskOwner: ['Ops PM', 'Programme Manager']
            , show: ['canStart', 'taskOwnerRMUser']
        },
    ],


     //Adding roles using scenarios for the access for the user related tasks
     TaskAccessRolesWithTaskRole :[
        {
            taskOwner:['E2E PM','Programme Manager'],
            role:['Corp PM', 'E2E PM', 'Programme Manager']
        },
        {
            taskOwner:['Corp PM','Programme Manager'],
            role:['E2E PM', 'Corp PM', 'Programme Manager', 'Corp Project Leader']
        },
        {
            taskOwner:['Ops PM','Programme Manager'],
            role:['Corp PM', 'E2E PM', 'Ops PM', 'Programme Manager']
        },
        {
            taskOwner:['RTM','Programme Manager'],
            role:['Corp PM', 'RTM', 'E2E PM', 'Programme Manager']
        },
        {
            taskOwner:['Project Specialist','Programme Manager'],
            role:['Project Specialist', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader']
        },
        {
            taskOwner: ['Regulatory Affairs','Programme Manager'],
            role: ['Regulatory Affairs', 'Corp PM', 'E2E PM', 'Programme Manager', 'Corp Project Leader', 'Regs Traffic Manager']
        },
        {
             taskOwner: ['Secondary AW Specialist','Programme Manager'],
             role: ['Corp PM', 'Secondary AW Specialist', 'E2E PM', 'Programme Manager']
        },
        {
            taskOwner: ['FPMS','Programme Manager'],
            role: ['Corp PM','E2E PM','Programme Manager']
       }
    ],


    allTaskName: [{
        task01: 'TASK 01',
        task02: 'TASK 02',
        task03: 'TASK 03',
        task04: 'TASK 04',
        task05: 'TASK 05',
        task06: 'TASK 06',
        task07: 'TASK 07',
        task08: 'TASK 08',
        task09: 'TASK 09',
        task10: 'TASK 10',
        task11: 'TASK 11',
        task12: 'TASK 12',
        task13: 'TASK 13',
        task13A: 'TASK 13A',
        task14: 'TASK 14',
        task15: 'TASK 15',
        task16: 'TASK 16',
        task17: 'TASK 17',
        task18: 'TASK 18',
        task19: 'TASK 19',
        task20: 'TASK 20',
        task21: 'TASK 21',
        task22: 'TASK 22',
        task23: 'TASK 23',
        task24: 'TASK 24',
        task25: 'TASK 25',
        task26: 'TASK 26',
        task27: 'TASK 27',
        task28: 'TASK 28',
        task29: 'TASK 29',
        task30: 'TASK 30',
        task31: 'TASK 31',
        task32: 'TASK 32',
        task33: 'TASK 33',
        task34: 'TASK 34',
        task35: 'TASK 35',
        task36: 'TASK 36',
        task37: 'TASK 37',
        task38: 'TASK 38',
        task39: 'TASK 39',
        task40: 'TASK 20',
        task41: 'TASK 41',
        task42: 'TASK 42',
        taskCP1: 'CP1',
        taskCP2: 'CP2',
        taskCP3: 'CP3',
        taskCP4: 'CP4',
        //Adding tasks related to UAE Templates
        // task43: 'TASK 43',
        // task44: 'TASK 44',
        // task45: 'TASK 45',
        // task46: 'TASK 46',
        // task47: 'TASK 47',
        // task48: 'TASK 48',
        // task49: 'TASK 49'
    }],

    nonClassficationTaskNames: ["TASK 02", "TASK 03", "TASK 04", "TASK 05", "TASK 06"],


   

}
