import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { ProjectConstant, MPM_LEVELS, TaskConstants, AppService, CommentsUtilService, EntityAppDefService, LoaderService, NotificationService, ProjectUtilService, SharingService, TaskService, UtilService } from 'mpm-library';
import { startWith, map, distinctUntilChanged } from 'rxjs/operators';
import { MPMTaskCreationComponent } from 'src/app/core/mpm-lib/project/tasks/task-creation/task-creation.component';
import { NpdTaskService } from '../services/npd-task.service';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { MonsterUtilService } from '../../services/monster-util.service';
import { Observable } from 'rxjs';
import { RequestService } from '../../services/request.service';
import { NpdProjectService } from '../../npd-project-view/services/npd-project.service';
// import { NPDTaskUpdate } from '../constants/custom-npd-task-update';
@Component({
  selector: 'app-npd-task-creation',
  templateUrl: './npd-task-creation.component.html',
  styleUrls: ['./npd-task-creation.component.scss']
})
export class NpdTaskCreationComponent extends MPMTaskCreationComponent {
    show = {
        startDate: false,
        endDate: false,
    };

    weekYear = {
        startDate: null,
        endDate: null,
    };

    fromDate = {
        startDate: null,
        endDate: null,
    };

    toDate = {
        startDate: null,
        endDate: null,
    };
    disable = {
        startDate: false,
        endDate: true,
    };
    // filterUserListData: Observable<any[]>;
    // rmRoleUsers = [];
    // rmRoles = []
    // selectedRMUser;
    // parameter :NPDTaskUpdate ={
    //     'NPD_Task-create':{
    //         Task_Id:'',
    //         Task_Item_Id:'',
    //         MPMProjectID:''
    //     }
    // }
        
    

    
  constructor(
    public appService: AppService,
    public sharingService: SharingService,
    public adapter: DateAdapter<any>,
    public utilService: UtilService,
    public taskService: TaskService,
    public entityAppdefService: EntityAppDefService,
    public loaderService: LoaderService,
    public dialog: MatDialog,
    public projectUtilService: ProjectUtilService,
    public commentsUtilService: CommentsUtilService,
    public notificationService: NotificationService,
    public monsterUtilService: MonsterUtilService,
    public npdTaskService:NpdTaskService,
    public requestService: RequestService,
    public npdProjectService:NpdProjectService,
    
    ) {

      super(appService,sharingService,adapter,utilService,taskService,entityAppdefService,loaderService,dialog,
          projectUtilService,commentsUtilService,notificationService)
  }

   validateDuration() {
        if (this.taskForm.controls.duration && this.taskForm.controls.duration.value) {
            if (this.taskForm.controls.duration.value < 1 || this.taskForm.controls.duration.value > 999) {
                this.durationErrorMessage = this.taskConstants.DURATION_MIN_ERROR_MESSAGE;
            } else {
                // let endDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskForm.controls.startDate.value), this.taskForm.controls.durationType.value === 'weeks' ? (this.taskForm.controls.duration.value) : (this.taskForm.controls.duration.value - 1), this.taskForm.controls.durationType.value, this.enableWorkWeek);
                // let endDateActual = this.npdTaskService.subTractUnitsToDate(endDateObject,1,'days');
                const [startDate] = this.getStartEndDate(
                    this.taskForm.controls.startDate.value
                  );

                  let endDateObject = this.commentsUtilService.addUnitsToDate(
                    startDate,
                    this.taskForm.controls.durationType.value === 'weeks'
                      ? this.taskForm.controls.duration.value
                      : this.taskForm.controls.duration.value - 1,
                    this.taskForm.controls.durationType.value,
                    this.enableWorkWeek
                  );
                  let endDateActual = this.npdTaskService.subTractUnitsToDate(
                    endDateObject,
                    1,
                    'days'
                  );
                  const endDateWeek =
                  this.monsterUtilService.calculateTaskWeek(endDateActual); 
                this.taskForm.controls.endDate.setValue(endDateWeek);
                this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(1)]);
                this.taskForm.controls.duration.updateValueAndValidity();
                if (this.taskModalConfig && this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) {
                    if (new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) >= new Date(this.taskForm.controls.endDate.value)) {
                        const minDuration = this.commentsUtilService.getDaysBetweenTwoDates(new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE),
                            new Date(this.taskForm.controls.endDate.value), this.taskForm.controls.durationType.value);
                        this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(minDuration + 1)]);
                        this.taskForm.controls.duration.updateValueAndValidity();
                        this.durationErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                    }
                }
            }
        }
    }

    startDateFilter: (date: Date | null) => boolean =
    (date: Date | null) => {
        const day = date.getDay();
        return day == 1;//day !== 0 && day !== 6;
        //0 means sunday
        //6 means saturday
    }

  initialiseTaskForm() {
    // this.rmRoles = JSON.parse(
    //     sessionStorage.getItem('ALL_RM_ROLE_ALLOCATION')
    //   )
    const disableField = false;
    this.taskForm = null;
    this.taskModalConfig.minDate = this.taskModalConfig.selectedProject.START_DATE;
    this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
    this.taskModalConfig.taskRoleConfig.filterOptions.map(eachRole => {
        if(eachRole.displayName == 'Secondary AW Specialist'){
            eachRole.displayName = 'Secondary AW Manager';
        }
    })
    const pattern = ProjectConstant.NAME_STRING_PATTERN;
    if (!this.taskModalConfig.isTemplate) {
        if (this.taskModalConfig.isNewTask) {
            this.taskStatus = this.taskModalConfig.taskInitialStatusList;
            const endDateWeekYearValue =
                this.monsterUtilService.calculateTaskWeek(this.taskModalConfig.selectedProject.DUE_DATE);
            let durationType = this.DurationType?.find(duration => duration.value == "weeks")?.value
            this.taskForm = new FormGroup({
                taskName: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                duration: new FormControl({ value: '', disabled: (disableField || !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({ value: durationType, disabled: (disableField || !this.enableDuration) }),
                //this.DurationType[0].value
                status: new FormControl({
                    value: this.taskModalConfig.taskInitialStatusList && this.taskModalConfig.taskInitialStatusList[0] &&
                        this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'] ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'].Id : '', disabled: true
                }
                ),
                owner: new FormControl({ value: '', disabled: disableField }),
                role: new FormControl({ value: '', disabled: disableField }),
                priority: new FormControl({
                    value: this.taskModalConfig.taskPriorityList && this.taskModalConfig.taskPriorityList[0] &&
                        this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'] ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                }, [Validators.required]),
                startDate: new FormControl({
                    value: null, disabled: disableField
                }, [Validators.required]),
                
                endDate: new FormControl({
                    value: endDateWeekYearValue, disabled: (disableField || this.enableDuration)
                }, [Validators.required]),
                description: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.required, Validators.maxLength(1000)]),
                revisionReviewRequired: new FormControl({ value: this.taskConfig.isApprovalTask ? this.defaultRevisionRequiredStatus : '', disabled: disableField }),
                isMilestone: new FormControl({ value: false, disabled: disableField }),
                viewOtherComments: new FormControl({ value: '', disabled: disableField }),
                predecessor: new FormControl({ value: '', disabled: disableField }),
                deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                allocation: new FormControl({ value: '', disabled: disableField }),
                // rmRole: new FormControl({
                //     value: ''
                    
                //   }),
                //   rmUser: new FormControl({
                //     value: ''
                //   }),
                //   rmUserId: new FormControl(),
                //   rmUserCN: new FormControl(),
            });
            this.taskForm.updateValueAndValidity();
        } else if (this.taskModalConfig.selectedTask) {
            this.taskStatus = this.projectUtilService.filterStatusOptions(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList, MPM_LEVELS.TASK);
            this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList);
            // if (selectedStatusInfo && (selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.FINAL)) {
            // this.overViewConfig.isReadOnly = true;
            // disableFields = true;
            // this.disableStatusOptions = true;
            // }
            const task = this.taskModalConfig.selectedTask;
            const taskField = this.getTaskFields(task);
            const taskRoleId = task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'] ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
            // const taskOwnerId = task.R_PO_OWNER_ID && task.R_PO_OWNER_ID['Identity-id'] ? task.R_PO_OWNER_ID['Identity-id'].Id : '';
            task.predecessor = (task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object') ? task.PARENT_TASK_ID : null;
            this.taskForm = new FormGroup({
                taskName: new FormControl({ value: task.NAME, disabled: disableField },
                    [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                duration: new FormControl({
                    value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                    disabled: (disableField || !this.enableDuration)
                }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({
                    value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                    disabled: (disableField || !this.enableDuration)
                }),
                status: new FormControl({
                    value: task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id'] ? task.R_PO_STATUS['MPM_Status-id'].Id : '',
                    disabled: this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL
                }),
                owner: new FormControl({
                    value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
                    disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                role: new FormControl({
                    value: this.formRoleValue(taskRoleId), disabled: disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                priority: new FormControl({
                    value: (task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id'] ? task.R_PO_PRIORITY['MPM_Priority-id'].Id : ''),
                    disabled: disableField
                }, [Validators.required]),
                startDate: new FormControl({
                    value: this.commentsUtilService.addUnitsToDate(new Date(task.START_DATE), 0, 'days', this.enableWorkWeek), disabled: (disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL))
                }, [Validators.required]),
                endDate: new FormControl({ value: task.DUE_DATE, disabled: (disableField || this.enableDuration) }, [Validators.required]),
                description: new FormControl({ value: this.utilService.isNullOrEmpty(task.DESCRIPTION) ? '' : task.DESCRIPTION, disabled: disableField }, [Validators.maxLength(1000)]),
                revisionReviewRequired: new FormControl({ value: this.utilService.getBooleanValue(task.REVISION_REVIEW_REQUIRED), disabled: true }),
                isMilestone: new FormControl({ value: this.utilService.getBooleanValue(task.IS_MILESTONE), disabled: disableField }),
                viewOtherComments: new FormControl({ value: taskField.showOtherComments, disabled: disableField }),
                predecessor: new FormControl(this.formPredecessor(task.predecessor)),
                // disabled: taskField.isApprovalTaskSelected || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL
                deliverableApprover: new FormControl({
                    value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
                    disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                deliverableReviewers: new FormControl({ value: this.savedReviewers }),
                allocation: new FormControl({
                    value: task.ASSIGNMENT_TYPE, disabled: disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                // rmRole: new FormControl({
                //     value: ''                    
                //   }),
                //   rmUser: new FormControl({
                //     value: ''
                //   }),
                //   rmUserId: new FormControl(),
                //   rmUserCN: new FormControl(),
            });
            this.taskModalConfig.selectedpredecessor = this.getPredecessortask(task.predecessor);
            this.taskForm.controls.predecessor.disable();
            if (this.taskModalConfig.selectedpredecessor) {
                const minDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek);
                this.taskModalConfig.minDate = minDateObject;
            }
            this.getTaskOwnerAssignmentCount();
            // this.onTaskSelection();
            this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
            this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject
                (this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers);
            this.taskForm.updateValueAndValidity();
        }
    } else {
        if (this.taskModalConfig.isNewTask) {
            this.taskStatus = this.taskModalConfig.taskInitialStatusList;
            this.taskForm = new FormGroup({
                taskName: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                // 205
                /*   duration: new FormControl({ value: '', disabled: (disableField || !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                  durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || !this.enableDuration) }),
                  */
                duration: new FormControl({ value: '', disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration) }),
                status: new FormControl(this.taskModalConfig.taskInitialStatusList && this.taskModalConfig.taskInitialStatusList[0] &&
                    this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'] ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'].Id : ''),
                owner: new FormControl({ value: '', disabled: disableField }),
                role: new FormControl({ value: '', disabled: disableField }),
                priority: new FormControl({
                    value: this.taskModalConfig.taskPriorityList && this.taskModalConfig.taskPriorityList[0] &&
                        this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'] ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                }, [Validators.required]),
                description: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(1000)]),
                revisionReviewRequired: new FormControl({ value: this.taskConfig.isApprovalTask ? this.defaultRevisionRequiredStatus : '', disabled: disableField }),
                isMilestone: new FormControl({ value: false, disabled: disableField }),
                viewOtherComments: new FormControl({ value: '', disabled: disableField }),
                predecessor: new FormControl({ value: '', disabled: disableField }),
                deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                allocation: new FormControl({ value: '', disabled: disableField }),
                // rmRole: new FormControl({
                //     value: ''
                //   }),
                //   rmUser: new FormControl({
                //     value: ''
                //   }),
                //   rmUserId: new FormControl(),
                //   rmUserCN: new FormControl(),
            });
        } else if (this.taskModalConfig.selectedTask) {
            this.taskStatus = this.projectUtilService.filterStatusOptions(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList, MPM_LEVELS.TASK);
            this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList);

            const task = this.taskModalConfig.selectedTask;
            const taskField = this.getTaskFields(task);
            const taskRoleId = task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'] ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
            task.predecessor = (task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object') ? task.PARENT_TASK_ID : null;
            this.taskForm = new FormGroup({
                taskName: new FormControl({ value: task.NAME, disabled: disableField },
                    [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                // 205
                /* duration: new FormControl({
                    value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                    disabled: (disableField || !this.enableDuration)
                }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({
                    value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                    disabled: (disableField || !this.enableDuration)
                }), */
                duration: new FormControl({
                    value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                    disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration)
                }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({
                    value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                    disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration)
                }),
                status: new FormControl({
                    value: task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id'] ? task.R_PO_STATUS['MPM_Status-id'].Id : '',
                    disabled: true
                }),
                owner: new FormControl({
                    value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
                    disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                role: new FormControl({
                    value: this.formRoleValue(taskRoleId), disabled: disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                priority: new FormControl({
                    value: (task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id'] ? task.R_PO_PRIORITY['MPM_Priority-id'].Id : ''),
                    disabled: disableField
                }, [Validators.required]),
                description: new FormControl({ value: this.utilService.isNullOrEmpty(task.DESCRIPTION) ? '' : task.DESCRIPTION, disabled: disableField }, [Validators.maxLength(1000)]),
                revisionReviewRequired: new FormControl({ value: this.utilService.getBooleanValue(task.REVISION_REVIEW_REQUIRED), disabled: true }),
                isMilestone: new FormControl({ value: this.utilService.getBooleanValue(task.IS_MILESTONE), disabled: disableField }),
                viewOtherComments: new FormControl({ value: taskField.showOtherComments, disabled: disableField }),
                predecessor: new FormControl(this.formPredecessor(task.predecessor)),
                deliverableApprover: new FormControl({
                    value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
                    disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                deliverableReviewers: new FormControl({ value: this.savedReviewers }),
                allocation: new FormControl({
                    value: task.ASSIGNMENT_TYPE, disabled: disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                })

            });
            this.taskModalConfig.selectedpredecessor = this.getPredecessortask(task.predecessor);
            this.taskForm.controls.predecessor.disable();
            if (this.taskModalConfig.selectedpredecessor) {
                const minDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek);
                this.taskModalConfig.minDate = minDateObject;
            }
            this.getTaskOwnerAssignmentCount();
            // this.onTaskSelection();
            this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
            this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject
                (this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers);
        }
    }

    this.filteredReviewers = this.taskForm.get('deliverableReviewers').valueChanges.pipe(
        startWith(''),
        map((reviewer: string | null) => reviewer ? this.filterReviewers(reviewer) : this.allReviewers.slice()));

    this.taskModalConfig.selectedStatusItemId = this.taskForm.value.status;
    // this.taskForm.controls.status.disable();
    this.taskModalConfig.predecessorConfig.formControl = this.taskForm.controls.predecessor;
    this.taskModalConfig.taskOwnerConfig.formControl = this.taskForm.controls.owner;
    this.taskModalConfig.taskRoleConfig.formControl = this.taskForm.controls.role;
    this.taskModalConfig.deliverableApprover.formControl = this.taskForm.controls.deliverableApprover;
    this.taskModalConfig.deliverableReviewer.formControl = this.taskForm.controls.deliverableReviewer;
    this.taskModalConfig.deliveryPackageConfig.formControl = this.taskForm.controls.predecessor;
    if (this.taskForm.controls.allocation.value === TaskConstants.ALLOCATION_TYPE_ROLE) {
        if (this.taskForm.controls.owner && !this.taskModalConfig.selectedTask) {
            this.taskForm.controls.owner.disable();
        }
        if (this.taskForm.controls.deliverableApprover && !this.taskModalConfig.selectedTask) {
            this.taskForm.controls.deliverableApprover.disable();
        }
        if (this.taskForm.controls.deliverableReviewer && !this.taskModalConfig.selectedTask) {
            this.taskForm.controls.deliverableReviewer.disable();
        }
        /* if (this.taskForm.controls.role.value !== '') {
            this.getUsersByRole().subscribe(() => {
                this.taskModalConfig.taskOwnerConfig.filterOptions = this.teamRoleUserOptions;
                this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
            });
        } */
    }

    this.taskForm.controls.predecessor.valueChanges.subscribe(predecessor => {
        // TODO predecessor change method
        if (!this.taskModalConfig.selectedTask && typeof predecessor === 'object' && predecessor && predecessor.value) {
            this.taskModalConfig.taskList.map(task => {
                if (task['Task-id'].Id === predecessor.value) {
                    /*if (task.REVISION_REVIEW_REQUIRED) {
                        this.taskForm.controls.revisionReviewRequired.patchValue(true);
                    }*/
                    const minDateObject = this.commentsUtilService.addUnitsToDate(new Date(task.DUE_DATE), 1, 'days', this.enableWorkWeek);
                    this.taskModalConfig.minDate = minDateObject;
                    this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
                    if (!this.taskModalConfig.isTemplate) {
                        this.taskForm.controls.startDate.setValue(minDateObject);
                        this.taskForm.controls.endDate.setValue(this.taskModalConfig.selectedProject.DUE_DATE);
                    }
                }
            });
        } else {
            this.taskModalConfig.minDate = this.taskModalConfig.selectedpredecessor ?
                this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek) : this.taskModalConfig.selectedProject.START_DATE;
            this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
            let selectedPredecessor;
            if (this.taskConfig.isApprovalTask) {
                selectedPredecessor = this.taskModalConfig.deliveryPackageConfig.filterOptions.find(predecessorValue => predecessorValue.displayName === predecessor);
            } else {
                selectedPredecessor = this.taskModalConfig.predecessorConfig.filterOptions.find(predecessorValue => predecessorValue.displayName === predecessor);
            }
            if (selectedPredecessor) {
                this.taskForm.controls.predecessor.setValue(selectedPredecessor);
            }
            // if (!this.taskModalConfig.isTemplate) {
            //     this.taskForm.controls.startDate.setValue(this.taskModalConfig.selectedProject.START_DATE);
            //     this.taskForm.controls.endDate.setValue(this.taskModalConfig.selectedProject.DUE_DATE);
            // }
        }
    });

    if (this.taskForm.controls.owner) {
        this.taskForm.controls.owner.valueChanges.subscribe(ownerValue => {
            if (ownerValue && typeof ownerValue === 'object' && !this.taskModalConfig.isTemplate) {
                this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                    if (taskOwnerName.value === ownerValue.value) {
                        this.getTaskOwnerAssignmentCount();
                    }
                });
            } else {
                this.ownerProjectCountMessage = '';
                const selectedOwner = this.taskModalConfig.taskOwnerConfig.filterOptions.find(owner => owner.displayName === ownerValue);
                if (selectedOwner) {
                    this.taskForm.controls.owner.setValue(selectedOwner);
                }
            }
        });
    }
    this.taskForm.controls.allocation.valueChanges.subscribe(allocationTypeValue => {
        if (this.taskForm.controls.role) {
            this.taskForm.controls.role.setValue('');
        }
        if (this.taskForm.controls.owner) {
            this.taskForm.controls.owner.setValue('');
        }
        if (this.taskForm.controls.deliverableApprover) {
            this.taskForm.controls.deliverableApprover.setValue('');
        }
        if (this.taskForm.controls.deliverableReviewers) {
            this.taskForm.controls.deliverableReviewers.setValue('');
        }
        this.onChangeofAllocationType(allocationTypeValue, null, null, true);
    });


    this.taskForm.controls.role.valueChanges.subscribe(role => {
        if (role && typeof role === 'object') {
            this.onChangeofRole(role);
        } else {
            const selectedRole = this.taskModalConfig.taskRoleConfig.filterOptions.find(taskRole => taskRole.displayName === role);
            if (selectedRole) {
                this.taskForm.controls.role.setValue(selectedRole);
            }
        }
    });

    if (this.taskForm.controls.startDate) {
        this.taskForm.controls.startDate.valueChanges.subscribe(() => {
            if (this.taskForm.value.owner && this.taskForm.value.owner.value) {
                this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                    if (taskOwnerName.value === this.taskForm.value.owner.value) {
                        this.getTaskOwnerAssignmentCount();
                    }
                });
            } else {
                this.ownerProjectCountMessage = '';
            }
            if (this.dateValidation && this.dateValidation.startMaxDate && new Date(this.taskForm.controls.startDate.value) > new Date(this.dateValidation.startMaxDate)) {
                this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            } else {
                this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
            }

            if (new Date(this.taskForm.controls.endDate.value) > new Date(this.taskModalConfig.selectedProject.DUE_DATE)) {
                this.durationErrorMessage = this.taskConstants.DURATION_ERROR_MESSAGE;
            }

            if (this.enableDuration) {
                let weekDate = this.taskForm.controls.startDate.value.split("/");
                if(weekDate[1]?.length == 4){
                    this.validateDuration();
                }
            }

            if (this.dateValidation && this.dateValidation.startMaxDate && new Date(this.taskForm.controls.startDate.value) > new Date(this.dateValidation.startMaxDate)) {
                this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            } else {
                this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
            }


        });




        this.taskForm.controls.endDate.valueChanges.subscribe(() => {
            if (this.taskForm.value.owner && this.taskForm.value.owner.value) {
                this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                    if (taskOwnerName.value === this.taskForm.value.owner.value) {
                        this.getTaskOwnerAssignmentCount();
                    }
                });
            } else {
                this.ownerProjectCountMessage = '';
            }
            if (this.dateValidation && this.dateValidation.endMinDate && new Date(this.taskForm.controls.endDate.value) < new Date(this.dateValidation.endMinDate)) {
                this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            } else if (new Date(this.taskForm.controls.endDate.value) < new Date(this.taskForm.controls.startDate.value)) {
                this.dateErrorMessage = this.taskConstants.START_DATE_ERROR_MESSAGE;
            } else {
                this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
            }



        });
    }
    if (this.enableDuration) {
        this.taskForm.controls.duration.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
            if (this.taskForm.controls.startDate && this.taskForm.controls.startDate.value && this.taskForm.controls.duration && this.taskForm.controls.duration.value) {

                this.validateDuration();
            }
        });
        this.taskForm.controls.durationType.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
            if (this.taskForm.controls.startDate && this.taskForm.controls.startDate.value && this.taskForm.controls.duration && this.taskForm.controls.duration.value) {

                this.validateDuration();
            }
        });
    }


    this.taskModalConfig.hasAllConfig = true;
}
changeTargetDP(weekFormat, date) {
    this.weekYear[date] = weekFormat;
    if (date == 'startDate') {
      this.taskForm.patchValue({ startDate: weekFormat });
      //this.taskForm.patchValue({endDate: `${+weekFormat.split("/")[0] + +this.taskForm?.controls?.duration?.value}/${weekFormat.split("/")[1]}`});
      /*   else if(date == "endDate"){
        this.taskForm.patchValue({endDate: weekFormat});
        }  */
    } else {
      this.taskForm.patchValue({ [date]: weekFormat });
      this.taskForm.controls[date].updateValueAndValidity();
    }
    this.taskForm.markAsDirty();
    [this.fromDate[date], this.toDate[date]] = this.getWeekFormat(weekFormat);
  }

  getWeekFormat(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates: NgbDate[] = this.calculateDateNgb(week, year);
      return [weekdates[0], weekdates[1]];
    }
  }

  calculateDateNgb(week: number, year: number): NgbDate[] {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate, ngbToDate];
  }
    getStopPropogation(event, date) {
    if (this.show[date]) {
      return event.stopPropagation();
    }
  }

  getBool(date) {
    this.show[date] = !this.show[date];
  }

  getAllBool() {
    Object.keys(this.show).map((ele) => {
      this.show[ele] = false;
    });
  }

  getStartEndDate(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates = this.calculateDate(week, year);
      return [weekdates[0], weekdates[1]];
    }
  }

  calculateDate(week: number, year: number) {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    return [fromDate, toDate];
  }

  updateTask(saveOption) {
    if (this.taskForm.getRawValue().deliverableApprover && this.taskForm.getRawValue().deliverableApprover.value) {
        const data = this.savedReviewers.find(reviewer => reviewer.value === this.taskForm.getRawValue().deliverableApprover.value);
        if (data) {
            const eventData = { message: 'Selected reviewer is already the approver', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            this.notificationHandler.next(eventData);
            this.disableTaskSave = false;
            return;
        }
    }
    if (this.taskForm.pristine && !(this.chipChanged)) {
        const eventData = { message: 'Kindly make changes to update the task', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
        this.notificationHandler.next(eventData);
        this.disableTaskSave = false;
        return;
    }
    const nameSplit = this.taskForm.value.taskName.split('_');
    if (nameSplit[0].length === 0) {
        const eventData = { message: 'Name should not start with "_"', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
        this.notificationHandler.next(eventData);
        return;
    }
    this.disableTaskSave = true;
    const taskEndDateWarning = false;
    if (taskEndDateWarning) {
        this.taskForm.patchValue({
            endDate: this.taskModalConfig.selectedTask.DUE_DATE
        });
        const eventData = { message: 'Task\'s end date should not be less than the deliverables\' due date', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
        this.notificationHandler.next(eventData);
        this.disableTaskSave = false;
        return;
    }
    if (this.taskForm.status === 'VALID') {
        if (this.taskForm.controls.taskName.value.trim() === '') {
            const eventData = { message: 'Please provide a valid task name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
            this.notificationHandler.next(eventData);
            this.disableTaskSave = false;
        }
        else {
            this.taskForm.controls.taskName.setValue(this.taskForm.controls.taskName.value.trim());
            this.disableTaskSave = true;
            this.updateTaskDetails(saveOption);
        }
    }
    else {
        const eventData = { message: 'Kindly fill all mandatory fields', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
        this.notificationHandler.next(eventData);
        this.disableTaskSave = false;
    }
}

getIsoFormatDateString(dateObject){
    const newDate = new Date(dateObject);
    const year = newDate.getFullYear();
    const actualMonth = newDate.getMonth() + 1;
    const month = actualMonth.toString().length <= 1 ? '0'+actualMonth : actualMonth;
    const date = newDate.getDate().toString().length <= 1 ? '0'+newDate.getDate() : newDate.getDate();
    return year + '-' + month + '-' + date + "T00:00:00.000Z"
  }

updateTaskDetails(saveOption) {
    const taskFormValues = this.taskForm.getRawValue();
    if (taskFormValues?.startDate != '') {
        [taskFormValues.startDate] = this.getStartEndDate(taskFormValues.startDate
        );
      }
      if (taskFormValues?.endDate != '') {
        [, taskFormValues.endDate] = this.getStartEndDate(taskFormValues.endDate);
      }
    this.formReviewerObj();
    const TaskObject = this.projectUtilService.createNewTaskUpdateObject(taskFormValues, this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.selectedReviewersObj);
    TaskObject.DueDate = this.getIsoFormatDateString(TaskObject.DueDate);
    TaskObject.StartDate = this.getIsoFormatDateString(TaskObject.StartDate);
    TaskObject.TaskName='~'+TaskObject.TaskName;
    if (this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask['Task-id']
        && this.taskModalConfig.selectedTask['Task-id'].Id) {
        TaskObject.TaskId.Id = this.taskModalConfig.selectedTask['Task-id'].Id;
        TaskObject.RevisionReviewRequired = this.utilService.isNullOrEmpty(this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED)
            ? '' : this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED;
        TaskObject.IsMilestone = this.utilService.isNullOrEmpty(this.taskModalConfig.selectedTask.IS_MILESTONE)
            ? '' : this.taskModalConfig.selectedTask.IS_MILESTONE;
        TaskObject.IsMilestone = this.utilService.isNullOrEmpty(taskFormValues.isMilestone)
            ? '' : taskFormValues.isMilestone;
    }
    this.saveTask(TaskObject, saveOption);    
}

// saveTaskMPM(TaskObject, saveOption) {
//     this.loaderService.show();
//     if (this.taskModalConfig.selectedTask) {
//         TaskObject = this.projectUtilService.compareTwoProjectDetails(TaskObject, this.oldTaskObject);
//         if (Object.keys(TaskObject).length > 1 || (JSON.stringify(this.deliverableReviewers) !== JSON.stringify(this.savedReviewers))) {
//             TaskObject.BulkOperation = false;
//             const requestObj = {
//                 TaskObject: TaskObject,
//                 DeliverableReviewers: {
//                     DeliverableReviewer: this.reviewersObj
//                 }
//             };
//             this.taskService.updateTask(requestObj).subscribe(response => {
//                 this.loaderService.hide();
//                 this.disableTaskSave = false;
//                 if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
//                     const eventData = { message: 'Task updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
//                     this.notificationHandler.next(eventData);
//                     this.saveCallBackHandler.next({
//                         taskId: response.APIResponse.data.Task['Task-id'].Id, projectId: this.taskModalConfig.projectId,
//                         isTemplate: this.taskModalConfig.isTemplate, saveOption: saveOption
//                     });
                    
//                 } else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
//                     && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
//                     const eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
//                     this.notificationHandler.next(eventData);
//                 } else {
//                     const eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
//                     this.notificationHandler.next(eventData);
//                 }
//             }, error => {
//                 const eventData = { message: 'Something went wrong while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
//                 this.notificationHandler.next(eventData);
//             });
//         } else {
//             this.loaderService.hide();
//             this.disableTaskSave = false;
//         }
//     } else {
//         const requestObj = {
//             TaskObject: TaskObject
//         };
//         this.taskService.createTask(requestObj)
//             .subscribe(response => {
//                 this.loaderService.hide();
//                 this.disableTaskSave = false;
//                 if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
//                     && response.APIResponse.data.Task && response.APIResponse.data.Task['Task-id'] && response.APIResponse.data.Task['Task-id'].Id) {
//                     const eventData = { message: 'Task saved successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
//                     this.notificationHandler.next(eventData);
//                     this.disableTaskSave = false;
//                     this.saveCallBackHandler.next({
//                         taskId: response.APIResponse.data.Task['Task-id'].Id, projectId: this.taskModalConfig.projectId,
//                         isTemplate: this.taskModalConfig.isTemplate, saveOption: saveOption
//                     });
//                     this.parameter['NPD_Task-create'].Task_Id = response.APIResponse.data.Task['Task-id'].Id;
//                     this.parameter['NPD_Task-create'].Task_Item_Id = response.APIResponse.data.Task['Task-id'].ItemId;
//                     this.parameter['NPD_Task-create'].MPMProjectID = this.taskModalConfig.projectId
                               
//                     console.log(response.APIResponse.data.Task['Task-id'].Id)
//                     console.log('parameter',this.parameter);
//                     this.npdTaskService.createNPDTask(this.parameter).subscribe()
//                 } else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
//                     && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
//                     const eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
//                     this.notificationHandler.next(eventData);
//                 } else {
//                     const eventData = { message: 'Something went wrong on while creating task', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
//                     this.notificationHandler.next(eventData);
//                 }
//             }, () => {
//                 this.loaderService.hide();
//                 const eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
//                 this.notificationHandler.next(eventData);
//                 this.disableTaskSave = false;
//             });
//     }
// }
//  onRMUserChange(userValue) {

//     if ((userValue != undefined &&  typeof userValue === 'string' && userValue?.trim() === '') || !this.rmRoleUsers.some(e => e.displayName === userValue?.displayName)) {
//             this.taskForm.patchValue({
//                 rmUserId: '',
//                 rmUserCN: ''
//             })
//         }
//   }
//   displayFn(user?: any): string | undefined {
//     return user ? user.displayName || user.name : '';
//   }

  

//   onRMUserSelection(selectedUser) {
//     console.log('this.selectedOwner',this.selectedOwner)
//     this.taskForm.get('rmUser')?.setErrors(null);
//     if (selectedUser?.option?.value) {
//         this.loaderService.show();
//       this.requestService
//         .getUserByID(selectedUser.option.value?.name)
//         .subscribe(
//           (response) => {
            
//             let user = response.result.items[0];
//             this.selectedRMUser.userIdentity = response.result.items[0];
            
//             this.taskForm.patchValue({
//               rmUserId: user?.Identity?.Id,
//               rmUserCN: selectedUser.option.value?.name,
//             });
//             console.log('user?.Identity?.Id',user?.Identity?.Id)
//             this.parameter['NPD_Task-create'].R_PO_RM_USER['Identity-id'].Id = user?.Identity?.Id;
//             this.parameter['NPD_Task-create'].RMUserCN = selectedUser.option.value?.name;
//             this.loaderService.hide();
//           },
//           (error) => {
//             this.loaderService.hide();
//           }
//         );
      
      
      
      
//     }
   
//   }

//   onFocusOut(event) {
//     if (
//       event.target.value.trim() === '' &&
//       (!this.taskForm.get('rmUser') || !this.taskForm.get('rmUser')?.errors)
//     ) {
//       //this.taskForm.get('rmUser').setErrors(null);
//       return;
//     } else {
//       if (this.rmRoleUsers) {
//         if (!this.rmRoleUsers.some((e) => e.displayName === event.target.value)) {
//           this.taskForm.get('rmUser').setErrors({ incorrect: true });
//         } else {
//           this.taskForm.get('rmUser').setErrors(null);
//         }
//       }
//     }
//   }
//   onRMRoleSelection(event):void{
    
//     let jsonString=event.value;
//     let jsonObj = JSON.parse(jsonString);
//     this.parameter['NPD_Task-create'].R_PO_RM_ROLE['Identity-id'].Id =jsonObj.Identity.Id;
//     this.parameter['NPD_Task-create'].RMRoleName =jsonObj.Properties.Name;
//     let roleDn = this.utilService.GetRoleDNByRoleId(this.parameter['NPD_Task-create'].R_PO_RM_ROLE['Identity-id'].Id);
//     if(roleDn == undefined){
//       let roleName = this.parameter['NPD_Task-create'].RMRoleName;
//       if(roleName === 'Corp PM'){
//         roleName='CORP_PM';
//       }
//       else if(roleName === 'Ops PM'){
//         roleName='OPS_PM';
//       }
//       else if(roleName === 'Project Specialist'){
//         roleName='PROJECT_SPECIALIST';
//       }
//       else if(roleName === 'Secondary AW Specialist'){
//         roleName='SECONDARY_PACKAGING'
//       }
//       else if(roleName === 'RTM'){
//         roleName ='REGIONAL_TECHNICAL_MANAGER'
//       }
//        roleDn = this.sharingService.getRoleByName(roleName);
//     }
//     this.loaderService.show();
//     this.npdProjectService
//       .getAllUsersByRole(roleDn?.ROLE_DN)
//       .subscribe(
//         (response) => {
//           this.rmRoleUsers = response;
//           this.loaderService.hide();
          
//         },
//         (error) => {
//           console.log('error',error);
//         }
//       );
//    }
  


  

}
