import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppService, AssetService, DeliverableService, EntityAppDefService, FieldConfigService, FormatToLocalePipe, IndexerService, LoaderService, MPMField, MPMFieldConstants, MPMFieldKeys, MPMSearchOperatorNames, MPMSearchOperators, MPM_LEVELS, NotificationService, OtmmMetadataService, OTMMService, ProjectService, QdsService, SearchRequest, SearchResponse, SharingService, StatusService, TaskConstants, TaskService, UtilService, ViewConfigService } from 'mpm-library';
import { MPMDeliverableCardComponent } from 'src/app/core/mpm-lib/project/tasks/deliverable/deliverable-card/deliverable-card.component';
import { TaskCreationModalComponent } from 'src/app/core/mpm/project-view/task-creation-modal/task-creation-modal.component';
import { CommentsModalComponent } from '../../collab/comments-modal/comments-modal.component';
import { CommentConfig } from '../../collab/objects/CommentConfig';
import { MonsterUtilService } from '../../services/monster-util.service';
import { NpdTaskService } from '../services/npd-task.service';

@Component({
  selector: 'app-npd-deliverable-card',
  templateUrl: './npd-deliverable-card.component.html',
  styleUrls: ['./npd-deliverable-card.component.scss']
})
export class NpdDeliverableCardComponent extends MPMDeliverableCardComponent {
  constructor(
    public dialog: MatDialog,
    public taskService: TaskService,
    public sharingService: SharingService,
    public formatToLocalePipe: FormatToLocalePipe,
    public notificationService: NotificationService,
    public loaderService: LoaderService,
    public appService: AppService,
    public entityAppdefService: EntityAppDefService,
    public otmmMetadataService: OtmmMetadataService,
    public otmmService: OTMMService,
    public assetService: AssetService,
    public utilService: UtilService,
    public projectService: ProjectService,
    public deliverableService: DeliverableService,
    public statusService: StatusService,
    public viewConfigService: ViewConfigService,
    public fieldConfigService: FieldConfigService,
    public qdsService: QdsService,
    public monsterUtilService: MonsterUtilService,
    public npdTaskService: NpdTaskService,
    public indexerService: IndexerService,

  ) {
    super(
      dialog,
      taskService,
      sharingService,
      formatToLocalePipe,
      notificationService,
      loaderService,
      appService,
      entityAppdefService,
      otmmMetadataService,
      otmmService,
      assetService,
      utilService,
      projectService,
      deliverableService,
      statusService,
      viewConfigService,
      fieldConfigService,
      qdsService,
      monsterUtilService
    );
  }

  selectedProject;
  @Input() taskDueDate
  @Input() mapper
  @Input() requestId
  MPMFeildConstants: any;

  top = 100;
  searchConfigId = -1;
  alltask = [];
  editTask(deliverable) {

    this.loaderService.show();
    const editTaskData = {
      TASK_NAME: this.taskData.NAME,
      TASK_DESCRIPTION: this.taskData.DESCRIPTION
    }
    let keyWord = '';
    let sortTemp = [];
    // let test = []
    // let com={}
    //  this.taskConfig.taskTotalCount = null;
    let projectSearch = null;

    let searchConditionList = this.fieldConfigService.formIndexerIdFromMapperId(TaskConstants.GET_ALL_TASKS_SEARCH_CONDITION_LIST.search_condition_list.search_condition);

    this.npdTaskService.setEditTaskHeaderData(editTaskData)
    this.loaderService.show();
    let projectId = deliverable?.PROJECT_ITEM_ID?.split('.')[1];
    let projectItemId = deliverable?.PROJECT_ITEM_ID


    projectSearch = this.fieldConfigService.createConditionObject(
      MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, projectItemId);
    if (projectSearch) {
      searchConditionList.push(projectSearch);
    }

    this.projectService.getProjectById(projectId)
      .subscribe(projectObj => {

        this.loaderService.hide();
        if (projectObj) {
          this.selectedProject = projectObj;
          let deliverableIsActive = (deliverable?.IS_ACTIVE_DELIVERABLE) ? 'true' : 'false';
          // this.getTasks(projectItemId)

          const parameters: SearchRequest = {
            search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
            keyword: keyWord,
            search_condition_list: {
              search_condition: searchConditionList
            },
            facet_condition_list: {
              facet_condition: []
            },
            sorting_list: {
              sort: sortTemp
            },
            cursor: {
              page_index: this.skip,
              page_size: this.top
            }
          };
          this.loaderService.show();
          this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
            const data = [];
            response.data.forEach(element => {
              if (element.IS_BULK_UPDATE_TASK !== 'true') {
                data.push(element);
              }
            });

            this.alltask = data;
            const currTask = this.alltask.find((task => task?.ITEM_ID == deliverable?.TASK_ITEM_ID));
            const taskEstimatedCompletionDate=this.alltask?.find(obj=>obj?.ITEM_ID===deliverable?.TASK_ITEM_ID)?.NPD_TASK_ESTIMATED_COMPLETION_DATE;
            this.taskDueDate={...this.taskDueDate,NPD_TASK_ESTIMATED_COMPLETION_DATE:taskEstimatedCompletionDate}

            const dialogRef = this.dialog.open(TaskCreationModalComponent, {
              // this.getTasks(projectItemId)

              width: '38%',
              disableClose: true,
              data: {
                projectId: projectId,
                isTemplate: false,
                taskId: deliverable?.TASK_ITEM_ID?.split('.')[1],
                projectObj: this.selectedProject,
                isTaskActive: deliverableIsActive,//eventData.data?.IS_ACTIVE_TASK,
                isApprovalTask: false,
                selectedTask: currTask,
                //selectedtask: this.taskDueDate,
                selectedtask: currTask,
                modalLabel: 'Edit Task',
                alltask: this.alltask
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              //if(result){}
              this.refresh();
              // console.log('Task Dialog closed', result);
            });
            /*    this.taskConfig.taskTotalCount = response.cursor.total_records;
               const ownerFeild: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_NAME, MPM_LEVELS.TASK);
               this.taskConfig.taskList.map(task => {
                   const resourceName = this.fieldConfigService.getFieldValueByDisplayColumn(ownerFeild, task);
                   const isExist = this.taskConfig.taskOwnerResources.find(element => element === resourceName);
                   if (!isExist && resourceName) {
                       this.taskConfig.taskOwnerResources.push(resourceName);
                   }
               });
               this.taskGridComponent.refreshChild(this.taskConfig);
               this.loaderService.hide(); */


          }, () => {
            this.loaderService.hide();
          });


        }
      }, (error) => {
        this.loaderService.hide();
      });
  }
  /**
   * @author SathishSrinivas
   * @description Override the comment module used in deliverable component
   * @param event
   */
  openCommentDialog(event) {
    
    const taskId: string = this.getProperty(this.deliverableData, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TASK_ID)?.split('.')[1];
    const projectId = this.getProperty(this.deliverableData, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_PROJECT_ID)?.split('.')[1];
    const taskItemId: string = this.deliverableData.TASK_ITEM_ID.split('.')[1];
    const dialogRef = this.dialog.open(CommentsModalComponent, {
      width: '75%',
      height: '90vh',
      maxHeight: '90vh',
      minHeight: '90vh',
      panelClass: 'comment-dialog-modal',
      disableClose: true,
      data: {
        requestId: this.requestId.PR_REQUEST_ITEM_ID.split('.')[1],
        commentType: CommentConfig.TASK_COMMENT_TYPE_VALUE,
        projectId: projectId,
        taskId: taskItemId,
        enableToComment: true // this.enableToComment
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('comments module dialog is closed \n', result);
    });
  }
}
