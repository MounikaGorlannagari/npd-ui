import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColumnChooserModule, CommentsModule, MpmLibraryModule, TasksModule } from 'mpm-library';
import { NpdTaskCardViewComponent } from './npd-task-card-view/npd-task-card-view.component';
import { NpdTaskListViewComponent } from './npd-task-list-view/npd-task-list-view.component';
import { MaterialModule } from 'src/app/material.module';
import { WeekPickerComponent } from './week-picker/week-picker.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NpdTaskEditComponent } from './npd-task-edit/npd-task-edit.component';
import { MissingTasksComponent } from './missing-tasks/missing-tasks.component';
import { NpdTaskCreationComponent } from './npd-task-creation/npd-task-creation.component';
import { BulkEditTasksComponent } from './bulk-edit-tasks/bulk-edit-tasks.component';
import { CollabModule } from '../collab/collab.module';

@NgModule({
  declarations: [NpdTaskListViewComponent, NpdTaskCardViewComponent,WeekPickerComponent,NpdTaskEditComponent, MissingTasksComponent, NpdTaskCreationComponent, BulkEditTasksComponent],
  imports: [
    CommonModule,
    MpmLibraryModule,
    MaterialModule,
    NgbModule,
    CommentsModule,
    CollabModule
  ],
  exports: [
    NpdTaskListViewComponent,
    NpdTaskCardViewComponent,
    NpdTaskEditComponent,
    NpdTaskCreationComponent,
    WeekPickerComponent,
    BulkEditTasksComponent

  ]
})
export class NpdTaskModule { }
