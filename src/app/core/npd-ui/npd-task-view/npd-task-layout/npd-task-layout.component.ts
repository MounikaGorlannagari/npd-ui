import { I } from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ProjectService, SharingService, TaskService, EntityAppDefService, OTMMService, NotificationService, LoaderService, SearchDataService, SearchChangeEventService, ViewConfigService, FieldConfigService, IndexerService, UtilService, AppService, MPMField, MPMFieldConstants, MPMFieldKeys, MPMSearchOperatorNames, MPMSearchOperators, MPM_LEVELS, SearchRequest, SearchResponse, TaskConstants, ApplicationConfigConstants, SearchConfigConstants, ViewConfig } from 'mpm-library';
import { Observable } from 'rxjs';
import { TaskCreationModalComponent } from 'src/app/core/mpm/project-view/task-creation-modal/task-creation-modal.component';
import { TaskLayoutComponent } from 'src/app/core/mpm/project-view/task/task-layout/task-layout.component';
import { RouteService } from 'src/app/core/mpm/route.service';
import { CustomWorkflowModalComponent } from 'src/app/core/mpm/shared/components/custom-workflow-modal/custom-workflow-modal.component';
import { NpdCustomWorkflowModalComponent } from '../../npd-shared/npd-custom-workflow-modal/npd-custom-workflow-modal.component';
import { NpdTaskService } from '../services/npd-task.service';
import { sequence } from '@angular/animations';
import { NpdProjectService } from '../../npd-project-view/services/npd-project.service';

@Component({
    selector: 'app-npd-task-layout',
    templateUrl: './npd-task-layout.component.html',
    styleUrls: ['./npd-task-layout.component.scss']
})
export class NpdTaskLayoutComponent extends TaskLayoutComponent {

    constructor(
        public projectService: ProjectService,
        public sharingService: SharingService,
        public taskService: TaskService,
        public entityAppDefService: EntityAppDefService,
        public otmmService: OTMMService,
        public notificationService: NotificationService,
        public dialog: MatDialog,
        public loaderService: LoaderService,
        public searchDataService: SearchDataService,
        public searchChangeEventService: SearchChangeEventService,
        public activatedRoute: ActivatedRoute,
        public routeService: RouteService,
        public viewConfigService: ViewConfigService,
        public fieldConfigService: FieldConfigService,
        public indexerService: IndexerService,
        public utilService: UtilService,
        public appService: AppService,
        public npdTaskService: NpdTaskService,
        public npdProjectService:NpdProjectService
    ) {
        super(projectService, sharingService, taskService, entityAppDefService, otmmService, notificationService, dialog,
            loaderService, searchDataService, searchChangeEventService, activatedRoute, routeService, viewConfigService, fieldConfigService,
            indexerService, utilService, appService);
    }

    getMPMFields(): Observable<any> {
        let viewConfig;
        return new Observable(observer => {
            this.loaderService.show();
            if (this.taskConfig && this.taskConfig.taskViewName) {
                viewConfig = this.taskConfig.taskViewName; // this.sharingService.getViewConfigById(this.taskConfig.taskViewId).VIEW;
            } else {
                viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
            }
            this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails: ViewConfig) => {
                if (!this.taskConfig.viewConfig) {
                    this.taskConfig.viewConfig = viewDetails;
                    this.updateSearchConfigId();
                }
                this.taskConfig.viewConfig = viewDetails;
                // this.taskConfig.viewConfig.displayFields = this.taskConfig.isTaskListView ? viewDetails.R_PM_LIST_VIEW_MPM_FIELDS : viewDetails.R_PM_CARD_VIEW_MPM_FIELDS;
                this.taskConfig.viewConfig.displayFields = viewDetails.R_PM_LIST_VIEW_MPM_FIELDS;
                this.taskConfig.metaDataFields = this.fieldConfigService.getFieldsbyFeildCategoryLevel(MPM_LEVELS.TASK);

                const taskListDataSet = this.npdTaskService.formrequiredDataForLoadView(viewDetails, true);
                const taskCardDataSet = this.fieldConfigService.formrequiredDataForLoadView(viewDetails, false);

                this.setUserPreference();
                const taskListViewArray = viewDetails.R_PM_LIST_VIEW_MPM_FIELDS ? viewDetails.R_PM_LIST_VIEW_MPM_FIELDS : [];
                const taskCardViewArray = viewDetails.R_PM_CARD_VIEW_MPM_FIELDS ? viewDetails.R_PM_CARD_VIEW_MPM_FIELDS : [];
                const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.taskConfig.viewConfig['MPM_View_Config-id'].Id);
                const userPreferenceFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA).data : null;
                this.taskConfig.taskCardSortOptions = taskCardDataSet.SORTABLE_FIELDS;
                this.taskConfig.taskListSortOptions = taskListDataSet.SORTABLE_FIELDS;
                if (this.taskConfig.taskCardSortOptions.length > 0) {
                    let selectedSort;
                    if (this.selectedSortByParam && !this.taskConfig.isTaskListView) {
                        selectedSort = this.taskConfig.taskCardSortOptions.find(sort => sort.displayName === this.selectedSortByParam);
                    }
                    if (selectedSort) {
                        this.taskConfig.selectedTaskCardSortOption = {
                            option: selectedSort,
                            sortType: this.selectedSortOrderParam ? this.selectedSortOrderParam :
                                (this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc')
                        };
                        this.taskConfig.taskCardSortFieldName = this.taskConfig.selectedTaskCardSortOption.option.name;
                        this.taskConfig.taskCardSortOrder = this.taskConfig.selectedTaskCardSortOption.sortType;
                    } else {
                        if (userPreferenceFieldData && (userPreference.IS_LIST_VIEW === false || userPreference.IS_LIST_VIEW === 'false')) {
                            userPreferenceFieldData.forEach(field => {
                                const fieldId = Object.keys(field)[0];
                                if (field[fieldId].IS_SORTABLE === true) {
                                    const selectedSortOption = this.taskConfig.taskCardSortOptions.find(field => field.indexerId === fieldId);
                                    if (selectedSortOption) {
                                        this.taskConfig.selectedTaskCardSortOption = {
                                            option: selectedSortOption,
                                            sortType: field[fieldId].SORT_ORDER ? field[fieldId].SORT_ORDER :
                                                (this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc')
                                        };
                                        this.taskConfig.taskCardSortFieldName = this.taskConfig.selectedTaskCardSortOption.option.name;
                                        this.taskConfig.taskCardSortOrder = this.taskConfig.selectedTaskCardSortOption.sortType;
                                    }
                                }
                            });
                        }
                    }
                    if (!(this.taskConfig.selectedTaskCardSortOption && this.taskConfig.selectedTaskCardSortOption.option && this.taskConfig.selectedTaskCardSortOption.option.name)) {
                        this.taskConfig.selectedTaskCardSortOption = {
                            option: this.taskConfig.taskCardSortOptions[0],
                            sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc'
                        };
                        this.taskConfig.selectedTaskCardSortOption = taskCardDataSet.DEFAULT_SORT_FIELD ? taskCardDataSet.DEFAULT_SORT_FIELD : this.taskConfig.selectedTaskCardSortOption;
                        this.taskConfig.taskCardSortFieldName = this.taskConfig.selectedTaskCardSortOption.option.name;
                        this.taskConfig.taskCardSortOrder = this.taskConfig.selectedTaskCardSortOption.sortType;
                    }
                }
                if (this.taskConfig.taskListSortOptions.length > 0) {
                    let selectedSort;
                    if (this.selectedSortByParam && this.taskConfig.isTaskListView) {
                        selectedSort = this.taskConfig.taskListSortOptions.find(sort => sort.displayName === this.selectedSortByParam);
                    }
                    if (selectedSort) {
                        this.taskConfig.selectedTaskListSortOption = {
                            option: selectedSort,
                            sortType: this.selectedSortOrderParam ? this.selectedSortOrderParam :
                                (this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc')
                        };
                        this.taskConfig.taskListSortFieldName = this.taskConfig.selectedTaskListSortOption.option.name;
                        this.taskConfig.taskListSortOrder = this.taskConfig.selectedTaskListSortOption.sortType;
                    } else {
                        if (userPreferenceFieldData && (userPreference.IS_LIST_VIEW === true || userPreference.IS_LIST_VIEW === 'true')) {
                            userPreferenceFieldData.forEach(field => {
                                const fieldId = Object.keys(field)[0];
                                if (field[fieldId].IS_SORTABLE === true) {
                                    const selectedSortOption = this.taskConfig.taskListSortOptions.find(field => field.indexerId === fieldId);
                                    if (selectedSortOption) {
                                        this.taskConfig.selectedTaskListSortOption = {
                                            option: selectedSortOption,
                                            sortType: field[fieldId].SORT_ORDER ? field[fieldId].SORT_ORDER :
                                                (this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc')
                                        };
                                        this.taskConfig.taskListSortFieldName = this.taskConfig.selectedTaskListSortOption.option.name;
                                        this.taskConfig.taskListSortOrder = this.taskConfig.selectedTaskListSortOption.sortType;
                                    }
                                }
                            });
                        }
                    }
                    if (!(this.taskConfig.selectedTaskListSortOption && this.taskConfig.selectedTaskListSortOption.option && this.taskConfig.selectedTaskListSortOption.option.name)) {
                        this.taskConfig.selectedTaskListSortOption = {
                            option: this.taskConfig.taskListSortOptions[0],
                            sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc'
                        };
                        this.taskConfig.selectedTaskListSortOption = taskListDataSet.DEFAULT_SORT_FIELD ? taskListDataSet.DEFAULT_SORT_FIELD : this.taskConfig.selectedTaskListSortOption;
                        this.taskConfig.taskListSortFieldName = this.taskConfig.selectedTaskListSortOption.option.name;
                        this.taskConfig.taskListSortOrder = this.taskConfig.selectedTaskListSortOption.sortType;
                    }
                }
                this.storeCurrentViewData();
                this.setCurrentView();
                if (this.taskConfig.taskStatus.length === 0) {
                    this.entityAppDefService.getStatusByCategoryLevel(MPM_LEVELS.TASK)
                        .subscribe(statusResponse => {
                            if (statusResponse) {
                                this.taskConfig.taskStatus = statusResponse;
                            }
                            observer.next(true);
                            observer.complete();
                            this.loaderService.hide();
                        }, error => {
                            observer.error(error);
                            this.loaderService.hide();
                        });
                } else {
                    observer.next(true);
                    observer.complete();
                    this.loaderService.hide();
                }
            }, metadataError => {
                observer.error(metadataError);
            });
        });
    }

    getTasks(editData?) {
        if (this.taskConfig && this.taskConfig.projectItemId) {
            this.loaderService.show();
            let keyWord = '';
            let sortTemp = [];
            this.taskConfig.taskTotalCount = null;
            let projectSearch = null;
            let searchConditionList = this.fieldConfigService.formIndexerIdFromMapperId(TaskConstants.GET_ALL_TASKS_SEARCH_CONDITION_LIST.search_condition_list.search_condition);

            if (this.taskConfig.selectedResources) {
                const resourceSearchCondtionList = [];
                this.taskConfig.selectedResources.forEach((resource, index) => {
                    const allPersons = this.sharingService.getAllPersons();
                    const user = allPersons.find(person => { return person.Title.Value === resource });
                    const Id = user ? user.PersonToUser["Identity-id"].Id : 0;
                    /*  projectSearch = this.fieldConfigService.createConditionObject(
                         MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_NAME, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, index > 0 ? MPMSearchOperators.OR : MPMSearchOperators.AND, resource);
                      */
                    projectSearch = this.fieldConfigService.createConditionObject(
                        MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, index > 0 ? MPMSearchOperators.OR : MPMSearchOperators.AND, Id);

                    if (projectSearch) {
                        resourceSearchCondtionList.push(projectSearch);
                    }
                });

                if (resourceSearchCondtionList.length > 0) {
                    searchConditionList = searchConditionList.concat(resourceSearchCondtionList);
                }
            }
            projectSearch = this.fieldConfigService.createConditionObject(
                MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, this.taskConfig.projectItemId);
            if (projectSearch) {
                searchConditionList.push(projectSearch);
            }
            if (this.searchConditionList && this.searchConditionList.length > 0 && this.searchConditionList[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
                keyWord = this.searchConditionList[0].keyword;
            } else if (this.searchConditionList && this.searchConditionList.length > 0) {
                searchConditionList = searchConditionList.concat(this.searchConditionList);
            }
            // searchConditionList.push({
            //     field_id : "ID",
            //     relational_operator : "and",
            //      relational_operator_id : "MPM.OPERATOR.CHAR.IS_NOT_EMPTY",
            //     relational_operator_name : "is not empty",
            //     type: "string",
            //      });

            if (this.taskConfig.isListView) {
                if (this.taskConfig.selectedTaskListSortOption && this.taskConfig.selectedTaskListSortOption.option) {
                    sortTemp = [{
                        field_id: this.taskConfig.selectedTaskListSortOption.option.name,
                        order: this.taskConfig.selectedTaskListSortOption.sortType
                    }];
                }
            } else {
                if (this.taskConfig.selectedTaskCardSortOption && this.taskConfig.selectedTaskCardSortOption.option) {
                    sortTemp = [{
                        field_id: this.taskConfig.selectedTaskCardSortOption.option.name,
                        order: this.taskConfig.selectedTaskCardSortOption.sortType
                    }];
                }
            }
            let displayabeleFields= this.npdTaskService.getProjectTaskViewDisplayableFields();
            const parameters: SearchRequest = {
                search_config_id: null,
                // this.searchConfigId > 0 ? this.searchConfigId : null,
                keyword: keyWord,
                search_condition_list: {
                    search_condition: searchConditionList
                },
                displayable_field_list :{
                    displayable_fields : displayabeleFields
                },
                facet_condition_list: {
                    facet_condition: []
                },
                sorting_list: {
                    sort: sortTemp
                },
                cursor: {
                    page_index: this.skip,
                    page_size: this.top
                }
            };
            this.loaderService.show();
            this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
                this.isPageRefresh = false;
                if (editData) {
                    this.editTask(editData);
                }
                const data = [];
                response.data.forEach(element => {
                    if (element.IS_BULK_UPDATE_TASK !== 'true') {
                        data.push(element);
                    }
                });
                if (sortTemp?.[0]?.field_id == 'IS_ACTIVE_TASK') {//sortTemp?.[0]

                //   console.log("PROJECT DETAILS",this.taskConfig.selectedProject['Project-id'].Id,);this.projectDetails;

                  this.getProjectDetailsById(this.taskConfig.selectedProject['Project-id'].Id,data).subscribe(res=>{
                    this.npdTaskService.sortTasksBySequence(data,this.classificationNumber).subscribe(dataResponse => {
                      this.taskConfig.taskList = dataResponse;
                      this.taskConfig.taskTotalCount = response.cursor.total_records;
                      const ownerFeild: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_NAME, MPM_LEVELS.TASK);
                      this.taskConfig.taskList.map(task => {
                          const resourceName = this.fieldConfigService.getFieldValueByDisplayColumn(ownerFeild, task);
                          const isExist = this.taskConfig.taskOwnerResources.find(element => element === resourceName);
                          if (!isExist && resourceName) {
                              this.taskConfig.taskOwnerResources.push(resourceName);
                          }
                      });
                      this.taskGridComponent.refreshChild(this.taskConfig);
                      this.loaderService.hide();
                  },err=>{

                    this.loaderService.hide();

                  });
                  })

                } else {
                    this.taskConfig.taskList = data;
                    this.taskConfig.taskTotalCount = response.cursor.total_records;
                    const ownerFeild: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_NAME, MPM_LEVELS.TASK);
                    this.taskConfig.taskList.map(task => {
                        const resourceName = this.fieldConfigService.getFieldValueByDisplayColumn(ownerFeild, task);
                        const isExist = this.taskConfig.taskOwnerResources.find(element => element === resourceName);
                        if (!isExist && resourceName) {
                            this.taskConfig.taskOwnerResources.push(resourceName);
                        }
                    });
                    this.taskGridComponent.refreshChild(this.taskConfig);
                    this.loaderService.hide();
                }

                /*    this.taskConfig.taskTotalCount = response.cursor.total_records;
                   const ownerFeild: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_NAME, MPM_LEVELS.TASK);
                   this.taskConfig.taskList.map(task => {
                       const resourceName = this.fieldConfigService.getFieldValueByDisplayColumn(ownerFeild, task);
                       const isExist = this.taskConfig.taskOwnerResources.find(element => element === resourceName);
                       if (!isExist && resourceName) {
                           this.taskConfig.taskOwnerResources.push(resourceName);
                       }
                   });
                   this.taskGridComponent.refreshChild(this.taskConfig);
                   this.loaderService.hide(); */


            }, () => {
                this.loaderService.hide();
            });
        }
    }

    editTask(eventData) {
        this.taskConfig.isEditTask = true;
        const sortOption = this.taskConfig.isTaskListView ? (this.taskConfig.selectedTaskListSortOption && this.taskConfig.selectedTaskListSortOption.option &&
            this.taskConfig.selectedTaskListSortOption.option.displayName ? this.taskConfig.selectedTaskListSortOption.option.displayName : null) :
            (this.taskConfig.selectedTaskCardSortOption && this.taskConfig.selectedTaskCardSortOption.option && this.taskConfig.selectedTaskCardSortOption.option.displayName
                ? this.taskConfig.selectedTaskCardSortOption.option.displayName : null);
        const sortOrder = this.taskConfig.isTaskListView ? (this.taskConfig.selectedTaskListSortOption && this.taskConfig.selectedTaskListSortOption.option &&
            this.taskConfig.selectedTaskListSortOption.option.displayName && this.taskConfig.taskListSortOrder ? this.taskConfig.taskListSortOrder : null) :
            (this.taskConfig.selectedTaskCardSortOption && this.taskConfig.selectedTaskCardSortOption.option && this.taskConfig.selectedTaskCardSortOption.option.displayName
                && this.taskConfig.taskCardSortOrder ? this.taskConfig.taskCardSortOrder : null);
      /*this.routeService.goToTaskView(this.taskConfig.isTemplate, this.taskConfig.projectId, this.taskConfig.isTaskListView, this.taskConfig.isEditTask, eventData.taskId,
          this.selectedCampaignId ? this.selectedCampaignId : null, sortOption, sortOrder
          , this.taskConfig.selectedResources && this.taskConfig.selectedResources.length > 0 ? this.taskConfig.selectedResources.toString() : null,
          this.searchName ? this.searchName : null, this.savedSearchName ? this.savedSearchName : null, this.advSearchData ? this.advSearchData : null);
      */let selectedtask = (eventData && eventData.taskList) ? eventData.taskList.find(task => task.ID == eventData.taskId) : null;

        const dialogRef = this.dialog.open(TaskCreationModalComponent, {
            width: '38%',
            disableClose: true,
            data: {
                taskConfig: this.taskConfig,
                projectId: eventData.projectId,
                isTemplate: eventData.isTemplate,
                taskId: eventData.taskId,
                projectObj: this.taskConfig.selectedProject,
                isApprovalTask: eventData.isApprovalTask,
                modalLabel: 'Edit Task',
                selectedtask: selectedtask,
                isTaskActive: selectedtask?.IS_ACTIVE_TASK,
                alltask: eventData.taskList
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            this.taskConfig.isEditTask = false;
            this.routeService.goToTaskView(this.taskConfig.isTemplate, this.taskConfig.projectId, this.taskConfig.isTaskListView, this.taskConfig.isEditTask, null,
                this.selectedCampaignId ? this.selectedCampaignId : null,
                sortOrder, sortOption
                , this.taskConfig.selectedResources && this.taskConfig.selectedResources.length > 0 ? this.taskConfig.selectedResources.toString() : null,
                this.searchName ? this.searchName : null, this.savedSearchName ? this.savedSearchName : null, this.advSearchData ? this.advSearchData : null);
            if (result) {
                this.getTasks();
            }
        });
    }
    openCustomWorkflow(eventData?) {


        if (this.taskConfig.isTemplate || eventData) {
            const dialogRef = this.dialog.open(CustomWorkflowModalComponent, {
                width: '100%',
                // minHeight: '15vh',
                // maxHeight: '100vh',
                minHeight: '25%',
                maxHeight: '100%',
                disableClose: true,
                data: {
                    currentTaskData: eventData ? eventData : null,
                    taskData: this.taskConfig.taskList,
                    isTemplate: this.taskConfig.isTemplate,
                    sequence: this.taskConfig.taskList.map(x => { return { name: x.TASK_NAME, sequence: x.sequence, taskId: x.ID } })
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.taskService.getActionRulesByProjectId(this.taskConfig.projectId).subscribe(response => {
                        this.taskConfig.workflowActionRules = response;
                    });
                }
            });
        } else {
            const dialogRef = this.dialog.open(NpdCustomWorkflowModalComponent, {
                width: '100%',
                minHeight: '25%',
                maxHeight: '100%',
                disableClose: true,
                data: {
                    currentTaskData: eventData ? eventData : null,
                    taskData: this.taskConfig.taskList,
                    isTemplate: this.taskConfig.isTemplate,
                    //Setting  the Sorting variable to CustomWorkFlow modal
                    sequence: this.taskConfig.taskList.map(x => { return { name: x.TASK_NAME, sequence: x.sequence, taskId: x.ID, taskStatusValue: x.TASK_STATUS_VALUE } })


                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.taskService.getActionRulesByProjectId(this.taskConfig.projectId).subscribe(response => {
                        this.taskConfig.workflowActionRules = response;
                    });
                }
            });
        }

    }

    getSearchField(type, field_id, operator_id, operator_name, value?, field_operator?) {

      let searchField = {
        "type": type,
        "field_id": field_id,
        "relational_operator_id": operator_id,
        "relational_operator_name": operator_name,
        "value": value,
        "relational_operator": field_operator
      }
      return searchField;
    }

    public projectDetails:any;
    public classificationNumber:any;
    getProjectDetailsById(projectId,taskList): Observable<any> {
      let searchConditionList = [
        this.getSearchField("string", "CONTENT_TYPE", "MPM.OPERATOR.CHAR.IS", "is", "MPM_PROJECT"),
        this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", projectId, "AND"),
      ];

      const parameters: SearchRequest = {
        search_config_id: null,
        keyword: "",
        search_condition_list: {
          search_condition: searchConditionList
        },
        facet_condition_list: {
          facet_condition: []
        },
        sorting_list: {
          sort: []
        },
        cursor: {
          page_index: 0,
          page_size: 1
        }
      };
      return new Observable(observer => {
        this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
          if (response.data.length > 0) {
            this.classificationNumber = this.npdProjectService.getClassificationNumberByTasksStatus(response.data,taskList);
            // console.log("Classification number",this.classificationNumber);
            this.projectDetails = response.data[0];
          }
          observer.next(response.data);
          observer.complete();
        }, () => {
          observer.error();
        });
      });
    }

}
