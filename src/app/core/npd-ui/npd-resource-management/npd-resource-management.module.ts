import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';

import { NpdResourceManagementRoutingModule } from './npd-resource-management-routing.module';
import { BaseResourceLayoutComponent } from './base-resource-layout/base-resource-layout.component';
import { ProjectTasksLayoutComponent } from './project-tasks-layout/project-tasks-layout.component';
import { GanttModule, GanttAllModule } from '@syncfusion/ej2-angular-gantt';
import { ResourceTaskQuad34Component } from './resource-task-quad34/resource-task-quad34.component';
import { MpmLibModule } from '../../mpm-lib/mpm-lib.module';
import { MpmLibraryModule,FacetModule, PaginationModule, ColumnChooserModule } from 'mpm-library';
import { RequestViewModule } from '../request-view/request-view.module';
import { ResizableDirective } from './directives/resizable.directive';
import { HoriResizableDirective } from './directives/hori-resizable.directive';
import { BulkResourceAssignmentComponent } from './bulk-resource-assignment/bulk-resource-assignment.component';
import { NpdPaginationModule } from '../npd-pagination/npd-pagination.module';
import { ResizeColumnDirective } from './directives/resize-column.directive';
import { ProjectTaskDetailPipe } from './project-task-detail.pipe';
import { ProjectTaskTableComponent } from './project-task-table/project-task-table.component';
import { BulkResourceTaskConfirmationComponent } from './bulk-resource-task-confirmation/bulk-resource-task-confirmation.component';

@NgModule({
  declarations: [
    BaseResourceLayoutComponent, 
    ProjectTasksLayoutComponent, 
    ResourceTaskQuad34Component, 
    ResizableDirective, 
    HoriResizableDirective, 
    BulkResourceAssignmentComponent, 
    ResizeColumnDirective,
    ProjectTaskDetailPipe,
    ProjectTaskTableComponent,
    BulkResourceTaskConfirmationComponent],
  imports: [
    CommonModule,
    NpdResourceManagementRoutingModule,
    MaterialModule,
    GanttModule,GanttAllModule,
    MpmLibraryModule,
    MpmLibModule,
    FacetModule,
    PaginationModule,
    NpdPaginationModule,
    RequestViewModule,//weekpicker
    ColumnChooserModule
  ]
})
export class NpdResourceManagementModule { }
