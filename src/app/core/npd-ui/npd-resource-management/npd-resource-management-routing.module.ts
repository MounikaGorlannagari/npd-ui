import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseResourceLayoutComponent } from './base-resource-layout/base-resource-layout.component';


const routes: Routes = [
  {
    path: '',
    component: BaseResourceLayoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NpdResourceManagementRoutingModule { }
