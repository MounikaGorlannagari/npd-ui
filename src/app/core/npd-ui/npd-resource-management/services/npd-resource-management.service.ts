import { Injectable } from '@angular/core';
import { AppService, CustomSelect, FieldConfigService, ListFilter, MPMFieldConstants, MPMSearchOperatorNames, MPMSearchOperators, MPM_LEVEL, MPM_LEVELS, OTMMMPMDataTypes, ProjectConstant, Status, StatusTypes, TaskConstants } from 'mpm-library';
import { ProjectTaskSearchConditions, ResourceManagementConstants } from '../constants/ProjectTaskViewParams';
import { TaskTypes } from 'mpm-library';
import { Observable, Subject } from 'rxjs';
import * as acronui from 'mpm-library';
import { MonsterRequestService } from '../../request-view/services/monster-request.service';
import { NpdSessionStorageConstants } from '../../request-view/constants/sessionStorageConstants';
@Injectable({
  providedIn: 'root'
})
export class NpdResourceManagementService {
  resizeWidth = new Subject();
  resizeInnerWidth = new Subject();

  AFTER_TASK_EDIT_METHOD_NS = 'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  AFTER_TASK_EDIT_WS_METHOD_NAME = 'AfterTaskEditActions';

  TASK_POINTS_BY_WEEKNO_METHOD_NS = 'http://schemas.npd.monster.com/jobrequest/1.0';
  GET_TASK_POINTS_BY_WEEKNO_WS_METHOD_NAME = 'GetTaskPointsByWeekNo';

  GROUP_POINTS_BY_WEEKNO_METHOD_NS = 'http://schemas.npd.monster.com/jobrequest/1.0';
  GET_GROUP_POINTS_BY_WEEKNO_WS_METHOD_NAME = 'GetGroupPointsByWeekNo';

  ROLE_POINTS_BY_WEEKNO_METHOD_NS = 'http://schemas.npd.monster.com/jobrequest/1.0';
  GET_ROLE_POINTS_BY_WEEKNO_WS_METHOD_NAME = 'GetRolePointsByweekNo';

  GET_ALL_WEEK_OFFS_METHOD_NS = 'http://schemas/NPDLookUpDomainModel/Week_Off/operations';
  GET_ALL_WEEK_OFFS_WS_METHOD_NAME = 'GetAllWeekOffs';

  GET_ALL_SUB_GROUPS_METHOD_NS = 'http://schemas/NPDLookUpDomainModel/User_Sub_Groups/operations';
  GET_ALL_SUB_GROUPS_WS_METHOD_NAME = 'GetAllSubGroups';

  BULK_ASSIGN_PROJECT_METHOD_NS = 'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  BULK_ASSIGN_PROJECT_WS_METHOD_NAME = 'BulkAssignProject';

  BULK_ASSIGN_TASK_METHOD_NS = 'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  BULK_ASSIGN_TASK_WS_METHOD_NAME = 'BulkTaskHandler';

  ALL_TASK_ROLES_METHOD_NS = 'http://schemas/NPDLookUpDomainModel/Task_Role_Mapping/operations';
  ALL_TASK_ROLES_WS_METHOD_NAME = 'GetAllTaskRoles';

  GET_ALL_TASKS_PRIOR_TOCP1_NS = 'http://schemas.npd.monster.com/jobrequest/1.0';
  GET_ALL_TASKS_PRIOR_TOCP1_WS = 'GetAllTasksPriorToCP1';


  constructor(private fieldConfigService: FieldConfigService, private appService: AppService,
  ) { }

  addDefaultConditionsToList(level: MPM_LEVEL, searchConditions: ProjectTaskSearchConditions): ProjectTaskSearchConditions {
    let condition = null;
    condition = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS,
      MPMSearchOperatorNames.IS, undefined, (level === MPM_LEVELS.CAMPAIGN ? OTMMMPMDataTypes.CAMPAIGN : level === MPM_LEVELS.PROJECT ? OTMMMPMDataTypes.PROJECT : OTMMMPMDataTypes.TASK));
    if (condition) {
      if (level === MPM_LEVELS.PROJECT) {
        searchConditions?.PROJECT_CONDITION.push(condition);
      } else {
        if (searchConditions?.TASK_CONDITION) {
          searchConditions?.TASK_CONDITION.push(condition);
        } else {
          console.log('');
        }
      }
    }
    if (level === MPM_LEVELS.PROJECT) {
      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE,
        MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, MPM_LEVELS.PROJECT);
      condition ? searchConditions.PROJECT_CONDITION.push(condition) : console.log('');
      condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_ITEM_ID, MPMSearchOperators.IS_NOT_EMPTY,
        MPMSearchOperatorNames.IS_NOT_EMPTY, undefined, undefined);
      condition ? ((level === MPM_LEVELS.PROJECT) ? searchConditions.GROUP_PROJECT_TASK_CONDITION.push(condition) :
        searchConditions.TASK_CONDITION.push(condition)) : console.log('');
    } else if (level === MPM_LEVELS.TASK) {
      if (searchConditions?.TASK_CONDITION) {
        condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_TYPE,
          MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, TaskTypes.TASK_WITHOUT_DELIVERABLE);
        condition ? searchConditions.TASK_CONDITION.push(condition) : console.log('');
        condition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_ITEM_ID, MPMSearchOperators.IS_NOT_EMPTY,
          MPMSearchOperatorNames.IS_NOT_EMPTY, MPMSearchOperators.AND, undefined);

        condition ? searchConditions.TASK_CONDITION.push(condition) : console.log('');
      }
    }
    return searchConditions;
  }


  getSearchCondition(listFilter: ListFilter, listDependentFilter: CustomSelect, level: MPM_LEVEL, startdates?, endDate?, userId?, requiredStatus?): ProjectTaskSearchConditions {
    /* getSearchConditionNew(listFilter: ListFilter, listDependentFilter: CustomSelect, level: MPM_LEVEL, dates?, userId?, requiredStatus?): TaskSearchConditions { */
    let searchConditions: ProjectTaskSearchConditions = {
      PROJECT_CONDITION: [],
      GROUP_PROJECT_TASK_CONDITION: []
    };
    searchConditions = this.addDefaultConditionsToList(level, searchConditions);
    if (!listFilter && listFilter !== undefined) {
      console.warn('List filter and group by filter and provided to getSearchCondidtion');
      return searchConditions;
    }
    let condition = null;

    if (listDependentFilter === undefined && listFilter === undefined) {

      if (endDate) {
        condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_START_DATE, MPMSearchOperators.IS_ON_OR_BEFORE, MPMSearchOperatorNames.IS_ON_OR_BEFORE, 'AND', endDate);
        condition ? (searchConditions.GROUP_PROJECT_TASK_CONDITION.push(condition)) : console.log('');

        condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_COMPLETION_DATE, MPMSearchOperators.IS_ON_OR_AFTER, MPMSearchOperatorNames.IS_ON_OR_AFTER, 'AND', startdates);
        condition ? (searchConditions.GROUP_PROJECT_TASK_CONDITION.push(condition)) : console.log('');
        condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_START_DATE, ResourceManagementConstants.MPMSearchOperators.BEFORE, ResourceManagementConstants.MPMSearchOperatorNames.BEFORE, 'AND', endDate);
        condition ? (searchConditions.GROUP_PROJECT_TASK_CONDITION.push(condition)) : console.log('');

        condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_COMPLETION_DATE, ResourceManagementConstants.MPMSearchOperators.AFTER, ResourceManagementConstants.MPMSearchOperatorNames.AFTER, 'AND', startdates);
        condition ? (searchConditions.GROUP_PROJECT_TASK_CONDITION.push(condition)) : console.log('');
      } else {
        condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_START_DATE, MPMSearchOperators.IS_BETWEEN, MPMSearchOperatorNames.IS_BETWEEN, 'AND', startdates);/* dates */
        condition ? (searchConditions.GROUP_PROJECT_TASK_CONDITION.push(condition)) : console.log('');
      }
      /*searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, listFilter.mapperName, listFilter.searchCondition, false, false, MPMSearchOperators.IS, MPMSearchOperatorNames.IS);

      searchConditions = this.handleStatusCheckAndConditionPush(searchConditions, MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE, 'TEMPLATE', true, true,
        MPMSearchOperators.IS_NOT,
        MPMSearchOperatorNames.IS_NOT); */
    }

    return searchConditions;
  }

  getTaskSearchCondition(listFilter: ListFilter, listDependentFilter: CustomSelect, level: MPM_LEVEL, startdates?, endDate?, userId?, requiredStatus?): ProjectTaskSearchConditions {
    let searchConditions: ProjectTaskSearchConditions = {
      TASK_CONDITION: [],
    };
    searchConditions = this.addDefaultConditionsToList(level, searchConditions);
    /*     if (!listFilter) {
      console.warn('List filter and group by filter and provided to getSearchCondidtion');
      return searchConditions;
    } */
    let condition = null;

    //if (!listDependentFilter && !listFilter) {
    if (startdates) {
      condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_START_DATE, MPMSearchOperators.IS_ON_OR_BEFORE, MPMSearchOperatorNames.IS_ON_OR_BEFORE, 'AND', endDate);
      condition ? (searchConditions.TASK_CONDITION.push(condition)) : console.log('');

      condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_COMPLETION_DATE, MPMSearchOperators.IS_ON_OR_AFTER, MPMSearchOperatorNames.IS_ON_OR_AFTER, 'AND', startdates);
      condition ? (searchConditions.TASK_CONDITION.push(condition)) : console.log('');

      // condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_START_DATE, MPMSearchOperators.IS_BETWEEN, MPMSearchOperatorNames.IS_BETWEEN, 'OR', startdates +' and '+endDate);
      // condition ? (searchConditions.TASK_CONDITION.push(condition)) : console.log('');

      // condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_COMPLETION_DATE, MPMSearchOperators.IS_BETWEEN, MPMSearchOperatorNames.IS_BETWEEN, 'OR', startdates +' and '+endDate);
      // condition ? (searchConditions.TASK_CONDITION.push(condition)) : console.log('');

      // condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_START_DATE, ResourceManagementConstants.MPMSearchOperators.BEFORE, ResourceManagementConstants.MPMSearchOperatorNames.BEFORE, 'AND', endDate);
      // condition ? (searchConditions.TASK_CONDITION.push(condition)) : console.log('');

      // condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_COMPLETION_DATE, ResourceManagementConstants.MPMSearchOperators.AFTER, ResourceManagementConstants.MPMSearchOperatorNames.AFTER, 'AND', startdates);
      // condition ? (searchConditions.TASK_CONDITION.push(condition)) : console.log('');



    } else {
      condition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.SEARCH_FIELDS.NPD_TASK_ESTIMATED_COMPLETION_DATE, MPMSearchOperators.IS_BETWEEN, MPMSearchOperatorNames.IS_BETWEEN, 'AND', endDate);/* dates */
      condition ? (searchConditions.TASK_CONDITION.push(condition)) : console.log('');
    }
    //}

    return searchConditions;
  }

  getUsersByRole(taskObject): Observable<any> {
    let teamRoleUserOptions = [];
    return new Observable(observer => {
      let getUserByRoleRequest = {};

      getUserByRoleRequest = {
        allocationType: taskObject.allocation,
        roleDN: taskObject.allocation === TaskConstants.ALLOCATION_TYPE_ROLE ? taskObject.role.value : '',
        teamID: taskObject.allocation === TaskConstants.ALLOCATION_TYPE_USER ? taskObject.teamId : '',
        isApproveTask: taskObject.isApprovalTask,
        isUploadTask: !taskObject.isApprovalTask
      };

      this.appService.getUsersForTeam(getUserByRoleRequest)
        .subscribe(response => {
          if (response && response.users && response.users.user) {
            const userList = acronui.findObjectsByProp(response, 'user');
            const users = [];
            if (userList && userList.length && userList.length > 0) {
              userList.map(user => {
                const fieldObj = users.find(e => e.value === user.dn);
                if (!fieldObj) {
                  users.push({
                    name: user.dn,
                    value: user.cn,
                    displayName: user.name
                  });
                }
              });
            }
            teamRoleUserOptions = users;
          } else {
            teamRoleUserOptions = [];
          }
          observer.next(teamRoleUserOptions);
          observer.complete();
        }, (error) => {
          //const eventData = { message: 'Something went wrong while fetching Individual Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
          observer.error();
        });
    });
  }
  public changeTaskBarColor(status) {
    let customClass;
    if (status == "Defined" || status == "Active") {
      customClass = 'calender-paint';
    } else if (status == "In Progress") {
      customClass = 'in-progress-paint';
    } else if (status == "Approved/Completed" || status == "Completed") {
      customClass = 'completed-calender-paint';
    } else if (status == "Cancelled" || status == "OnHold" || status == "Requested Changes") {
      customClass = 'cancelled-calender-paint';
    } else {
      customClass = 'default-paint';
    }
    return customClass;
  }

  public changeTaskBarColors(status) {
    let customClass;
    if (status == "Defined" || status == "Active") {
      customClass = 'calender-paint';
    } else if (status == "In Progress") {
      customClass = 'calender-paint';
    } else if (status == "Approved/Completed" || status == "Completed") {
      customClass = 'completed-calender-paint';
    } else if (status == "Cancelled") {
      customClass = 'task-cancelled-paint';
    } else if (status == "OnHold" || status == "Requested Changes") {
      customClass = 'cancelled-calender-paint';
    } else {
      customClass = 'default-paint';
    }
    return customClass;
  }

  getResizeWidth(value) {
    this.resizeWidth.next(value);
  }

  getResizeInnerWidth(value, index) {
    this.resizeInnerWidth.next({ value, index });
  }

  updateTaskAdjustmentPoint(taskObject): Observable<any> {
    return new Observable(observer => {
      const getTaskPointsObject = {
        afterTaskEditActions: {
          Task: {
            Id: taskObject.Id,
            ItemId: taskObject.ItemId,
          },
          AdjustmentPoint: taskObject.adjustmentPoint
        },
      };
      this.appService.invokeRequest(this.AFTER_TASK_EDIT_METHOD_NS, this.AFTER_TASK_EDIT_WS_METHOD_NAME, getTaskPointsObject)
        .subscribe(response => {
          observer.next(response);
          observer.complete();
        }, (error) => {
          observer.error();
        });
    });
  }

  getUserTaskCalculatedPointsByWeek(startWeekNo, endWeekNo, cursor?): Observable<any> {
    return new Observable(observer => {
      const getTaskPointsObject = {
        /*         cursor: {
                  '@numRows' : 200
                } , */
        startWeekNo: startWeekNo,
        endWeekNo: endWeekNo
      };
      this.appService.invokeRequest(this.TASK_POINTS_BY_WEEKNO_METHOD_NS, this.GET_TASK_POINTS_BY_WEEKNO_WS_METHOD_NAME, getTaskPointsObject)
        .subscribe(response => {
          observer.next(response);
          observer.complete();
        }, (error) => {
          observer.error();
        });
    });
  }

  getGroupPointsByWeek(startWeekNo, endWeekNo, cursor?): Observable<any> {
    return new Observable(observer => {
      const getTaskPointsObject = {
        /*     cursor: {
              '@numRows' : 200
            } , */
        startWeekNo: startWeekNo,
        endWeekNo: endWeekNo
      };
      this.appService.invokeRequest(this.GROUP_POINTS_BY_WEEKNO_METHOD_NS, this.GET_GROUP_POINTS_BY_WEEKNO_WS_METHOD_NAME, getTaskPointsObject)
        .subscribe(response => {
          observer.next(response);
          observer.complete();
        }, (error) => {
          observer.error();
        });
    });
  }

  getRolePointsByWeek(startWeekNo, endWeekNo, roleId, cursor?): Observable<any> {
    return new Observable(observer => {
      const getTaskPointsObject = {
        /*  cursor: {
           '@numRows' : 200
         } , */
        startWeekNo: startWeekNo,
        endWeekNo: endWeekNo,
        roleId: roleId
      };
      this.appService.invokeRequest(this.ROLE_POINTS_BY_WEEKNO_METHOD_NS, this.GET_ROLE_POINTS_BY_WEEKNO_WS_METHOD_NAME, getTaskPointsObject)
        .subscribe(response => {
          observer.next(response);
          observer.complete();
        }, (error) => {
          observer.error();
        });
    });
  }


  fetchAllUserGroupMappings(callBack) {

    const useGroupMappingObject = {
      Cursor: {
        '@limit': 200
      },
    };
    this.appService.invokeRequest(this.GET_ALL_WEEK_OFFS_METHOD_NS, this.GET_ALL_WEEK_OFFS_WS_METHOD_NAME, useGroupMappingObject)
      .subscribe(response => {
        let userGroupOptions = [];
        const options = this.getPropertyListFromResponse(response, 'Week_Off');
        userGroupOptions = this.getListAsResponse(options);
        window.sessionStorage.alluserGroups = JSON.stringify(userGroupOptions);
        if (callBack) {
          callBack();
        }
      }, (error) => {
        window.sessionStorage.alluserGroups = JSON.stringify([]);
        if (callBack) {
          callBack();
        }

      });
  }


  fetchAllSubGroups(callBack) {
    const getSubGroupsObject = {
      Cursor: {
        '@limit': 200
      },
    };
    this.appService.invokeRequest(this.GET_ALL_SUB_GROUPS_METHOD_NS, this.GET_ALL_SUB_GROUPS_WS_METHOD_NAME, getSubGroupsObject)
      .subscribe(response => {
        let subGroupOptions = [];
        const options = this.getPropertyListFromResponse(response, 'User_Sub_Groups');
        subGroupOptions = this.getListAsResponse(options);
        window.sessionStorage.allSubGroups = JSON.stringify(subGroupOptions);
        if (callBack) {
          callBack();
        }
      }, (error) => {
        window.sessionStorage.allSubGroups = JSON.stringify([]);
        if (callBack) {
          callBack();
        }
      });
  }


  getAllSubGroups(cursor?): Observable<any> {
    return new Observable((observer) => {
      if (window.sessionStorage.allSubGroups) {
        observer.next(this.parseStorageValue(window.sessionStorage.allSubGroups));
      }
      else {
        this.fetchAllSubGroups(() => {
          observer.next(this.parseStorageValue(window.sessionStorage.allSubGroups));
        });
      }
    });
  }

  getUserGroupMappings(cursor?): Observable<any> {
    return new Observable((observer) => {
      if (window.sessionStorage.alluserGroups) {
        observer.next(this.parseStorageValue(window.sessionStorage.alluserGroups));
      }
      else {
        this.fetchAllUserGroupMappings(() => {
          observer.next(this.parseStorageValue(window.sessionStorage.alluserGroups));
        });
      }
    });
  }

  // to parse stored data in local storage
  parseStorageValue(data: string) {
    const json = JSON.parse(data);
    return json;
  }

  // to get property node from appworks response
  getPropertyListFromResponse(response, property) {
    const propertyList = acronui.findObjectsByProp(response, property);
    return propertyList;
  }

  getListAsResponse(options) {
    let arrayList = [];
    if (options) {
      if (Array.isArray(options)) {
        arrayList = options;
      }
      else {
        arrayList.push(options);
      }
    }
    return arrayList;
  }

  transformResponse(list, propertyId, isSort?, isActive?, isMarket?) {
    let allList = [];
    if (isMarket) {
      for (var i = 0; i < list.length; i++) {
        let options = {
          displayName: list[i].DisplayName || list[i].DISPLAY_NAME,
          value: list[i].Value || list[i].VALUE,
          ItemId: list[i][propertyId].ItemId,
          Id: list[i][propertyId].Id,
          sequence: list[i].Sequence || list[i].SEQUENCE,//to sort dropdown values based on it
          businessUnit: list[i]?.R_PO_BUSINESS_UNIT && list[i]?.R_PO_BUSINESS_UNIT['Business_Unit-id'],
          ConversionRateEURO: list[i]?.ConversionRateEURO
        }
        allList.push(options);
      }
    } else {
      for (var i = 0; i < list.length; i++) {
        let options = {
          displayName: list[i].DisplayName || list[i].DISPLAY_NAME,
          value: list[i].Value || list[i].VALUE,
          ItemId: list[i][propertyId].ItemId,
          Id: list[i][propertyId].Id,
          isActive: list[i].IsActive,
          sequence: list[i].Sequence || list[i].SEQUENCE,//to sort dropdown values based on it
        }
        allList.push(options);
      }
    }
    if (isActive) {
      allList = allList.filter(list => list.isActive)
    }
    if (isSort) {
      allList.sort((a, b) => {
        return a.sequence - b.sequence;
      });
    }
    return allList;
  }

  bulkProjectAssign(bulkAssignObject): Observable<any> {
    return new Observable(observer => {
      this.appService.invokeRequest(this.BULK_ASSIGN_PROJECT_METHOD_NS, this.BULK_ASSIGN_PROJECT_WS_METHOD_NAME, bulkAssignObject)
        .subscribe(response => {
          observer.next(response);
          observer.complete();
        }, (error) => {
          observer.error();
        });
    });
  }

  bulkTaskAssign(bulkAssignObject): Observable<any> {
    return new Observable(observer => {
      this.appService.invokeRequest(this.BULK_ASSIGN_TASK_METHOD_NS, this.BULK_ASSIGN_TASK_WS_METHOD_NAME, bulkAssignObject)
        .subscribe(response => {
          observer.next(response);
          observer.complete();
        }, (error) => {
          observer.error();
        });
    });
  }


  getAllTaskRoles(cursor?): Observable<any> {
    return new Observable((observer) => {
      if (sessionStorage.getItem(NpdSessionStorageConstants.ALL_TASK_ROLES) !== null) {
        observer.next(this.parseStorageValue(sessionStorage.getItem(NpdSessionStorageConstants.ALL_TASK_ROLES)));
        observer.complete();
      }
      else {
        this.fetchAllTaskRoles(() => {
          observer.next(this.parseStorageValue(sessionStorage.getItem(NpdSessionStorageConstants.ALL_TASK_ROLES)));
          observer.complete();
        });
      }
    });
  }


  getAllTaskPriorToCP1(parameter): Observable<any> {
    return new Observable(observer => {
      this.appService.invokeRequest(this.GET_ALL_TASKS_PRIOR_TOCP1_NS, this.GET_ALL_TASKS_PRIOR_TOCP1_WS, parameter)
        .subscribe(response => {
          observer.next(response);
          observer.complete();
        }, error => {
          observer.error(error);
        });
    });
  }

  fetchAllTaskRoles(callBack) {

    const taskRoleMappingObject = {
      Cursor: {
        '@limit': 100
      },
    };
    this.appService.invokeRequest(this.ALL_TASK_ROLES_METHOD_NS, this.ALL_TASK_ROLES_WS_METHOD_NAME, taskRoleMappingObject)
      .subscribe(response => {
        let allTaskRoleOptions = [];
        const options = this.getPropertyListFromResponse(response, 'Task_Role_Mapping');
        allTaskRoleOptions = this.getListAsResponse(options);
        //window.sessionStorage.allTaskRoles = JSON.stringify(allTaskRoleOptions);
        sessionStorage.setItem(NpdSessionStorageConstants.ALL_TASK_ROLES, JSON.stringify(allTaskRoleOptions));
        if (callBack) {
          callBack();
        }
      }, (error) => {
        sessionStorage.setItem(NpdSessionStorageConstants.ALL_TASK_ROLES, JSON.stringify([]));
        if (callBack) {
          callBack();
        }

      });
  }
  fetchAllTaskRolesByClassification(classificationNumber) {

    let params = {
      ClassificationNumber: classificationNumber
    }
    return new Observable((observer) => {

      this.appService.invokeRequest('http://schemas.npd.monster.com/jobrequest/1.0', 'GetProjectTasksConfigsByProjectClassificationNumber', params)
        .subscribe(response => {
          let allTaskRoleOptions = [];
          const options = this.getPropertyListFromResponse(response, 'ProjectTaskConfig');
          let data=options.map(x=>{
            x.TaskSequence=x.TASK_SEQUENCE;
            x.TaskName=x.TASK_NAME;
            x.TaskDescription=x.TASK_DESCRIPTION;
            return x;
          })
          allTaskRoleOptions = this.getListAsResponse(data
            );
          observer.next(allTaskRoleOptions);

        }, (error) => {
          observer.error(error);

        });
    })
  }

}
