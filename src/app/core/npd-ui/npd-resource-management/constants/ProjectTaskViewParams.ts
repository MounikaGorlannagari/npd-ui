import { SortDirection } from '@angular/material/sort';

export interface ProjectTasksViewComponentParams {
    groupByFilter?: string;
    deliverableTaskFilter?: string;
    listDependentFilter?: string;
    emailLinkId?: string;
    sortBy?: string;
    sortOrder?: SortDirection;
    pageNumber?: number;
    pageSize?: number;
    searchName?: string;
    savedSearchName?: string;
    advSearchData?: string;
    facetData?: string;
}

export interface ProjectTaskSearchConditions {
    PROJECT_CONDITION?: Array<any>;
    GROUP_PROJECT_TASK_CONDITION?: Array<any>;
    TASK_CONDITION?: Array<any>;
    USER_TASK_CONDITION?: Array<any>;
}


export const ResourceManagementConstants = {
    PROJECT_ASSIGNMENT_ROLES: [
        'E2E PM','GFG','Regulatory Affairs','Corp PM','Ops PM','RTM', 'Project Specialist', 'Secondary AW Manager'],
    CHECKPOINT_TASKS: ['CPO','CP1','CP2','CP3','CP4','TASK 30'],
    GFG_TASKS: ['TASK 12','TASK 13','TASK 14','TASK 16','TASK 19'],
    FACET_CONSTRAINTS: { 
        type :'SimpleFieldRestriction',
        behavior: 'EXCLUDE',
        TASK_FIELDS: {
            TASK_ROLE_VALUE:'TASK_ROLE_VALUE',
            TASK_ROLE_ID:'TASK_ROLE_ID'
        }
    },
    SORT_FIELDS: {
        TASK : {
            TASK_ESTIMATED_START_DATE:'NPD_TASK_ESTIMATED_START_DATE'
        },
    },
    SORT_ORDER : {
        SORT_ASC:'asc',
        SORT_DESC:'desc'
    },
    NPD_TASK_FIELDS : {
        NPD_TASK_RM_USER_ID:'NPD_TASK_RM_USER_ID',
        NPD_TASK_RM_ROLE_ID:'NPD_TASK_RM_ROLE_ID'
    },
    NPD_TEAM:'EM NPD Team',

    SEARCH_FIELDS : {
        NPD_TASK_ESTIMATED_START_DATE:'NPD_TASK_ESTIMATED_START_DATE',
        NPD_TASK_ESTIMATED_COMPLETION_DATE:'NPD_TASK_ESTIMATED_COMPLETION_DATE'
    },

    MPMSearchOperatorNames :{
        AFTER:'is after',
        BEFORE:'is before'
    },

    MPMSearchOperators :{
        AFTER:'MPM.OPERATOR.DATE.IS_AFTER',
        BEFORE:'MPM.OPERATOR.DATE.IS_BEFORE'
    }

}