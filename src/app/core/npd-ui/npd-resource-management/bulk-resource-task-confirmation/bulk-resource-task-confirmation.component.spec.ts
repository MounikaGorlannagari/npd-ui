import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkResourceTaskConfirmationComponent } from './bulk-resource-task-confirmation.component';

describe('BulkResourceTaskConfirmationComponent', () => {
  let component: BulkResourceTaskConfirmationComponent;
  let fixture: ComponentFixture<BulkResourceTaskConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkResourceTaskConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkResourceTaskConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
