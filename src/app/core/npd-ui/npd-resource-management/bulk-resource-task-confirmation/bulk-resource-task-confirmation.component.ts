import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NpdResourceManagementService } from '../services/npd-resource-management.service';
import { LoaderService, NotificationService } from 'mpm-library';
@Component({
  selector: 'app-bulk-resource-task-confirmation',
  templateUrl: './bulk-resource-task-confirmation.component.html',
  styleUrls: ['./bulk-resource-task-confirmation.component.scss']
})
/**
     * 
     * @author JeevaR
 */
export class BulkResourceTaskConfirmationComponent implements OnInit {

  projectArray = [];
  taskObject : any;
  constructor(
    public dialogRef: MatDialogRef<BulkResourceTaskConfirmationComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,private npdResourceManagementService:NpdResourceManagementService,
    private notificationService: NotificationService,private loaderService: LoaderService
  ) {}

  onClick(decision): void {
    this.dialogRef.close(decision);
    if(decision === 'Yes'){
      this.loaderService.show();
        this.npdResourceManagementService.bulkTaskAssign(this.taskObject).subscribe(response=> {
          this.loaderService.hide();
          this.notificationService.success('Assigned user to the selected Task(s) Succesfully.');
          this.dialogRef.close(true);
        },(error)=> {
          this.notificationService.error('Something went wrong while assigning user to task(s).')
          this.loaderService.hide();
          this.dialogRef.close();
        });
        }
    }
  
  ngOnInit(): void {
    this.projectArray = this.dialogData.projectArray;
    this.taskObject = this.dialogData.taskObject;
  }

}
