import { DatePipe } from '@angular/common';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { GanttComponent, getTaskData } from '@syncfusion/ej2-angular-gantt';
import * as moment from 'moment';
import { LoaderService, IndexerService, TaskService, ProjectService, NotificationService, ProjectConstant, SearchRequest, SearchResponse, MPM_LEVELS, SearchConfigConstants, OTMMMPMDataTypes, MPMFieldConstants, MPMSearchOperatorNames, MPMSearchOperators, FieldConfigService, OTMMService, ViewConfigService, ViewConfig, SearchChangeEventService, FacetChangeEventService, MPMField, SharingService, ApplicationConfigConstants, IndexerDataTypes, FacetFieldCondition, TaskConstants } from 'mpm-library';

import { Observable, Subject } from 'rxjs';
import { GROUP_BY_ALL_FILTERS } from 'src/app/core/mpm/deliverables-task-view/groupByFilter/GroupByFilter';
import { GroupByFilterTypes } from 'src/app/core/mpm/deliverables-task-view/groupByFilter/GroupByFilterTypes';
import { TaskSearchConditions } from 'src/app/core/mpm/deliverables-task-view/object/DeliverableTasksViewComponentParams';
import { TaskCreationModalComponent } from 'src/app/core/mpm/project-view/task-creation-modal/task-creation-modal.component';
import { ProjectTaskSearchConditions, ResourceManagementConstants } from '../constants/ProjectTaskViewParams';
import { NpdResourceManagementService } from '../services/npd-resource-management.service';
import { Subscription, forkJoin } from 'rxjs';
import { SearchDataService } from 'mpm-library';
import { MediaMatcher } from '@angular/cdk/layout';
import { F, I, P } from '@angular/cdk/keycodes';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ThrowStmt } from '@angular/compiler';
import { MonsterUtilService } from '../../services/monster-util.service';
import { takeUntil } from 'rxjs/operators'
import { ProjectTaskTableComponent } from '../project-task-table/project-task-table.component';
import { RouteService } from 'src/app/core/mpm/route.service';
import { NpdTaskService } from '../../npd-task-view/services/npd-task.service';
import { NPDFacetService } from 'src/app/core/mpm-lib/facet/facet.service';
@Component({
  selector: 'app-project-tasks-layout',
  templateUrl: './project-tasks-layout.component.html',
  styleUrls: ['./project-tasks-layout.component.scss']
})
export class ProjectTasksLayoutComponent implements OnInit, AfterViewInit {
  private unsubscribe$: Subject<any> = new Subject<any>();
  @ViewChild('projectTaskTableComponent')
  projectTaskTableComponent: ProjectTaskTableComponent;

  resizeWidth;
  resizeInnerWidth = [];
  showDragContent = false;
  @Input() startDate;
  @Input() endDate;
  @Output() totalDataListHandler = new EventEmitter<any>();
  @Output() refreshData = new EventEmitter<any>();
  @Input() paginator;
  @Input() selectedGroupByFilter;
  @Output() resetPagination = new EventEmitter();
  @Input() displayableFields;
  @Input() columnChooserFields;
  @Input() columnChooserFieldsOfTasks;
  @Output() orderedDisplayableFields = new EventEmitter<any>();
  @Input() searchName;
  @Input() savedSearchName;
  @Input() advSearchData;
  @Input() facetData;
  @Input() userPreferenceFreezeCount;
  drag: number = 300;
  scrollLeftQuad1;
  scrollLeftQuad2;
  rstartDate;
  rendDate;
  dateArray: any = [];
  editTaskAdjustmentPoint;
  private subscriptions: Array<Subscription> = [];
  isPageRefresh = true;
  isRoute = false;
  myTaskViewConfigDetails: ViewConfig = null;
  flagChangedColor = false;
  facetExpansionState = false;
  mobileQuery: MediaQueryList;
  @ViewChild('scrollQuad1') scrollLeftQuad1Px: ElementRef;
  @ViewChild('scrollQuad2') scrollLeftQuad2Px: ElementRef;
  @ViewChild('head') defaultWidthOfHead: ElementRef;
  @ViewChild('content') defaultWidthOfContent: ElementRef;
  private mobileQueryListener: () => void;
  sortableFields: Array<any> = [];
  selectedSort: MatSort = new MatSort();
  selectAllChecked;
  selection = new SelectionModel<any>(true, []);
  @Output() selectedProjectsCallBackHandler = new EventEmitter<any>();
  appConfig: any;
  index: number;
  sortFun: string;
  fieldId: any;
  paginationCliked: boolean;
  isResetView: boolean = false;
  constructor(public npdTaskService: NpdTaskService, private mpmRouteService: RouteService, private loaderService: LoaderService, private indexerService: IndexerService,
    private taskService: TaskService, public activatedRoute: ActivatedRoute, private projectService: ProjectService, private dialog: MatDialog,
    private notificationService: NotificationService, public datepipe: DatePipe, private npdResourceManagementService: NpdResourceManagementService,
    private fieldConfigService: FieldConfigService, public searchDataService: SearchDataService, private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher, private facetChangeEventService: FacetChangeEventService, private sharingService: SharingService,
    private monsterUtilService: MonsterUtilService, private npdFacetService: NPDFacetService,
    public otmmService: OTMMService, public viewConfigService: ViewConfigService, public searchChangeEventService: SearchChangeEventService) {
    this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
    if (this.mobileQuery.matches) {
      // tslint:disable-next-line: deprecation
      this.mobileQuery.addListener(this.mobileQueryListener);
    }
    this.getStartEndDate();
    this.onload();
    this.getPaintedData();
    this.npdResourceManagementService.resizeWidth.subscribe(width => console.log(this.resizeWidth = width));
    this.npdResourceManagementService.resizeInnerWidth.subscribe(data => console.log(this.resizeInnerWidth[data['index']] = data['value']));
  }
  level;
  viewConfig;
  skip: number;

  @Input() searchConditions = [];
  @Input() facetRestrictionList;
  searchConfigId = -1;
  projectListData = [];
  totalListDataCount;

  selectedListFilter;
  selectedListDependentFilter;
  selectedTaskFilters;
  defaultWidth;
  defaultWidthContent;
  widthOfSecondQuad;
  momentPtr = moment;

  @ViewChild('gantt', { static: true })
  public ganttObj: GanttComponent;
  public data: object[];
  public taskSettings: object;
  public columns: object[];
  public timelineSettings: object;
  public gridLines: string;
  public labelSettings: object;
  public toolbar: string[];
  public editSettings: object;
  editingData = [];
  enableResizing = true;
  selectedProject;
  projectId;
  project;
  allProjectTasks = [];
  ruleList = [];
  isEditMode = true;
  public splitterSettings: object;
  public eventMarkers: object[];
  public filterSettings: object;



  //PROJECT_NAME,PROJECT_START_DATE,PROJECT_END_DATE
  @Input() mainHeadercolumnsToDisplay = [];//{ id: 'ID', name: 'Id' }, { id: 'PROJECT_NAME', name: 'Name' }
  @Input() subLevelHeadercolumnsToDisplay = [];
  projectcolumnsToDisplay = [];
  taskColumnsToDisplay = []

  projectData = [];
  calendarData = [];
  getProperty(displayColumn: MPMField, projectData: any): string {
    return this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData);
  }
  getProjectTask(project, i) {
    project.show = !project.show;
    this.getPaintedData();
  }

  editTask(eventData) {

    this.npdTaskService.setEditTaskHeaderData(eventData)
    if (eventData && eventData?.CONTENT_TYPE == OTMMMPMDataTypes.TASK) {
      this.getProjectDetails(eventData)
    }
  }
  alltask = [];
  top = 100;
  getProjectDetails(eventData) {

    //open dialog only for task
    let projectId = eventData.PROJECT_ITEM_ID.split('.')[1];
    let projectItemId = eventData?.PROJECT_ITEM_ID

    let keyWord = '';
    let sortTemp = [];
    // let test = []
    // let com={}
    //  this.taskConfig.taskTotalCount = null;
    let projectSearch = null;

    let searchConditionList = this.fieldConfigService.formIndexerIdFromMapperId(TaskConstants.GET_ALL_TASKS_SEARCH_CONDITION_LIST.search_condition_list.search_condition);
    projectSearch = this.fieldConfigService.createConditionObject(
      MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, projectItemId);
    if (projectSearch) {
      searchConditionList.push(projectSearch);
    }

    if (projectId) {
      this.loaderService.show();
      this.projectService.getProjectById(projectId)
        .subscribe(projectObj => {
          this.loaderService.hide();
          if (projectObj) {
            this.selectedProject = projectObj;
            const parameters: SearchRequest = {
              search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
              keyword: keyWord,
              search_condition_list: {
                search_condition: searchConditionList
              },
              facet_condition_list: {
                facet_condition: []
              },
              sorting_list: {
                sort: sortTemp
              },
              cursor: {
                page_index: this.skip,
                page_size: this.top
              }
            };
            this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
              const data = [];
              response.data.forEach(element => {
                if (element.IS_BULK_UPDATE_TASK !== 'true') {
                  data.push(element);
                }
              });

              this.alltask = data
              const dialogRef = this.dialog.open(TaskCreationModalComponent, {
                width: '38%',
                disableClose: true,
                data: {
                  projectId: projectId,
                  isTemplate: false,
                  taskId: eventData.ID,
                  projectObj: this.selectedProject,
                  isTaskActive: eventData.IS_ACTIVE_TASK,
                  isApprovalTask: false,
                  selectedTask: eventData,
                  selectedtask: eventData,
                  modalLabel: 'Edit Task',
                  alltask: this.alltask

                }
              });
              dialogRef.afterClosed().subscribe(result => {
                if (result) {
                  //this.refresh();
                  this.paginationCliked = true;
                  this.refreshData.emit();
                }
              });


              /*    this.taskConfig.taskTotalCount = response.cursor.total_records;
                 const ownerFeild: MPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_NAME, MPM_LEVELS.TASK);
                 this.taskConfig.taskList.map(task => {
                     const resourceName = this.fieldConfigService.getFieldValueByDisplayColumn(ownerFeild, task);
                     const isExist = this.taskConfig.taskOwnerResources.find(element => element === resourceName);
                     if (!isExist && resourceName) {
                         this.taskConfig.taskOwnerResources.push(resourceName);
                     }
                 });
                 this.taskGridComponent.refreshChild(this.taskConfig);
                 this.loaderService.hide(); */


            }, () => {
              this.loaderService.hide();
            });

          }
        }, (error) => {
          this.loaderService.hide();
          this.notificationService.error('Something went wrong while fetching Project details');
        });
    }
  }

  onSortBy(column, i, sortField) {
    let descFlag = this.mainHeadercolumnsToDisplay[i]['defaultSortArrowShow'] == true;
    if (this.mainHeadercolumnsToDisplay[i]['sortArrow'] == true) {
      descFlag = false;
    }
    this.mainHeadercolumnsToDisplay = this.mainHeadercolumnsToDisplay.map(ele => {
      ele['defaultSortArrowShow'] = false
      return ele;
    });
    this.mainHeadercolumnsToDisplay[i]['defaultSortArrowShow'] = true;
    this.mainHeadercolumnsToDisplay[i]['sortArrow'] = this.mainHeadercolumnsToDisplay[i]?.sortArrow == undefined ? this.mainHeadercolumnsToDisplay[i]['sortArrow'] = false : this.mainHeadercolumnsToDisplay[i]['sortArrow'] == false ? this.mainHeadercolumnsToDisplay[i]['sortArrow'] = true : this.mainHeadercolumnsToDisplay[i]['sortArrow'] = false;
    this.fieldId = this.mainHeadercolumnsToDisplay[i]["MPM_Fields_Config-id"].Id;
    if (descFlag || i == this.index) {
      this.sortFun = 'DSC';
    } else {
      this.sortFun = sortField;
    }
    this.getGroupByFilterDetails(true);
    this.index = i;
  }
  showSortIcon(column, i) {
    this.mainHeadercolumnsToDisplay[i]['sortArrowShow'] = true;
  }
  hideSortIcon(column, i) {
    this.mainHeadercolumnsToDisplay[i]['sortArrowShow'] = false;
  }
  updateTask(requestObj) {
    this.taskService.updateTask(requestObj).subscribe(response => {
      this.loaderService.hide();
      if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
        const eventData = { message: 'Task updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
        this.notificationService.success(eventData.message);
      } else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
        && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
        const eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
        this.notificationService.error(eventData.message);
      } else {
        const eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
        this.notificationService.error(eventData.message);
      }
    }, error => {
      const eventData = { message: 'Something went wrong while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
      this.notificationService.error(eventData.message);
    });
  }


  getTasks() {
    this.onScrollQuad();
    this.projectListData = [];
    this.totalListDataCount = 0;
    this.facetChangeEventService.updateFacetFilter([]);
    let keyWord = '';
    let startDate = this.startDate?.toISOString() || this.rstartDate?.toISOString()
    let endDate = this.endDate?.toISOString() || this.rendDate?.toISOString()
    const conditionObject: ProjectTaskSearchConditions = this.npdResourceManagementService.getTaskSearchCondition(this.selectedGroupByFilter, this.selectedListDependentFilter,
      this.level, startDate,
      endDate, undefined, false);
    const otmmMPMDataTypes = OTMMMPMDataTypes.TASK;
    let sortTemp = [];

    const dataSet = this.fieldConfigService.formrequiredDataForLoadView(this.myTaskViewConfigDetails, true);
    if (dataSet?.SORTABLE_FIELDS?.length > 0) {
      const filteredDataSetFag = dataSet.SORTABLE_FIELDS.findIndex(ele => ele.fieldId == this.fieldId)
      let sortOptions = {
        option: dataSet?.SORTABLE_FIELDS?.[filteredDataSetFag],
        sortType: ['ASC', 'asc'].some(ele => ele == this.sortFun) ? 'asc' : 'desc'
      };
      if (!(sortOptions.option)) {
        if (dataSet.DEFAULT_SORT_FIELD) {
          dataSet.DEFAULT_SORT_FIELD['option']['sortType'] = dataSet.DEFAULT_SORT_FIELD?.option?.sortType ? dataSet.DEFAULT_SORT_FIELD?.option?.sortType.toUpperCase() : null;
          this.mainHeadercolumnsToDisplay.forEach(ele => {
            if (ele["MPM_Fields_Config-id"].Id == dataSet.DEFAULT_SORT_FIELD?.option?.fieldId) {
              ele['defaultSortArrowShow'] = true;
            } else {
              ele['defaultSortArrowShow'] = false;
            }
          });
        }
      }
      sortOptions = sortOptions.option ? sortOptions : dataSet.DEFAULT_SORT_FIELD;

      sortTemp = sortOptions ? [{
        field_id: sortOptions.option?.indexerId,
        order: ['ASC', 'asc'].some(ele => ele == sortOptions.sortType) ? 'asc' : 'desc'//this.selectedSort.direction
      }] : [];
    }

    ResourceManagementConstants.CHECKPOINT_TASKS.forEach((task) => {
      conditionObject.TASK_CONDITION?.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_NAME, MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT,
        MPMSearchOperators.AND, task));
    });
    //   conditionObject.TASK_CONDITION.push({
    //     field_id : "ID",
    //     relational_operator : "and",
    //     relational_operator_id : "MPM.OPERATOR.CHAR.IS_NOT_EMPTY",
    //     relational_operator_name : "is not empty",
    //     type: "string",
    // });

    if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
      keyWord = this.searchConditions[0].keyword;
    } else if (this.searchConditions && this.searchConditions.length > 0) {
      conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(this.searchConditions);
    }
    let displayableFieldsFiltered;
    if (!this.isResetView) {
      displayableFieldsFiltered = this.columnChooserFieldsOfTasks.filter(ele => {
        if (ele.selected == true) {
          return ele;
        }
      });
    } else {
      displayableFieldsFiltered = this.columnChooserFieldsOfTasks;
    }
    // const displayableFieldsFiltered = this.columnChooserFieldsOfTasks.filter(ele => {
    //   if (ele.selected == true) {
    //     return ele;
    //   }
    // });
    const displayableFields = displayableFieldsFiltered.map(ele => {
      return { field_id: ele.INDEXER_FIELD_ID }
    });
    displayableFields.push({ field_id: 'PROJECT_ITEM_ID' })
    displayableFields.push({ field_id: 'TASK_OWNER_ID' })
    displayableFields.push({ field_id: 'NPD_TASK_RM_USER_ID' })
    displayableFields.push({ field_id: 'NPD_PROJECT_BUSINESS_ID' })
    displayableFields.push({ field_id: 'NPD_PROJECT_ROLE_ID' })
    displayableFields.push({ field_id: 'NPD_TASK_RM_USER' })
    displayableFields.push({ field_id: 'NPD_TASK_RM_ROLE' })
    const parameters: SearchRequest = {
      search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
      keyword: keyWord,
      displayable_field_list: {
        displayable_fields: [
          ...displayableFields
        ]
      },
      search_condition_list: {
        search_condition: [...conditionObject.TASK_CONDITION]
      },
      facet_condition_list: {
        facet_condition: (this.facetRestrictionList && this.facetRestrictionList.facet_condition) ? this.facetRestrictionList.facet_condition : []
      },
      sorting_list: {
        sort: sortTemp
      },
      cursor: {
        page_index: this.paginator.skip,
        page_size: this.paginator.top
      }
    };
   

    this.loaderService.show();
    this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
      this.projectData = JSON.parse(JSON.stringify(response.data));
      this.calendarData = JSON.parse(JSON.stringify(response.data));
      this.totalListDataCount = response.cursor.total_records;
      this.totalDataListHandler.emit(this.totalListDataCount);
      this.npdFacetService.updateOrderdDisplayableFields(displayableFields);
      this.facetChangeEventService.updateFacetFilter(response.facet_field_response_list ? response.facet_field_response_list : []);
      if (this.projectData.length > 0) {
        this.projectData.forEach((projectData, index) => {
          projectData.show = false;
          projectData.arr = [];
          this.calendarData[index].show = false;
        });
      }
      this.getPaintedData();
      this.loaderService.hide();
    }, error => {
      this.notificationService.error('Failed while getting projects');
      this.loaderService.hide();
    });
  }

  getBaseFacetConstraint() {
    let facetFilter: FacetFieldCondition;
    facetFilter = {
      type: ResourceManagementConstants.FACET_CONSTRAINTS.type,
      behavior: ResourceManagementConstants.FACET_CONSTRAINTS.behavior,
      values: []
    }
    return facetFilter;
  }

  groupTaskFacetFilters() {//applied only for group by project roles & task name conditions
    let groupByTaskRoleFilterCondition: FacetFieldCondition = {};
    let groupByTaskNameFilterCondition: FacetFieldCondition = {};
    let selectedFacetFilters: FacetFieldCondition[] = [];

    if (this.selectedTaskFilters?.selectedMultiRoles?.length > 0) {
      groupByTaskRoleFilterCondition = Object.assign(this.getBaseFacetConstraint(), groupByTaskRoleFilterCondition);
      /*  groupByTaskRoleFilterCondition.field_id = ResourceManagementConstants.FACET_CONSTRAINTS.TASK_FIELDS.TASK_ROLE_ID//MPMFieldConstants.MPM_TASK_FIELDS.TASK_ROLE_ID
       this.selectedTaskFilters?.selectedMultiRoles?.forEach((role,index) => {
         groupByTaskRoleFilterCondition.values.push(role?.['MPM_Team_Role_Mapping-id']?.Id);
       }); */
      groupByTaskRoleFilterCondition.field_id = ResourceManagementConstants.FACET_CONSTRAINTS.TASK_FIELDS.TASK_ROLE_VALUE;//TASK_ROLE_NAME
      this.selectedTaskFilters?.selectedMultiRoles?.forEach((role, index) => {
        groupByTaskRoleFilterCondition.values.push(role?.NAME);
      });
      selectedFacetFilters.push(groupByTaskRoleFilterCondition);
    }

    if (this.selectedTaskFilters?.selectedMultiTasks?.length > 0) {
      groupByTaskNameFilterCondition = Object.assign(this.getBaseFacetConstraint(), groupByTaskNameFilterCondition);
      groupByTaskNameFilterCondition.field_id = MPMFieldConstants.MPM_TASK_FIELDS.TASK_NAME
      this.selectedTaskFilters?.selectedMultiTasks?.forEach((task, index) => {
        groupByTaskNameFilterCondition.values.push(task);
      })
      selectedFacetFilters.push(groupByTaskNameFilterCondition);
    }
    return selectedFacetFilters;
  }

  groupTaskFilters() {
    let groupByTaskFilterConditions = []
    if (this.selectedTaskFilters?.selectedMultiRoles?.length > 0) {
      this.selectedTaskFilters?.selectedMultiRoles?.forEach((role, index) => {
        if (index == 0) {
          groupByTaskFilterConditions.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_ROLE_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
            MPMSearchOperators.AND, role?.['MPM_Team_Role_Mapping-id']?.Id));
        } else {
          groupByTaskFilterConditions.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_ROLE_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR, role?.['MPM_Team_Role_Mapping-id']?.Id));
        }

      })
    }

    if (this.selectedTaskFilters?.selectedMultiTasks?.length > 0) {
      this.selectedTaskFilters?.selectedMultiTasks?.forEach((task, index) => {
        if (index == 0) {
          groupByTaskFilterConditions.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_NAME, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
            MPMSearchOperators.AND, task));
        } else {
          groupByTaskFilterConditions.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_NAME, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR, task));
        }

      })
    }
    return groupByTaskFilterConditions;
  }

  getProjects() {//group by projects
    this.onScrollQuad();
    this.projectListData = [];
    this.totalListDataCount = 0;
    this.facetChangeEventService.updateFacetFilter([]);
    let keyWord = '';
    /*  const conditionObject: ProjectTaskSearchConditions = this.npdResourceManagementService.getSearchCondition(this.selectedListFilter, this.selectedListDependentFilter,
       this.level, this.momentPtr.utc(this.startDate).format('YYYY-MM-DDT00:00:00.000') + 'Z',
       this.momentPtr.utc(this.endDate).format('YYYY-MM-DDT00:00:00.000') + 'Z',undefined,false); */
    let startDate = this.startDate?.toISOString() || this.rstartDate?.toISOString()
    let endDate = this.endDate?.toISOString() || this.rendDate?.toISOString();
    const conditionObject: ProjectTaskSearchConditions = this.npdResourceManagementService.getSearchCondition(this.selectedListFilter, this.selectedListDependentFilter,
      this.level, startDate,
      endDate, undefined, false);
    const otmmMPMDataTypes = OTMMMPMDataTypes.TASK;
    let sortTemp = [];
    const dataSet = this.fieldConfigService.formrequiredDataForLoadView(this.myTaskViewConfigDetails, true);
    if (dataSet?.SORTABLE_FIELDS?.length > 0) {
      const filteredDataSetFag = dataSet.SORTABLE_FIELDS.findIndex(ele => ele.fieldId == this.fieldId);
      let sortOptions = {
        option: dataSet?.SORTABLE_FIELDS?.[filteredDataSetFag],
        sortType: ['ASC', 'asc'].some(ele => ele == this.sortFun) ? 'asc' : 'desc'
      };
      if (!(sortOptions.option)) {
        if (dataSet.DEFAULT_SORT_FIELD) {
          dataSet.DEFAULT_SORT_FIELD['option']['sortType'] = dataSet.DEFAULT_SORT_FIELD?.option?.sortType ? dataSet.DEFAULT_SORT_FIELD?.option?.sortType.toUpperCase() : null;
          this.mainHeadercolumnsToDisplay.forEach(ele => {
            if (ele["MPM_Fields_Config-id"].Id == dataSet.DEFAULT_SORT_FIELD?.option?.fieldId) {
              ele['defaultSortArrowShow'] = true;
            } else {
              ele['defaultSortArrowShow'] = false;
            }
          });
        }
      }
      sortOptions = sortOptions.option ? sortOptions : dataSet.DEFAULT_SORT_FIELD;
      sortTemp = sortOptions ? [{
        field_id: sortOptions.option?.indexerId,
        order: ['ASC', 'asc'].some(ele => ele == sortOptions.sortType) ? 'asc' : 'desc'//this.selectedSort.direction
      }] : [];
    }

    //if (this.selectedSort && this.selectedSort.active) {
    /* const sortOption = this.sortableFields.find(option => option.name === this.selectedSort.active);
    sortTemp = sortOption ? [{
        field_id: sortOption.indexerId,
        order: this.selectedSort.direction
    }] : []; */


    let groupByTaskFilterConditions = []
    groupByTaskFilterConditions = this.groupTaskFilters()
    if (groupByTaskFilterConditions.length > 0) {
      conditionObject.GROUP_PROJECT_TASK_CONDITION = conditionObject.GROUP_PROJECT_TASK_CONDITION.concat(groupByTaskFilterConditions);
    }

    if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
      keyWord = this.searchConditions[0].keyword;
    } else if (this.searchConditions && this.searchConditions.length > 0) {
      conditionObject.PROJECT_CONDITION = conditionObject.PROJECT_CONDITION.concat(this.searchConditions);
    }
    let displayableFieldsFiltered;
    if (!this.isResetView) {
      displayableFieldsFiltered = this.columnChooserFields.filter(ele => {
        if (ele.selected == true) {
          return ele;
        }
      });
    } else {
      displayableFieldsFiltered = this.columnChooserFields;
    }

    const displayableFields = displayableFieldsFiltered.map(ele => {
      return { field_id: ele.INDEXER_FIELD_ID }
    });
    const parameters: SearchRequest = {
      search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
      keyword: keyWord,
      grouping: {
        group_by: this.level,
        group_from_type: otmmMPMDataTypes,
        search_condition_list: {
          search_condition: [...conditionObject.GROUP_PROJECT_TASK_CONDITION]//[...conditionObject.DELIVERABLE_CONDTION ]//...this.emailSearchCondition
        }
      },
      displayable_field_list: {
        displayable_fields: [
          ...displayableFields
        ]
      },
      search_condition_list: {
        search_condition: [...conditionObject.PROJECT_CONDITION]//conditionObject.TASK_CONDITION
      },
      facet_condition_list: {
        facet_condition: (this.facetRestrictionList && this.facetRestrictionList.facet_condition) ? this.facetRestrictionList.facet_condition : []
      },
      sorting_list: {
        sort: sortTemp
      },
      cursor: {
        page_index: this.paginator?.skip ? this.paginator.skip : 0,
        page_size: this.paginator?.top ? this.paginator.top : 100
      }
    };

    

    this.loaderService.show();
    this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
      this.projectData = JSON.parse(JSON.stringify(response.data));
      this.getcalendarData();
      this.calendarData = JSON.parse(JSON.stringify(response.data));// [...response.data]//response.data.map(ele => ele);
      this.totalListDataCount = response.cursor.total_records;
      this.totalDataListHandler.emit(this.totalListDataCount);
      this.npdFacetService.updateOrderdDisplayableFields(displayableFields);
      this.facetChangeEventService.updateFacetFilter(response.facet_field_response_list ? response.facet_field_response_list : []);
      if (this.projectData.length > 0) {
        this.projectData.forEach((projectData, index) => {
          projectData.show = false;
          this.calendarData[index].show = false;
          projectData['emptyColumns'] = {
            show: false,
            row: '',
          }
          projectData['paintedValues'] = {
            show: false,
            row: '',
          }
        });
      }
      this.getPaintedData();
      if (response.data.length == 0) {
        this.loaderService.hide();
      }
    }, error => {
      this.notificationService.error('Failed while getting projects');
      this.loaderService.hide();
    });
  }

  getcalendarData() {//project expansion-tasks

    let keyWord = '';
    let startDate = this.startDate?.toISOString() || this.rstartDate?.toISOString()
    let endDate = this.endDate?.toISOString() || this.rendDate?.toISOString()
    const conditionObject: ProjectTaskSearchConditions = this.npdResourceManagementService.getTaskSearchCondition(this.selectedListFilter, this.selectedListDependentFilter,
      MPM_LEVELS.TASK, startDate,
      endDate, undefined, false);
    let allProjectSearchConditions = [];
    let facetConditions = [];
    let searchConfigId;
    let displayableFieldforTask = [];
    this.viewConfigService.getAllDisplayFeildForViewConfig(SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_TASK).subscribe((viewDetails: ViewConfig) => {
      searchConfigId = viewDetails?.R_PO_ADVANCED_SEARCH_CONFIG;
      displayableFieldforTask = viewDetails.LIST_FIELD_ORDER.split(',')

    });

    ResourceManagementConstants.CHECKPOINT_TASKS.forEach((task) => {
      conditionObject.TASK_CONDITION?.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_NAME, MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT,
        MPMSearchOperators.AND, task));
    });
    if (this.projectData.length > 0) {
      this.projectData.forEach((project, index) => {
        if (index == 0) {
          allProjectSearchConditions.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
            MPMSearchOperators.AND, project?.ITEM_ID));
        } else {
          allProjectSearchConditions.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR, project?.ITEM_ID));
        }
      })
      conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(allProjectSearchConditions);
    }
    /*groupByTaskFilterConditions = this.groupTaskFilters();
    if(groupByTaskFilterConditions.length > 0) {
          conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(groupByTaskFilterConditions);
     } */
    facetConditions = this.groupTaskFacetFilters();

    const dataSet = this.fieldConfigService.formrequiredDataForLoadView(this.myTaskViewConfigDetails, true);
    let sortTemp = [];
    if (dataSet?.SORTABLE_FIELDS?.length > 0) {
      sortTemp = [{
        field_id: ResourceManagementConstants.SORT_FIELDS.TASK.TASK_ESTIMATED_START_DATE,//"TASK_DUE_DATE",//"TASK_NAME",
        order: ResourceManagementConstants.SORT_ORDER.SORT_ASC
      }];
    }
    // const displayableFieldsFiltered = this.columnChooserFieldsOfTasks.filter(ele => {
    //   if (ele.selected == true) {
    //     return ele;
    //   }
    // });
    const displayableFields = []
    displayableFieldforTask.forEach(ele => {
      displayableFields.push({ field_id: ele })
    });
    displayableFields.push({
      fieldId: 'NPD_TASK_IS_CP1_COMPLETED'
    })
    let parameters: SearchRequest = {
      search_config_id: searchConfigId ? searchConfigId : null,
      keyword: '',
      displayable_field_list: {
        displayable_fields: [
          ...displayableFields
        ]
      },
      search_condition_list: {
        search_condition: [...conditionObject.TASK_CONDITION]
      },
      facet_condition_list: {
        facet_condition: facetConditions
      },
      sorting_list: {
        sort: sortTemp
      },
      cursor: {
        page_index: 0,
        page_size: 100//200
      }
    };
    // if(displayableFields.length > 1){
    //   parameters = {
    //     ...parameters,
    //     displayable_field_list: {displayable_fields: [
    //       ...displayableFields
    //       ]},
    //   }
    // }
  
    this.loaderService.show();
    this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
      let taskData = [];
      taskData = JSON.parse(JSON.stringify(response.data));
      this.projectData.forEach((project, index) => {
        this.projectData[index].taskData = [];
        this.calendarData[index].taskData = [];
        this.projectData[index].isExpand = false;
        this.calendarData[index].isExpand = false;
        taskData.forEach(task => {
          if (task?.PROJECT_ITEM_ID == project?.ITEM_ID) {
            this.projectData[index].taskData.push(task);
            this.calendarData[index].taskData.push(task);
          }
        })
      })
      this.getPaintedData();
      this.loaderService.hide();
    }, error => {
      this.notificationService.error('Failed while getting tasks');
      this.loaderService.hide();
    });


  }

  setLevel(routedGroupFilter?) {
    // this.selectedGroupByFilter = (routedGroupFilter) ?routedGroupFilter : GROUP_BY_ALL_FILTERS[1]
    if (this.selectedGroupByFilter === GroupByFilterTypes.GROUP_BY_PROJECT) {
      this.level = MPM_LEVELS.PROJECT;
      this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_PROJECT;
    } else if (this.selectedGroupByFilter === GroupByFilterTypes.GROUP_BY_TASK) {
      this.level = MPM_LEVELS.TASK;
      this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_TASK;
    }
  }

  showExpandIcon(data) {
    if (data.CONTENT_TYPE == OTMMMPMDataTypes.PROJECT) {
      return true;
    }
    return false;
  }

  taskBarName(data) {
    if (data?.CONTENT_TYPE == OTMMMPMDataTypes.PROJECT) {
      return data?.['PROJECT_NAME'];
    } else {
      return data?.['TASK_NAME'] + ' - ' + data?.['TASK_DESCRIPTION'];
    }
  }

  taskBarNameStatus(data) {
    if (data?.CONTENT_TYPE == OTMMMPMDataTypes.PROJECT) {
      return data?.['PROJECT_STATUS_VALUE'];
    } else {
      return data?.['TASK_STATUS_VALUE'];
    }
  }

  expandProject(project, index) {

    if (project.CONTENT_TYPE == OTMMMPMDataTypes.PROJECT) {
      this.onScrollQuad();
      this.getProjectTask(project, index);
      //let projectItemId = project?.ITEM_ID;
      /* if(this.projectData[index].isExpand) {
        this.getProjectTask(project,index);
      } else {
        this.getTaskData(project,projectItemId,index);
      } */
    }
  }

  onRowClick(project) {
    if (project.PROJECT_ITEM_ID) {
      this.mpmRouteService.goToProjectDetailViewNewTab(project.PROJECT_ITEM_ID.split(".")[1], false, null);
    }
    else {
      this.mpmRouteService.goToProjectDetailViewNewTab(project.ITEM_ID.split(".")[1], false, null);
    }
  }

  getTaskData(project, projectItemId, index) {

    let keyWord = '';
    //const dates = this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z';

    let startDate = this.startDate?.toISOString() || this.rstartDate?.toISOString()
    let endDate = this.endDate?.toISOString() || this.rendDate?.toISOString()
    const conditionObject: ProjectTaskSearchConditions = this.npdResourceManagementService.getTaskSearchCondition(this.selectedListFilter, this.selectedListDependentFilter,
      MPM_LEVELS.TASK, startDate,
      endDate, undefined, false);

    const otmmMPMDataTypes = OTMMMPMDataTypes.TASK;
    let sortTemp = [];
    const dataSet = this.fieldConfigService.formrequiredDataForLoadView(this.myTaskViewConfigDetails, true);
    if (dataSet?.SORTABLE_FIELDS?.length > 0) {
      sortTemp = [{
        field_id: ResourceManagementConstants.SORT_FIELDS.TASK.TASK_ESTIMATED_START_DATE,//"TASK_DUE_DATE",//"TASK_NAME",
        order: ResourceManagementConstants.SORT_ORDER.SORT_ASC
      }];
    }

    let searchCondition;
    searchCondition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
      MPMSearchOperators.AND, projectItemId);

    conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(searchCondition);

    let groupByTaskFilterConditions = []
    groupByTaskFilterConditions = this.groupTaskFilters();

    if (groupByTaskFilterConditions.length > 0) {
      conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(groupByTaskFilterConditions);
    }
    const displayableFieldsFiltered = this.columnChooserFields.filter(ele => {
      if (ele.selected == true) {
        return ele;
      }
    });
    const displayableFields = displayableFieldsFiltered.map(ele => {
      return { field_id: ele.INDEXER_FIELD_ID }
    });
    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: '',
      displayable_field_list: {
        displayable_fields: [
          ...displayableFields
        ]
      },
      search_condition_list: {
        search_condition: [...conditionObject.TASK_CONDITION]
      },
      facet_condition_list: {
        facet_condition: []
      },
      sorting_list: {
        sort: sortTemp
      },
      cursor: {
        page_index: 0,//this.skip
        page_size: 100
      }
    };
    this.loaderService.show();
    this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
      let taskData = response.data;
      let totalTaskListDataCount = response.cursor.total_records;
      this.loaderService.hide();
      if (taskData.length > 0) {
        this.projectData[index].taskData = [];
        this.calendarData[index].taskData = [];
        this.projectData[index].isExpand = true;
        this.calendarData[index].isExpand = true;
        taskData.forEach((taskData) => {
          this.projectData[index]?.taskData.push({ ...taskData })
          this.calendarData[index]?.taskData.push({ ...taskData })
          this.getPaintedData();
        });
      }
      this.getProjectTask(project, index);

    }, error => {
      this.notificationService.error('Failed while getting tasks');
      this.loaderService.hide();
    });
  }

  public changeTaskBarColor(a, data, status) {
    let style;
    let isOverdue = this.isOverdue(data);
    if (status === "Cancelled") {
      style = "task-cancelled-paint"
      return style;
    } else if (isOverdue && status !== "Approved/Completed") {
      style = "overdue-paint";
      return style;
    }
    else {
      return this.npdResourceManagementService.changeTaskBarColors(status);
    }
  }

  isOverdue(taskData) {
    let currentdate = new Date();
    currentdate.setHours(23, 59, 59, 998);
    let estimatedEndDate
    estimatedEndDate = new Date(taskData)
    return currentdate > estimatedEndDate;
  }

  updateTaskAdjustmentPoint(task, value) {
    if (value?.length > 0) {
      let taskObject = {
        Id: task.ID,
        ItemId: task.ITEM_ID,
        adjustmentPoint: value
      }
      var regex = /^\d*\.?\d*$/;
      if (taskObject.adjustmentPoint.length <= 64) {
        if (regex.test(taskObject.adjustmentPoint)) {
          this.npdResourceManagementService.updateTaskAdjustmentPoint(taskObject).subscribe(response => {
            this.notificationService.success('Task Adjustment point updated succesfully.');
          }, (error) => {
            this.notificationService.error('Something went wrong while updating Adjustment point.');
          });
        } else {
          this.notificationService.error('Invalid Task Adjustment point');
        }
      } else {
        this.notificationService.error('Task Adjustment point exceed max limit');
      }
    }
  }


  setListViewConfig(viewDetails) {

    /*       if(this.selectedGroupByFilter === GroupByFilterTypes.GROUP_BY_TASK ) {
            //this.mainHeadercolumnsToDisplay = this.getListConfig(viewDetails);
          } else {
            this.viewConfigService.getAllDisplayFeildForViewConfig(SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_TASK).subscribe((viewDetails: ViewConfig) => {
              this.subLevelHeadercolumnsToDisplay = this.getListConfig(viewDetails);
            });
            //this.mainHeadercolumnsToDisplay = this.getListConfig(viewDetails);
          } */
  }
  /*
      getListConfig(viewDetails) {
          let selectedViewDetails : ViewConfig
          selectedViewDetails = viewDetails;
          let displayableFields= [];
          displayableFields = selectedViewDetails.R_PM_LIST_VIEW_MPM_FIELDS;
          const order = selectedViewDetails.LIST_FIELD_ORDER.split(",");
          displayableFields.sort(function (a, b) {
                  return order.indexOf(a.MAPPER_NAME) - order.indexOf(b.MAPPER_NAME);
          })
          return displayableFields;
      } */


  getStartEndDate() {
    this.rstartDate = new Date(this.getSetDate(1));
    let month = new Date().getMonth();
    if ([1, 3, 5, 7, 8, 10, 12].some(ele => ele == month + 1)) {
      this.rendDate = new Date(this.getSetEndDate(31));
    } else if ([4, 6, 9, 11].some(ele => ele == month + 1)) {
      this.rendDate = new Date(this.getSetEndDate(30));
    } else {
      this.rendDate = new Date(this.getSetEndDate(28));
    }
  }

  getSetEndDate(setDate) {
    const day = new Date(new Date().setDate(setDate)).getDay();
    let date;
    if (day) {
      date = new Date().setDate(setDate) + ((6 - day) * 24 * 60 * 60 * 1000);
    } else {
      date = new Date().setDate(setDate);
    }
    return date;
  }

  getSetDate(setDate) {
    const day = new Date(new Date().setDate(setDate)).getDay();
    let date;
    if (day) {
      date = new Date().setDate(setDate) - ((6 - (6 - (day - 1))) * 24 * 60 * 60 * 1000);
    } else {
      date = new Date().setDate(setDate) - (6 * 24 * 60 * 60 * 1000);
    }
    // date = new Date(new Date(date).setHours(-1)).getTime();
    return date;
  }

  onload() {
    this.dateArray = [];
    let startTimeStamp = this.rstartDate.getTime();
    this.getUpdatedArray(0, startTimeStamp);
    let endTimeStamp = this.rendDate.getTime();
    let timeStamp: number;
    timeStamp = startTimeStamp;
    let i = 1;
    do {
      timeStamp = timeStamp + 24 * 60 * 60 * 1000;
      this.getUpdatedArray(i, timeStamp);
      i++;
    } while (timeStamp < endTimeStamp);
  }
  getUpdatedArray(i, timeStamp) {
    const day = new Date(timeStamp).getDate();
    this.dateArray[i] = {};
    this.dateArray[i]['day'] = day;
    this.dateArray[i]['month'] = new Date(timeStamp).getMonth() + 1;
    this.dateArray[i]['weekDay'] = this.getDayOfWeek(new Date(timeStamp));
    // if( this.dateArray[i]['weekDay'] == 'Su') {
    //   if(i){
    //     // this.dateArray[i]['weekNo'] = this.dateArray[i - 1]['weekNo'];
    //   }
    // } else {
    //   this.dateArray[i]['weekNo'] = moment(new Date(timeStamp)).isoWeek();
    // }
    this.dateArray[i]['weekNo'] = moment(new Date(timeStamp)).isoWeek();
    // this.dateArray[i]['year'] = new Date(timeStamp).getFullYear();
    this.dateArray[i]['year'] = moment(new Date(timeStamp)).isoWeekYear();
    this.dateArray[i]['date'] = new Date(timeStamp);
  }
  getDayOfWeek(date) {
    const dayOfWeek = new Date(date).getDay();
    return isNaN(dayOfWeek)
      ? null
      : ['Su', 'M', 'T', 'W', 'Th', 'F', 'Sa'][dayOfWeek];
  }


  getWeekNo(i) {
    return this.dateArray[i]?.weekNo == this.dateArray[i + 1]?.weekNo;
  }

  getProjectDateArray(startDate, endDate) {
    let projectDateArray = [];
    if (new Date().getTimezoneOffset() > 0) {
      startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + 1);
      endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() + 1);
    }
    let startTimeStamp = startDate.getTime();
    // this.getUpdatedProjectDateArray(0, startTimeStamp, projectDateArray);
    let endTimeStamp = endDate.getTime();
    let timeStamp: number;
    timeStamp = startTimeStamp;
    let i = 0;
    do {
      if (!(i == 0)) {
        timeStamp = timeStamp + 24 * 60 * 60 * 1000;
      }
      this.getUpdatedProjectDateArray(i, timeStamp, projectDateArray);
      i++;
    } while (timeStamp < endTimeStamp);
    return projectDateArray;
  }
  getUpdatedProjectDateArray(i, timeStamp, projectDateArray) {
    const day = new Date(timeStamp).getDate();
    projectDateArray[i] = {};
    projectDateArray[i]['day'] = day;
    projectDateArray[i]['month'] = new Date(timeStamp).getMonth() + 1;
    // projectDateArray[i]['year'] = new Date(timeStamp).getFullYear();
    projectDateArray[i]['year'] = moment(new Date(timeStamp)).isoWeekYear();
    projectDateArray[i];
  }
  getPaintedData() {
    if (this.selectedGroupByFilter == GroupByFilterTypes.GROUP_BY_PROJECT) {
      this.projectData.forEach(item => {
        item['arr'] = [];
        item['paintedData'] = [];
        item['taskPaintedData'] = [];
        item['arrTasks'] = [];
        let startDate = new Date(item['PROJECT_START_DATE']);
        let endDate = new Date(item['PROJECT_END_DATE']);
        if (item?.taskData) {
          item.taskData.forEach(segment => {

            segment['arr'] = [];
            segment['paintedData'] = [];
            let finalStartDate;
            let finalEndDate;
            const flagS = (segment['NPD_TASK_ACTUAL_START_DATE'] == '') || (segment['NPD_TASK_ACTUAL_START_DATE'] == 'NA') || (segment['NPD_TASK_ACTUAL_START_DATE'] == null) || (segment['NPD_TASK_ACTUAL_START_DATE'] == undefined)
            const flagD = (segment['NPD_TASK_ACTUAL_DUE_DATE'] == '') || (segment['NPD_TASK_ACTUAL_DUE_DATE'] == 'NA') || (segment['NPD_TASK_ACTUAL_DUE_DATE'] == null) || (segment['NPD_TASK_ACTUAL_DUE_DATE'] == undefined)
            if (!flagS && !flagD) {
              finalStartDate = segment['NPD_TASK_ACTUAL_START_DATE'];
              finalEndDate = segment['NPD_TASK_ACTUAL_DUE_DATE'];
            } else if (flagS && flagD) {
              finalStartDate = segment['TASK_START_DATE'];
              finalEndDate = segment['TASK_DUE_DATE'];
            } else if (flagS && !flagD) {
              finalStartDate = segment['TASK_START_DATE'];
              finalEndDate = segment['NPD_TASK_ACTUAL_DUE_DATE'];
            } else if (!flagS && flagD) {
              finalStartDate = segment['NPD_TASK_ACTUAL_START_DATE'];
              let duration = segment['TASK_DURATION'];
              let finalTimeStamp = new Date(finalStartDate).getTime() + (new Date().getTimezoneOffset() * 60 * 1000) - 20;
              finalEndDate = new Date(finalTimeStamp + (((+duration * 7) - 1) * 24 * 60 * 60 * 1000));
            }

            let taskStartDate = new Date(finalStartDate);
            let taskEndDate = new Date(finalEndDate);
            let taskDateArray = [];
            taskDateArray = this.getProjectDateArray(taskStartDate, taskEndDate);
            this.dateArray.forEach((piece, i) => {
              let bool = taskDateArray.some(section => {
                // if (segment["TASK_NAME"].includes('cp') || segment["TASK_NAME"].includes('CP')) {
                //   return false;
                // } else {
                //   if ((section.day == piece.day) && (section.month == piece.month) && (section.year == piece.year)) {
                //     return true;
                //   } else {
                //     return false;
                //   }
                // }
                if ((section.day == piece.day) && (section.month == piece.month) && (section.year == piece.year)) {
                  return true;
                } else {
                  return false;
                }
              })

              if (bool) {
                segment['arr'][i] = {}
                segment['arr'][i] = {
                  cell: '1',
                  taskName: `${segment['TASK_NAME']} - ${segment['TASK_DESCRIPTION']}`,
                  taskStatusValue: segment['TASK_STATUS_VALUE']
                }
              } else {
                segment['arr'][i] = {}
                segment['arr'][i] = {
                  cell: '0',
                }
              }
            })
            let j = 0;
            segment['arr'].forEach((sec, i) => {
              if (sec?.cell == segment['arr'][i + 1]?.cell) {
                segment['paintedData'][j] = segment['paintedData'][j] ? segment['paintedData'][j] : {};
                segment['paintedData'][j] = {
                  rowLength: segment['paintedData'][j].rowLength ? segment['paintedData'][j].rowLength + 20 : 20,
                  count: segment['paintedData'][j].count ? segment['paintedData'][j].count + 1 : 1,
                  cell: sec?.cell
                };
              } else {
                if (i != (this.dateArray.length - 1)) {
                  segment['paintedData'][j] = segment['paintedData'][j] ? segment['paintedData'][j] : {};
                  segment['paintedData'][j] = segment['paintedData'][j] ? {
                    rowLength: segment['paintedData'][j].rowLength ? segment['paintedData'][j].rowLength + 20 : 20,
                    count: segment['paintedData'][j].count ? segment['paintedData'][j].count + 1 : 1,
                    cell: segment['arr'][i]?.cell
                  } : {};
                  j++;
                } else {
                  segment['paintedData'][j] = segment['paintedData'][j] ? segment['paintedData'][j] : {};
                  segment['paintedData'][j] = {
                    rowLength: segment['paintedData'][j].rowLength ? segment['paintedData'][j].rowLength + 20 : 20,
                    count: segment['paintedData'][j].count ? segment['paintedData'][j].count + 1 : 1,
                    cell: sec?.cell
                  };
                }
              }
            })
            // }
          });
          item.taskData.forEach(obj => {
            this.dateArray.forEach((pie, x) => {
              if (item['arrTasks'][x]?.cell) {
                if (item['arrTasks'][x]?.cell == '0') {
                  item['arrTasks'][x] = {
                    cell: obj['arr'][x].cell,
                    changeColor: false,
                    taskName: obj['arr'][x]?.taskName,
                    taskStatusValue: obj['arr'][x]?.taskStatusValue,
                    taskEstimatedEndDate: obj.NPD_TASK_ESTIMATED_COMPLETION_DATE
                  }
                } else {
                  if (item['arrTasks'][x]?.cell == obj['arr'][x].cell) {
                    item['arrTasks'][x] = {
                      cell: obj['arr'][x].cell,
                      changeColor: true,
                      taskName: obj['arr'][x]?.taskName,
                      taskStatusValue: obj['arr'][x]?.taskStatusValue,
                      taskEstimatedEndDate: obj.NPD_TASK_ESTIMATED_COMPLETION_DATE
                    }
                  }
                }
              } else {
                item['arrTasks'][x] = {
                  cell: obj['arr'][x].cell,
                  changeColor: false,
                  taskName: obj['arr'][x]?.taskName,
                  taskStatusValue: obj['arr'][x]?.taskStatusValue,
                  taskEstimatedEndDate: obj.NPD_TASK_ESTIMATED_COMPLETION_DATE
                }
              }
            })
          })
          // item['flagChangedColor'] = item['arrTasks'].some(ele => ele.changeColor == true);
          let z = 0;
          item['arrTasks'].forEach((sec, i) => {

            if (sec?.cell == item['arrTasks'][i + 1]?.cell) {
              if (sec?.changeColor == item['arrTasks'][i + 1]?.changeColor) {
                if (sec?.taskName == item['arrTasks'][i + 1]?.taskName) {
                  item['taskPaintedData'][z] = item['taskPaintedData'][z] ? item['taskPaintedData'][z] : {};
                  item['taskPaintedData'][z] = {
                    rowLength: item['taskPaintedData'][z].rowLength ? item['taskPaintedData'][z].rowLength + 20 : 20,
                    count: item['taskPaintedData'][z].count ? item['taskPaintedData'][z].count + 1 : 1,
                    cell: sec?.cell,
                    taskName: sec?.taskName,
                    taskStatusValue: sec?.taskStatusValue,
                    changeColor: sec?.changeColor,
                    taskEstimatedEndDate: sec?.taskEstimatedEndDate
                  };
                } else {
                  item['taskPaintedData'][z] = item['taskPaintedData'][z] ? item['taskPaintedData'][z] : {};
                  item['taskPaintedData'][z] = {
                    rowLength: item['taskPaintedData'][z].rowLength ? item['taskPaintedData'][z].rowLength + 20 : 20,
                    count: item['taskPaintedData'][z].count ? item['taskPaintedData'][z].count + 1 : 1,
                    cell: sec?.cell,
                    taskName: sec?.taskName,
                    taskStatusValue: sec?.taskStatusValue,
                    changeColor: sec?.changeColor,
                    taskEstimatedEndDate: sec?.taskEstimatedEndDate

                  };
                  z++;
                }
              } else {
                if (sec?.taskName == item['arrTasks'][i + 1]?.taskName) {
                  item['taskPaintedData'][z] = item['taskPaintedData'][z] ? item['taskPaintedData'][z] : {};
                  item['taskPaintedData'][z] = {
                    rowLength: item['taskPaintedData'][z].rowLength ? item['taskPaintedData'][z].rowLength + 20 : 20,
                    count: item['taskPaintedData'][z].count ? item['taskPaintedData'][z].count + 1 : 1,
                    cell: sec?.cell,
                    taskName: sec?.taskName,
                    taskStatusValue: sec?.taskStatusValue,
                    changeColor: sec?.changeColor,
                    taskEstimatedEndDate: sec?.taskEstimatedEndDate

                  };
                } else {
                  z++;
                  item['taskPaintedData'][z] = item['taskPaintedData'][z] ? item['taskPaintedData'][z] : {};
                  item['taskPaintedData'][z] = {
                    rowLength: item['taskPaintedData'][z].rowLength ? item['taskPaintedData'][z].rowLength + 20 : 20,
                    count: item['taskPaintedData'][z].count ? item['taskPaintedData'][z].count + 1 : 1,
                    cell: sec?.cell,
                    taskName: sec?.taskName,
                    taskStatusValue: sec?.taskStatusValue,
                    changeColor: sec?.changeColor,
                    taskEstimatedEndDate: sec?.taskEstimatedEndDate

                  };
                }
                z++;
              }
            } else {
              if (i != (this.dateArray.length - 1)) {
                item['taskPaintedData'][z] = item['taskPaintedData'][z] ? item['taskPaintedData'][z] : {};
                item['taskPaintedData'][z] = item['taskPaintedData'][z] ? {
                  rowLength: item['taskPaintedData'][z].rowLength ? item['taskPaintedData'][z].rowLength + 20 : 20,
                  count: item['taskPaintedData'][z].count ? item['taskPaintedData'][z].count + 1 : 1,
                  cell: item['arrTasks'][i]?.cell,
                  taskName: sec?.taskName,
                  taskStatusValue: sec?.taskStatusValue,
                  changeColor: sec?.changeColor,
                  taskEstimatedEndDate: sec?.taskEstimatedEndDate

                } : {};
                z++;
              } else {
                item['taskPaintedData'][z] = item['taskPaintedData'][z] ? item['taskPaintedData'][z] : {};
                item['taskPaintedData'][z] = {
                  rowLength: item['taskPaintedData'][z].rowLength ? item['taskPaintedData'][z].rowLength + 20 : 20,
                  count: item['taskPaintedData'][z].count ? item['taskPaintedData'][z].count + 1 : 1,
                  cell: sec?.cell,
                  taskName: sec?.taskName,
                  taskStatusValue: sec?.taskStatusValue,
                  changeColor: sec?.changeColor,
                  taskEstimatedEndDate: sec?.taskEstimatedEndDate

                };
              }
            }
          })
        }

        let projectDateArray = [];
        projectDateArray = this.getProjectDateArray(startDate, endDate);
        this.dateArray.forEach((ele, i) => {
          let bool = projectDateArray.some(part => {
            // if((startDate >= date) && (date <= endDate)){
            if ((part.day == ele.day) && (part.month == ele.month) && (part.year == ele.year)) {
              return true;
            } else {
              return false;
            }
          })

          if (bool) {
            item['arr'][i] = {}
            item['arr'][i] = {
              cell: '1',
            }
          } else {
            item['arr'][i] = {}
            item['arr'][i] = {
              cell: '0',
            }
          }
        })
        let j = 0;
        item['arr'].forEach((part, i) => {
          if (part?.cell == item['arr'][i + 1]?.cell) {
            item['paintedData'][j] = item['paintedData'][j] ? item['paintedData'][j] : {};
            item['paintedData'][j] = {
              rowLength: item['paintedData'][j].rowLength ? item['paintedData'][j].rowLength + 20 : 20,
              count: item['paintedData'][j].count ? item['paintedData'][j].count + 1 : 1,
              cell: part?.cell
            };
          } else {
            if (i != (this.dateArray.length - 1)) {
              item['paintedData'][j] = item['paintedData'][j] ? item['paintedData'][j] : {};
              item['paintedData'][j] = item['paintedData'][j] ? {
                rowLength: item['paintedData'][j].rowLength ? item['paintedData'][j].rowLength + 20 : 20,
                count: item['paintedData'][j].count ? item['paintedData'][j].count + 1 : 1,
                cell: item['arr'][i]?.cell
              } : {};
              j++;
            } else {
              item['paintedData'][j] = item['paintedData'][j] ? item['paintedData'][j] : {};
              item['paintedData'][j] = {
                rowLength: item['paintedData'][j].rowLength ? item['paintedData'][j].rowLength + 20 : 20,
                count: item['paintedData'][j].count ? item['paintedData'][j].count + 1 : 1,
                cell: part?.cell
              };
            }
          }
        })
      })
    } else {
      this.projectData.forEach(item => {
        item['arrTasks'] = [];
        item['taskPaintedData'] = [];
        let finalStartDate;
        let finalEndDate;
        const flagS = (item['NPD_TASK_ACTUAL_START_DATE'] == '') || (item['NPD_TASK_ACTUAL_START_DATE'] == 'NA') || (item['NPD_TASK_ACTUAL_START_DATE'] == null) || (item['NPD_TASK_ACTUAL_START_DATE'] == undefined);
        const flagD = (item['NPD_TASK_ACTUAL_DUE_DATE'] == '') || (item['NPD_TASK_ACTUAL_DUE_DATE'] == 'NA') || (item['NPD_TASK_ACTUAL_DUE_DATE'] == null) || (item['NPD_TASK_ACTUAL_DUE_DATE'] == undefined);
        if (!flagS && !flagD) {
          finalStartDate = item['NPD_TASK_ACTUAL_START_DATE'];
          finalEndDate = item['NPD_TASK_ACTUAL_DUE_DATE'];
        } else if (flagS && flagD) {
          finalStartDate = item['TASK_START_DATE'];
          finalEndDate = item['TASK_DUE_DATE'];
        } else if (flagS && !flagD) {
          finalStartDate = item['TASK_START_DATE'];
          finalEndDate = item['NPD_TASK_ACTUAL_DUE_DATE'];
        } else if (!flagS && flagD) {
          finalStartDate = item['NPD_TASK_ACTUAL_START_DATE'];
          let duration = item['TASK_DURATION'];
          let finalTimeStamp = new Date(finalStartDate).getTime() + (new Date().getTimezoneOffset() * 60 * 1000);
          finalEndDate = new Date(finalTimeStamp + (((+duration * 7) - 1) * 24 * 60 * 60 * 1000));
        }
        let startDate = new Date(finalStartDate);
        let endDate = new Date(finalEndDate);
        let projectDateArray = [];
        projectDateArray = this.getProjectDateArray(startDate, endDate);
        this.dateArray.forEach((ele, i) => {
          let bool = projectDateArray.some(part => {
            // if((startDate >= date) && (date <= endDate)){
            if ((part.day == ele.day) && (part.month == ele.month) && (part.year == ele.year)) {
              return true;
            } else {
              return false;
            }
          })

          if (bool) {
            item['arrTasks'][i] = {}
            item['arrTasks'][i] = {
              cell: '1',
            }
          } else {
            item['arrTasks'][i] = {}
            item['arrTasks'][i] = {
              cell: '0',
            }
          }
        })
        let j = 0;
        item['arrTasks'].forEach((part, i) => {
          if (part?.cell == item['arrTasks'][i + 1]?.cell) {
            item['taskPaintedData'][j] = item['taskPaintedData'][j] ? item['taskPaintedData'][j] : {};
            item['taskPaintedData'][j] = {
              rowLength: item['taskPaintedData'][j].rowLength ? item['taskPaintedData'][j].rowLength + 20 : 20,
              count: item['taskPaintedData'][j].count ? item['taskPaintedData'][j].count + 1 : 1,
              cell: part?.cell
            };
          } else {
            if (i != (this.dateArray.length - 1)) {
              item['taskPaintedData'][j] = item['taskPaintedData'][j] ? item['taskPaintedData'][j] : {};
              item['taskPaintedData'][j] = item['taskPaintedData'][j] ? {
                rowLength: item['taskPaintedData'][j].rowLength ? item['taskPaintedData'][j].rowLength + 20 : 20,
                count: item['taskPaintedData'][j].count ? item['taskPaintedData'][j].count + 1 : 1,
                cell: item['arrTasks'][i]?.cell
              } : {};
              j++;
            } else {
              item['taskPaintedData'][j] = item['taskPaintedData'][j] ? item['taskPaintedData'][j] : {};
              item['taskPaintedData'][j] = {
                rowLength: item['taskPaintedData'][j].rowLength ? item['taskPaintedData'][j].rowLength + 20 : 20,
                count: item['taskPaintedData'][j].count ? item['taskPaintedData'][j].count + 1 : 1,
                cell: part?.cell
              };
            }
          }
        })
      })
    }
    //this.loaderService.hide();
  }

  updateStartEndDate(startDate, endDate, selectedFilters) {
    if (selectedFilters) {
      this.selectedTaskFilters = selectedFilters;
    }
    if (startDate && endDate) {
      this.rstartDate = new Date(startDate);
      this.rendDate = new Date(endDate);
      this.onload();
      this.reload(false, true);
      //this.getPaintedData();
    }

  }

  getWidthLength(no, year) {
    let count = 0;
    this.dateArray.forEach(ele => {
      if (ele?.weekNo == no && ele?.year == year) {
        count = count + 1;
      }
    })
    return `${20 * count}Px`
  }

  getRowLength(length) {
    return `${length}px`
  }

  getData(part) {
    if (part?.cell == '0') {
      return 1;
    } else if (part?.cell == '1') {
      return 2;
    }
  }

  getEmptyWidth() {
    return `${this.dateArray.length * 20}px`;
  }

  onScrollQuad1() {
    this.scrollLeftQuad1 = this.scrollLeftQuad1Px.nativeElement.scrollLeft;
  }

  onScrollQuad2() {
    this.scrollLeftQuad2 = this.scrollLeftQuad2Px.nativeElement.scrollLeft;
  }

  masterToggle() {
    this.selectAllChecked ? this.checkAllRows() : this.uncheckAllRows();
  }
  onScrollQuad() {
    this.scrollLeftQuad1 = 0;
    this.scrollLeftQuad2 = 0;
  }
  checkAllRows() {
    this.projectData.forEach(row => {
      row.selected = true;
      this.selection.select(row)
    });
    this.selectedProjectsCallBackHandler.emit(this.selection.selected);
  }

  uncheckAllRows() {
    this.projectData.forEach(row => { row.selected = false; this.selection.deselect(row); });
    this.selection.clear();
    this.selectedProjectsCallBackHandler.emit(this.selection.selected);
  }

  setUnsubscribe() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  resetDataView() {
    this.setUnsubscribe();
    this.facetRestrictionList = {};
    this.facetChangeEventService.updateFacetSelectFacetCondition(null);
    this.facetChangeEventService.resetFacet();
  }

  checkRow(projectData, event) {
    if (event.checked) {
      projectData.selected = true;
      this.selection.select(projectData);
    } else {
      this.selection.deselect(projectData);
    }
    this.selectedProjectsCallBackHandler.emit(this.selection.selected);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.projectData.length;
    return numSelected === numRows;
  }
  originalMainHeadercolumnsToDisplay: any[];


  drop(event: CdkDragDrop<string[]>) {

    moveItemInArray(this.mainHeadercolumnsToDisplay, event.previousIndex, event.currentIndex);

    const visibleColumns = this.originalMainHeadercolumnsToDisplay.filter(column => {
      return this.mainHeadercolumnsToDisplay.includes(column) || !column.visible;
    });

    // emit the updated array to the parent component
    this.orderedDisplayableFields.emit(visibleColumns);


  }





  getShowDragContent() {
    this.showDragContent = true;
  }

  getHideDragContent() {
    this.showDragContent = false;
  }

  getWidthAdj(width, taskName) {
    if (+(width.replaceAll('px', '')) < 200) {
      return true;
    }
    if (taskName?.length > 50) {
      return true;
    }
  }
  checkDateType(displayColumn) {
    return displayColumn?.DATA_TYPE?.toLowerCase() === IndexerDataTypes.DATETIME?.toLowerCase()
  }

  getWeek(column, row) {
    let date = this.monsterUtilService.getFieldValueByDisplayColumn(column, row);
    if (date && column?.DATA_TYPE.toLowerCase() === IndexerDataTypes.DATETIME.toLowerCase()) {
      return this.monsterUtilService.calculateWeek(date);
    }
  }

  getWidthOfHead(offsetWidth) {
    if (this.defaultWidthContent && this.defaultWidth) {
      this.widthOfSecondQuad = this.defaultWidthContent + (this.defaultWidth - offsetWidth);
      return this.widthOfSecondQuad;
    }
  }

  getGroupByFilterDetails(isDefaultSort) {
    if (isDefaultSort) {
      this.mainHeadercolumnsToDisplay.forEach(ele => ele['defaultSortArrowShow'] = false);
    }
    if (this.selectedGroupByFilter === GroupByFilterTypes.GROUP_BY_TASK) {
      this.getTasks();
    } else {
      this.getProjects();
    }
  }

  updateSearchConfigId() {
    if (this.myTaskViewConfigDetails) {
      const lastValue = this.searchConfigId;
      this.searchConfigId = parseInt(this.myTaskViewConfigDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10);
      if (this.searchConfigId > 0 && lastValue !== this.searchConfigId) {
        this.searchChangeEventService.update(this.myTaskViewConfigDetails);
      }
    }
  }

  reload(isRefresh?, isPaginationReset?) {//isPaginationReset?
    this.uncheckAllRows();
    if (this.paginationCliked) {
      this.paginationCliked = false;
    } else {
      this.resetPagination.emit();
    }
    this.getGroupByFilterDetails(false);
  }

  facetToggled(event) {
    this.facetExpansionState = event;
    if (event) {
      // this.reload(false);
    }
  }

  getLevelDetails() {
    if (this.viewConfig) {
      this.viewConfigService.getAllDisplayFeildForViewConfig(this.viewConfig).subscribe((viewDetails: ViewConfig) => {
        this.myTaskViewConfigDetails = viewDetails;
        this.updateSearchConfigId();
        this.reload(false);
        //this.setListViewConfig(viewDetails);
      });
      //this.reload(false);
    }
  }
  refresh(facetRestrictionList?, searchConditions?, isResetView?) {
    this.facetRestrictionList = facetRestrictionList ? facetRestrictionList : [];
    this.searchConditions = searchConditions ? searchConditions : [];
    this.isResetView = isResetView;
    this.setLevel();
    this.getLevelDetails();
  }

  /* initializeComponent() {
    this.subscriptions.push(this.searchDataService.searchData.subscribe(data => {
      if (this.searchName || this.savedSearchName || this.advSearchData || this.facetData) {
        if (data?.length > 0) {
          this.searchConditions = (data && Array.isArray(data) ? this.otmmService.addRelationalOperator(data) : []);
        }
        if (this.advSearchData && this.advSearchData.length > 0) {
          this.advSearchData = JSON.parse(this.advSearchData);
        }
      } else {
        this.searchConditions = (data && Array.isArray(data) ? this.otmmService.addRelationalOperator(data) : []);
      }
      if ((this.searchName || this.savedSearchName || this.advSearchData || this.facetData) && this.isDataRefresh) {
        if (data && data.length > 0) {
          if (!this.facetData || ((this.facetData && this.facetRestrictionList) && (this.advSearchData || this.savedSearchName))) {
            //this.getLevelDetails();
          } else {
            this.validateFacet = true;
          }
        }
      } else if (((this.searchName && this.savedSearchName === null && this.advSearchData === null && this.facetData === null)
        || (!this.searchName && !this.savedSearchName && !this.advSearchData && !this.facetData)) && this.isDataRefresh) {
        //this.getLevelDetails();
      } else if (!this.isDataRefresh) {
        this.isReset = true;
      }
      this.getLevelDetails();
      //this.searchDataService.searchDataSubject.complete();
    })
    );
    this.subscriptions.push(this.facetChangeEventService.onFacetCondidtionChange.pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
      this.facetRestrictionList = data;
      if (this.facetData && this.isDataRefresh) {
        if (data && data.facet_condition && data.facet_condition.length > 0) {
          if (!(this.advSearchData || this.savedSearchName) || this.validateFacet) {
            //this.getLevelDetails();
            this.validateFacet = false;
          }
        }
      } else if (this.facetData === null && this.isDataRefresh) {
        //this.getLevelDetails();
      } else if (!this.isDataRefresh) {
        this.isReset = true;
      }
      this.getLevelDetails();
    })
    );
  } */

  public ngOnInit(): void {
   


    this.loaderService.hide();
    this.appConfig = this.sharingService.getAppConfig();

  }

  ngAfterViewInit() {
    this.defaultWidth = this.defaultWidthOfHead.nativeElement.offsetWidth;
    this.defaultWidthContent = this.defaultWidthOfContent.nativeElement.offsetWidth;
    this.getWidthOfHead(this.defaultWidth);
  }

  ngOnDestroy(): void {
    this.searchChangeEventService.update(null);
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
    this.resetDataView();
    // this.facetChangeEventService.facetCondidtion.next(null);
    // this.facetChangeEventService.facetCondidtion.complete();
    //unsubscribe();//updateFacetSelectFacetCondition(null);
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

