import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoaderService, NotificationService,SharingService,AppService,TaskConstants, UtilService } from 'mpm-library';
import { Observable } from 'rxjs';
import * as acronui from 'mpm-library';
import {map, startWith} from 'rxjs/operators';
import { NpdResourceManagementService } from '../services/npd-resource-management.service';
import { ResourceManagementConstants } from '../constants/ProjectTaskViewParams';
import { I } from '@angular/cdk/keycodes';
import { BulkResourceTaskConfirmationComponent } from '../bulk-resource-task-confirmation/bulk-resource-task-confirmation.component';


@Component({
  selector: 'app-bulk-resource-assignment',
  templateUrl: './bulk-resource-assignment.component.html',
  styleUrls: ['./bulk-resource-assignment.component.scss']
})
export class BulkResourceAssignmentComponent implements OnInit {

  constructor(
        public dialogRef: MatDialogRef<BulkResourceAssignmentComponent>,
        @Inject(MAT_DIALOG_DATA) public dialogData: any,private npdResourceManagementService:NpdResourceManagementService,
        private notificationService: NotificationService,private sharingService: SharingService,public utilService: UtilService,
        private loaderService: LoaderService,private appService:AppService,private dialog:MatDialog
    ) { }

    modalLabel;
    filterUserListData :Observable<any[]>;
    userFormControl = new FormControl();
    roleFormControl = new FormControl('');
    userGroup = new FormGroup({
      
      taskOwnerControl : new FormControl(''),
      rmUserControl : new FormControl()
    })
    
    
    data = [];
    isProject;
    allRoles;
    allTeams = [];
    selectedTeam;
    selectedRole;
    totalUsers = [];
    users = [];
    taskOwner = false;
    rmUser = false;
    isUserFieldDisable = true;
    isUserNameEntered = false;
    isAllGFGTasksSelected;
    selectedRoleName;
    closeDialog() {
      this.dialogRef.close();
    }

    mapData() {
      this.modalLabel = this.dialogData.modalLabel;
      this.data  = this.dialogData.data;
      this.isProject = this.dialogData.isProject;
      this.isAllGFGTasksSelected = this.dialogData.isAllGFGTasks
      

    }

    getRoles() {
      const allTeamRoles = this.sharingService.getAllTeamRoles();
      this.allTeams = this.sharingService.getAllTeams();
      this.selectedTeam = this.allTeams.find(team => team.NAME === ResourceManagementConstants.NPD_TEAM);
      this.allRoles = [];
      if(this.selectedTeam) {
        allTeamRoles.forEach(teamRole => {
          if (teamRole && teamRole.R_PO_TEAM !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"] !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"].Id !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"].Id === this.selectedTeam['MPM_Teams-id']?.Id) {
            const teamRoles = teamRole;
            if (teamRoles !== undefined) {
              this.allRoles.push(teamRoles);
            }
          }
        }); 
        if(!this.isProject) {//for tasks
          this.selectedRole = this.allRoles.find(role => role['MPM_Team_Role_Mapping-id'].Id == this.data[0]?.TASK_ROLE_ID)
          this.getUsersByRole(this.selectedRole,this.selectedTeam).subscribe();
          if(!this.isAllGFGTasksSelected){
            this.roleFormControl.setValue(this.selectedRole.NAME==='Secondary AW Specialist'?'Secondary AW Manager':this.selectedRole.NAME)
          }
          this.selectedRoleName = this.selectedRole.NAME==='Secondary AW Specialist'?'Secondary AW Manager':this.selectedRole.NAME;
        } 
        if(this.isProject) {
          let roles = [];
          roles = JSON.parse(JSON.stringify(this.allRoles));
          let assignmentRoles = ResourceManagementConstants.PROJECT_ASSIGNMENT_ROLES;
          this.allRoles = [];
          roles.forEach(role => {
              if(role.NAME == 'Secondary AW Specialist'){
                role.NAME = 'Secondary AW Manager';
              }
              if(assignmentRoles.find(assignmentRole => assignmentRole == role.NAME)) {
                this.allRoles.push(role);
              }
          });
        }
      }
    }

    getUser(role) {
      this.selectedRole = role;
      this.getUsersByRole(role, this.selectedTeam).subscribe();
    }

    displayFn(user?: any): string | undefined {
      return user ? (user.name || user.FullName) : '';
    }


    getUsersByRole(role, selectedTeam): Observable<any> {
      return new Observable(observer => {
        let getUserByRoleRequest = {};
        this.users = [];
        //this.userFormControl.setValue('');
        getUserByRoleRequest = {
          allocationType: TaskConstants.ALLOCATION_TYPE_ROLE,
          roleDN: role.ROLE_DN,
          teamID: selectedTeam ? selectedTeam['MPM_Teams-id'].Id : '',
          isApproveTask: false,
          isUploadTask: false
        };
  
        this.appService.getUsersForTeam(getUserByRoleRequest)
          .subscribe(response => {
            if (response && response.users && response.users.user) {
              const userList = acronui.findObjectsByProp(response, 'user');
              const users = [];
              if (userList && userList.length && userList.length > 0) {
                userList.forEach(user => {
                  const fieldObj = users.find(e => e.value === user.dn);
                  const userIdentity = this.totalUsers.find(u => u.cn == user.cn);
                  let userObject;
                  if (!fieldObj) {
                    userObject = {
                      name: user.name,
                      value: user.cn,
                      displayName: user.name,
                      cn: user.cn,
                      userId: userIdentity?.userId,
                      identityId : userIdentity?.identityId,
                    }
                    users.push(userObject);
                  }
                });
              }
              this.users = users;
            } else {
              this.users = [];
            }
            observer.next(true);
            observer.complete();
          }, () => {
            observer.next(false);
            observer.complete();
          });
      });
    }

    filterUserList() {
      this.filterUserListData = this.userFormControl.valueChanges
      .pipe(
        startWith(''),
       // map(value => this._filter(value))
       map(value  => value.length >= 1 ? this._filter(value): [])
      );
    }

    private _filter(value: string): string[] {
      const filterValue = value.toLowerCase();
      return this.users.filter(option => option.name?.toLowerCase().includes(filterValue));
    }  
    
    onUserSelected(user) {
      //let selectedResource  = user?.option?.value;
    
    }

   getAllUsers() : Observable<any> {
      return new Observable(observer => {
        let users = []
        this.loaderService.show();
        this.appService.getAllUsers().subscribe(allUsers => {
          this.loaderService.hide();
          if (allUsers && allUsers.User) {
            allUsers.User.forEach(user => {
              if (user && user.FullName && typeof (user.FullName) !== 'object') {
                users.push({
                  name: user.FullName,
                  cn: user.UserId,
                  userId: user?.toPerson?.['Person-id']?.Id,
                  identityId : user?.['Identity-id'].Id,
                });
                this.totalUsers = users;
              }
            });
          }
          observer.next(true);
          observer.complete();
      },(error) => {
        observer.error(error);
        console.log(error);
      });
    });
    }

    assignUser() {
     this.isProject ? this.assignProject() :this.assignTask();
    }

    assignProject() {
      let projectDetails = [];
      this.data?.forEach(projectData => {
        projectDetails.push({
          project : {
            requestId : projectData?.PR_REQUEST_ITEM_ID?.split('.')[1],
            projectId: projectData?.ID,
            projectItemId: projectData?.ITEM_ID,
          }
        })
      })
      let projectObject = {
        bulkAssign : {
          roleName : this.selectedRole?.NAME,
          userId: this.userFormControl.value?.identityId,
          projectDetails : projectDetails
        }
      }
      this.loaderService.show();
      this.npdResourceManagementService.bulkProjectAssign(projectObject).subscribe(response=> {
        this.loaderService.hide();
        this.notificationService.success('Assigned user to the selected Project(s) Succesfully.')
        this.dialogRef.close(true);
      },(error)=> {
        this.notificationService.error('Something went wrong while assigning user to Project(s).')
        this.loaderService.hide();
        this.dialogRef.close();
      })
    }

    /**
     * 
     * @author JeevaR
     */
    assignTask() {
      if(this.userFormControl.value=="" || this.userFormControl.value == null || this.userFormControl.value == undefined){
        this.notificationService.warn('Please Enter User Name!');
        return;
      }
        let taskDetails = [];
        let projectArray = []
        this.data?.forEach(taskData => {
          taskDetails.push({
            task : {
              id: taskData?.ID,
              itemId : taskData?.ITEM_ID,
              requestId : taskData?.PR_REQUEST_ITEM_ID?.split('.')[1]
            }
          })
          if(taskData.TASK_OWNER_ID!=0 && taskData.NPD_TASK_RM_USER_ID!=0 && taskData.TASK_OWNER_ID!=taskData.NPD_TASK_RM_USER_ID){
            if(!projectArray.includes(taskData.NPD_PROJECT_BUSINESS_ID)){
              projectArray.push(taskData.NPD_PROJECT_BUSINESS_ID);
            }
            
          }
        })
        let taskObject:any = {
          bulkTaskHandler : {
            action : 'CLAIM',
            userCN: this.userFormControl.value?.cn,
            taskDetails : taskDetails,
            role : this.selectedRole.NAME,
            isTaskOwnerChange :this.userGroup.controls['taskOwnerControl'].value == true ?'true':'false',
            isRmUserChange : this.userGroup.controls['rmUserControl'].value == true ?'true':'false',
            MakeTaskOwnerAsRmUser :(this.userGroup.controls['taskOwnerControl'].value && this.userGroup.controls['rmUserControl'].value) == true ?'true':'false'
          }
        }

        if(taskObject.bulkTaskHandler.MakeTaskOwnerAsRmUser == 'true' && projectArray.length>0){
            const dialogRef = this.dialog.open(BulkResourceTaskConfirmationComponent, {
              height:'auto',
              width: '25%',
              data: {
                projectArray :projectArray,
                taskObject : taskObject,
              }
              
            });
        
            dialogRef.afterClosed().subscribe(result => {
              this.dialogRef.close();
            });
        }
        else{
          this.loaderService.show();
        this.npdResourceManagementService.bulkTaskAssign(taskObject).subscribe(response=> {
          this.loaderService.hide();
          this.notificationService.success('Assigned user to the selected Task(s) Succesfully.');
          this.dialogRef.close(true);
        },(error)=> {
          this.notificationService.error('Something went wrong while assigning user to task(s).')
          this.loaderService.hide();
          this.dialogRef.close();
        });
        }        
    }

    /**
     * 
     * @author JeevaR
     */
    onChangeTaskOwner(event){
     
      if(this.isAllGFGTasksSelected){
        this.getUsersByRole(this.selectedRole,this.selectedTeam).subscribe()
        this.roleFormControl.setValue(this.selectedRole.NAME)
        this.userGroup.controls['taskOwnerControl'].setValue(event.checked);
        this.userGroup.controls['rmUserControl'].setValue(event.checked == true?!event.checked :false);
        if(event.checked == false){
          this.roleFormControl.setValue('')
        }
        this.isUserFieldDisable = !this.userGroup.controls['taskOwnerControl'].value && !this.userGroup.controls['rmUserControl'].value;
        return
      }
       this.userGroup.controls['taskOwnerControl'].setValue(event.checked);
       this.userGroup.controls['rmUserControl'].setValue(event.checked);
      this.isUserFieldDisable = !this.userGroup.controls['taskOwnerControl'].value || !this.userGroup.controls['rmUserControl'].value;
      
    }

    /**
     * 
     * @author JeevaR
     */
    onChangeRMUser(event){
      if(this.isAllGFGTasksSelected){
        this.selectedRoleName ='GFG';
        let selectedRole = this.selectedRole;
        selectedRole = this.allRoles.find(role => role.NAME ===this.selectedRoleName);
        this.getUsersByRole(selectedRole,this.selectedTeam).subscribe()
        this.roleFormControl.setValue(this.selectedRoleName)
        this.userGroup.controls['taskOwnerControl'].setValue(event.checked == true?!event.checked :false);
        if(event.checked == false){
          this.roleFormControl.setValue('')
        }
      }
      else if(!this.userGroup.controls['taskOwnerControl'].value){
        this.userGroup.controls['rmUserControl'].setValue(event.checked);
      }
      else{
        this.userGroup.controls['rmUserControl'].setValue(true);
      }
      this.isUserFieldDisable = !this.userGroup.controls['taskOwnerControl'].value && !this.userGroup.controls['rmUserControl'].value;
    }

    /**
     * 
     * @author JeevaR
     */
    onUserChange(){
      if(this.userFormControl.value != null &&  this.userFormControl.value !=undefined &&  this.userFormControl.value != ''){
        this.isUserNameEntered = true;
      }
      else{
        this.isUserNameEntered=false;
      }
    }

    ngOnInit(): void {
      this.isUserNameEntered = false;
      this.mapData();
      this.getAllUsers().subscribe(response => {
        this.getRoles();
      })
      this.filterUserList();
    }

}