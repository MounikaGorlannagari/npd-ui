import { I } from '@angular/cdk/keycodes';
import {
    Pipe,
    PipeTransform
} from '@angular/core';
import { OTMMMPMDataTypes, IndexerDataTypes, FieldConfigService } from 'mpm-library';
import { MonsterUtilService } from '../services/monster-util.service';

@Pipe({
    name: 'ProjectTaskDetailPipe'
})

export class ProjectTaskDetailPipe implements PipeTransform {

    constructor(
        public fieldConfigService: FieldConfigService,
        public monsterUtilService: MonsterUtilService
    ) { }
    transform(projectTaskRowData: any, headerColumn: any, isGetWeek:any): string {
        if(isGetWeek){
            let date = this.monsterUtilService.getFieldValueByDisplayColumn(headerColumn,projectTaskRowData);
            if (date && headerColumn?.DATA_TYPE.toLowerCase() === IndexerDataTypes.DATETIME.toLowerCase()) {
                return this.monsterUtilService.calculateWeek(date) ? this.monsterUtilService.calculateWeek(date) : 'NA';
            }
        }else{
            return this.fieldConfigService.getFieldValueByDisplayColumn(headerColumn, projectTaskRowData) ? 
                this.fieldConfigService.getFieldValueByDisplayColumn(headerColumn, projectTaskRowData) : 'NA';
        }
    }
}
