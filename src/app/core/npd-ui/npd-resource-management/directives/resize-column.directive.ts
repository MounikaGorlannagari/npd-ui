import { Directive, ElementRef, OnInit, Input } from '@angular/core';
import { NpdResourceManagementService } from '../services/npd-resource-management.service';

@Directive({
  selector: '[resizeColumn]'
})
export class ResizeColumnDirective implements OnInit {
  @Input('resizeColumn') index;
  @Input() resizableGrabWidth = 8;
  @Input() resizableMinWidth = 0;
  @Input() resizableMaxWidth = window.screen.width - 150;
  dragging = false;

  constructor(private el: ElementRef,private npdResourceManagementService: NpdResourceManagementService) {

    const self = this;

    const EventListenerMode = { capture: true };

    function preventGlobalMouseEvents() {
      document.body.style['pointer-events'] = 'none';
    }

    function restoreGlobalMouseEvents() {
      document.body.style['pointer-events'] = 'auto';
    }


    const newWidth = (wid) => {
      let newWidth = Math.max(this.resizableMinWidth, wid);
      newWidth = Math.min(this.resizableMaxWidth, wid);
      el.nativeElement.style.width = (newWidth) + "px";
      this.npdResourceManagementService.getResizeInnerWidth(el.nativeElement.style.width, this.index);
    }


    const mouseMoveG = (evt) => {
      if (!this.dragging) {
        return;
      }
      newWidth(evt.clientX - el.nativeElement.offsetLeft)
      evt.stopPropagation();
    };

    const dragMoveG = (evt) => {
      if (!this.dragging) {
        return;
      }
      const newWidth = Math.max(this.resizableMinWidth, (evt.clientX - el.nativeElement.offsetLeft)) + "px";
      el.nativeElement.style.width = (evt.clientX - el.nativeElement.offsetLeft) + "px";
      evt.stopPropagation();
    };

    const mouseUpG = (evt) => {
      if (!this.dragging) {
        return;
      }
      restoreGlobalMouseEvents();
      this.dragging = false;
      evt.stopPropagation();
    };

    const mouseDown = (evt) => {
      if (this.inDragRegion(evt)) {
        this.dragging = true;
        preventGlobalMouseEvents();
        evt.stopPropagation();
      }
    };


    const mouseMove = (evt) => {
      if (this.inDragRegion(evt) || this.dragging) {
        el.nativeElement.style.cursor = "col-resize";
      } else {
        el.nativeElement.style.cursor = "default";
      }
    }


    document.addEventListener('mousemove', mouseMoveG, true);
    document.addEventListener('mouseup', mouseUpG, true);
    el.nativeElement.addEventListener('mousedown', mouseDown, true);
    el.nativeElement.addEventListener('mousemove', mouseMove, true);
  }

  ngOnInit(): void {
    this.el.nativeElement.style["border-right"] = this.resizableGrabWidth + "px solid darkgrey;";
  }

  inDragRegion(evt) {
    return this.el.nativeElement.clientWidth - (evt.clientX - 30) + this.el.nativeElement.offsetLeft < this.resizableGrabWidth;
  }

}
