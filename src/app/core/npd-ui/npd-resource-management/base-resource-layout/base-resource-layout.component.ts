import { getRtlScrollAxisType } from '@angular/cdk/platform';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import {
  ApplicationConfigConstants,
  AppService,
  FacetChangeEventService,
  FieldConfigService,
  LoaderService,
  MPM_LEVELS,
  NotificationService,
  OTMMDataTypes,
  OTMMMPMDataTypes,
  OTMMService,
  SearchChangeEventService,
  SearchConfigConstants,
  SearchDataService,
  SharingService,
  UtilService,
  ViewConfig,
  ViewConfigService,
  MPMFieldConstants,
  SearchResponse
  , IndexerService,
  SearchRequest
} from 'mpm-library';
import { ResourceManagementComponentParams } from 'mpm-library/lib/resource-management/object/ResourceManagementComponentParams';
import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NPDFacetService } from 'src/app/core/mpm-lib/facet/facet.service';
import { GroupByFilterTypes } from 'src/app/core/mpm/deliverables-task-view/groupByFilter/GroupByFilterTypes';
import { RouteService } from 'src/app/core/mpm/route.service';
import { FilterConfigService } from '../../filter-configs/filter-config.service';
import { MONSTER_ROLES } from '../../filter-configs/role.config';
import { NpdSessionStorageConstants } from '../../request-view/constants/sessionStorageConstants';
import { MonsterConfigApiService } from '../../services/monster-config-api.service';
import { BulkResourceAssignmentComponent } from '../bulk-resource-assignment/bulk-resource-assignment.component';
import { ResourceManagementConstants } from '../constants/ProjectTaskViewParams';
import { ProjectTasksLayoutComponent } from '../project-tasks-layout/project-tasks-layout.component';
import { ResourceTaskQuad34Component } from '../resource-task-quad34/resource-task-quad34.component';
import { NpdResourceManagementService } from '../services/npd-resource-management.service';

@Component({
  selector: 'app-base-resource-layout',
  templateUrl: './base-resource-layout.component.html',
  styleUrls: ['./base-resource-layout.component.scss'],
})
export class BaseResourceLayoutComponent implements OnInit {
  @ViewChild('resourceTaskQuad34Component')
  resourceTaskQuad34Component: ResourceTaskQuad34Component;
  @ViewChild('projectTasksLayoutComponent')
  projectTasksLayoutComponent: ProjectTasksLayoutComponent;

  private unsubscribe$: Subject<any> = new Subject<any>();
  private subscriptions: Array<Subscription> = [];
  searchConditions = [];
  facetRestrictionList;
  tempsearchConditionsSubscribe: any;




  viewConfig: any;
  myTaskViewConfigDetails: any;
  level: any;
  selectedSortOption: any;
  selectedSortOrder: any;
  columnChooserFieldsOfTasks: any = [];
  columnChoosersForTasks: boolean;
  searchName;
  savedSearchName;
  advSearchData;
  facetData;
  interval;
  dateRangeFormGroup: FormGroup;
  paginator = {
    page: 1,
    skip: 0,
    top: 0,
    pageSize: 0,
    pageNumber: 0,
  };
  totalListDataCount;
  selectedGroupByFilter;
  data = [];
  appConfig: any;
  show = {
    startDate: null,
    endDate: null,
  };
  fromDate = {
    startDate: null,
    endDate: null,
  };
  toDate = {
    startDate: null,
    endDate: null,
  };
  weekYear = {
    startDate: null,
    endDate: null,
  };
  groupFilterOptions = [
    { name: 'Project', value: GroupByFilterTypes.GROUP_BY_PROJECT },
    { name: 'Task', value: GroupByFilterTypes.GROUP_BY_TASK },
  ];
  queryParams;
  columnChooserFields;
  displayableFields;
  subLevelTaskHeadercolumnsToDisplay = [];
  isListView = null;
  defaultViewFilterName;
  dashboardMenuConfig;
  selectedData;
  selectedDataCounts;
  allTasks;
  allRoles;
  selectedFilters = {
    selectedMultiTasks: '',
    selectedMultiRoles: '',
  };
  groupByFilterTypes = GroupByFilterTypes;
  allDisplayableFields = [];
  isColumnsFreezable: boolean = true;
  userPreferenceFreezeCount: any;
  isAllGFGTaskSelected;
  anyGFGTask;
  isAdmin;
  orderDisplayFields: any;

  constructor(
    private loaderService: LoaderService,
    private adapter: DateAdapter<any>,
    private utilService: UtilService,
    private sharingService: SharingService,
    private routeService: RouteService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private appService: AppService,
    private notificationService: NotificationService,
    private searchChangeEventService: SearchChangeEventService,
    private dialog: MatDialog,
    private fieldConfigService: FieldConfigService,
    private npdResourceManagementService: NpdResourceManagementService,
    private viewConfigService: ViewConfigService,
    public searchDataService: SearchDataService,
    public otmmService: OTMMService,
    public facetChangeEventService: FacetChangeEventService,
    public filterConfigService: FilterConfigService,
    public monsterConfigApiService: MonsterConfigApiService,
    public indexerService: IndexerService,
    private npdFacetService: NPDFacetService
  ) {
    this.loaderService.hide();
  }

  refresh(isResetView?) {
    this.projectTasksLayoutComponent?.refresh(this.facetRestrictionList, this.searchConditions, isResetView);
    this.resourceTaskQuad34Component?.refresh(this.searchConditions);
  }

  groupByFilterChange(option) {
    const queryParams: ResourceManagementComponentParams = {};
    if (this.selectedGroupByFilter) {
      queryParams['groupByFilter'] = this.selectedGroupByFilter;
    }
    this.projectTasksLayoutComponent?.resetDataView();
    this.projectTasksLayoutComponent?.uncheckAllRows();
    this.projectTasksLayoutComponent?.onScrollQuad();
    this.routeService.goToResourceManagement(queryParams, null, null, null, null);
  }

  assign() {
    if (this.validateAssignment()) { return; }
    let canAccess;
    if (this.selectedData?.[0]?.CONTENT_TYPE === OTMMMPMDataTypes.PROJECT) {
      this.assignData()
    }
    else {
      let isSuperAccess: any;

      isSuperAccess = this.filterConfigService.isProgramManagerRole() || this.isAdmin
      if (isSuperAccess) {
        canAccess = true;
      }
      let userDisplayName: string = JSON.parse(
        sessionStorage.getItem('USER_DETAILS_WORKFLOW')
      )?.User?.UserDisplayName;


      if (this.selectedData.length > 0) {
        let taskRole = this.selectedData[0].TASK_ROLE_VALUE;
        if (taskRole == undefined) {
          taskRole = JSON.parse(sessionStorage.getItem('ALL_TEAM_ROLES'))?.MPM_Team_Role_Mapping.find(role => role['MPM_Team_Role_Mapping-id'].Id == this.selectedData[0].TASK_ROLE_ID).NAME
        }
        if (MONSTER_ROLES.NPD_REGULATORY === taskRole && this.filterConfigService.isRegsTrafficManager()) {
          canAccess = true;
        }
        else if ((MONSTER_ROLES.NPD_CORPPM === taskRole || MONSTER_ROLES.NPD_PROjECT_SPECIALIST === taskRole || MONSTER_ROLES.NPD_REGULATORY === taskRole) && this.filterConfigService.isCorpProjectLeader()) {
          canAccess = true;
        }
        let isNotAccessible = true;
        if (this.selectedData?.[0]?.CONTENT_TYPE === OTMMMPMDataTypes.TASK) {
          if (!canAccess) {
            this.loaderService.show();
            this.getProjectForAccessValidation().subscribe(response => {
              this.loaderService.hide();
              for (var i = 0; i < this.selectedData.length; i++) {
                let taskOwner: string = this.selectedData[i].TASK_OWNER_NAME;
                let isTaskOwner = taskOwner === userDisplayName;
                if (isTaskOwner) {
                  isNotAccessible = false;
                }
                else if (this.filterConfigService.isE2EPMRole() && response.data[i]?.NPD_PROJECT_E2E_PM_NAME && response.data[i]?.NPD_PROJECT_E2E_PM_NAME === userDisplayName) {
                  isNotAccessible = false;
                }
                else if ((MONSTER_ROLES.NPD_CORPPM === taskRole || MONSTER_ROLES.NPD_PROjECT_SPECIALIST === taskRole || MONSTER_ROLES.NPD_REGULATORY === taskRole) && (this.filterConfigService.isCorpPMRole() && response.data[i]?.NPD_PROJECT_CORP_PM_NAME && response.data[i]?.NPD_PROJECT_CORP_PM_NAME === userDisplayName)) {
                  isNotAccessible = false;
                }
                else if (MONSTER_ROLES.NPD_OPS_PM === taskRole && (this.filterConfigService.isOPSPMRole() && response.data[i]?.NPD_PROJECT_OPS_PM_NAME && response.data[i]?.NPD_PROJECT_OPS_PM_NAME === userDisplayName)) {
                  isNotAccessible = false;
                }
                else if (MONSTER_ROLES.NPD_SECONDARY_PACKAGING === taskRole && (this.filterConfigService.isSecondaryPackagingRole() && response.data[i]?.NPD_PROJECT_SECONDARY_AW_SPECIALIST_NAME && response.data[i]?.NPD_PROJECT_SECONDARY_AW_SPECIALIST_NAME === userDisplayName)) {
                  isNotAccessible = false;
                }
                else if (MONSTER_ROLES.NPD_REGULATORY === taskRole && (this.filterConfigService.isRegulatoryRole() && response.data[i]?.NPD_PROJECT_REGULATORY_LEAD_NAME && response.data[i]?.NPD_PROJECT_REGULATORY_LEAD_NAME === userDisplayName)) {
                  isNotAccessible = false;
                }
                else if (taskRole === 'RTM' && (this.filterConfigService.isRegionalTechnicalManagerRole() && response.data[i]?.NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME && response.data[i]?.NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME === userDisplayName)) {
                  isNotAccessible = false;
                }
                else if (MONSTER_ROLES.NPD_PROjECT_SPECIALIST === taskRole && (this.filterConfigService.isProjectSpecialistRole() && response.data[i]?.NPD_PROJECT_PR_SPECIALIST_NAME && response.data[i]?.NPD_PROJECT_PR_SPECIALIST_NAME === userDisplayName)) {
                  isNotAccessible = false;
                }
                else if (isNotAccessible) {
                  this.notificationService.warn('You dont have access to update the selected Task(s)');
                  return;
                }

              }
              if (isNotAccessible) {
                return this.notificationService.warn('You dont have access to update the selected Task(s)')
              }

              this.assignData()

            });
          }
          else {
            this.assignData();
          }

        }
      }
    }

  }

  validateAssignment() {
    this.isAllGFGTaskSelected = true;
    this.anyGFGTask = false

    if (this.selectedData.length > 0) {
      if (this.selectedData?.[0]?.CONTENT_TYPE === OTMMMPMDataTypes.TASK) {
        //or check for group by filter task dropdown
        let hasDifferentRole = false;
        let hasCompletedTaskorCanceledTask = false
        for (var i = 0; i < this.selectedData.length; i++) {
          this.isAllGFGTaskSelected = this.isAllGFGTaskSelected == true ? ResourceManagementConstants.GFG_TASKS.includes(this.selectedData[i].TASK_NAME) : false;
          this.anyGFGTask = this.anyGFGTask == false ? ResourceManagementConstants.GFG_TASKS.includes(this.selectedData[i].TASK_NAME) : true;
          if (this.selectedData[i].TASK_STATUS_ID == 16402 || this.selectedData[i].TASK_STATUS_ID == 16403) {
            hasCompletedTaskorCanceledTask = true;
            break;
          }
          for (var k = i + 1; k < this.selectedData.length; k++) {
            if (
              this.selectedData[i].TASK_ROLE_ID !=
              this.selectedData[k].TASK_ROLE_ID

            ) {
              hasDifferentRole = true;
              break;
            }
            if (this.selectedData[k].TASK_STATUS_ID == 16402 || this.selectedData[i].TASK_STATUS_ID == 16403) {
              hasCompletedTaskorCanceledTask = true;
              break;
            }
          }
        }
        ((!this.isAllGFGTaskSelected && this.anyGFGTask) || hasDifferentRole) && this.notificationService.warn('Task(s) selected have different role');
        hasCompletedTaskorCanceledTask && this.notificationService.warn('Task(s) selected have Completed/Cancelled Task');
        return (!this.isAllGFGTaskSelected && this.anyGFGTask) || hasDifferentRole || hasCompletedTaskorCanceledTask;
      }
      for (var i = 0; i < this.selectedData.length; i++) {
        if (this.selectedData[i].PROJECT_STATUS_ID == 16411 || this.selectedData[i].PROJECT_STATUS_ID == 16399) {
          this.notificationService.warn('Project(s) selected have Completed/Cancelled Project')
          return true;
        }
      }
      return false;
    } else {
      return true;
    }
  }

  getSelectedProject(projectData) {
    this.selectedData = projectData;
    this.selectedDataCounts = projectData.length;
  }

  resetPagination() {
    this.paginator.page = 1;
    this.paginator.skip = 0;
  }

  updatePagination(totalList) {
    this.totalListDataCount = totalList;
  }

  getTaskName(task) {
    return task.TaskName + ' - ' + task.TaskDescription;
  }

  getStopPropogation(event, date) {
    if (this.show[date]) {
      return event.stopPropagation();
    }
  }

  getAllBool() {
    Object.keys(this.show).map((ele) => { this.show[ele] = false; });
  }

  getBool(date) {
    this.getAllBool();
    this.show[date] = !this.show[date];
  }

  changeTargetDP(weekFormat, date) {
    this.weekYear[date] = weekFormat;
    if (date == 'startDate') {
      this.dateRangeFormGroup.patchValue({ start_date: weekFormat });
    } else {
      this.dateRangeFormGroup.patchValue({ end_date: weekFormat });
    }
    this.dateRangeFormGroup.markAsDirty();
    [this.fromDate[date], this.toDate[date]] = this.getWeekFormat(weekFormat);
  }

  getWeekFormat(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates: NgbDate[] = this.calculateDateNgb(week, year);
      return [weekdates[0], weekdates[1]];
    }
  }

  calculateDateNgb(week: number, year: number): NgbDate[] {
    let ngbFromDate, ngbToDate;
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate, ngbToDate];
  }

  pagination(pagination) {
    this.paginator.pageSize = pagination.pageSize;
    this.paginator.top = pagination.pageSize;
    this.paginator.skip = pagination.pageIndex * pagination.pageSize;
    this.paginator.page = 1 + pagination.pageIndex;
    this.projectTasksLayoutComponent?.uncheckAllRows();
    this.projectTasksLayoutComponent.paginationCliked = true;
    this.refresh();
  }

  filterByDates() {
    let startDate, endDate;
    [startDate] = this.getStartEndDate(this.dateRangeFormGroup.value.start_date);
    [, endDate] = this.getStartEndDate(this.dateRangeFormGroup.value.end_date);
    this.projectTasksLayoutComponent.updateStartEndDate(startDate, endDate, this.selectedFilters);
    this.resourceTaskQuad34Component.updateStartEndDate(startDate, endDate);
  }

  getStartEndDate(weekFormat) {
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates = this.calculateDate(week, year);
      return [weekdates[0], weekdates[1]];
    }
  }

  calculateDate(week: number, year: number) {
    let ngbFromDate, ngbToDate;

    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    return [fromDate, toDate];
  }

  updateDisplayColumns(displayColumnFields, result?) {
    let tempDisplayableFields = [];
    const listFieldOrder = [];
    // if(result!=true){
    this.columnChooserFieldsOfTasks = displayColumnFields.column;
    displayColumnFields.column.sort((a, b) => a.POSITION - b.POSITION);
    displayColumnFields.column.forEach((displayColumnField) => {
      // let displayColumnFields = [];
      // let newDisplayFields = [];
      // if(this.orderDisplayFields.length > 0){
      //   this.orderDisplayFields.forEach(e => {
      //     const index = displayColumnField.column.findIndex(displayColumn => displayColumn.INDEXER_FIELD_ID == e.INDEXER_FIELD_ID);
      //     if(index >= 0){
      //       displayColumnFields.push(e);
      //     }
      //   })
      // }
      // console.log(displayColumnFields);

      // displayColumnFields.sort((a, b) => a.POSITION - b.POSITION);
      // console.log(displayColumnFields);

      // displayColumnFields.forEach((displayColumnField) => {
      // if (displayColumnField) {.

      if (displayColumnField.selected === true) {
        const selectedField = this.dashboardMenuConfig.projectViewConfig.displayFields.find((field) => field.INDEXER_FIELD_ID === displayColumnField.INDEXER_FIELD_ID);
        if (selectedField) {
          tempDisplayableFields.push(selectedField);
          listFieldOrder.push(selectedField.MAPPER_NAME);
        }
      }
    });
    if (tempDisplayableFields && tempDisplayableFields.length > 0) {
      this.displayableFields = tempDisplayableFields;
    }
    // }

    // this.userPreferenceFreezeCount = displayColumnFields.frezeCount;
    this.userPreferenceFreezeCount = displayColumnFields.frezeCount;
    this.dashboardMenuConfig.projectViewConfig.freezeCount = displayColumnFields.frezeCount;
    this.dashboardMenuConfig.projectViewConfig.listFieldOrder = listFieldOrder.toString();
    const userPreferenceData = this.formViewData();
    this.setViewData(userPreferenceData);
    this.setCurrentViewData(userPreferenceData);
    this.refresh();
    // this.ngOnInit();
    // this.refresh();
  }

  // formColumnChooserField(listFields) {
  //   const columnChooserFields = [];
  //   const extraFields = [];
  //   this.dashboardMenuConfig.projectViewConfig.displayFields.forEach((displayField, index) => {
  //     const selectedFieldIndex = listFields.findIndex((listField) =>
  //       listField.INDEXER_FIELD_ID === displayField.INDEXER_FIELD_ID
  //     );
  //     if (selectedFieldIndex >= 0) {
  //       columnChooserFields.push({
  //         INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
  //         DISPLAY_NAME: displayField.DISPLAY_NAME,
  //         POSITION: index,
  //         selected: true,
  //       });
  //     } else {
  //       columnChooserFields.push({
  //         INDEXER_FIELD_ID: displayField.INDEXER_FIELD_ID,
  //         DISPLAY_NAME: displayField.DISPLAY_NAME,
  //         POSITION: index,
  //         selected: false,
  //       });
  //     }
  //   });
  //   return columnChooserFields;
  // }

  formColumnChooserField(listFields) {
    const columnChooserFields = [];
    const extraFields = [];
    listFields.forEach((listField, index) => {
      const selectedFieldIndex = this.dashboardMenuConfig.projectViewConfig.displayFields.findIndex((displayField) =>
        displayField.INDEXER_FIELD_ID === listField.INDEXER_FIELD_ID
      );
      if (selectedFieldIndex >= 0) {
        columnChooserFields.push({
          INDEXER_FIELD_ID: listField.INDEXER_FIELD_ID,
          DISPLAY_NAME: listField.DISPLAY_NAME,
          POSITION: index,
          selected: true,
        });
      } else {
        columnChooserFields.push({
          INDEXER_FIELD_ID: listField.INDEXER_FIELD_ID,
          DISPLAY_NAME: listField.DISPLAY_NAME,
          POSITION: index,
          selected: false,
        });
      }
    })

    return columnChooserFields;
  }


  getOrderedDisplayableFields(displayableFields) {
    // let displayFields = [];
    // displayableFields.map(fields => {
    //   displayFields.push(fields)
    // });
    // this.orderDisplayFields = displayableFields;
    this.columnChooserFields = this.formColumnChooserField(displayableFields);
    this.updateDisplayColumns({
      column: this.columnChooserFields,
      frezeCount: this.userPreferenceFreezeCount,
    }, true);
    this.columnChooserFieldsOfTasks = this.formColumnChooserField(displayableFields);
    // console.log(this.columnChooserFields)


  }

  setCurrentView() {
    const currentViewData = {
      sortName: this.dashboardMenuConfig.selectedSortOption.option.indexerId,
      sortOrder: this.dashboardMenuConfig.selectedSortOption.sortType,
      pageNumber: this.paginator.pageNumber,
      pageSize: this.paginator.pageSize,
      isListView: this.dashboardMenuConfig.IS_LIST_VIEW,
      viewId: this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id,
      viewName: this.dashboardMenuConfig.projectViewConfig.VIEW,
      listfilter: this.dashboardMenuConfig.selectedViewByName,
      isPMView: true,
    };
    sessionStorage.setItem('CURRENT_VIEW', JSON.stringify(currentViewData));
  }

  setViewData(userPreference) {
    sessionStorage.setItem('USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW, userPreference);
  }

  setCurrentViewData(userPreference) {
    sessionStorage.setItem('CURRENT_USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW, userPreference);
  }

  formViewData() {
    const userPreference = {
      USER_ID: this.sharingService.getCurrentPerson().Id,
      PREFERENCE_TYPE: 'VIEW',
      VIEW_ID: this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id,
      FIELD_DATA: [],
      FREEZE_COLUMN_COUNT: 0,
    };
    this.dashboardMenuConfig.projectViewConfig.displayFields.forEach((viewField) => {
      if (viewField.INDEXER_FIELD_ID) {
        const fieldIndex = this.displayableFields.findIndex((displayableField) => displayableField.INDEXER_FIELD_ID === viewField.INDEXER_FIELD_ID);
        userPreference.FIELD_DATA.push({
          [viewField.INDEXER_FIELD_ID]: {
            POSITION: fieldIndex >= 0 ? fieldIndex : null,
            WIDTH: viewField && viewField.width ? viewField.width : '',
            IS_DISPLAY: fieldIndex >= 0,
          },
        });
      }
    }
    );
    if (this.userPreferenceFreezeCount === undefined) {
      userPreference.FREEZE_COLUMN_COUNT =
        MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT;
    }
    userPreference.FREEZE_COLUMN_COUNT = this.userPreferenceFreezeCount;
    return JSON.stringify(userPreference);
  }

  resetView() {
    sessionStorage.removeItem('USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW);
    const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
    const userPreferenceId = userPreference && userPreference['MPM_User_Preference-id'] && userPreference['MPM_User_Preference-id'].Id ?
      userPreference['MPM_User_Preference-id'].Id : null;
    if (userPreferenceId) {
      this.loaderService.show();
      this.appService.deleteUserPreference(userPreferenceId).subscribe((response) => {
        this.loaderService.hide();
        if (response) {
          this.resetCurrentView();
          this.notificationService.success('User preference have been reset successfully');
        }
      });
    } else {
      // this.displayableFields = this.allDisplayableFields;
      // this.columnChooserFields.forEach((ele) => { ele['selected'] = true; });
      // this.resetCurrentView();
      this.notificationService.success('User preference have been reset successfully');
    }
  }

  resetCurrentView() {
    this.dashboardMenuConfig.projectViewConfig.displayFields.forEach((displayField) => {
      displayField.width = '';
    });
    this.dashboardMenuConfig.projectViewConfig.listFieldOrder = this.dashboardMenuConfig.projectViewConfig.LIST_FIELD_ORDER;
    this.displayableFields = this.dashboardMenuConfig.projectViewConfig.displayFields;
    this.allDisplayableFields = this.dashboardMenuConfig.projectViewConfig.displayFields;
    this.columnChooserFields = this.formColumnChooserField(this.dashboardMenuConfig.projectViewConfig.displayFields);
    const userPreferenceData = this.formViewData();
    this.setCurrentViewData(userPreferenceData);
    this.defaultViewFilterName = null;
    this.userPreferenceFreezeCount =
      MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT;
  }

  searchConfigId = -1;

  updateSearchConfigId() {
    if (this.dashboardMenuConfig.projectViewConfig) {
      this.searchConfigId =
        this.dashboardMenuConfig.projectViewConfig.R_PO_ADVANCED_SEARCH_CONFIG;
      this.searchChangeEventService.update(
        this.dashboardMenuConfig.projectViewConfig
      );
    }
  }

  setUserPreference() {
    let userPreferenceSessionData: any = sessionStorage.getItem('USER_PREFERENCE-' + this.dashboardMenuConfig.projectViewConfig.VIEW);
    userPreferenceSessionData = userPreferenceSessionData ? JSON.parse(userPreferenceSessionData) : null;
    const userPreferenceViewField = userPreferenceSessionData && userPreferenceSessionData.FIELD_DATA ? userPreferenceSessionData.FIELD_DATA : null;
    if (userPreferenceViewField) {
      userPreferenceViewField.sort((a, b) => {
        const firstKey: any = Object.keys(a);
        const firstValue = a[firstKey];
        const secondKey: any = Object.keys(b);
        const secondValue = b[secondKey];
        return firstValue.POSITION - secondValue.POSITION;
      });
      const displayableFields = [];
      const listFieldOrder = [];
      userPreferenceViewField.forEach((viewField) => {
        const viewFieldId = Object.keys(viewField)[0];
        if (viewField[viewFieldId].IS_DISPLAY === true) {
          const selectedField =
            this.dashboardMenuConfig.projectViewConfig.displayFields.find(
              (field) => field.INDEXER_FIELD_ID === viewFieldId
            );
          if (selectedField) {
            selectedField.width = viewField[viewFieldId].WIDTH;
            displayableFields.push(selectedField);
            listFieldOrder.push(selectedField.MAPPER_NAME);
          }
        }
      });
      this.dashboardMenuConfig.projectViewConfig.listFieldOrder = listFieldOrder.toString();
      const order = this.dashboardMenuConfig.projectViewConfig.listFieldOrder.split(',');
      this.dashboardMenuConfig.projectViewConfig.displayFields.sort(function (a, b) {
        return order.indexOf(a.MAPPER_NAME) - order.indexOf(b.MAPPER_NAME);
      });
      this.displayableFields = displayableFields;
      this.allDisplayableFields = displayableFields;
      this.columnChooserFields = this.formColumnChooserField(displayableFields);
      if (this.selectedGroupByFilter == "GROUP_BY_TASK") {
        this.columnChooserFieldsOfTasks = this.columnChooserFields;
      }
      this.userPreferenceFreezeCount =
        userPreferenceSessionData.FREEZE_COLUMN_COUNT;
    } else {
      const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
      const currentViewFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
      this.dashboardMenuConfig.projectViewConfig.freezeCount = this.userPreferenceFreezeCount;
      if (currentViewFieldData && this.utilService.getBooleanValue(userPreference.IS_LIST_VIEW)) {
        this.userPreferenceFreezeCount = currentViewFieldData.FREEZE_COLUMN_COUNT;
        currentViewFieldData.data.sort((a, b) => {
          const firstKey: any = Object.keys(a);
          const firstValue = a[firstKey];
          const secondKey: any = Object.keys(b);
          const secondValue = b[secondKey];
          return firstValue.POSITION - secondValue.POSITION;
        });
        const displayableFields = [];
        const listFieldOrder = [];
        currentViewFieldData.data.forEach((viewField) => {
          const viewFieldId = Object.keys(viewField)[0];
          if (viewField[viewFieldId].IS_DISPLAY === true) {
            const selectedField =
              this.dashboardMenuConfig.projectViewConfig.displayFields.find(
                (field) => field.INDEXER_FIELD_ID === viewFieldId
              );
            if (selectedField) {
              selectedField.width = viewField[viewFieldId].WIDTH;
              displayableFields.push(selectedField);
              listFieldOrder.push(selectedField.MAPPER_NAME);
            }
          }
        });
        this.dashboardMenuConfig.projectViewConfig.listFieldOrder = listFieldOrder.toString();
        this.displayableFields = displayableFields;
        this.allDisplayableFields = displayableFields;
        // this.userPreferenceFreezeCount = userPreferenceSessionData.FREEZE_COLUMN_COUNT;
        this.columnChooserFields = this.formColumnChooserField(displayableFields);
        if (this.columnChoosersForTasks) {
          this.columnChooserFieldsOfTasks = this.columnChooserFields;
          this.columnChoosersForTasks = false;
        }
      } else {
        this.dashboardMenuConfig.projectViewConfig.listFieldOrder = this.dashboardMenuConfig.projectViewConfig.LIST_FIELD_ORDER;
        const order = this.dashboardMenuConfig.projectViewConfig.listFieldOrder.split(',');
        this.dashboardMenuConfig.projectViewConfig.displayFields.sort(function (a, b) {
          return order.indexOf(a.MAPPER_NAME) - order.indexOf(b.MAPPER_NAME);
        });
        this.displayableFields = this.dashboardMenuConfig.projectViewConfig.displayFields;
        this.allDisplayableFields = this.dashboardMenuConfig.projectViewConfig.displayFields;
        this.columnChooserFields = this.formColumnChooserField(this.dashboardMenuConfig.projectViewConfig.displayFields);
        this.userPreferenceFreezeCount =
          MPMFieldConstants.USER_PREFERENCE_FREEZE_COUNT;
        if (this.columnChoosersForTasks) {
          this.columnChooserFieldsOfTasks = this.columnChooserFields;
          this.columnChoosersForTasks = false;
        }
      }
    }
  }

  setLevel(routedGroupFilter?) {
    if (this.selectedGroupByFilter === GroupByFilterTypes.GROUP_BY_PROJECT) {
      this.level = MPM_LEVELS.PROJECT;
      this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_PROJECT;
    } else if (
      this.selectedGroupByFilter === GroupByFilterTypes.GROUP_BY_TASK
    ) {
      this.level = MPM_LEVELS.TASK;
      this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_TASK;
    }
  }

  getListConfig(viewDetails) {
    let selectedViewDetails: ViewConfig;
    selectedViewDetails = viewDetails;
    let displayableFields = [];
    displayableFields = selectedViewDetails.R_PM_LIST_VIEW_MPM_FIELDS;
    const order = selectedViewDetails.LIST_FIELD_ORDER.split(',');
    displayableFields.sort(function (a, b) {
      return order.indexOf(a.MAPPER_NAME) - order.indexOf(b.MAPPER_NAME);
    });
    return displayableFields;
  }

  setListViewConfig() {
    if (this.selectedGroupByFilter === GroupByFilterTypes.GROUP_BY_PROJECT) {
      this.viewConfigService.getAllDisplayFeildForViewConfig(SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_TASK).subscribe((viewDetails: ViewConfig) => {
        this.subLevelTaskHeadercolumnsToDisplay = this.getListConfig(viewDetails);
      });
    }
  }

  checkDate() {
    if (this.dateRangeFormGroup.get('end_date').touched) {
      if (
        this.dateRangeFormGroup.get('end_date').value == undefined ||
        this.dateRangeFormGroup.get('start_date').value == undefined ||
        this.dateRangeFormGroup.get('end_date').value.trim() === '' ||
        this.dateRangeFormGroup.get('end_date').value == null
      ) {
        return false;
      } else {
        const endYear = this.dateRangeFormGroup.get('end_date').value.slice(-4);
        const startYear = this.dateRangeFormGroup
          .get('start_date')
          .value.slice(-4);
        if (endYear < startYear) {
          return true;
        } else if (endYear > startYear) {
          return false;
        } else {
          let endWeek = '';
          let startWeek = '';
          if (this.dateRangeFormGroup.get('end_date').value?.length == 6) {
            endWeek = this.dateRangeFormGroup
              .get('end_date')
              .value?.slice(0, 1)
              .padStart(2, 0);
          } else {
            endWeek = this.dateRangeFormGroup
              .get('end_date')
              .value?.slice(0, 2);
          }

          if (this.dateRangeFormGroup.get('start_date').value?.length == 6) {
            startWeek = this.dateRangeFormGroup
              .get('start_date')
              .value?.slice(0, 1)
              .padStart(2, 0);
          } else {
            startWeek = this.dateRangeFormGroup
              .get('start_date')
              .value?.slice(0, 2);
          }

          if (+endWeek < +startWeek) {
            return true;
          } else {
            return false;
          }
        }
      }
    } else {
      return false;
    }
  }
  myStopFunction() {
    clearInterval(this.interval);
  }

  updateViewDetailsBasedonViewChange(viewDetails: ViewConfig) {
    if (!this.dashboardMenuConfig.projectViewConfig || viewDetails.VIEW !== this.dashboardMenuConfig.projectViewConfig.VIEW) {
      this.dashboardMenuConfig.projectViewConfig = viewDetails;
      this.updateSearchConfigId();
    }
    const userPreference = this.sharingService.getCurrentUserPreferenceByViewId(this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id);
    const userPreferenceFieldData = userPreference && !this.utilService.isNullOrEmpty(userPreference.FIELD_DATA) ? JSON.parse(userPreference.FIELD_DATA) : null;
    this.dashboardMenuConfig.projectViewConfig = viewDetails;
    this.dashboardMenuConfig.projectViewConfig.displayFields = viewDetails.R_PM_LIST_VIEW_MPM_FIELDS;
    this.columnChooserFields = this.formColumnChooserField(viewDetails.R_PM_LIST_VIEW_MPM_FIELDS);

    this.dashboardMenuConfig.projectViewConfig.listFieldOrder = viewDetails.LIST_FIELD_ORDER;
    this.dashboardMenuConfig.metaDataFields = this.fieldConfigService.getFieldsbyFeildCategoryLevel('REQUEST');
    const dataSet = this.fieldConfigService.formrequiredDataForLoadView(viewDetails, this.dashboardMenuConfig.IS_LIST_VIEW);
    const compDataSet = this.fieldConfigService.formrequiredDataForLoadView(viewDetails, !this.dashboardMenuConfig.IS_LIST_VIEW);

    let arrayList = this.dashboardMenuConfig.IS_LIST_VIEW ? viewDetails.R_PM_LIST_VIEW_MPM_FIELDS : viewDetails.R_PM_CARD_VIEW_MPM_FIELDS; arrayList = arrayList ? arrayList : [];
    this.setUserPreference();

    this.dashboardMenuConfig.projectSortOptions = dataSet.SORTABLE_FIELDS;
    this.dashboardMenuConfig.projectSearchableFields = dataSet.SEARCHABLE_FIELDS;
    let existingSortColumn: boolean;
    const sortInput = compDataSet.SORTABLE_FIELDS.find((option) => option.displayName === this.selectedSortOption);
    if (sortInput === undefined) {
      existingSortColumn = true;
    } else {
      existingSortColumn = false;
    }
    const sortableFields = existingSortColumn === false ? compDataSet.SORTABLE_FIELDS : dataSet.SORTABLE_FIELDS;
    let isDefaultSort = true;
    if (sortableFields.length > 0) {
      let sortOptions = { option: sortableFields[0], sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc', };
      sortOptions = dataSet.DEFAULT_SORT_FIELD ? dataSet.DEFAULT_SORT_FIELD : sortOptions;

      if (this.selectedSortOrder || this.selectedSortOption) {
        const sortData = sortableFields.find((option) => option.displayName === this.selectedSortOption);
        const selectedSort = {
          option: sortData,
          sortType: this.selectedSortOrder ? this.selectedSortOrder : sortOptions.sortType,
        };
        isDefaultSort = false;
        this.dashboardMenuConfig.selectedSortOption = sortData ? JSON.parse(JSON.stringify(selectedSort)) : JSON.parse(JSON.stringify(sortOptions));
        this.dashboardMenuConfig.sortFieldName = this.dashboardMenuConfig.selectedSortOption.option.name;
        this.dashboardMenuConfig.sortOrder = this.dashboardMenuConfig.selectedSortOption.sortType;
      } else {
        if (userPreferenceFieldData) {
          userPreferenceFieldData.data.forEach((field) => {
            const fieldId = Object.keys(field)[0];
            if (field[fieldId].IS_SORTABLE === true) {
              const selectedField = sortableFields.find((field) => field.indexerId === fieldId);
              if (selectedField) {
                const selectedSort = {
                  option: selectedField,
                  sortType: field[fieldId].SORT_ORDER ? field[fieldId].SORT_ORDER : this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc',
                };
                isDefaultSort = false;
                this.dashboardMenuConfig.selectedSortOption = JSON.parse(JSON.stringify(selectedSort));
                this.dashboardMenuConfig.sortFieldName = this.dashboardMenuConfig.selectedSortOption.option.name;
                this.dashboardMenuConfig.sortOrder = this.dashboardMenuConfig.selectedSortOption.sortType;
              }
            }
          });
        }
      }
      if (isDefaultSort) {
        this.dashboardMenuConfig.selectedSortOption = JSON.parse(JSON.stringify(sortOptions));
        this.dashboardMenuConfig.sortFieldName = this.dashboardMenuConfig.selectedSortOption.option.name;
        this.dashboardMenuConfig.sortOrder = this.dashboardMenuConfig.selectedSortOption.sortType;
      }
    }
    this.setCurrentView();
    const userPreferenceData = this.formViewData();
    this.setCurrentViewData(userPreferenceData);
  }

  getMPMDataFields(): Observable<any> {
    return new Observable((observer) => {
      this.setLevel();
      this.viewConfigService.getAllDisplayFeildForViewConfig(this.viewConfig).subscribe((viewDetails: ViewConfig) => {
        this.updateViewDetailsBasedonViewChange(viewDetails);
        this.setListViewConfig();
        observer.next(true);
        observer.complete();
      },
        (metadataError) => {
          observer.error(metadataError);
        }
      );
    });
  }

  getLoadConfigForColumnChoosers() {
    this.appConfig = this.sharingService.getAppConfig();
    this.paginator.pageSize = 15;
    this.paginator.top = this.paginator.pageSize;
    this.columnChoosersForTasks = true;
    if (this.selectedGroupByFilter == GroupByFilterTypes.GROUP_BY_PROJECT) {
      this.dateRangeFormGroup?.get('task_role')?.setValidators(Validators.required);
    } else {
      this.dateRangeFormGroup?.get('task_role')?.clearValidators();
      this.dateRangeFormGroup?.get('task_role').updateValueAndValidity();
    }
    this.loaderService.show();
    forkJoin([this.getMPMDataFields(), this.npdResourceManagementService.getAllTaskRoles()]).subscribe(response => {
      let allTeams = this.sharingService.getAllTeams();
      let allRoles = this.sharingService.getAllTeamRoles().filter((ele) => !(ele.NAME == 'GFG' || ele.NAME == 'Programme Manager'));
      let selectedTeam = allTeams.find((team) => team.NAME === 'EM NPD Team');
      this.allRoles = [];
      if (selectedTeam) {
        allRoles.forEach((teamRole) => {
          if (teamRole && teamRole.R_PO_TEAM !== undefined && teamRole.R_PO_TEAM['MPM_Teams-id'] !== undefined &&
            teamRole.R_PO_TEAM['MPM_Teams-id'].Id !== undefined && teamRole.R_PO_TEAM['MPM_Teams-id'].Id === selectedTeam['MPM_Teams-id']?.Id) {
            this.allRoles.push(teamRole);
          }
        });
      }
      this.allTasks = response[1];
    });
    this.sharingService.resetViewData().subscribe(response => {
      if (response && this.dashboardMenuConfig && this.dashboardMenuConfig.projectViewConfig &&
        this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'] && this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id &&
        response === this.dashboardMenuConfig.projectViewConfig['MPM_View_Config-id'].Id) {
        this.resetCurrentView();
        this.refresh(true);
      }
    })
    let interval = setInterval(() => {
      if (this.columnChooserFields?.length > 1) {
        this.refresh();
        clearInterval(interval)
      }
    }, 500);
  }

  initializeComponent() {
    this.tempsearchConditionsSubscribe = this.searchDataService.searchData.subscribe(data => {
      this.searchConditions = (data && Array.isArray(data) ? this.otmmService.addRelationalOperator(data) : []);
      /* if (this.searchName || this.savedSearchName || this.advSearchData || this.facetData) {
        this.refresh();
      } else {
        this.searchConditions = (data && Array.isArray(data) ? this.otmmService.addRelationalOperator(data) : []);
      } */

      //this.searchDataService.searchDataSubject.complete();
    }
    );
    this.subscriptions.push(this.facetChangeEventService.onFacetCondidtionChange.pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
      if (JSON.stringify(data) !== '{}') {
        this.facetRestrictionList = data;
        if (data) {
          this.refresh();
        }
      }
    })
    );
  }

  /**
     *
     * @author JeevaR
  */
  validateAccess() {
    let isSuperAccess;

    isSuperAccess = this.filterConfigService.isProgramManagerRole() || this.isAdmin
    // if(isSuperAccess){
    //   return false;
    // }
    let userDisplayName: string = JSON.parse(
      sessionStorage.getItem('USER_DETAILS_WORKFLOW')
    )?.User?.UserDisplayName;


    if (this.selectedData.length > 0) {
      let taskRole = this.selectedData[0].TASK_ROLE_VALUE;
      if (MONSTER_ROLES.NPD_REGULATORY === taskRole && this.filterConfigService.isRegsTrafficManager()) {
        return false;
      }
      else if ((MONSTER_ROLES.NPD_CORPPM === taskRole || MONSTER_ROLES.NPD_PROjECT_SPECIALIST === taskRole || MONSTER_ROLES.NPD_REGULATORY === taskRole) && this.filterConfigService.isCorpProjectLeader()) {
        return false;
      }


      let isNotAccessible = true;
      if (this.selectedData?.[0]?.CONTENT_TYPE === OTMMMPMDataTypes.TASK) {


        this.getProjectForAccessValidation().subscribe(response => {
          for (var i = 0; i < this.selectedData.length; i++) {
            if (this.filterConfigService.isE2EPMRole() && response.data[i].NPD_PROJECT_E2E_PM_NAME === userDisplayName) {
              isNotAccessible = false;
            }
            else if ((MONSTER_ROLES.NPD_CORPPM === taskRole || MONSTER_ROLES.NPD_PROjECT_SPECIALIST === taskRole || MONSTER_ROLES.NPD_REGULATORY === taskRole) && (this.filterConfigService.isCorpPMRole() && response.data[i].NPD_PROJECT_CORP_PM_NAME === userDisplayName)) {
              isNotAccessible = false;
            }
            else if (MONSTER_ROLES.NPD_OPS_PM === taskRole && (this.filterConfigService.isOPSPMRole() && response.data[i].NPD_PROJECT_OPS_PM_NAME === userDisplayName)) {
              isNotAccessible = false;
            }
            else if (MONSTER_ROLES.NPD_SECONDARY_PACKAGING === taskRole && (this.filterConfigService.isSecondaryPackagingRole() && response.data[i].NPD_PROJECT_SECONDARY_AW_SPECIALIST_NAME === userDisplayName)) {
              isNotAccessible = false;
            }
            else if (MONSTER_ROLES.NPD_REGULATORY === taskRole && (this.filterConfigService.isRegulatoryRole() && response.data[i].NPD_PROJECT_REGULATORY_LEAD_NAME === userDisplayName)) {
              isNotAccessible = false;
            }
            else if (taskRole === 'RTM' && (this.filterConfigService.isRegionalTechnicalManagerRole() && response.data[i].NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME === userDisplayName)) {
              isNotAccessible = false;
            }
            else if (MONSTER_ROLES.NPD_PROjECT_SPECIALIST === taskRole && (this.filterConfigService.isProjectSpecialistRole() && response.data[i].NPD_PROJECT_PR_SPECIALIST_NAME === userDisplayName)) {
              isNotAccessible = false;
            }
            else if (isNotAccessible) {
              return true;
            }

          }

        });
      }
    }
  }

  getProjectForAccessValidation(): Observable<any> {
    let displayableFieldsForProject = []
    let conditionObject = []
    let searchConditionList = [];
    searchConditionList.push(this.getSearchField(
      'string',
      'CONTENT_TYPE',
      'MPM.OPERATOR.CHAR.IS',
      'is',
      'MPM_PROJECT',
    ))

    for (var project = 0; project < this.selectedData.length; project++) {
      let projectId = this.selectedData[project].PROJECT_ITEM_ID.split('.')[1];
      if (project == 0) {
        searchConditionList.push(
          this.getSearchField(
            'string',
            'ID',
            'MPM.OPERATOR.CHAR.IS',
            'is',
            projectId,
            'AND'
          ))
      }
      else {
        searchConditionList.push(
          this.getSearchField(
            'string',
            'ID',
            'MPM.OPERATOR.CHAR.IS',
            'is',
            projectId,
            'OR'
          ))
      }

    }
    displayableFieldsForProject.push({ field_id: 'NPD_PROJECT_CORP_PM_NAME' })
    displayableFieldsForProject.push({ field_id: 'NPD_PROJECT_PR_SPECIALIST_NAME' })
    displayableFieldsForProject.push({ field_id: 'NPD_PROJECT_REGIONAL_TECHNICAL_MANAGER_NAME' })
    displayableFieldsForProject.push({ field_id: 'NPD_PROJECT_REGULATORY_LEAD_NAME' })
    displayableFieldsForProject.push({ field_id: 'NPD_PROJECT_OPS_PM_NAME' })
    displayableFieldsForProject.push({ field_id: 'NPD_PROJECT_E2E_PM_NAME' })
    displayableFieldsForProject.push({ field_id: 'NPD_PROJECT_SECONDARY_AW_SPECIALIST_NAME' })
    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: '',
      displayable_field_list: {
        displayable_fields: [
          ...displayableFieldsForProject
        ]
      },
      search_condition_list: {
        search_condition: [...searchConditionList]
      },
      facet_condition_list: {
        facet_condition: (this.facetRestrictionList && this.facetRestrictionList.facet_condition) ? this.facetRestrictionList.facet_condition : []
      },
      sorting_list: {
        sort: []
      },
      cursor: null
    };
    return new Observable((observer) => {
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          observer.next(response);
          observer.complete();
        },
        () => {
          observer.error();
        }

      )
    });

  }

  getSearchField(
    type,
    field_id,
    operator_id,
    operator_name,
    value?,
    field_operator?
  ) {
    let searchField = {
      type: type,
      field_id: field_id,
      relational_operator_id: operator_id,
      relational_operator_name: operator_name,
      value: value,
      relational_operator: field_operator,
    };
    return searchField;
  }
  assignData() {
    const dialogRef = this.dialog.open(BulkResourceAssignmentComponent, {
      width: '25%',
      disableClose: true,
      data: {
        data: this.selectedData,
        isProject: this.selectedData?.[0]?.CONTENT_TYPE === OTMMMPMDataTypes.PROJECT ? true : false,
        modalLabel: 'Bulk Assign ' + (this.selectedData?.[0]?.CONTENT_TYPE === OTMMMPMDataTypes.PROJECT ? 'Project' : 'Task'),
        isAllGFGTasks: this.isAllGFGTaskSelected
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.projectTasksLayoutComponent?.uncheckAllRows();
        this.refresh();
      }
    });
  }

  ngOnInit(): void {
    this.dashboardMenuConfig = {
      FILTER_LIST: [],
      APPLIED_FILTER: [],
      SERACH_FILTER: [],
      IS_PROJECT_TYPE: true,
      IS_CAMPAIGN_TYPE: false,
      SELECTED_SORT: [],
      IS_LIST_VIEW: true,
      projectFilterList: [],
      projectDashBoadFilters: [],
      selectedViewByName: '',
      applyFilterList: [],
      totalProjectCount: 0,
      projectData: [],
      isViewTypeChanged: false,
      isAdvancedSearchOpen: false,
      searchInputValue: '',
      searchCondition: [],
      projectSortOptions: [],
      projectSearchableFields: [],
      projectViewConfig: null,
      projectViewName: null,
      templateViewName: null,
      campaignData: [],
      campaignViewName: null
    };
    this.initializeComponent();
    if (this.activatedRoute.queryParams) {
      this.activatedRoute.queryParams.subscribe((params) => {
        this.queryParams = params;
        if (params.groupByFilter) {
          this.selectedGroupByFilter = params.groupByFilter;
        }
        if (this.selectedGroupByFilter === '' || this.selectedGroupByFilter == undefined
        ) {
          this.selectedGroupByFilter = GroupByFilterTypes.GROUP_BY_PROJECT;
          this.groupByFilterChange(null);
        }
        this.searchName = params.searchName;
        this.savedSearchName = params.savedSearchName;
        this.advSearchData = params.advSearchData;
        this.facetData = params.facetData;
        this.getLoadConfigForColumnChoosers();
      });
    } else {
      this.getLoadConfigForColumnChoosers();
    }

    if (navigator.language !== undefined) {
      this.adapter.setLocale(navigator.language);
    }
    this.dateRangeFormGroup = new FormGroup({
      start_date: new FormControl('', Validators.required),
      end_date: new FormControl('', Validators.required),
      task_name: new FormControl(''),
      task_role: new FormControl(''), //, Validators.required
    });
    this.monsterConfigApiService.getAllRolesForCurrentUser().subscribe(response => {
      const admin = response.find(eachRole => eachRole['@id'].substring(3, eachRole['@id'].indexOf(',')) === MONSTER_ROLES.NPD_ADMIN);
      this.isAdmin = admin != null ? true : false;

    });
  }

  ngOnDestroy(): void {
    this.searchChangeEventService.update(null);
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    if (this.tempsearchConditionsSubscribe) {
      this.tempsearchConditionsSubscribe.unsubscribe();
    }
  }
}
