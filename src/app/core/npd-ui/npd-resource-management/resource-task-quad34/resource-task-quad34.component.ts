import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { ApplicationConfigConstants, AppService, FacetChangeEventService, FieldConfigService, IndexerService, LoaderService, MPMFieldConstants, MPMSearchOperatorNames, MPMSearchOperators, MPM_LEVELS, NotificationService, OTMMMPMDataTypes, OTMMService, ProjectService, SearchChangeEventService, SearchConfigConstants, SearchDataService, SearchRequest, SearchResponse, SharingService, TaskConstants, UtilService, ViewConfig, ViewConfigService } from 'mpm-library';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { ProjectTaskSearchConditions, ResourceManagementConstants } from '../constants/ProjectTaskViewParams';
import { NpdResourceManagementService } from '../services/npd-resource-management.service';
import * as acronui from 'mpm-library';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { MediaMatcher } from '@angular/cdk/layout';
import { GroupByFilterTypes } from 'src/app/core/mpm/deliverables-task-view/groupByFilter/GroupByFilterTypes';
import { ActivatedRoute } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import { TaskCreationModalComponent } from 'src/app/core/mpm/project-view/task-creation-modal/task-creation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { I } from '@angular/cdk/keycodes';
import { NPDConfig } from '../../utils/config/NPDConfig';
import { NpdTaskService } from '../../npd-task-view/services/npd-task.service';
@Component({
  selector: 'app-resource-task-quad34',
  templateUrl: './resource-task-quad34.component.html',
  styleUrls: ['./resource-task-quad34.component.scss']
})
export class ResourceTaskQuad34Component implements OnInit {

  name = 'Angular';
  currentMonth: string;
  currentYear: string;
  count: number = 0;
  weekNames = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa',];
  startDate;
  endDate;
  scrollLeft;
  //userAllocation = [{name: 'project1',show:false, ele: [1,2,3,4]},{name: 'project2',show:false,ele: [5],cn:''},{name: 'project3',show:false,ele: [5]},{name: 'project4',show:false,ele: [5],cn:''}];

  data2 = [{ name: 'project5', show: false, ele: [7, 8, 9, 10] }, { name: 'project5', show: false, ele: [6], cn: '' }, { name: 'project5', show: false, ele: [6] }, { name: 'project5', show: false, ele: [6], cn: '' }];

  page = 1;
  skip = 0;
  top: number;
  pageSize: number;
  pageNumber: number;
  previousRequest;
  level;
  viewConfig;
  pageIndex: number = 0;
  searchConfigId = -1;

  totalListDataCount;
  searchName;
  savedSearchName;
  advSearchData;
  facetData;
  appConfig;
  queryParams;
  selectedListFilter;
  selectedListDependentFilter;
  facetExpansionState = false;
  mobileQuery: MediaQueryList;
  paginator = {
    page: 1,
    skip: 0,
    top: 0,
    pageSize: 0,
    pageNumber: 0,
  }
  private mobileQueryListener: () => void;

  private subscriptions: Array<Subscription> = [];
  isPageRefresh = true;
  validateFacet = false;
  facetRestrictionList;
  isReset = false;
  isRoute = false;
  myTaskViewConfigDetails: ViewConfig = null;


  allTeams;
  allRoles;

  filterUserListData: Observable<any[]>;
  userFormControl = new FormControl();

  allUser;
  currentGroups;


  teamInitial = '';
  roleInitial = '';
  userInitial = '';

  momentPtr = moment;
  selectedTeam;
  selectedRole;

  startWeek;// = '372021';
  endWeek;//= '412021';

  selectedItems: any = [];
  dateArray: any = [];
  calender = [];
  userPoints = [];
  rolePoints = [];
  groupPoints = [];
  allUsers = [];
  totalUsers = [];
  teams = [];
  roles = [];
  users = [];
  allSubGroups = [];
  allUserGroupMappings = [];
  projectListData = [];
  searchConditions = [];
  userAllocation = [];
  integrationConfig;
  isSelected = false;

  @Input() selectedGroupByFilter;
  @Input() currentStartDate;
  @Input() currentEndDate;
  @Input() isRefreshNeeded;
  @Output() taskData = new EventEmitter<any>();
  @Output() selectedUser = new EventEmitter<any>();
  @Output() selectedFilters = new EventEmitter<any>();
  @Output() refreshData = new EventEmitter<any>();

  @ViewChild('scrollQuad') scrollLeftPx: ElementRef;

  constructor(public npdTaskService: NpdTaskService, public sharingService: SharingService, public appService: AppService, public indexerService: IndexerService,
    public fieldConfigService: FieldConfigService, public npdResourceManagementService: NpdResourceManagementService,
    private notificationService: NotificationService, private loaderService: LoaderService, private utilService: UtilService,
    public searchDataService: SearchDataService, private changeDetectorRef: ChangeDetectorRef, private dialog: MatDialog,
    private media: MediaMatcher, private facetChangeEventService: FacetChangeEventService, public activatedRoute: ActivatedRoute,
    public otmmService: OTMMService, public viewConfigService: ViewConfigService, public searchChangeEventService: SearchChangeEventService,
    public projectService: ProjectService) {
    // this.getCurrentMonth();
    // this.getCalenderDays();
    this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
    if (this.mobileQuery.matches) {
      // tslint:disable-next-line: deprecation
      this.mobileQuery.addListener(this.mobileQueryListener);
    }
    this.getStartEndDate();
    this.onload();
    this.getPaintedData();
  }

  // indexerService


  /* getUserAssignedTasks(value?) {

    let keyWord = '';
    let userCN = "Joe";
    //check if works with Date ISO conversion
    //const dates = this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z';
    const conditionObject: ProjectTaskSearchConditions = this.npdResourceManagementService.getTaskSearchCondition(this.selectedGroupByFilter, null,
      MPM_LEVELS.TASK, this.momentPtr.utc(this.startDate).format('YYYY-MM-DDT00:00:00.000') + 'Z',
      this.momentPtr.utc(this.endDate).format('YYYY-MM-DDT00:00:00.000') + 'Z', undefined, false);
    let sortTemp = [];
    let searchCondition;

    //searchCondition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_ACTIVE_USER_CN ,MPMSearchOperators.IS, MPMSearchOperatorNames.IS, 
    //   MPMSearchOperators.AND,userCN);//check userId or userCN
    //conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(searchCondition);
    let allUserSearchConditions = [];
    if (this.userAllocation.length > 0) {
      this.userAllocation.forEach((userAllocation, index) => {
        if (index == 0) {
          allUserSearchConditions.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_ACTIVE_USER_CN, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
            MPMSearchOperators.AND, userAllocation?.cn));//check userId or userCN
        } else {
          allUserSearchConditions.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_ACTIVE_USER_CN, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
            MPMSearchOperators.OR, userAllocation?.cn));//check userId or userCN
        }
      })
    }
    //TASK_ACTIVE_USER_ID or task owner
    //searchCondition = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_OWNER_CN ,MPMSearchOperators.IS, MPMSearchOperatorNames.IS,    MPMSearchOperators.AND,userId);


    if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
      keyWord = this.searchConditions[0].keyword;
    } else if (this.searchConditions && this.searchConditions.length > 0) {
      conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(this.searchConditions);
    }
    if (allUserSearchConditions && allUserSearchConditions.length > 0) {
      conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(allUserSearchConditions);
    }

    const parameters: SearchRequest = {
      search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
      keyword: keyWord,
      search_condition_list: {
        search_condition: [...conditionObject.TASK_CONDITION]
      },
      facet_condition_list: {
        facet_condition: []
      },
      sorting_list: {
        sort: sortTemp
      },
      cursor: {
        page_index: 0,//this.skip,
        page_size: 1//this.top
      }
    };

    if (!(JSON.stringify(this.previousRequest) === JSON.stringify(parameters))) {

      this.previousRequest = parameters;
      this.loaderService.show();
      this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
        let taskData = response.data;
        this.totalListDataCount = response.cursor.total_records;
        //this.facetChangeEventService.updateFacetFilter(response.facet_field_response_list ? response.facet_field_response_list : []);
        this.loaderService.hide();
      }, error => {
        this.notificationService.error('Failed while getting tasks');
        this.loaderService.hide();
      });
    }
  } */


  // npdResourceManagementService

  getTaskCalculatedPointsByWeek(): Observable<any> {
    return new Observable(observer => {
      this.npdResourceManagementService.getUserTaskCalculatedPointsByWeek(this.getPrefixZero(this.startWeek), this.getPrefixZero(this.endWeek)).subscribe(response => {
        if (response?.tuple?.length > 0) {
          this.userPoints = response.tuple
        } else {
          this.userPoints = [];
        }
        observer.next(this.userPoints);
        observer.complete();
      }, (error) => {
        observer.error(error);
        console.log(error);
      });
    });
  }

  getRoleCalculatedPointsByWeek(roleId): Observable<any> {
    return new Observable(observer => {
      this.npdResourceManagementService.getRolePointsByWeek(this.getPrefixZero(this.startWeek), this.getPrefixZero(this.endWeek), roleId).subscribe(response => {
        if (response?.tuple?.length > 0) {
          this.rolePoints = response.tuple
        } else {
          this.rolePoints = [];
        }
        observer.next(this.rolePoints);
        observer.complete();
      }, (error) => {
        observer.error(error);
        console.log(error);
      });
    });
  }

  getGroupCalculatedPointsByWeek(): Observable<any> {
    return new Observable(observer => {
      this.npdResourceManagementService.getGroupPointsByWeek(this.getPrefixZero(this.startWeek), this.getPrefixZero(this.endWeek)).subscribe(response => {
        if (response?.tuple?.length > 0) {
          this.groupPoints = response.tuple
        } else {
          this.groupPoints = [];
        }
        observer.next(this.groupPoints);
        observer.complete();
      }, (error) => {
        observer.error(error);
        console.log(error);
      });
    });
  }

  getUserGroupMappings(): Observable<any> {
    return new Observable(observer => {
      this.npdResourceManagementService.getUserGroupMappings().subscribe(response => {
        this.allUserGroupMappings = response;
        observer.next(this.allUserGroupMappings);
        observer.complete();
      }, (error) => {
        observer.error(error);
        console.log(error);
      });
    });
  }

  getAllSubGroups(): Observable<any> {
    return new Observable(observer => {
      this.npdResourceManagementService.getAllSubGroups().subscribe(response => {
        this.allSubGroups = response;
        observer.next(this.allSubGroups);
        observer.complete();
      }, (error) => {
        observer.error(error);
        console.log(error);
      });
    });
  }

  public changeTaskBarColor(status, task) {
    let style;
    let isOverdue = this.isOverdue(task);
    if (status === "Cancelled") {
      style = "task-cancelled-paint"
      return style;
    } else if (isOverdue && status !== "Approved/Completed") {
      style = "overdue-paint";
      return style;
    }
    else {
      // return this.monsterUtilService.changeTaskStatusColor(status);
      return this.npdResourceManagementService.changeTaskBarColors(status);
    }
  }

  isOverdue(taskData) {
    let currentdate = new Date();
    let estimatedEndDate = new Date(taskData.NPD_TASK_ESTIMATED_COMPLETION_DATE)
    currentdate.setHours(23, 59, 59, 998);
    return currentdate > estimatedEndDate;
  }
  // fieldConfigService


  // appService

  getUsersByRole(role, selectedTeam): Observable<any> {
    return new Observable(observer => {
      let getUserByRoleRequest = {};
      this.users = [];
      //this.userFormControl.setValue('');
      getUserByRoleRequest = {
        allocationType: TaskConstants.ALLOCATION_TYPE_ROLE,
        roleDN: role.ROLE_DN,
        teamID: selectedTeam ? selectedTeam['MPM_Teams-id'].Id : '',
        isApproveTask: false,
        isUploadTask: false
      };

      this.appService.getUsersForTeam(getUserByRoleRequest)
        .subscribe(response => {
          if (response && response.users && response.users.user) {
            const userList = acronui.findObjectsByProp(response, 'user');
            const users = [];
            if (userList && userList.length && userList.length > 0) {
              const groups = this.allUserGroupMappings.filter(ele => ele?.R_PO_IDENTITY?.['Identity-id'].Id == role.Id)
              const groupsIds = groups.map(ele => { ele = ele?.R_PO_SUB_GROUP?.['User_Sub_Groups-id'].Id; return ele; });
              const allAllocatedGroups = this.allSubGroups.filter(ele => {
                const flag = groupsIds.some(item => item == ele['User_Sub_Groups-id'].Id)
                if (flag) {
                  return true;
                }
              });
              this.userAllocation = [...this.userAllocation, ...allAllocatedGroups];
              this.userAllocation.forEach(ele => {
                if (ele?.['User_Sub_Groups-id']) {
                  ele['subGroupArray'] = [];
                }
              });
              userList.forEach(user => {
                const fieldObj = users.find(e => e.value === user.dn);
                const userIdentity = this.totalUsers.find(u => u.cn == user.cn);
                let userGroup;
                let userObject;
                if (userIdentity) {
                  const allUserGroupMappings = this.allUserGroupMappings.filter(ele => ele?.R_PO_IDENTITY?.['Identity-id'].Id == role.Id)
                  userGroup = allUserGroupMappings.find(userGroup => userGroup?.R_PO_USER?.['Identity-id'].Id == userIdentity?.identityId)
                }
                if (!fieldObj) {
                  userObject = {
                    name: user.name,
                    value: user.cn,
                    displayName: user.name,
                    cn: user.cn,
                    userId: userIdentity?.userId,
                    identityId: userIdentity?.identityId,
                    isUser: true,
                    groupId: userGroup?.R_PO_SUB_GROUP?.['User_Sub_Groups-id']?.Id,
                    isFantomUser: userGroup?.IS_FANTOM_USER,
                    showExpand: true,
                    showDelete: true
                  }
                  users.push(
                    userObject
                  );
                  let subGroupIndex = this.userAllocation.findIndex(subGroup => {
                    if (subGroup?.['User_Sub_Groups-id']?.Id) {
                      if (subGroup?.['User_Sub_Groups-id']?.Id == userGroup?.R_PO_SUB_GROUP?.['User_Sub_Groups-id']?.Id) {
                        subGroup.isGroup = true;
                        subGroup.showExpand = true;
                        subGroup.show = false;
                        subGroup.groupId = subGroup?.['User_Sub_Groups-id']?.Id;
                        return true;
                      };
                    }
                  });
                  let selectedResource = userObject;
                  selectedResource.show = false;
                  //selectedResource.ele = [];
                  //selectedResource.ele = ['','','',''];
                  if (subGroupIndex > 0) {
                    if (this.userAllocation[subGroupIndex]?.SubGroupName == selectedResource.value && selectedResource.isFantomUser == "true") {
                      //this.userAllocation[subGroupIndex].userObject = userObject;
                      //this.userAllocation[subGroupIndex].cn = userObject.cn;
                      //this.userAllocation[subGroupIndex].identityId = userObject.identityId;
                      this.userAllocation[subGroupIndex] = Object.assign(this.userAllocation[subGroupIndex], userObject);
                      this.userAllocation[subGroupIndex].isUser = false;
                    } else {
                      this.userAllocation[subGroupIndex]?.subGroupArray.push(selectedResource);
                    }
                  } else {
                    this.userAllocation.push(selectedResource);
                  }
                }
                //do for RA & GFG alone 
              });
            }
            this.users = users;
          } else {
            this.users = [];
            //this.userFormControl.setValue('');
          }
          observer.next([]);
          observer.complete();
        }, (error) => {
          observer.next(error);
          observer.complete();
        });
    });
  }

  getUsersForTeam(team): Observable<any> {
    return new Observable(observer => {
      const getUserByRoleRequest = {
        allocationType: 'ROLE',
        roleDN: team.ROLE_DN,
        teamID: team['MPM_Teams-id'].Id,
        isApproveTask: false,
        isUploadTask: false
      };
      this.appService.getUsersForTeam(getUserByRoleRequest)
        .subscribe(allUsers => {
          if (allUsers && allUsers.users && allUsers.users?.user) {
            allUsers.users.user.forEach(user => {
              const userIdentity = this.totalUsers.find(u => u.cn == user.cn);
              this.users.push({
                name: user.FullName,
                cn: user.UserId,
                userId: userIdentity?.userId,
                identityId: userIdentity?.identityId,
                isUser: true,
                showExpand: true,
                showDelete: true
              });
            });
          }
          observer.next(true);
          observer.complete();
        });
    })
  }

  getProjectDetails(eventData) {
    //open dialog only for task
    let projectId = eventData.PROJECT_ITEM_ID.split('.')[1];
    if (projectId) {
      this.loaderService.show();
      this.projectService.getProjectById(projectId)
        .subscribe(projectObj => {
          this.loaderService.hide();
          if (projectObj) {
            let selectedProject
            selectedProject = projectObj;
            /*  let isTaskActive;
             if(!(eventData.IS_ACTIVE_TASK == "true")) {
               isTaskActive = ((eventData.TASK_STATUS_VALUE != 'Approved/Completed') &&
               (eventData?.PROJECT_OWNER_ID == this.sharingService.getcurrentUserItemID()))?'true':'false';
             }
             else {
               isTaskActive =  eventData.IS_ACTIVE_TASK
             } */
            const dialogRef = this.dialog.open(TaskCreationModalComponent, {
              width: '38%',
              disableClose: true,
              data: {
                projectId: projectId,
                isTemplate: false,
                taskId: eventData.ID,
                projectObj: selectedProject,
                isTaskActive: eventData.IS_ACTIVE_TASK,
                isApprovalTask: false,
                selectedTask: eventData,
                modalLabel: 'Edit Task',
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.refreshData.emit();
              }
            });
          }
        }, (error) => {
          this.loaderService.hide();
          this.notificationService.error('Something went wrong while fetching Project details');
        });
    }
  }
  getTeamsAndUser(role) {
    this.selectedRole = role;
    //  role.ROLE_NAME = role.ROLE_NAME.replace(/_/g, ' ');
    if (this.selectedItems.indexOf(role) !== -1) {
      return;
    } else {
      this.selectedItems[1] = role;
    }
    this.selectedFilters.emit(this.selectedItems);
    //this.loaderService.show();
    this.userAllocation = [];

    if (["GFG", "Regulatory Affairs"].some(ele => ele == role?.NAME)) {
      let roleSelected = {
        name: role.NAME + ' Capacity',
        roleId: role?.Id,//role?.['MPM_Team_Role_Mapping-id']?.Id,
        isRole: true,
        isUser: false,
        show: false,
        ele: ['', '', '', ''],
        showExpand: false,
        showDelete: false
      }
      let selectedResource = roleSelected;
      let unassigned = {
        name: 'Unassigned',
        roleId: 'UNASSIGNED-' + role?.Id,//role?.['MPM_Team_Role_Mapping-id']?.Id,
        actualRoleId: role?.Id,//role?.['MPM_Team_Role_Mapping-id']?.Id,
        isRole: true,
        isUser: false,
        show: false,
        ele: ['', '', '', ''],
        showExpand: true,
        showDelete: false
      }
      this.userAllocation.push(selectedResource);
      this.userAllocation.push(unassigned);
    }

    //this.getUserGroups();
    this.loaderService.show();
    forkJoin([this.getUsersByRole(role, this.selectedTeam), this.getRoleCalculatedPointsByWeek(role?.['MPM_Team_Role_Mapping-id']?.Id), this.getTaskCalculatedPointsByWeek(), this.getGroupCalculatedPointsByWeek()]).subscribe(response => {
      this.loaderService.hide();
    }, (error) => {
      this.loaderService.hide();
    });
  }

  getPoints(isReload?) {
    if (this.startWeek && this.endWeek) {
      forkJoin([this.getTaskCalculatedPointsByWeek(), this.getGroupCalculatedPointsByWeek()]).subscribe(response => {
        if (this.selectedRole) {
          this.getRoleCalculatedPointsByWeek(this.selectedRole?.['MPM_Team_Role_Mapping-id']?.Id).subscribe();
        }
        //this.getRoleCalculatedPointsByWeek()
      });
    }
  }

  getGroupsConfig() {
    forkJoin([this.getAllSubGroups(), this.getUserGroupMappings()]).subscribe(response => { });
  }


  getUpdatedProjectDateArray(i, timeStamp, projectDateArray) {
    const day = new Date(timeStamp).getDate();
    projectDateArray[i] = {};
    projectDateArray[i]['day'] = day;
    projectDateArray[i]['month'] = new Date(timeStamp).getMonth() + 1;
    // projectDateArray[i]['year'] = new Date(timeStamp).getFullYear();
    projectDateArray[i]['year'] = moment(new Date(timeStamp)).isoWeekYear();
    projectDateArray[i];
  }

  onScrollQuad() {
    this.scrollLeft = this.scrollLeftPx.nativeElement.scrollLeft;
  }

  getScrollQuadToStart() {
    this.scrollLeft = 0;
  }

  getProjectDateArray(startDate, endDate) {
    let projectDateArray = [];
    let startTimeStamp = startDate.getTime();
    // this.getUpdatedProjectDateArray(0, startTimeStamp, projectDateArray);
    let endTimeStamp = endDate.getTime();
    let timeStamp: number;
    timeStamp = startTimeStamp;
    let i = 0;
    do {
      if (!(i == 0)) {
        timeStamp = timeStamp + 24 * 60 * 60 * 1000;
      }
      this.getUpdatedProjectDateArray(i, timeStamp, projectDateArray);
      i++;
    } while (timeStamp < endTimeStamp);
    return projectDateArray;
  }

  getPaintedData() {
    this.userAllocation.forEach(item => {
      if (item?.SubGroupName) {
        item.subGroupArray.forEach(each => {
          if (each?.taskData) {
            each.taskData.forEach(segment => {
              segment['arr'] = [];
              segment['paintedData'] = [];
              let finalStartDate;
              let finalEndDate;
              const flagS = (segment['NPD_TASK_ACTUAL_START_DATE'] == '') || (segment['NPD_TASK_ACTUAL_START_DATE'] == 'NA') || (segment['NPD_TASK_ACTUAL_START_DATE'] == null) || (segment['NPD_TASK_ACTUAL_START_DATE'] == undefined);
              const flagD = (segment['NPD_TASK_ACTUAL_DUE_DATE'] == '') || (segment['NPD_TASK_ACTUAL_DUE_DATE'] == 'NA') || (segment['NPD_TASK_ACTUAL_DUE_DATE'] == null) || (segment['NPD_TASK_ACTUAL_DUE_DATE'] == undefined);
              if (!flagS && !flagD) {
                finalStartDate = segment['NPD_TASK_ACTUAL_START_DATE'];
                finalEndDate = segment['NPD_TASK_ACTUAL_DUE_DATE'];
              } else if (flagS && flagD) {
                finalStartDate = segment['TASK_START_DATE'];
                finalEndDate = segment['TASK_DUE_DATE'];
              } else if (flagS && !flagD) {
                finalStartDate = segment['TASK_START_DATE'];
                finalEndDate = segment['NPD_TASK_ACTUAL_DUE_DATE'];
              } else if (!flagS && flagD) {
                finalStartDate = segment['NPD_TASK_ACTUAL_START_DATE'];
                let duration = segment['TASK_DURATION'];
                let finalTimeStamp = new Date(finalStartDate).getTime() + (new Date().getTimezoneOffset() * 60 * 1000);
                finalEndDate = new Date(finalTimeStamp + (((+duration * 7) - 1) * 24 * 60 * 60 * 1000));
              }

              let taskStartDate = new Date(finalStartDate);
              let taskeEndDate = new Date(finalEndDate);
              let taskDateArray = [];
              taskDateArray = this.getProjectDateArray(taskStartDate, taskeEndDate);
              this.dateArray.forEach((piece, i) => {
                let bool = taskDateArray.some(section => {
                  // if((startDate >= date) && (date <= endDate)){
                  if ((section.day == piece.day) && (section.month == piece.month) && (section.year == piece.year)) {
                    return true;
                  } else {
                    return false;
                  }
                })

                if (bool) {
                  segment['arr'][i] = {}
                  segment['arr'][i] = {
                    cell: '1',
                  }
                } else {
                  segment['arr'][i] = {}
                  segment['arr'][i] = {
                    cell: '0',
                  }
                }
              })
              let j = 0;
              segment['arr'].forEach((sec, i) => {
                if (sec?.cell == segment['arr'][i + 1]?.cell) {
                  segment['paintedData'][j] = segment['paintedData'][j] ? segment['paintedData'][j] : {};
                  segment['paintedData'][j] = {
                    rowLength: segment['paintedData'][j].rowLength ? segment['paintedData'][j].rowLength + 20 : 20,
                    count: segment['paintedData'][j].count ? segment['paintedData'][j].count + 1 : 1,
                    cell: sec?.cell
                  };
                } else {
                  if (i != (this.dateArray.length - 1)) {
                    segment['paintedData'][j] = segment['paintedData'][j] ? segment['paintedData'][j] : {};
                    segment['paintedData'][j] = segment['paintedData'][j] ? {
                      rowLength: segment['paintedData'][j].rowLength ? segment['paintedData'][j].rowLength + 20 : 20,
                      count: segment['paintedData'][j].count ? segment['paintedData'][j].count + 1 : 1,
                      cell: segment['arr'][i]?.cell
                    } : {};
                    j++;
                  } else {
                    segment['paintedData'][j] = segment['paintedData'][j] ? segment['paintedData'][j] : {};
                    segment['paintedData'][j] = {
                      rowLength: segment['paintedData'][j].rowLength ? segment['paintedData'][j].rowLength + 20 : 20,
                      count: segment['paintedData'][j].count ? segment['paintedData'][j].count + 1 : 1,
                      cell: sec?.cell
                    };
                  }
                }
              })
            });
          }
        })
      }
      // else {

      // }
      if (item?.taskData) {
        item.taskData.forEach(segment => {
          segment['arr'] = [];
          segment['paintedData'] = [];
          let finalStartDate;
          let finalEndDate;
          const flagS = (segment['NPD_TASK_ACTUAL_START_DATE'] == '') || (segment['NPD_TASK_ACTUAL_START_DATE'] == 'NA') || (segment['NPD_TASK_ACTUAL_START_DATE'] == null) || (segment['NPD_TASK_ACTUAL_START_DATE'] == undefined);
          const flagD = (segment['NPD_TASK_ACTUAL_DUE_DATE'] == '') || (segment['NPD_TASK_ACTUAL_DUE_DATE'] == 'NA') || (segment['NPD_TASK_ACTUAL_DUE_DATE'] == null) || (segment['NPD_TASK_ACTUAL_DUE_DATE'] == undefined);
          if (!flagS && !flagD) {
            finalStartDate = segment['NPD_TASK_ACTUAL_START_DATE'];
            finalEndDate = segment['NPD_TASK_ACTUAL_DUE_DATE'];
          } else if (flagS && flagD) {
            finalStartDate = segment['TASK_START_DATE'];
            finalEndDate = segment['TASK_DUE_DATE'];
          } else if (flagS && !flagD) {
            finalStartDate = segment['TASK_START_DATE'];
            finalEndDate = segment['NPD_TASK_ACTUAL_DUE_DATE'];
          } else if (!flagS && flagD) {
            finalStartDate = segment['NPD_TASK_ACTUAL_START_DATE'];
            let duration = segment['TASK_DURATION'];
            let finalTimeStamp = new Date(finalStartDate).getTime() + (new Date().getTimezoneOffset() * 60 * 1000);
            finalEndDate = new Date(finalTimeStamp + (((+duration * 7) - 1) * 24 * 60 * 60 * 1000));
          }
          let taskStartDate = new Date(finalStartDate);
          let taskeEndDate = new Date(finalEndDate);
          let taskDateArray = [];
          taskDateArray = this.getProjectDateArray(taskStartDate, taskeEndDate);
         
          this.dateArray.forEach((piece, i) => {
            let bool = taskDateArray.some(section => {
              // if((startDate >= date) && (date <= endDate)){
              if ((section.day == piece.day) && (section.month == piece.month) && (section.year == piece.year)) {
                return true;
              } else {
                return false;
              }
            })

            if (bool) {
              segment['arr'][i] = {}
              segment['arr'][i] = {
                cell: '1',
              }
            } else {
              segment['arr'][i] = {}
              segment['arr'][i] = {
                cell: '0',
              }
            }
          })
          let j = 0;
          segment['arr'].forEach((sec, i) => {
            if (sec?.cell == segment['arr'][i + 1]?.cell) {
              segment['paintedData'][j] = segment['paintedData'][j] ? segment['paintedData'][j] : {};
              segment['paintedData'][j] = {
                rowLength: segment['paintedData'][j].rowLength ? segment['paintedData'][j].rowLength + 20 : 20,
                count: segment['paintedData'][j].count ? segment['paintedData'][j].count + 1 : 1,
                cell: sec?.cell
              };
            } else {
              if (i != (this.dateArray.length - 1)) {
                segment['paintedData'][j] = segment['paintedData'][j] ? segment['paintedData'][j] : {};
                segment['paintedData'][j] = segment['paintedData'][j] ? {
                  rowLength: segment['paintedData'][j].rowLength ? segment['paintedData'][j].rowLength + 20 : 20,
                  count: segment['paintedData'][j].count ? segment['paintedData'][j].count + 1 : 1,
                  cell: segment['arr'][i]?.cell
                } : {};
                j++;
              } else {
                segment['paintedData'][j] = segment['paintedData'][j] ? segment['paintedData'][j] : {};
                segment['paintedData'][j] = {
                  rowLength: segment['paintedData'][j].rowLength ? segment['paintedData'][j].rowLength + 20 : 20,
                  count: segment['paintedData'][j].count ? segment['paintedData'][j].count + 1 : 1,
                  cell: sec?.cell
                };
              }
            }
          })
        });
      }
    })
  }

  scrollHandler(scroller) {
    
  }

  getData(part) {
    if (part?.cell == '0') {
      return 1;
    } else if (part?.cell == '1') {
      return 2;
    }
  }

  getRowLength(length) {
    return `${length}px`
  }

  editTask(eventData) {
    if (eventData && eventData?.CONTENT_TYPE == OTMMMPMDataTypes.TASK) {
      this.npdTaskService.setEditTaskHeaderData(eventData);
      this.getProjectDetails(eventData)
    }
  }

  getUserCellPoints(userId, weekYear) {
    let cellPoint;
    let percentage;
    if (this.userPoints.length > 0) {
      cellPoint = this.userPoints.find(userPoint => {
        if (userPoint?.old?.TaskPoints?.WEEK_NO == weekYear && userPoint?.old?.TaskPoints?.USER_ID == userId)
          return userPoint?.old?.TaskPoints.POINTS
      });
      percentage = cellPoint?.old?.TaskPoints?.POINTS ? +(this.checkValueIsNullOrEmpty((+cellPoint.old.TaskPoints.POINTS), 0)).toFixed(2) : '0';//toFixed(0)
      return percentage + '';
    } else {
      return '0'
    }
  }

  getRoleCellPoints(roleId, weekYear, user) {
    let cellPoint;
    let percentage;
    let isPercent = '%';//(user?.name != 'Unassigned') ? '%' : '';
    if (this.rolePoints.length > 0) {
      cellPoint = this.rolePoints.find(userPoint => {
        if (userPoint?.old?.TaskPoints?.WEEK_NO == weekYear && userPoint?.old?.TaskPoints?.ROLE_ID == roleId)
          return (userPoint?.old?.TaskPoints.POINTS) + '' + (isPercent)
      });
      percentage = cellPoint?.old?.TaskPoints?.POINTS ? +((+cellPoint.old.TaskPoints.POINTS).toFixed(2)) : '0';
      return percentage + '' + (isPercent);
    } else {
      return '0' + '' + (isPercent)
    }
  }

  getGroupCellPoints(groupId, weekYear) {
    let cellPoint;
    let percentage;
    if (this.groupPoints.length > 0) {
      cellPoint = this.groupPoints.find(userPoint => {
        if (userPoint?.old?.TaskPoints?.WEEK_NO == weekYear && userPoint?.old?.TaskPoints?.SUB_GROUP_ID == groupId)
          return userPoint?.old?.TaskPoints.POINTS
      });
      percentage = cellPoint?.old?.TaskPoints?.POINTS ? +((+cellPoint.old.TaskPoints.POINTS).toFixed(2)) : '0';//toFixed(0)
      return percentage;
    } else {
      return '0'
    }
  }

  collapseAll() {
    //collapse all views
    this.userAllocation.forEach(allocation => {
      if (allocation['User_Sub_Groups-id']) {
        allocation['subGroupArray'].forEach(ele => {
          ele.show = false;
        })
        allocation.show = false;
      } else {
        allocation.show = false;
      }
    });
  }


  displayFn(user?: any): string | undefined {
    return user ? (user.name || user.FullName) : '';
  }

  updateStartEndDate(startDate, endDate) {
    if (startDate && endDate) {
      this.startDate = new Date(startDate);
      this.endDate = new Date(endDate);
      this.onload();
      this.reload();
    }
  }

  getWidthLength(no, year) {
    let count = 0;
    this.dateArray.forEach(ele => {
      if (ele?.weekNo == no && ele?.year == year) {
        count = count + 1;
      }
    })
    return `${20 * count}Px`
  }

  getWeekNo(i) {
    return this.dateArray[i]?.weekNo == this.dateArray[i + 1]?.weekNo;
  }


  getCellPoints(user, date) {
    
    let weekYear = `${date?.weekNo}${date?.year}`;
    weekYear = this.getPrefixZero(weekYear);
    if (user.isUser) {
      return this.getUserCellPoints(user?.identityId, weekYear) + '%';//user?.identityId
    } else if (user.isRole) {
      return this.getRoleCellPoints(user?.roleId, weekYear, user);
    } else if (user.isGroup) {
      return this.getGroupCellPoints(user?.groupId, weekYear) + '%';
    } else {
      return 0;
    }
  }

  checkValueIsNullOrEmpty(data: any, defaultVale: any): any {
    if (!data || (data['@xsi:nil'] === 'true') || (data === '') || (data['@nil'] === 'true')) {
      return defaultVale;
    }
    return data;
  }

  //3rd Qurad
  getRolesAndUser(team) {

    this.selectedTeam = team;
    if (this.selectedItems.indexOf(team) !== -1) {
      return;
    } else {
      this.selectedItems = [];
      this.selectedItems[0] = team;
      this.roleInitial = '';
    }
    /* this.selectedItems.push(team); */
    this.selectedFilters.emit(this.selectedItems);
    const allTeamRoles = this.sharingService.getAllTeamRoles();
    this.allRoles = [];
    this.users = [];
    this.userAllocation = [];
    //this.userFormControl.setValue('');
    allTeamRoles.forEach(teamRole => {
      if (teamRole && teamRole.R_PO_TEAM !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"] !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"].Id !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"].Id === team['MPM_Teams-id'].Id) {
        const teamRoles = teamRole//this.GetRoleDNByRoleId(teamRole['MPM_Team_Role_Mapping-id'].Id); // this.utilService.GetRoleDNByRoleId(teamRole['MPM_Team_Role_Mapping-id'].Id);
        if (teamRoles !== undefined) {
          this.allRoles.push(teamRoles);
        }
      }
    });
    this.loaderService.show();
    this.getUsersForTeam(team).subscribe(response => {
      this.loaderService.hide();
    }, (error) => {
      this.loaderService.hide();
    });
  }

  onload() {
    // let startDay = this.startDate.getDate();
    this.dateArray = [];
    let startTimeStamp = this.startDate.getTime();
    this.getUpdatedArray(0, startTimeStamp);
    let endTimeStamp = this.endDate.getTime();
    let timeStamp: number;
    timeStamp = startTimeStamp;
    //;
    let i = 1;
    do {
      //;
      timeStamp = timeStamp + 24 * 60 * 60 * 1000;
      this.getUpdatedArray(i, timeStamp);
      i++;
    } while (timeStamp < endTimeStamp);

    if (this.dateArray?.length > 0) {
      this.startWeek = `${this.dateArray[0].weekNo}${this.dateArray[0].year}`;
      this.endWeek = `${this.dateArray[(this.dateArray.length) - 1].weekNo}${this.dateArray[(this.dateArray.length) - 1].year}`;
    }
  }

  getUpdatedArray(i, timeStamp) {
    const day = new Date(timeStamp).getDate();
    this.dateArray[i] = {};
    this.dateArray[i]['day'] = day;
    this.dateArray[i]['month'] = new Date(timeStamp).getMonth() + 1;
    this.dateArray[i]['weekDay'] = this.getDayOfWeek(new Date(timeStamp));
    // if (this.dateArray[i]['weekDay'] == 'Su') {
    //   if(i){
    //     // this.dateArray[i]['weekNo'] = this.dateArray[i - 1]['weekNo'];
    //   }
    // } else {
    //   // this.dateArray[i]['weekNo'] = moment(new Date(timeStamp)).week() - 1;
    //   this.dateArray[i]['weekNo'] = moment(new Date(timeStamp)).isoWeek();
    // }
    this.dateArray[i]['weekNo'] = moment(new Date(timeStamp)).isoWeek();
    // this.dateArray[i]['year'] = new Date(timeStamp).getFullYear();
    this.dateArray[i]['year'] = moment(new Date(timeStamp)).isoWeekYear();
    this.dateArray[i]['date'] = new Date(timeStamp);
  }
  getDayOfWeek(date) {
    const dayOfWeek = new Date(date).getDay();
    return isNaN(dayOfWeek)
      ? null
      : ['Su', 'M', 'T', 'W', 'Th', 'F', 'Sa'][dayOfWeek];
  }

  getCalenderDays() {
    let startDate;
    for (let i = -1; i < 7; i++) {
      let date = new Date();
      let day = new Date(date.setDate(date.getDate() - (date.getDate() + i))).toString().slice(0, 3);
      if (day === "Sun") {
        let date = new Date();
        startDate = new Date(date.setDate(date.getDate() - (date.getDate() + i)));
        break;
      }
    }
    for (let i = 0; i < 42; i++) {
      let fixedDate = new Date();
      this.calender[i] = new Date(new Date(startDate).setDate(new Date(startDate).getDate() + i)).getDate();
    }
  }

  getCurrentMonth() {
    ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"].forEach((ele, i) => {
      if (new Date().getMonth() === i) {
        this.currentMonth = ele;
        this.currentYear = new Date().getFullYear().toString();
      }
    })
  }

  facetToggled(event) {
    this.facetExpansionState = event;
    //this.refreshRequest();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.users.filter(option => option.name?.toLowerCase().includes(filterValue));
  }


  getProjectTask(i, user?, j?) {
    if (this.userAllocation[i]?.SubGroupName) {
      this.userAllocation[i].subGroupArray[j].show = !this.userAllocation[i].subGroupArray[j].show;
    } else {
      this.userAllocation[i].show = !this.userAllocation[i].show;
    }
    // this.data2[i].show = !this.data2[i]?.show;
    // user.show = !user.show;
  }

  getProjectTaskForSubGroups(i, user?) {
    this.userAllocation[i].show = !this.userAllocation[i].show;
  }

  getStartEndDate() {
    this.startDate = new Date(this.getSetDate(1));
    let month = new Date().getMonth();
    if ([1, 3, 5, 7, 8, 10, 12].some(ele => ele == month + 1)) {
      this.endDate = new Date(this.getSetEndDate(31));
    } else if ([4, 6, 9, 11].some(ele => ele == month + 1)) {
      this.endDate = new Date(this.getSetEndDate(30));
    } else {
      this.endDate = new Date(this.getSetEndDate(28));
    }
  }

  getSetEndDate(setDate) {
    const day = new Date(new Date().setDate(setDate)).getDay();
    let date;
    if (day) {
      date = new Date().setDate(setDate) + ((6 - day) * 24 * 60 * 60 * 1000);
    } else {
      date = new Date().setDate(setDate);
    }
    return date;
  }

  getSetDate(setDate) {
    const day = new Date(new Date().setDate(setDate)).getDay();
    let date;
    if (day) {
      date = new Date().setDate(setDate) - ((6 - (6 - (day - 1))) * 24 * 60 * 60 * 1000);
    } else {
      date = new Date().setDate(setDate) - (6 * 24 * 60 * 60 * 1000);
    }
    return date;
  }

  filterUserList() {
    this.filterUserListData = this.userFormControl.valueChanges
      .pipe(
        startWith(''),
        // map(value => this._filter(value))
        map(value => value.length >= 1 ? this._filter(value) : [])
      );
  }

  onUserChange(user) {

  }

  onUserSelected(user) {
    let selectedResource = user?.option?.value;
    selectedResource.show = false;
    // selectedResource.showExpand = false;
    selectedResource.ele = [];
    selectedResource.ele = ['', '']
    const flag = this.userAllocation.some(ele => {
      if (ele['User_Sub_Groups-id']) {
        if (ele.groupId == selectedResource.groupId) {
          const subFlag = ele['subGroupArray'].some(item => item.cn == selectedResource.cn);
          return subFlag;
        } else {
          return false;
        }
      } else {
        return ele.cn == selectedResource.cn
      }
    });
    if (!flag) {
      if (selectedResource.groupId) {
        const flagGroup = this.userAllocation.some(slice => {
          if (slice?.groupId == selectedResource.groupId) {
            return true;
          }
        });
        if (flagGroup) {
          this.userAllocation.forEach(ele => {
            if (ele['User_Sub_Groups-id']) {
              if (ele.groupId == selectedResource.groupId) {
                ele['subGroupArray'].push(selectedResource);
                this.notificationService.success('User Added Successfully');
              }
            }
          });
        } else {
          this.userAllocation.push(selectedResource);
          this.notificationService.success('User Added Successfully');
        }
      } else {
        this.userAllocation.push(selectedResource);
        this.notificationService.success('User Added Successfully');
      }
    } else {
      this.notificationService.warn('The selected User is already in the Current List');
    }
  }

  removeUserAllocation(element, index) {
    if (this.userAllocation[index]['User_Sub_Groups-id']) {
      if (element['User_Sub_Groups-id']) {
        if (this.userAllocation[index]['subGroupArray'].length >= 1) {
          this.notificationService.warn('Remove the Users from this Group');
        } else {
          this.userAllocation.splice(index, 1);
        }
      } else {
        const subIndex = this.userAllocation[index]['subGroupArray'].findIndex(ele => ele.identityId == element.identityId);
        this.userAllocation[index]['subGroupArray'].splice(subIndex, 1)
      }
    } else {
      this.userAllocation.splice(index, 1)
    }
  }

  resetPagination() {
    this.paginator.page = 1;
    this.paginator.skip = 0;
  }

  updatePagination(totalList) {
    this.totalListDataCount = totalList;
  }

  pagination(pagination) {
    this.paginator.pageSize = pagination.pageSize;
    this.paginator.top = pagination.pageSize;
    this.paginator.skip = (pagination.pageIndex * pagination.pageSize);
    this.paginator.page = 1 + pagination.pageIndex;
    //this.refresh();
  }

  updateSearchConfigId() {
    if (this.myTaskViewConfigDetails) {
      const lastValue = this.searchConfigId;
      this.searchConfigId = parseInt(this.myTaskViewConfigDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10);
      if (this.searchConfigId > 0 && lastValue !== this.searchConfigId) {
        this.searchChangeEventService.update(this.myTaskViewConfigDetails);
      }
    }
  }

  setLevel(routedGroupFilter?) {
    this.level = MPM_LEVELS.TASK;
    this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_TASK;
  }

  getPrefixZero(week) {
    if (week?.length == 5) {
      const year = week?.slice(-4);
      const weekNo = week?.slice(0, 1).padStart(2, 0);
      return `${year}1${weekNo}`;
    } else {
      const year = week?.slice(-4);
      const weekNo = week?.slice(0, 2)
      return `${year}1${weekNo}`;
    }
  }

  getViewSpecificTasks(allocationSearchCondition, isExpand, index, allExpand, subIndex?, allocation?) {

    let keyWord = '';
    let startDate = this.startDate?.toISOString();
    let endDate = this.endDate?.toISOString();
    const conditionObject: ProjectTaskSearchConditions = this.npdResourceManagementService.getTaskSearchCondition(this.selectedGroupByFilter, this.selectedListDependentFilter,
      this.level, startDate,
      endDate, undefined, false);
    let sortTemp = [];
    sortTemp = [{
      field_id: ResourceManagementConstants.SORT_FIELDS.TASK.TASK_ESTIMATED_START_DATE,//"TASK_DUE_DATE",//"TASK_NAME",
      order: ResourceManagementConstants.SORT_ORDER.SORT_ASC
    }];
    if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
      keyWord = this.searchConditions[0].keyword;
    } else if (this.searchConditions && this.searchConditions.length > 0) {
      conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(this.searchConditions);
    }

    conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(allocationSearchCondition);

    ResourceManagementConstants.CHECKPOINT_TASKS.forEach((task) => {
      conditionObject.TASK_CONDITION?.push(this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_NAME, MPMSearchOperators.IS_NOT, MPMSearchOperatorNames.IS_NOT,
        MPMSearchOperators.AND, task));
    });

    const parameters: SearchRequest = {
      search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
      keyword: keyWord,
      search_condition_list: {
        search_condition: [...conditionObject.TASK_CONDITION]
      },
      facet_condition_list: {
        facet_condition: []
      },
      sorting_list: {
        sort: sortTemp
      },
      cursor: {
        page_index: 0,//this.skip
        page_size: 100000
      }
    };

    // if (allExpand || !(JSON.stringify(this.previousRequest) === JSON.stringify(parameters))) {

    this.previousRequest = parameters;
    this.loaderService.show();
    this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
      let taskData = response.data;
      if (allocation.isGroup) {
        this.userAllocation[index]['taskData'] = taskData;
      } else {
        if (this.userAllocation[index]?.SubGroupName) {
          this.userAllocation[index].subGroupArray[subIndex]['taskData'] = taskData;
        } else {
          this.userAllocation[index]['taskData'] = taskData;
        }
      }
      this.getPaintedData();
      if (isExpand) {
        if (this.userAllocation[index]?.SubGroupName) {
          this.userAllocation[index].show = true;
          if (subIndex) {
            this.userAllocation[index].subGroupArray[subIndex].show = true;
          }
        } else {
          this.userAllocation[index].show = true;
        }
        //open all the allocations-expand everything & push based on pagination
        //make allocation.isExpand value to true
      }

      //this.totalListDataCount = response.cursor.total_records;
      //this.facetChangeEventService.updateFacetFilter(response.facet_field_response_list ? response.facet_field_response_list : []);
      this.loaderService.hide();
    }, error => {
      this.notificationService.error('Failed while getting tasks');
      this.loaderService.hide();
    });
    // }
  }

  onResourceAllocationExpand(allocation, isExpand?, index?, allExpand?, subIndex?) {
    this.getScrollQuadToStart();
    let searchCondition = [];
    if (allocation.isUser || allocation.isGroup) {
      //check condition for group & user both
      searchCondition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.NPD_TASK_FIELDS.NPD_TASK_RM_USER_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
        MPMSearchOperators.AND, allocation?.identityId);//allocation?.cn
    } else if (allocation.isRole && allocation.actualRoleId) {//only for unnasigned role based tasks
      let roleSearchCondition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.NPD_TASK_FIELDS.NPD_TASK_RM_ROLE_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
        MPMSearchOperators.AND, allocation?.actualRoleId);
      let unassignedRoleSearchCondition = this.fieldConfigService.createConditionObject(ResourceManagementConstants.NPD_TASK_FIELDS.NPD_TASK_RM_USER_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS,
        MPMSearchOperators.AND, 0);
      searchCondition = [roleSearchCondition, unassignedRoleSearchCondition];
    } else if (allocation.isRole) {
      return;
    }
    this.getViewSpecificTasks(searchCondition, isExpand, index, allExpand, subIndex, allocation);
  }

  onContainerScroll(e: Event, subAllocation, i, u) {
    let scrollBottom = e.target["scrollTop"] + e.target["clientHeight"];
    let scrollHeight = e.target["scrollHeight"];
    if (scrollBottom === scrollHeight) {
      this.pageIndex++;
      this.onResourceAllocationExpand(subAllocation, null, i, null, u)
    }
    let scrollTop = e.target["scrollTop"];
    if (scrollTop === 0) {
      if (this.pageIndex > 0) {
        this.pageIndex--;
      }
      this.onResourceAllocationExpand(subAllocation, null, i, null, u)
    }
  }

  expandAllResources() {
    this.isSelected = true;//adv search
    //current pagination
    this.userAllocation.forEach((allocation, index) => {
      if (allocation['User_Sub_Groups-id']) {
        allocation['subGroupArray'].forEach((ele, subIndex) => {
          if (!ele.show) {
            this.onResourceAllocationExpand(ele, true, index, true, subIndex)
          }
        })
        this.onResourceAllocationExpand(allocation, true, index, true);
      } else {
        if (!allocation.show) {
          this.onResourceAllocationExpand(allocation, true, index, true);
        }
      }

    })
  }

  reload() {
    if (this.dateArray?.length > 0) {
      this.getPoints();
    }
    if (this.isSelected) {
      this.expandAllResources();
    }
  }
  getLevelDetails(trigger?) {
    if (this.selectedGroupByFilter === GroupByFilterTypes.GROUP_BY_TASK) {
      if (this.viewConfig) {
        this.viewConfigService.getAllDisplayFeildForViewConfig(this.viewConfig).subscribe((viewDetails: ViewConfig) => {
          this.myTaskViewConfigDetails = viewDetails;
          this.updateSearchConfigId();
        });
      }
    }
    this.reload();
  }

  initializeComponent() {
    this.setLevel();
    this.subscriptions.push(
      this.searchDataService.searchData.subscribe(data => {
        this.searchConditions = (data && Array.isArray(data) ? this.otmmService.addRelationalOperator(data) : []);
        if ((this.searchName || this.savedSearchName || this.advSearchData || this.facetData)) {
          if (data && data.length > 0) {
            if (!this.facetData || ((this.facetData && this.facetRestrictionList) && (this.advSearchData || this.savedSearchName))) {

            }
          }
        }
        this.getLevelDetails();
      })
    );
  }

  refresh(searchConditions?) {
    this.roleInitial = "";
    this.userAllocation = [];
    this.selectedItems = [];
    this.searchConditions = searchConditions;
    this.previousRequest = null;
    this.initializeConfig();
    //this.resetPagination();
    this.getLevelDetails();
    //this.initializeComponent();
    //this.reload();
    //this.getPoints();
  }

  initializeConfig() {
    this.appConfig = this.sharingService.getAppConfig();
    this.pageSize = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
    this.top = this.pageSize;
  }
  loadConfigs() {
    this.integrationConfig = NPDConfig.getIntgrationConfig();
    this.allTeams = this.sharingService.getAllTeams();
    this.selectedTeam = this.allTeams.find(team => team.NAME === ResourceManagementConstants.NPD_TEAM);
    this.allRoles = this.integrationConfig?.resourceManagementRoles;
    this.userAllocation = [];
    this.getGroupsConfig();
    this.appService.getAllUsers().subscribe(allUsers => {
      if (allUsers && allUsers?.User) {
        this.allUser = allUsers?.User;
        allUsers?.User.forEach(user => {
          if (user && user.FullName && typeof (user.FullName) !== 'object') {
            this.users.push({
              name: user.FullName,
              cn: user.UserId,
              userId: user?.toPerson?.['Person-id']?.Id,
              identityId: user?.['Identity-id'].Id,
              isUser: true,
              showExpand: true,
              showDelete: true
            });
            this.totalUsers = this.users;
          }
        });
      }
    });
  }

  ngOnInit(): void {
    this.loadConfigs()
    this.filterUserList();
    this.paginator.pageSize = 5;
    this.paginator.top = this.paginator.pageSize;
  }

}
