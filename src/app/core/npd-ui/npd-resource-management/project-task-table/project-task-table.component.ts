import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { OTMMMPMDataTypes } from 'mpm-library';
import { IndexerDataTypes } from 'mpm-library';
import { FieldConfigService, MPMField } from 'mpm-library';
import { NotificationService } from 'mpm-library';
import { MonsterUtilService } from '../../services/monster-util.service';
import { NpdResourceManagementService } from '../services/npd-resource-management.service';

@Component({
  selector: 'app-project-task-table',
  templateUrl: './project-task-table.component.html',
  styleUrls: ['./project-task-table.component.scss']
})
export class ProjectTaskTableComponent implements OnInit, AfterViewInit {

  @Input() mainHeadercolumnsToDisplay;
  @Input() projectData;
  @Input() resizeInnerWidth;
  @Input() calendarData;
  @Input() dateArray;
  @Input() scrollLeftQuad1;
  @Input() scrollLeftQuad2;
  @Input() scrollLeftQuad1Px;
  @Input() scrollLeftQuad2Px;
  @Input() drag;
  @Input() resizeWidth;
  @Input() selectedGroupByFilter;
  @Input() headOffsetWidth;
  @Input() subLevelHeadercolumnsToDisplay;
  @Input() selection

  @Output() editTask = new EventEmitter<any>();
  @Output() getProjectTask = new EventEmitter<any>();

  @ViewChild('content') defaultWidthOfContent: ElementRef;
  @ViewChild('head') defaultWidthOfHead: ElementRef;



  defaultWidthContent;
  defaultWidth;
  widthOfSecondQuad;



  constructor(
    private fieldConfigService: FieldConfigService,
    private npdResourceManagementService: NpdResourceManagementService,
    private monsterUtilService: MonsterUtilService,
    private notificationService: NotificationService, 
  ) { }

  showExpandIcon(data) {
    if (data.CONTENT_TYPE == OTMMMPMDataTypes.PROJECT) {
      return true;
    }
    return false;
  }
  checkDateType(displayColumn) {
    return displayColumn?.DATA_TYPE?.toLowerCase() === IndexerDataTypes.DATETIME?.toLowerCase()
  }

  getProperty(displayColumn: MPMField, projectData: any): string {
    return this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData);
  }

  taskBarName(data) {
    if (data?.CONTENT_TYPE == OTMMMPMDataTypes.PROJECT) {
      return data?.['PROJECT_NAME'];
    } else {
      return data?.['TASK_NAME'] + ' - ' + data?.['TASK_DESCRIPTION'];
    }
  }


  taskBarNameStatus(data) {
    if (data?.CONTENT_TYPE == OTMMMPMDataTypes.PROJECT) {
      return data?.['PROJECT_STATUS_VALUE'];
    } else {
      return data?.['TASK_STATUS_VALUE'];
    }
  }
  public changeTaskBarColor(status) {
    return this.npdResourceManagementService.changeTaskBarColor(status);
  }
  getRowLength(length) {
    return `${length}px`
  }
  getData(part) {
    if (part?.cell == '0') {
      return 1;
    } else if (part?.cell == '1') {
      return 2;
    }
  }
  getEmptyWidth() {
    return `${this.dateArray.length * 20}px`;
  }

  onScrollQuad() {
    this.scrollLeftQuad1 = 0;
    this.scrollLeftQuad2 = 0;
  }

  onScrollQuad1() {
    this.scrollLeftQuad1 = this.scrollLeftQuad1Px.nativeElement.scrollLeft;
  }

  onScrollQuad2() {
    this.scrollLeftQuad2 = this.scrollLeftQuad2Px.nativeElement.scrollLeft;
  }
  getWidthAdj(width, taskName) {
    if (+(width.replaceAll('px', '')) < 200) {
      return true;
    }
    if (taskName?.length > 50) {
      return true;
    }
  }
  editcurrentTask(eventData) {
    this.editTask.emit(eventData)
  }
  expandProject(project, index) {
    this.onScrollQuad();
    if (project.CONTENT_TYPE == OTMMMPMDataTypes.PROJECT) {
      this.getProjectTask.emit(project);
    }
  }
  getWeek(column, row) {
    let date = this.monsterUtilService.getFieldValueByDisplayColumn(column, row);
    if (date && column?.DATA_TYPE.toLowerCase() === IndexerDataTypes.DATETIME.toLowerCase()) {
      return this.monsterUtilService.calculateWeek(date);
    }
  }
  
  getWidthOfHead(offsetWidth) {
    if (this.defaultWidthContent && this.defaultWidth) {
      this.widthOfSecondQuad = this.defaultWidthContent + (this.defaultWidth - offsetWidth);
      return this.widthOfSecondQuad;
    }
  }

  checkRow(projectData, event) {
    if (event.checked) {
      projectData.selected = true;
      this.selection.select(projectData);
    } else {
      this.selection.deselect(projectData);
    }
    //this.selectedProjectsCallBackHandler.emit(this.selection.selected);
  }

  updateTaskAdjustmentPoint(task, value) {
    if (value?.length > 0) {
      let taskObject = {
        Id: task.ID,
        ItemId: task.ITEM_ID,
        adjustmentPoint: value
      }
      this.npdResourceManagementService.updateTaskAdjustmentPoint(taskObject).subscribe(response => {
        this.notificationService.success('Task Adjustment point updated succesfully.');
      }, (error) => {
        this.notificationService.error('Something went wrong while updating Adjustment point.');
      });
    }
  }


  ngAfterViewInit() {
    this.defaultWidth = this.defaultWidthOfHead.nativeElement.offsetWidth;
    this.defaultWidthContent = this.defaultWidthOfContent.nativeElement.offsetWidth;
    this.getWidthOfHead(this.defaultWidth);
  }
  ngOnInit(): void {
    
  }

}
