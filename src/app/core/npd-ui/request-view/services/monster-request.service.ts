import { Injectable } from '@angular/core';
import { BaseRequest, RequestConfig } from '../constants/RequestConfig';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MonsterRequestService {

  constructor() { }

   // Observable string sources
   private emitChangeSource = new Subject<any>();
   // Observable string streams
   changeEmitted$ = this.emitChangeSource.asObservable();
   relationshipEntityPropsIdMapper = [];

   // Service message commands
   emitChange(change: any) {
     this.emitChangeSource.next(change);
   }

  mapRequestToForm(request,requestType,requestTypeValue?,requestSubjectType?) {
    let requestObject : BaseRequest;

    requestObject = {
      requestId : request.Properties.RequestID ? request.Properties.RequestID : '',//request.Properties.RequestID,
      status : request.Properties.Request_Status,
      requestorName : request.CreatedBy$Properties.FullName,//this.sharingService.getCurrentUserDisplayName();
      requestorUserId : request.CreatedBy$Properties.UserId,
      currentCheckpoint : request.Properties && request.Properties.Current_Checkpoint ? request.Properties.Current_Checkpoint:0,
      projectName :  request.Properties.Project_Name,
      requestItemId : request.Identity.ItemId,
      requestType : requestType,
      requestTypeValue : requestTypeValue,
      requestSubjectType : requestSubjectType
    }
    return requestObject;
  }

  mapDisabilityConfigByTaskCriteria(requestFieldHandler,isDraft,canComplete,isPM,isE2EPM,isReqRework,isBDA) {
    Object.keys(requestFieldHandler).filter(configObject => {
      Object.keys(requestFieldHandler[configObject]).filter(ele => {
        if(requestFieldHandler[configObject][ele] && 'disabled' in requestFieldHandler[configObject][ele])  {
          if(ele != 'businessUnit' && ele != 'stage'
          && ele != 'targetStartDate'
          && ele != 'decisionDate'
          && ele != 'decision'
          && ele != 'earlyProjectClassification'
          && ele != 'earlyProjectClassificationDesc'
          && ele != 'nsvCase'
          && ele != 'cogsCase'
          && ele != 'annualisedEuro') {
            requestFieldHandler[configObject][ele].disabled = canComplete ? false : true// // (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework || isBDA)) ? false : true
          }
        }
      });
    });
    return requestFieldHandler;
     /*   Object.keys(requestFieldHandler.projectCoreData).filter(ele =>
      {
        if(requestFieldHandler.projectCoreData[ele] && disabled in requestFieldHandler.projectCoreData[ele])  {
          requestFieldHandler.projectCoreData[ele].disabled = canComplete ? false : true
          // (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework || isBDA)) ? false : true
        }
      }); */
  }

  initializeRequestConfig() {
    let requestFieldHandler : RequestConfig = {
      projectCoreData : {
        model: {
          ngIf : false,
        },
        businessUnit : {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        bottler: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
      /*   productionSite : {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        }, */
        platform: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        brand : {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        primaryPackagingType: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        secondaryPackagingType : {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        casePackSize: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        consumerUnitSize : {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        variant: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        skuDetails : {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        commercialCategory: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        governanceApproach: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        svpAligned: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        postLaunchAnaylsis : {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        }
      },
      marketScope : {
        harmonisedMarket: {
          disabled : true,
          ngIf : false,
        },
        leadMarket: {
          disabled : true,
          ngIf : false,
        },
        targetDP: {
          disabled : true,
          ngIf : false,
        },
        threeMonthLauchVolume: {
          disabled : true,
          ngIf : false,
        },
        annualVolumne: {
          disabled : true,
          ngIf : false,
        },
        cannibalisationNImpact: {
          disabled : true,
          ngIf : false,
        },
        nsvCase: {
          disabled : true,
          ngIf : false,
        },
        cogsCase: {
          disabled : true,
          ngIf : false,
        },
        annualisedNsv: {
          disabled : true,
          ngIf : false,
        },
        annualisedEuro: {
          disabled : true,
          ngIf : false,
        },
        directlyEnterCurrencyInEuro: {
          disabled :  true,
          ngIf :  false
        },
        grossProfit: {
          disabled : true,
          ngIf : false,
        },
        grossMargin: {
          disabled : true,
          ngIf : false,
        },
        model: {
          ngIf : false,
        },
        actions: {
          ngIf :  false
        }
      },
      deliverablesThreshold : {
        threshold: {
          disabled : true,
          ngIf : false,
        },
        nSVPerCase: {
          disabled : true,
          ngIf : false,
        },
        gM: {
          disabled : true,
          ngIf : false,
        },
        cogs: {
          disabled : true,
          ngIf : false,
        },
        volume: {
          disabled : true,
          ngIf : false,
        },
        ros: {
          disabled : true,
          ngIf : false,
        },
        distribution: {
          disabled : true,
          ngIf : false,
        },
        model : {
          ngIf :  true
        }
      },
      portfolioStrategy : {
        model: {
          ngIf :  false
        },
        incrementalReplacementSku: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : false,
          validators : {
            required : false,
            length : null,
          }
        },
        commercialStrategy: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : false,
          validators : {
            required : false,
            length : null,
          }
        },
        specificCutOffDate : {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : false,
          validators : {
            required : false,
            length : null,
          }
        },
        portfolioDelistStrategy: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : false,
          validators : {
            required : false,
            length : null,
          }
        },
        switchDateAndDrivingDateReason: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : false,
          validators : {
            required : false,
            length : null,
          }
        },
      },
      gainApprovalScope : {
         model : {
          ngIf: false
        },
        whyToDoThisProject: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : false,
          validators : {
            required : false,
            length : null,
          }
        },
        projectIsAbout: {
          disabled : true,
          ngIf : false,
          value : null,
           formControl : false,
          validators : {
            required : false,
            length : null,
          }
        }
      },
      projectClassificationData : {
        model: {
          ngIf : true,
        },
        earlyProjectClassification :  {
           disabled: false,
           ngIf : true,
           value : null,
           formControl : false,
           validators : null
        },
        earlyManufacturingSite : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
           validators : null
        },
        draftManufacturingLocation : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
           validators : null
        },
        projectType: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        newFg : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
           validators : null
        },
        newRnDFormula : {
          disabled: false,
           ngIf : true,
           value : null,
           formControl : false,
           validators : null
        },
        newHBCFormula : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        earlyProjectClassificationDesc : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        newPrimaryPackaging : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        secondaryPackaging : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        registrationDossier : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        preProdReg : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        postProdReg : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        techDesc : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        corpPm : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        opsPm : {
          disabled: false,
           ngIf : true,
           value : null,
           formControl : false,
           validators : null
        },
        programTag : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        rtmUser : {
          disabled: false,
           ngIf : true,
           value : null,
           formControl : false,
           validators : null
        },
        secondaryAWUser : {
          disabled: false,
           ngIf : true,
           value : null,
           formControl : false,
           validators : null
        },
        registrationClassificationAvailable : {
          disabled: false,
          ngIf: true,
          value:null,
          formControl :  null,
          validators : null
        },
        registrationRequirement : {
          disabled: false,
          ngIf: true,
          value:null,
          formControl :  null,
          validators : null
        },
        leadFormula : {
          disabled: false,
          ngIf : true,
          value : null,
          formControl : false,
          validators : null
        },
        e2ePm : {
          disabled: true,
           ngIf : true,
           value : null,
           formControl : false,
           validators : null
        },
      },
      governanceMilestone : {
        model : {
          ngIf: false
        },
        stage : {
          disabled : true,
          ngIf : false
        },
        targetStartDate: {
          disabled : true,
          ngIf : false,
        },
        decisionDate: {
          disabled : true,
          ngIf : false
        },
        decision: {
          disabled : true,
          ngIf : false
        },
        comments: {
          disabled : true,
          ngIf : false
        }
      },
      actionButtons : {
            discard: {
                ngIf : false,
              },
              saveAsDraft: {
                ngIf : false,
              },
              preview: {
                ngIf : false,
              },
              submit: {
                ngIf : false,
              },
              approve: {
                ngIf : false,
              },
              deny: {
                ngIf: false,
              },
              resubmit: {
                ngIf : false,
              },
              convertToProject : {
                ngIf : false,
              },
              complete : {
                ngIf : false,
              },
              backToHome: {
                ngIf : false,
                showConfirmation : false
          }
      },
      other : {
        taskHeader : {
          ngIf : false,
          value : null
        },
        requestedDate : {
          ngIf : false,
          value : null
       },
       autoSave : {
        value : false,
       }
      },
      managerConfig : {
        model : {
          ngIf : false
        },
        e2ePM: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
      },
      requestRationaleConfig : {
        model : {
          ngIf : false
        },
        requestRationale: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
      },
      businessCaseCommentsConfig : {
          model : {
            ngIf : false
          },
          businessCaseComments: {
            disabled : true,
            ngIf : false,
            value : null,
            formControl : true,
            validators : {
              required : false,
              length : null
            }
          }
      },
      programmeManagerReporting : {
        model : {
          ngIf : false
        },
        projectType: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false
          }
        },
        projectSubType: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false
          }
        },
        projectDetail: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false
          }
        },
        delivery: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        linkedMarkets: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        pmComments: {
          disabled : true,
          ngIf : false,
          value : null,
          formControl : true,
          validators : {
            length : null
          }
        }
    }
    }
    return requestFieldHandler;
  }
}
