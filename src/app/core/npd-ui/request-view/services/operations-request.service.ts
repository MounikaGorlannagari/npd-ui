import { Injectable } from '@angular/core';
import { BusinessConstants } from '../constants/BusinessConstants';

@Injectable({
  providedIn: 'root'
})
export class OperationsRequestService {

  constructor() { }

  mapProjectCoreDataConfigurations(request,isDraft,canComplete,isPM,isE2EPM,isReqRework,isRequestView) {
    let projectCoreData = {
        model: {
          ngIf: true,
        },
        businessUnit : {
          disabled: true,
          ngIf: true,
          value: request?.R_PO_BUSINESS_UNIT$Identity && request.R_PO_BUSINESS_UNIT$Identity.ItemId,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        },
        bottler: {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.R_PO_BOTTLER$Identity && request.R_PO_BOTTLER$Identity.ItemId,
          formControl : true,
          validators : {
            required : true,
            length : null
          }
        },
     /*    productionSite : {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.R_PO_PRODUCTION_SITE$Identity && request.R_PO_PRODUCTION_SITE$Identity.ItemId,
          formControl : true,
          validators : {
            required : false,
            length : null
          }
        }, */
        platform: {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.R_PO_PLATFORMS$Identity && request.R_PO_PLATFORMS$Identity.ItemId,
          formControl : true,
          validators : {
            required : true,
            length : null
          }
        },
        brand : {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.R_PO_BRANDS$Identity && request.R_PO_BRANDS$Identity.ItemId,
          formControl : true,
          validators : {
            required : true,
            length : null
          }
        },
        primaryPackagingType: {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.R_PO_PACKAGING_TYPE$Identity && request.R_PO_PACKAGING_TYPE$Identity.ItemId,
          formControl : true,
          validators : {
            required : true,
            length : null
          }
        },
        secondaryPackagingType : {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.Properties?.SecondaryPackagingType,
          formControl : true,
          validators : {
            required : true,
            length : {
              max: 64
            }
          }
        },
        casePackSize: {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.R_PO_CASE_PACK_SIZE$Identity && request.R_PO_CASE_PACK_SIZE$Identity.ItemId,
          formControl : true,
          validators : {
            required : true,
            length : null
          }
        },
        consumerUnitSize : {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.R_PO_CONSUMER_UNIT_SIZE$Identity && request.R_PO_CONSUMER_UNIT_SIZE$Identity.ItemId,
          formControl : true,
          validators : {
            required : true,
            length : null
          }
        },
        variant: {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.R_PO_VARIANT_SKU$Identity && request.R_PO_VARIANT_SKU$Identity.ItemId,
          formControl : true,
          validators : {
            required : true,
            length : null
          }
        },
        skuDetails : {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true,
          ngIf: true,
          value: request?.R_PO_SKU_Details$Identity && request.R_PO_SKU_Details$Identity.ItemId,
          formControl : true,
          validators : {
            required : true,
            length : null
          }
        },
        requestRationale: {
          disabled: true,
          ngIf: false,
          value: null ,//request?.Properties?.Request_Rationale
          formControl : false,
          validators : {
            required : false,
            length : null
          }
        },
        commercialCategory: {
          disabled: true,//(canComplete && (isPM || isE2EPM)) ? false : true,
          ngIf: false,//(isPM || isE2EPM || (!isDraft && isRequestView)),
          value: '',//request?.R_PO_COMMERCIAL_CATEGORISATION$Identity && request.R_PO_COMMERCIAL_CATEGORISATION$Identity.ItemId,
          formControl : false,//true,
          validators : {
            required : false,//true,
            length : null
          }
        },
        governanceApproach: {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework)) ? false : true,
          ngIf: true,//(isPM || (!isDraft && isRequestView)) ? true : false,
          value: request?.R_PO_GOVERNANCE_APPROACH$Identity && request.R_PO_GOVERNANCE_APPROACH$Identity.ItemId,
          formControl : true,//(isPM || (!isDraft && isRequestView)) ? true : false,
          validators : {
            required : isPM ? true : false,
            length : null
          }
        },
        svpAligned: {
          disabled: false,//(canComplete && (isE2EPM)) ? false : true,
          ngIf: false,//(isE2EPM || (!isDraft && isRequestView)),
          value: request?.Properties?.Is_SVP_Aligned,
          formControl : false,//(isE2EPM || (!isDraft && isRequestView)),
          validators : {
            required : false,
            length : null
          }
        },
        postLaunchAnaylsis : {
          disabled: (canComplete && (isE2EPM)) ? false : true,
          ngIf: (isE2EPM || (!isDraft && isRequestView)),
          value: request?.Properties?.PostLaunchAnalysis,
          formControl : (isE2EPM || (!isDraft && isRequestView)),
          validators : {
            required : false,
            length : null
          }
        }
      }
      return projectCoreData;
  }

  mapManagerConfig(request,isDraft,canComplete,isPM,isE2EPM,isReqRework,isRequestView) {
    let managerConfig = {
      model : {
        // ngIf : (isPM || (!isDraft && isRequestView)) ? true : false,
        ngIf : isPM ? true : false,
      },
      e2ePM: {
        disabled: (canComplete && (isPM)) ? false : true,
        ngIf: (isPM || (!isDraft && isRequestView)) ? true : false,
        value: request && request.R_PO_E2E_PM$Identity && request.R_PO_E2E_PM$Identity.ItemId,
        formControl : (isPM || (!isDraft && isRequestView)) ? true : false,
        validators : {
          required : (isPM || (!isDraft && isRequestView)) ? true : false,
          length : null
        }
      },
   }
   return managerConfig;
  }

  mapRequestRationaleConfig(request,isDraft,canComplete,isPM,isE2EPM,isReqRework,isRequestView) {
    let requestRationaleConfig = {
      model : {
        ngIf : true
      },
      requestRationale: {
        disabled:  (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework)) ? false : true,
        ngIf: true,
        value: request?.Properties?.Request_Rationale,
        formControl : true ,
        validators : {
          required : false,
          length : {
            max : 2000
          }
        }
      },
   }
   return requestRationaleConfig;
  }
  mapprogrammeManagerReportingConfigurations(request,isDraft,canComplete,isPM,isE2EPM,isReqRework,isRequestView) {
    let programmeManagerReporting = {
      model: {
        ngIf :  (isPM || (!isDraft && isRequestView)) ? true : false,
      },
      projectType: {
        disabled: (canComplete && (isPM)) ? false : true,
          ngIf: (isPM || (!isDraft && isRequestView)) ? true : false,
          value: request && request.R_PO_CP_PROJECT_TYPE$Identity &&
          request.R_PO_CP_PROJECT_TYPE$Identity.Id,
          formControl : (isPM || (!isDraft && isRequestView)) ? true : false,
          validators : {
            required : true,
          }
      },
      projectSubType: {
        disabled: (canComplete && (isPM)) ? false : true,
          ngIf: (isPM || (!isDraft && isRequestView)) ? true : false,
          value: request && request.R_PO_PROJECT_SUB_TYPE$Identity &&
          request.R_PO_PROJECT_SUB_TYPE$Identity.ItemId,
          formControl : (isPM || (!isDraft && isRequestView)) ? true : false,
          validators : {
            required : true,
          }
      },
      projectDetail: {
        disabled: (canComplete && (isPM)) ? false : true,
          ngIf: (isPM || (!isDraft && isRequestView)) ? true : false,
          value: request && request.R_PO_PROJECT_DETAIL$Identity &&
          request.R_PO_PROJECT_DETAIL$Identity.ItemId,
          formControl : (isPM || (!isDraft && isRequestView)) ? true : false,
          validators : {
            required : true,
          }
      },
      delivery: {
        disabled: (canComplete && (isPM)) ? false : true,
          ngIf: (isPM || (!isDraft && isRequestView)) ? true : false,
          value: request?.R_PO_PM_DELIVERY_QUARTER$Identity?.ItemId,//ProgManagerDeliverQuarter
          formControl : (isPM || (!isDraft && isRequestView)) ? true : false,
          validators : {
            required : true,
            length : {
              max: 500
            }
          }
      },
      linkedMarkets: {
        disabled: true,//(canComplete && (isPM)) ? false : true,
          ngIf: (isPM || (!isDraft && isRequestView)) ? true : false,
          value: request?.Properties?.NumberOfLinkedMarkets,
          formControl : (isPM || (!isDraft && isRequestView)) ? true : false,
          validators : {
            required : true,
            length : {
              max: 64
            }
          }
      },
      pmComments: {
        disabled: (canComplete && (isPM)) ? false : true,
          ngIf: (isPM || (!isDraft && isRequestView)) ? true : false,
          value: request?.Properties?.ProgMangerComments,
          formControl : (isPM || (!isDraft && isRequestView)) ? true : false,
          validators : {
            length : {
              max: 2000
            }
          }
      }
    };
    return programmeManagerReporting;
  }
  mapMarketScopeConfigurations(isDraft,canComplete,isPM,isE2EPM,isReqRework,isRequestView) {
    let marketScope = {
      harmonisedMarket: {
        disabled :  (isDraft) || (canComplete && (isPM || isE2EPM  || isReqRework)) ? false : true,
        ngIf :  true
      },
      leadMarket: {
        disabled :  (isDraft) || (canComplete && (isPM || isE2EPM  || isReqRework)) ? false : true,
        ngIf :  true
      },
      targetDP: {
        disabled :  (isDraft) || (canComplete && (isPM || isE2EPM  || isReqRework)) ? false : true,
        ngIf :  true
      },
      threeMonthLauchVolume: {
        disabled : (isDraft) || (canComplete && (isPM || isE2EPM  || isReqRework)) ? false : true,
        ngIf :  true
      },
      annualVolumne: {
        disabled : (isDraft) || (canComplete && (isPM || isE2EPM  || isReqRework)) ? false : true,
        ngIf :  true
      },
      cannibalisationNImpact: {
        disabled : true,
        ngIf :  false
      },
      nsvCase: {
        disabled : true,
        ngIf :  false
      },
      cogsCase: {
        disabled : true,
        ngIf :  false
      },
      annualisedNsv: {
        disabled : true,
        ngIf :  false
      },
      annualisedEuro: {
        disabled : true,
        ngIf :  false
      },
      directlyEnterCurrencyInEuro: {
        disabled :  true,
        ngIf :  false
      },
      grossProfit: {
        disabled : true,
        ngIf :  false
      },
      grossMargin: {
        disabled : true,
        ngIf :  false
      },
      model: {
        ngIf :  true
      },
      actions: {
        ngIf :  true
      }
    };
    return marketScope;
  }

  mapGovernanceMilestoneConfigurations(request,isDraft,canComplete,isPM,isE2EPM,isReqRework,isRequestView) {
    let governanceMilestoneData = {
        model: {
          ngIf:isPM || isE2EPM || isReqRework  ||  (!isDraft && isRequestView)
        },
        stage: {
          disabled: true,
          ngIf: isPM || isE2EPM || isReqRework  || (!isDraft && isRequestView),
        },
        targetStartDate: {
          disabled: true,
          ngIf: isPM || isE2EPM || isReqRework || (!isDraft && isRequestView),
        },
        decisionDate: {
          disabled: true,
          ngIf: isPM || isE2EPM || isReqRework  || (!isDraft && isRequestView),
        },
        decision: {
          disabled: true,
          ngIf: isPM || isE2EPM || isReqRework  || (!isDraft && isRequestView),
        },
        comments: {
          disabled: true,
          ngIf: isPM || isE2EPM || isReqRework  ||  (!isDraft && isRequestView),
        }
      }
      return governanceMilestoneData;
  }

  mapProjectClassificationConfigurations(request,isDraft,canComplete,isE2EPM,isRequestView,isPM,isReqRework,isAdmin) {
    let projectClassificationData = {
        model: {
          ngIf: true,
        },
        earlyProjectClassification :  {
          disabled: true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.R_PO_PROJECT_CLASSIFICATION$Identity &&
                    request.R_PO_PROJECT_CLASSIFICATION$Identity.ItemId &&
                    request.R_PO_PROJECT_CLASSIFICATION$Properties.ProjectClassificationNumber,
          formControl : isE2EPM || (!isDraft && isRequestView),
          validators : {
            required : false,
            length : null
          }
        },
        earlyManufacturingSite : {
          disabled: isDraft || (canComplete && (isPM || isE2EPM || isReqRework)) || isAdmin ? false : true,
          ngIf: true,//isE2EPM || (!isDraft && isRequestView),
          value: request && request.R_PO_EARLY_MANUFACTURING_SITE$Identity &&
                    request.R_PO_EARLY_MANUFACTURING_SITE$Identity.ItemId,
          formControl :  true,
          validators : {
              required : isE2EPM,
              length : null
          }
        },
        draftManufacturingLocation : {
          disabled: isDraft || (canComplete && (isPM || isE2EPM || isReqRework)) || isAdmin ? false : true,
          ngIf: true,//isE2EPM || (!isDraft && isRequestView),
          value: request && request.R_PO_DRAFT_MANUFACTURING_LOCATION$Identity && request.R_PO_DRAFT_MANUFACTURING_LOCATION$Identity.ItemId,
          formControl :  true,
          validators : {
              required : isE2EPM,
              length : null
          }
        },
        projectType: {
          disabled: (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) || isAdmin ? false : true,
          ngIf: true,
          value: request?.R_PO_PROJECT_TYPE$Identity && request.R_PO_PROJECT_TYPE$Identity.ItemId,
          formControl : true,
          validators : {
            required : true,
            length : null
          }
        },
        newFg : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.NewFGSAP,
          formControl : isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : true,
              length : null
          }
        },
        newRnDFormula : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.NewRDFormula,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : true,
              length : null
          }
        },
        newHBCFormula : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.NewHBCFormula,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : true,
              length : null
          }
        },
        earlyProjectClassificationDesc : {
          disabled: true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.R_PO_PROJECT_CLASSIFICATION$Identity &&
                    request.R_PO_PROJECT_CLASSIFICATION$Identity.ItemId &&
                    request.R_PO_PROJECT_CLASSIFICATION$Properties.ProjectClassificationDescription,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : false,
              length : null
          }
        },
        newPrimaryPackaging : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request &&  request.Properties && request.Properties.NewPrimaryPackaging,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : true,
              length : null
          }
        },
        secondaryPackaging : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.IsSecondaryPackaging,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : true,
              length : null
          }
        },
        registrationDossier : {
          disabled: true,//(canComplete && (isE2EPM)) ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.RegistrationDossier,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : false,
              length : null
          }
        },
        preProdReg : {
          disabled: true,//(canComplete && (isE2EPM)) ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.PreProductionRegistartion,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : false,
              length : null
          }
        },
        postProdReg : {
          disabled: true,//(canComplete && (isE2EPM)) ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.PostProductionRegistartion,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : false,
              length : null
          }
        },
        techDesc : {
          disabled: (canComplete && (isE2EPM)) ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.TechnicalDescription,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : false,
              length : {
                max : 2000
              }
          }
        },
        corpPm : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.R_PO_CORP_PM$Identity && request.R_PO_CORP_PM$Identity.ItemId,
          formControl : isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : true,
              length : null
          }
        },
        opsPm : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.R_PO_OPS_PM$Identity && request.R_PO_OPS_PM$Identity.ItemId,
          formControl : isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : true,
              length : null
          }
        },
        programTag : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request &&  request.R_PO_PROGRAM_TAG$Identity && request.R_PO_PROGRAM_TAG$Identity.ItemId,
          formControl : isE2EPM || (!isDraft && isRequestView) || isAdmin,
          validators : {
              required : false,
              length : null
          }
        },
        rtmUser : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request &&  request.R_PO_RTM$Identity && request.R_PO_RTM$Identity.ItemId,
          formControl : isE2EPM || (!isDraft && isRequestView) || isAdmin,
          validators : {
              required : true,
              length : null
          }

        },
        secondaryAWUser : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request &&  request.R_PO_SECONDARY_AW_SPECIALIST$Identity && request.R_PO_SECONDARY_AW_SPECIALIST$Identity.ItemId,
          formControl : isE2EPM || (!isDraft && isRequestView) || isAdmin,
          validators : {
              required : true,
              length : null
          }
        },
        registrationClassificationAvailable : {
          disabled: true,//(canComplete && (isE2EPM)) ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.Registration_Classification_Available,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : false,
              length : null
          }
        },
        registrationRequirement : {
          disabled: true,//(canComplete && (isE2EPM)) ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.REGISTRATION_Requirement,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : false,
              length : null
          }
        },
        leadFormula : {
          disabled: (canComplete && (isE2EPM)) || isAdmin ? false : true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request && request.Properties && request.Properties.LeadFormula,
          formControl :  isE2EPM || (!isDraft && isRequestView),
          validators : {
              required : false,
              length : null
          }
        },
        e2ePm : {
          disabled: true,
          ngIf: isE2EPM || (!isDraft && isRequestView),
          value: request &&  request.R_PO_E2E_PM$Identity && request.R_PO_E2E_PM$Identity.ItemId,
          formControl : isE2EPM || (!isDraft && isRequestView) ,
          validators : {
              required : true,
              length : null
          }
        },
      }
      return projectClassificationData;
  }

  mapActionButtonsConfigurations(isDraft,canComplete,isPM,isE2EPM,isReqRework,isAdmin?) {
    const isAdminUser = (isAdmin && isAdmin == true) ? true : false;
    let actionButtons = {
        discard: {
          ngIf: isDraft
        },
        saveAsDraft: {
          ngIf: isDraft
        },
        preview: {
          ngIf: false//isDraft
        },
        submit: {
          ngIf: isDraft ///false
        },
        approve: {
          ngIf: (canComplete && (isPM )) ? true : false,
        },
        deny: {
          ngIf: (canComplete && (isPM )) ? true : false,
        },
        resubmit: {
          ngIf: (canComplete && (isPM )) ? true : false,
        },
        convertToProject : {
          ngIf : (canComplete && (isE2EPM)) || isAdminUser ? true : false,
        },
        complete : {
          ngIf : (canComplete && isReqRework) ? true : false,
        },
        backToHome: {
          ngIf : true,
          showConfirmation : (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? true : false,
        }
      }
      return actionButtons;
  }

  mapOtherConfig(request,isDraft,isPM,isE2EPM,isReqRework,isRequestView,canComplete) {
    let otherConfig = {
          taskHeader : {
            ngIf : isPM || isE2EPM || isReqRework  ,
            value : (isPM && BusinessConstants.PM_HEADER ) || (isE2EPM && BusinessConstants.E2E_PM_HEADER )
                    || (isReqRework && BusinessConstants.REQ_REWORK )
          },
          requestedDate : {
            ngIf : isPM || isE2EPM || isReqRework  || (!isDraft && isRequestView),//add for inprogress task also(Excpet for draft)
            value : request && request.Tracking && request.Tracking.CreatedDate
        },
        autoSave : {
          value : isDraft || (canComplete && (isPM || isE2EPM || isReqRework)) ? true : false,
        }
    }
    return otherConfig;
  }

  mapDisabilityConfigByTaskCriteria(requestFieldHandler,isDraft,canComplete,isPM,isE2EPM,isReqRework) {
    Object.keys(requestFieldHandler).filter(configObject => {
      Object.keys(requestFieldHandler[configObject]).filter(ele => {
        if(requestFieldHandler[configObject][ele] && 'disabled' in requestFieldHandler[configObject][ele])  {
          if(ele != 'businessUnit' && ele != 'stage'
          && ele != 'targetStartDate'
          && ele != 'decisionDate'
          && ele != 'decision'
          && ele != 'comments'
          && ele != 'earlyProjectClassification'
          && ele != 'earlyProjectClassificationDesc'
          && ele != 'linkedMarkets') {
            requestFieldHandler[configObject][ele].disabled = canComplete ? false : true// // (isDraft) || (canComplete && (isPM || isE2EPM || isReqRework )) ? false : true
          }
        }
      });
    });
    if(requestFieldHandler?.other?.autoSave) {
      requestFieldHandler.other.autoSave.value =  isDraft || (canComplete && (isPM || isE2EPM || isReqRework)) ? true : false;
    }
    return requestFieldHandler;
  }
}
