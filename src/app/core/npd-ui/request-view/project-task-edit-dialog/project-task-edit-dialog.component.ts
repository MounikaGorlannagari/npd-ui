import { Component, Inject, OnInit,ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-project-task-edit-dialog',
  templateUrl: './project-task-edit-dialog.component.html',
  styleUrls: ['./project-task-edit-dialog.component.scss']
})
export class ProjectTaskEditDialogComponent implements OnInit {
  projectTaskForm: FormGroup;
  @ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: string[] = ['Innovation Technical Proto'];
  allFruits: string[] = ['Project Classification', 'Innovation Technical Proto'];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
  private formBuilder: FormBuilder,) {
    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));
  }
  get formControls() {
    return this.projectTaskForm?.controls
  }
  ngOnInit(): void {
    this.projectTaskForm = this.formBuilder.group({
      plannedStartDate: [null, Validators.required],
      plannedEndDate: [null, Validators.required],
      actualStartDate: [null, Validators.required],
      actualEndDate: [null, Validators.required],
      role: ['', Validators.required],
      status: ['', Validators.required],
      taskOwner: ['', Validators.required],
      annonymous1: ['', Validators.required],
      description: ['', Validators.maxLength(500)],
    });
  }

  closeDialog(){

  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.fruits.push(value);
    }

    // Clear the input value
    event['chipInput']!.clear();

    this.fruitCtrl.setValue(null);
  }

  remove(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFruits.filter(fruit => fruit.toLowerCase().includes(filterValue));
  }
}
