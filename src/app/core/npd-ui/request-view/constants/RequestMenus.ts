export const RequestMenus = {
    menuConfig: [
        {
            LOCATION: 'requestDetails',
            DISPLAY_NAME: 'Request Details',
            ACTIVE: true,
            ICON: 'chrome_reader_mode',
        },
        {
           LOCATION: 'activityTracking',
           DISPLAY_NAME: 'Activity Tracking',
           ACTIVE: false,
           ICON: 'insert_chart', 
        }, 
        {
            LOCATION: 'audit',
            DISPLAY_NAME: 'Audit',
            ACTIVE: false,
            ICON: 'verified_user',
        }, {
            LOCATION: 'discussions',
            DISPLAY_NAME: 'Discussions',
            ACTIVE: false,
            ICON: 'group',
        }],
    MENU_REQUEST_DETAILS: 'requestDetails'
};
