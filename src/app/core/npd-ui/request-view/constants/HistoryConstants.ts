export const HistoryConstant = {
    REQUEST:'REQUEST',
    REQUEST_CONSTANTS: ['NAME', 'IS_HIGH_PROFILE','IS_ORIGINAL', 'NOTES', 'DESCRIPTION', 'DUE_DATE', 'STATUS',
    'Request Priority', 'Licensed Partner', 'CAMPAIGN_ITEM_ID', 'PROJECT_ITEM_ID', 'DELETED_BY',


    'EPISODE_NO','DELIVERY_DATE','FIRST_CUT_DATE','OV_2_0_AUDIO','OV_5_1_AUDIO','OV_5_1_MUSIC_AND_EFFECTS',
    'LOCK_DATE','OV_CC','OV_COMBINED_CONTINUITY_SPOTTING_LIST','OV_FORCED_NARRATIVE','OV_PRORES_DELIVERY_TO_STARZ',
    'OV_SUBTITLE','PROXY_DELIVERY_DATE_TO_DUB_HOUSE','RELEASE_DATE','RUNTIME','SEASON_NO','STARZ_MIX_PLAYBACK_DATE',
    'VERSION_ID','EPISODE_TITLE','TITLE','TITLE_BREF','FINAL_PROTOOLS_DELIVERY_DATE_TO_VENDOR',
    'FINAL_PROXY_DELIVERY_DATE_TO_VENDOR','SCRIPT_DELIVERY_DATE_TO_VENDOR',

    'Final Protools Delivery Date to Vendor Status', 'Final Proxy Delivery Date To Vendor Status', 
    'Proxy Delivery Date to Dub House Status', 'Script Delivery Date to Vendor Status',
    'OV 2.0 Audio Status', 'OV 5.1 Audio Status', 'OV 5.1 Music and Effects Status', 'OV CC Status',
    'OV Combined Continuity & Spotting List Status', 'OV Forced Narrative Status', 'OV ProRes Delivery to Starz Status',
    'OV Subtitle Status', 'RELEASE_STATUS'],
    HISTORY_EVENTS: {
        propertyChange: 'com.opentext.cordys.entityCore.elements.itemListElement.PropertyChangeHistoryEvent',
        relationshipChange: 'com.opentext.cordys.entityCore.elements.RelationshipChangedHistoryEvent'
    },
}