import { DYNAMIC_TYPE } from "@angular/compiler";

export const BusinessConstants = {

  ROLE_E2EPM: 'E2E PM',//PE role name
  ROLE_CORPPM: 'CORP_PM',//PE role name
  ROLE_OPSPM: 'OPS_PM',//PE role name
  ROLE_SECONDARY_ARTWORK_SPECIALIST: 'SECONDARY_PACKAGING',

  ROLE_RTM: 'REGIONAL_TECHNICAL_MANAGER',//PE role name
  ROLE_OPS_PLANNER: 'OPS_PLANNER',//PE role name
  ROLE_GFG: 'GFG',//PE role name
  ROLE_PROJECT_SPECIALIST: 'PROJECT_SPECIALIST',//PE role name
  ROLE_REGULATORY_LEAD: 'REGULATORY_LEAD',//PE role name
  ROLE_COMMERCIAL_LEAD: 'COMMERCIAL',//PE role name
  ROLE_NAME_COLLABORATION_ACCESS: 'Collaboration Access',//appworks role name
  ROLE_FPMS : 'FPMS',//PE role name
  //SECONDARY_PACKAGING,TECH_AND_QUAL
  REQUEST_STATUS_DRAFT: 'InDraft',
  REQUEST_STATUS_ACTIVE: 'Active',
  REQUEST_STATUS_PROGRESS: 'InProgress',
  REQUEST_STATUS_DENIED: 'Denied',
  REQUEST_STATUS_COMPLETED: 'Completed',
  REQUEST_STATUS_DELETED: 'Deleted',
  REQUEST_STATUS: [
    {
      DISPLAY_NAME: 'In Draft',
      VALUE: 'InDraft'
    },
    {
      DISPLAY_NAME: 'Deleted',
      VALUE: 'Deleted'
    },
    {
      DISPLAY_NAME: 'In Progress',
      VALUE: 'InProgress'
    },
    {
      DISPLAY_NAME: 'Denied',
      VALUE: 'Denied'
    },
    {
      DISPLAY_NAME: 'Completed',
      VALUE: 'Completed'
    },
    {
      DISPLAY_NAME: 'Cancelled',
      VALUE: 'Cancelled'
    },
    {
      DISPLAY_NAME: 'Active',
      VALUE: 'Active'
    },
  ],
  TECHNICAL_REQUEST: 'TECHNICAL',
  COMMERCIAL_REQUEST: 'COMMERCIAL',
  OPERATIONAL_REQUEST: 'OPERATIONS',

  searchConditionForDeleteAfterSubmit: {
    'field_id': 'RT_REQUEST_STATUS',
    'relational_operator': 'AND',
    'relational_operator_id': 'MPM.OPERATOR.CHAR.IS_NOT',
    'relational_operator_name': 'is not',
    'type': 'string',
    'value': 'Deleted'
  },

  TECHNICAL_REQUEST2: 'Technical',
  COMMERCIAL_REQUEST2: 'Commercial',
  OPERATIONAL_REQUEST2: 'Operations',

  TECHNICAL_REQUEST_HEADER_NAME: 'Technical Business Request',
  COMMERCIAL_REQUEST_HEADER_NAME: 'Commercial Business Request',
  OPERATIONAL_REQUEST_HEADER_NAME: 'Operations Business Request',

  TASK_ACTIVITY_PM: 'PM Review',
  TASK_ACTIVITY_E2EPM: 'E2E Review',
  TASK_ACTIVITY_BDA: 'Business Dev Approval',
  TASK_ACTIVITY_REQUEST_REWORK: 'Request Rework',
  PM_HEADER: 'PROGRAM MANAGER APPROVAL',
  E2E_PM_HEADER: 'E2E PM CLASSIFICATION',
  REQ_REWORK: 'REQUEST REWORK',
  BDA_HEADER: 'BUSINESS DEVLOPMENT APPROVAL',


  stageDecisions: [
    {
      'displayName': 'Approved',
      'value': 'Approved'
    },
    {
      'displayName': 'Recycle',
      'value': 'Recycle'
    },
    {
      'displayName': 'Stop',
      'value': 'Stop'
    }
  ],

  radioButtons: [
    {
      'displayName': 'Yes',
      'value': true
    },
    {
      'displayName': 'No',
      'value': false
    },
  ],

  incrementalReplacementSKU: [
    {
      'displayName': 'Incremental',
      'value': 'Increment'
    },
    {
      'displayName': 'Replacement',
      'value': 'Replacement'
    },
  ],

  specificCutOffSKU: [
    {
      'displayName': 'Hard',
      'value': 'Hard'
    },
    {
      'displayName': 'Soft',
      'value': 'Soft'
    },
  ],

  TASK_ACTION_APPROVED: 'APPROVED',
  TASK_ACTION_REJECTED: 'DENIED',
  TASK_ACTION_RESUBMIT: 'REQUESTED_CHANGES',


  REQUEST_FORM: {
    projectName: 'Project_Name',
    requestRationale: 'Request_Rationale',
    secondaryPackagingType: 'SecondaryPackagingType',
    status: 'Request_Status',
    svpAligned: 'Is_SVP_Aligned',
    postLaunchAnaylsis: 'PostLaunchAnalysis',
    primaryPackagingType: 'R_PO_PACKAGING_TYPE',
    brand: 'R_PO_BRANDS',
    variant: 'R_PO_VARIANT_SKU',
    platform: 'R_PO_PLATFORMS',
    businessUnit: 'R_PO_BUSINESS_UNIT',
    casePackSize: 'R_PO_CASE_PACK_SIZE',
    consumerUnitSize: 'R_PO_CONSUMER_UNIT_SIZE',
    //productionSite : 'R_PO_PRODUCTION_SITE',
    bottler: 'R_PO_BOTTLER',
    skuDetails: 'R_PO_SKU_Details',
    commercialCategory: 'R_PO_COMMERCIAL_CATEGORISATION',
    governanceApproach: 'R_PO_GOVERNANCE_APPROACH',
    e2ePM: 'R_PO_E2E_PM',
    businessCaseComments: 'BusinessCaseComments'//Business_Case_Comments
  },

  DYNAMIC_COMMERCIAL_MARKET_SCOPE_FORM: {
    annualisedEuro: 'AnnualisedYear1NSV_EURO',
    annualisedNsv: 'AnnualisedYear1NSV_LocalCurrency',
    annualVolumne: 'AnnualisedYear1Volume',
    cogsCase: 'COGCase',
    cannibalisationNImpact: 'Cannibilisation_Impact',
    grossMargin: 'GrossMargin',
    grossProfit: 'GrossProfit',
    leadMarket: 'LeadMarket',
    nsvCase: 'NSVCase',
    targetDP: 'TargetDPInWarehouse',
    threeMonthLauchVolume: 'Three_Month_LaunchVolume',
    market: 'R_PO_MARKET',
    directlyEnterCurrencyInEuro:'EnterCurrencyDirectlyInEuros'
  },

  DYNAMIC_OPERATIONS_MARKET_SCOPE_FORM: {
    leadMarket: 'Lead_Market',
    threeMonthLauchVolume: 'Three_Month_Launch_Volume',
    annualVolumne: 'AnnualisedYear1Volume',
    targetDP: 'TargetDPInWarehouse'
  },

  DYNAMIC_TECHNICAL_MARKET_SCOPE_FORM: {
    leadMarket: 'LEAD_MARKET',
    targetDP: 'TargetDPInWarehouse',
    market: 'R_PO_MARKET'
  },

  DYNAMIC_COMMERCIAL_DELIVERABLES_FORM: {
    ros: 'ROS',
    distribution: 'Distribution',
    volume: 'Volume',
    nSVPerCase: 'NSVPerCase',
    cogs: 'COGS',
    gM: 'GM',
  },

  DYNAMIC_GOVERNANCE_MILESTONE_FORM: {
    stages: 'Stage',
    targetStartDate: 'Target_Start_Date',
    comments: 'Comments',
    decisionDate: 'Decision_Date',
    decision: 'Decision'
  },

  PROJECT_CLASSIFICATION_DATA_FORM: {
    earlyProjectClassification: 'R_PO_PROJECT_CLASSIFICATION',
    newFg: 'NewFGSAP',
    newRnDFormula: 'NewRDFormula',
    newHBCFormula: 'NewHBCFormula',
    earlyProjectClassificationDesc: 'R_PO_PROJECT_CLASSIFICATION',
    newPrimaryPackaging: 'NewPrimaryPackaging',
    secondaryPackaging: 'IsSecondaryPackaging',
    registrationDossier: 'RegistrationDossier',
    preProdReg: 'PreProductionRegistartion',
    postProdReg: 'PostProductionRegistartion',
    techDesc: 'TechnicalDescription',
    earlyManufacturingSite: 'R_PO_EARLY_MANUFACTURING_SITE',
    draftManufacturingLocation: 'R_PO_DRAFT_MANUFACTURING_LOCATION',
    projectType: 'R_PO_PROJECT_TYPE',
    corpPm: 'R_PO_CORP_PM',
    opsPm: 'R_PO_OPS_PM',
    programTag: 'R_PO_PROGRAM_TAG',
    rtmUser: 'R_PO_RTM',
    secondaryAWUser: 'R_PO_SECONDARY_AW_SPECIALIST',
    registrationClassificationAvailable:'Registration_Classification_Available',
    registrationRequirement:'REGISTRATION_Requirement',
    leadFormula:'LeadFormula',
    e2ePm:'RPO_E2E_PM'
  },

  GAIN_APPROVAL_SCOPE_FORM: {
    whyToDoThisProject: 'WhyProject',
    projectIsAbout: 'ProjectAbout'
  },

  PORTFOLIO_STRATEGY_FORM: {
    incrementalReplacementSku: 'IncrementalReplacemntalSKU',
    commercialStrategy: 'CommercialStrategy',
    specificCutOffDate: 'SpecificSKUCutOffIntro',
    portfolioDelistStrategy: 'PortfolioDelistStrategy',
    switchDateAndDrivingDateReason: 'SwitchDateAndDrivingDateReason'
  },

  PROGRAMME_MANAGER_REPORTING: {
    projectType: 'R_PO_CP_PROJECT_TYPE',
    projectSubType: 'R_PO_PROJECT_SUB_TYPE',
    projectDetail: 'R_PO_PROJECT_DETAIL',
    delivery: 'R_PO_PM_DELIVERY_QUARTER',//'ProgManagerDeliverQuarter',
    linkedMarkets: 'NumberOfLinkedMarkets',
    pmComments: 'ProgMangerComments'
  },

  CLASSIFICATION_DATA: [
    { classificationNo: 0 },
    { classificationNo: 1 },
    { classificationNo: 2 },
    { classificationNo: 3 },
    { classificationNo: 4 },
    { classificationNo: 5 },
    { classificationNo: 6 },
    { classificationNo: 7 },
    { classificationNo: 8 },
    { classificationNo: 9 },
    { classificationNo: 10 },
    { classificationNo: 11 },
    { classificationNo: 12 },
    { classificationNo: 13 },
    { classificationNo: 14 },
    { classificationNo: 15 },
    { classificationNo: 16 },
    { classificationNo: 17 },
    { classificationNo: 18 },
    { classificationNo: 19 },
    { classificationNo: 20 },
    { classificationNo: 21 },
    { classificationNo: 22 },
    { classificationNo: 23 },
    { classificationNo: 24 },
    { classificationNo: 25 },
    { classificationNo: 26 },
    { classificationNo: 27 },
    { classificationNo: 28 },
    { classificationNo: 29 },
    { classificationNo: 30 },
    { classificationNo: 31 },
    { classificationNo: 32 },
    { classificationNo: 33 },
    { classificationNo: 34 },
    { classificationNo: 35 },
    { classificationNo: 36 },
    { classificationNo: 37 },
    { classificationNo: 38 },
    { classificationNo: 39 },
    { classificationNo: 40 },
    { classificationNo: 41 },
    { classificationNo: 42 },
    { classificationNo: 43 },
    { classificationNo: 44 },
    { classificationNo: 45 },
    { classificationNo: 46 },
    { classificationNo: 47 },
    { classificationNo: 48 },
    { classificationNo: 49 },
    { classificationNo: 50 },
    { classificationNo: 51 },
    { classificationNo: 52 },
    { classificationNo: 53 },
    { classificationNo: 54 },
    { classificationNo: 55 },
    { classificationNo: 56 },
    { classificationNo: 57 },
    { classificationNo: 58 },
    { classificationNo: 59 },
    { classificationNo: 60 },
    { classificationNo: 61 },
    { classificationNo: 62 },
    { classificationNo: 63 },
    { classificationNo: 64 },
    { classificationNo: 65 },
    { classificationNo: 66 },
    { classificationNo: 67 },
    { classificationNo: 68 },
    { classificationNo: 69 },
    { classificationNo: 70 },
    { classificationNo: 71 },
    { classificationNo: 72 },
    { classificationNo: 73 },
    { classificationNo: 74 },
    { classificationNo: 75 },
    { classificationNo: 76 },
    { classificationNo: 77 },
    { classificationNo: 78 },
    { classificationNo: 79 },
    { classificationNo: 80 },
    { classificationNo: 81 },
    { classificationNo: 82 },
    { classificationNo: 83 },
    { classificationNo: 84 },
    { classificationNo: 85 },
    { classificationNo: 86 },
    { classificationNo: 87 },
    { classificationNo: 88 },
    { classificationNo: 89 },
    { classificationNo: 90 },
    { classificationNo: 91 },
    { classificationNo: 92 },
    { classificationNo: 93 },
    { classificationNo: 94 },
    { classificationNo: 95 },
    { classificationNo: 96 },
    { classificationNo: 97 },
    { classificationNo: 98 },
    { classificationNo: 99 },
    { classificationNo: 100 },
    { classificationNo: 101 },
    { classificationNo: 102 },
    { classificationNo: 103 },
    { classificationNo: 104 },
    { classificationNo: 105 },
    { classificationNo: 106 },
    { classificationNo: 107 },
    { classificationNo: 108 },
    { classificationNo: 109 },
    { classificationNo: 110 },
    { classificationNo: 111 },
    { classificationNo: 112 },
    { classificationNo: 113 },
    { classificationNo: 114 },
    { classificationNo: 115 }
  ],

  DESC_DATA: [
    { desc: 'Launch of existing SKU in another market with no changes' },
    { desc: 'EASY CHANGE: Update BOM only' },
    { desc: 'COMPLEX  CHANGE : FLAVOR AND LABEL ' },
    { desc: 'INGREDIENT TECHNICAL CHANGE (No impact to label)' },
    { desc: 'COMPLEX CHANGE : PRODUCTION CAPABILITY  OR LABEL' },
    { desc: 'MEDIUM  CHANGE : PRODUCTION CAPABILITY' },
    { desc: 'EASY CHANGE : LABEL CHANGE' },
    { desc: 'EASY CHANGE : Secondary Packaging Only' },
    { desc: 'EASY CHANGE : Primary and Secondary Packaging Only' },
    { desc: 'PROMO with Primary Pack Only' },
    { desc: 'PROMO with primary / secondary pack change' },
    { desc: 'PROMO: BOM change only (e.g. Tab colour change)' },
    { desc: 'R&D Formula Change and/ or primary, 2ndary Label Change' },
    { desc: 'R&D Formula Change and/ or primary' },
    { desc: 'HBC Formula Change, primary, 2ndary Label Change' },
    { desc: 'HBC Formula Change, primary' },
    { desc: 'HBC Formula Change, 2ndary packaging)' },
    { desc: 'HBC Formula Change' },
    { desc: 'Label Change , 2ndary packaging' },
    { desc: 'Label Change' },
    { desc: '2ndary packaging' },
    { desc: 'simple' },
    { desc: 'Innovation or Adaption: Technical /  brand / flavor' },
    {
      desc:
        'Innovation or Adaption: Technical /  brand / flavor without secondary pack'
    },
    { desc: 'Market Extension : label and plant specific formula w/ 2nd AW' },
    { desc: 'Market Extension : label and plant specific formula' },
    { desc: 'Market Extension : label w/ 2nd AW' },
    { desc: 'Market Extension : label' },
    { desc: 'Market Extension : 2nd packaging only' },
    { desc: 'Launch of existing SKU in another market with no changes' },
    { desc: 'EASY CHANGE: Update BOM only (ZERO REGISTRATION TASKS)' },
    { desc: 'COMPLEX  CHANGE : FLAVOR AND LABEL' },
    { desc: 'COMPLEX CHANGE : PRODUCTION CAPABILITY  OR LABEL' },
    { desc: 'INGREDIENT TECHNICAL CHANGE (No impact to label)' },
    { desc: 'MEDIUM  CHANGE : PRODUCTION CAPABILITY' },
    { desc: 'EASY CHANGE : LABEL CHANGE' },
    { desc: 'EASY CHANGE : Secondary Packaging Only' },
    { desc: 'EASY CHANGE : Primary and Secondary Packaging Only' },
    { desc: 'PROMO with Primary Pack only' },
    { desc: 'PROMO with primary / secondary pack change' },
    {
      desc:
        'PROMO: BOM change only (e.g. Tab colour change) - ZERO REGISTRATION TASKS'
    },
    { desc: 'R&D Formula Change and/ or primary, 2ndary Label Change' },
    { desc: 'R&D Formula Change and/ or primary' },
    { desc: 'HBC Formula Change, primary, 2ndary Label Change' },
    { desc: 'HBC Formula Change, primary' },
    { desc: 'HBC Formula Change, 2ndary packaging)' },
    { desc: 'HBC Formula Change' },
    { desc: 'Label Change , 2ndary packaging' },
    { desc: 'Label Change' },
    { desc: '2ndary packaging' },
    { desc: 'simple' },
    { desc: 'Innovation or Adaption: Technical /  brand / flavor' },
    {
      desc:
        'Innovation or Adaption: Technical /  brand / flavor without secondary pack'
    },
    { desc: 'Market Extension : label and plant specific formula w/ 2nd AW' },
    { desc: 'Market Extension : label and plant specific formula' },
    { desc: 'Market Extension : label w/ 2nd AW' },
    { desc: 'Market Extension : label' },
    { desc: 'Market Extension : 2nd packaging only' },
    { desc: 'Launch of existing SKU in another market with no changes' },
    { desc: 'EASY CHANGE: Update BOM only (ZERO REGISTRATION TASKS)' },
    { desc: 'COMPLEX  CHANGE : FLAVOR AND LABEL' },
    { desc: 'COMPLEX CHANGE : PRODUCTION CAPABILITY  OR LABEL' },
    { desc: 'INGREDIENT TECHNICAL CHANGE (No impact to label)' },
    { desc: 'MEDIUM  CHANGE : PRODUCTION CAPABILITY' },
    { desc: 'EASY CHANGE : LABEL CHANGE' },
    { desc: 'EASY CHANGE : Secondary Packaging Only' },
    { desc: 'EASY CHANGE : Primary and Secondary Packaging Only' },
    { desc: 'PROMO with Primary Pack only' },
    { desc: 'PROMO with primary / secondary pack change' },
    {
      desc:
        'PROMO: BOM change only (e.g. Tab colour change) - ZERO REGISTRATION TASKS'
    },
    { desc: 'R&D Formula Change and/ or primary, 2ndary Label Change' },
    { desc: 'R&D Formula Change and/ or primary' },
    { desc: 'HBC Formula Change, primary, 2ndary Label Change' },
    { desc: 'HBC Formula Change, primary' },
    { desc: 'HBC Formula Change, 2ndary packaging)' },
    { desc: 'HBC Formula Change' },
    { desc: 'Label Change , 2ndary packaging' },
    { desc: 'Label Change' },
    { desc: '2ndary packaging' },
    { desc: 'simple' },
    { desc: 'Innovation or Adaption: Technical /  brand / flavor' },
    {
      desc:
        'Innovation or Adaption: Technical /  brand / flavor without secondary pack'
    },
    { desc: 'Market Extension : label and plant specific formula w/ 2nd AW' },
    { desc: 'Market Extension : label and plant specific formula' },
    { desc: 'Market Extension : label w/ 2nd AW' },
    { desc: 'Market Extension : label' },
    { desc: 'Market Extension : 2nd packaging only' },
    { desc: 'Launch of existing SKU in another market with no changes' },
    { desc: 'EASY CHANGE: Update BOM only (ZERO REGISTRATION TASKS)' },
    { desc: 'COMPLEX  CHANGE : FLAVOR AND LABEL' },
    { desc: 'COMPLEX CHANGE : PRODUCTION CAPABILITY  OR LABEL' },
    { desc: 'INGREDIENT TECHNICAL CHANGE (No impact to label)' },
    { desc: 'MEDIUM  CHANGE : PRODUCTION CAPABILITY' },
    { desc: 'EASY CHANGE : LABEL CHANGE' },
    { desc: 'EASY CHANGE : Secondary Packaging Only' },
    { desc: 'EASY CHANGE : Primary and Secondary Packaging Only' },
    { desc: 'PROMO with Primary Pack only' },
    { desc: 'PROMO with primary / secondary pack change' },
    {
      desc:
        'PROMO: BOM change only (e.g. Tab colour change) - ZERO REGISTRATION TASKS'
    },
    { desc: 'R&D Formula Change and/ or primary, 2ndary Label Change' },
    { desc: 'R&D Formula Change and/ or primary' },
    { desc: 'HBC Formula Change, primary, 2ndary Label Change' },
    { desc: 'HBC Formula Change, primary' },
    { desc: 'HBC Formula Change, 2ndary packaging)' },
    { desc: 'HBC Formula Change' },
    { desc: 'Label Change , 2ndary packaging' },
    { desc: 'Label Change' },
    { desc: '2ndary packaging' },
    { desc: 'simple' },
    { desc: 'Innovation or Adaption: Technical /  brand / flavor' },
    {
      desc:
        'Innovation or Adaption: Technical /  brand / flavor without secondary pack'
    },
    { desc: 'Market Extension : label and plant specific formula w/ 2nd AW' },
    { desc: 'Market Extension : label and plant specific formula' },
    { desc: 'Market Extension : label w/ 2nd AW' },
    { desc: 'Market Extension : label' },
    { desc: 'Market Extension : 2nd packaging only' }
  ],

  TOTAL_SCORE: [
    { totalScore: 0 },
    { totalScore: 0 },
    { totalScore: 1110000 },
    { totalScore: 110000 },
    { totalScore: 1100000 },
    { totalScore: 100000 },
    { totalScore: 1000000 },
    { totalScore: 10000000 },
    { totalScore: 11000000 },
    { totalScore: 1001000 },
    { totalScore: 11001000 },
    { totalScore: 1000 },
    { totalScore: 11110000 },
    { totalScore: 1110000 },
    { totalScore: 11100000 },
    { totalScore: 1100000 },
    { totalScore: 10100000 },
    { totalScore: 100000 },
    { totalScore: 11000000 },
    { totalScore: 1000000 },
    { totalScore: 10000000 },
    { totalScore: 0 },
    { totalScore: 11111000 },
    { totalScore: 1111000 },
    { totalScore: 11101000 },
    { totalScore: 1101000 },
    { totalScore: 11001000 },
    { totalScore: 1001000 },
    { totalScore: 10001000 },
    { totalScore: 1100000000 },
    { totalScore: 1100000000 },
    { totalScore: 1101110000 },
    { totalScore: 1101100000 },
    { totalScore: 1100110000 },
    { totalScore: 1100100000 },
    { totalScore: 1101000000 },
    { totalScore: 1110000000 },
    { totalScore: 1111000000 },
    { totalScore: 1101001000 },
    { totalScore: 1111001000 },
    { totalScore: 1100001000 },
    { totalScore: 1111110000 },
    { totalScore: 1111110000 },
    { totalScore: 1111100000 },
    { totalScore: 1101100000 },
    { totalScore: 1110100000 },
    { totalScore: 1100100000 },
    { totalScore: 1111000000 },
    { totalScore: 1101000000 },
    { totalScore: 1110000000 },
    { totalScore: 1100000000 },
    { totalScore: 1111111000 },
    { totalScore: 1101111000 },
    { totalScore: 1111101000 },
    { totalScore: 1101101000 },
    { totalScore: 1111001000 },
    { totalScore: 1101001000 },
    { totalScore: 1110001000 },
    { totalScore: 11100000000 },
    { totalScore: 11100000000 },
    { totalScore: 11101110000 },
    { totalScore: 11101100000 },
    { totalScore: 11100110000 },
    { totalScore: 11100100000 },
    { totalScore: 11101000000 },
    { totalScore: 11110000000 },
    { totalScore: 11111000000 },
    { totalScore: 11101001000 },
    { totalScore: 11111001000 },
    { totalScore: 11100001000 },
    { totalScore: 11111110000 },
    { totalScore: 11111110000 },
    { totalScore: 11111100000 },
    { totalScore: 11101100000 },
    { totalScore: 11110100000 },
    { totalScore: 11100100000 },
    { totalScore: 11111000000 },
    { totalScore: 11101000000 },
    { totalScore: 11110000000 },
    { totalScore: 11100000000 },
    { totalScore: 11111111000 },
    { totalScore: 11101111000 },
    { totalScore: 11111101000 },
    { totalScore: 11101101000 },
    { totalScore: 11111001000 },
    { totalScore: 11101001000 },
    { totalScore: 1110001000 },
    { totalScore: 10100000000 },
    { totalScore: 10100000000 },
    { totalScore: 10101110000 },
    { totalScore: 10101100000 },
    { totalScore: 10100110000 },
    { totalScore: 10100100000 },
    { totalScore: 10101000000 },
    { totalScore: 10110000000 },
    { totalScore: 10111000000 },
    { totalScore: 10101001000 },
    { totalScore: 10111001000 },
    { totalScore: 10100001000 },
    { totalScore: 10111110000 },
    { totalScore: 10111110000 },
    { totalScore: 10111100000 },
    { totalScore: 10101100000 },
    { totalScore: 10110100000 },
    { totalScore: 10100100000 },
    { totalScore: 10111000000 },
    { totalScore: 10101000000 },
    { totalScore: 10110000000 },
    { totalScore: 10100000000 },
    { totalScore: 10111111000 },
    { totalScore: 10101111000 },
    { totalScore: 10111101000 },
    { totalScore: 10101101000 },
    { totalScore: 10111001000 },
    { totalScore: 10101001000 },
    { totalScore: 10110001000 }
  ],

  PROJECT_TYPES: {
    COMMERCIAL_NEW_SKU: 'Commercial New SKU',
    LIFT_AND_SHIFT: 'Lift & Shift',
    ON_PACK_PROMO: 'On Pack Promo',
    MANUFACTURING_EXTENSION:'Manufacturing Extension',
    NPD_CHANGE: 'NPD Change',
  },

  // RM_ROLE_ALLOCATION: ['E2E PM','Corp PM','Ops PM','RTM','Project Specialist','GFG','Regulatory Affairs']
}

export const SPField = [
  'NPD_PROJECT_DATE_DEVELOPMENT_COMPLETED_S1',
  'NPD_PROJECT_DATE_PROOFING_COMPLETED_S1',
  'NPD_PROJECT_DATE_DEVELOPMENT_COMPLETED_S2',
  'NPD_PROJECT_DATE_DEVELOPMENT_COMPLETED_S3',
  'NPD_PROJECT_DATE_DEVELOPMENT_COMPLETED_S4',
  'NPD_PROJECT_DATE_PROOFING_COMPLETED_S2',
  'NPD_PROJECT_DATE_PROOFING_COMPLETED_S3',
  'NPD_PROJECT_DATE_PROOFING_COMPLETED_S4',
  'NPD_PROJECT_REOPENED_DATE'
]



