export const TimelineConstants = {
    requestPhases:
        [
            {
                "ACTIVITY_NAME": "Request Rework",
                "ACTIVITY_TYPE": "TASK",
                "END_TIME": "",
                "START_TIME": "",
                "STATUS": "",
                "IS_BDA": false,//,
                "IS_DISPLAY": false,
            },
            {
                "ACTIVITY_NAME": "Business Dev Approval",
                "ACTIVITY_TYPE": "TASK",
                "END_TIME": "",
                "START_TIME": "",
                "STATUS": "",
                "IS_BDA": true,//commercial,
                "IS_DISPLAY": true,
            },
            {
                "ACTIVITY_NAME": "PM Review",//Program Manager Approval
                "ACTIVITY_TYPE": "TASK",
                "END_TIME": "",
                "START_TIME": "",
                "STATUS": "",
                "IS_BDA": false,
                "IS_DISPLAY": true,
            },
            {
                "ACTIVITY_NAME": "E2E Review",
                "ACTIVITY_TYPE": "TASK",
                "END_TIME": "",
                "START_TIME": "",
                "STATUS": "",
                "IS_BDA": false,
                "IS_DISPLAY": true,
            }
        ],
}

export const TimelineState={
  '5':'COMPLETE',
  '2':'WAITING'
}
