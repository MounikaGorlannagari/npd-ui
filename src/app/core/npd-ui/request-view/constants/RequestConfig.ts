
export class RequestConfig {
  projectCoreData : {
    model: {
      ngIf : boolean,
    },
    businessUnit : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    bottler: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
/*     productionSite : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    }, */
    platform: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    brand : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    primaryPackagingType: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    secondaryPackagingType : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    casePackSize: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    consumerUnitSize : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    variant: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    skuDetails : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    commercialCategory: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    governanceApproach: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    svpAligned: {
      disabled :  boolean;
      ngIf :  boolean;
      value : boolean;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    postLaunchAnaylsis : {
      disabled :  boolean;
      ngIf :  boolean;
      value : boolean;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    }
  }
  marketScope: {
    harmonisedMarket: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    leadMarket: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    targetDP: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    threeMonthLauchVolume: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    annualVolumne: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    cannibalisationNImpact: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    nsvCase: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    cogsCase: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    annualisedNsv: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    annualisedEuro: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    directlyEnterCurrencyInEuro: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    grossProfit: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    grossMargin: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    model: {
      ngIf :  boolean;
    },
    actions: {
      ngIf :  boolean;
    }
  }
  deliverablesThreshold : {
    model: {
      ngIf: boolean;
    },
    threshold: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    nSVPerCase: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    gM: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    cogs: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    volume: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    ros: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    distribution: {
      disabled :  boolean;
      ngIf :  boolean;
    }
  }
  portfolioStrategy : {
    model: {
      ngIf: boolean;
    },
    incrementalReplacementSku: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    commercialStrategy: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    specificCutOffDate : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    portfolioDelistStrategy: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    switchDateAndDrivingDateReason : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    }
  }
  gainApprovalScope : {
     model : {
      ngIf: boolean;
    },
    whyToDoThisProject: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    projectIsAbout: {
      disabled :  boolean;
       ngIf :  boolean;
       value : string;
       formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    }
  }
  governanceMilestone : {
    model : {
      ngIf: boolean;
    },
    stage : {
       disabled :  boolean;
       ngIf :  boolean;
    },
    targetStartDate: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    decisionDate: {
      disabled :  boolean;
      ngIf :  boolean;
    },
    decision: {
      disabled :  boolean;
       ngIf :  boolean;
    },
    comments: {
      disabled :  boolean;
       ngIf :  boolean;
    }
  };
  projectClassificationData: {
     model :{
      ngIf :  boolean;
    },
    earlyProjectClassification :  {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    earlyManufacturingSite : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    draftManufacturingLocation : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    projectType: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
    newFg : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    newRnDFormula : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    newHBCFormula : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    earlyProjectClassificationDesc : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    newPrimaryPackaging : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    secondaryPackaging : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    registrationDossier : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    preProdReg : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    postProdReg : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    techDesc : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    corpPm : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    opsPm : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    programTag : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    rtmUser : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    secondaryAWUser : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    registrationClassificationAvailable : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    registrationRequirement: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    leadFormula : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },
    e2ePm : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
        validators : {
          required : boolean,
          length : {
            max : number;
          }
      }
    },

  }
  actionButtons : {
        discard: {
            ngIf: boolean;
          },
          saveAsDraft: {
            ngIf: boolean;
          },
          preview: {
            ngIf: boolean;
          },
          submit: {
            ngIf: boolean;
          },
          approve: {
            ngIf: boolean;
          },
          deny: {
            ngIf: boolean;
          },
          resubmit: {
            ngIf: boolean;
          },
          convertToProject : {
            ngIf: boolean;
          },
          complete : {
            ngIf: boolean;
          },
          backToHome: {
            ngIf: boolean;
            showConfirmation : boolean;
          },
          // delete: {
          //   ngIf: boolean;
          //   // showConfirmation : boolean;
          // }
  }
  other : {
    taskHeader : {
      ngIf : boolean,
      value : string
    },
    requestedDate : {
      ngIf: boolean
      value : string
   },
   autoSave : {
    value : boolean
   }
  }
  managerConfig : {
    model : {
      ngIf : boolean;
    }
    e2ePM: {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    },
  }
  requestRationaleConfig : {
    model : {
      ngIf : boolean;
    }
    requestRationale : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    }
  }
  businessCaseCommentsConfig : {
    model : {
      ngIf : boolean;
    }
    businessCaseComments : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    }
  }
  programmeManagerReporting : {
    model : {
      ngIf : boolean;
    }
    projectType : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
      }
    }
    projectSubType : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
      }
    }
    projectDetail : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
      }
    }
    delivery : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    }
    linkedMarkets : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        required : boolean,
        length : {
          max : number;
        }
      }
    }
    pmComments : {
      disabled :  boolean;
      ngIf :  boolean;
      value : string;
      formControl : boolean;
      validators : {
        length : {
          max : number;
        }
      }
    }
  }
}

export class BaseRequest {
  requestId : string;
  status : string;
  requestorName : string;
  requestorUserId : string;
  currentCheckpoint : string;
  projectName : string;
  requestItemId : string;
  requestType : string;
  requestTypeValue : string;
  requestSubjectType : string;
}
