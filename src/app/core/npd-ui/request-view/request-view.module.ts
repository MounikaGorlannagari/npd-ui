import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestViewRoutingModule } from './request-view-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { WeekPickerComponent } from './generic-components/week-picker/week-picker.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TaskOperationsComponent } from './generic-components/task-operations/task-operations.component';
import { StatusHeaderComponent } from './generic-components/status-header/status-header.component';
import { ProjectNameHeaderComponent } from './generic-components/project-name-header/project-name-header.component';
import { ProjectCoreDataComponent } from './generic-components/project-core-data/project-core-data.component';
import { MarketScopeComponent } from './generic-components/market-scope/market-scope.component';
import { GainApprovalScopeComponent } from './generic-components/gain-approval-scope/gain-approval-scope.component';
import { DeliverablesVsThresholdsComponent } from './generic-components/deliverables-vs-thresholds/deliverables-vs-thresholds.component';
import { GovernanceMilestonesComponent } from './generic-components/governance-milestone/governance-milestones.component';
import { ProjectClassificationComponent } from './generic-components/project-classification/project-classification.component';
import { PortfolioStrategyComponent } from './generic-components/portfolio-strategy/portfolio-strategy.component';
import { ManagerFieldConfigComponent } from './generic-components/manager-field-config/manager-field-config.component';
import { RequestRationaleFieldConfigComponent } from './generic-components/request-rationale-field-config/request-rationale-field-config.component';
import { DatePickerFormatDirective } from './directives/date-picker-format.directive';
import { TechnicalBusinessComponent } from './bkp/technical-business/technical-business.component';
import { GovernanceMilestoneComponent } from './bkp/governance-milestone/governance-milestone.component';
import { ProjectClassificationDataComponent } from './bkp/project-classification-data/project-classification-data.component';
import { TechnicalBusinessRequestComponent } from './request-types/technical-business-request/technical-business-request.component';
import { OperationsBusinessRequestComponent } from './request-types/operations-business-request/operations-business-request.component';
import { CommercialBusinessRequestComponent } from './request-types/commercial-business-request/commercial-business-request.component';
import { RequestLayoutComponent } from './request-layout/request-layout.component';
import { TimeLineReviewComponent } from './time-line-review/time-line-review.component';
import { MpmLibraryModule } from 'mpm-library';
import { DiscussionsComponent } from './discussions/discussions.component';
import { CollabModule } from '../collab/collab.module';
import { HistoryLogComponent } from './history-log/history-log.component';
import { MonsterSharedModule } from '../monster-shared/monster-shared.module';
import { ProjectTaskEditDialogComponent } from './project-task-edit-dialog/project-task-edit-dialog.component';
import { GenericFormComponent } from './generic-components/generic-form/generic-form.component';
import { ProgrammeManagerReportingComponent } from './generic-components/programme-manager-reporting/programme-manager-reporting.component';
import { ClickOutsideDirective } from '../directives/click-outside.directive';
@NgModule({
  declarations: [TechnicalBusinessComponent,GovernanceMilestoneComponent,ProjectClassificationDataComponent,TechnicalBusinessRequestComponent,CommercialBusinessRequestComponent,OperationsBusinessRequestComponent,WeekPickerComponent, TaskOperationsComponent, StatusHeaderComponent, ProjectNameHeaderComponent, ProjectCoreDataComponent, MarketScopeComponent, PortfolioStrategyComponent, GainApprovalScopeComponent, DeliverablesVsThresholdsComponent,GovernanceMilestonesComponent,ProjectClassificationComponent, ManagerFieldConfigComponent, RequestRationaleFieldConfigComponent, DatePickerFormatDirective, RequestLayoutComponent, TimeLineReviewComponent, HistoryLogComponent, DiscussionsComponent, ProjectTaskEditDialogComponent, GenericFormComponent, ProgrammeManagerReportingComponent,ClickOutsideDirective],
  imports: [
    CommonModule,
    RequestViewRoutingModule,
    MaterialModule,
    NgbModule,
    MpmLibraryModule,
    CollabModule,
    MonsterSharedModule
  ],
  exports: [
    WeekPickerComponent,
    TechnicalBusinessRequestComponent,
    CommercialBusinessRequestComponent,
    OperationsBusinessRequestComponent,
    DatePickerFormatDirective,
    GovernanceMilestonesComponent,
    ClickOutsideDirective
  ]
})
export class RequestViewModule { }
