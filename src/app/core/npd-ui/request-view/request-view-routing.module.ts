import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiscussionsComponent } from './discussions/discussions.component';
import { RequestLayoutComponent } from './request-layout/request-layout.component';
import { CommercialBusinessRequestComponent } from './request-types/commercial-business-request/commercial-business-request.component';
import { OperationsBusinessRequestComponent } from './request-types/operations-business-request/operations-business-request.component';
import { TechnicalBusinessRequestComponent } from './request-types/technical-business-request/technical-business-request.component';
import { TimeLineReviewComponent } from './time-line-review/time-line-review.component';
import { HistoryLogComponent } from './history-log/history-log.component';

const routes: Routes = [
  {
    path: '',
    component: RequestLayoutComponent,
    children: [
      {
        path: 'technical/requestDetails',
        component: TechnicalBusinessRequestComponent
      },
      {
        path: 'technical/requestDetails/:requestId',
        component: TechnicalBusinessRequestComponent,
      }, 
      {
        path: 'commercial/requestDetails',
        component: CommercialBusinessRequestComponent
      },
      {
        path: 'commercial/requestDetails/:requestId',
        component: CommercialBusinessRequestComponent,
      },
      {
        path: 'operations/requestDetails',
        component: OperationsBusinessRequestComponent
      },
      {
        path: 'operations/requestDetails/:requestId',
        component: OperationsBusinessRequestComponent,
      },
      {
        path: 'audit',
       component: HistoryLogComponent
      },
      {
        path: 'activityTracking',
        component: TimeLineReviewComponent,
      }, {
        path: 'discussions',
        component: DiscussionsComponent
      },
    ]
  }
          
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestViewRoutingModule { }
