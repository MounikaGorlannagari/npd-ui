import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router, ActivatedRoute } from '@angular/router';
import { RouteService } from 'src/app/core/mpm/route.service';
import { ConfirmationModalComponent, NotificationService, UtilService } from 'mpm-library';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatDialog } from '@angular/material/dialog';
import { MonsterRequestService } from '../services/monster-request.service';
import { EntityService } from '../../services/entity.service';
import { RequestMenus } from '../constants/RequestMenus';
import { EntityListConstant } from '../constants/EntityListConstant';
import { MonsterUtilService } from '../../services/monster-util.service';
import { BusinessConstants } from '../constants/BusinessConstants';
import { RequestService } from '../../services/request.service';
@Component({
  selector: 'app-request-layout',
  templateUrl: './request-layout.component.html',
  styleUrls: ['./request-layout.component.scss'],
  animations: [
    trigger('menuToggle', [
        state('collapsed', style({ width: '64px' })),
        state('expanded', style({ width: '*' })),
        transition('expanded <=> collapsed', animate('100ms ease-in-out')),
    ]),
    trigger('rotatedState', [
        state('default', style({ transform: 'rotate(0deg)' })),
        state('rotated', style({ transform: 'rotate(180deg)' })),
        transition('rotated => default', animate('250ms ease-out')),
        transition('default => rotated', animate('250ms ease-in'))
    ])
]
})
export class RequestLayoutComponent implements OnInit {
    
  mobileQuery: MediaQueryList;
    private mobileQueryListener: () => void;
    currentMenuName;
    urlParams
    isTemplate;
    requestType;
    isExpanded = true;
    menus: any[] = [];
    state = 'default';
    requestId;
    menu;
    selectedRequest;
    isFormEnabled = false;
    assetUrl = 'styles/favicon.png'
    processIndentifier;


    constructor(
        private media: MediaMatcher,
        private router: Router,
        private changeDetectorRef: ChangeDetectorRef,
        private mpmRouteService: RouteService,
        private activatedRoute: ActivatedRoute,
        private dialog: MatDialog,
        private monsterRequestService: MonsterRequestService,
        private utilService: UtilService,
        private entityService: EntityService,
        private monsterUtilService: MonsterUtilService,
        private requestService: RequestService,
    ) {
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
        this.monsterRequestService.changeEmitted$.subscribe(
            data => {
                this.isFormEnabled = data.isAnyActionPossible;
                this.selectedRequest = data.requestDetails;
                this.menu = data.goBackMenu;
            });

    }

    setSideBarExpansionSate() {
        localStorage.setItem('REQUEST-LAYOUT-SIDE-BAR-EXPANDED-STATE', (this.isExpanded ? 'true' : 'false'));
    }

    rotate() {
        this.state = (this.state === 'default' ? 'rotated' : 'default');
        this.setSideBarExpansionSate();
    }

    getSideBarExpansionSate(): string {
        return localStorage.getItem('REQUEST-LAYOUT-SIDE-BAR-EXPANDED-STATE');
    }

    routeToSelectedMenu(menu) {
        if (this.menus.filter(e => {
            return e.ACTIVE === true && e.LOCATION === RequestMenus.MENU_REQUEST_DETAILS;
        })[0]) {
            if (this.requestId) {
                this.confirmBack(() => {
                    this.routeToMenu(menu);
                });
            } else { return; }
        } else {
            this.routeToMenu(menu);
        }
    }
   
    routeToMenu(menu) {
        this.currentMenuName = menu;
        if (this.urlParams) {
            this.selectedRequest = sessionStorage.getItem('SELECTED_REQUEST_VIEW');
            this.selectedRequest = this.selectedRequest && JSON.parse(this.selectedRequest)?.requestDetails;
            if(this.currentMenuName === RequestMenus.MENU_REQUEST_DETAILS) {//&& this.selectedRequest
               // this.mpmRouteService.goToRequestDetailView(this.requestId, this.urlParams, this.selectedRequest?.requestTypeValue?.toLowerCase(), this.selectedRequest?.requestSubjectType);
                this.mpmRouteService.goToRequestDetailView(this.requestId, this.urlParams, this.selectedRequest?.requestTypeValue?.toLowerCase(), this.selectedRequest?.requestSubjectType);
            }
            else {
                //this.urlParams['requestType']= this.selectedRequest?.requestTypeValue?.toLowerCase()
                this.mpmRouteService.goToRequestViewMenus(menu, this.urlParams, this.requestId);
            }
        }
    }

    backToDashboard() {
        if (this.menus.filter(e => {
            return e.ACTIVE === true && e.LOCATION === RequestMenus.MENU_REQUEST_DETAILS;
        })[0]) {
            this.confirmBack(() => {
                this.requestService.setBackNavigation(true);
                // this.goback();
            });
        } else {
            this.goback();
        }
    }

    goback() {
        this.mpmRouteService.goToRequestDashboard(this.menu);
    }

    confirmBack(callback) {
        if (!this.isFormEnabled) {
            if (callback) { callback(); }
        } else {
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'If there are any unsaved changes, it will be lost. Do you want to continue?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result && result.isTrue) {
                    if (callback) { callback(); }
                }
            });
        }
    }
    
    getProcessIndentifier() {
        if (!this.utilService.isValid(this.requestId)) { return; }
        const filters = [];
        filters.push({
          'name': EntityListConstant.REQUEST_ENTITY_PARAMS.ID,
          'operator': 'eq',
          'value': this.requestId.split('.')[1]
        });
        const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.REQUEST, filters, '', this.utilService.APP_ID);
        this.entityService.getEntityObjects(EntityListConstant.REQUEST, filterParam, null, null, null, null, this.utilService.APP_ID)
          .subscribe((requestResponse) => {
            if (requestResponse && requestResponse.result &&
              requestResponse.result.items && requestResponse.result.items.length === 1) {
              this.processIndentifier = requestResponse.result.items[0].Properties?.PROCESS_IDENTIFIER
            }
          });
    }

    readUrlParams(callback) {
        this.activatedRoute.queryParams.subscribe(queryParams => {
            if (this.activatedRoute.parent.params && this.activatedRoute.parent.params['value']
                && this.activatedRoute.parent.params['value'].requestId) {
                this.activatedRoute.parent.params.subscribe(parentRouteParams => {
                    callback(parentRouteParams, queryParams);
                });
            } else if (this.activatedRoute.children[0].params && this.activatedRoute.children[0].params['value']
                && this.activatedRoute.children[0].params['value'].requestId) {
                this.activatedRoute.children[0].params.subscribe(childRouteParams => {
                    callback(childRouteParams, queryParams);
                });
            } else {
                this.activatedRoute.params.subscribe(routeParams => {
                    callback(routeParams, queryParams);
                });
            }
        });
    }

    handleMenuChange() {
        if (this.menus && this.menus.length > 0) {
            this.menus.forEach(menu => {
                if (this.router.url.includes(menu.LOCATION)) {
                    menu.ACTIVE = true;
                } else {
                    menu.ACTIVE = false;
                }
            });
        }
    }

    ngOnInit() {
        this.router.events.subscribe(res => {
            this.handleMenuChange();
        });
        const lastExpansionState = this.getSideBarExpansionSate();
        if (lastExpansionState) {
            this.isExpanded = lastExpansionState === 'true' ? true : false;
        }
        this.readUrlParams((routeParams, queryParams) => {
            if (queryParams && routeParams) {
                this.requestId = routeParams.requestId;
                this.urlParams = queryParams;
                this.requestType = this.urlParams.menu;
                if (this.urlParams.IsEmailUrl === 'true') { this.requestType = 'Email'; }
                this.menu = this.urlParams.menu;
                this.getProcessIndentifier();
                if (this.urlParams && this.urlParams.isTemplate && this.urlParams.isTemplate === 'true') {
                    this.isTemplate = true;
                }
                //check collab role-display comments menu only for users having collaboration access role
                if(this.requestService.hasRole(BusinessConstants.ROLE_NAME_COLLABORATION_ACCESS)) {
                    this.menus = RequestMenus.menuConfig;
                } else {
                    this.menus = RequestMenus.menuConfig.filter(requestMenu => requestMenu.LOCATION != 'discussions');
                }  
            }
            this.handleMenuChange();
        });

    }
    
    ngOnDestroy(): void {

    }


}
