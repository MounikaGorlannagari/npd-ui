import { Component, OnInit } from '@angular/core';
import { LoaderService, NotificationService, UtilService } from 'mpm-library';
import * as acronui from 'mpm-library';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { RouteService } from 'src/app/core/mpm/route.service';
import { EntityService } from '../../services/entity.service';
import { RequestService } from '../../services/request.service';
import { EntityListConstant } from '../constants/EntityListConstant';
import { HistoryConstant } from '../constants/HistoryConstants';

@Component({
  selector: 'app-history-log',
  templateUrl: './history-log.component.html',
  styleUrls: ['./history-log.component.scss']
  // animations: [trigger('detailExpand', [
  //   state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
  //   state('expanded', style({ height: '*' })),
  //   transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.42, 0.0, 0.58, 1.0)')),
  // ])]

})
export class HistoryLogComponent implements OnInit {

  constructor(
    private loaderService: LoaderService,
    private utilService: UtilService,
    private entityService: EntityService,
    private requestService: RequestService,
    private notificationService: NotificationService,
    private activeRoute: ActivatedRoute,
    // private sharedCallService: SharedCallService,
    private routerService: RouteService
  ) {
   }
  locRequestId;
  requestItemId;
  plannerRequestItemId;
  currentHistoryLogs: any[] = [];
  rawLogs = [];
  requestDetails;
  sortorder = 'desc';
  menu;
  urlParams;

  localizationComponentList = [];
  displayedSubColumns: string[] = ['Expand', 'language'];

  backToDashboard() {
    this.routerService.goToRequestDashboard(this.menu,this.urlParams && this.urlParams.listDependentFilter?this.urlParams.listDependentFilter : null );
  }

  formHistorLogdata(entityName){
    // this.requestService.fetchRelationshipMappers(this.rawLogs, () => {
    //   this.currentHistoryLogs = this.requestService.formatHistoryLog(this.rawLogs,
    //     entityName, this.requestDetails, this.sharedCallService.relationshipEntityPropsIdMapper);
    //   this.loaderService.hide();
    // });
  }
  fetchRequestHistoryLog() {
    this.loaderService.show();
    const entityName = HistoryConstant.REQUEST;
    this.requestItemId = this.requestDetails.Identity.ItemId;
    // this.plannerRequestItemId = this.requestDetails.R_PO_REQUEST_TYPE$Identity.ItemId;
    if (this.utilService.isValid(this.locRequestId)) {
      this.entityService.getHistoryByItemId(this.locRequestId).subscribe(response => {
        this.rawLogs = acronui.findObjectsByProp(response, 'History');
        this.currentHistoryLogs = this.rawLogs;
        if (this.utilService.isValid(this.requestItemId)) {
          this.entityService.getHistoryByItemId(this.requestItemId).subscribe(reqResponse => {
            this.rawLogs = this.rawLogs.concat(acronui.findObjectsByProp(reqResponse, 'History'));
            this.currentHistoryLogs = this.rawLogs;
              this.formHistorLogdata(entityName);
              this.loaderService.hide();
          }, (error) => {
            this.loaderService.hide();
            this.notificationService.error('Unable to load History Logs');
          });
        } else {

          // this.currentHistoryLogs = this.requestService.formatHistoryLog(this.rawLogs,
          //   entityName, this.requestDetails, this.sharedCallService.relationshipEntityPropsIdMapper);
          this.loaderService.hide();
        }

      }, (error) => {
        this.loaderService.hide();
        this.notificationService.error('Unable to load History Logs');
      });
    }
  }
  changeSort() {
    this.sortorder = (this.sortorder === 'desc') ? 'asc' : 'desc';
    this.currentHistoryLogs.reverse();
  }
  // fetchLocalizationComponentHistory(locComponent) {
  //   if (locComponent.currentHistoryLogs) {
  //     return;
  //   }
  //   this.loaderService.show();
  //   const entityName = HistoryConstant.ENTITY_LOC_COMPONENT;
  //   const locComponentId = locComponent.Identity.ItemId;
  //   if (this.utilService.isValid(locComponentId)) {
  //     this.entityService.getHistoryByItemId(locComponentId).subscribe(response => {
  //       const historyLogs = acronui.findObjectsByProp(response, 'History');
  //       locComponent.currentHistoryLogs = this.requestService.formatHistoryLog(historyLogs,
  //         entityName, this.requestDetails, this.sharedCallService.relationshipEntityPropsIdMapper);
  //       this.loaderService.hide();
  //     }, (error) => {
  //       this.loaderService.hide();
  //       this.notificationService.error('Unable to load History Logs');
  //     });
  //   }
  // }
  // expandRow(event, row) {
  //   event.stopPropagation();
  //   if (row.isExpanded) {
  //     row.isExpanded = false;
  //   } else {
  //     row.isExpanded = true;
  //     this.fetchLocalizationComponentHistory(row);
  //   }
  // }
  // localizationComponentDetails() {
  //   this.loaderService.show();
  //   this.loaderService.show();
  //   this.localizationComponentList = [];
  //   const filters = [];
  //   filters.push({
  //     name: LocalizationConstants.MPM_PARA_LOC_CONTENT_LOC_REQUEST_ID,
  //     operator: 'eq',
  //     value: this.locRequestId.split('.')[1]
  //   });
  //   const filterParam = this.entityService.perpareFilterParametersone(LocalizationConstants.MPM_ENTITY_LOC_CONTENT, filters, '', this.utilService.APP_ID);
  //   this.entityService.getEntityObjects(LocalizationConstants.MPM_ENTITY_LOC_CONTENT, filterParam, null, null, null, null, this.utilService.APP_ID)
  //     .subscribe((localizationComponentResponse) => {
  //       if (localizationComponentResponse && localizationComponentResponse.result && localizationComponentResponse.result.items) {
  //         this.localizationComponentList = localizationComponentResponse.result.items;
  //       }
  //     }, (error) => {
  //       this.loaderService.hide();
  //     });
  // }
  loadLocalizationRequestFormGroups() {
    this.loaderService.show();
    const filters = [];
    filters.push({
      'name': EntityListConstant.REQUEST_ENTITY_PARAMS.ID,
      'operator': 'eq',
      'value': this.locRequestId.split('.')[1]
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.REQUEST, filters, '', this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.REQUEST, filterParam, null, null, null, null, this.utilService.APP_ID)
      .subscribe((localizationRequestResponse) => {
        if (localizationRequestResponse && localizationRequestResponse.result &&
          localizationRequestResponse.result.items && localizationRequestResponse.result.items.length === 1) {
          this.requestDetails = localizationRequestResponse.result.items[0];
          // this.localizationComponentDetails();
          this.fetchRequestHistoryLog();
        }
      }, error => {
        this.loaderService.hide();
      });
  }
  /**
   * To load the configs(dropdowns, autocomplete,..) required for the form
   * @param successCallback
   * @param errorCallback 
   */
  readUrlParams(callback) {
    this.activeRoute.queryParams.subscribe(queryParams => {
      if (this.activeRoute.parent.params && this.activeRoute.parent.params['value']
        && this.activeRoute.parent.params['value'].requestId) {
        this.activeRoute.parent.params.subscribe(parentRouteParams => {
          callback(parentRouteParams, queryParams);
        });
      } else {
        this.activeRoute.params.subscribe(routeParams => {
          callback(routeParams, queryParams);
        });
      }
    });
  }
  refresh() {
    if (this.locRequestId) {
      this.loadLocalizationRequestFormGroups();
    }
  }
  ngOnInit(): void {
    this.readUrlParams((routeParams, queryParams) => {
      if (queryParams && routeParams) {
        this.urlParams = queryParams;
        this.locRequestId = routeParams.requestId;
        this.menu = queryParams.menu;
        if (this.locRequestId) {
          this.loadLocalizationRequestFormGroups();
        }
      }
    });
  }
}
