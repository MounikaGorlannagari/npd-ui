import { Component, OnInit, Input, HostListener } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { LoaderService, NotificationService, UtilService, AppService, findObjectsByProp } from 'mpm-library';
import { ActivatedRoute } from '@angular/router';
import { TimelineConstants, TimelineState } from '../constants/TimelineConstants';
import { RouteService } from 'src/app/core/mpm/route.service';
import { EntityService } from '../../services/entity.service';
import { EntityListConstant } from '../constants/EntityListConstant';
import { BusinessConstants } from '../constants/BusinessConstants';
import { forkJoin } from 'rxjs';


@Component({
  selector: 'app-time-line-review',
  templateUrl: './time-line-review.component.html',
  styleUrls: ['./time-line-review.component.scss']
})
export class TimeLineReviewComponent implements OnInit {
  public windowWidth = window.innerWidth;
  public timeLinesCount = 5;
  public timeLineIterations = [];
  public selectedRequest;
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.windowWidth = window.innerWidth;
    if (this.windowWidth > 1500 && this.windowWidth <= 1600) {
      this.timeLinesCount = 5;
    } else if (this.windowWidth > 1600 && this.windowWidth <= 2500) {
      this.timeLinesCount = 6;
    } else if (this.windowWidth > 2500 && this.windowWidth <= 5000) {
      this.timeLinesCount = 7;
    } else if (this.windowWidth > 5000) {
      this.timeLinesCount = 8
    }
    this.setTimeLineChart();

  }
  peerReviewEventId = 12;
  requestId;
  urlParams;
  menu;
  isDraft;
  isNewRequest;
  canComplete;
  APP_ID;
  statusobj;
  PROCESS_ACTIVITIES_NS = 'http://schemas.cordys.com/pim/queryinstancedata/1.0';
  PROCESS_ACTIVITIES_METHOD_NAME = 'GetProcessActivities';
  processIndentifier;
  requestDetails;
  requestPhases = [];
  trackingAvailable = true;

  public requestStageTimeline = {};

  constructor(
    private adapter: DateAdapter<any>, private notificationService: NotificationService,
    private loaderService: LoaderService, private appService: AppService,
    private activeRoute: ActivatedRoute, private utilService: UtilService,
    private entityService: EntityService,
    private routerService: RouteService
  ) {
    this.selectedRequest = JSON.parse(sessionStorage.getItem('SELECTED_REQUEST_VIEW'))?.requestDetails;
    this.getUserDetails(this.selectedRequest?.requestorUserId);

  }

  backToDashboard() {
    this.routerService.goToRequestDashboard(this.menu, this.urlParams && this.urlParams.listDependentFilter ? this.urlParams.listDependentFilter : null);
  }

  // trackingDetails() {
  //   this.loaderService.show();

  //   const reqObject = {
  //     Query: {
  //       '@xmlns': 'http://schemas.cordys.com/cql/1.0',
  //       Select: {
  //         QueryableObject: 'PROCESS_ACTIVITY',
  //         Field: ['ACTIVITY_ID', 'INSTANCE_ID', 'ACTIVITY_TYPE', 'STATUS', 'ACTIVITY_NAME', 'START_TIME', 'END_TIME', 'EXECUTED_BY']
  //       },
  //       Filters: {
  //         And: {
  //           EQ: [{
  //             '@field': 'INSTANCE_ID',
  //             Value: this.processIndentifier
  //           },
  //           {
  //             '@field': 'REPLACED',
  //             Value: 0
  //           }]
  //         }
  //       },
  //       OrderBy: {
  //         Property: 'START_TIME'
  //       }
  //     }
  //   };

  //   this.appService.invokeRequest(this.PROCESS_ACTIVITIES_NS, this.PROCESS_ACTIVITIES_METHOD_NAME, reqObject).subscribe((response) => {
  //     this.loaderService.hide();
  //     const requests = findObjectsByProp(response, 'PROCESS_ACTIVITY');
  //     if (Array.isArray(requests)) {
  //       this.requestPhases = requests.filter(x => x.ACTIVITY_TYPE == 'TASK');
  //       // selectedRequest = selectedRequest && JSON.parse(selectedRequest)?.requestDetails;

  //       //filter for Request Rework
  //       // requests.forEach(request => {
  //       //   if (request.ACTIVITY_TYPE === 'TASK' && request.ACTIVITY_NAME === BusinessConstants.TASK_ACTIVITY_REQUEST_REWORK) {
  //       //     isRequestRework = true;
  //       //   }
  //       // });
  //       // phases = !isRequestRework ? TimelineConstants.requestPhases.filter(phases => phases.ACTIVITY_NAME != BusinessConstants.TASK_ACTIVITY_REQUEST_REWORK) : TimelineConstants.requestPhases;

  //       //remove BDA for non-commercial request
  //       // if (selectedRequest?.requestTypeValue?.toLowerCase() == 'commercial') {//this.urlParams && this.urlParams['requestType']
  //       //   this.requestPhases = phases;
  //       // } else {
  //       //   this.requestPhases = phases.filter(phases => !phases.IS_BDA)
  //       // }

  //       // for (const i in requests) {
  //       //   if (requests[i].ACTIVITY_TYPE === 'TASK') {
  //       //     this.requestPhases.forEach((requestPhase) => {
  //       //       if (requests[i].ACTIVITY_NAME == requestPhase.ACTIVITY_NAME) {
  //       //         requestPhase.ACTIVITY_NAME = requestPhase.ACTIVITY_NAME == 'PM Review' ? 'Program Manager Review' : requestPhase.ACTIVITY_NAME
  //       //         requestPhase.STATUS = requests[i].STATUS
  //       //         requestPhase.START_TIME = requests[i].START_TIME
  //       //         requestPhase.END_TIME = requests[i].END_TIME
  //       //         requestPhase.user = requests[i].EXECUTED_BY?.split(',')[0]?.split('=')[1]
  //       //       }
  //       //     })
  //       //   }
  //       // }
  //       let distinctUsers = [];
  //       this.requestPhases.forEach((requestPhase) => {

  //         if (requestPhase.ACTIVITY_NAME == requestPhase.ACTIVITY_NAME) {
  //           requestPhase.ACTIVITY_NAME = requestPhase.ACTIVITY_NAME == 'PM Review' ? 'Program Manager Review' : requestPhase.ACTIVITY_NAME == 'E2E Review' ? 'E2E PM Review' : requestPhase.ACTIVITY_NAME
  //           requestPhase.STATUS = requestPhase.STATUS
  //           requestPhase.START_TIME = requestPhase.START_TIME
  //           requestPhase.END_TIME = requestPhase.END_TIME
  //           if (typeof (requestPhase.EXECUTED_BY) == 'string') {
  //             requestPhase.user = requestPhase.EXECUTED_BY?.split(',')[0]?.split('=')[1];
  //             if (!distinctUsers.includes(requestPhase.user)) {
  //               distinctUsers.push(requestPhase.user);

  //             }

  //           } else {
  //             requestPhase.user = '';
  //           }
  //         }
  //       })

  //       if (!this.requestPhases.find(x => x.ACTIVITY_NAME == 'Program Manager Review')) {
  //         let obj = {
  //           "ACTIVITY_NAME": "Program Manager Review",//Program Manager Approval
  //           "ACTIVITY_TYPE": "TASK",
  //           "END_TIME": "",
  //           "START_TIME": "",
  //           "STATUS": "",
  //           "IS_BDA": false,
  //           "IS_DISPLAY": true,
  //         };
  //         this.requestPhases.push(obj);
  //       }
  //       if (!this.requestPhases.find(x => x.ACTIVITY_NAME == 'E2E PM Review')) {
  //         let obj = {
  //           "ACTIVITY_NAME": "E2E PM Review",
  //           "ACTIVITY_TYPE": "TASK",
  //           "END_TIME": "",
  //           "START_TIME": "",
  //           "STATUS": "",
  //           "IS_BDA": false,
  //           "IS_DISPLAY": true,
  //         };
  //         this.requestPhases.push(obj);
  //       }
  //       if (this.selectedRequest) {
  //         this.requestStageTimeline['user'] = this.selectedRequest.requestorName;
  //         this.requestStageTimeline['ACTIVITY_NAME'] = 'Request';
  //         this.requestStageTimeline['STATUS'] = 'COMPLETE';
  //         this.requestStageTimeline['START_TIME'] = new Date(this.requestDetails?.Tracking?.CreatedDate);
  //         this.requestStageTimeline['END_TIME'] = new Date(this.requestPhases[0]?.START_TIME);
  //         this.requestStageTimeline['INSTANCE_ID'] = 'requestUser'
  //         if (!distinctUsers.includes(this.selectedRequest.requestorName)) {
  //           distinctUsers.push(this.selectedRequest.requestorName);
  //         }
  //       }
  //       this.requestPhases.unshift(this.requestStageTimeline);
  //       this.onResize();
  //     }
  //     else {
  //       this.trackingAvailable = false;
  //     }
  //   }, () => {
  //     this.trackingAvailable = false;
  //     this.notificationService.error("Unable to load request timeline.")
  //     // console.log('Error:');
  //   });
  // }


  filterTask() {
    this.requestPhases.filter
  }

  getRequestDetails() {
    this.loaderService.show();
    const filters = [];
    filters.push({
      'name': EntityListConstant.REQUEST_ENTITY_PARAMS.ID,
      'operator': 'eq',
      'value': this.requestId.split('.')[1]
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.REQUEST, filters, '', this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.REQUEST, filterParam, null, null, null, null, this.utilService.APP_ID)
      .subscribe((requestResponse) => {
        if (requestResponse && requestResponse.result &&
          requestResponse.result.items && requestResponse.result.items.length === 1) {
          this.requestDetails = requestResponse.result.items[0];
          this.processIndentifier = requestResponse.result.items[0].Properties?.ProcessIdentifier;

          // this.requestTimeline['START_TIME']=new Date(requestResponse.result.items[0].Tracking.CreatedDate);
          // this.requestTimeline['ACTIVITY_NAME']='Request';
          //Lifecycle
          //this.processIndentifier = requestResponse.result.items[0].Lifecycle?.InstanceIdentifier
          // this.trackingDetails();
          if (this.processIndentifier) {
            // this.trackingDetails();
            this.getProcessActivities();
          }
          else {
            this.trackingAvailable = false;
            this.loaderService.hide();
            this.notificationService.error("Unable to load request timeline.")
          }
        }
      }, error => {
        this.trackingAvailable = false;
        this.loaderService.hide();
        this.notificationService.error("Unable to load request timeline.")
      });
  }

  readUrlParams(callback) {
    this.activeRoute.queryParams.subscribe(queryParams => {
      if (this.activeRoute.parent.params && this.activeRoute.parent.params['value']
        && this.activeRoute.parent.params['value'].requestId) {
        this.activeRoute.parent.params.subscribe(parentRouteParams => {
          callback(parentRouteParams, queryParams);
        });
      } else {
        this.activeRoute.params.subscribe(routeParams => {
          callback(routeParams, queryParams);
        });
      }
    });
  }

  ngOnInit() {
    this.APP_ID = this.utilService.APP_ID;
    this.loaderService.show();
    this.readUrlParams((routeParams, queryParams) => {
      if (queryParams && routeParams) {
        this.requestId = routeParams.requestId;
        this.urlParams = queryParams;
        this.menu = this.urlParams.menu;
        if (this.urlParams && this.requestId) {
          this.getRequestDetails();
          // this.getProcessActivities();
        }
        else {
          this.trackingAvailable = false;
          this.notificationService.error("Request id not found.")
        }
      }
    });

    if (navigator.language !== undefined) {
      this.adapter.setLocale(navigator.language);
    }
    this.loaderService.hide();
  }

  public reuestedUserDisplayName = '';
  public getUserDetails(user) {

    const reqObject = {
      'userCN': user
    }
    this.loaderService.show();
    this.appService.invokeRequest('http://schemas/MPMCustomizedApplicationPackages/Person/operations', 'GetPersonByUserId', reqObject).subscribe((response: any) => {
      this.reuestedUserDisplayName = response?.Person?.DisplayName?.__text;

      this.loaderService.hide()
    }, err => {
      this.loaderService.hide();
    })

  }


  public setTimeLineChart() {
    this.timeLineIterations = [];
    let reqphases = JSON.parse(JSON.stringify(this.requestPhases));
    let timeLineIterations = Math.ceil(this.requestPhases.length / this.timeLinesCount);
    for (let i = 0; i < timeLineIterations; i++) {
      this.timeLineIterations.push(reqphases.splice(0, this.timeLinesCount));
    }
  }


  public getProcessActivities() {
    this.loaderService.show();
    const filters = [];
    filters.push({
      'name': EntityListConstant.REQUEST_ENTITY_PARAMS.REQUEST_ID,
      'operator': 'eq',
      'value': this.selectedRequest.requestId
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.INBOX_ALL_TASK, filters, null, this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.INBOX_ALL_TASK, filterParam, null, null, null, null, this.utilService.APP_ID)
      .subscribe((res) => {
        this.loaderService.hide();
        
        let resultItems = res.result.items.map(x => { return x.Task });
        this.setTimeLineChartData(resultItems);


      }, err => {
        this.loaderService.hide();
      });
  }
  public refresh() {
    this.getProcessActivities();
    // this.trackingDetails();

  }


  public setTimeLineChartData(requests: Array<any>) {
    let resultItems=requests.sort((a,b)=>{
      return Date.parse(a.StartDate) - Date.parse(b.StartDate);
    })



    this.requestPhases = [];


    resultItems.forEach((requestPhase) => {
      let obj = {};

      obj['ACTIVITY_NAME'] = requestPhase.Subject == 'PM Review' ? 'Program Manager Review' : requestPhase.Subject == 'E2E Review' ? 'E2E PM Review' : requestPhase.Subject;
      obj['STATUS'] = TimelineState[requestPhase.State];
      obj['START_TIME'] = requestPhase?.StartDate;
      obj['END_TIME'] = requestPhase?.CompletionDate;
      obj['TaskOwnerName'] = requestPhase?.TaskOwnerName;

      this.requestPhases.push(obj);

    })
    if (!this.requestPhases.find(x => x.ACTIVITY_NAME == 'Program Manager Review')) {
      let obj = {
        "ACTIVITY_NAME": "Program Manager Review",//Program Manager Approval
        "ACTIVITY_TYPE": "TASK",
        "END_TIME": "",
        "START_TIME": "",
        "STATUS": "",
        "IS_BDA": false,
        "IS_DISPLAY": true,
        "TaskOwnerName": ''
      };
      this.requestPhases.push(obj);
    }
    if (!this.requestPhases.find(x => x.ACTIVITY_NAME == 'E2E PM Review')) {
      let obj = {
        "ACTIVITY_NAME": "E2E PM Review",
        "ACTIVITY_TYPE": "TASK",
        "END_TIME": "",
        "START_TIME": "",
        "STATUS": "",
        "IS_BDA": false,
        "IS_DISPLAY": true,
        "TaskOwnerName": ''
      };
      this.requestPhases.push(obj);
    }
    if (this.selectedRequest) {
      this.requestStageTimeline['ACTIVITY_NAME'] = 'Request';
      this.requestStageTimeline['STATUS'] = 'COMPLETE';
      this.requestStageTimeline['START_TIME'] = new Date(this.requestDetails?.Tracking?.CreatedDate);
      this.requestStageTimeline['END_TIME'] = new Date(this.requestPhases[0]?.START_TIME);
      this.requestStageTimeline['INSTANCE_ID'] = 'requestUser'
    }
    this.requestPhases.unshift(this.requestStageTimeline);
    this.onResize();
  }
}
