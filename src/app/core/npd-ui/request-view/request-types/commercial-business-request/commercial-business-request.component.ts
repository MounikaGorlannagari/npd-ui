import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ConfirmationModalComponent, LoaderService, NotificationService, SharingService, UtilService } from 'mpm-library';
import { forkJoin, Observable } from 'rxjs';
import { RouteService } from 'src/app/core/mpm/route.service';
import { RequestDashboardControlConstant } from '../../../request-management/constants/request-dashboard-control-constants';
import { InboxService } from '../../../request-management/requestor-dashboard/inbox/inbox.service';
import { EntityService } from '../../../services/entity.service';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { RequestService } from '../../../services/request.service';
import { BusinessConstants } from '../../constants/BusinessConstants';
import { EntityListConstant } from '../../constants/EntityListConstant';
import { RequestConfig } from '../../constants/RequestConfig';
import { DeliverablesVsThresholdsComponent } from '../../generic-components/deliverables-vs-thresholds/deliverables-vs-thresholds.component';
import { GainApprovalScopeComponent } from '../../generic-components/gain-approval-scope/gain-approval-scope.component';
import { GovernanceMilestonesComponent } from '../../generic-components/governance-milestone/governance-milestones.component';
import { MarketScopeComponent } from '../../generic-components/market-scope/market-scope.component';
import { PortfolioStrategyComponent } from '../../generic-components/portfolio-strategy/portfolio-strategy.component';
import { ProjectCoreDataComponent } from '../../generic-components/project-core-data/project-core-data.component';
import { ProjectNameHeaderComponent } from '../../generic-components/project-name-header/project-name-header.component';
import { TaskOperationsComponent } from '../../generic-components/task-operations/task-operations.component';
import { MonsterRequestService } from '../../services/monster-request.service';
import * as acronui from 'mpm-library';
import { ProjectClassificationComponent } from '../../generic-components/project-classification/project-classification.component';
import { ManagerFieldConfigComponent } from '../../generic-components/manager-field-config/manager-field-config.component';
import { CommercialRequestService } from '../../services/commercial-request.service';
import { MonsterConfirmationModalComponent } from '../../../monster-shared/monster-confirmation-modal/monster-confirmation-modal.component';
import { GenericFormComponent } from '../../generic-components/generic-form/generic-form.component';
import { ProgrammeManagerReportingComponent } from '../../generic-components/programme-manager-reporting/programme-manager-reporting.component';
import { CommentConfig } from '../../../collab/objects/CommentConfig';
import { NewCommentModal } from '../../../collab/objects/comment.modal';
import { CommentsService } from '../../../collab/services/comments.service';
import { TechnicalRequestService } from '../../services/technical-request.service';
import { RequestsCopyConstants } from '../../constants/RequestsCopyConstants';
import { MONSTER_ROLES } from '../../../filter-configs/role.config';

@Component({
  selector: 'app-commercial-business-request',
  templateUrl: './commercial-business-request.component.html',
  styleUrls: ['./commercial-business-request.component.scss']
})
export class CommercialBusinessRequestComponent implements OnInit {
  selectedMarket: string;
  selectedBusinessUnit;
  requestId: any;
  urlParams: any;
  menu: any;
  isNewRequest: boolean;
  taskId: any;
  isDraft: boolean;
  isCopyRequest: boolean = false
  requestDetails: any
  pcopiedRequestDetails: any
  copiedMarketScopeDetails: any
  copiedDeliverablesDetails: any
  canComplete: boolean;
  commercialRequestItemId: string;
  requestItemId: string;
  requestForm: any;
  dynamicMarketScopeForm: any;
  formBuilder: any;
  msGridColumns: string[];
  showMarketScopeForm: boolean;
  businessUnitInitialCount = 0;
  marketScopeData: any;
  taskStatus: any;
  deliverableThresholdData: any;
  showManagers;
  marketProperties = {
    requestMarketScopeRelation: 'COMMERCIAL_REQUEST_MARKET_SCOPE_RELATION',
    requestType: 'commercial',
    dynamicMarketScopeFormControlNames: 'DYNAMIC_COMMERCIAL_MARKET_SCOPE_FORM',
    R_PO_MARKET: 'R_PO_MARKETS',
    marketScope: 'COMMERCIAL_MARKET_SCOPE'
  }
  deliverablesProperties = {
    requestDeliverablesRelation: 'COMMERCIAL_REQUEST_DELIVERABLE_THRESHOLD',
  }
  projectName;

  @ViewChild('programmeManagerReportingComponent') programmeManagerReportingComponent: ProgrammeManagerReportingComponent;
  @ViewChild('marketScopeComponent') marketScopeComponent: MarketScopeComponent;
  @ViewChild('projectCoreComponent') projectCoreComponent: ProjectCoreDataComponent;
  @ViewChild('gainApprovalScopeComponent') gainApprovalScopeComponent: GainApprovalScopeComponent;
  @ViewChild('portfolioStrategyComponent') portfolioStrategyComponent: PortfolioStrategyComponent;
  @ViewChild('projectNameHeaderComponent') projectNameHeaderComponent: ProjectNameHeaderComponent;
  @ViewChild('governancemilestone') governancemilestone: GovernanceMilestonesComponent;
  @ViewChild('deliverablesVsThresholdsComponent') deliverablesVsThresholdsComponent: DeliverablesVsThresholdsComponent
  @ViewChild('projectClassificationComponent') projectClassificationComponent: ProjectClassificationComponent;
  @ViewChild('taskOperations') taskOperations: TaskOperationsComponent;
  @ViewChild('managerFieldComponent') managerFieldComponent: ManagerFieldConfigComponent;
  @ViewChild('genericFormComponent') genericFormComponent: GenericFormComponent;
  showInboxControls = false;

  allListConfig = {
    markets: [],
    marketsCopy: [],
    businessUnits: [],
    brands: [],
    projectTypes: [],
    primaryPackagingTypes: [],
    commercialCategories: [],
    governanceApproaches: [],
    casePackSizes: [],
    consumerUnitSizes: [],
    //productionSites: [],
    bottlers: [],
    skuDetails: [],
    platforms: [],
    variants: [],
    e2ePMUsers: [],
    projectClassifications: [],
    reportingProjectType: [],
  }
  requestObject = {
    requestId: null,
    status: '',
    requestorName: '',
    requestorUserId: '',
    currentCheckpoint: '',
    projectName: '',
    requestItemId: '',
    requestType: ''
  }

  stageDecisions = BusinessConstants.stageDecisions;
  showCommercialRequestForm = false;
  panelHTML: HTMLElement;
  commercialReq: any;
  isPM: boolean;
  currentActivity: string;
  isE2EPM: boolean;
  isReqRework: boolean;
  isBDA: boolean;
  isRequestView: boolean;
  taskDetails: any;
  taskOwner: any;
  requestFieldHandler: RequestConfig;
  isAnyActionPossible: any;
  @Input() requestIdInput;
  isAdmin: boolean;


  constructor(private loaderService: LoaderService, private monsterConfigApiService: MonsterConfigApiService,
    private monsterUtilService: MonsterUtilService, private entityService: EntityService,
    private activeRoute: ActivatedRoute, private utilService: UtilService, private notificationService: NotificationService,
    private monsterRequestService: MonsterRequestService, private commercialRequestService: CommercialRequestService, private sharingService: SharingService, private routerService: RouteService,
    private dialog: MatDialog, private inboxService: InboxService, private requestService: RequestService, private commentsService: CommentsService,
    private technicalRequestService: TechnicalRequestService) { }

  /**
* To deep copy the objects
* */
  copyObj(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  getMarkets(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllMarkets().subscribe((response) => {
        this.allListConfig.markets = this.monsterConfigApiService.transformResponse(response, "Markets-id", true, false, true);
        this.allListConfig.marketsCopy = this.allListConfig.markets.length > 0 ? this.copyObj(this.allListConfig.markets) : [];
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getBusinessUnits(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllBusinessUnits().subscribe((response) => {
        this.allListConfig.businessUnits = this.monsterConfigApiService.transformResponse(response, "Business_Unit-id", false);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getBrands(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllBrands().subscribe((response) => {
        this.allListConfig.brands = this.monsterConfigApiService.transformResponse(response, "Brands-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getReportingProjectType() {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllReportingProjectType().subscribe((response) => {
        this.allListConfig.reportingProjectType = this.monsterConfigApiService.transformResponse(response, "CP_Project_Type-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getProjectTypes(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllProjectTypes().subscribe((response) => {
        this.allListConfig.projectTypes = this.monsterConfigApiService.transformResponse(response, "Project_Type-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getPackagingTypes(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllPackagingTypes().subscribe((response) => {
        this.allListConfig.primaryPackagingTypes = this.monsterConfigApiService.transformResponse(response, "Packaging_Type-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getCommercialCategorization(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllCommercialCategorization().subscribe((response) => {
        this.allListConfig.commercialCategories = this.monsterConfigApiService.transformResponse(response, "Commercial_Categorisation-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getGovernanceApproaches(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllGovernanceApproaches().subscribe((response) => {
        this.allListConfig.governanceApproaches = this.monsterConfigApiService.transformResponse(response, "Governance_Approach-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getCasePackSizes(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllCasePackSizes().subscribe((response) => {
        this.allListConfig.casePackSizes = this.monsterConfigApiService.transformResponse(response, "Case_Pack_Size-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getConsumerUnits(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllConsumerUnits().subscribe((response) => {
        this.allListConfig.consumerUnitSizes = this.monsterConfigApiService.transformResponse(response, "Consumer_Unit_Size-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  /*   getProductionSites(): Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllProductionSites().subscribe((response) => {
          this.allListConfig.productionSites = this.monsterConfigApiService.transformResponse(response,"Production_Site-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    } */

  getBottlers(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllBottlers().subscribe((response) => {
        this.allListConfig.bottlers = this.monsterConfigApiService.transformResponse(response, "Bottler-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getSKUDetails(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllSKUDetails().subscribe((response) => {
        this.allListConfig.skuDetails = this.monsterConfigApiService.transformResponse(response, "Sku_Details-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getPlatformsByBrand(parameters): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchPlatformsByBrand(parameters).subscribe(response => {
        this.allListConfig.platforms = this.monsterConfigApiService.transformResponse(response, 'Platforms-id', true, true);
        observer.next(true);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
  }

  getVariantsByPlatform(parameters): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchVariantsByPlatform(parameters).subscribe(response => {
        this.allListConfig.variants = this.monsterConfigApiService.transformResponse(response, 'Variant_SKU-id', true, true);
        observer.next(true);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
  }

  getE2EPMUsers(): Observable<any> {
    return new Observable(observer => {
      this.requestService.getUsersByRole().subscribe(response => {
        this.allListConfig.e2ePMUsers = response;
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
        this.notificationService.error('Unable to get all the E2E PM.');
      });
    });
  }

  /**
* To create the new commercial request
* @author Sindhuja
* @param successCallback
* @param errorCallback
*/
  createNewCommercialRequestData(successCallback?, errorCallback?) {
    this.loaderService.show()
    this.commercialRequestItemId = '';
    this.requestItemId = '';
    let requestTypeId;
    let status;
    this.loaderService.show();
    for (var i = 0; i < RequestDashboardControlConstant.newRequestType.options.length; i++) {
      if (RequestDashboardControlConstant.newRequestType.options[i].value == BusinessConstants.COMMERCIAL_REQUEST) {
        requestTypeId = RequestDashboardControlConstant.newRequestType.options[i].itemId; //read it from URL params or send itemid from URL params
        break;
      }
    }
    status = this.monsterUtilService.getStatusDisplayName(BusinessConstants.REQUEST_STATUS_DRAFT);
    let createData = {
      Properties: {
        Request_Status: status && status.VALUE,
      },
      R_PO_REQUEST_TYPE: {
        ItemId: requestTypeId
      }
    };

    this.entityService.createEntitywithOutParent(EntityListConstant.REQUEST, createData, this.utilService.APP_ID).subscribe((response) => {
      this.commercialRequestItemId = response.package.itemId;
      this.requestId = response.package.itemId;
      // if(this.isCopyRequest){
      //   this.copyRequestData();
      // }
      // else{
      //   this.retrieveRequest();
      // }
      this.retrieveRequest();
      this.loaderService.hide();
      if (successCallback) { successCallback(); }
    }, (error) => {
      this.loaderService.hide();
      if (errorCallback) { errorCallback(); }
      console.log('Error while creating a new commercial request.');

    });
  }

  /**
 * To load commercial request and set form group
 */
  retrieveRequest() {
    // to get request details
    this.loaderService.show();
    const filters = [];
    filters.push({
      'name': EntityListConstant.REQUEST_ENTITY_PARAMS.ID,
      'operator': 'eq',
      'value': this.requestId.split('.')[1]
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.REQUEST, filters, '', this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.REQUEST, filterParam, null, null, null, null, this.utilService.APP_ID)
      .subscribe((requestResponse) => {
        if (requestResponse && requestResponse.result &&
          requestResponse.result.items && requestResponse.result.items.length === 1) {
          const techReq = requestResponse.result.items[0];
          this.requestItemId = techReq.Identity.ItemId;
          this.requestObject = this.monsterRequestService.mapRequestToForm(techReq, BusinessConstants.COMMERCIAL_REQUEST_HEADER_NAME);
          if (this.isCopyRequest) {
            this.copyRequestData();
          }

          this.loadFieldConfigurations();
        }
        this.loaderService.hide();
      }, (error) => {
        this.loaderService.hide();
      });
  }

  copyCommercialRequestFormGroups() {
    this.isCopyRequest = true;
    this.createNewCommercialRequestData(() => {
      this.loadFieldConfigurations();
    }, () => {

    });
  }

  copyRequestData() {
    this.loaderService.show();
    const filters2 = [];
    filters2.push({
      'name': EntityListConstant.REQUEST_ENTITY_PARAMS.ID,
      'operator': 'eq',
      'value': this.requestDetails.split('.')[1]
    });
    const filterParam2 = this.entityService.perpareFilterParametersone(EntityListConstant.REQUEST, filters2, '', this.utilService.APP_ID);

    this.entityService.getEntityObjects(EntityListConstant.REQUEST, filterParam2, null, null, null, null, this.utilService.APP_ID)
      .subscribe((requestResponse) => {
        this.pcopiedRequestDetails = requestResponse
      })
    this.entityService.getEntityItemDetails(this.requestDetails,
      EntityListConstant.COMMERCIAL_REQUEST_MARKET_SCOPE_RELATION).subscribe(res => {
        this.copiedMarketScopeDetails = res;

        this.marketScopeComponent?.copyCommercialRequest(this.copiedMarketScopeDetails);
      })

    this.entityService.getEntityItemDetails(this.requestDetails,
      EntityListConstant.COMMERCIAL_REQUEST_DELIVERABLE_THRESHOLD).subscribe((response) => {
        this.copiedDeliverablesDetails = response
        this.deliverablesVsThresholdsComponent?.copyDeliverablesVSThreshold(this.copiedDeliverablesDetails)
      })
    this.loaderService.hide();

  }

  loadFieldConfigurations(isConfigLoad?) {
    this.isPM = (this.currentActivity === BusinessConstants.TASK_ACTIVITY_PM) ? true : false;
    this.isE2EPM = (this.currentActivity === BusinessConstants.TASK_ACTIVITY_E2EPM) ? true : false;
    this.isReqRework = (this.currentActivity === BusinessConstants.TASK_ACTIVITY_REQUEST_REWORK) ? true : false;
    this.isBDA = (this.currentActivity === BusinessConstants.TASK_ACTIVITY_BDA) ? true : false;
    this.isRequestView = !this.taskId && !(this.isPM || this.isE2EPM || this.isReqRework);
    const delegatedUser = (this.taskDetails && this.taskDetails[0] && this.taskDetails[0].Task &&
      this.taskDetails[0].Task.DelegatedToUser && this.taskDetails[0].Task.DelegatedToUser.__text) ?
      this.taskDetails[0].Task.DelegatedToUser.__text : null;
    const isEditable = (this.taskOwner === this.sharingService.getCurrentUserDN() || this.sharingService.getCurrentUserDN() === delegatedUser) || this.isAdmin;
    this.canComplete = (this.canComplete && isEditable) ? true : false;

    if (!isConfigLoad) {
      this.requestFieldHandler.projectCoreData = this.commercialRequestService.mapProjectCoreDataConfigurations(this.commercialReq,
        this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA, this.isRequestView);
      this.requestFieldHandler.marketScope = this.commercialRequestService.mapMarketScopeConfigurations(this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA, this.isRequestView)
      this.requestFieldHandler.deliverablesThreshold = this.commercialRequestService.mapDeliverableThresholdConfigurations(this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA, this.isRequestView)
      this.requestFieldHandler.portfolioStrategy = this.commercialRequestService.mapPortfolioStrategyConfigurations(this.commercialReq,
        this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA, this.isRequestView);
      this.requestFieldHandler.gainApprovalScope = this.commercialRequestService.mapGainApprovalScopeConfigurations(this.commercialReq,
        this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA);
      this.requestFieldHandler.governanceMilestone = this.commercialRequestService.mapGovernanceMilestoneConfigurations(this.commercialReq,
        this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA, this.isRequestView);
      this.requestFieldHandler.actionButtons = this.commercialRequestService.mapActionButtonsConfigurations(
        this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA, this.isAdmin);
      this.requestFieldHandler.projectClassificationData = this.commercialRequestService.mapProjectClassificationConfigurations
        (this.commercialReq, this.isDraft, this.canComplete, this.isE2EPM, this.isRequestView, this.isPM, this.isReqRework, this.isBDA, this.isAdmin)
      this.requestFieldHandler.other = this.commercialRequestService.mapOtherConfig(this.commercialReq, this.isDraft, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA, this.isRequestView, this.canComplete);
      this.requestFieldHandler.managerConfig = this.commercialRequestService.mapManagerConfig(this.commercialReq,
        this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA, this.isRequestView);
      this.requestFieldHandler.businessCaseCommentsConfig = this.commercialRequestService.mapBusinessCaseCommentsConfig(this.commercialReq,
        this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA, this.isRequestView);

      this.requestFieldHandler.programmeManagerReporting = this.commercialRequestService.mapprogrammeManagerReportingConfigurations(this.commercialReq, this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isRequestView);
    }
    else {
      this.requestFieldHandler = this.commercialRequestService.mapDisabilityConfigByTaskCriteria(this.requestFieldHandler, this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA);
      this.requestFieldHandler.actionButtons = this.commercialRequestService.mapActionButtonsConfigurations(
        this.isDraft, this.canComplete, this.isPM, this.isE2EPM, this.isReqRework, this.isBDA);
    }
    // this.ngOnInit();
    this.showCommercialRequestForm = true;
  }


  /**
  * To retrieve task details
  */
  getTaskDetails(isConfigLoad?) {
    this.loaderService.show();
    this.inboxService.getTaskDetails(this.taskId).subscribe((response) => {
      this.taskDetails = response;
      this.currentActivity = this.taskDetails[0].Task.Activity;
      this.taskOwner = this.taskDetails[0].Task.Assignee.__text;
      this.taskStatus = this.taskDetails[0].Task.State;
      this.canComplete = this.taskDetails[0].Task.PossibleActions.hasOwnProperty('COMPLETE');
      this.getTaskInfo(isConfigLoad);
      if (!isConfigLoad) {
        this.loadCommercialRequestFormGroups();
      } else {
        this.loadFieldConfigurations(isConfigLoad);
        this.updateFormDisability();
      }
      //this.loaderService.hide();
    }, (error) => {
      this.loaderService.hide();
    });
  }
  /**
   * To load form groups, if a user is opening the existing  request record
   * @param null
   */
  loadCommercialRequestFormGroups() {
    this.loaderService.show();
    const filters = [];
    filters.push({
      'name': EntityListConstant.REQUEST_ENTITY_PARAMS.ID,
      'operator': 'eq',
      'value': this.requestId.split('.')[1]
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.REQUEST, filters, '', this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.REQUEST, filterParam, null, null, null, null, this.utilService.APP_ID)
      .subscribe((commercialRequestResponse) => {
        if (commercialRequestResponse && commercialRequestResponse.result &&
          commercialRequestResponse.result.items && commercialRequestResponse.result.items.length === 1) {
          const techReq = commercialRequestResponse.result.items[0];
          this.commercialReq = techReq;
          this.requestItemId = techReq.Identity.ItemId;

          this.requestObject = this.monsterRequestService.mapRequestToForm(techReq, BusinessConstants.COMMERCIAL_REQUEST_HEADER_NAME,
            BusinessConstants.COMMERCIAL_REQUEST, this.currentActivity);
          const data = { isAnyActionPossible: this.isAnyActionPossible, requestDetails: this.requestObject, goBackMenu: this.menu };
          this.monsterRequestService.emitChange(data);
          sessionStorage.setItem('SELECTED_REQUEST_VIEW', JSON.stringify(data));

          let status = this.monsterUtilService.getStatusDisplayName(BusinessConstants.REQUEST_STATUS_DRAFT);
          this.isDraft = (this.requestObject.status === (status && status.VALUE)) ? true : false;
          this.isDraft = this.isDraft && this.requestObject.requestorUserId.indexOf(this.sharingService.getCurrentUserCN()) >= 0;//check if the same requestor has opened the request
          this.invokeServicesByTaskType();
          this.loadFieldConfigurations();
          this.marketScopeData = true;
          this.deliverableThresholdData = true;
        } else {
          this.notificationService.error('No Commercial Request found.')
        }
        this.loaderService.hide();
      }, error => {
        this.loaderService.hide();
      });
  }



  invokeServicesByTaskType() {
    this.loaderService.show();
    forkJoin([this.getE2EPMUsers()]).subscribe(response => {
      this.showManagers = true;
      this.loaderService.hide();
    });
  }

  storePanelHTML(event) {
    this.panelHTML = event.srcElement || event.target;
  }

  handlePanelLeaveEvent(event) {
    if (this.monsterUtilService.checkElementEventPosition(this.panelHTML, event) &&
      (this.showCommercialRequestForm) && (this.requestFieldHandler?.other?.autoSave?.value)) {

      const [isMarketScopeAdded, isLeadMarketSelected] = this.getMarketScopeValidation();
      if (isMarketScopeAdded && isLeadMarketSelected) {
        this.updateSavedData();// Save as Draft, only if atleast one market is added
      } else {
        if (isMarketScopeAdded) {
          if (!isLeadMarketSelected) {
            console.log("Not saving the request as Draft, Since no Lead Maket is selected.")
          }
        } else {
          console.log("Not saving the request as Draft, Since no market scope is added.")
        }
      }
    }
  }

  getMarketScopeValidation() {
    let isLeadMarketSelected = false;
    let isMarketScopeAdded = false;

    //checing count of markets
    if (this.marketScopeComponent.dynamicMarketScopeForm.value.marketScope.length != 0) {
      isMarketScopeAdded = true;
    }
    //add validation for lead market region
    for (var i = 0; i < this.marketScopeComponent.dynamicMarketScopeForm.value.marketScope.length; i++) {
      if ((this.marketScopeComponent.dataSource[i].leadMarket)) {
        isLeadMarketSelected = true;
      }
    }
    return [isMarketScopeAdded, isLeadMarketSelected];
  }
  updateSavedData() {
    const params = {
      listDependentFilter: null,
      menu: this.menu
    }
    this.monsterUtilService.updateFormProperty(true, (isUpdate?) => {
      if (isUpdate) {
        if (this.isNewRequest) {
          this.routerService.goToRequestDetailView(this.requestObject.requestItemId, params, this.requestObject.requestType.split(' ')[0].toLowerCase());
        }
        this.notificationService.success('Form changes are saved successfully.');
        this.entityService.getEntityObjectsByfilter(this.requestObject.requestItemId).subscribe(response => {
          this.requestObject.requestId = response?.item?.Properties?.RequestID;
        });
      }
    }, () => {
      //error callback
    });

  }

  readUrlParams(callback) {
    this.activeRoute.queryParams.subscribe(queryParams => {
      if (this.activeRoute.parent.params && this.activeRoute.parent.params['value']
        && this.activeRoute.parent.params['value'].requestId) {
        this.activeRoute.parent.params.subscribe(parentRouteParams => {
          callback(parentRouteParams, queryParams);
        });
      } else {
        this.activeRoute.params.subscribe(routeParams => {
          callback(routeParams, queryParams);
        });
      }
    });
  }

  getAllRequestTypes(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllRequestTypes().subscribe((response) => {
        RequestDashboardControlConstant.newRequestType.options = response;
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  getIsCopied() {
    this.technicalRequestService.getCopyCondition().subscribe((copyStatus) => {
      this.isCopyRequest = copyStatus
      return this.isCopyRequest;
    })
  }

  ngOnInit(): void {
    this.loaderService.show();
    this.monsterConfigApiService.getAllRolesForCurrentUser().subscribe(response => {
      const admin = response.find(eachRole => eachRole['@id'].substring(3, eachRole['@id'].indexOf(',')) === MONSTER_ROLES.NPD_ADMIN);
      this.isAdmin = admin != null ? true : false;
      forkJoin([this.getMarkets(), this.getBusinessUnits(), this.getBrands(), this.getPackagingTypes(), this.getProjectTypes()
        , this.getCasePackSizes(), this.getConsumerUnits(), this.getBottlers(), this.getSKUDetails(), this.getCommercialCategorization(), this.getGovernanceApproaches(), this.getReportingProjectType(), this.getAllRequestTypes()])
        .subscribe(responseList => {//this.getProductionSites()
          this.getIsCopied()
          this.readUrlParams((routeParams, queryParams) => {
            this.initialiseRequestConfig();
            if (this.requestIdInput) {
              this.requestId = this.requestIdInput;
              this.isNewRequest = false;
              this.canComplete = false;
              this.loadCommercialRequestFormGroups();
              this.menu = "allRequest";
            } else if (queryParams && routeParams) {
              this.requestId = routeParams.requestId;
              this.urlParams = queryParams;
              this.menu = this.urlParams.menu;
              if (this.urlParams && this.requestId) {
                this.isNewRequest = false;
                if (this.urlParams && this.urlParams.taskId) {
                  this.taskId = this.urlParams.taskId;
                  this.isDraft = false;
                  this.getTaskDetails();
                } else {
                  this.canComplete = false;
                  this.loadCommercialRequestFormGroups();
                }
              } else if (this.isCopyRequest) {
                this.initializeCopy()
              }
              else {
                this.isNewRequest = true;
                this.canComplete = false;
                this.isDraft = true;
                this.createNewCommercialRequestData();
              }
            }
          });
        }, () => {
          console.log('Error while loading the form configs.')
        });
      //check data code required here or not?
    },
      error => {
        this.notificationService.error('Cannot Get Roles for Current User');
      })
  }
  ngAfterViewInit(): void {

  }
  updateBusinessUnit(selectedBusinessUnit) {
    this.projectCoreComponent.updateBusinessUnit(selectedBusinessUnit);
  }

  updateRequestRelationsInfo(formControl, formName, index?) {
    let relationName;
    let objectValue;
    let itemId;

    if (formName === 'requestForm') {
      relationName = BusinessConstants.REQUEST_FORM[formControl];
      objectValue = this.requestForm.controls[formControl] && this.requestForm.controls[formControl].value;
      itemId = this.requestItemId;

      if (formControl === 'businessUnit') {
        objectValue = this.selectedBusinessUnit && this.selectedBusinessUnit.ItemId
      }
    }

    this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
  }

  deleteMarketScopeGroup() {
    this.projectCoreComponent.projectRequestorForm.patchValue({ businessUnit: '' });
  }

  updateProjectName() {
    //update value through service call,notify
    this.projectName = null;
    let projectNameValues: string[];
    projectNameValues = [null, null, null];
    let name = this.commercialReq?.Properties?.Project_Name;
    this.selectedMarket = this.selectedMarket ? this.selectedMarket : name?.split('-')[0]
    const market = this.selectedMarket || name?.split('-')[0];
    let platform = this.commercialReq?.R_PO_PLATFORMS$Properties?.DisplayName ? this.commercialReq.R_PO_PLATFORMS$Properties.DisplayName : null;
    let variant = this.commercialReq?.R_PO_VARIANT_SKU$Properties?.DisplayName ? this.commercialReq.R_PO_VARIANT_SKU$Properties.DisplayName : null;
    let brand = this.projectCoreComponent.selectedBrand
    if (brand == true) {
      projectNameValues[1] = null
      projectNameValues[2] = null
      variant = null
      platform = null
    }
    projectNameValues[0] = this.selectedMarket && this.selectedMarket.trim() ? this.selectedMarket : null;
    projectNameValues[1] = this.projectCoreComponent.selectedPlatformName && this.projectCoreComponent.selectedPlatformName.trim() ? this.projectCoreComponent.selectedPlatformName : null;
    projectNameValues[2] = this.projectCoreComponent.selectedVariantName && this.projectCoreComponent.selectedVariantName.trim() ? this.projectCoreComponent.selectedVariantName : null;

    if ((projectNameValues[0] && projectNameValues[0].trim()) || market) {
      this.projectName = projectNameValues[0] || market;
    }
    if ((projectNameValues[1] && projectNameValues[1].trim())) {
      if (this.projectName) {
        this.projectName += "-" + (projectNameValues[1]);
        variant = null
      }
      else {
        this.projectName = projectNameValues[1];
        variant = null
      }
    }
    else if (platform != null) {
      if (this.projectName)
        this.projectName += "-" + (platform);
      else
        this.projectName = platform;
    }
    if ((projectNameValues[2] && projectNameValues[2].trim())) {
      if (this.projectName)
        this.projectName += "-" + (projectNameValues[2]);
      else
        this.projectName = projectNameValues[2];
    } else if (variant != null) {
      if (this.projectName)
        this.projectName += "-" + (variant);
      else
        this.projectName = variant;
    }

    this.requestObject.projectName = this.projectName;//this.editProjectName;fetch from a service
    //this.updateRequestInfo(BusinessConstants.REQUEST_FORM['projectName'],this.requestObject.projectName,this.requestObject.requestItemId);
    if (this.selectedMarket && this.selectedMarket.trim()) {
      this.updateRequestInfo(BusinessConstants.REQUEST_FORM['projectName'], this.requestObject.projectName, this.requestObject.requestItemId);
    }
  }

  updateProjectNameEvent(selectedMarket) {
    this.selectedMarket = selectedMarket;
    this.updateProjectName();
  }
  updateThisProjectEvent(data) {
    this.deliverablesVsThresholdsComponent.updateThisProject(data);
  }

  ngOnDestroy() {
    this.monsterUtilService.clearFormPropertiesandRelations();
  }

  validateRequest() {
    const validation = {
      projectCoreComponent: true,
      gainApprovalScopeComponent: true,
      portfolioStrategyComponent: true,
      deliverablesVsThresholdsComponent: true,
      marketScopeComponent: true,
      managerFieldComponent: true,
      projectClassificationComponent: true,
      genericFormComponent: true,
      projectClassificationComponentAuto: true,
      programmeManagerReportingComponent: true
    };
    if (this.projectClassificationComponent?.validateEarlyProjectClassificationFields()) {
      this.notificationService.error('Please check if classifications are configured');
      validation.projectClassificationComponentAuto = true;
    } else {
      validation.projectClassificationComponentAuto = false;
    }
    if (this.projectCoreComponent?.onValidate()) {
      this.notificationService.error('Please fill in all the mandatory fields');
      validation.projectCoreComponent = true;
    } else {
      validation.projectCoreComponent = false;
    }
    if (this.gainApprovalScopeComponent?.onValidate()) {
      this.notificationService.error('Please fill in all the mandatory fields');
      validation.gainApprovalScopeComponent = true;
    } else {
      validation.gainApprovalScopeComponent = false;
    }
    if (this.portfolioStrategyComponent?.onValidate()) {
      this.notificationService.error('Please fill in all the mandatory fields');
      validation.portfolioStrategyComponent = true;
    } else {
      validation.portfolioStrategyComponent = false;
    }
    if (this.deliverablesVsThresholdsComponent?.onValidate()) {
      this.notificationService.error('Please fill in all the mandatory fields');
      validation.deliverablesVsThresholdsComponent = true;
    } else {
      validation.deliverablesVsThresholdsComponent = false;
    }
    if (this.marketScopeComponent?.onValidate()) {
      this.notificationService.error('Please fill in all the mandatory fields');
      validation.marketScopeComponent = true;
    } else {
      validation.marketScopeComponent = false;
    }
    if (this.managerFieldComponent?.onValidate()) {
      this.notificationService.error('Please fill in all the mandatory fields');
      validation.managerFieldComponent = true;
    } else {
      validation.managerFieldComponent = false;
    }
    if (this.genericFormComponent?.onValidate()) {
      this.notificationService.error('Please fill in all the mandatory fields');
      validation.genericFormComponent = true;
    } else {
      validation.genericFormComponent = false;
    }
    if (this.projectClassificationComponent?.onValidate()) {
      this.notificationService.error('Please fill in all the mandatory fields');
      validation.projectClassificationComponent = true;
    } else {
      validation.projectClassificationComponent = false;
    }
    if (this.programmeManagerReportingComponent?.onValidate()) {
      this.notificationService.error('Please fill in all the mandatory fields.');
      validation.programmeManagerReportingComponent = true;
    } else {
      validation.programmeManagerReportingComponent = false;
    }
    // validation.deliverablesVsThresholdsComponent = false;//remove this after complete code
    // validation.marketScopeComponent = false;//remove this after complete code
    const flag = Object.keys(validation).every(ele => validation[ele] === false);

    // Validate project classification data
    if (flag) {
      return true;
    } else {
      return false;
    }
  }

  previewForm() {
    //add submit validations
    if (!this.validateRequest()) {
      return;
    }
    this.requestFieldHandler.actionButtons.preview.ngIf = false;
    this.requestFieldHandler.actionButtons.submit.ngIf = true;
  }

  onSubmit() {
    //add submit validations
    if (!this.validateRequest()) {
      return;
    }
    let notifyMessage = "Request submitted successfully ";
    if (this.requestObject.requestId) {
      notifyMessage += '- ' + this.requestObject.requestId
    }
    let status = this.monsterUtilService.getStatusDisplayName(BusinessConstants.REQUEST_STATUS_PROGRESS);
    this.updateRequestInfo(BusinessConstants.REQUEST_FORM['status'], status && status.VALUE, this.requestObject.requestItemId);
    this.monsterUtilService.finalBulkUpdateFormData(() => {
      this.notificationService.success(notifyMessage);
      this.navigateToDashboard();
    }, () => {
      this.notificationService.error('Something went wrong while submitting the request.');
    });
  }

  onApprove() {
    if (!this.validateRequest()) {
      return;
    }
    const dialogRef = this.dialog.open(MonsterConfirmationModalComponent, {
      width: '50%',
      disableClose: true,
      data: {
        isCommentRequired: false,
        message: 'Please specify the reason for approval?',
        submitButton: 'Save',
        cancelButton: 'Cancel',
        hasConfirmationComment: true,
        maxLength: 500
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.isTrue) {
        this.loaderService.show();
        let successMessage = 'Task Approved succesfully';
        let errorMessage = 'Something went wrong while approving the task';//this.loaderService.show();
        let decision = this.stageDecisions.find(decision => decision.value === 'Approved').value && this.stageDecisions.find(decision => decision.value === 'Approved').value
        this.genericTaskComplete(successMessage, errorMessage, BusinessConstants.TASK_ACTION_APPROVED, true, decision, true, result.confirmationComment);
      }
    });
  }

  onRejected() {
    const dialogRef = this.dialog.open(MonsterConfirmationModalComponent, {
      width: '50%',
      disableClose: true,
      data: {
        //confirmationHeading : 'Resubmit',
        message: 'Please specify the reason for rejection?',
        submitButton: 'Save',
        cancelButton: 'Cancel',
        hasConfirmationComment: true,
        isCommentRequired: true,
        errorMessage: 'Please specify the reason for rejection',
        maxLength: 500
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.isTrue && result.confirmationComment) {
        this.loaderService.show();
        let successMessage = 'Task Rejected succesfully';
        let errorMessage = 'Something went wrong while rejecting the task';
        let decision = this.stageDecisions.find(decision => decision.value === 'Stop').value && this.stageDecisions.find(decision => decision.value === 'Stop').value
        this.genericTaskComplete(successMessage, errorMessage, BusinessConstants.TASK_ACTION_REJECTED, true, decision, true, result.confirmationComment);
      }
    });

  }

  onResubmit() {//provide popup comment box & store it in CPO stage
    if (!this.validateRequest()) {
      return;
    }
    const dialogRef = this.dialog.open(MonsterConfirmationModalComponent, {
      width: '50%',
      disableClose: true,
      data: {
        //confirmationHeading : 'Resubmit',
        message: 'Please specify the reason for requested change?',
        submitButton: 'Save',
        cancelButton: 'Cancel',
        hasConfirmationComment: true,
        isCommentRequired: true,
        errorMessage: 'Please specify the reason for requested change',
        maxLength: 500
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.isTrue && result.confirmationComment) {
        this.loaderService.show();
        let successMessage = 'Task Resubmitted succesfully';
        let errorMessage = 'Something went wrong while resubmitting the task';
        let decision = this.stageDecisions.find(decision => decision.value === 'Recycle').value && this.stageDecisions.find(decision => decision.value === 'Recycle').value
        this.genericTaskComplete(successMessage, errorMessage, BusinessConstants.TASK_ACTION_RESUBMIT, true, decision, true, result.confirmationComment);
      }
    });
  }

  completeTask() {
    if (!this.validateRequest()) {
      return;
    }
    let successMessage = 'Task completed succesfully';
    let errorMessage = 'Something went wrong while completing the task';
    this.genericTaskComplete(successMessage, errorMessage, '', false);
  }

  convertToProject() {
    if (!this.validateRequest()) {
      return;
    }
    let status = this.monsterUtilService.getStatusDisplayName(BusinessConstants.REQUEST_STATUS_ACTIVE);
    this.monsterUtilService.setFormProperty(this.requestId, 'Request_Status', status.VALUE);
    this.monsterUtilService.finalBulkUpdateFormData(() => {
    }, () => {
    });
    let successMessage = 'Task converted to project succesfully';
    let errorMessage = 'Something went wrong while converting to project';
    this.genericTaskComplete(successMessage, errorMessage, BusinessConstants.TASK_ACTION_APPROVED, false);
  }

  genericTaskComplete(notifyMessage, errorMessage, action, isSaveData, decision?, decisionDate?, comment?) {
    let requestObject = {
      requestId: this.requestObject && this.requestObject.requestId,
      projectName: this.requestObject && this.requestObject.projectName
    }
    /*  if(!this.isBDA) {
       this.updateGovernanceMilestoneObject(isSaveData,decision,comment,decisionDate);
     }  */
    this.updateGovernanceMilestoneObject(isSaveData, action, decision, comment, decisionDate);
    if (isSaveData) {
      this.monsterUtilService.finalBulkUpdateFormData(() => {
        this.loaderService.show();
        this.requestService.triggerActionFlow(action, requestObject, this.taskId).subscribe(response => {

          if (action == BusinessConstants.TASK_ACTION_APPROVED || action == BusinessConstants.TASK_ACTION_REJECTED || action == BusinessConstants.TASK_ACTION_RESUBMIT) {
            let commentDetails: NewCommentModal = {
              requestId: this.requestId.split('.')[1],
              commentText: 'Request- ' + comment,
              commentType: CommentConfig.REQUEST_GROUP_FILTER_VALUE,
              refCommentId: null
            }
            if (comment.length > 0) {
              this.commentsService.createNewComment(commentDetails).subscribe(response => {
                if (response) {
                  this.loaderService.hide();
                  this.notificationService.success(notifyMessage);
                  this.routerService.goToRequestDashboard(this.menu);
                }
              }, (error) => {
                this.loaderService.hide();
                this.notificationService.error(errorMessage);
              });
            }
            else {
              this.loaderService.hide();
              this.notificationService.success(notifyMessage);
              this.routerService.goToRequestDashboard(this.menu);
            }
          } else {
            this.loaderService.hide();
            this.notificationService.success(notifyMessage);
            this.routerService.goToRequestDashboard(this.menu);
          }
        }, (error) => {
          this.loaderService.hide();
          this.notificationService.error(errorMessage);
        });
      }, () => {
        this.loaderService.hide();
        this.notificationService.error('Something went wrong while updating details.');
      });
    } else {
      this.requestService.triggerActionFlow(action, requestObject, this.taskId).subscribe(response => {
        this.loaderService.hide();
        this.notificationService.success(notifyMessage);
        this.routerService.goToRequestDashboard(this.menu);
      }, (error) => {
        this.loaderService.hide();
        this.notificationService.error(errorMessage);
      });
    }

  }



  updateGovernanceMilestoneObject(isUpdateAction, action, decision?, comment?, decisionDate?) {
    if (isUpdateAction) {
      for (var i = 0; i < this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope.length; i++) {
        if (this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[i].Stage == 'CP0') {
          if (decision) {
            this.updateRequestInfo(BusinessConstants.DYNAMIC_GOVERNANCE_MILESTONE_FORM['decision'], decision, this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[i].itemId);
          }
          if (comment && !(this.isBDA && action == BusinessConstants.TASK_ACTION_APPROVED)) {
            this.updateRequestInfo(BusinessConstants.DYNAMIC_GOVERNANCE_MILESTONE_FORM['comments'], comment, this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[i].itemId);
          }
          if (decisionDate) {
            //let date = new Date().toISOString();//new Date().toISOString()
            let date;
            let week = this.monsterUtilService.calculateWeek(new Date());//to get week number
            [, date] = this.monsterUtilService.getStartEndDate(week);//to get end date of week
            this.updateRequestInfo(BusinessConstants.DYNAMIC_GOVERNANCE_MILESTONE_FORM['decisionDate'], date, this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[i].itemId);
          }
        }
      }
    }
  }

  updateRequestInfo(property, value, itemId, formName?) {
    this.monsterUtilService.setFormProperty(itemId, property, value);
  }

  navigateToDashboard() {
    this.routerService.goToRequestDashboard(this.menu);
  }

  cancelRequestCreation(isDiscard) {
    if (!isDiscard && !(this.requestFieldHandler.actionButtons.backToHome.showConfirmation)) {
      this.navigateToDashboard();
      return;
    }
    let openConfirmationPopup = this.validateBackButton(isDiscard)
    if (openConfirmationPopup) {
      const dialogRef = this.dialog.open(MonsterConfirmationModalComponent, {
        width: '40%',
        disableClose: true,
        data: {
          message: isDiscard ? 'Are you sure you want to Discard the changes?' : 'Are you sure you want to go back?',
          submitButton: 'Yes',
          cancelButton: 'No'
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result && result.isTrue && isDiscard) {
          this.deleteRequest();
        }
        else if (result && result.isTrue && !isDiscard) {
          this.notificationService.info('Redirecting to Request Dashboard');
          this.navigateToDashboard();
        }
      });
    }
  }

  deleteRequest() {
    this.loaderService.show();
    const itemIds = [];
    if (this.marketScopeComponent) {
      for (let i = 0; i < this.marketScopeComponent.dynamicMarketScopeForm.value.marketScope.length; i++) {
        itemIds.push(this.marketScopeComponent.dynamicMarketScopeForm.value.marketScope[i].itemId);
      }
    }
    if (this.deliverablesVsThresholdsComponent) {
      for (let i = 0; i < this.deliverablesVsThresholdsComponent.deliverablesForm.value.deliverablesRow.length; i++) {
        itemIds.push(this.deliverablesVsThresholdsComponent.deliverablesForm.value.deliverablesRow[i].itemId);
      }
    }
    const deleteitems = [{
      entity_key: EntityListConstant.REQUEST,
      itemIds: [this.requestObject.requestItemId]
    }];
    if (itemIds.length > 0) {
      deleteitems.push({
        entity_key: EntityListConstant.COMMERCIAL_MARKET_SCOPE,
        itemIds: itemIds
      });
    }
    this.entityService.deleteMultipleDifferentEntityObjects(deleteitems, this.utilService.APP_ID).subscribe(() => {
      this.monsterUtilService.clearFormPropertiesandRelations();
      this.notificationService.success('Request was discarded successfully.');
      this.navigateToDashboard();
      this.loaderService.hide();
    }, () => {
      this.notificationService.error('Error while discarding draft.');
      this.loaderService.hide();
    });
  }

  validateBackButton(isDiscard) {
    let openConfirmationPopup = true;
    //NPD1-81 - Check whether atleast one market is added and one lead market is selected
    if (!isDiscard) {

      const [isMarketScopeAdded, isLeadMarketSelected] = this.getMarketScopeValidation();
      if (isMarketScopeAdded && isLeadMarketSelected) {
        openConfirmationPopup = true;
      } else {
        openConfirmationPopup = false;
        if (isMarketScopeAdded) {
          if (!isLeadMarketSelected) {
            this.notificationService.warn('Select atleast one Lead Market.');
          }
        } else {
          this.notificationService.warn('Select atleast one Market Scope.');
        }
      }
    }
    return openConfirmationPopup;
  }

  initialiseRequestConfig() {
    this.requestFieldHandler = this.monsterRequestService.initializeRequestConfig();
  }

  getAllBool() {
    this.marketScopeComponent.getAllBool();
  }
  getTaskInfo(isConfigLoad) {
    const filters = [];
    filters.push({
      name: EntityListConstant.INBOX_PARENT_TASK_ID,
      operator: 'eq',
      value: this.taskDetails[0].Task.ParentTaskId
    });
    const filterParam = this.entityService.perpareFilterParameters(EntityListConstant.INBOX_ALL_TASK, filters, '', this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.INBOX_ALL_TASK, filterParam, '', '', '', '', this.utilService.APP_ID).subscribe((response) => {
      this.showInboxControls = true;
      const requestObject = acronui.findObjectsByProp(response, 'items');
      this.inboxService.setSelectedTaskObject(requestObject[0]);
      this.taskOperations?.enableInboxOperationControls();
      isConfigLoad && this.loaderService.hide();
    }, () => {
      this.notificationService.error('Error while retrieving the inbox task details.');
    });
  }

  refreshTaskConfig(event) {
    this.getTaskDetails(true);
    this.marketScopeComponent.ngOnInit()
  }

  updateFormDisability() {
    if (this.projectCoreComponent) {
      this.projectCoreComponent.updateFormControlsDisability();
    }
    if (this.governancemilestone && this.governancemilestone.stagesScopeFormArray) {
      this.governancemilestone.updateFormControlsDisability();
    }
    if (this.gainApprovalScopeComponent) {
      this.gainApprovalScopeComponent.updateFormControlsDisability();
    }
    if (this.portfolioStrategyComponent) {
      this.portfolioStrategyComponent.updateFormControlsDisability();
    }
    if (this.projectClassificationComponent) {
      this.projectClassificationComponent.updateFormControlsDisability();
    }
    if (this.managerFieldComponent) {
      this.managerFieldComponent.updateFormControlsDisability();
    }
    if (this.genericFormComponent) {
      this.genericFormComponent.updateFormControlsDisability();
    }
    if (this.marketScopeComponent) {
      this.marketScopeComponent.updateFormControlsDisability();
    }
    if (this.deliverablesVsThresholdsComponent) {
      this.deliverablesVsThresholdsComponent.updateFormControlsDisability();
    }
    if (this.programmeManagerReportingComponent) {
      this.programmeManagerReportingComponent.updateFormControlsDisability();
    }
  }

  allThresholdValuesEvent(data) {
    // this.marketScopeComponent.getAllThresholdValues(data);
    this.deliverablesVsThresholdsComponent.getAllThresholdValuesMinimumThreshold(data);
  }

  allThresholdValuesForMonsterGreenEvent(data) {
    // this.marketScopeComponent.getAllThresholdValues(data);
    this.deliverablesVsThresholdsComponent.getAllThresholdValues(data);
  }
  projectClassification(data) {
    if (this.projectClassificationComponent) {
      this.projectClassificationComponent.getProjectClassification(data);
    }
  }

  linkedMarkets(data) {
    if (this.programmeManagerReportingComponent) {
      this.programmeManagerReportingComponent.updateLinkedMarketsFormValue(data);
    }
    this.monsterUtilService.setFormProperty(this.requestItemId, BusinessConstants.PROGRAMME_MANAGER_REPORTING.linkedMarkets, data);
  }

  updateMonsterGreenEvent(data) {
    this.deliverablesVsThresholdsComponent.getAllThresholdValues(data);
    this.deliverablesVsThresholdsComponent.getAllThresholdValuesMinimumThreshold(data);
  }

  updateDeliverablesThresholdValues() {
    this.projectCoreComponent.updateDeliverablesThresholdValues();
  }
  setRegulatoryTimeLineLocations(data) {
    if (this.projectClassificationComponent) {
      this.projectClassificationComponent.setRegulatoryTimeLineLocations(data);
    }
  } getRegulatoryTimeLineLocations(data) {
    if (this.projectClassificationComponent) {
      this.projectClassificationComponent.getRegulatoryTimeLineLocations(data);
    }
  }

  deleteRequestUpdate() {
    this.confirmDelete((result) => {
      if (result) {
        this.loaderService.show();
        let status = this.monsterUtilService.getStatusDisplayName(BusinessConstants.REQUEST_STATUS_DELETED);
        this.updateRequestInfo(BusinessConstants.REQUEST_FORM['status'], status && status.VALUE, this.requestObject.requestItemId);
        this.monsterUtilService.finalBulkUpdateFormData(() => {
          this.loaderService.hide();
          this.notificationService.success("Request deleted successfully");
          this.navigateToDashboard();
        }, () => {
          this.notificationService.error('Something went wrong while deleting the request.');
        });
      }
    })
  }


  confirmDelete(callback) {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '40%',
      disableClose: true,
      data: {
        message: 'Are you sure you want to delete the Request?',
        submitButton: 'Yes',
        cancelButton: 'No'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (callback) { callback(result ? result.isTrue : null) };
    });
  }

  copyRequestButton() {
    this.loaderService.show();
    this.isCopyRequest = RequestsCopyConstants.COPY_REQUEST_TRUE;
    this.technicalRequestService.sendData(this.requestId);
    this.technicalRequestService.setCopyCondtion(this.isCopyRequest);
    this.routerService.goToRequestView(("Commercial").toLowerCase(), "allRequest");
    this.loaderService.hide();
  }

  initializeCopy() {
    this.isNewRequest = true;
    this.canComplete = false;
    this.isDraft = true;
    this.technicalRequestService.sendDatum().subscribe(data => {
      this.requestDetails = data;
      this.copyCommercialRequestFormGroups();
    })
  }
  public leadMarket = '';
  public getLeadMArketRegion() {
    this.leadMarket = this.projectCoreComponent.getLeadMarketRegion();
  }
}
