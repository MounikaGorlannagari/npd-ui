import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { AppService, LoaderService, NotificationService, SharingService, UtilService } from 'mpm-library';

import { forkJoin, Observable } from 'rxjs';
import { RouteService } from 'src/app/core/mpm/route.service';
import { InboxService } from '../../../request-management/requestor-dashboard/inbox/inbox.service';
import { EntityService } from '../../../services/entity.service';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { RequestService } from '../../../services/request.service';
import { EntityListConstant } from '../../constants/EntityListConstant';
import { BusinessConstants } from '../../constants/BusinessConstants';


@Component({
  selector: 'app-governance-milestone',
  templateUrl: './governance-milestone.component.html',
  styleUrls: ['./governance-milestone.component.scss']
})
export class GovernanceMilestoneComponent implements OnInit {


  constructor(private loaderService:LoaderService,private entityService: EntityService,
    private monsterUtilService: MonsterUtilService,private formBuilder: FormBuilder,private adapter: DateAdapter<any>,
    private notificationService: NotificationService,private sharingService: SharingService,
    private appService: AppService) { }

  @Input() technicalRequestId;
  @Input() governanceMilestoneConfig;
  readonly decisions =  BusinessConstants.stageDecisions;

   /*
  * Market Scope Grid Variables
  */
   @ViewChild('stagesScopeTable') stagesScopeTable: MatTable<any>;

  dynamicGovernanceMilestoneForm: FormGroup;
  gmGridColumns = [];
  showGovernanceMilestoneForm;
  loadingGovernanceMilestone;
  isGovernanceMilestoneUpdate = false;

  // convenience getters for easy access to form fields
  get dynamicGovernanceMilestoneFormControl() { return this.dynamicGovernanceMilestoneForm.controls; }
  get stagesScopeFormArray() { return this.dynamicGovernanceMilestoneFormControl.stagesScope as FormArray; }

  mapStagesScopeContentToForm(stagesScope) {
    this.dynamicGovernanceMilestoneForm = this.formBuilder.group({
      stagesScope: new FormArray([])
    });
    this.gmGridColumns = ['stages', 'targetStartDate', 'decisionDate', 'decision','comments'];
    this.showGovernanceMilestoneForm = true;
    for (let i = 0; i < stagesScope.length; i++) {
      this.stagesScopeFormArray.push(this.formBuilder.group({
        stages: [stagesScope[i].Properties.Stage],
        targetStartDate:  [{value:stagesScope[i].Properties.Target_Start_Date && new Date(stagesScope[i].Properties.Target_Start_Date).toISOString(), disabled: this.governanceMilestoneConfig.targetStartDate.disabled}],
        decisionDate: [{value:stagesScope[i].Properties.Decision_Date && new Date(stagesScope[i].Properties.Decision_Date).toISOString(), disabled: this.governanceMilestoneConfig.decisionDate.disabled}],
        decision: [stagesScope[i].Properties.Decision],
        comments: [{ value: stagesScope[i].Properties.Comments,disabled: this.governanceMilestoneConfig.comments.disabled },[Validators.maxLength(2000)]],
        itemId: [stagesScope[i].Identity.ItemId]
      }));
      this.renderGMTableRows();
    }
  }

  test(event) {
    // console.log(event);
  }
  /**
   * To re-render material table 
   */
  renderGMTableRows() {
      this.stagesScopeTable && this.stagesScopeTable.renderRows ?
        this.stagesScopeTable.renderRows() : console.log('No rows in Governance Milestone.');
  }

 /**
   * To load Governance Milestones and set form groups
   */
 getAllGovernanceMilestones() {
  // this.loaderService.show();
  this.loadingGovernanceMilestone = true;
  this.entityService.getEntityItemDetails(this.technicalRequestId,
    EntityListConstant.TECHNICAL_REQUEST_GOVERNANCE_MILESTONE_RELATION).subscribe((governanceMilestoneResponse) => {
      // create form groups for GovernanceMilestones
      if (governanceMilestoneResponse && governanceMilestoneResponse.items) {
        this.mapStagesScopeContentToForm(governanceMilestoneResponse.items);
        // this.loaderService.hide();
      }
      this.loadingGovernanceMilestone = false;
    }, (error) => {
      // this.loaderService.hide();
      this.loadingGovernanceMilestone = false;
    });
  }

  updateGMInfo(formControl,index,property) {
    //this.isGovernanceMilestoneUpdate = true;//dynamicGovernanceMilestoneForm.value.stagesScope[i].decisionDate
    this.monsterUtilService.setFormProperty(this.dynamicGovernanceMilestoneForm.value.stagesScope[index].itemId, property, this.dynamicGovernanceMilestoneForm.value.stagesScope[index][formControl]);
  }

  ngOnInit() {
    // this.loaderService.show();
    if(this.technicalRequestId && this.governanceMilestoneConfig) {
      this.getAllGovernanceMilestones();
    }
    if (navigator.language !== undefined) {//for mat date picker
      this.adapter.setLocale(navigator.language);
    }
  }

}
