import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatOption } from '@angular/material/core';
import { MatSelect } from '@angular/material/select';
import { MatTable } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { AppService, ConfirmationModalComponent, NotificationService, SharingService, UtilService } from 'mpm-library';
import { LoaderService } from 'mpm-library';
import { forkJoin, Observable } from 'rxjs';
import { EntityService } from '../../../services/entity.service';
import { EntityListConstant } from '../../constants/EntityListConstant';
import * as acronui from 'mpm-library';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../constants/BusinessConstants';
import { RouteService } from '../../../../mpm/route.service';
import { MatDialog } from '@angular/material/dialog';
import { WeekPickerComponent } from '../../generic-components/week-picker/week-picker.component';
import { RequestDashboardControlConstant } from '../../../request-management/constants/request-dashboard-control-constants';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { InboxService } from '../../../request-management/requestor-dashboard/inbox/inbox.service';
import { RequestService } from '../../../services/request.service';
import { ProjectClassificationDataComponent } from '../project-classification-data/project-classification-data.component';
import { GovernanceMilestoneComponent } from '../governance-milestone/governance-milestone.component';
import { InvokeMethodExpr } from '@angular/compiler';
import { TaskOperationsComponent } from '../../generic-components/task-operations/task-operations.component';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';


@Component({
  selector: 'app-technical-business',
  templateUrl: './technical-business.component.html',
  styleUrls: ['./technical-business.component.scss']
})
export class TechnicalBusinessComponent implements OnInit {

  @ViewChild('projectClassificationData') projectClassificationData : ProjectClassificationDataComponent;
  @ViewChild('taskOperations') taskOperations:TaskOperationsComponent;
  projectClassificationResult = {};
  show = [];
  modal : any;
  selectedBusinessUnit;
  businessUnitInitialCount = 0;
  targetDPDate = [];
  convertedTargetDPDate = [];
  isTaskType = false;
  requestStatus;
  requestForm: FormGroup;
  dynamicMarketScopeForm: FormGroup;
  requestItemId: any;
  technicalRequestItemId: any;
  requestId: any;
  updateObject = [];
  showTechnicalRequestForm: boolean;
  showMarketScopeForm: boolean;
  loadingMarketScope = false;
  projectTypes = [];
  platforms = [];
  brands = [];
  variants = [];
  commercialCategories = [];
  governanceApproaches = [];
  primaryPackagingTypes = [];
  //NPD1-81
  casePackSizes = [];
  consumerUnitSizes = [];
  productionSites = [];
  bottlers = [];
  skuDetails = [];
  selectedMarket:string;
  selectedPlatformName:string=null;
  selectedVariantName:string=null;
  projectName:string = null;
  fromTargetDate:NgbDate[] = [];
  toTargetDate:NgbDate[] = [];

  msGridColumns = [];
  markets = [];
  businessUnits = [];
  selectedMarketOption = [];
  selectedMultiMarket = [];
  marketsCopy = [];
  e2ePMUsers = [];
  allMarketSelected = false;
  selectede2EPM;
  selection = new SelectionModel<any>(true, []);
  urlParams;
  menu;
  isNewRequest = false;
  taskId;
  panelHTML: HTMLElement;
  editProjectName;
  showProjectName = true;
  isPM = false;
  isE2EPM = false;
  isReqRework = false;
  isRequestView = false;
  readonly radioButtonsList = BusinessConstants.radioButtons;
  taskObjectHandler : {};
  showInboxControls = false;

  requestFieldHandler = {
    technicalRequest: {
      'projectName' : {
        'disabled': false,
        'ngIf': true
      },
      'businessUnit' : {
        'disabled': true,
        'ngIf': true
      },
      'bottler': {
        'disabled': false,
        'ngIf': true
      },
      'projectType': {
        'disabled': false,
        'ngIf': true
      },
      'productionSite' : {
        'disabled': false,
        'ngIf': true
      },
      'platform': {
        'disabled': false,
        'ngIf': true
      },
      'brand' : {
        'disabled': false,
        'ngIf': true
      },
      'primaryPackagingType': {
        'disabled': false,
        'ngIf': true
      },
      'secondaryPackagingType' : {
        'disabled': false,
        'ngIf': true
      },
      'casePackSize': {
        'disabled': false,
        'ngIf': true
      },
      'consumerUnitSize' : {
        'disabled': false,
        'ngIf': true
      },
      'variant': {
        'disabled': false,
        'ngIf': true
      },
      'skuDetails' : {
        'disabled': false,
        'ngIf': true
      },
      'requestRationale': {
        'disabled': false,
        'ngIf': true
      },
      'commercialCategory': {
        'disabled': false,
        'ngIf': false
      },
      'governanceApproach': {
        'disabled': false,
        'ngIf': false
      },
      'e2ePM': {
        'disabled': false,
        'ngIf': false
      },
      'svpAligned': {
        'disabled': false,
        'ngIf': false
      },
      'postLaunchAnaylsis' : {
        'disabled': false,
        'ngIf': false
      },
      'requestedDate' : {
        'ngIf': false
      }
    },
    marketScope: {
      'market': {
        'disabled': false,
        'ngIf': false
      },
      'leadMarket': {
        'disabled': false,
        'ngIf': true
      },
      'targetDP': {
        'disabled': false,
        'ngIf': true
      },
      'Model': {
        'ngIf': true
      },
      'actions': {
        'ngIf': true
      }
    },
    governanceMilestone: {
      'Model':{
        'ngIf': false
      },
      'stage' : {
        'disabled': false,
        'ngIf': true
      },
      'targetStartDate' : {
        'disabled': false,
        'ngIf': true
      },
      'decisionDate' : {
        'disabled': false,
        'ngIf': true
      },
      'decision' : {
        'disabled': false,
        'ngIf': true
      },
      'comments' : {
        'disabled': false,
        'ngIf': true
      }
    },
    projectClassificationData: {
      'Model':{
        'ngIf': false
      },
      'earlyProjectClassification' :  {
        'disabled': false,
        'ngIf': true
      },
      'earlyManufacturingSite' : {
        'disabled': false,
        'ngIf': true
      },
      'draftManufacturingLocation' : {
        'disabled': false,
        'ngIf': true
      },
      'newFg' : {
        'disabled': false,
        'ngIf': true
      },
      'newRnDFormula' : {
        'disabled': false,
        'ngIf': true
      },
      'newHBCFormula' : {
        'disabled': false,
        'ngIf': true
      },
      'earlyProjectClassificationDesc' : {
        'disabled': false,
        'ngIf': true
      },
      'newPrimaryPackaging' : {
        'disabled': false,
        'ngIf': true
      },
      'secondaryPackaging' : {
        'disabled': false,
        'ngIf': true
      },
      'registrationDossier' : {
        'disabled': false,
        'ngIf': true
      },
      'preProdReg' : {
        'disabled': false,
        'ngIf': true
      },
      'postProdReg' : {
        'disabled': false,
        'ngIf': true
      },
      'techDesc' : {
        'disabled': false,
        'ngIf': true
      },
      'corpPm' : {
        'disabled': false,
        'ngIf': true
      },
      'opsPm' : {
        'disabled': false,
        'ngIf': true
      },
      'registrationClassificationAvailable':{
        'disabled':false,
        'ngIf':true
      },
      'registrationRequirement':{
        'disabled':false,
        'ngIf':true
      },
      'leadFormula' : {
        'disabled': false,
        'ngIf' : true
      },
      'e2ePm' : {
        'disabled': false,
        'ngIf' : true
      }
    },
    actionButtons: {
      discard: {
        ngIf: false
      },
      saveAsDraft: {
        ngIf: false
      },
      preview: {
        ngIf: false
      },
      submit: {
        ngIf: false
      },
      approve: {
        ngIf: false
      },
      deny: {
        ngIf: false
      },
      resubmit: {
        ngIf: false
      },
      backToHome : {
        ngIf : true,
        showConfirmation : false
      },
      convertToProject : {
        ngIf : false
      },
      complete : {
        ngIf : false,
      }
    },
    other : {
      taskHeader : {
        ngIf : false,
        value : ''
      },
      autoSave : {
        value : false
      }
    }
  };

  isDraft: boolean;
  canComplete: boolean;
  technicalRequestResponse: any = {};
  propertyUpdateErrorRetryCount = 0;
  fromDate: NgbDate //| null = null;
  toDate: NgbDate //| null = null;
  stageDecisions  = BusinessConstants.stageDecisions;

  /*
  * Market Scope Grid Variables
  */
  @ViewChild('marketScopeTable') marketScopeTable: MatTable<any>;
  @ViewChild('marketSelect') marketSelect: MatSelect;
  @ViewChild('governancemilestone') governancemilestone :GovernanceMilestoneComponent;
  taskDetails: any;
  currentActivity: any;
  taskOwner: any;
  taskStatus: any;
  technicalReq: any;

  // convenience getters for easy access to form fields
  get dynamicMarketScopeFormControl() { return this.dynamicMarketScopeForm.controls; }
  get marketScopeFormArray() { return this.dynamicMarketScopeFormControl.marketScope as FormArray; }


  constructor(private loaderService:LoaderService,private entityService: EntityService,
    private utilService: UtilService,private formBuilder: FormBuilder,
    private notificationService: NotificationService,private sharingService: SharingService,
    private activeRoute: ActivatedRoute,private appService: AppService,private monsterUtilService:MonsterUtilService,
    private routerService: RouteService,private dialog: MatDialog,
    private inboxService:InboxService,private requestService:RequestService,
    private monsterConfigApiService:MonsterConfigApiService) { }

  getStatusDisplayName(statusValue) {
    let status = this.monsterUtilService.getStatusDisplayName(statusValue);
    return status && status.DISPLAY_NAME;
  }

  onBrandChange(brandFormControl,formName,brand) {
    this.updateRequestRelationsInfo(brandFormControl,formName);
    let brandRequestParams = {
      'Brands-id' : {
         Id : brand && brand.split('.')[1],
         ItemId : brand
      }
    }
    this.platforms = [];
    this.variants = [];

    this.updateRequestRelationsInfo('platform',formName);
    this.updateRequestRelationsInfo('variant',formName);
    //NPD1-81
    this.selectedPlatformName = null;
    this.selectedVariantName = null;
    this.updateProjectName();

    this.loaderService.show();
    this.getPlatformsByBrand(brandRequestParams).subscribe(response=> {
      this.loaderService.hide();
    },(error) => {
      this.loaderService.hide();
    });
  }

  onPlatformChange(platformFormControl,formName,platformItemId) {
    this.updateRequestRelationsInfo(platformFormControl,formName);
    this.updateRequestRelationsInfo('variant',formName);
    let platformRequestParams = {
      'Platforms-id' : {
         Id : platformItemId && platformItemId.split('.')[1],
         ItemId : platformItemId
      }
    }
    let selectedPlatForm:any = this.platforms.filter( platform => {
      return platform.ItemId === platformItemId ? platform.displayName : null
    });

    //NPD1-81
    this.selectedPlatformName= selectedPlatForm[0].displayName;
    this.selectedVariantName=null;
    this.updateProjectName();

    this.variants = [];
    this.loaderService.show();
    this.getVariantsByPlatform(platformRequestParams).subscribe(response=> {
      this.loaderService.hide();
    },(error) => {
      this.loaderService.hide();
    });
  }

  onVariantChange(variantFormControl,formName,variant) {
     //changing project name based on platform & variant applied
     this.updateRequestRelationsInfo(variantFormControl,formName);
     let selectedVarint = this.variants.filter(v => { return v.ItemId === this.requestForm.controls.variant.value ? v.displayName : null} );
     this.selectedVariantName = selectedVarint[0].displayName;
     this.updateProjectName();
  }

  updateRequestInfo(formControl,formName,propertyValue?,index?) {
    let property;
    let value;
    let itemId;

    if(formName === 'requestForm') {
      property = BusinessConstants.REQUEST_FORM[formControl];
      value = this.requestForm.controls[formControl] && this.requestForm.controls[formControl].value;
      itemId = this.requestItemId;
      if(formControl === 'projectName') {
        value = this.projectName;
      }
      if(formControl === 'status' && propertyValue) {
        value = propertyValue;
      }
    }
    if(formName === 'dynamicMarketScopeForm' && index >=0) {
      property = BusinessConstants.DYNAMIC_TECHNICAL_MARKET_SCOPE_FORM[formControl];
      value = propertyValue? propertyValue : this.dynamicMarketScopeForm.value.marketScope[index] && this.dynamicMarketScopeForm.value.marketScope[index][formControl]
      itemId = this.dynamicMarketScopeForm.value.marketScope[index] && this.dynamicMarketScopeForm.value.marketScope[index].itemId
    }
    if(formName === 'dynamicGovernanceMilestoneForm' && index >=0 && this.governancemilestone) {
      property = BusinessConstants.DYNAMIC_GOVERNANCE_MILESTONE_FORM[formControl];
      value = propertyValue ? propertyValue : this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[index][formControl];
      itemId = this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[index] && this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[index].itemId;
    }
    this.monsterUtilService.setFormProperty(itemId, property, value);
  }

  updateRequestRelationsInfo(formControl,formName,index?) {
    let relationName;
    let objectValue;
    let itemId;

    if(formName === 'requestForm') {
      relationName = BusinessConstants.REQUEST_FORM[formControl];
      objectValue = this.requestForm.controls[formControl] && this.requestForm.controls[formControl].value;
      itemId = this.requestItemId;

      if(formControl === 'businessUnit') {
        objectValue = this.selectedBusinessUnit && this.selectedBusinessUnit.ItemId
      }
      if(formControl === 'e2ePM') {
        objectValue =  this.selectede2EPM && this.selectede2EPM.userIdentity && this.selectede2EPM.userIdentity.Identity && this.selectede2EPM.userIdentity.Identity.ItemId
      }
    }

    if(formName === 'dynamicMarketScopeForm' && index) {
      relationName = BusinessConstants.DYNAMIC_GOVERNANCE_MILESTONE_FORM[formControl];
      objectValue = this.dynamicMarketScopeForm.value.marketScope[index] && this.dynamicMarketScopeForm.value.marketScope[index][formControl];
      itemId = this.dynamicMarketScopeForm.value.marketScope[index] && this.dynamicMarketScopeForm.value.marketScope[index].itemId
    }

    this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
  }

  updateSavedData(){
    this.monsterUtilService.updateFormProperty(true,(isUpdate?) => {
        if(isUpdate) {
          this.notificationService.success('Form changes are saved successfully.');
          this.entityService.getEntityObjectsByfilter(this.requestItemId).subscribe(response => {
            if (response && response.item.Properties.RequestID != null) {
              this.technicalRequestResponse.requestId = response.item.Properties.RequestID
            }
        });
        }
    },()=>{
      //error callback
    });
  }

  changeTargetDP(weekFormat,i) {
    this.targetDPDate[i] = weekFormat;
    this.marketScopeFormArray.controls[i].patchValue({targetDP: weekFormat});
    this.updateRequestInfo('targetDP','dynamicMarketScopeForm',weekFormat,i);
    //NPD1-83
    if(weekFormat && weekFormat!==undefined){
      let week = weekFormat.split("/")[0];
      let year = weekFormat.split("/")[1];

      let weekdates:NgbDate[] = this.calculateDate(week,year);

      this.fromTargetDate[i] = weekdates[0];
      this.toTargetDate[i] = weekdates[1];
    }

  }

  getBool(i) {
    this.show[i] = !this.show[i];
  }

  getAllBool() {
    this.show = this.show.map(ele => {
      ele = false;
    })
  }

  marketDuplicateValidator(selectedMarketOptions): ValidatorFn {
      return (control: AbstractControl): ValidationErrors | null => {
        const currentMarket = control.value;
        if (selectedMarketOptions.length === 1) {
          return null;
        }
        for (var i = 0; i < selectedMarketOptions.length; i++) {
          if (selectedMarketOptions[i] == currentMarket.ItemId) {
            return { 'marketDuplicateValidator': true };
          }
        }
        return null;
      }
  }

  /**
   * To re-render material table
   */
   renderMSTableRows() {
    this.marketScopeTable && this.marketScopeTable.renderRows ?
      this.marketScopeTable.renderRows() : console.log('No rows in Market Scope.');
  }

  /**
   * Toggle selected rows in Market Scope table
   */
   masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.marketScopeFormArray.controls.forEach(row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.marketScopeFormArray.controls.length;
    return numSelected === numRows;
  }

  checkboxLabel(row?: string): string {
    return '';
  }

  /**
   * To toggle header check boxes in lc gird
   * @param columnName
   * @param headerControlName -- Header Row Check box Name
   */
   optionLCClick(columnName, headerControlName, index?) {
    const formArray = this.dynamicMarketScopeForm.get('marketScope') as FormArray;
    let isChecked = true;
    if (formArray.controls.length === 0) {
      isChecked = false;
    }
    for (let i = 0; i < formArray.controls.length; i++) {
      if (formArray.controls[i].get(columnName).value !== true) {
        isChecked = false;
      }
    }
    if (this[headerControlName]) {
      this[headerControlName].checked = isChecked;
    }
  }

  /**
   * For toggleing All Select Option in Select Languages Dropdown
  */
  toggleAllSelection() {
      this.allMarketSelected = !this.allMarketSelected;
      if (this.allMarketSelected) {
        this.marketSelect.options.forEach((item: MatOption) => item.select());
      } else {
        this.marketSelect.options.forEach((item: MatOption) => item.deselect());
      }
  }

  deleteMarketScopeGroup(index?, itemId?) {
      const formArray = this.dynamicMarketScopeForm.get('marketScope') as FormArray;
      const deleteItem = formArray.controls[index].value.itemId;
      this.loaderService.show();
      this.entityService.deleteEntityObjects(deleteItem).subscribe(response => {
        this.monsterUtilService.removeFormProperty(deleteItem);
        this.monsterUtilService.removeFormRelations(deleteItem);
        if(formArray.controls[index].value.leadMarket) {
          this.requestForm.patchValue({businessUnit: ''});
        }
        this.marketScopeFormArray.removeAt(index);
        this.selectedMarketOption.splice(index, 1);

        //NPD1-83 Removing fromTargetDate & toTargetDate entry
        this.targetDPDate.splice(index,1);
        this.fromTargetDate.splice(index, 1);
        this.toTargetDate.splice(index, 1);
        //NPD1-81 Removing Market name from project name since lead market is deleted
        if(this.selectedMarketOption.length == 0){
          this.selectedMarket = null;
        } else{
          this.selectedMarket = this.selectedMarketOption[0].displayName;
        }
        this.updateProjectName();
        this.updateMultiMarkets();
        this.renderMSTableRows();
        this.loaderService.hide();
        this.notificationService.success('Market Scope deleted successfully.');
      }, error => {
        this.loaderService.hide();
      });
  }

   /**
   * To update multi markets
   */
  updateMultiMarkets() {
      this.marketsCopy = [];
      this.markets.forEach((market) => {
        const matchedLanguage = this.selectedMarketOption.filter((selectedMarket) => {
          return (market.ItemId === (selectedMarket && selectedMarket.ItemId));
        });
        if (matchedLanguage.length === 0) {
          this.marketsCopy.push(this.copyObj(market));
        }
      });
  }
   /**
   * To deep copy the objects
   * */
  copyObj(obj) {
      return JSON.parse(JSON.stringify(obj));
  }
  /**
   * To update the lc rows
   * @param columnName
   * @param value
  */
  updateMSColumns(columnName, value, isVendor?) {
    const formArray = this.dynamicMarketScopeForm.get('marketScope') as FormArray;
    for (let i = 0; i < formArray.controls.length; i++) {
      formArray.controls[i].get(columnName).setValue(value.checked);
    }
  }

  /**
   * Click event of market option in Select Markets Dropdown
  */
  optionClick() {
      let newStatus = true;
      this.marketSelect.options.forEach((item: MatOption) => {
        if (!item.selected && item.value !== 0) {
          newStatus = false;
        }
      });
      this.allMarketSelected = newStatus;
      this.allMarketSelected ? this.marketSelect.options.first.select() : this.marketSelect.options.first.deselect();
   }

  bulkCreateMS() {
     if (this.selectedMultiMarket.length === 0 || (this.selectedMultiMarket.length === 1 && this.selectedMultiMarket[0] === 0)) {
        this.notificationService.error('Please select at least one market.');
        return;
      }
      const updateData = [];
      let temp = {};
      for (let i = 0; i < this.selectedMultiMarket.length; i++) {
        if (this.selectedMultiMarket[i] !== 0) {
          temp = {
            operationType: 'Create',
            parentItemId: this.technicalRequestItemId,
            relationName: EntityListConstant.TECHNICAL_REQUEST_MARKET_SCOPE_RELATION,//change this-make it as a constant
            template: null,
            item: {
              Properties: {
                LEAD_MARKET: false,
                TargetDPInWarehouse: null
              },
              R_PO_MARKET : {
                ItemId: this.selectedMultiMarket[i].ItemId,
              }
            }
          };
          updateData.push(temp);
        }
      }
      this.loaderService.show();
      this.entityService.bulkCreateEntitywithParent(updateData, null, null).subscribe((response) => {
        const items = response.changeLog.changeEventInfos;
        items.forEach((data) => {
          if (data.changeType !== 'Created') {
            return;
          }
          this.marketScopeFormArray.push(this.formBuilder.group({
            market: ['', [Validators.required, this.marketDuplicateValidator(this.selectedMarketOption)]],
            leadMarket: false,
            targetDP: ['',[Validators.required,Validators.pattern('^([1-9]|[1-4][0-9]|5[1-2])\/[0-9]{4}$')] ],
            itemId: data.item.Identity.ItemId,
          }));
           this.selectedMarketOption[this.marketScopeFormArray.length - 1] = this.markets.filter((market) => {

            return (data && data.item && data.item.R_PO_MARKET &&
              data.item.R_PO_MARKET.Identity && data.item.R_PO_MARKET.Identity.ItemId &&
              (data.item.R_PO_MARKET.Identity.ItemId === market.ItemId));
          })[0];
          //Adding from & todate for each market scope record.
          this.initialDate();
          this.fromTargetDate.push(this.fromDate);
          this.toTargetDate.push(this.toDate);

        });
        this.updateMultiMarkets();
        this.selectedMultiMarket = [];
        this.renderMSTableRows();
        this.loaderService.hide();
        this.allMarketSelected = false;
        //Update Project Name - NPD1-81
        this.selectedMarket=this.selectedMarketOption[0].displayName;
        this.updateProjectName();
      }, () => {
        this.loaderService.hide();
      });
  }

  bulkDeleteMS() {

    if (this.dynamicMarketScopeForm.value.marketScope.length === 0) {
      this.notificationService.error('Add at Least one Market Scope.');
      return;
    }
    if (this.selection.selected.length < 1) {
      this.notificationService.error('Select Market(s) to delete.');
      return;
    }
    this.loaderService.show();
    const formArray = this.dynamicMarketScopeForm.get('marketScope') as FormArray;
    const itemIds = [];
    for (let i = 0; i < this.selection.selected.length; i++) {
      itemIds.push(this.selection.selected[i].value.itemId);
    }
    const formRowsLenght = formArray.controls.length;
    this.entityService.deleteMultipleEntityObjects(itemIds, EntityListConstant.TECHNICAL_MARKET_SCOPE, this.utilService.APP_ID).subscribe(() => {
      for (let i = formRowsLenght - 1; i > -1; i--) {
        if (itemIds.indexOf(formArray.controls[i].value.itemId) !== -1) {
          this.monsterUtilService.removeFormProperty(formArray.controls[i].value.itemId);
          this.monsterUtilService.removeFormRelations(formArray.controls[i].value.itemId);
          this.selectedMarketOption.splice(i, 1);
          this.marketScopeFormArray.removeAt(i);
          //NPD1-83 Removing entries fromTargetDate & toTargetDate
          this.targetDPDate.splice(i,1);
          this.fromTargetDate.splice(i, 1);
          this.toTargetDate.splice(i, 1);

        }
      }
      this.selection = new SelectionModel<Element>(true, []);
      this.renderMSTableRows();
      this.updateMultiMarkets();
      this.loaderService.hide();
      //NPD1-81 Update prject name
      if(this.selectedMarketOption.length == 0){
        this.selectedMarket = "";
      }
      else{
        this.selectedMarket = this.selectedMarketOption[0].displayName;
      }
      this.updateProjectName();
      this.notificationService.success('Successfully completed the deletion process.');
    }, () => {
      this.notificationService.error('Error while deleting the market scope.');
    });
  }

   /**
   * To create the new technical request
   * @author Sindhuja
   * @param successCallback
   * @param errorCallback
   */
    createNewTechnicalRequestData(successCallback?, errorCallback?) {

      this.technicalRequestItemId = '';
      this.requestItemId = '';
      let requestTypeId;
      let status;
      this.loaderService.show();
      for (var i = 0; i < RequestDashboardControlConstant.newRequestType.options.length; i++) {
        if (RequestDashboardControlConstant.newRequestType.options[i].value == BusinessConstants.TECHNICAL_REQUEST) {
          requestTypeId = RequestDashboardControlConstant.newRequestType.options[i].itemId; //read it from URL params or send itemid from URL params
          break;
        }
      }
      status = this.monsterUtilService.getStatusDisplayName(BusinessConstants.REQUEST_STATUS_DRAFT);
      let createData = {
        Properties: {
          Bottler: '',
          Case_Pack_Size: '',
          Consumer_Unit_Size: '',
          Production_Site: '',
          Project_Name: '',
          Request_Rationale: '',
          Request_Status: status && status.VALUE,
          SKU_Details: '',
        },
        R_PO_REQUEST_TYPE : {
          ItemId : requestTypeId
        }
      };

      this.entityService.createEntitywithOutParent(EntityListConstant.REQUEST, createData,this.utilService.APP_ID).subscribe((response) => {
        this.technicalRequestItemId = response.package.itemId;
        this.requestId = response.package.itemId;
        this.retrieveRequest();
        this.loaderService.hide();
        if (successCallback) { successCallback(); }
      }, (error) => {
        this.loaderService.hide();
        if (errorCallback) { errorCallback(); }
        console.log('Error while creating a new technical request.');
      });
  }

  /**
   * To load technical request and set form group
   */
  retrieveRequest() {
    // to get request details
    this.loaderService.show();
    const filters = [];
    filters.push({
      'name': EntityListConstant.REQUEST_ENTITY_PARAMS.ID,
      'operator': 'eq',
      'value': this.requestId.split('.')[1]
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.REQUEST, filters, '', this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.REQUEST, filterParam, null, null, null, null, this.utilService.APP_ID)
      .subscribe((requestResponse) => {
        if (requestResponse && requestResponse.result &&
          requestResponse.result.items && requestResponse.result.items.length === 1) {
          const techReq = requestResponse.result.items[0];
          this.requestItemId = techReq.Identity.ItemId;
          this.mapRequestToForm(techReq);
        }
        this.loaderService.hide();
      }, (error) => {
        this.loaderService.hide();
      });
  }


  mapRequestToForm(request) {
      this.technicalRequestResponse.requestId = '';//request.Properties.RequestID,
      this.technicalRequestResponse.status = request.Properties.Request_Status;
      this.technicalRequestResponse.requestorName = request.CreatedBy$Properties.FullName;//this.sharingService.getCurrentUserDisplayName();
      this.technicalRequestResponse.currentCheckpoint = request.Properties && request.Properties.Current_Checkpoint ? request.Properties.Current_Checkpoint:0
      this.technicalRequestResponse.projectName =  request.Properties.Project_Name;
      this.showTechnicalRequestForm = true;
   }

  createTechnicalRequestFormGroups() {
    this.createNewTechnicalRequestData(() => {
      this.requestForm = new FormGroup({
        businessUnit: new FormControl({value:'',disabled: true}, [Validators.required]),
        bottler: new FormControl('', [Validators.required]),
        projectType: new FormControl(''),
        productionSite: new FormControl('' ),
        platform: new FormControl('', [Validators.required]),
        brand: new FormControl('', [Validators.required]),
        primaryPackagingType: new FormControl('', [Validators.required]),
        secondaryPackagingType: new FormControl('', [Validators.required,Validators.maxLength(64)]),
        casePackSize: new FormControl('', [Validators.required]),
        consumerUnitSize: new FormControl('', [Validators.required]),
        variant: new FormControl('', [Validators.required,]),
        skuDetails: new FormControl('', [Validators.required]),
        requestRationale: new FormControl('', [Validators.required,Validators.maxLength(2000)]),
        projectName : new FormControl('',[Validators.required])
      });

      this.dynamicMarketScopeForm = this.formBuilder.group({
        marketScope: new FormArray([])
      });

      this.msGridColumns = ['select', 'market', 'leadMarket', 'targetDP', 'actions'];
      this.showMarketScopeForm = true;
      this.loadFieldConfigurations();
    }, () => {

    });
  }

  /**
   * Hide and show logic of all elements
   */
  loadFieldConfigurations() {

    this.isPM = (this.currentActivity === BusinessConstants.TASK_ACTIVITY_PM) ? true : false;
    this.isE2EPM = (this.currentActivity === BusinessConstants.TASK_ACTIVITY_E2EPM) ? true : false;
    this.isReqRework = (this.currentActivity === BusinessConstants.TASK_ACTIVITY_REQUEST_REWORK) ? true : false;

    this.isRequestView = !this.taskId && !(this.isPM  || this.isE2EPM || this.isReqRework);
    const delegatedUser = (this.taskDetails && this.taskDetails[0] && this.taskDetails[0].Task &&
      this.taskDetails[0].Task.DelegatedToUser && this.taskDetails[0].Task.DelegatedToUser.__text) ?
      this.taskDetails[0].Task.DelegatedToUser.__text : null;
    const isEditable = (this.taskOwner === this.sharingService.getCurrentUserDN() || this.sharingService.getCurrentUserDN() === delegatedUser);
    this.canComplete = (this.canComplete && isEditable) ? true : false;

    this.requestFieldHandler.technicalRequest = {
        'projectName' : {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'businessUnit' : {
          'disabled': true ,//(this.isDraft) ? false : true,//add other conditions when review form is integrated
          'ngIf': true
        },
        'bottler': {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'projectType': {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'productionSite' : {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'platform': {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'brand' : {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'primaryPackagingType': {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'secondaryPackagingType' : {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'casePackSize': {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'consumerUnitSize' : {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'variant': {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'skuDetails' : {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'requestRationale': {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true
        },
        'commercialCategory': {
          'disabled': (this.canComplete && (this.isPM || this.isE2EPM)) ? false : true,
          'ngIf': (this.isPM || this.isE2EPM || (!this.isDraft && this.isRequestView)) ? true : false
        },
        'governanceApproach': {
          'disabled': (this.canComplete && (this.isPM)) ? false : true,
          'ngIf': (this.isPM || (!this.isDraft && this.isRequestView) ) ? true : false
        },
        'e2ePM': {
          'disabled': (this.canComplete && (this.isPM)) ? false : true,
          'ngIf': (this.isPM || (!this.isDraft && this.isRequestView)) ? true : false
        },
        'svpAligned': {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': (this.isE2EPM || (!this.isDraft && this.isRequestView)) ? true : false
        },
        'postLaunchAnaylsis' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': (this.isE2EPM || (!this.isDraft && this.isRequestView)) ? true : false
        },
        'requestedDate' : {
          'ngIf': this.isPM || this.isE2EPM || this.isReqRework || (!this.isDraft && this.isRequestView)//add for inprogress task also(Excpet for draft)
        }
      };
      this.requestFieldHandler.marketScope = {
        'market': {
          'disabled':  (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': true,
        },
        'leadMarket': {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework))? false : true,
          'ngIf': true,
        },
        'targetDP': {
          'disabled': (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework))  ? false : true,
          'ngIf': true
        },
        'Model': {
          'ngIf': true
        },
        'actions': {
          'ngIf': true
        }
      };
      this.requestFieldHandler.governanceMilestone = {
        'Model': {
          'ngIf': this.isPM || this.isE2EPM || this.isReqRework || (!this.isDraft && this.isRequestView)
        },
        'stage' : {
          'disabled': true,
          'ngIf': this.isPM || this.isE2EPM || this.isReqRework || (!this.isDraft && this.isRequestView)
        },
        'targetStartDate' : {
          'disabled': true,
          'ngIf': this.isPM || this.isE2EPM || this.isReqRework || (!this.isDraft && this.isRequestView)
        },
        'decisionDate' : {
          'disabled': true,
          'ngIf': this.isPM || this.isE2EPM || this.isReqRework || (!this.isDraft && this.isRequestView)
        },
        'decision' : {
          'disabled': true,
          'ngIf': this.isPM || this.isE2EPM || this.isReqRework || (!this.isDraft && this.isRequestView)
        },
        'comments' : {
          'disabled': (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
          'ngIf': this.isPM || this.isE2EPM || this.isReqRework || (!this.isDraft && this.isRequestView)
        },
      }
      this.requestFieldHandler.projectClassificationData =  {
        'Model':{
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'earlyProjectClassification' :  {
          'disabled': true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'earlyManufacturingSite' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'draftManufacturingLocation' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'newFg' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'newRnDFormula' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'newHBCFormula' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'earlyProjectClassificationDesc' : {
          'disabled': true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'newPrimaryPackaging' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'secondaryPackaging' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'registrationDossier' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'preProdReg' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'postProdReg' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'techDesc' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'corpPm' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },
        'opsPm' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },'registrationClassificationAvailable':{
          'disabled':true,
          'ngIf':this.isE2EPM || (!this.isDraft && this.isRequestView)
        },'registrationRequirement':{
          'disabled':true,
          'ngIf':this.isE2EPM || (!this.isDraft && this.isRequestView)
        },'leadFormula' : {
          'disabled': (this.canComplete && (this.isE2EPM)) ? false : true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        },'e2ePm' : {
          'disabled': true,
          'ngIf': this.isE2EPM || (!this.isDraft && this.isRequestView)
        }
      },
      this.requestFieldHandler.actionButtons = {
        discard: {
          ngIf: this.isDraft
        },
        saveAsDraft: {
          ngIf: this.isDraft
        },
        preview: {
          ngIf: false//this.isDraft
        },
        submit: {
          ngIf:  this.isDraft //false
        },
        approve: {
          ngIf: (this.canComplete && (this.isPM)) ? true : false,
        },
        deny: {
          ngIf: (this.canComplete && (this.isPM)) ? true : false,
        },
        resubmit: {
          ngIf: (this.canComplete && (this.isPM)) ? true : false,
        },
        convertToProject : {
          ngIf : (this.canComplete && (this.isE2EPM)) ? true : false,
        },
        complete : {
          ngIf : (this.canComplete && this.isReqRework) ? true : false,
        },
        backToHome: {
          ngIf : true,
          showConfirmation : (this.isDraft) || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? false : true,
        },
      };
      this.requestFieldHandler.other = {
        taskHeader : {
          ngIf : this.isPM || this.isE2EPM || this.isReqRework,
          value : (this.isPM && BusinessConstants.PM_HEADER ) || (this.isE2EPM && BusinessConstants.E2E_PM_HEADER )
                  || (this.isReqRework && BusinessConstants.REQ_REWORK )
        },
        autoSave : {
          value : this.isDraft || (this.canComplete && (this.isPM || this.isE2EPM || this.isReqRework)) ? true : false,
        }
      };
    }

    invokeServicesByTaskType() {
      this.loaderService.show();
      forkJoin([this.getCommercialCategorization(),this.getGovernanceApproaches(),this.getE2EPMUsers()]).subscribe(response => {
        if(this.technicalReq && this.technicalReq.R_PO_E2E_PM$Identity && this.technicalReq.R_PO_E2E_PM$Identity.ItemId ) {
          this.selectede2EPM = this.formE2EPMValue(this.technicalReq);
        }
        this.loaderService.hide();
      });
    }

    getE2EPMUsers(): Observable<any> {
      return new Observable(observer => {
        this.requestService.getUsersByRole().subscribe(response => {
          this.e2ePMUsers = response;
          observer.next(true);
          observer.complete();
        },(error)=> {
            observer.error(error);
            this.notificationService.error('Unable to get all the E2E PM.');
        });
      });
    }


    one2EPMSelection(formControl,formName) {
      if(this.selectede2EPM) {
        this.requestService.getUserByID(this.selectede2EPM.name).subscribe(response => {
          this.selectede2EPM.userIdentity = response.result.items[0];
          this.updateRequestRelationsInfo(formControl,formName);
        }, error => {
        });
      }
    }

    // NPD1-81 - Changed the the method for update project name based on Market-Platform-Variant -
    updateProjectName() {
        //update value through service call,notify
        this.showProjectName = true;
        let projectNameValues:string[];
        projectNameValues = [null,null,null];

        projectNameValues[0]= this.selectedMarket && this.selectedMarket.trim() ? this.selectedMarket : null;
        projectNameValues[1]= this.selectedPlatformName && this.selectedPlatformName.trim() ? this.selectedPlatformName : null;
        projectNameValues[2]= this.selectedVariantName && this.selectedVariantName.trim() ? this.selectedVariantName : null;

        this.projectName=null;
        if(projectNameValues[0] && projectNameValues[0].trim()){
          this.projectName= projectNameValues[0];
        }
        if(projectNameValues[1] && projectNameValues[1].trim()){
          if(this.projectName)
            this.projectName+= "-"+projectNameValues[1];
          else
            this.projectName=projectNameValues[1];
        }
        if(projectNameValues[2] && projectNameValues[2].trim()){
          if(this.projectName)
             this.projectName+= "-"+projectNameValues[2];
          else
            this.projectName=projectNameValues[2];
        }

        this.technicalRequestResponse.projectName = this.projectName;//this.editProjectName;fetch from a service
        this.requestForm.patchValue({
          projectName : this.projectName
        })

        if(this.selectedMarket && this.selectedMarket.trim()){
          this.updateRequestInfo('projectName','requestForm');
         }
        else{
          console.log("Not saving project name, since no market is selected")
        }
    }

    PreviewForm() {
       //add submit validations
       if (!this.validateRequest()) {
        return;
      }
      this.requestFieldHandler.technicalRequest.businessUnit.disabled = true;
      this.requestFieldHandler.marketScope.market.disabled = true;
      this.requestFieldHandler.actionButtons.preview.ngIf = false;
      this.requestFieldHandler.actionButtons.submit.ngIf = true;

      this.requestForm.disable();//this.form.controls['name'].disable();
      this.dynamicMarketScopeForm.disable();
      //this.requestFieldHandler.marketScope.leadMarket.disabled = true;//disables radio btn

    }

    onSubmit() {
       //add submit validations
       if (!this.validateRequest()) {
        return;
      }
      let notifyMessage ="Request submitted successfully ";
      if(this.technicalRequestResponse && this.technicalRequestResponse.requestId) {
        notifyMessage += '- '+ this.technicalRequestResponse.requestId
      }
      let status = this.monsterUtilService.getStatusDisplayName(BusinessConstants.REQUEST_STATUS_PROGRESS);
      this.updateRequestStatus(status && status.VALUE);
      this.monsterUtilService.finalBulkUpdateFormData(() => {
        this.notificationService.success(notifyMessage);
        this.navigateToDashboard();
       },()=>{
        this.notificationService.error('Something went wrong while submitting the request.');
      });
    }

    navigateToDashboard() {
      this.routerService.goToRequestDashboard(this.menu);
    }

    validateRequest(requestStatus?, taskAction?) {
      if(this.projectClassificationData){
        if(this.projectClassificationData.onValidate()){
          this.notificationService.error('Please fill in all the mandatory fields');
          return false;
        }
      }
      this.requestForm.markAllAsTouched();
      this.dynamicMarketScopeForm.markAllAsTouched();
      if (this.requestForm.invalid) {
         /*  if(this.requestForm.controls.projectName.invalid) {
          this.notificationService.error('Please Fill Project Name');
          return false;
        } */
        this.notificationService.error('Please fill in all the mandatory fields');
        return false;
      }
      if (this.dynamicMarketScopeForm.value.marketScope.length === 0) {
        this.notificationService.error('At least one market scope should be added.');
        return false;
      }
      let isLeadMarketSelected = false;//add validation for lead market region
      for (var i = 0; i < this.dynamicMarketScopeForm.value.marketScope.length; i++) {
        if ((this.dynamicMarketScopeForm.value.marketScope[i].leadMarket )) {
          isLeadMarketSelected = true;
        }
      }
      if(!isLeadMarketSelected) {
        this.notificationService.error('At least one Lead market should be selected');
        return false;
      }

      for (var i = 0; i < this.dynamicMarketScopeForm.value.marketScope.length; i++) {

          if (!(this.dynamicMarketScopeForm.value.marketScope[i].targetDP )) {
            this.notificationService.error('Please select Target DP IN WAREHOUSE');
            return false;
          }
      }
      // Validate project classification data
      return true;
    }

    validateBackButton(isDiscard) {
      let openConfirmationPopup=true;
      //NPD1-81 - Check whether atleast one market is added and one lead market is selected
      if(!isDiscard){
        let isLeadMarketSelected = false; let isMarketScopeAdded =false;
        //checing count of markets
        if(this.dynamicMarketScopeForm.value.marketScope.length != 0 ){
           isMarketScopeAdded = true;
        }
        //add validation for lead market region
        for (var i = 0; i < this.dynamicMarketScopeForm.value.marketScope.length; i++) {
          if ((this.dynamicMarketScopeForm.value.marketScope[i].leadMarket )) {
            isLeadMarketSelected = true;
          }
        }

        if(isMarketScopeAdded && isLeadMarketSelected){
          openConfirmationPopup=true;
        } else{
          openConfirmationPopup=false;
        }
      }
      return openConfirmationPopup;
    }

    cancelRequestCreation(isDiscard) {
      this.confirmCancel(isDiscard);
    }

    confirmCancel(isDiscard) {

      if(!isDiscard && !(this.requestFieldHandler.actionButtons.backToHome.showConfirmation)) {
        this.navigateToDashboard();
        return;
      }
      let openConfirmationPopup =this.validateBackButton(isDiscard)
      if(openConfirmationPopup){
          const dialogRef = this.dialog.open(ConfirmationModalComponent, {
            width: '40%',
            disableClose: true,
            data: {
              message: isDiscard? 'Are you sure you want to Discard the changes?':'Are you sure you want to go back?',
              submitButton: 'Yes',
              cancelButton: 'No'
            }
          });
          dialogRef.afterClosed().subscribe(result => {
            if (result && result.isTrue && isDiscard) {
              this.deleteRequest();
            }
            else if (result && result.isTrue && !isDiscard) {
              this.notificationService.info('Redirecting to Request Dashboard');
              this.navigateToDashboard();            }
          });
      }
      else {
        this.notificationService.warn('Select atleast one Market Scope.');
      }

    }

    deleteRequest() {
      this.loaderService.show();
      const itemIds = [];
      for (let i = 0; i < this.dynamicMarketScopeForm.value.marketScope.length; i++) {
        itemIds.push(this.dynamicMarketScopeForm.value.marketScope[i].itemId);
      }
      const deleteitems = [{
        entity_key: EntityListConstant.REQUEST,
        itemIds: [this.requestItemId]
      }];
      if (itemIds.length > 0) {
        deleteitems.push({
          entity_key: EntityListConstant.TECHNICAL_MARKET_SCOPE,
          itemIds: itemIds
        });
      }
      this.entityService.deleteMultipleDifferentEntityObjects(deleteitems, this.utilService.APP_ID).subscribe(() => {
        this.monsterUtilService.clearFormPropertiesandRelations();
        this.notificationService.success('Request was discarded successfully.');
        this.navigateToDashboard();
        this.loaderService.hide();
      }, () => {
        this.notificationService.error('Error while discarding draft.');
        this.loaderService.hide();
      });
    }

    storePanelHTML(event){
      this.panelHTML = event.srcElement || event.target;
    }

    handlePanelLeaveEvent(event){
      if (this.monsterUtilService.checkElementEventPosition(this.panelHTML, event) &&
         (this.requestForm && this.showTechnicalRequestForm) && (this.requestFieldHandler.other.autoSave.value)){
        let isLeadMarketSelected = false;
        let isMarketScopeAdded = false;

        //checing count of markets
        if(this.dynamicMarketScopeForm.value.marketScope.length != 0 ){
           isMarketScopeAdded = true;
        }
        //add validation for lead market region
        for (var i = 0; i < this.dynamicMarketScopeForm.value.marketScope.length; i++) {
          if ((this.dynamicMarketScopeForm.value.marketScope[i].leadMarket )) {
            isLeadMarketSelected = true;
          }
        }

        if(isMarketScopeAdded && isLeadMarketSelected) {
          this.updateSavedData();// Save as Draft, only if atleast one market is added
        } else {
          console.log("Not saving the request as Draft, Since no market scope is added.")
        }
      }
    }

    updateRequestStatus(status) {
      this.updateRequestInfo('status','requestForm',status);
    }

    onLeadMarketClick(event,index,leadMarket) {
      // console.log(event);
      for (let i = 0; i < this.marketScopeFormArray.length - 1; i++) {
          if (i != index) {
            this.marketScopeFormArray.controls[i].patchValue({leadMarket: false});
            this.updateRequestInfo('leadMarket','dynamicMarketScopeForm',false,i);
             //change all radio btn form controls to false
          }
      }
      if(event) {
        this.marketScopeFormArray.controls[index].patchValue({leadMarket: true}); //for radio btn selected to true-change lead market region value
        this.updateRequestInfo('leadMarket','dynamicMarketScopeForm',true,index);
      }
      let businessUnitItemId = this.selectedMarketOption[index] && this.selectedMarketOption[index].businessUnit && this.selectedMarketOption[index].businessUnit.ItemId;
      if(businessUnitItemId) {
        this.selectedBusinessUnit = this.businessUnits.find(b=>b.ItemId == businessUnitItemId)
        if(this.selectedBusinessUnit) {
          this.requestForm.patchValue({
            businessUnit : this.selectedBusinessUnit.displayName
          })
          this.updateRequestRelationsInfo('businessUnit','requestForm');
        }
      }
      //Updating Project Name - When Lead Market Radio Button is changed
      //NPD1-81
      this.selectedMarket = this.selectedMarketOption[index].displayName;
      this.updateProjectName();
    }

    checkLeadMarket(leadMarket,index) {
      if(leadMarket && leadMarket.value && leadMarket.value.leadMarket) {
        return true;
      } else if(this.isNewRequest && index == 0) {
         this.marketScopeFormArray.controls[index].patchValue({leadMarket: true});
         this.updateRequestInfo('leadMarket','dynamicMarketScopeForm',false,index);
         if(this.businessUnitInitialCount <= 0) {
          let businessUnitItemId = this.selectedMarketOption[index] && this.selectedMarketOption[index].businessUnit && this.selectedMarketOption[index].businessUnit.ItemId;
          if(businessUnitItemId) {
            this.selectedBusinessUnit = this.businessUnits.find(b=>b.ItemId == businessUnitItemId)
            if(this.selectedBusinessUnit) {
              this.requestForm.patchValue({
                businessUnit : this.selectedBusinessUnit.displayName
              })
              this.updateRequestRelationsInfo('businessUnit','requestForm');
            }
          }
          this.businessUnitInitialCount++;
         }
         return true;
      } else {
         return false;
      }

    }

    getMarkets() : Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllMarkets().subscribe((response) => {
          this.markets = this.monsterConfigApiService.transformResponse(response,"Markets-id",true,false,true);
          this.marketsCopy = this.markets.length > 0 ? this.copyObj(this.markets) : [];
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

    getBusinessUnits() : Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllBusinessUnits().subscribe((response) => {
          this.businessUnits = this.monsterConfigApiService.transformResponse(response,"Business_Unit-id",false);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

    getBrands() : Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllBrands().subscribe((response) => {
          this.brands = this.monsterConfigApiService.transformResponse(response,"Brands-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

    getProjectTypes() : Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllProjectTypes().subscribe((response) => {
          this.projectTypes = this.monsterConfigApiService.transformResponse(response,"Project_Type-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

    getPackagingTypes() : Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllPackagingTypes().subscribe((response) => {
          this.primaryPackagingTypes = this.monsterConfigApiService.transformResponse(response,"Packaging_Type-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

    getCommercialCategorization() : Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllCommercialCategorization().subscribe((response) => {
          this.commercialCategories = this.monsterConfigApiService.transformResponse(response,"Commercial_Categorisation-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

    getGovernanceApproaches() : Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllGovernanceApproaches().subscribe((response) => {
          this.governanceApproaches = this.monsterConfigApiService.transformResponse(response,"Governance_Approach-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

    //NPD1-81
    getCasePackSizes(): Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllCasePackSizes().subscribe((response) => {
          this.casePackSizes = this.monsterConfigApiService.transformResponse(response,"Case_Pack_Size-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

    getConsumerUnits(): Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllConsumerUnits().subscribe((response) => {
          this.consumerUnitSizes = this.monsterConfigApiService.transformResponse(response,"Consumer_Unit_Size-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }
/*
    getProductionSites(): Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllProductionSites().subscribe((response) => {
          this.productionSites = this.monsterConfigApiService.transformResponse(response,"Production_Site-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    } */

    getBottlers(): Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllBottlers().subscribe((response) => {
          this.bottlers = this.monsterConfigApiService.transformResponse(response,"Bottler-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

    getSKUDetails(): Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllSKUDetails().subscribe((response) => {
          this.skuDetails = this.monsterConfigApiService.transformResponse(response,"Sku_Details-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    }

  readUrlParams(callback) {
      this.activeRoute.queryParams.subscribe(queryParams => {
        if (this.activeRoute.parent.params && this.activeRoute.parent.params['value']
          && this.activeRoute.parent.params['value'].requestId) {
          this.activeRoute.parent.params.subscribe(parentRouteParams => {
            callback(parentRouteParams, queryParams);
          });
        } else {
          this.activeRoute.params.subscribe(routeParams => {
            callback(routeParams, queryParams);
          });
        }
      });
   }


  getPlatformsByBrand(parameters): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchPlatformsByBrand(parameters).subscribe(response => {
        this.platforms = this.monsterConfigApiService.transformResponse(response,'Platforms-id',true,true);
        observer.next(true);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
  }

  getVariantsByPlatform(parameters): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchVariantsByPlatform(parameters).subscribe(response => {
        this.variants = this.monsterConfigApiService.transformResponse(response,'Variant_SKU-id',true,true);
        observer.next(true);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
  }

  /**
   * To load form groups, if a user is opening the existing loc request record
   * @param null
   */
   loadTechnicalRequestFormGroups() {
    this.loaderService.show();
    const filters = [];
    filters.push({
      'name': EntityListConstant.REQUEST_ENTITY_PARAMS.ID,
      'operator': 'eq',
      'value': this.requestId.split('.')[1]
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.REQUEST, filters, '', this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.REQUEST, filterParam, null, null, null, null, this.utilService.APP_ID)
      .subscribe((technicalRequestResponse) => {
        if (technicalRequestResponse && technicalRequestResponse.result &&
          technicalRequestResponse.result.items && technicalRequestResponse.result.items.length === 1) {
          const techReq = technicalRequestResponse.result.items[0];
          this.technicalReq = techReq;
          this.technicalRequestItemId = techReq.Identity.ItemId;//Properties.RequestID for business req Id
          this.requestItemId = techReq.Identity.ItemId;
          this.technicalRequestResponse.requestId = techReq.Properties.RequestID;
          this.technicalRequestResponse.status = techReq.Properties.Request_Status;
          this.technicalRequestResponse.currentCheckpoint = techReq.Properties.Current_Checkpoint;
          this.technicalRequestResponse.requestorName = techReq.CreatedBy$Properties.FullName;
          this.technicalRequestResponse.projectName = techReq.Properties.Project_Name;
          this.technicalRequestResponse.requestedDate = techReq.Tracking.CreatedDate;
          //NPD1-81 Update Project Name
          let projectName:any[] ;
          if(techReq.Properties.Project_Name){
            projectName = techReq.Properties.Project_Name.split("-");
          }

          if(projectName && projectName.length == 1){
            this.selectedMarket=projectName[0];
          }else if(projectName && projectName.length == 2){
            this.selectedMarket=projectName[0];
            this.selectedPlatformName=projectName[1];
          }else if(projectName && projectName.length == 3){
            this.selectedMarket=projectName[0];
            this.selectedPlatformName=projectName[1];
            this.selectedPlatformName=projectName[2];
          }

          this.editProjectName = this.technicalRequestResponse.projectName;
          let status = this.monsterUtilService.getStatusDisplayName(BusinessConstants.REQUEST_STATUS_DRAFT);
          this.isDraft = (this.technicalRequestResponse.status === (status && status.VALUE)) ? true : false;
          this.isDraft = this.isDraft && this.technicalRequestResponse.requestorName.indexOf(this.sharingService.getCurrentUserCN()) >= 0;//check if the same requestor has opened the request
          this.loadFieldConfigurations();
          this.mapTechnicalRequestToForm(techReq);
          this.invokeServicesByTaskType();
          if(this.requestForm.controls.brand.value) {
            this.getDependentDropDownValues();
          }
          this.retrieveMarketScopeContents();
          //this.requestBusinessId = locReqItem.REQUEST_ID$Identity.Id;
        }  else {
          this.notificationService.error('No Technical Request found.')
        }
      }, error => {
        this.loaderService.hide();
      });
  }

  getDependentDropDownValues() {
    let brandRequestParams = {
      'Brands-id' : {
         Id : this.requestForm.controls.brand.value && this.requestForm.controls.brand.value.split('.')[1],
         ItemId : this.requestForm.controls.brand.value
      }
    }
    this.loaderService.show();
        this.getPlatformsByBrand(brandRequestParams).subscribe(response => {
          if(this.platforms.length > 0) {//&& this.requestForm.controls.variant.value
            let platformRequestParams = {
              'Platforms-id' : {
                Id : this.requestForm.controls.platform.value && this.requestForm.controls.platform.value.split('.')[1],
                ItemId : this.requestForm.controls.platform.value
              }
            }
            if(this.requestForm.controls.platform.value) {
              this.loaderService.show();
              this.getVariantsByPlatform(platformRequestParams).subscribe(response=> {
                this.loaderService.hide();
              },(error) => {
                this.loaderService.hide();
              });
            }
          }
      },(error) => {
        this.loaderService.hide();
      });
  }

  mapTechnicalRequestToForm(technicalRequest) {

    let businessUnitItemId = technicalRequest.R_PO_BUSINESS_UNIT$Identity && technicalRequest.R_PO_BUSINESS_UNIT$Identity.ItemId;
    if(businessUnitItemId) {
      this.selectedBusinessUnit = this.businessUnits.find(b=>b.ItemId == businessUnitItemId)
    }
    this.requestForm = new FormGroup({
        businessUnit: new FormControl({ value: this.selectedBusinessUnit && this.selectedBusinessUnit.displayName,
                      disabled: this.requestFieldHandler.technicalRequest.businessUnit.disabled },[Validators.required]),
        bottler: new FormControl({ value: technicalRequest.R_PO_BOTTLER$Identity && technicalRequest.R_PO_BOTTLER$Identity.ItemId,
                        disabled: this.requestFieldHandler.technicalRequest.bottler.disabled }, [Validators.required]),
        projectType: new FormControl({ value: technicalRequest.R_PO_PROJECT_TYPE$Identity && technicalRequest.R_PO_PROJECT_TYPE$Identity.ItemId,
                        disabled: this.requestFieldHandler.technicalRequest.projectType.disabled }),
        productionSite: new FormControl({ value: technicalRequest.R_PO_PRODUCTION_SITE$Identity && technicalRequest.R_PO_PRODUCTION_SITE$Identity.ItemId,
                        disabled: this.requestFieldHandler.technicalRequest.productionSite.disabled }), //,[Validators.maxLength(120)]
        platform: new FormControl({ value: technicalRequest.R_PO_PLATFORMS$Identity && technicalRequest.R_PO_PLATFORMS$Identity.ItemId,
                        disabled: this.requestFieldHandler.technicalRequest.platform.disabled }, [Validators.required]),
        brand:  new FormControl({ value: technicalRequest.R_PO_BRANDS$Identity && technicalRequest.R_PO_BRANDS$Identity.ItemId,
                      disabled: this.requestFieldHandler.technicalRequest.brand.disabled }, [Validators.required]),
        primaryPackagingType: new FormControl({ value: technicalRequest.R_PO_PACKAGING_TYPE$Identity && technicalRequest.R_PO_PACKAGING_TYPE$Identity.ItemId,
                      disabled: this.requestFieldHandler.technicalRequest.primaryPackagingType.disabled }, [Validators.required]),
        secondaryPackagingType: new FormControl({ value: technicalRequest.Properties.SecondaryPackagingType,
                                disabled: this.requestFieldHandler.technicalRequest.secondaryPackagingType.disabled }, [Validators.required,Validators.maxLength(64)]),

        casePackSize: new FormControl({ value: technicalRequest.R_PO_CASE_PACK_SIZE$Identity && technicalRequest.R_PO_CASE_PACK_SIZE$Identity.ItemId,
                      disabled: this.requestFieldHandler.technicalRequest.casePackSize.disabled }, [Validators.required]),
        consumerUnitSize: new FormControl({ value: technicalRequest.R_PO_CONSUMER_UNIT_SIZE$Identity && technicalRequest.R_PO_CONSUMER_UNIT_SIZE$Identity.ItemId,
                          disabled: this.requestFieldHandler.technicalRequest.consumerUnitSize.disabled }, [Validators.required]),
        variant: new FormControl({ value: technicalRequest.R_PO_VARIANT_SKU$Identity && technicalRequest.R_PO_VARIANT_SKU$Identity.ItemId,
                          disabled: this.requestFieldHandler.technicalRequest.variant.disabled }, [Validators.required]),
        skuDetails: new FormControl({ value: technicalRequest.R_PO_SKU_Details$Identity && technicalRequest.R_PO_SKU_Details$Identity.ItemId,
                      disabled: this.requestFieldHandler.technicalRequest.skuDetails.disabled }, [Validators.required]),
        requestRationale: new FormControl({ value: technicalRequest.Properties.Request_Rationale,
                      disabled: this.requestFieldHandler.technicalRequest.requestRationale.disabled }, [Validators.required,Validators.maxLength(2000)]),
        projectName : new FormControl({ value: technicalRequest.Properties.Project_Name,
                      disabled: this.requestFieldHandler.technicalRequest.projectName.disabled }, [Validators.required]),
    });
    if(this.isPM || this.isE2EPM || (!this.isDraft && this.isRequestView)) {
      this.requestForm.addControl('commercialCategory', new FormControl({ value: technicalRequest.R_PO_COMMERCIAL_CATEGORISATION$Identity && technicalRequest.R_PO_COMMERCIAL_CATEGORISATION$Identity.ItemId,
      disabled: this.requestFieldHandler.technicalRequest.commercialCategory.disabled },[Validators.required]));
    }
    if(this.isPM || (!this.isDraft && this.isRequestView)) {
      this.requestForm.addControl('governanceApproach',new FormControl({ value: technicalRequest.R_PO_GOVERNANCE_APPROACH$Identity && technicalRequest.R_PO_GOVERNANCE_APPROACH$Identity.ItemId,
      disabled: this.requestFieldHandler.technicalRequest.governanceApproach.disabled }));
      this.requestForm.addControl('e2ePM',new FormControl({ value: '',
        disabled: this.requestFieldHandler.technicalRequest.e2ePM.disabled },[Validators.required]));
    }
    if(this.isE2EPM || (!this.isDraft && this.isRequestView)) {

      this.requestForm.addControl('svpAligned',new FormControl({  value: technicalRequest.Properties.Is_SVP_Aligned,
        disabled: this.requestFieldHandler.technicalRequest.svpAligned.disabled }));
      this.requestForm.addControl('postLaunchAnaylsis',new FormControl({ value: technicalRequest.Properties.PostLaunchAnalysis,
          disabled: this.requestFieldHandler.technicalRequest.postLaunchAnaylsis.disabled }));
    }
    this.showTechnicalRequestForm = true;
  }

  formE2EPMValue(technicalRequest) {
    if (technicalRequest && technicalRequest.R_PO_E2E_PM$Identity && technicalRequest.R_PO_E2E_PM$Identity.ItemId && technicalRequest.R_PO_E2E_PM$Properties &&
      technicalRequest.R_PO_E2E_PM$Properties.UserId) {
      const userIdentity = {
        Identity: {
          ItemId: technicalRequest.R_PO_E2E_PM$Identity.ItemId,
        }
      };
      const selectedE2EPM = this.e2ePMUsers.find(element => element.name === technicalRequest.R_PO_E2E_PM$Properties.UserId);
      if (selectedE2EPM) {
        selectedE2EPM.userIdentity = userIdentity;
      }
      return (selectedE2EPM) ? selectedE2EPM : { userIdentity: userIdentity };
    }
    return {};
  }

  /**
   * To load market scope and set form groups
   */
  retrieveMarketScopeContents() {
    this.loaderService.show();
    this.loadingMarketScope = true;
    this.entityService.getEntityItemDetails(this.requestId,
      EntityListConstant.TECHNICAL_REQUEST_MARKET_SCOPE_RELATION).subscribe((marketScopeResponse) => {
        // create form groups for market scope
        // retrieveRequest
        if (marketScopeResponse && marketScopeResponse.items) {
          this.mapMarketScopeContentToForm(marketScopeResponse.items);
          this.loaderService.hide();
        }
        this.loadingMarketScope = false;
      }, (error) => {
        this.loaderService.hide();
        this.loadingMarketScope = false;
      });
  }

  mapMarketScopeContentToForm(marketScope) {
    this.dynamicMarketScopeForm = this.formBuilder.group({
      marketScope: new FormArray([])
    });
    this.msGridColumns = ['select', 'market', 'leadMarket', 'targetDP', 'actions'];
    this.showMarketScopeForm = true;
    for (let i = 0; i < marketScope.length; i++) {
      const options = {
        displayName: marketScope[i].R_PO_MARKET.Properties.DisplayName,
        value: marketScope[i].R_PO_MARKET.Properties.Value,
        ItemId: marketScope[i].R_PO_MARKET.Identity.ItemId,
        sequence: marketScope[i].R_PO_MARKET.Properties.Sequence,//to sort dropdown values based on it
        businessUnit : marketScope[i].R_PO_MARKET && marketScope[i].R_PO_MARKET.R_PO_BUSINESS_UNIT && marketScope[i].R_PO_MARKET.R_PO_BUSINESS_UNIT.Identity
      };
      this.selectedMarketOption.push(options);
      this.targetDPDate.push(marketScope[i].Properties.TargetDPInWarehouse);
      this.marketScopeFormArray.push(this.formBuilder.group({
        market: [this.selectedMarketOption[i]],
        leadMarket: [marketScope[i].Properties.LEAD_MARKET],
        targetDP: [{value:marketScope[i].Properties.TargetDPInWarehouse, disabled: this.requestFieldHandler.marketScope.targetDP.disabled}],
        itemId: [marketScope[i].Identity.ItemId]
      }));
      //NPD1-83
      if(marketScope[i].Properties.TargetDPInWarehouse && marketScope[i].Properties.TargetDPInWarehouse!==undefined){
        let week = marketScope[i].Properties.TargetDPInWarehouse.split("/")[0];
        let year = marketScope[i].Properties.TargetDPInWarehouse.split("/")[1];

        let weekdates:NgbDate[] = this.calculateDate(week,year);

        this.fromTargetDate[i] = weekdates[0];
        this.toTargetDate[i] = weekdates[1];
      }
      this.renderMSTableRows();

      //NPD1-83
      if(marketScope[i].Properties.TargetDPInWarehouse && marketScope[i].Properties.TargetDPInWarehouse!==undefined){
        let week = marketScope[i].Properties.TargetDPInWarehouse.split("/")[0];
        let year = marketScope[i].Properties.TargetDPInWarehouse.split("/")[1];

        let weekdates:NgbDate[] = this.calculateDate(week,year);

        this.fromTargetDate[i] = weekdates[0];
        this.toTargetDate[i] = weekdates[1];
      }

    }
    this.updateMultiMarkets();
   /*  setTimeout(() => {
      this.checkLocGridHeaderCheckBox();
    }, 500); */
  }

   /**
   * To retrieve task details
   */
    getTaskDetails(isConfigLoad?) {
      this.loaderService.show();
      this.inboxService.getTaskDetails(this.taskId).subscribe((response) => {
        this.taskDetails = response;
        this.currentActivity = this.taskDetails[0].Task.Activity;
        this.taskOwner = this.taskDetails[0].Task.Assignee.__text;
        this.taskStatus = this.taskDetails[0].Task.State;
        this.canComplete = this.taskDetails[0].Task.PossibleActions.hasOwnProperty('COMPLETE');
        //this.mapTaskConfig();
        this.getTaskInfo(isConfigLoad);
        if(!isConfigLoad) {
          this.loadTechnicalRequestFormGroups();
        } else {
          this.loadFieldConfigurations();
          this.updateFormDisability();
        }
        //this.loaderService.hide();
      }, (error) => {
        this.loaderService.hide();
      });
  }

  updateFormDisability() {

    this.monsterUtilService.disableFormControls(this.requestForm,this.requestFieldHandler.technicalRequest);
    if(this.projectClassificationData && this.projectClassificationData.projectClassificationDataForm) {
      this.monsterUtilService.disableFormControls(this.projectClassificationData.projectClassificationDataForm,this.requestFieldHandler.projectClassificationData);
    }
    if(this.marketScopeFormArray) {
      for(let i = 0; i< this.marketScopeFormArray.length ;i++ ) {
        this.monsterUtilService.disableFormControls(this.marketScopeFormArray.controls[i],this.requestFieldHandler.marketScope);
      }
    }
    if(this.governancemilestone && this.governancemilestone.stagesScopeFormArray) {
      for(let i = 0; i< this.governancemilestone.stagesScopeFormArray.length ;i++ ) {
        this.monsterUtilService.disableFormControls(this.governancemilestone.stagesScopeFormArray.controls[i],this.requestFieldHandler.governanceMilestone);
      }
    }
  }

  getTaskInfo(isConfigLoad) {
    const filters = [];
    filters.push({
        name: EntityListConstant.INBOX_PARENT_TASK_ID,
        operator: 'eq',
        value: this.taskDetails[0].Task.ParentTaskId
    });
    const filterParam = this.entityService.perpareFilterParameters(EntityListConstant.INBOX_ALL_TASK, filters, '', this.utilService.APP_ID);
    this.entityService.getEntityObjects(EntityListConstant.INBOX_ALL_TASK, filterParam, '', '', '', '', this.utilService.APP_ID).subscribe((response) => {
        this.showInboxControls = true;
        const requestObject = acronui.findObjectsByProp(response, 'items');
        this.inboxService.setSelectedTaskObject(requestObject[0]);
        this.taskOperations?.enableInboxOperationControls();
        isConfigLoad && this.loaderService.hide();
    }, () => {
        this.notificationService.error('Error while retrieving the inbox task details.');
    });
  }

  refreshTaskConfig(event) {
    this.getTaskDetails(true);
  }

  getPickerStyles(index){
    return `${ 70 + (90 + (index * 98.68) )}px`
  }


  //NPD1-83 Validation for TargetDP
  initialDate(){
    let fromDate = new Date();
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    this.fromDate = new NgbDate(
      fromDate.getFullYear(),
      Number((fromDate.getMonth() + 1).toString().padStart(2,'0')),
      Number((fromDate.getDate()).toString().padStart(2,'0')),
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    this.toDate = new NgbDate(
      toDate.getFullYear(),
      Number((toDate.getMonth() + 1).toString().padStart(2,'0')),
      Number((toDate.getDate()).toString().padStart(2,'0'))
    );
  }

  //NPD1-83
  calculateDate(week: number, year: number):NgbDate[] {

    let ngbFromDate,ngbToDate;
    // console.log("week:"+week);
    // console.log("year:"+year);
    const firstDay = new Date(year + "/1/4"); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(`${selectDate.year.toString()}-${selectDate.month.toString().padStart(2,'0')}-${selectDate.day.toString().padStart(2,'0')}`);
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate,ngbToDate];

  }

  //NPD1-83
  isDateValid(index){
    if(this.marketScopeFormArray.controls.length > 0 && index < this.marketScopeFormArray.controls.length &&
      this.marketScopeFormArray.controls[index].get('targetDP').touched )
    {
      if(this.marketScopeFormArray.controls[index].get('targetDP').value == undefined
                        || this.marketScopeFormArray.controls[index].get('targetDP').value.trim() === ""
                         || this.marketScopeFormArray.controls[index].get('targetDP').value == null)
      {
        return true;
      }
      else {
        return false;
      }
    }else{
      return false;
    }

  }
  //NPD1-83
  checkDatePattern(index){
    let isValid=true;

    if(this.marketScopeFormArray.controls.length > 0 && index < this.marketScopeFormArray.controls.length &&
      this.marketScopeFormArray.controls[index].get('targetDP').touched && this.marketScopeFormArray.controls[index].get('targetDP').value
      && this.marketScopeFormArray.controls[index].get('targetDP').value.trim().length > 0)
    {
      let regexp = new RegExp('^([1-9]|[1-4][0-9]|5[1-2])\/[0-9]{4}$');
      isValid = regexp.test(this.marketScopeFormArray.controls[index].get('targetDP').value);
    }
    return !isValid;
  }

  genericTaskComplete(notifyMessage,errorMessage,action,isSaveData,decision?,decisionDate?,comment?) {
    let requestObject = {
      requestId : this.technicalRequestResponse && this.technicalRequestResponse.requestId,
      projectName: this.technicalRequestResponse && this.technicalRequestResponse.projectName
    }
    this.updateGovernanceMilestoneObject(isSaveData,decision,comment,decisionDate);
      if(isSaveData) {
        this.monsterUtilService.finalBulkUpdateFormData(() => {
          this.loaderService.show();
          this.requestService.triggerActionFlow(action,requestObject,this.taskId).subscribe(response => {
            this.loaderService.hide();
            this.notificationService.success(notifyMessage);
            this.routerService.goToRequestDashboard(this.menu);
          },(error)=> {
            this.loaderService.hide();
            this.notificationService.error(errorMessage);
          });
        }, () => {
          this.loaderService.hide();
          this.notificationService.error('Something went wrong while updating details.');
      });
    } else {
      this.requestService.triggerActionFlow(action,requestObject,this.taskId).subscribe(response => {
        this.loaderService.hide();
        this.notificationService.success(notifyMessage);
        this.routerService.goToRequestDashboard(this.menu);
      },(error)=> {
        this.loaderService.hide();
        this.notificationService.error(errorMessage);
      });
    }

  }


  onApprove() {
    if (!this.validateRequest()) {
      return;
    }
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '50%',
      disableClose: true,
      data: {
        isCommentRequired : false,
        message: 'Please specify the reason for approval?',
        submitButton: 'Save',
        cancelButton: 'Cancel',
        hasConfirmationComment: true
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.isTrue) {
        this.loaderService.show();
        let successMessage = 'Task Approved succesfully';
        let errorMessage = 'Something went wrong while approving the task';//this.loaderService.show();
        let decision = this.stageDecisions.find(decision=> decision.value === 'Approved').value && this.stageDecisions.find(decision=> decision.value === 'Approved').value
        this.genericTaskComplete(successMessage,errorMessage,BusinessConstants.TASK_ACTION_APPROVED,true,decision,true,result.confirmationComment);
      }
    });
  }

  onRejected() {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '50%',
      disableClose: true,
      data: {
        //confirmationHeading : 'Resubmit',
        message: 'Please specify the reason for rejection?',
        submitButton: 'Save',
        cancelButton: 'Cancel',
        hasConfirmationComment: true,
        isCommentRequired : true,
        errorMessage : 'Please specify the reason for rejection'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.isTrue && result.confirmationComment) {
        this.loaderService.show();
        let successMessage = 'Task Rejected succesfully';
        let errorMessage = 'Something went wrong while rejecting the task';
        let decision = this.stageDecisions.find(decision=> decision.value === 'Stop').value&& this.stageDecisions.find(decision=> decision.value === 'Stop').value
        this.genericTaskComplete(successMessage,errorMessage,BusinessConstants.TASK_ACTION_REJECTED,true,decision,true,result.confirmationComment);
      }
    });

  }

  onResubmit() {//provide popup comment box & store it in CPO stage
    if (!this.validateRequest()) {
      return;
    }
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '50%',
      disableClose: true,
      data: {
        //confirmationHeading : 'Resubmit',
        message: 'Please specify the reason for requested change?',
        submitButton: 'Save',
        cancelButton: 'Cancel',
        hasConfirmationComment: true,
        isCommentRequired : true,
        errorMessage : 'Please specify the reason for requested change'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.isTrue && result.confirmationComment) {
        this.loaderService.show();
        let successMessage = 'Task Resubmitted succesfully';
        let errorMessage = 'Something went wrong while resubmitting the task';
        let decision = this.stageDecisions.find(decision=> decision.value === 'Recycle').value&& this.stageDecisions.find(decision=> decision.value === 'Recycle').value
        this.genericTaskComplete(successMessage,errorMessage,BusinessConstants.TASK_ACTION_RESUBMIT,true,decision,true,result.confirmationComment);
      }
    });
  }

  convertToProject() {
    if (!this.validateRequest()) {
      return;
    }
    let successMessage = 'Task converted to project succesfully';
    let errorMessage = 'Something went wrong while converting to project';
    this.genericTaskComplete(successMessage,errorMessage,BusinessConstants.TASK_ACTION_APPROVED,false);
  }

  completeTask() {
    if (!this.validateRequest()) {
      return;
    }
    let successMessage = 'Task completed succesfully';
    let errorMessage = 'Something went wrong while completing the task';
    this.genericTaskComplete(successMessage,errorMessage,'',false);
  }

  updateGovernanceMilestoneObject(isUpdateAction,decision?,comment?,decisionDate?) {
    if(isUpdateAction) {
      for (var i = 0; i < this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope.length; i++) {
        if(this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[i].stages == 'CP0') {
          if(decision) {
            this.updateRequestInfo('decision','dynamicGovernanceMilestoneForm',decision,i);
          }
          if(comment) {
           let commentValue =
             (this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[i].comments ? this.governancemilestone.dynamicGovernanceMilestoneForm.value.stagesScope[i].comments :'') + comment;
             this.updateRequestInfo('comments','dynamicGovernanceMilestoneForm',commentValue,i);
          }
          if(decisionDate) {
            this.updateRequestInfo('decisionDate','dynamicGovernanceMilestoneForm',new Date().toISOString(),i);
          }
        }
      }
    }
  }


  ngOnInit(): void {
    this.loaderService.show();
    forkJoin([this.getMarkets(),this.getBusinessUnits(),this.getBrands(),this.getPackagingTypes(),this.getProjectTypes()
        ,this.getCasePackSizes(),this.getConsumerUnits(),this.getBottlers(),this.getSKUDetails()])
    .subscribe(responseList => {
      this.readUrlParams((routeParams, queryParams) => {
       if (queryParams && routeParams) {
          this.requestId = routeParams.requestId;
          this.urlParams = queryParams;
          this.menu = this.urlParams.menu;
          if (this.urlParams && this.requestId) {
            this.isNewRequest = false;
            if (this.urlParams && this.urlParams.taskId) {
              this.taskId = this.urlParams.taskId;
              this.isDraft = false;
              this.getTaskDetails();
            }  else {
              //this.menu = this.getMenuBySubject('-1');
              this.canComplete = false;
              this.loadTechnicalRequestFormGroups();
            }
          } else {
            this.isNewRequest = true;
            this.canComplete = false;
            this.isDraft = true;
            this.createTechnicalRequestFormGroups();
          }
        }

      });
    }, () => {
      console.log('Error while loading the form configs.')
    });

    //check if this code is required & remove it
    let fromDate = new Date();
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    this.fromDate = new NgbDate(
      fromDate.getFullYear(),
      Number((fromDate.getMonth() + 1).toString().padStart(2,'0')),
      Number((fromDate.getDate()).toString().padStart(2,'0')),
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    this.toDate = new NgbDate(
      toDate.getFullYear(),
      Number((toDate.getMonth() + 1).toString().padStart(2,'0')),
      Number((toDate.getDate()).toString().padStart(2,'0'))
    );

  }

  ngOnDestroy() {
    this.monsterUtilService.clearFormPropertiesandRelations();
  }

}


/*

  mapTaskConfig() {
    this.taskObjectHandler = {
      Task: {
        State : this.taskDetails[0].Task.State,
        ParentTaskId : this.taskDetails[0].Task.ParentTaskId,
        TaskOwnerName : (this.taskDetails && this.taskDetails[0] && this.taskDetails[0].Task &&
          this.taskDetails[0].Task.Assignee && this.taskDetails[0].Task.Assignee.__text) ?
          this.taskDetails[0].Task.Assignee.__text : null,
        DelegatedTo : (this.taskDetails && this.taskDetails[0] && this.taskDetails[0].Task &&
          this.taskDetails[0].Task.DelegatedToUser && this.taskDetails[0].Task.DelegatedToUser.__text) ?
          this.taskDetails[0].Task.DelegatedToUser.__text : null
      }
    }
    this.showInboxControls = true;
  }

 /*  PLATFORM_DETAILS_METHOD_NS = 'http://schemas/NPDLookUpDomainModel/Platforms/operations';
  GET_PLATFORM_DETAILS_WS_METHOD_NAME = 'GetActivePlatform';

  VARIANT_DETAILS_METHOD_NS = 'http://schemas/NPDLookUpDomainModel/Variants/operations';
  GET_VARIANT_DETAILS_WS_METHOD_NAME = 'GetActiveVariants';

  getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
  } */

