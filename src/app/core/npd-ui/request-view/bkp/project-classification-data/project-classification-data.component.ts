import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { forkJoin,Observable } from 'rxjs';

import { AppService, NotificationService } from 'mpm-library';
import * as acronui from 'mpm-library';
import { RequestService } from '../../../services/request.service';
import { BusinessConstants } from '../../constants/BusinessConstants';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';

@Component({
  selector: 'app-project-classification-data',
  templateUrl: './project-classification-data.component.html',
  styleUrls: ['./project-classification-data.component.scss']
})
export class ProjectClassificationDataComponent implements OnInit {
 
 
 
  @Input() technicalRequestItemId;
  @Input() projectClassificationDataConfig;
  @Input() technicalRequest;

  projectClassificationDataForm: FormGroup;
  readonly radioButtonsList = BusinessConstants.radioButtons;


  requestId: any;
  selectedCorpPM;
  selectedOpsPM;
  corpPMUsers = [];
  opsPMUsers = [];

  earlyManufacturingSites = [];
  draftManufacturingLocations = [];

  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private notificationService: NotificationService,
    private requestService: RequestService,
    private monsterUtilService:MonsterUtilService,
    private monsterConfigApiService:MonsterConfigApiService
    ) { }
  
    get classificationDataForm() {
      return this.projectClassificationDataForm?.controls
    }
  ngOnInit(): void {
    if(this.technicalRequest && this.projectClassificationDataConfig) {
      this.createForm();
      this.getData();
    }
   
  }

  createForm(){
   this.projectClassificationDataForm = this.formBuilder.group({
      earlyProjectClassification: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.EarlyProjectClassification,disabled: this.projectClassificationDataConfig.earlyProjectClassification.disabled }],
      earlyManufacturingSite: [{ value: this.technicalRequest.R_PO_EARLY_MANUFACTURING_SITE$Identity &&
        this.technicalRequest.R_PO_EARLY_MANUFACTURING_SITE$Identity.ItemId,disabled: this.projectClassificationDataConfig.earlyManufacturingSite.disabled }, Validators.required],
      draftManufacturingLocation: [this.technicalRequest.R_PO_DRAFT_MANUFACTURING_LOCATION$Identity &&
        this.technicalRequest.R_PO_DRAFT_MANUFACTURING_LOCATION$Identity.ItemId, Validators.required],
      newFg: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.NewFGSAP,
        disabled: this.projectClassificationDataConfig.newFg.disabled }, Validators.required],
      newRnDFormula: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.NewRDFormula
        ,disabled: this.projectClassificationDataConfig.newRnDFormula.disabled }, Validators.required],
      newHBCFormula: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.NewHBCFormula
        ,disabled: this.projectClassificationDataConfig.newHBCFormula.disabled }, Validators.required],
      earlyProjectClassificationDesc: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.EarlyProjectClassificationDescription
        ,disabled: this.projectClassificationDataConfig.earlyProjectClassificationDesc.disabled }],
      newPrimaryPackaging: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.NewPrimaryPackaging
        ,disabled: this.projectClassificationDataConfig.newPrimaryPackaging.disabled }, Validators.required],
      secondaryPackaging: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.IsSecondaryPackaging
        ,disabled: this.projectClassificationDataConfig.secondaryPackaging.disabled }, Validators.required],
      registrationDossier: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.RegistrationDossier
        ,disabled: this.projectClassificationDataConfig.registrationDossier.disabled }],
      preProdReg: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.PreProductionRegistartion
        ,disabled: this.projectClassificationDataConfig.preProdReg.disabled }],
      postProdReg: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.PostProductionRegistartion
        ,disabled: this.projectClassificationDataConfig.postProdReg.disabled }],
      techDesc: [{ value: this.technicalRequest.Properties && this.technicalRequest.Properties.TechnicalDescription
        ,disabled: this.projectClassificationDataConfig.techDesc.disabled }],
      corpPm: [{ value: this.technicalRequest.R_PO_CORP_PM$Identity &&
        this.technicalRequest.R_PO_CORP_PM$Identity.ItemId
        ,disabled: this.projectClassificationDataConfig.corpPm.disabled }, Validators.required],
      opsPm: [{ value: this.technicalRequest.R_PO_OPS_PM$Identity &&
        this.technicalRequest.R_PO_OPS_PM$Identity.ItemId
        ,disabled: this.projectClassificationDataConfig.opsPm.disabled }, Validators.required]
    });
  }

  loadForm(){

  }
  
  getData(){
    forkJoin([this.getDraftManufacturingLocation(),this.getCorpPMUsers(),this.getOpsPMUsers()])
    .subscribe(responseList => {
      if(this.technicalRequest && this.technicalRequest.R_PO_CORP_PM$Identity && this.technicalRequest.R_PO_CORP_PM$Identity.ItemId ) {
        this.selectedCorpPM = this.formE2EPMValue(this.technicalRequest);
      }
      if(this.technicalRequest && this.technicalRequest.R_PO_OPS_PM$Identity && this.technicalRequest.R_PO_OPS_PM$Identity.ItemId ) {
        this.selectedOpsPM = this.formE2EPMOpsValue(this.technicalRequest);
      }
    }, () => {
      console.log('Error while loading the form configs.')
    });
  }

/*   getEarlyManufacturingSites() : Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllEarlyManufacturingSites().subscribe((response) => {
        this.earlyManufacturingSites = this.monsterConfigApiService.transformResponse(response,"Early_Manufacturing_Site-id",true); 
        observer.next(true);
        observer.complete();
      },(error)=> {
        observer.error(error);
      });
    });
  } */

  getDraftManufacturingLocation() : Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllDraftManufacturingLocations().subscribe((response) => {
        this.draftManufacturingLocations = this.monsterConfigApiService.transformResponse(response,"Draft_Manufacturing_Location-id",true); 
        observer.next(true);
        observer.complete();
      },(error)=> {
        observer.error(error);
      });
    });
  }


  getCorpPMUsers(): Observable<any> {
    return new Observable(observer => {
      this.requestService.getUsersByRoleForCorpPM().subscribe(response => {
        this.corpPMUsers = response;
        observer.next(true);
        observer.complete();
      },(error)=> {
          observer.error(error);
          this.notificationService.error('Unable to get all the CORP PM.');
      });
    });
  }

  getOpsPMUsers(): Observable<any> {
    return new Observable(observer => {
      this.requestService.getUsersByRoleForOpsPM().subscribe(response => {
        this.opsPMUsers = response;
        observer.next(true);
        observer.complete();
      },(error)=> {
          observer.error(error);
          this.notificationService.error('Unable to get all the CORP PM.');
      });
    });
  }

  // onCorpPMSelection() {
  //   if(this.projectClassificationDataForm.value.corpPm) {
  //     this.requestService.getUserByID(this.selectede2EPM.name).subscribe(response => {
  //       this.selectede2EPM.userIdentity = response.result.items[0];
  //       this.isRequestUpdate = true;
  //     }, error => {
  //     });
  //   }
  // }

  corpPMSelection(formControl,formName) {
    if(this.selectedCorpPM) {
      this.requestService.getUserByID(this.selectedCorpPM.name).subscribe(response => {
        this.selectedCorpPM.userIdentity = response.result.items[0];
        let value = this.selectedCorpPM && this.selectedCorpPM.userIdentity && this.selectedCorpPM.userIdentity.Identity && this.selectedCorpPM.userIdentity.Identity.ItemId
        this.updateRequestRelationsInfo(formControl,formName,value);
        //this.isRequestUpdate = true;
      }, error => {

      });
    }
  }
  opsPMSelection(formControl,formName) {
    if(this.selectedOpsPM) {
      this.requestService.getUserByID(this.selectedOpsPM.name).subscribe(response => {
        this.selectedOpsPM.userIdentity = response.result.items[0];
        let value = this.selectedOpsPM && this.selectedOpsPM.userIdentity && this.selectedOpsPM.userIdentity.Identity && this.selectedOpsPM.userIdentity.Identity.ItemId
        this.updateRequestRelationsInfo(formControl,formName,value);
        //this.isRequestUpdate = true;
      }, error => {

      });
    }
  }
  formE2EPMValue(technicalRequest) {
    if (technicalRequest && technicalRequest.R_PO_CORP_PM$Identity && technicalRequest.R_PO_CORP_PM$Identity.ItemId && technicalRequest.R_PO_CORP_PM$Properties &&
      technicalRequest.R_PO_CORP_PM$Properties.UserId) {
      const userIdentity = {
        Identity: {
          ItemId: technicalRequest.R_PO_CORP_PM$Identity.ItemId,
        }
      };
      const selectedE2EPM = this.corpPMUsers.find(element => element.name === technicalRequest.R_PO_CORP_PM$Properties.UserId);
      if (selectedE2EPM) {
        selectedE2EPM.userIdentity = userIdentity;
      }
      return (selectedE2EPM) ? selectedE2EPM : { userIdentity: userIdentity };
    } 
    return {};
  }
  formE2EPMOpsValue(technicalRequest) {
    if (technicalRequest && technicalRequest.R_PO_OPS_PM$Identity && technicalRequest.R_PO_OPS_PM$Identity.ItemId && technicalRequest.R_PO_OPS_PM$Properties &&
      technicalRequest.R_PO_OPS_PM$Properties.UserId) {
      const userIdentity = {
        Identity: {
          ItemId: technicalRequest.R_PO_OPS_PM$Identity.ItemId,
        }
      };
      const selectedE2EPM = this.opsPMUsers.find(element => element.name === technicalRequest.R_PO_OPS_PM$Properties.UserId);
      if (selectedE2EPM) {
        selectedE2EPM.userIdentity = userIdentity;
      }
      return (selectedE2EPM) ? selectedE2EPM : { userIdentity: userIdentity };
    } 
    return {};
  }
  onChangeTotal(){
    const newFg = this.projectClassificationDataForm.value.newFg === true ? 1000 : 0;
    const newRnDFormula = this.projectClassificationDataForm.value.newRnDFormula === true ? 10000 : 0;
    const newHBCFormula = this.projectClassificationDataForm.value.newHBCFormula === true ? 100000 : 0;
    const newPrimaryPackaging = this.projectClassificationDataForm.value.newPrimaryPackaging === true ? 1000000 : 0;
    const secondaryPackaging = this.projectClassificationDataForm.value.secondaryPackaging === true ? 10000000 : 0;
    const registrationDossier = this.projectClassificationDataForm.value.registrationDossier === true ? 100000000 : 0;
    const preProdReg = this.projectClassificationDataForm.value.preProdReg === true ? 1000000000 : 0;
    const postProdReg = this.projectClassificationDataForm.value.postProdReg === true ? 10000000000 : 0;

    return {
      formulaResult: newFg + newRnDFormula + newHBCFormula + newPrimaryPackaging + secondaryPackaging + registrationDossier + preProdReg + postProdReg,
      // lookUpResult: `${this.projectClassificationDataForm.value.earlyManufacturingSite}${this.projectClassificationDataForm.value.draftManufacturingLocation}`
    };

    
  }
  onValidate() {
    this.projectClassificationDataForm.markAllAsTouched();
    if(this.projectClassificationDataForm.invalid){
      return true;
    }else{
      return false;
    }
  }
  updateRequestInfo(formControl,formName,propertyValue?) {
    let property;
    let value;
    let itemId;

    if(formName === 'projectClassificationDataForm') {
      property = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
      value = propertyValue? propertyValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;
      itemId = this.technicalRequestItemId;   
    }
    this.monsterUtilService.setFormProperty(itemId, property, value);
  }

  getClassificationChange(){
    const totalScore = this.onChangeTotal();
    const desc = BusinessConstants.DESC_DATA;
    let data: any[] = [];
    data = BusinessConstants.TOTAL_SCORE.map((ele, i) => {
      return ele;
    });
    data = data.map((ele, i) => {
      ele = {
        totalScore: ele.totalScore,
        desc: BusinessConstants.DESC_DATA[i].desc,
        classificationNo: BusinessConstants.CLASSIFICATION_DATA[i].classificationNo
      };
      return ele;
    });
    let projectClassificationNo: Number;
    let projectClassificationDesc: String;
    data.forEach(ele => {
      if(ele.totalScore === totalScore.formulaResult){
        projectClassificationNo = ele.classificationNo;
        projectClassificationDesc = ele.desc;
      }
    });
    this.projectClassificationDataForm.patchValue({
      earlyProjectClassification: projectClassificationNo,
      earlyProjectClassificationDesc: projectClassificationDesc,
    })
  }
  updateRequestRelationsInfo(formControl,formName,objValue?) {
    let relationName;
    let objectValue;
    let itemId;

    if(formName === 'projectClassificationDataForm') {
      relationName = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
      objectValue = objValue? objValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;
      itemId = this.technicalRequestItemId;
    }
    this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
  } 
  
}
