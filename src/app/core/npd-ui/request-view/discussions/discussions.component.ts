import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoaderService } from 'mpm-library';

@Component({
  selector: 'app-discussions',
  templateUrl: './discussions.component.html',
  styleUrls: ['./discussions.component.scss']
})
export class DiscussionsComponent implements OnInit {

  requestId: number;
  isGroupFilter: boolean;
  constructor(
    private activatedRoute: ActivatedRoute,private loaderService:LoaderService
  ) {
  }

  readUrlParams(callback) {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.activatedRoute.parent.params.subscribe(parentRouteParams => {
        callback(parentRouteParams, queryParams);
      });
    });
  }
  
  ngOnInit(): void {
    this.loaderService.hide();
    this.requestId = null;
    this.readUrlParams((routeParams, queryParams) => {
      if (queryParams && routeParams) {
        this.requestId = routeParams.requestId && routeParams.requestId.split('.')[1];
        this.isGroupFilter = true;
      }
    });
  }

}
