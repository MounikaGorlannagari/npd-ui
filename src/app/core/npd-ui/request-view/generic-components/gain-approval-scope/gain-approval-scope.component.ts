import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../constants/BusinessConstants';

@Component({
  selector: 'app-gain-approval-scope',
  templateUrl: './gain-approval-scope.component.html',
  styleUrls: ['./gain-approval-scope.component.scss']
})
export class GainApprovalScopeComponent implements OnInit {

  @Input() gainApprovalScopeConfig;
  @Input() requestDetails;
  @Input() ccopiedRequestDetails;
  gainApprovalScopeForm: FormGroup;
  
  constructor(private formBuilder: FormBuilder,private monsterUtilService: MonsterUtilService) { }

  get formControls() { return this.gainApprovalScopeForm.controls;};

  onValidate() {
    this.gainApprovalScopeForm.markAllAsTouched();
    if(this.gainApprovalScopeForm.invalid) {
      return true;
    } else{
      return false;
    }
  }

  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(this.gainApprovalScopeForm,this.gainApprovalScopeConfig);
  }

  updateRequestInfo(formControl,formName,propertyValue?,index?) {
    let property;
    let value;
    let itemId;

    if(formName === 'gainApprovalScopeForm') {
      property = BusinessConstants.GAIN_APPROVAL_SCOPE_FORM[formControl];
      value = this.gainApprovalScopeForm.controls[formControl] && this.gainApprovalScopeForm.controls[formControl].value;
      itemId = this.requestDetails && this.requestDetails.requestItemId;
      this.monsterUtilService.setFormProperty(itemId, property, value);
    }
  }

  initializeForm() {
    /* this.gainApprovalScopeForm = this.formBuilder.group({
      projectIsAbout: [{ value: this.gainApprovalScopeConfig.projectIsAbout.value,
        disabled: this.gainApprovalScopeConfig.projectIsAbout.disabled },[Validators.maxLength(2000),Validators.required]],
      whyToDoThisProject: [{ value: this.gainApprovalScopeConfig.whyToDoThisProject.value,
        disabled: this.gainApprovalScopeConfig.whyToDoThisProject.disabled }, [Validators.maxLength(2000),Validators.required]],
    }); */
    this.gainApprovalScopeForm = this.formBuilder.group({});

    Object.keys(this.gainApprovalScopeConfig).filter(formControlConfigName => {
      if(formControlConfigName && this.gainApprovalScopeConfig[formControlConfigName].ngIf && 'formControl' in this.gainApprovalScopeConfig[formControlConfigName] && this.gainApprovalScopeConfig[formControlConfigName]['formControl'] === true)  {
        let validators = [];
        if(this.gainApprovalScopeConfig[formControlConfigName].validators) {
          if(this.gainApprovalScopeConfig[formControlConfigName].validators.required) {
            validators.push(Validators.required);
          }
          if(this.gainApprovalScopeConfig[formControlConfigName].validators.length && this.gainApprovalScopeConfig[formControlConfigName].validators.length.max) {
             validators.push(Validators.maxLength(this.gainApprovalScopeConfig[formControlConfigName].validators.length.max));
          }
        }
        this.gainApprovalScopeForm.addControl(formControlConfigName, new FormControl({ value: this.gainApprovalScopeConfig[formControlConfigName].value,
          disabled: this.gainApprovalScopeConfig[formControlConfigName].disabled },validators));
      }
    });
    // console.log(this.gainApprovalScopeConfig);
  }

  ngOnInit(): void {
    if(this.requestDetails && this.gainApprovalScopeConfig?.model?.ngIf) {
      this.initializeForm();
    }
  }

  ngOnChanges(){
    if(this.ccopiedRequestDetails){
      this.gainApprovalScopeForm.controls.projectIsAbout.setValue(this.ccopiedRequestDetails.result.items[0].Properties.ProjectAbout)
      this.gainApprovalScopeForm.controls.whyToDoThisProject.setValue(this.ccopiedRequestDetails.result.items[0].Properties.WhyProject)
    }
  }

}
