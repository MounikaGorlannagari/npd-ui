import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'mpm-library';
import { Observable } from 'rxjs';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../constants/BusinessConstants';

@Component({
  selector: 'app-programme-manager-reporting',
  templateUrl: './programme-manager-reporting.component.html',
  styleUrls: ['./programme-manager-reporting.component.scss']
})
export class ProgrammeManagerReportingComponent implements OnInit {
  //show;
  programmeMangaerReportingForm: FormGroup;
  @Input() requestDetails;
  @Input() programmeManagerReporting;
  @Input() reportingProjectType;
  reportingProjectSubType:any;
  reportingProjectDetail:any;
  deliveryQuarters:any;
  constructor(private formBuilder: FormBuilder,private monsterUtilService:MonsterUtilService,
    private monsterConfigApiService: MonsterConfigApiService,
    private loaderService: LoaderService) { }

  get formControls(){ return this.programmeMangaerReportingForm.controls; };

  onValidate() {
    this.programmeMangaerReportingForm.markAllAsTouched();
    if(this.programmeMangaerReportingForm.invalid) {
      return true;
    } else{
      return false;
    }
  }

  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(this.programmeMangaerReportingForm,this.programmeManagerReporting);
  }

  updateLinkedMarketsFormValue(value) {
    this.programmeMangaerReportingForm?.patchValue({
      linkedMarkets : value
    });
  }
  
  updateRequestInfo(formControl,formName,propertyValue?,index?) {
    let property;
    let value;
    let itemId;

    if(formName === 'programmeMangaerReportingForm') {
      property = BusinessConstants.PROGRAMME_MANAGER_REPORTING[formControl];
      value = this.programmeMangaerReportingForm.controls[formControl] && this.programmeMangaerReportingForm.controls[formControl].value;
      itemId = this.requestDetails && this.requestDetails.requestItemId;
      this.monsterUtilService.setFormProperty(itemId, property, value);
    }
  }

  updateRequestRelationsInfo(formControl,formName,index?) {
    let relationName;
    let objectValue;
    let itemId;

    if(formName === 'programmeMangaerReportingForm') {
      relationName = BusinessConstants.PROGRAMME_MANAGER_REPORTING[formControl];
      objectValue = this.programmeMangaerReportingForm.controls[formControl] && this.programmeMangaerReportingForm.controls[formControl].value;
      itemId = this.requestDetails && this.requestDetails.requestItemId;
      this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
    }
  }

  initializeForm() {
    this.programmeMangaerReportingForm = this.formBuilder.group({});
    Object.keys(this.programmeManagerReporting).filter(formControlConfigName => {
      if(formControlConfigName && this.programmeManagerReporting[formControlConfigName].ngIf && 'formControl' in this.programmeManagerReporting[formControlConfigName] && this.programmeManagerReporting[formControlConfigName]['formControl'] === true)  {
        let validators = [];
        if(this.programmeManagerReporting[formControlConfigName].validators) {
          if(this.programmeManagerReporting[formControlConfigName].validators.required) {
            validators.push(Validators.required);
          }
          if(this.programmeManagerReporting[formControlConfigName].validators.length && this.programmeManagerReporting[formControlConfigName].validators.length.max) {
             validators.push(Validators.maxLength(this.programmeManagerReporting[formControlConfigName].validators.length.max));
          }
        }
        if(formControlConfigName == 'projectType') {
           const projectTypeIndex = this.reportingProjectType.findIndex(ele => ele.Id == this.programmeManagerReporting[formControlConfigName].value)
          this.programmeMangaerReportingForm.addControl(formControlConfigName, new FormControl({ value: projectTypeIndex == -1 ? '' : this.reportingProjectType[projectTypeIndex].ItemId,
            disabled: this.programmeManagerReporting[formControlConfigName].disabled },validators));
        } else {
          this.programmeMangaerReportingForm.addControl(formControlConfigName, new FormControl({ value: this.programmeManagerReporting[formControlConfigName].value,
            disabled: this.programmeManagerReporting[formControlConfigName].disabled },validators));
        }
      }
    });
  }


  ngOnInit(): void {
    if(this.programmeManagerReporting) {
      this.initializeForm();
      this.getDependentDropDownValues();
      this.getAllDeliveryQuarters().subscribe();
    }

    //let userDetails = JSON.parse(sessionStorage.getItem('USER_DETAILS_WORKFLOW')).User.ManagerFor.Target;
    //this.show = userDetails.some(ele => ele.Name == "Programme Manager");
  }

  getDependentDropDownValues() {
    if(this.programmeMangaerReportingForm.controls.projectType?.value) {
      this.onProjectTypeChange(this.programmeMangaerReportingForm.controls.projectType.value);
      this.onProjectSubTypeChange(this.programmeMangaerReportingForm.controls.projectSubType.value);
    }
  }

  onProjectTypeChange(projectType){
    let projectTypeRequestParams = {
      'CP_Project_Type-id' : {
         Id : projectType && projectType.split('.')[1],
         ItemId : projectType
      }
    }

    if(projectType) {
      this.loaderService.show();
      this.getReportingProjectSubType(projectTypeRequestParams).subscribe(response=> {
        this.loaderService.hide();
      },(error) => {
        this.loaderService.hide();
      });
    }
  }

  onProjectSubTypeChange(projectSubType){
    let projectSubTypeRequestParams = {
      'Project_Sub_Type-id' : {
         Id : projectSubType && projectSubType.split('.')[1],
         ItemId : projectSubType
      }
    }

    if(projectSubType) {
      this.loaderService.show();
      this.getReportingProjectDetail(projectSubTypeRequestParams).subscribe(response=> {
        this.loaderService.hide();
      },(error) => {
        this.loaderService.hide();
      });
    }
  }

  getReportingProjectSubType(parameters) : Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchReportingProjectSubType(parameters).subscribe((response) => {
        this.reportingProjectSubType = this.monsterConfigApiService.transformResponse(response,"Project_Sub_Type-id",true); 
        observer.next(true);
        observer.complete();
      },(error)=> {
        observer.error(error);
      });
    });
  }

  getAllDeliveryQuarters() : Observable<any> {
    return new Observable(observer => {
      this.loaderService.show();
      this.monsterConfigApiService.getAllDeliveryQuarters().subscribe((response) => {
        this.deliveryQuarters = this.monsterConfigApiService.transformResponse(response,"PM_Delivery_Quarter-id",true); 
        this.loaderService.hide();
        observer.next(true);
        observer.complete();
      },(error)=> {
        this.loaderService.hide();
        observer.error(error);
      });
    });
  }


  getReportingProjectDetail(parameters) : Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchReportingProjectDetail(parameters).subscribe((response) => {
        this.reportingProjectDetail = this.monsterConfigApiService.transformResponse(response,"Project_Detail-id",true); 
        observer.next(true);
        observer.complete();
      },(error)=> {
        observer.error(error);
      });
    });
  }

  getProjectType(value){
    if(this.reportingProjectType){
      return this.reportingProjectType[this.reportingProjectType.findIndex(ele => ele.ItemId == value)]?.displayName;
    }
  }

  getProjectSubType(value){
    if(this.reportingProjectSubType){
      return this.reportingProjectSubType[this.reportingProjectSubType.findIndex(ele => ele.ItemId == value)]?.displayName;
    }
  }

  getProjectDetail(value){
    if(this.reportingProjectDetail){
      return this.reportingProjectDetail[this.reportingProjectDetail.findIndex(ele => ele.ItemId == value)]?.displayName;
    }
  }

  getProgMgrDeliveryQuarter(value){
    if(this.deliveryQuarters){
      return this.deliveryQuarters[this.deliveryQuarters.findIndex(ele => ele.ItemId == value)]?.displayName;
    }
  }
}
