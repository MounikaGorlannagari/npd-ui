import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoaderService, NotificationService, SharingService, UtilService } from 'mpm-library';
import { InboxAssignmentModalComponent } from '../../../request-management/requestor-dashboard/inbox/inbox-assignment-modal/inbox-assignment-modal.component';
import { InboxService } from '../../../request-management/requestor-dashboard/inbox/inbox.service';
import { BusinessConstants } from '../../constants/BusinessConstants';

@Component({
  selector: 'app-task-operations',
  templateUrl: './task-operations.component.html',
  styleUrls: ['./task-operations.component.scss']
})
export class TaskOperationsComponent implements OnInit {

  enableClaim: boolean;
  enableRevoke: boolean;
  enableAssign: boolean;
  enableDelegate: boolean;
  selectedTasksStates: any[];
  selectedTasksID: any[];
  selectedTasksAssignee: any[];
  selectedTasksDelegatedTo: any[];
  selectedTasksSubject: any[];
  selectedTasks: any;
  APP_ID;
  @Input() taskObjectHandler;
  @Input() showInboxControls;
  @Output() refreshHandler = new EventEmitter<any>();

    constructor(
      private loaderService: LoaderService,
      private notificationService: NotificationService,
      private sharingService: SharingService,
      private sharingservice: SharingService,
      private inboxService: InboxService,
      private utilService: UtilService,
      private dialog: MatDialog
  ) {}

   disableAllControls() {
      this.enableClaim = false;
      this.enableRevoke = false;
      this.enableAssign = false;
      this.enableDelegate = false;
  }

  enableInboxOperationControls() {
      this.taskObjectHandler = this.inboxService.getSelectedTaskObject();
      this.selectedTasksStates = [];
      this.selectedTasksID = [];
      this.selectedTasksAssignee = [];
      this.selectedTasksDelegatedTo = [];
      this.selectedTasks = [];
      this.selectedTasksSubject = [];
      this.selectedTasks.push(this.taskObjectHandler);
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.selectedTasks.length; i++) {
        this.selectedTasksStates.push(this.selectedTasks[i].Task.State);
        this.selectedTasksID.push(this.selectedTasks[i].Task.ParentTaskId);
        this.selectedTasksAssignee.push(this.selectedTasks[i].Task.TaskOwnerName);
        this.selectedTasksDelegatedTo.push(this.selectedTasks[i].Task.DelegatedTo);
        this.selectedTasksSubject.push(this.selectedTasks[i].Task.Subject);
      }

      const containsDelegatedTasks = this.selectedTasksDelegatedTo.some((el) => {
        return el !== null;
      });
 
      // If any one Task Status is CREATED or If it in collaboration then disable all the actions
      if (this.selectedTasksStates.indexOf('1') !== -1) {
          this.enableClaim = true;
          this.enableRevoke = false;
          this.enableAssign = true;
          this.enableDelegate = false;
      }
      // If any one Task Status is CREATED or If it in collaboration then disable all the actions
      if (this.selectedTasksStates.indexOf('5') !== -1) {
          this.disableAllControls();
      } else if (this.selectedTasksStates.indexOf('2') !== -1 && this.selectedTasksStates.indexOf('1') !== -1) {
          this.disableAllControls();
      } else {
          // Check if all Task are assigned to same user
          if (this.selectedTasksStates.indexOf('2') !== -1) {
              const arryString = this.selectedTasksStates.toString();
              // tslint:disable-next-line: no-shadowed-variable
              const regExp = new RegExp('2', 'g');
              const StatusData = arryString.match(regExp);
              if ((StatusData != null) && (StatusData.length === this.selectedTasksStates.length)) {
                  const arrayUserString = this.selectedTasksAssignee.toString();
                  const regExp1 = new RegExp(this.sharingService.getCurrentUserDisplayName(), 'g');
                  const AssigneeData = arrayUserString.match(regExp1);
                  // Check if The task belong to him or not
                  if (((AssigneeData != null) && (AssigneeData.length === this.selectedTasksAssignee.length))) { // || (this.isNotificationAdmin)
                      this.enableClaim = false;
                      this.enableAssign = false;
                      this.enableRevoke = true;
                      
                      if (containsDelegatedTasks) {
                          this.enableDelegate = false;
                      } else {
                          this.enableDelegate = true;
                      }

                  } else {
                      this.disableAllControls();
                  }
              }
          }
          // Check all Task for CREATED state
          else {
              const arryStateString = this.selectedTasksStates.toString();
              const regExp = new RegExp('1', 'g');
              const TaskStatusData = arryStateString.match(regExp);
              // Check if all the selected task are in CREATED state
              if ((TaskStatusData != null) && (TaskStatusData.length === this.selectedTasksStates.length)) {
                  this.enableClaim = true;
                  this.enableRevoke = false;
                  // Enable assign button if he is Notification admin
                  /* if (this.isNotificationAdmin) {
                      this.enableAssign = true;
                  } else {
                      this.enableAssign = false;
                  } */
                  this.enableAssign = true;
                  this.enableDelegate = false;
              } else {
                  this.disableAllControls();
              }
          }
          const containsNonRevokableTasks = this.selectedTasksSubject.some((el) => {
            return el && (el == BusinessConstants.TASK_ACTIVITY_E2EPM || el == BusinessConstants.TASK_ACTIVITY_REQUEST_REWORK) ;
          });       
          if(containsNonRevokableTasks) {
              this.enableRevoke = false;
          }
      }
  }


  claim() {
      const taskToSend = [];
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.selectedTasks.length; i++) {
          let inboxOperationTaskObject: any = this.inboxService.getTaskObjStructure();
          inboxOperationTaskObject.taskId = this.selectedTasks[i].Task.ParentTaskId; 
          inboxOperationTaskObject.taskData.data.operation = '';
          inboxOperationTaskObject.customData.data.applicationId = '';
          taskToSend.push(inboxOperationTaskObject);
      }
      this.loaderService.show();
      this.inboxService.claim(this.APP_ID,  this.sharingservice.currentUserDN, taskToSend, (response) => {
          this.disableAllControls();
          if (response != null) {
              setTimeout(() => {
                  this.notificationService.success('Task(s) claimed successfully');
                  this.refreshHandler.emit('');
              }, 3000)
          } else {
              this.loaderService.hide();
              this.notificationService.error('Error while claiming the task(s). Please try again.');
          }
      });
  }

  revoke() {
      const taskToSend = [];
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.selectedTasks.length; i++) {
        let inboxOperationTaskObject: any = this.inboxService.getTaskObjStructure();
        inboxOperationTaskObject.taskId = this.selectedTasks[i].Task.ParentTaskId; 
          inboxOperationTaskObject.taskData.data.operation = '';
          inboxOperationTaskObject.customData.data.applicationId = '';
          taskToSend.push(inboxOperationTaskObject);
      }
      this.loaderService.show();
      this.inboxService.revoke(this.APP_ID, taskToSend, (response) => {
          this.disableAllControls();
          if (response != null) {
              setTimeout(() => {
                  this.notificationService.success('User is revoked from the Task(s)');
                  this.refreshHandler.emit('');
                }, 3000)

          } else {
              this.loaderService.hide();
              this.notificationService.error('Error while revoking the Task(s). Please try again.');
          }
      });
  }

  userAssignment(operation: string) {
      const dialogRef = this.dialog.open(InboxAssignmentModalComponent, {
          // tslint:disable-next-line: object-literal-shorthand
          width: '25%',
          //height: '35%',
          data: { tasks: this.selectedTasksID, operation: operation }
      });

      dialogRef.afterClosed().subscribe(result => {
          if (result.userId === '') {
              return;
          }
          const taskToSend = [];
          const selectedTasksAssignees = [];
          // tslint:disable-next-line: prefer-for-of
          for (let i = 0; i < this.selectedTasks.length; i++) {
              let inboxOperationTaskObject: any = this.inboxService.getTaskObjStructure();
              inboxOperationTaskObject.taskId = this.selectedTasks[i].Task.ParentTaskId;
              inboxOperationTaskObject.taskData.data.operation = '';
              inboxOperationTaskObject.customData.data.applicationId = this.APP_ID;
              inboxOperationTaskObject.customData.data.reviewState =
                this.selectedTasks[i].Task.Subject ? this.selectedTasks[i].Task.Subject.split(' ')[0].toUpperCase() : '';
              inboxOperationTaskObject.customData.data.requestId = this.selectedTasks[i].Identity.ItemId;
              taskToSend.push(inboxOperationTaskObject);
              selectedTasksAssignees.push(this.selectedTasks[i].RT_TASK_OWNER_NAME);
          }
          if (result.operation === 'Assign') {
              this.loaderService.show();
              this.inboxService.assign(this.APP_ID, result.userId, taskToSend, (response) => {
                  this.disableAllControls();
                  if (response === null) {
                      this.loaderService.hide();
                      this.notificationService.error('Error while assigning the user. Please try again.');
                  }
                  else {
                      setTimeout(() => {
                          //this.notificationService.success('Task(s) is delegated to the User');
                          this.notificationService.success('User is assigned for the task(s)');
                          this.refreshHandler.emit('');
                      }, 3000)
                      // this.notificationService.success('User is assigned for the task(s)');

                  }
              });
          }
          else {
              this.loaderService.show();
              this.inboxService.delegate(this.APP_ID, result.userId, result.transferOwnership, taskToSend, (response) => {
                  this.disableAllControls();
                  if (response === null) {
                      this.loaderService.hide();
                      this.notificationService.error('Error while delegating the user. Please try again.');
                  }
                  else {
                      setTimeout(() => {
                          this.notificationService.success('Task(s) is delegated to the User');
                          // this.notificationService.success('User is delegated for the task(s)');
                          this.refreshHandler.emit('');
                      }, 3000)

                  }
              });
          }
      });
  }

  ngOnInit(): void {
    this.APP_ID = this.utilService.APP_ID;
    if(this.showInboxControls) {//&& this.taskObjectHandler
      this.enableInboxOperationControls();
    }
  }

}
