import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTable } from '@angular/material/table';
import { LoaderService } from 'mpm-library';
import { Observable } from 'rxjs';
import { EntityService } from '../../../services/entity.service';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../constants/BusinessConstants';
import { EntityListConstant } from '../../constants/EntityListConstant';

@Component({
  selector: 'app-deliverables-vs-thresholds',
  templateUrl: './deliverables-vs-thresholds.component.html',
  styleUrls: ['./deliverables-vs-thresholds.component.scss']
})
export class DeliverablesVsThresholdsComponent implements OnInit {
  deliverablesForm: FormGroup;
  dGridColumns = [];
  dataSource = [];
  thresholdValuesForMarketBrand: any;
  thresholdParams:any = {
    leadMarket: null,
    brand: null,
};
deliverablesCopyData:any
thresholdParamsMonsterGreen:any = {
  leadMarket: null,
  bottler: null,
  brand: 32769,
  platform: 32769,
  variant: 32769,
};
  @Input() properties;
  @Input() requestDetails;
  @Input() deliverableThresholdData;
  @Input() deliverableThresholdDataConfig;
  @Output() updateDeliverablesThresholdValues = new EventEmitter<any>();
  @ViewChild('stagesScopeTable') stagesScopeTable: MatTable<any>;
  constructor(private formBuilder: FormBuilder,
    private monsterUtilService:MonsterUtilService,
    private entityService: EntityService,
    private loaderService: LoaderService,
    private monsterConfigApiService: MonsterConfigApiService) { }
  get formControls() { return this.deliverablesForm.controls; }
  get deliverablesRowFormArray() { return this.formControls.deliverablesRow as FormArray; }

  ngOnInit(): void {
    this.thresholdParamsMonsterGreen = {
      leadMarket: null,
      bottler: null,
      brand: JSON.parse(sessionStorage.getItem('ALL_BRANDS')).find(ele => ele['DisplayName'] == 'Monster')['Brands-id'].Id,
      platform: JSON.parse(sessionStorage.getItem('ALL_BRANDS')).find(ele => ele['DisplayName'] == 'Monster')['Brands-id'].Id,
      variant: JSON.parse(sessionStorage.getItem('ALL_BRANDS')).find(ele => ele['DisplayName'] == 'Monster')['Brands-id'].Id,
    };
    this.getDataSource();

    if(this.requestDetails && this.deliverableThresholdData){
      this.retrieveDeliverableThresholdContents();
    }else{
      this.onload();
    }
    // this.getAllThresholdValues();
    // this.getAllThresholdValuesMinimumThreshold();
  }

  getAllThresholdValues(data){
    this.thresholdParamsMonsterGreen[data.property] = +data.value;
    const flag = ['leadMarket','brand','platform','variant'].every(ele => this.thresholdParamsMonsterGreen[ele]);
    // const thresholdParamsMonsterGreen = {
    //   leadMarket: 2,
    //   brand: 49154,
    //   platform: 49153,
    //   bottler: 1,
    // }
    if(flag) {
      this.getAllThresholdValuesForMarketBrand(this.thresholdParamsMonsterGreen).subscribe(response =>{
        response;
      }, () => {
        console.log('Error while loading the form configs.')
      });
    } else {
      this.dataSource[2] = {
        threshold: 'Monster Green', nSVPerCase: 0, gM: 0,cogs: 0
      };
      this.updateRequestInfo('nSVPerCase','deliverablesRowFormArray',2);
      this.updateRequestInfo('gM','deliverablesRowFormArray',2);
      this.updateRequestInfo('cogs','deliverablesRowFormArray',2);
    }
  }
  getAllThresholdValuesMinimumThreshold(data){
    this.thresholdParams[data.property] = +data.value;
    
    const flag = ['leadMarket','brand'].every(ele => this.thresholdParams[ele]);
    // const thresholdParams = {
    //   leadMarket: 2,
    //   brand: 32769,
    //   platform: 32769,
    //   bottler: 1,
    // }
    if(flag){
      this.getAllThresholdValuesForMarketBrandMinimumThreshold(this.thresholdParams).subscribe(response =>{
        response;
      }, () => {
        console.log('Error while loading the form configs.')
      });
    } else {
      this.dataSource[1] = {
        threshold: 'Minimum Threshold', nSVPerCase: 0, gM: 0,cogs: 0
      }
      // console.log('getAllThresholdValuesMinimumThreshold');
      // console.log(this.deliverablesForm?.value?.deliverablesRow[1]?.itemId);

      //console.log(this.dataSource);
      setTimeout(()=> {
        this.updateRequestInfo('nSVPerCase','deliverablesRowFormArray',1);
        this.updateRequestInfo('gM','deliverablesRowFormArray',1);
        this.updateRequestInfo('cogs','deliverablesRowFormArray',1);
      },2000);
    
    }
  }
  getAllThresholdValuesForMarketBrand(parameters) : Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchAllThresholdValuesForMarketBrand(parameters).subscribe((response) => {
        this.thresholdValuesForMarketBrand = response; 
        if(this.thresholdValuesForMarketBrand.length > 0){
          this.thresholdValuesForMarketBrand.forEach(item => {
            this.dataSource[2] = {
              threshold: 'Monster Green', nSVPerCase: isNaN(item.NSV) ? 0 : item.NSV, gM: isNaN(item.GPPercent) ? 0 : parseFloat(item.GPPercent).toFixed(3),cogs: isNaN(item.COG) ? 0 : item.COG
            };
            this.updateRequestInfo('nSVPerCase','deliverablesRowFormArray',2);
            this.updateRequestInfo('gM','deliverablesRowFormArray',2);
            this.updateRequestInfo('cogs','deliverablesRowFormArray',2);
          });
        } else {
          this.dataSource[2] = {
            threshold: 'Monster Green', nSVPerCase: 0, gM: 0,cogs: 0
          };
          this.updateRequestInfo('nSVPerCase','deliverablesRowFormArray',2);
          this.updateRequestInfo('gM','deliverablesRowFormArray',2);
          this.updateRequestInfo('cogs','deliverablesRowFormArray',2);
        }
        this.renderMSTableRows();
        observer.next(true);
        observer.complete();
      },(error)=> {
        observer.error(error);
      });
    });
  }
  getAllThresholdValuesForMarketBrandMinimumThreshold(parameters) : Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchAllThresholdValuesForMarketBrand(parameters).subscribe((response) => {
        this.thresholdValuesForMarketBrand = response; 
        if(this.thresholdValuesForMarketBrand.length > 0){
          this.thresholdValuesForMarketBrand.forEach(item => {
            this.dataSource[1] = {
              threshold: 'Minimum Threshold', nSVPerCase: isNaN(item.NSV) ? 0 : item.NSV , gM: isNaN(item.GPPercent) ? 0 : parseFloat(item.GPPercent).toFixed(3),cogs: isNaN(item.COG) ? 0 : item.COG
            }
            this.updateRequestInfo('nSVPerCase','deliverablesRowFormArray',1);
            this.updateRequestInfo('gM','deliverablesRowFormArray',1);
            this.updateRequestInfo('cogs','deliverablesRowFormArray',1);
          });
        }else {
          this.dataSource[1] = {
            threshold: 'Minimum Threshold', nSVPerCase: 0, gM: 0,cogs: 0
          }
          this.updateRequestInfo('nSVPerCase','deliverablesRowFormArray',1);
          this.updateRequestInfo('gM','deliverablesRowFormArray',1);
          this.updateRequestInfo('cogs','deliverablesRowFormArray',1);
        }
        this.renderMSTableRows();
        observer.next(true);
        observer.complete();
      },(error)=> {
        observer.error(error);
      });
    });
  }
  getDataSource(){
    this.dGridColumns = ['threshold','nSVPerCase', 'gM', 'cogs', 'volume','ros','distribution'];

    this.deliverablesForm = this.formBuilder.group({
      deliverablesRow: new FormArray([])
    });
     
    this.dataSource[0] = {
      threshold: 'This Project', nSVPerCase: '0', gM: '0',cogs: '0',volume: '0'
    }
    this.dataSource[1] = {
      threshold: 'Minimum Threshold', nSVPerCase: '0', gM: '0',cogs: '0'
    }
    this.dataSource[2] = {
      threshold: 'Monster Green', nSVPerCase: '0', gM: '0',cogs: '0'
    }
  }

  onload(isCopyRequest?){
    if(isCopyRequest){
      // this.deliverablesRowFormArray.setValue(null)
      // console.log('this.deliverablesRowFormArray',this.deliverablesRowFormArray);
      while (this.deliverablesRowFormArray.length !== 0) {
        this.deliverablesRowFormArray.removeAt(0)
      }
    }
    const updateData = [];
    let temp = {};
    for (let i = 0; i < this.dataSource.length; i++) {
      if (this.dataSource[i] !== 0) {
        temp = {
          operationType: 'Create',
          parentItemId: this.requestDetails.requestItemId,
          relationName: EntityListConstant[this.properties.requestDeliverablesRelation],
          template: null,
          item: {
            Properties: {
              COGS: this.dataSource[i].cogs,
              Distribution: null,
              GM: this.dataSource[i].gM,
              NSVPerCase: this.dataSource[i].nSVPerCase,
              ROS: null,
              ThresholdType: this.dataSource[i].threshold,
              // Volume: ,
            },
          }
        };

        
      }
      updateData.push(temp);
    }
    this.loaderService.show();
    // console.log(updateData);
    // if(this.requestDetails.requestItemId.length>0){
    this.entityService.bulkCreateEntitywithParent(updateData, null, null).subscribe((response) => {
      let items = response.changeLog.changeEventInfos;

      items=items.filter(data=>{
        return  (data.changeType == 'Created') 
      })
      // console.log(items);
      // console.log(this.deliverablesCopyData);
      if(isCopyRequest){
       
            this.deliverablesCopyData.forEach(ed=>{
          for (let index = 0; index < (items.length); index++) {
            if(items[index].item.Properties.ThresholdType == ed.Properties.ThresholdType){
            // items[index].item.Properties.ThresholdType=ed.Properties.ThresholdType;
            items[index].item.Properties.NSVPerCase=ed.Properties.NSVPerCase;
            items[index].item.Properties.GM=ed.Properties.GM;
            items[index].item.Properties.COGS=ed.Properties.COGS;
            items[index].item.Properties.Volume=ed.Properties.Volume; 
            items[index].item.Properties.ROS=ed.Properties.ROS;
            items[index].item.Properties.Distribution=ed.Properties.Distribution;
          }
        }
        })
      }

    //   if(isCopyRequest){
    //     this.deliverablesCopyData.forEach(ed=>{
    //       items.forEach(ei=>{
    //         ei.item.Properties.ThresholdType=ed.Properties.ThresholdType;
    //         ei.item.Properties.NSVPerCase=ed.Properties.NSVPerCase;
    //         ei.item.Properties.GM=ed.Properties.GM;
    //         ei.item.Properties.COGS=ed.Properties.COGS;
    //         ei.item.Properties.Volume=ed.Properties.Volume; 
    //         ei.item.Properties.ROS=ed.Properties.ROS;
    //         ei.item.Properties.Distribution=ed.Properties.Distribution;
    //     })
    //   })
    // }
  
      items.forEach((ele,i) => {
        if (ele.changeType !== 'Created') {
          return;
        }
        if(i === 1){
          this.deliverablesRowFormArray.push(this.formBuilder.group({
            volume:[isCopyRequest?ele.item.Properties.Volume : '',[Validators.required,Validators.pattern("^[0-9]*$"),Validators.maxLength(64)]],
            ros:[{value: isCopyRequest?ele.item.Properties.ROS: '', disabled: true}, [Validators.required,Validators.pattern("^[0-9]*$"),Validators.maxLength(64)]],
            distribution:[{value: isCopyRequest?ele.item.Properties.Distribution : '', disabled: true}, [Validators.required,Validators.pattern("^[0-9]*$"),Validators.maxLength(64)]],
            itemId: ele.item.Identity.ItemId,
          }));
        } else{
          const formValues = {
            volume:[isCopyRequest?ele.item.Properties.Volume :'',[Validators.required,Validators.pattern("^[0-9]*$"),Validators.maxLength(64)]],
            ros:[ isCopyRequest?ele.item.Properties.ROS:  '',[Validators.required,Validators.pattern("^[0-9]*$"),Validators.maxLength(64)]],
            distribution:[ isCopyRequest?ele.item.Properties.Distribution :'', [Validators.required,Validators.pattern("^[0-9%]*$"),Validators.maxLength(64)]],
            itemId: ele.item.Identity.ItemId,
          }
          if(i === 0){
            delete formValues.volume;
          }
          this.deliverablesRowFormArray.push(this.formBuilder.group(formValues));
        }
        // console.log( this.deliverablesRowFormArray,' this.deliverablesRowFormArrayROW');
        
        this.updateRequestInfo('volume','deliverablesRowFormArray',i)
        this.updateRequestInfo('ros','deliverablesRowFormArray',i)
        this.updateRequestInfo('distribution','deliverablesRowFormArray',i)
      })
      // console.log( this.deliverablesRowFormArray,' this.deliverablesRowFormArray');
      
      this.renderMSTableRows();
      this.loaderService.hide();
    }, () => {
      this.loaderService.hide();
    });
  // }
    
  }

  isErrorValid(index,control){
    if(this.deliverablesRowFormArray?.controls[index]?.get(control)?.touched){
      return this.deliverablesRowFormArray?.controls[index].get(control).errors 
      && this.deliverablesRowFormArray?.controls[index].get(control).errors.required;
    }
  }

  isErrorPatternValid(index,control){
    if(this.deliverablesRowFormArray?.controls[index]?.get(control)?.touched){
      return this.deliverablesRowFormArray?.controls[index].get(control).errors 
      && this.deliverablesRowFormArray?.controls[index].get(control).errors.pattern;
    }
  }
  isErrorMaxLength(index,control){
    if(this.deliverablesRowFormArray?.controls[index]?.get(control)?.touched){
      return this.deliverablesRowFormArray?.controls[index].get(control).errors 
      && this.deliverablesRowFormArray?.controls[index].get(control).errors.maxlength;
    }
  }

  updateRequestInfo(formControl,formName,index?) {
    let property;
    let value;
    let itemId;
    let arr = [];
    if(index == 0){
      arr = ['nSVPerCase','gM','cogs','volume'];
    } else {
      arr = ['nSVPerCase','gM','cogs'];
    }
    const flag = arr.some(ele => ele == formControl);
    if(formName === 'deliverablesRowFormArray' && index >=0) {
      property = BusinessConstants.DYNAMIC_COMMERCIAL_DELIVERABLES_FORM[formControl];
      if(flag){
        value = this.dataSource[index] && this.dataSource[index][formControl];
      } else {
        value = this.deliverablesForm.value.deliverablesRow[index] && this.deliverablesForm.value.deliverablesRow[index][formControl]
      }
      itemId = this.deliverablesForm.value.deliverablesRow[index] && this.deliverablesForm.value.deliverablesRow[index].itemId
      //console.log(this.deliverablesForm.value);
      //console.log(this.deliverablesForm.value.deliverablesRow[index]);
      //console.log(itemId);
    }
    if(itemId) {
      this.monsterUtilService.setFormProperty(itemId, property, value);
    }
    // if(formControl=='distribution'){
    //   this.monsterUtilService.setFormProperty(itemId, property, 110);
    // }
  }

  onValidate() {
    this.deliverablesForm.markAllAsTouched();
    if(this.deliverablesForm.invalid) {
      return true;
    } else{
      return false;
    }
  }

  updateThisProject(data){
      this.dataSource[0].nSVPerCase = data.nsvPerCase ? data.nsvPerCase : 0;
      this.dataSource[0].gM = data.gM ? data.gM : 0;
      this.dataSource[0].cogs = data.cogs ? data.cogs : 0;
      this.dataSource[0].volume = data.volume ? data.volume : 0;
      // this.dataSource[2].volume = data.volume ? (data.volume/12) * 100 : 0;
      this.updateRequestInfo('nSVPerCase','deliverablesRowFormArray',0);
      this.updateRequestInfo('gM','deliverablesRowFormArray',0);
      this.updateRequestInfo('cogs','deliverablesRowFormArray',0);
      this.updateRequestInfo('volume','deliverablesRowFormArray',0);
  }

  /**
   * To load Deliverables vs thresholds
   */
   retrieveDeliverableThresholdContents() {
    this.loaderService.show();
    this.entityService.getEntityItemDetails(this.requestDetails.requestItemId,
      EntityListConstant.COMMERCIAL_REQUEST_DELIVERABLE_THRESHOLD).subscribe((response) => {
      //  console.log('response.items',response.items)
        if (response && response.items) {
          this.mapDeliverableThresholdContentToForm(response.items);
          this.loaderService.hide();
        }
      }, (error) => {
        this.loaderService.hide();
      });
    }

  mapDeliverableThresholdContentToForm(deliverablesData){
    this.dataSource[0] = {
      threshold: 'This Project', nSVPerCase: deliverablesData[0].Properties.NSVPerCase, gM: deliverablesData[0].Properties.GM,cogs: deliverablesData[0].Properties.COGS,volume: deliverablesData[0].Properties.Volume
    }
    this.dataSource[1] = {
      threshold: 'Minimum Threshold', nSVPerCase: deliverablesData[1].Properties.NSVPerCase, gM: deliverablesData[1].Properties.GM,cogs: deliverablesData[1].Properties.COGS
    }
    this.dataSource[2] = {
      threshold: 'Monster Green', nSVPerCase: deliverablesData[2].Properties.NSVPerCase, gM: deliverablesData[2].Properties.GM,cogs: deliverablesData[2].Properties.COGS
    }
    this.dataSource.forEach((ele,i) => {
      if(i === 1){
        this.deliverablesRowFormArray.push(this.formBuilder.group({
          volume:[{value: deliverablesData[i].Properties.Volume, disabled:this.deliverableThresholdDataConfig.volume.disabled},[Validators.required,Validators.pattern("^[0-9]*$"),Validators.maxLength(64)]],
          ros:[{value: '', disabled: true}, [Validators.required,Validators.pattern("^[0-9]*$"),Validators.maxLength(64)]],
          distribution:[{value: '', disabled: true}, [Validators.required,Validators.pattern("^[0-9]*$"),Validators.maxLength(64)]],
          itemId: [deliverablesData[i].Identity.ItemId]
        }));
      } else{
        const formValues = {
          volume:[{value: deliverablesData[i].Properties.Volume, disabled: this.deliverableThresholdDataConfig.volume.disabled},[Validators.required,Validators.pattern("^[0-9]*$")]],
          ros:[{value: deliverablesData[i].Properties.ROS, disabled: this.deliverableThresholdDataConfig.ros.disabled},[Validators.required,Validators.pattern("^[0-9]*$")]],
          distribution:[{value: deliverablesData[i].Properties.Distribution,disabled: this.deliverableThresholdDataConfig.distribution.disabled }, [Validators.required,Validators.pattern("^[0-9%]*$")]],
          itemId: [deliverablesData[i].Identity.ItemId]
        }
        if(i === 0){
          delete formValues.volume;
        }
        this.deliverablesRowFormArray.push(this.formBuilder.group(formValues));
      }
    });
    this.updateDeliverablesThresholdValues.emit();
    this.renderMSTableRows();
  }

  /**
   * To re-render material table 
   */
   renderMSTableRows() {
    this.stagesScopeTable && this.stagesScopeTable.renderRows ?
      this.stagesScopeTable.renderRows() : console.log('No rows in Deliverables Vs Threshold.');
  }

  updateFormControlsDisability() {
    for(let [ i, formControls] of this.deliverablesRowFormArray.controls.entries()){
      if(i !== 1){
        this.monsterUtilService.disableFormControls(formControls,this.deliverableThresholdDataConfig);
      }
      else{
        this.monsterUtilService.disableFormControlsForDeliverable(formControls,this.deliverableThresholdDataConfig);
      }
    }
  }

  copyDeliverablesVSThreshold(deliverablesDetails){
    this.deliverablesCopyData=deliverablesDetails.items
    this.dataSource
    
    // // this.deliverablesCopyData.forEach(data=>{
    // //   this.dataSource.push(data.Properties)
    // // })
    // console.log("this.deliverablesCopyData",this.deliverablesCopyData);
    this.onload(true);
  }

}
