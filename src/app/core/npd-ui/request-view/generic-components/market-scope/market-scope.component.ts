import { SelectionModel } from '@angular/cdk/collections';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatOption } from '@angular/material/core';
import { MatSelect } from '@angular/material/select';
import { MatTable } from '@angular/material/table';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService, NotificationService, UtilService } from 'mpm-library';
import { forkJoin, Observable } from 'rxjs';
import { EntityService } from '../../../services/entity.service';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../constants/BusinessConstants';
import { EntityListConstant } from '../../constants/EntityListConstant';

@Component({
  selector: 'app-market-scope',
  templateUrl: './market-scope.component.html',
  styleUrls: ['./market-scope.component.scss']
})
export class MarketScopeComponent implements OnInit {
  @Input() marketsCopy:any[];
  @Input() markets:any[];
  @Input() requestFieldHandler;
  @Input() requestDetails;
  @Input() showMarketScopeForm: boolean;
  @Input() marketScopeDataConfig;
  @Input() properties;
  @Input() marketScopeData;
  @Input() isNewRequest: boolean;
  @Input() businessUnitInitialCount;
  @Input() businessUnits = [];
  @Input() selectedBusinessUnit;
  @Input() selectedMarket:string;
  @Output() updateBusinessUnitEvent = new EventEmitter<any>();
  @Output() updateThisProject = new EventEmitter<any>();
  @Output() deleteMarketScopeGroupEvent = new EventEmitter<any>();
  @Output() updateProjectName = new EventEmitter<any>();
  @Output() updateMonsterGreen = new EventEmitter<any>();
  @Output() updateNoOfLinkedMarkets = new EventEmitter<any>();
  @ViewChild('marketSelect') marketSelect: MatSelect;
  @ViewChild('marketScopeTable') marketScopeTable: MatTable<any>;
  @ViewChild('marketScopeTemp', { static: false }) marketScopeTemp: ElementRef;
  @Output() marketSelected = new EventEmitter<any>();
  @Output() onLeadMarketChange = new EventEmitter<any>();

  weekPickerMinDate;

  dynamicMarketScopeForm: FormGroup;
  thresholdValuesForMarketBrand = [];
  dataSource = [];
  dataSourceCopy = [];
  msGridColumns = [];
  selectedMultiMarket = [];
  toTargetDate:NgbDate[] = [];
  fromTargetDate:NgbDate[] = [];
  targetDPDate = [];
  show = [];
  allMarketSelected = false;
  selection = new SelectionModel<any>(true, []);
  fromDate: NgbDate;
  toDate: NgbDate;
  thisProjectData:any = {
    nsvPerCase: 0,
    gM: 0,
    cogs: 0,
    volume: 0
  }
  thresholdParams:any = {
      leadMarket: null,
      brand: null,
      bottler: null,
  };
  loadingMarketScope = false;

  leadMarketCopyIndex
  isMarketAdded: boolean;
  enterAnnulaisedEuro: any;
  constructor(
  private formBuilder: FormBuilder,
  private notificationService: NotificationService,
  private loaderService:LoaderService,
  private entityService: EntityService,
  private utilService: UtilService,
  private monsterUtilService:MonsterUtilService,
  private monsterConfigApiService:MonsterConfigApiService,
  ) { }

  get formControls() { return this.dynamicMarketScopeForm.controls; }
  get marketScopeFormArray() { return this.formControls.marketScope as FormArray; }

  ngOnInit(): void {
    this.onLoadCreateForm();
    this.getMinWeekPickerDate();
      if(this.requestDetails && this.marketScopeData){
        this.retrieveMarketScopeContents();
      }else{
        // this.onLoadDataSource();
      }
      this.onLoadDataSource()
  }

  getMinWeekPickerDate() {
    let dateVal = new Date();
    let weekYearValue = this.monsterUtilService.calculateWeek(dateVal);//''+(this.calculateWeek(date)+'/'+ new Date(date).getFullYear());
    [dateVal, ] = this.monsterUtilService.getStartEndDate(weekYearValue);
    this.weekPickerMinDate = {year: dateVal.getFullYear(), month: dateVal.getMonth() + 1, day: dateVal.getDate()};
  }

  // getAllThresholdValues(data){
  //   this.thresholdParams[data.property] = +data.value;
  //   const flag = Object.keys(this.thresholdParams).every(ele => this.thresholdParams[ele]);
  //   // this.thresholdParams = {
  //     // leadMarket: 1,
  //     // brand: 32769,
  //     // platform: 32769,
  //     // bottler: 1,
  //     // variant: 32769,
  //   // }
  //   if(flag){
  //     this.getAllThresholdValuesForMarketBrand(this.thresholdParams).subscribe(response =>{
  //       response;
  //     }, () => {
  //       console.log('Error while loading the form configs.')
  //     });
  //   }
  // }
  onLoadCreateForm(){
    this.dynamicMarketScopeForm = this.formBuilder.group({
      marketScope: new FormArray([])
    });

    this.msGridColumns = this.properties.requestType === 'commercial' ? ['select','harmonisedMarket','leadMarket','threeMonthLauchVolume',
     'annualVolumne','cannibalisationNImpact','nsvCase','cogsCase','annualisedNsv','directlyEnterCurrencyInEuro','annualisedEuro','grossProfit','grossMargin', 'targetDP','actions']
     : this.properties.requestType === 'operations' ? ['select','harmonisedMarket','leadMarket','threeMonthLauchVolume',
     'annualVolumne', 'targetDP','actions'] : ['select','harmonisedMarket','leadMarket', 'targetDP','actions'];
    this.showMarketScopeForm = true;
  }
  // click events
  toggleAllSelection(){
    this.allMarketSelected = !this.allMarketSelected;
    if (this.allMarketSelected) {
      this.marketSelect.options.forEach((item: MatOption) => item.select());
    } else {
      this.marketSelect.options.forEach((item: MatOption) => item.deselect());
    }
  }
  optionClick(){
    let newStatus = true;
    this.marketSelect.options.forEach((item: MatOption) => {
      if (!item.selected && item.value !== 0) {
        newStatus = false;
      }
    });
    this.allMarketSelected = newStatus;
    this.allMarketSelected ? this.marketSelect.options.first.select() : this.marketSelect.options.first.deselect();
  }

  bulkDeleteMS(){
    if (this.dynamicMarketScopeForm.value.marketScope.length === 0) {
      this.notificationService.error('Add at Least one Market Scope.');
      return;
    }
    if (this.selection.selected.length < 1) {
      this.notificationService.error('Select Market(s) to delete.');
      return;
    }
    this.loaderService.show();
    const formArray = this.dynamicMarketScopeForm.get('marketScope') as FormArray;
    const itemIds = [];
    for (let i = 0; i < this.selection.selected.length; i++) {
      itemIds.push(this.selection.selected[i].value.itemId);
    }
    const formRowsLenght = formArray.controls.length;
    this.entityService.deleteMultipleEntityObjects(itemIds, EntityListConstant[this.properties.marketScope], this.utilService.APP_ID).subscribe(() => {
      for (let i = formRowsLenght - 1; i > -1; i--) {
        if (itemIds.indexOf(formArray.controls[i].value.itemId) !== -1) {
          this.monsterUtilService.removeFormProperty(formArray.controls[i].value.itemId);
          this.monsterUtilService.removeFormRelations(formArray.controls[i].value.itemId);
          this.dataSource.splice(i, 1);
          this.marketScopeFormArray.removeAt(i);
          //NPD1-83 Removing entries fromTargetDate & toTargetDate
          this.targetDPDate.splice(i,1);
          this.fromTargetDate.splice(i, 1);
          this.toTargetDate.splice(i, 1);

        }
      }
      this.selection = new SelectionModel<Element>(true, []);
      this.renderMSTableRows();
      this.updateMultiMarkets();
      this.loaderService.hide();
      //NPD1-81 Update prject name
      this.updateLeadMarket();
      this.updateLinkedMarkets();
      this.getUpdatedThisProject();
      this.notificationService.success('Successfully completed the deletion process.');
    }, () => {
      this.notificationService.error('Error while deleting the market scope.');
    });
  }

  getBool(i){
    this.show[i] = !this.show[i];
  }

  getAllBool() {
    this.show = this.show.map(ele => {
      ele = false;
    });
  }
  updateLeadMarket(){
    if(this.dataSource.length == 0){
      this.updateProjectName.emit('');
      this.updateBusinessUnitEvent.emit('');
      this.properties.requestType === 'commercial' ? this.updateMonsterGreen.emit({property: 'leadMarket', value: null}) : null;;
      this.thisProjectData = {};
      this.properties.requestType === 'commercial' ? this.updateThisProject.emit('') : null;
    }
    else{
      this.dataSourceLeadMarket();
    }
  }
  dataSourceLeadMarket(){
    if(this.dataSource.length > 0){
      const flag = this.dataSource.some(ele => ele.leadMarket);
      if(!flag){
        this.dataSource.forEach((ele,i) => {
          if(i === 0){
            ele.leadMarket = true;
            this.updateLeadMarketInfo(i);
          }else {
            ele.leadMarket = false;
            if(this.properties.requestType === 'commercial'){
            this.marketScopeFormArray.controls[i]['controls'].cogsCase.disable();
            }
            this.updateRequestInfo('leadMarket','dynamicMarketScopeForm',false,i);
          }
        })
      }
    }
  }
  onLeadMarketClick(index) {
    this.dataSource.forEach((ele,i) =>{
      if(i === index){
        //Updating Project Name - When Lead Market Radio Button is changed
        //NPD1-81
       
        ele.leadMarket = true;
        this.updateLeadMarketInfo(i);
      }else {
        ele.leadMarket = false;
        if(this.properties.requestType === 'commercial'){
        this.marketScopeFormArray.controls[i]['controls']?.cogsCase.disable();
        }
        this.updateRequestInfo('leadMarket','dynamicMarketScopeForm',false,i);
      }
    });
  }
  updateLeadMarketInfo(index){

    this.selectedMarket = this.dataSource[index].harmonisedMarket.displayName;
    this.updateProjectName.emit(this.selectedMarket);
    this.updateRegulatoryLocations(index);
    if(this.properties.requestType === 'commercial'){
      this.marketScopeFormArray.controls[index]['controls']?.cogsCase.enable();
      // this.getAllThresholdValues({property: 'leadMarket', value:  this.dataSource[index].harmonisedMarket.Id})
      this.getUpdatedMonsterGreen(index);
    }
    this.updateRequestInfo('leadMarket','dynamicMarketScopeForm',true,index);
    let businessUnitItemId = this.dataSource[index].harmonisedMarket && this.dataSource[index].harmonisedMarket.businessUnit && this.dataSource[index].harmonisedMarket.businessUnit.ItemId;
    if(businessUnitItemId) {
      this.selectedBusinessUnit = this.businessUnits.find(b=>b.ItemId == businessUnitItemId)
      if(this.selectedBusinessUnit) {
        this.updateBusinessUnitEvent.emit(this.selectedBusinessUnit);
      }
    }
  }

  updateRegulatoryLocations(index) {//regulatoryClassification//this.dataSource[index].harmonisedMarket?.ItemId?.split('.')[1]
    this.marketSelected.emit({property: 'market', value: this.dataSource[index].harmonisedMarket?.regulatoryClassification,isDataLoad : false});
    this.onLeadMarketChange.emit({property: 'market', value: this.dataSource[index].harmonisedMarket,isDataLoad : false});

  }

  getUpdatedMonsterGreen(index){
    this.updateMonsterGreen.emit({property: 'leadMarket',
     value: this.dataSource[index].harmonisedMarket.Id || this.dataSource[index].harmonisedMarket.ItemId.split('.')[1]});
  }
  getUpdatedThisProject(){
    // const data = {
    //   nsvPerCase: this.dataSource[index].nsvCase,
    //   gM: this.dataSource[index].grossMargin,
    //   cogs: this.dataSource[index].cogsCase,
    //   volume: this.marketScopeFormArray.controls[index]['controls'].annualVolumne.value,
    // }
    if(this.properties?.requestType === 'commercial') {
      this.thisProjectData = {};
      this.updateThisProjectNSVCase();
      this.updateThisProjectCogsCase();
      this.updateThisProjectGm();
      this.updateThisProjectVolume();
      this.updateThisProject.emit(this.thisProjectData);
    }
  }
  deleteMarketScopeGroup(index?, itemId?) {
    const formArray = this.dynamicMarketScopeForm.get('marketScope') as FormArray;
    const deleteItem = formArray.controls[index].value.itemId;
    this.loaderService.show();
    this.entityService.deleteEntityObjects(deleteItem).subscribe(response => {
      this.monsterUtilService.removeFormProperty(deleteItem);
      this.monsterUtilService.removeFormRelations(deleteItem);
      if(formArray.controls[index].value.leadMarket) {
        this.deleteMarketScopeGroupEvent.emit();
      }
      this.marketScopeFormArray.removeAt(index);
      this.dataSource.splice(index, 1);

      //NPD1-83 Removing fromTargetDate & toTargetDate entry
      this.targetDPDate.splice(index,1);
      this.fromTargetDate.splice(index, 1);
      this.toTargetDate.splice(index, 1);
      //NPD1-81 Removing Market name from project name since lead market is deleted
      this.updateLeadMarket();
      this.getUpdatedThisProject();
      this.updateMultiMarkets();
      this.loaderService.hide();
      this.renderMSTableRows();
      this.updateLeadMarket();
      this.updateLinkedMarkets();
      this.notificationService.success('Market Scope deleted successfully.');
    }, error => {
      this.loaderService.hide();
    });
}


// change events
  /**
   * Toggle selected rows in Market Scope table
   */
   masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.marketScopeFormArray.controls.forEach(row => this.selection.select(row));
  }



  // ngModelChange event
  updateRequestInfo(formControl,formName,propertyValue?,index?) {
    let property;
    let value;
    let itemId;
    

    const directlyEnterCurrencyInEuro = this.marketScopeFormArray.controls[index] && this.marketScopeFormArray.controls[index]['controls'].directlyEnterCurrencyInEuro?.value

    const flag = ['leadMarket','grossProfit','grossMargin'].some(ele => ele == formControl);

    if(formName === 'dynamicMarketScopeForm' && index >=0) {
      property = BusinessConstants[this.properties.dynamicMarketScopeFormControlNames][formControl];
      if(flag){
        value = this.dataSource[index] && this.dataSource[index][formControl];
      }
      else if(formControl == 'annualisedEuro'){
        value = this.marketScopeFormArray.controls[index]['controls'][formControl].value?.includes('€ ') ? this.marketScopeFormArray.controls[index]['controls'][formControl].value :
        `€ ${parseFloat(this.marketScopeFormArray.controls[index]['controls'][formControl].value)?.toFixed(2)}`
      }
      else {
        value = this.marketScopeFormArray.controls[index] && this.marketScopeFormArray.controls[index]['controls'][formControl].value
      }
      itemId = this.marketScopeFormArray.controls[index] && this.marketScopeFormArray.controls[index]['controls'].itemId.value
    }
    this.monsterUtilService.setFormProperty(itemId, property, value);
  }

  // template functions
  isDateValid(index){
    if(this.marketScopeFormArray.controls.length > 0 && index < this.marketScopeFormArray.controls.length &&
      this.marketScopeFormArray.controls[index].get('targetDP').touched )
    {
      if(this.marketScopeFormArray.controls[index].get('targetDP').value == undefined
                        || this.marketScopeFormArray.controls[index].get('targetDP').value.trim() === ""
                         || this.marketScopeFormArray.controls[index].get('targetDP').value == null)
      {
        return true;
      }
      else {
        return false;
      }
    }else{
      return false;
    }

  }
  isErrorValid(index,control){
    if(this.marketScopeFormArray?.controls[index]?.get(control)?.touched){
      return this.marketScopeFormArray?.controls[index].get(control).errors
      && this.marketScopeFormArray?.controls[index].get(control).errors.required;
    }
  }
  isErrorPatternValid(index,control){
    if(this.marketScopeFormArray?.controls[index]?.get(control)?.touched){
      return this.marketScopeFormArray?.controls[index].get(control).errors
      && this.marketScopeFormArray?.controls[index].get(control).errors.pattern;
    }
  }
  isErrorMaxLength(index,control){
    if(this.marketScopeFormArray?.controls[index]?.get(control)?.touched){
      return this.marketScopeFormArray?.controls[index].get(control).errors
      && this.marketScopeFormArray?.controls[index].get(control).errors.maxlength;
    }
  }
  checkDatePattern(index){
    let isValid=true;

    if(this.marketScopeFormArray.controls.length > 0 && index < this.marketScopeFormArray.controls.length &&
      this.marketScopeFormArray.controls[index].get('targetDP').touched && this.marketScopeFormArray.controls[index].get('targetDP').value
      && this.marketScopeFormArray.controls[index].get('targetDP').value.trim().length > 0)
    {
      let regexp = new RegExp('^([1-9]|[1-4][0-9]|5[0-2])\/[0-9]{4}$');
      isValid = regexp.test(this.marketScopeFormArray.controls[index].get('targetDP').value);
    }
    return !isValid;
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.marketScopeFormArray.controls.length;
    return numSelected === numRows;
  }
  getPickerStyles(index){
    const nativeElement = this.marketScopeTable['_elementRef'].nativeElement;
    return this.properties.requestType === 'commercial' ? `${ (74 + (nativeElement.children[1].offsetHeight + (index * nativeElement.children[1].offsetHeight))) - (nativeElement.scrollTop) }px`
           : `${ (56 + (nativeElement.children[1].offsetHeight + (index * nativeElement.children[1].offsetHeight) )) - (nativeElement.scrollTop)}px` ;
  }
  checkLeadMarket(leadMarket,index) {
    if(leadMarket && leadMarket.value && leadMarket.value.leadMarket) {
      return true;
    } else if(this.isNewRequest && index == 0) {
       this.marketScopeFormArray.controls[index].patchValue({leadMarket: true});
       this.updateRequestInfo('leadMarket','dynamicMarketScopeForm',false,index);
       if(this.businessUnitInitialCount <= 0) {
        let businessUnitItemId = this.dataSource[index].harmonisedMarket && this.dataSource[index].harmonisedMarket.businessUnit && this.dataSource[index].harmonisedMarket.businessUnit.ItemId;
        if(businessUnitItemId) {
          this.selectedBusinessUnit = this.businessUnits.find(b=>b.ItemId == businessUnitItemId)
          if(this.selectedBusinessUnit) {
            this.updateBusinessUnitEvent.emit(this.selectedBusinessUnit);
          }
        }
        this.businessUnitInitialCount++;
       }
       return true;
    } else {
       return false;
    }

  }
  checkboxLabel(row?: string): string {
    return '';
  }
  // child functions
  changeTargetDP(weekFormat,i) {
    this.targetDPDate[i] = weekFormat;
    this.marketScopeFormArray.controls[i].patchValue({targetDP: weekFormat});
    this.updateRequestInfo('targetDP','dynamicMarketScopeForm',weekFormat,i);
    //NPD1-83
    if(weekFormat && weekFormat!==undefined){
      let week = weekFormat.split("/")[0];
      let year = weekFormat.split("/")[1];

      let weekdates:NgbDate[] = this.calculateDate(week,year);

      this.fromTargetDate[i] = weekdates[0];
      this.toTargetDate[i] = weekdates[1];
    }

  }
  calculateDate(week: number, year: number):NgbDate[] {

    let ngbFromDate,ngbToDate;
   
    const firstDay = new Date(year + "/1/4"); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(`${selectDate.year.toString()}-${selectDate.month.toString().padStart(2,'0')}-${selectDate.day.toString().padStart(2,'0')}`);
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate,ngbToDate];

  }

  // validation functions

  marketDuplicateValidator(dataSource): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const currentMarket = control.value;
      if (dataSource.length === 1) {
        return null;
      }
      for (var i = 0; i < dataSource.length; i++) {
        if (dataSource[i].harmonisedMarkets == currentMarket.ItemId) {
          return { 'marketDuplicateValidator': true };
        }
      }
      return null;
    }
}

 /**
   * To re-render material table
   */
  renderMSTableRows() {
    this.marketScopeTable && this.marketScopeTable.renderRows ?
      this.marketScopeTable.renderRows() : console.log('No rows in Market Scope.');
  }

   /**
   * To update multi markets
   */
    updateMultiMarkets() {
      this.marketsCopy = [];
      this.markets.forEach((market) => {
        const matchedLanguage = this.dataSource.filter((selectedMarket) => {
          return (market.ItemId === (selectedMarket.harmonisedMarket && selectedMarket.harmonisedMarket.ItemId));
        });
        if (matchedLanguage.length === 0) {
          this.marketsCopy.push(this.copyObj(market));
        }
      });
  }

    /**
   * To deep copy the objects
   * */
     copyObj(obj) {
      return JSON.parse(JSON.stringify(obj));
  }

  initialDate(){
    let fromDate = new Date();
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    this.fromDate = new NgbDate(
      fromDate.getFullYear(),
      Number((fromDate.getMonth() + 1).toString().padStart(2,'0')),
      Number((fromDate.getDate()).toString().padStart(2,'0')),
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    this.toDate = new NgbDate(
      toDate.getFullYear(),
      Number((toDate.getMonth() + 1).toString().padStart(2,'0')),
      Number((toDate.getDate()).toString().padStart(2,'0'))
    );
  }



    //  Calculation
    onLoadDataSource(){
      const array = [];
      this.markets.map((ele,i) => {
        array[i] = {};
        // if(i === 0){
        //   array[i]['leadMarket'] = true
        // }else {
        //   array[i]['leadMarket'] = false
        // }
        // array[i]['ConversionRateEURO'] = ele.ConversionRateEURO;
        array[i]['leadMarket'] = false;
        array[i]['harmonisedMarket'] = ele;
      });
      if(this.properties.requestType === 'commercial'){
        // get nsv/case and cogsCase
        array.forEach((ele, i) => {
          ele['nsvCase'] = 0,
          ele['cogsCase'] = 0
        });

        array.forEach((ele, i) => {
          ele['grossProfit'] = 0;
          ele['grossMargin'] = (+ele.nsvCase !== 0 && +ele.cogsCase !== 0) ? `${(((+ele.nsvCase - +ele.cogsCase) / +ele.nsvCase ) * 100).toFixed(2)}%`: null;
        });
      }
      // this.dataSource = array.map(ele => ele);
      this.dataSourceCopy = array.map(ele => ele);
    }

    updateLinkedMarkets() {
     let isLeadMarket = this.dataSource.find(ms => ms.leadMarket == true)
     let noOfLinkedMarkets = 0;
     if(isLeadMarket && this.dataSource.length > 0) {
      noOfLinkedMarkets = (this.dataSource.length - 1);
     }
     this.updateNoOfLinkedMarkets.emit(noOfLinkedMarkets);
    }

    getDataSource(isCopyRequest?,multiMarket?) {
    
      // this.selectedMultiMarket = isCopyRequest ? multiMarket : this.selectedMultiMarket;
      if (this.selectedMultiMarket.length === 0 || (this.selectedMultiMarket.length === 1 && this.selectedMultiMarket[0] === 0)) {
        this.notificationService.error('Please select at least one market.');
        return;
      }

      const selectedMultiMarket = this.selectedMultiMarket.filter(ele =>{
        if(ele === 0){
          return false;
        }else {
          return  true;
        }
      });
      const dataSourceCopy = JSON.parse(JSON.stringify(this.dataSourceCopy.map(ele => ele)));
      // selectedMultiMarket = dataSourceCopy.filter(i => i.value == selectedMultiMarket[0].value);
      let dataSource = []
      if(isCopyRequest){
        this.dataSource=[]
        while (this.marketScopeFormArray.length !== 0) {
          this.marketScopeFormArray.removeAt(0)
        }

        dataSource = dataSourceCopy.filter(item => {
          const flag = selectedMultiMarket.some((ele, i) => {
            return ele.displayName == item.harmonisedMarket.displayName
          });
          return flag;
        });
      }else{
      dataSource = dataSourceCopy
                          .filter(item => {
                            const flag = selectedMultiMarket.some((ele, i) => {
                              return ele.ItemId == item.harmonisedMarket.ItemId
                            });
                            return flag;
                          });

                        }
      const updateData = [];
      dataSource.forEach((ele,i) => {
        const temp = {
          operationType: 'Create',
          parentItemId: this.requestDetails.requestItemId,
          relationName: EntityListConstant[`${this.properties.requestMarketScopeRelation}`],//change this-make it as a constant
          template: null,
          item: {
            Properties: this.properties.requestType === 'commercial' ? {
              // AnnualisedYear1NSV_EURO: null,
              // AnnualisedYear1NSV_LocalCurrency: null,
              // AnnualisedYear1Volume: null,
              COGCase: ele.cogsCase,
              // Cannibilisation_Impact: null,
              GrossMargin: ele.grossMargin,
              GrossProfit: ele.grossProfit,
              LeadMarket: ele.leadMarket,
              NSVCase: ele.nsvCase,
              // TargetDPInWarehouse: null,
              // Three_Month_LaunchVolume: null,
            } : this.properties.requestType === 'operations' ?{
              Lead_Market: ele.leadMarket,
            } : {
              LEAD_MARKET: ele.leadMarket,
            },
            [this.properties.R_PO_MARKET]: {
              ItemId: ele.harmonisedMarket.ItemId,
            }
          }
        };
        updateData.push(temp);
      });
      //this.dataSource = dataSource
      // this.dataSource=[]
      // dataSource=[]
      this.dataSource.push(...dataSource);
      this.loaderService.show();

      this.entityService.bulkCreateEntitywithParent(updateData, null, null).subscribe((response) => {
        let items = response.changeLog.changeEventInfos;
        

        if(isCopyRequest){
          selectedMultiMarket.forEach(em=>{
            items.forEach(ei=>{
              if(ei.item && (ei.item.R_PO_MARKETS?.Properties.DisplayName==em.displayName || ei.item.R_PO_MARKET?.Properties.DisplayName==em.displayName)){
                // if(ei.item && ei.item['R_PO_MARKETS'].Properties.DisplayName==em.displayName){
                this.properties.requestType ==='commerial'? ei.item.Properties.LeadMarket=em.leadMarket :
                this.properties.requestType ==='operations'? ei.item.Properties.Lead_Market=em.leadMarket :
                ei.item.Properties.LEAD_MARKET=em.leadMarket;
                ei.item.Properties.TargetDPInWarehouse=em.targetDp;
                if(em.annualisedYear1 && em.threeMonthLaunch){
                  ei.item.Properties.AnnualisedYear1Volume=em.annualisedYear1
                  ei.item.Properties.Three_Month_LaunchVolume=em.threeMonthLaunch
                  ei.item.Properties.Three_Month_Launch_Volume=em.threeMonthLaunch
                }if(em.nsvCase && em.cogCase && em.annualisedYear1Euro && em.cannabalisationImpact){
                  ei.item.Properties.NSVCase=em.nsvCase
                  ei.item.Properties.COGCase=em.cogCase
                  ei.item.Properties.GrossMargin=em.grossMargin
                  ei.item.Properties.GrossProfit= em.grossProfit
                  ei.item.Properties.AnnualisedYear1NSV_LocalCurrency= em.annualisedYear1LocalCurrency
                  ei.item.Properties.AnnualisedYear1NSV_EURO=em.annualisedYear1Euro
                  ei.item.Properties.Cannibilisation_Impact= em.cannabalisationImpact
                  ei.item.Properties.EnterCurrencyDirectlyInEuros= em.directlyEnterEuro
                }
              }
            })
          })
        }
        items=items.filter(data=>{
          return  (data.changeType == 'Created')
        })
     
        items.forEach((data,j) => {
          if (data.changeType !== 'Created') {
            return;
          }
          let cogs;
          if(+dataSource[j].leadMarket){
            cogs = {value: +dataSource[j].cogsCase ,disabled: false};
          } else {
            cogs = {value: +dataSource[j].cogsCase ,disabled: true};
          }
          //if(!isCopyRequest){
            this.marketScopeFormArray.push(this.formBuilder.group(
              this.properties.requestType === 'commercial' ? {
              // market: ['', [Validators.required, this.marketDuplicateValidator(this.dataSource.harmonisedMarket)]],
              threeMonthLauchVolume: [isCopyRequest? data.item.Properties.Three_Month_LaunchVolume: '', [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
              annualVolumne: [isCopyRequest? data.item.Properties.AnnualisedYear1Volume:'', [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
              cannibalisationNImpact: [ isCopyRequest? data.item.Properties.Cannibilisation_Impact:'', [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
              nsvCase: [{value: isCopyRequest? data.item.Properties.NSVCase : +dataSource[j].nsvCase ,disabled: this.marketScopeDataConfig.nsvCase.disabled}, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
              cogsCase: [ isCopyRequest? data.item.Properties.COGCase : '' , [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
              annualisedNsv: [{value :isCopyRequest? data.item.Properties.AnnualisedYear1NSV_LocalCurrency:'',disabled: (data.item.Properties?.EnterCurrencyDirectlyInEuros || this.marketScopeDataConfig.annualisedNsv.disabled )? true :false }, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
              annualisedEuro: [{value:  isCopyRequest? data.item.Properties.AnnualisedYear1NSV_EURO:'',disabled: ((!data.item.Properties?.EnterCurrencyDirectlyInEuros) || this.marketScopeDataConfig.annualisedNsv.disabled )? true: false}, [Validators.required,Validators.maxLength(64)]],
              targetDP: [isCopyRequest? data.item.Properties.TargetDPInWarehouse:'',[Validators.required,Validators.pattern('^([1-9]|[1-4][0-9]|5[0-2])\/[0-9]{4}$')]],
              directlyEnterCurrencyInEuro:[ isCopyRequest ? data.item.Properties.EnterCurrencyDirectlyInEuros :''],
              itemId: data.item.Identity.ItemId,
            } : this.properties.requestType === 'operations' ? {
                threeMonthLauchVolume: [isCopyRequest? data.item.Properties.Three_Month_Launch_Volume:'', [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
                annualVolumne: [isCopyRequest? data.item.Properties.AnnualisedYear1Volume:'', [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
                targetDP: [isCopyRequest? data.item.Properties.TargetDPInWarehouse:'',[Validators.required,Validators.pattern('^([1-9]|[1-4][0-9]|5[0-2])\/[0-9]{4}$')] ],
                itemId: data.item.Identity.ItemId,
              } : {
                targetDP: [isCopyRequest? data.item.Properties.TargetDPInWarehouse:'',[Validators.required,Validators.pattern('^([1-9]|[1-4][0-9]|5[0-2])\/[0-9]{4}$')] ],
                itemId: data.item.Identity.ItemId,
                // targetDP :['',[Validators.required,Validators.pattern('^([1-9]|[1-4][0-9]|5[0-2])\/[0-9]{4}$')] ],
                // itemId: data.item.Identity.ItemId,
            }));
          //}
          //this.marketScopeFormArray.controls[j].patchValue(data.item.Properties.TargetDPInWarehouse);
          //Adding from & todate for each market scope record.
          if(isCopyRequest){
            if(data.item.Properties.LEAD_MARKET==true || data.item.Properties.Lead_Market==true || data.item.Properties.LeadMarket==true){
              this.leadMarketCopyIndex=j;
            }
            this.changeTargetDP(data.item.Properties.TargetDPInWarehouse,j);
            if(data.item.Properties.AnnualisedYear1Volume && data.item.Properties.Three_Month_LaunchVolume){
              this.updateRequestInfo('threeMonthLauchVolume','dynamicMarketScopeForm',null,j);
              this.updateRequestInfo('annualVolumne','dynamicMarketScopeForm',null,j);
              if(data.item.Properties.NSVCase && data.item.Properties.COGCase &&( data.item.Properties.AnnualisedYear1NSV_LocalCurrency || data.item.Properties.AnnualisedYear1NSV_EURO )&& data.item.Properties.Cannibilisation_Impact){
              this.updateRequestInfo('cannibalisationNImpact','dynamicMarketScopeForm',null,j);
              this.updateGrossProfit(j);
              this.updateRequestInfo('nsvCase','dynamicMarketScopeForm',null,j);
              this.updateRequestInfo('directlyEnterCurrencyInEuro','dynamicMarketScopeForm',null,j);
              this.updateRequestInfo('cogsCase','dynamicMarketScopeForm',null,j);
              this.updateCogsCase(j,this.marketScopeFormArray.controls[j].value.cogsCase);
              this.updateGrossProfit(j);
              this.updateGrossMargin(j);
              this.marketScopeFormArray.controls[j].value?.directlyEnterCurrencyInEuro ?
               this.updateAnnualisedEuro(j,this.marketScopeFormArray.controls[j]['controls'].annualisedEuro.value) :
               this.updateAnnualisedNsv(j,this.marketScopeFormArray.controls[j].value.annualisedNsv)
              // this.updateAnnualisedEuro(j,this.marketScopeFormArray.controls[j].value.annualisedEuro);
              this.updateNSVPerCase(j);
              this.updateGrossProfit(j);
              this.updateGrossMargin(j);
              }
            }
        }
          this.initialDate();
          this.fromTargetDate.push(this.fromDate);
          this.toTargetDate.push(this.toDate);
        });
        if(isCopyRequest){
        this.onLeadMarketClick(this.leadMarketCopyIndex)
        }
        // this.dataSource=[]
        // this.dataSource = [...this.dataSource, ...dataSource]
        this.dataSourceLeadMarket();
        this.updateMultiMarkets();
        this.selectedMultiMarket = [];
        this.renderMSTableRows();
        this.updateLinkedMarkets();
        this.loaderService.hide();
        this.allMarketSelected = false;
      }, () => {
        this.loaderService.hide();
      });
    }

    onValidate() {
      this.isMarketAdded = this.marketScopeFormArray.length === 0 ? false : true;
      if(this.isMarketAdded){
        this.dynamicMarketScopeForm.markAllAsTouched();
        if(this.dynamicMarketScopeForm.invalid) {
          return true;
        } else{
          return false;
        }
      }else{
        return true;
      }
    }

    // getAllThresholdValuesForMarketBrand(parameters) : Observable<any> {
    //   return new Observable(observer => {
    //     this.monsterConfigApiService.fetchAllThresholdValuesForMarketBrand(parameters).subscribe((response) => {
    //       this.thresholdValuesForMarketBrand = response;
    //       this.thresholdValuesForMarketBrand.forEach(item => {
    //         this.dataSource.forEach((ele, i) => {
    //         if(ele.harmonisedMarket.ItemId === item.R_PO_LEAD_MARKET['Markets-id'].ItemId){
    //           ele['nsvCase'] = item.NSVPerCase;
    //           ele['cogsCase'] = item.COGsPerCase;
    //           ele['grossMargin'] = (+ele.nsvCase !== 0 && +ele.cogsCase !== 0) ? `${(((+ele.nsvCase - +ele.cogsCase) / +ele.nsvCase ) * 100).toFixed(2)}%`: null;
    //           this.marketScopeFormArray.controls[i].patchValue({
    //             nsvCase: item.NSV,
    //             cogsCase: item.COG,
    //           });
    //           this.updateRequestInfo('nsvCase','dynamicMarketScopeForm',null,i);
    //           this.updateRequestInfo('cogsCase','dynamicMarketScopeForm',null,i);
    //           this.updateRequestInfo('grossMargin','dynamicMarketScopeForm',null,i);
    //         }
    //         });
    //       })
    //       this.dataSource.forEach((ele,i) => {
    //         if(ele.nsvCase){
    //           if(ele.leadMarket){
    //             this.getUpdatedThisProject();
    //           }
    //         }
    //       })
    //       observer.next(true);
    //       observer.complete();
    //     },(error)=> {
    //       observer.error(error);
    //     });
    //   });
    // }

    updateAnnualisedNsv(index,formValue){     // this.isdirectLocalCurrency = true ? (formValue='') : formValue
      const currencyConverter = this.dataSource[index].harmonisedMarket.ConversionRateEURO;
      const directlyEnterCurrencyInEuro = this.marketScopeFormArray.controls[index]['controls'].directlyEnterCurrencyInEuro.value
      if(directlyEnterCurrencyInEuro){
        formValue =''
        this.marketScopeFormArray.controls[index].patchValue({
          annualisedEuro: ''
        })
         this.updateRequestInfo('annualisedNsv','dynamicMarketScopeForm',null,index);
         this.updateRequestInfo('annualisedEuro','dynamicMarketScopeForm',`$ ${formValue * currencyConverter}`,index);
      }else{
        this.marketScopeFormArray.controls[index].patchValue({
          annualisedEuro: `€ ${(formValue * currencyConverter).toFixed(2)}`
        })
          this.updateRequestInfo('annualisedNsv','dynamicMarketScopeForm',null,index);
          this.updateRequestInfo('annualisedEuro','dynamicMarketScopeForm',`$ ${formValue * currencyConverter}`,index);
      }
    }
    updateNSVPerCase(index){
      const currencyConverter = +this.dataSource[index].harmonisedMarket.ConversionRateEURO;
      const annualisedNsv = +this.marketScopeFormArray.controls[index]['controls'].annualisedNsv.value;
      let annualisedEuroVal
      let annualisedEuro
      const directlyEnterCurrencyInEuro = this.marketScopeFormArray.controls[index]['controls'].directlyEnterCurrencyInEuro.value
      if(annualisedNsv>0){
        annualisedEuro =  this.marketScopeFormArray.controls[index]['controls'].annualisedEuro.value?.split('€ ')[1]
      }else{
        annualisedEuro =  this.marketScopeFormArray.controls[index]['controls'].annualisedEuro.value
      }
       annualisedEuroVal = annualisedEuro?.length==0 ? 0 : annualisedEuro
      const annualVolumne = +this.marketScopeFormArray.controls[index]['controls'].annualVolumne.value;
      if(annualisedEuroVal >=0 && annualVolumne){
        this.marketScopeFormArray.controls[index].patchValue({
          nsvCase: (parseFloat(annualisedEuroVal) / annualVolumne).toFixed(3)
        });
        this.updateRequestInfo('nsvCase','dynamicMarketScopeForm',null,index);
      }
      this.getUpdatedThisProject();
    }
    updateGrossProfit(index){
      if(this.properties.requestType === 'commercial'){
        const nsvCase = +this.marketScopeFormArray.controls[index]['controls'].nsvCase.value;
        const cogsCase = +this.marketScopeFormArray.controls[index]['controls'].cogsCase.value;
        const annualVolumne = +this.marketScopeFormArray.controls[index]['controls'].annualVolumne.value;
        const cannibalisationNImpact = +this.marketScopeFormArray.controls[index]['controls'].cannibalisationNImpact.value;
        if(nsvCase >= 0 && cogsCase && annualVolumne){
          this.dataSource[index].grossProfit = ((nsvCase - cogsCase) * (annualVolumne - cannibalisationNImpact)).toFixed(3);
          this.updateRequestInfo('grossProfit','dynamicMarketScopeForm',null,index);
        }
      }
    }
    updateGrossMargin(index){
      if(this.properties.requestType === 'commercial'){
        const nsvCase = +this.marketScopeFormArray.controls[index]['controls'].nsvCase.value;
        const cogsCase = +this.marketScopeFormArray.controls[index]['controls'].cogsCase.value;
        if(nsvCase >=0 && cogsCase){
          this.dataSource[index].grossMargin = `${(((nsvCase - cogsCase) / nsvCase) * 100).toFixed(3)}%`;
          this.updateRequestInfo('grossMargin','dynamicMarketScopeForm',null,index);
        }
        this.getUpdatedThisProject();
      }
    }
    updateCogsCase(index,formValue){
      if(formValue){
        for(let [i,formGroup] of this.marketScopeFormArray.controls.entries()){
          if(i !== index){
            formGroup.patchValue({
              cogsCase: formValue
            })
          }
        }
      }
      this.getUpdatedThisProject();
    }
    onVolumeChange(index){
      this.getUpdatedThisProject();
    }

    // Deliverables updating Values
    updateThisProjectNSVCase(){
      let [nsvPerCase, annualizedTotal, annualVolumneTotal] = [0,0,0];

      for(let [i,formGroup] of this.marketScopeFormArray.controls.entries()){
        const currencyConverter = +this.dataSource[i].harmonisedMarket.ConversionRateEURO;
        const annualisedNsvVal = +formGroup['controls'].annualisedNsv.value;
        const directlyEnterEuro = formGroup['controls'].directlyEnterCurrencyInEuro.value
        // const annualisedEuroVal = directlyEnterEuro ? parseFloat(formGroup['controls'].annualisedEuro.value) :  parseFloat(formGroup['controls'].annualisedEuro.value?.split('€ ')[1]);
        const annualisedEuroVal = formGroup['controls'].annualisedEuro.value?.includes('€ ') ? formGroup['controls'].annualisedEuro.value?.split('€ ')[1] : formGroup['controls'].annualisedEuro.value
        // const annualisedEuroVal = directlyEnterEuro ? parseFloat(formGroup['controls'].annualisedEuro.value) :  parseFloat(formGroup['controls'].annualisedEuro.value?.split('€ ')[1]);
        const annualisedEuroNum =  +annualisedEuroVal
        // const annualisedEuro = this.enterAnnulaisedEuro
        const annualVolumneVal = +formGroup['controls'].annualVolumne.value;
        if(annualisedEuroNum && annualVolumneVal) {
        annualizedTotal = annualizedTotal + (annualisedEuroNum);
        annualVolumneTotal = annualVolumneTotal + annualVolumneVal;
      }
      }
      if(annualizedTotal && annualVolumneTotal){
        nsvPerCase = annualizedTotal / annualVolumneTotal;
      }
      this.thisProjectData['nsvPerCase'] = nsvPerCase ? nsvPerCase.toFixed(3) : 0;
    }
    updateThisProjectCogsCase(){
      let cogs = 0;

      for(let [i,formGroup] of this.marketScopeFormArray.controls.entries()){
        const cogsCase = +formGroup['controls'].cogsCase.value;
        if(cogsCase) {
          cogs = cogsCase;
        }
      }
      this.thisProjectData['cogs'] = cogs ? cogs.toFixed(3) : 0;
    }
    updateThisProjectGm(){
      let gM = 0;
        if(this.thisProjectData['nsvPerCase'] && this.thisProjectData['cogs']) {
          gM = ((this.thisProjectData['nsvPerCase'] - this.thisProjectData['cogs']) / this.thisProjectData['nsvPerCase'])* 100;
        }
          this.thisProjectData['gM'] = gM ? `${gM.toFixed(3)}%` : 0;
    }
    updateThisProjectVolume(){
      let volumeSum:any = 0;
      for(let [i,formGroup] of this.marketScopeFormArray.controls.entries()){
        volumeSum = volumeSum + +formGroup.value.annualVolumne;
      }
      this.thisProjectData['volume'] = volumeSum;
    }


    updateFormControlsDisability() {
     for(let formControls of this.marketScopeFormArray.controls){
       this.monsterUtilService.disableFormControls(formControls,this.marketScopeDataConfig);
     }
    }
    /**
   * To load market scope and set form groups
   */
  retrieveMarketScopeContents() {
    this.loaderService.show();
    this.loadingMarketScope = true;
    this.entityService.getEntityItemDetails(this.requestDetails.requestItemId,
      EntityListConstant[this.properties.requestMarketScopeRelation]).subscribe((marketScopeResponse) => {
        // create form groups for market scope
        // retrieveRequest
        if (marketScopeResponse && marketScopeResponse.items) {
          this.mapMarketScopeContentToForm(marketScopeResponse.items);
          this.loaderService.hide();
        }
        this.loadingMarketScope = false;
      }, (error) => {
        this.loaderService.hide();
        this.loadingMarketScope = false;
      });
  }

    mapMarketScopeContentToForm(marketScope) {
      for (let i = 0; i < marketScope.length; i++) {
        const options = {
          displayName: marketScope[i][this.properties.R_PO_MARKET]?.Properties?.DisplayName,
          value: marketScope[i][this.properties.R_PO_MARKET]?.Properties?.Value,
          ItemId: marketScope[i][this.properties.R_PO_MARKET]?.Identity?.ItemId,
          sequence: marketScope[i][this.properties.R_PO_MARKET]?.Properties?.Sequence,//to sort dropdown values based on it
          businessUnit : marketScope[i][this.properties.R_PO_MARKET] && marketScope[i][this.properties.R_PO_MARKET].R_PO_BUSINESS_UNIT && marketScope[i][this.properties.R_PO_MARKET].R_PO_BUSINESS_UNIT.Identity,
          regulatoryClassification : marketScope[i][this.properties.R_PO_MARKET]?.Properties?.RegulatoryClassification,
          ConversionRateEURO: marketScope[i][this.properties.R_PO_MARKET]?.Properties?.ConversionRateEURO,
        };
        this.dataSource.push(
          this.properties.requestType === 'commercial' ? {
            harmonisedMarket: options,
            nsvCase: marketScope[i].Properties.NSVCase,
            cogsCase: marketScope[i].Properties.COGCase,
            grossMargin: marketScope[i].Properties.GrossMargin,
            grossProfit: marketScope[i].Properties.GrossProfit ? marketScope[i].Properties.GrossProfit : 0,
            leadMarket: marketScope[i].Properties.LeadMarket,
            directlyEnterCurrencyInEuro: marketScope[i].Properties.EnterCurrencyDirectlyInEuros,
          } : this.properties.requestType === 'operations' ? {
            harmonisedMarket: options,
            leadMarket: marketScope[i].Properties.Lead_Market,
          } :{
            harmonisedMarket: options,
            leadMarket: marketScope[i].Properties.LEAD_MARKET,
          });
        this.marketScopeFormArray.push(this.formBuilder.group( this.properties.requestType === 'commercial' ? {
          threeMonthLauchVolume:[ {value: marketScope[i].Properties.Three_Month_LaunchVolume, disabled: this.marketScopeDataConfig.threeMonthLauchVolume.disabled}, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
          annualVolumne:[ {value: marketScope[i].Properties.AnnualisedYear1Volume, disabled: this.marketScopeDataConfig.annualVolumne.disabled}, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
          cannibalisationNImpact:[ {value: marketScope[i].Properties.Cannibilisation_Impact, disabled: this.marketScopeDataConfig.cannibalisationNImpact.disabled}, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
          nsvCase:[{value: marketScope[i].Properties.NSVCase ,disabled: this.marketScopeDataConfig.nsvCase.disabled}, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
          cogsCase:[{value: marketScope[i].Properties.COGCase ,disabled:(marketScope[i].Properties.LeadMarket && this.requestDetails.status=='InDraft') ? false : true}, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
          annualisedNsv:[ {value: marketScope[i].Properties.AnnualisedYear1NSV_LocalCurrency, disabled:(marketScope[i].Properties.EnterCurrencyDirectlyInEuros || this.marketScopeDataConfig.annualisedNsv.disabled) ? true :false}, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
          annualisedEuro:[{value: marketScope[i].Properties.AnnualisedYear1NSV_EURO ,disabled: ((!marketScope[i].Properties.EnterCurrencyDirectlyInEuros) || this.marketScopeDataConfig.annualisedEuro.disabled)? true : false}, [Validators.required,Validators.maxLength(64)]],
          targetDP: [{value:marketScope[i].Properties.TargetDPInWarehouse, disabled: this.marketScopeDataConfig.targetDP.disabled}, [Validators.required,Validators.pattern('^([1-9]|[1-4][0-9]|5[0-2])\/[0-9]{4}$')]],
          directlyEnterCurrencyInEuro: [{value: marketScope[i].Properties.EnterCurrencyDirectlyInEuros , disabled: this.marketScopeDataConfig.directlyEnterCurrencyInEuro.disabled}],
          itemId: [marketScope[i].Identity.ItemId],
        } : this.properties.requestType === 'operations' ? {
          threeMonthLauchVolume:[ {value: marketScope[i].Properties.Three_Month_Launch_Volume, disabled: this.marketScopeDataConfig.threeMonthLauchVolume.disabled}, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
          annualVolumne:[ {value: marketScope[i].Properties.AnnualisedYear1Volume, disabled: this.marketScopeDataConfig.annualVolumne.disabled}, [Validators.required,Validators.pattern(/^(?!0\d)\d*(\.\d+)?$/),Validators.maxLength(64)]],
          targetDP: [{value:marketScope[i].Properties.TargetDPInWarehouse, disabled: this.marketScopeDataConfig.targetDP.disabled}, [Validators.required,Validators.pattern('^([1-9]|[1-4][0-9]|5[0-2])\/[0-9]{4}$')]],
          itemId: [marketScope[i].Identity.ItemId]
        } : {
          targetDP: [{value:marketScope[i].Properties.TargetDPInWarehouse, disabled: this.marketScopeDataConfig.targetDP.disabled}, [Validators.required,Validators.pattern('^([1-9]|[1-4][0-9]|5[0-2])\/[0-9]{4}$')]],
          itemId: [marketScope[i].Identity.ItemId]
        }
        ));
        //NPD1-83
        if(marketScope[i].Properties.TargetDPInWarehouse && marketScope[i].Properties.TargetDPInWarehouse!==undefined){
          let week = marketScope[i].Properties.TargetDPInWarehouse.split("/")[0];
          let year = marketScope[i].Properties.TargetDPInWarehouse.split("/")[1];

          let weekdates:NgbDate[] = this.calculateDate(week,year);

          this.fromTargetDate[i] = weekdates[0];
          this.toTargetDate[i] = weekdates[1];
        }

        //NPD1-83
        if(marketScope[i].Properties.TargetDPInWarehouse && marketScope[i].Properties.TargetDPInWarehouse!==undefined){
          let week = marketScope[i].Properties.TargetDPInWarehouse.split("/")[0];
          let year = marketScope[i].Properties.TargetDPInWarehouse.split("/")[1];

          let weekdates:NgbDate[] = this.calculateDate(week,year);

          this.fromTargetDate[i] = weekdates[0];
          this.toTargetDate[i] = weekdates[1];
        }

      }
      this.dataSource.forEach((ele,j) => {
        if(ele.leadMarket === true){
          this.getUpdatedMonsterGreen(j);
          this.updateRegulatoryLocations(j)
        }
      })
      this.renderMSTableRows();
      this.updateMultiMarkets();

    }

    getRowHeight(i){
      let height = '';
      // if(this.marketScopeTable){
      //   return height = `${this.marketScopeTable['_elementRef'].nativeElement.children[ i + 1].clientHeight}px`;
      // }
      let arr = [];
      if(this.properties.requestType === 'commercial') {
        // arr = ['threeMonthLauchVolume',
        // 'annualVolumne','cannibalisationNImpact','nsvCase','cogsCase','annualisedNsv','directlyEnterCurrencyInEuro','annualisedEuro'];
          arr = ['threeMonthLauchVolume',
        'annualVolumne','cannibalisationNImpact','nsvCase','cogsCase','annualisedNsv','annualisedEuro'];
      } else if(this.properties.requestType === 'operations') {
        arr = ['threeMonthLauchVolume','annualVolumne'];
      }
      const flag = arr.some(ele => {
        if(this.marketScopeFormArray.controls[i]['controls'][ele].touched){
          return this.marketScopeFormArray.controls[i]['controls'][ele].errors;
        }else {
          return false
        }
      });
      if(flag){
        return height = '120px';
      } else {
        return height = '70px';
      }
    }

    sendTechnicalCopyMarketScopeDetails(copiedMarketScopeDetails){
      copiedMarketScopeDetails.items.forEach(item => {
        this.selectedMultiMarket.push({
          displayName:item['R_PO_MARKET'].Properties.DisplayName,
          leadMarket:item.Properties.LEAD_MARKET,
          targetDp:item.Properties.TargetDPInWarehouse,
          // annualisedYear1:item.Properties?.AnnualisedYear1Volume,
          // threeMonthLaunch:item.Properties?.Three_Month_Launch_Volume
        })
      })
      this.getDataSource(true);
    }

    copyOperationRequest(copiedMarketScopeDetails){
      copiedMarketScopeDetails.items.forEach(item => {
        this.selectedMultiMarket.push({
          displayName:item['R_PO_MARKET'].Properties.DisplayName,
          leadMarket:item.Properties.Lead_Market,
          targetDp:item.Properties.TargetDPInWarehouse,
          annualisedYear1:item.Properties.AnnualisedYear1Volume,
          threeMonthLaunch:item.Properties.Three_Month_Launch_Volume,
        })
      })
      this.getDataSource(true);
    }
    copyCommercialRequest(copiedMarketScopeDetails){
      copiedMarketScopeDetails.items.forEach(item => {
        this.selectedMultiMarket.push({
          displayName:item['R_PO_MARKETS'].Properties.DisplayName,
          leadMarket:item.Properties.LeadMarket,
          targetDp:item.Properties.TargetDPInWarehouse,
          annualisedYear1:item.Properties.AnnualisedYear1Volume,
          threeMonthLaunch:item.Properties.Three_Month_LaunchVolume?item.Properties.Three_Month_LaunchVolume :item.Properties.Three_Month_Launch_Volume,
          cannabalisationImpact:item.Properties.Cannibilisation_Impact,
          nsvCase:item.Properties.NSVCase,
          cogCase:item.Properties.COGCase,
          annualisedYear1LocalCurrency:item.Properties.AnnualisedYear1NSV_LocalCurrency,
          annualisedYear1Euro:item.Properties.AnnualisedYear1NSV_EURO,
          grossProfit:item.Properties.GrossProfit,
          grossMargin:item.Properties.GrossMargin,
          directlyEnterEuro:item.Properties.EnterCurrencyDirectlyInEuros,
        })
      })
      this.getDataSource(true);
    }

    // To enable the Euro field, so user can enter currency directly in EURO
    onCheckDirectlyEnterCurrency(formControl,localCurrency,formName,propertyValue?,index?){      if(propertyValue){
        this.marketScopeFormArray.controls[index]['controls'].annualisedNsv.disable();
        this.marketScopeFormArray.controls[index].patchValue({
          annualisedNsv: localCurrency,
        })
        this.marketScopeFormArray.controls[index]['controls'].annualisedEuro.enable();
        this.updateRequestInfo('annualisedEuro',formName,null,index)
        this.updateRequestInfo('annualisedNsv',formName,null,index)
      }
      else{
        this.marketScopeFormArray.controls[index].patchValue({
          annualisedEuro: localCurrency
        })
        this.marketScopeFormArray.controls[index]['controls'].annualisedNsv.enable();
        this.marketScopeFormArray.controls[index]['controls'].annualisedEuro.disable();
        this.updateNSVPerCase(index);
         this.updateGrossProfit(index);
         this.updateGrossMargin(index)
      }
      this.updateRequestInfo(formControl,formName,propertyValue,index)
    }

    //On entry of euro currency
    updateAnnualisedEuro(index,formValue){

      this.marketScopeFormArray.controls[index].patchValue({
        annualisedEuro: formValue
      })
      this.updateRequestInfo('annualisedNsv','dynamicMarketScopeForm',null,index);
      this.updateRequestInfo('annualisedEuro','dynamicMarketScopeForm',null,index);
    }

    onFocusOut(formValue,index){
      if(formValue){
      this.marketScopeFormArray.controls[index].patchValue({
          annualisedEuro: `€ ${parseFloat(formValue).toFixed(2)}`
        })
        }
        else{
            this.marketScopeFormArray.controls[index].patchValue({
            annualisedEuro : ''
          })
        }
     }

    onFocus(formValue,index){
      if(formValue){
        this.marketScopeFormArray.controls[index].patchValue({
          annualisedEuro : parseFloat(formValue?.split('€ ')[1])
        })
      }else{
        this.marketScopeFormArray.controls[index].patchValue({
          annualisedEuro : ''
        })
      }
    }
}
