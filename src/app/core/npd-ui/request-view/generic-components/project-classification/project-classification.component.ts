import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, RequiredValidator, Validators } from '@angular/forms';
import { forkJoin, Observable } from 'rxjs';
import { AppService, LoaderService, NotificationService } from 'mpm-library';
import { BusinessConstants } from '../../constants/BusinessConstants';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { RequestService } from '../../../services/request.service';
import { NpdProjectService } from '../../../npd-project-view/services/npd-project.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-project-classification',
  templateUrl: './project-classification.component.html',
  styleUrls: ['./project-classification.component.scss']
})
export class ProjectClassificationComponent implements OnInit {
  @Input() config;
  @Input() requestDetails;
  @Input() projectClassificationDataConfig;
  @Input() request;
  @Input() projectType;
  @Input() isNewRequest;
  @Input() ccopiedRequestDetails;
  isDraftManufactureDataLoad
  @Input() LeadMarketRegion;
  public leadMarket = '';
  @Output() getLeadMArketRegion = new EventEmitter<any>();
  public bussinessUnits = [];
  public allMarkets = [];

  earlyProjectClassificationVal;
  selectedProjectClassification;
  projectClassificationDataForm: FormGroup;
  readonly radioButtonsList = BusinessConstants.radioButtons;

  requestId: any;
  selectedCorpPM;
  selectedOpsPM;
  selectedProgramTag;
  selectedRTMUser;
  selectedSecondaryAWUser;
  selectedE2EPM;
  selectedProjectType;
  selected2ndaryPackaging;
  corpPMUsers = [];
  opsPMUsers = [];
  selectedCorp;
  selectedOps;
  requestStatus;
  earlyManufacturingSites = [];
  draftManufacturingLocations = [];
  projectClassificationParams: any = {
    projectType: null,
    projectClassificationScore: null
  }
  regulatoryTimelineForLocations = {
    market: null,
    manufacturingLocation: null
  }
  programTags: any[];
  rtmUsers: any[];
  secondaryAWUsers: any[];
  e2ePmUsers: any[];
  selectionBasedProjectTypes: string[] = ['Lift & Shift', 'On Pack Promo'];
  allDraftManufacturingLocations:any[];

  constructor(
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private requestService: RequestService,
    private monsterUtilService: MonsterUtilService,
    private monsterConfigApiService: MonsterConfigApiService,
    private loaderService: LoaderService,
    private npdProjectService: NpdProjectService
  ) {


  }

  get classificationDataForm() {
    return this.projectClassificationDataForm?.controls
  }

  ngOnInit(): void {
    this.getBusinessUnits();
    this.getAllMarkets();
    this.getDraftManufacturingLocation();
    
    if (this.projectClassificationDataConfig) {//this.request &&
      this.createForm();
      this.getData();
      if (!this.isNewRequest) {
        if (this.projectClassificationDataForm?.controls?.draftManufacturingLocation?.value)
          this.getEarlyManufacturingSites(this.projectClassificationDataForm.controls.draftManufacturingLocation.value).subscribe();
      }
    }
    this.disableLeadFormulaField(true);
    
  }
  ngAfterViewInit() {
  }

  ngOnChanges() {
    


    // this.selectedProjectType =this.config.projectTypes.find(x=>x.ItemId==this.projectClassificationDataForm.value.projectType)
    if (this.ccopiedRequestDetails) {
      this.projectClassificationDataForm.controls.draftManufacturingLocation.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_DRAFT_MANUFACTURING_LOCATION$Identity.ItemId);
      this.projectClassificationDataForm.controls.earlyManufacturingSite.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_EARLY_MANUFACTURING_SITE$Identity.ItemId);
      this.projectClassificationDataForm.controls.projectType.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_PROJECT_TYPE$Identity.ItemId);
    }
  }

  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(this.projectClassificationDataForm, this.projectClassificationDataConfig);
  }
  getBusinessUnits() {
    this.monsterConfigApiService.getAllBusinessUnits().subscribe((response) => {
     
      this.bussinessUnits = response;
    }, (error) => {
    });
  }
  getAllMarkets() {
    this.monsterConfigApiService.getAllMarkets().subscribe(res => {
     
      this.allMarkets = res;
    })
  }

  setProjectClassification(property, value) {
    this.projectClassificationParams.projectType = value?.split('.')[1];
    this.selectedProjectType = this.config.projectTypes.find(x => x.ItemId == value);
    this.disableLeadFormulaField();
  }

  //this.projectClassification.emit({property: property, value: value && value.split('.')[1]});

  createForm() {
    /*     if(this.projectType?.value) {
          this.projectClassificationParams.projectType = this.projectType.value.split('.')[1];
        } */
    this.projectClassificationDataForm = this.formBuilder.group({});
    Object.keys(this.projectClassificationDataConfig).filter(formControlConfigName => {
      if (formControlConfigName && this.projectClassificationDataConfig[formControlConfigName].ngIf && 'formControl' in this.projectClassificationDataConfig[formControlConfigName] && this.projectClassificationDataConfig[formControlConfigName]['formControl'] === true) {
        let validators = [];
        if (this.projectClassificationDataConfig[formControlConfigName].validators) {
          if (this.projectClassificationDataConfig[formControlConfigName].validators.required) {
            validators.push(Validators.required);
          }
          if (this.projectClassificationDataConfig[formControlConfigName].validators.length && this.projectClassificationDataConfig[formControlConfigName].validators.length.max) {
            validators.push(Validators.maxLength(this.projectClassificationDataConfig[formControlConfigName].validators.length.max));
          }
        }
        if( formControlConfigName=='registrationClassificationAvailable' || formControlConfigName=='registrationRequirement'){
          this.projectClassificationDataForm.addControl(formControlConfigName, new FormControl({
            value: this.projectClassificationDataConfig[formControlConfigName].value=='true' ||this.projectClassificationDataConfig[formControlConfigName].value==true ?'Yes':'No' ,
            disabled: this.projectClassificationDataConfig[formControlConfigName].disabled
          }, validators));

        }
        if(formControlConfigName=='leadFormula'){
          this.projectClassificationDataForm.addControl(formControlConfigName, new FormControl({
            value: this.projectClassificationDataConfig[formControlConfigName].value=='NA' || this.projectClassificationDataConfig[formControlConfigName].value=='' ?false:this.projectClassificationDataConfig[formControlConfigName].value ,
            disabled: this.projectClassificationDataConfig[formControlConfigName].disabled
          }, validators));
        }
          else this.projectClassificationDataForm.addControl(formControlConfigName, new FormControl({
          value: this.projectClassificationDataConfig[formControlConfigName].value,
          disabled: this.projectClassificationDataConfig[formControlConfigName].disabled
        }, validators));{

        }

      }

    });
    if (this.projectClassificationDataForm?.controls?.projectType?.value) {
      this.projectClassificationParams.projectType = this.projectClassificationDataForm.controls.projectType.value?.split('.')[1];
      this.selectedProjectType = this.config.projectTypes.find(type => type.ItemId === this.projectClassificationDataForm?.controls?.projectType?.value);
      
    }
    
    if (this.projectClassificationDataForm?.controls?.secondaryPackaging?.value) {
      this.selected2ndaryPackaging = this.projectClassificationDataForm?.controls?.secondaryPackaging?.value;
    }
    /*  if(this.projectClassificationDataForm?.controls?.draftManufacturingLocation?.value) {
       this.isDraftManufactureDataLoad = true;
       this.regulatoryTimelineForLocations.manufacturingLocation = this.projectClassificationDataForm.controls.draftManufacturingLocation.value?.split('.')[1];
     } */
    /*    this.projectClassificationDataForm = this.formBuilder.group({
          earlyProjectClassification: [{ value: this.projectClassificationDataConfig.earlyManufacturingSite.value,disabled: this.projectClassificationDataConfig.earlyProjectClassification.disabled }],
          earlyManufacturingSite: [{ value: this.projectClassificationDataConfig.earlyManufacturingSite.value,disabled: this.projectClassificationDataConfig.earlyManufacturingSite.disabled }, Validators.required],
          draftManufacturingLocation: [{ value: this.projectClassificationDataConfig.draftManufacturingLocation.value,
                                      disabled: this.projectClassificationDataConfig.draftManufacturingLocation.disabled }, Validators.required],
          newFg: [{ value: this.projectClassificationDataConfig.newFg.value,
            disabled: this.projectClassificationDataConfig.newFg.disabled }, Validators.required],
          newRnDFormula: [{ value: this.projectClassificationDataConfig.newRnDFormula.value
            ,disabled: this.projectClassificationDataConfig.newRnDFormula.disabled }, Validators.required],
          newHBCFormula: [{ value: this.projectClassificationDataConfig.newHBCFormula.value
            ,disabled: this.projectClassificationDataConfig.newHBCFormula.disabled }, Validators.required],
          earlyProjectClassificationDesc: [{ value: this.projectClassificationDataConfig.earlyProjectClassificationDesc.value
            ,disabled: this.projectClassificationDataConfig.earlyProjectClassificationDesc.disabled }],
          newPrimaryPackaging: [{ value: this.projectClassificationDataConfig.newPrimaryPackaging.value
            ,disabled: this.projectClassificationDataConfig.newPrimaryPackaging.disabled }, Validators.required],
          secondaryPackaging: [{ value: this.projectClassificationDataConfig.secondaryPackaging.value
            ,disabled: this.projectClassificationDataConfig.secondaryPackaging.disabled }, Validators.required],
          registrationDossier: [{ value: this.projectClassificationDataConfig.registrationDossier.value
            ,disabled: this.projectClassificationDataConfig.registrationDossier.disabled }],
          preProdReg: [{ value: this.projectClassificationDataConfig.preProdReg.value
            ,disabled: this.projectClassificationDataConfig.preProdReg.disabled }],
          postProdReg: [{ value: this.projectClassificationDataConfig.postProdReg.value
            ,disabled: this.projectClassificationDataConfig.postProdReg.disabled }],
          techDesc: [{ value: this.projectClassificationDataConfig.techDesc.value
            ,disabled: this.projectClassificationDataConfig.techDesc.disabled }],
          corpPm: [{ value: this.projectClassificationDataConfig.corpPm.value
            ,disabled: this.projectClassificationDataConfig.corpPm.disabled }, Validators.required],
          opsPm: [{ value: this.projectClassificationDataConfig.opsPm.value
            ,disabled: this.projectClassificationDataConfig.opsPm.disabled }, Validators.required]
        }); */
  }

  getData() {
    forkJoin([this.getDraftManufacturingLocation(), this.getCorpPMUsers(), this.getOpsPMUsers(), this.getProgramTags(), this.getRTMUsers(), this.getSecondaryAWUsers(),this.getE2EPMUSers()])
      .subscribe(responseList => {
        if (this.request && this.projectClassificationDataConfig?.corpPm?.value) {
          this.selectedCorpPM = this.formCorpValue(this.request);
        }
        if (this.request && this.projectClassificationDataConfig?.opsPm?.value) {
          this.selectedOpsPM = this.formOpsValue(this.request);
        }
        if (this.request && this.projectClassificationDataConfig?.programTag?.value) {
          this.selectedProgramTag = this.formProgramTagValue(this.request);
        }
        if (this.request && this.projectClassificationDataConfig?.rtmUser?.value) {
          this.selectedRTMUser = this.formRTMUserValue(this.request);
        }
        if (this.request && this.projectClassificationDataConfig?.secondaryAWUser?.value) {
          this.selectedSecondaryAWUser = this.formSecondaryAWUser(this.request);
        }if(this.request && this.projectClassificationDataConfig?.e2ePm?.value){
          this.selectedE2EPM=this.formE2EPMValue(this.request)
        }
        this.getLeadMArketRegion.emit();

      }, () => {
        console.log('Error while loading the form configs.')
      });
  }

  /*   getDraftManufacturingLocation() : Observable<any> {
      return new Observable(observer => {
        this.monsterConfigApiService.getAllDraftManufacturingLocations().subscribe((response) => {
          this.draftManufacturingLocations = this.monsterConfigApiService.transformResponse(response,"Draft_Manufacturing_Location-id",true);
          observer.next(true);
          observer.complete();
        },(error)=> {
          observer.error(error);
        });
      });
    } */
  getDraftManufacturingLocation(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllDraftManufacturingLocations().subscribe((response) => {
        this.allDraftManufacturingLocations=response;
        let draftManufacturingLocationOptions = [];
        draftManufacturingLocationOptions = response;
        draftManufacturingLocationOptions.forEach(draftManufacturingLocation => {
          let location = {
            ItemId: draftManufacturingLocation?.Identity?.ItemId,
            Id: draftManufacturingLocation?.Identity?.Id,
            displayName: draftManufacturingLocation?.['R_PO_MARKET$Properties']?.DisplayName,
            regulatoryClassification: draftManufacturingLocation?.['R_PO_MARKET$Properties']?.RegulatoryClassification
          }
          this.draftManufacturingLocations.push(location);
        });
        if (this.projectClassificationDataForm?.controls?.draftManufacturingLocation?.value) {
          this.isDraftManufactureDataLoad = true;
          let selectedDraftManufacturingLocation = this.draftManufacturingLocations.find(manufacturingLocation => manufacturingLocation.ItemId == this.projectClassificationDataForm.controls.draftManufacturingLocation.value)
          //let data = {property :'manufacturingLocation',value:selectedDraftManufacturingLocation?.regulatoryClassification,isDataLoad : false }//draftManufacturingLocation?.split('.')[1]
          this.regulatoryTimelineForLocations.manufacturingLocation = selectedDraftManufacturingLocation?.regulatoryClassification;
          //this.projectClassificationDataForm.controls.draftManufacturingLocation.value?.split('.')[1];
        }
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }

  onDraftManufactureLocationChange(draftManufacturingLocation, formName, property) {
    this.earlyManufacturingSites = [];
    this.loaderService.show();
    this.projectClassificationDataForm.patchValue({
      earlyManufacturingSite: ''
    });
    this.updateRequestRelationsInfo('earlyManufacturingSite', formName);
    this.isDraftManufactureDataLoad = false;
    let selectedDraftManufacturingLocation = this.draftManufacturingLocations.find(manufacturingLocation => manufacturingLocation.ItemId == draftManufacturingLocation)
    let data = { property: 'manufacturingLocation', value: selectedDraftManufacturingLocation?.regulatoryClassification, isDataLoad: false }//draftManufacturingLocation?.split('.')[1]
    // this.getRegulatoryTimeLineLocations(data);.
    this.getClassificationChange(false);
    this.getEarlyManufacturingSites(draftManufacturingLocation).subscribe(response => {
      this.loaderService.hide();
    }, (error) => {
      this.loaderService.hide();
    });
  }


  getEarlyManufacturingSites(draftManufacturingLocation): Observable<any> {
    let draftManufacturingRequestParams = {
      'Draft_Manufacturing_Location-id': {
        Id: draftManufacturingLocation && draftManufacturingLocation.split('.')[1],
        ItemId: draftManufacturingLocation
      }
    }
    return new Observable(observer => {
      this.monsterConfigApiService.fetchEarlyManufacturingSitesByDraftManufacturingLocation(draftManufacturingRequestParams).subscribe((response) => {
        this.earlyManufacturingSites = this.monsterConfigApiService.transformResponse(response, "Early_Manufacturing_Site-id", true);
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });
  }



  getCorpPMUsers(): Observable<any> {
    return new Observable(observer => {
      this.requestService.getUsersByRoleForCorpPM().subscribe(response => {
        this.corpPMUsers = response.filter(corpPm => {
          return corpPm.isActive === 'true'
        });
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
        this.notificationService.error('Unable to get all the CORP PM.');
      });
    });
  }

  getE2EPMUSers():Observable<any>{
    return new Observable(observer => {
      this.npdProjectService.getAllE2EPMUsers().subscribe(response => {
        this.e2ePmUsers = response.filter(e2ePm => {
          return e2ePm.isActive === 'true'
        });
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
        this.notificationService.error('Unable to get all the CORP PM.');
      });
    });
  }

  getOpsPMUsers(): Observable<any> {
    return new Observable(observer => {
      this.requestService.getUsersByRoleForOpsPM().subscribe(response => {
        this.opsPMUsers = response.filter(opsPm => {
          return opsPm.isActive === 'true'
        });
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
        this.notificationService.error('Unable to get all the OPS PM.');
      });
    });
  }



  getProgramTags(): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.getAllProgramTags().subscribe((response) => {
        this.programTags = response;
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
        this.notificationService.error('Unable to get all the Program Tags.');
      })
    })
  }

  getRTMUsers(): Observable<any> {

    return new Observable(observer => {
      this.npdProjectService.getAllRTMUsers().subscribe((response) => {
        this.rtmUsers = response;

        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
        this.notificationService.error('Unable to get all the RTM Users.');
      })
    })
  }

  getSecondaryAWUsers(): Observable<any> {
    return new Observable(observer => {
      this.npdProjectService.getAllSecondaryArtworkSpecialists().subscribe((response) => {
        this.secondaryAWUsers = response;
        observer.next(true);
        observer.complete();
      }, (error) => {
        observer.error(error);
        this.notificationService.error('Unable to get all the Secondary AW Managers.');
      })
    })
  }

  corpPMSelection(formControl, formName) {
    if (this.selectedCorpPM) {
      this.requestService.getUserByID(this.selectedCorpPM.name).subscribe(response => {
        this.selectedCorpPM.userIdentity = response.result.items[0];
        let value = this.selectedCorpPM && this.selectedCorpPM.userIdentity && this.selectedCorpPM.userIdentity.Identity && this.selectedCorpPM.userIdentity.Identity.ItemId
        this.updateRequestRelationsInfo(formControl, formName, value);
      }, error => {

      });
    }
  }
  opsPMSelection(formControl, formName) {
    if (this.selectedOpsPM) {
      this.requestService.getUserByID(this.selectedOpsPM.name).subscribe(response => {
        this.selectedOpsPM.userIdentity = response.result.items[0];
        let value = this.selectedOpsPM && this.selectedOpsPM.userIdentity && this.selectedOpsPM.userIdentity.Identity && this.selectedOpsPM.userIdentity.Identity.ItemId
        this.updateRequestRelationsInfo(formControl, formName, value);
      }, error => {

      });
    }
  }

  rtmUserSelection(formControl, formName) {
    if (this.selectedRTMUser) {
      this.requestService.getUserByID(this.selectedRTMUser.name).subscribe(response => {
        this.selectedRTMUser.userIdentity = response.result.items[0];
        let value = this.selectedRTMUser && this.selectedRTMUser.userIdentity && this.selectedRTMUser.userIdentity.Identity && this.selectedRTMUser.userIdentity.Identity.ItemId
        this.updateRequestRelationsInfo(formControl, formName, value);
      }, error => {

      });
    }
  }

  secondaryAWUserSelection(formControl, formName) {
    if (this.selectedSecondaryAWUser) {
      this.requestService.getUserByID(this.selectedSecondaryAWUser.name).subscribe(response => {
        this.selectedSecondaryAWUser.userIdentity = response.result.items[0];
        let value = this.selectedSecondaryAWUser && this.selectedSecondaryAWUser.userIdentity && this.selectedSecondaryAWUser.userIdentity.Identity && this.selectedSecondaryAWUser.userIdentity.Identity.ItemId
        this.updateRequestRelationsInfo(formControl, formName, value);
      }, error => {

      });
    }
  }

  formCorpValue(request) {
    if (request && request.R_PO_CORP_PM$Identity && request.R_PO_CORP_PM$Identity.ItemId && request.R_PO_CORP_PM$Properties &&
      request.R_PO_CORP_PM$Properties.UserId) {
      const userIdentity = {
        Identity: {
          ItemId: request.R_PO_CORP_PM$Identity.ItemId,
        }
      };
      const selectedE2EPM = this.corpPMUsers.find(element => element.name === request.R_PO_CORP_PM$Properties.UserId);
      if (selectedE2EPM) {
        selectedE2EPM.userIdentity = userIdentity;
      }
      this.selectedCorp = request.R_PO_CORP_PM$Properties.UserId;
      return (selectedE2EPM) ? selectedE2EPM : { userIdentity: userIdentity };
    }
    return {};
  }
  formOpsValue(request) {
    if (request && request.R_PO_OPS_PM$Identity && request.R_PO_OPS_PM$Identity.ItemId && request.R_PO_OPS_PM$Properties &&
      request.R_PO_OPS_PM$Properties.UserId) {
      const userIdentity = {
        Identity: {
          ItemId: request.R_PO_OPS_PM$Identity.ItemId,
        }
      };
      const selectedE2EPM = this.opsPMUsers.find(element => element.name === request.R_PO_OPS_PM$Properties.UserId);
      if (selectedE2EPM) {
        selectedE2EPM.userIdentity = userIdentity;
      }
      this.selectedOps = request.R_PO_OPS_PM$Properties.UserId;
      return (selectedE2EPM) ? selectedE2EPM : { userIdentity: userIdentity };
    }
    return {};
  }

  formE2EPMValue(request) {
    
    if (request && request.R_PO_E2E_PM$Identity && request.R_PO_E2E_PM$Identity.ItemId && request.R_PO_E2E_PM$Properties &&
      request.R_PO_E2E_PM$Properties.UserId) {
      const userIdentity = {
        Identity: {
          ItemId: request.R_PO_E2E_PM$Identity.ItemId,
        }
      };
      const selectedE2EPM = this.e2ePmUsers.find(element => element.name === request.R_PO_E2E_PM$Properties.UserId);
      if (selectedE2EPM) {
        selectedE2EPM.userIdentity = userIdentity;
      }
      return (selectedE2EPM) ? selectedE2EPM : { userIdentity: userIdentity };
    } 
    return {};
  }

  formRTMUserValue(request) {
    if (request && request.R_PO_RTM$Identity && request.R_PO_RTM$Identity.ItemId && request.R_PO_RTM$Properties &&
      request.R_PO_RTM$Properties.UserId) {
      const userIdentity = {
        Identity: {
          ItemId: request.R_PO_RTM$Identity.ItemId,
        }
      };
      const rtmUser = this.rtmUsers.find(element => element.name === request.R_PO_RTM$Properties.UserId);
      if (rtmUser) {
        rtmUser.userIdentity = userIdentity;
      }
      this.selectedRTMUser = request.R_PO_RTM$Properties.UserId;
      return (rtmUser) ? rtmUser : { userIdentity: userIdentity };
    }
    return {};
  }

  formSecondaryAWUser(request) {
    if (request && request.R_PO_SECONDARY_AW_SPECIALIST$Identity && request.R_PO_SECONDARY_AW_SPECIALIST$Identity.ItemId && request.R_PO_SECONDARY_AW_SPECIALIST$Properties &&
      request.R_PO_SECONDARY_AW_SPECIALIST$Properties.UserId) {
      const userIdentity = {
        Identity: {
          ItemId: request.R_PO_SECONDARY_AW_SPECIALIST$Identity.ItemId,
        }
      };
      const secondaryAWUser = this.secondaryAWUsers.find(element => element.name === request.R_PO_SECONDARY_AW_SPECIALIST$Properties.UserId);
      if (secondaryAWUser) {
        secondaryAWUser.userIdentity = userIdentity;
      }
      this.selectedSecondaryAWUser = request.R_PO_SECONDARY_AW_SPECIALIST$Properties.UserId;
      return (secondaryAWUser) ? secondaryAWUser : { userIdentity: userIdentity };
    }
    return {};
  }

  formProgramTagValue(request) {
    if (request && request.R_PO_PROGRAM_TAG$Identity && request.R_PO_PROGRAM_TAG$Identity.ItemId && request.R_PO_PROGRAM_TAG$Properties &&
      request.R_PO_PROGRAM_TAG$Properties.DisplayName) {
      const userIdentity = {
        Identity: {
          ItemId: request.R_PO_PROGRAM_TAG$Identity.ItemId,
        }
      };
      const selectedE2EPM = this.programTags.find(element => element.Name === request.R_PO_PROGRAM_TAG$Properties.DisplayName);
      if (selectedE2EPM) {
        selectedE2EPM.userIdentity = userIdentity;
      }
      return (selectedE2EPM) ? selectedE2EPM : { userIdentity: userIdentity };
    }
    return {};
  }



  onChangeTotal() {
    let projectClasification = this.projectClassificationDataForm.getRawValue();
    const newFg = projectClasification.newFg === true ? 1000 : 0;//this.projectClassificationDataForm.value.newFg === true ? 1000 : 0;
    const newRnDFormula = projectClasification.newRnDFormula === true ? 10000 : 0;
    const newHBCFormula = projectClasification.newHBCFormula === true ? 100000 : 0;
    const newPrimaryPackaging = projectClasification.newPrimaryPackaging === true ? 1000000 : 0;
    const secondaryPackaging = projectClasification.secondaryPackaging === true ? 10000000 : 0;
    const registrationDossier = projectClasification.registrationDossier === true ? 100000000 : 0;
    const preProdReg = projectClasification.preProdReg === true ? 1000000000 : 0;
    const postProdReg = projectClasification.postProdReg === true ? 10000000000 : 0;

    return {
      formulaResult: newFg + newRnDFormula + newHBCFormula + newPrimaryPackaging + secondaryPackaging + registrationDossier + preProdReg + postProdReg,
      // lookUpResult: `${this.projectClassificationDataForm.value.earlyManufacturingSite}${this.projectClassificationDataForm.value.draftManufacturingLocation}`
    };


  }

  onValidate() {
    this.projectClassificationDataForm.markAllAsTouched();
    if (this.projectClassificationDataForm.invalid) {
      return true;
    } else {
      return false;
    }
  }

  // validateProjClassificationAutoPopulateFields() {
  //   // if (this.projectClassificationDataForm.controls?.registrationDossier &&
  //   //   this.projectClassificationDataForm.controls.preProdReg &&
  //   //   this.projectClassificationDataForm.controls.postProdReg) {
  //   //   if (this.projectClassificationDataForm.controls.registrationDossier.value == null &&
  //   //     this.projectClassificationDataForm.controls.preProdReg.value == null
  //   //     && this.projectClassificationDataForm.controls.postProdReg.value == null) {
  //   //     return true;
  //   //   } else {
  //   //     return false;
  //   //   }
  //   // }
  //   return false;
  //   /*     if(this.projectClassificationDataForm.controls.registrationDossier.invalid ||
  //       this.projectClassificationDataForm.controls.preProdReg.invalid ||
  //       this.projectClassificationDataForm.controls.postProdReg.invalid
  //       ) {

  //     } this.projectClassificationDataForm.controls.registrationDossier.value == true
  //     this.projectClassificationDataForm.controls.registrationDossier.value == false
  //     */
  // }

  checkValueIsEmpty(value) {
    if (value == '' || value == null) {
      return true;
    }
    return false
  }

  validateEarlyProjectClassificationFields() {
    let registrationClassificationAvailable:any=this.projectClassificationDataForm.controls.registrationClassificationAvailable?.value;
    if (this.projectClassificationDataForm.controls?.earlyProjectClassification && this.projectClassificationDataForm.controls?.earlyProjectClassificationDesc) {
      if (this.checkValueIsEmpty(this.projectClassificationDataForm.controls.earlyProjectClassification.value) ||
        this.checkValueIsEmpty(this.projectClassificationDataForm.controls.earlyProjectClassificationDesc.value) ||
        registrationClassificationAvailable!='Yes') {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }


  updateRequestInfo(formControl, formName, propertyValue?) {
    let property;
    let value;
    let itemId;

    if (formName === 'projectClassificationDataForm') {
      property = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
      if(formControl=='registrationClassificationAvailable'|| formControl=='registrationRequirement'){
        value = propertyValue || propertyValue=='Yes' ? true:false;
      }else{
        value = propertyValue ? propertyValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;

      }
      itemId = this.requestDetails && this.requestDetails.requestItemId;
    }
    this.monsterUtilService.setFormProperty(itemId, property, value);
  }

  updateRequestInfoByValue(formControl, formName, propertyValue) {
    let property;
    let value;
    let itemId;

    if (formName === 'projectClassificationDataForm') {
      property = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
      value = propertyValue;
      itemId = this.requestDetails && this.requestDetails.requestItemId;
    }
    this.monsterUtilService.setFormProperty(itemId, property, value);
  }

  getClassificationChange(isThrowableError?) {
    const totalScore = this.onChangeTotal();
    this.getProjectClassification({ property: 'projectClassificationScore', value: totalScore.formulaResult },isThrowableError);


  }
  setRegulatoryTimeLineLocations(data) {
    this.leadMarket = data?.value?.value;
    this.getLeadMArketRegion.emit();
    this.getClassificationChange(false);
  }
  getRegulatoryTimeLineLocations(data) {


    // this.regulatoryTimelineForLocations[data.property] = data.value;
    // if (this.regulatoryTimelineForLocations.manufacturingLocation && this.regulatoryTimelineForLocations.market
    // ) {//&& !this.isDraftManufactureDataLoad && !this.regulatoryTimelineForLocations?.market?.isDataLoad
    // this.loaderService.show();
    // this.monsterConfigApiService.fetchRegulatoryTimelineForLocations(this.regulatoryTimelineForLocations).subscribe(
    //   response => {
    //     this.loaderService.hide();
    //     let registrationDossier = false;
    //     let preProdReg = false;
    //     let postProdReg = false;
    //     if (response && response?.['Regulatory_Timeline']) {

    //       let value = response['Regulatory_Timeline'];
    //       // registrationDossier = (value?.RegistrationPreparationDuration > 0) ? true : false;
    //       // preProdReg = (value?.PreProductionRegistartionDuration > 0) ? true : false;
    //       // postProdReg = (value?.PostProductionDuration > 0) ? true : false;
    //       /*  this.updateRequestInfoByValue('registrationDossier','projectClassificationDataForm',registrationDossier)
    //        this.updateRequestInfoByValue('preProdReg','projectClassificationDataForm',preProdReg)
    //        this.updateRequestInfoByValue('postProdReg','projectClassificationDataForm',postProdReg)
    //        this.getClassificationChange(); */

    //     } else {
    //       registrationDossier = null;
    //       preProdReg = null;
    //       postProdReg = null;
    //     }
    //     this.projectClassificationDataForm.patchValue(
    //       {
    //         registrationDossier: registrationDossier,
    //         preProdReg: preProdReg,
    //         postProdReg: postProdReg,
    //       });
    //     this.updateRequestInfoByValue('registrationDossier', 'projectClassificationDataForm', registrationDossier)
    //     this.updateRequestInfoByValue('preProdReg', 'projectClassificationDataForm', preProdReg)
    //     this.updateRequestInfoByValue('postProdReg', 'projectClassificationDataForm', postProdReg)
    //     if (this.projectClassificationDataForm?.controls?.registrationDossier && this.projectClassificationDataForm?.controls?.preProdReg
    //       && this.projectClassificationDataForm?.controls?.postProdReg) {
    //     }
    //   }, (error) => {
    //     this.loaderService.hide();
    //     console.log('Error while loading the Regulatory Timelines.')
    //   })
    // }
  }

  getProjectClassification(data,isThrowableError?) {
    
    this.projectClassificationParams[data.property] = +data.value;
    let formValue = this.projectClassificationDataForm.value;
    let regionId = 0;
    let countryOfSaleId = 0;
    let countryOfSaleRC="";
    let countryOfManufactureRc="";
   

    this.bussinessUnits.forEach(x => {
            if (x['DISPLAY_NAME'] == this.LeadMarketRegion) {
        regionId = x['R_PO_REGION']['Region-id']['Id'];
      }
    })
    this.allDraftManufacturingLocations.forEach(location=>{
      if(location['Identity']['Id']==formValue.draftManufacturingLocation?.split('.')[1]){
        countryOfManufactureRc=location['R_PO_MARKET$Properties']['RegulatoryClassification'];
      }
    })
    this.allMarkets.forEach(x => {
      if (x['Name'] == this.leadMarket) {
        countryOfSaleId = x['Markets-id']['Id'];
        countryOfSaleRC=x['RegulatoryClassification'];
      }
    })

    let param = {
      regionId: regionId,
      countryOfSaleId: countryOfSaleId,
      countryOfManufactureId: formValue.draftManufacturingLocation?.split('.')[1],
      projectTypeId: formValue.projectType?.split('.')[1],
      newFG: formValue.newFg,
      newRD: formValue.newRnDFormula,
      newHBC: formValue.newHBCFormula,
      primaryPackaging: formValue.newPrimaryPackaging,
      secondaryPackaging: formValue.secondaryPackaging,
      countryOfSaleRC: countryOfSaleRC,
      countryOfManufactureRC: countryOfManufactureRc,
      leadFormula: formValue.leadFormula

    }
   
    // if (param.newFG != null && param.newRD != null && param.newHBC != null && param.primaryPackaging != null && param.secondaryPackaging != null && param.countryOfSaleRC!=null && param.countryOfManufactureRC!=null) {
    //   this.generateClassificationNumber(param);
    // }
    this.generateClassificationNumber(param,isThrowableError);
    // this.getProjectClassificationNoDescription(this.projectClassificationParams).subscribe(response =>{
    //   response;
    // }, () => {
    //   console.log('Error while loading the form configs.')
    // });
  }
  // getProjectClassificationNoDescription(parameters): Observable<any> {
  //   return new Observable(observer => {
  //     this.monsterConfigApiService.fetchProjectClassificationNoDescription(parameters).subscribe((response) => {
  //       if (response.length > 0) {
  //         this.projectClassificationDataForm.patchValue({
  //           earlyProjectClassification: response[0].ProjectClassificationNumber,
  //           earlyProjectClassificationDesc: response[0].ProjectClassificationDescription,
  //         });
  //         this.selectedProjectClassification = response[0]['Project_Classifier_Data-id'];
  //       } else {
  //         this.projectClassificationDataForm.patchValue({
  //           earlyProjectClassification: '',
  //           earlyProjectClassificationDesc: '',
  //         });
  //         this.selectedProjectClassification = null;
  //       }
  //       this.updateRequestRelationsInfo('earlyProjectClassification', 'projectClassificationDataForm', null);
  //       observer.next(true);
  //       observer.complete();
  //     }, (error) => {
  //       observer.error(error);
  //     });
  //   });
  // }

  /**
   * GenerateClassificationNumber method is to fetch classification
   */
  public generateClassificationNumber(parameters,isThrowableError) {

    const notificationValidationArray = [
      parameters.newFG,
      parameters.newHBC,
      parameters.newRD,
      parameters.primaryPackaging,
      parameters.secondaryPackaging,
    ];
    const notificationValidation = notificationValidationArray.every(value => value !== null && value !== undefined);

    if(this.requestDetails?.requestSubjectType && this.requestDetails?.requestSubjectType=='E2E Review'){
      this.monsterConfigApiService.generateClassificationNumber(parameters,isThrowableError).subscribe(response => {
      
        if (response.length > 0) {
          this.projectClassificationDataForm.patchValue({
            earlyProjectClassification: notificationValidation?response[0].CLASSIFICATION_NUMBER:null,
            earlyProjectClassificationDesc:notificationValidation?response[0].CLASSIFICATION_DESCRIPTION:null,
            registrationClassificationAvailable: response[0]?.REGISTRATION_CLASSIFICATION_AVAILABLE == 'true' ? 'Yes' : 'No',
            registrationRequirement: response[0]?.REGISTRATION_REQUIREMENT == 'true' ? 'Yes' :'No'
          });
          this.selectedProjectClassification = response[0];
          this.updateRequestRelationsInfo('earlyProjectClassification', 'projectClassificationDataForm', response[0].CLASSIFICATION_NUMBER);
          // this.updateRequestRelationsInfo('earlyProjectClassificationDesc', 'projectClassificationDataForm', response[0].CLASSIFICATION_DESCRIPTION);
          this.updateRequestInfo('registrationClassificationAvailable', 'projectClassificationDataForm', response[0]?.REGISTRATION_CLASSIFICATION_AVAILABLE == 'true' ? true : false);
          this.updateRequestInfo('registrationRequirement', 'projectClassificationDataForm', response[0]?.REGISTRATION_REQUIREMENT == 'true' ? true : false);
  
          if(response[0]?.CLASSIFICATION_NUMBER==undefined && isThrowableError){
            if(notificationValidation){
              this.notificationService.error(
                'No Project Classification Data Available for the Input'
              );
            }
            
          }

        }
      }, error => {
        if(isThrowableError && notificationValidation){
          this.notificationService.error(
            'No Project Classification Data Available for the Input'
          );
        }
        this.projectClassificationDataForm.patchValue({
          earlyProjectClassification: null,
          earlyProjectClassificationDesc: null,
          registrationClassificationAvailable: 'No',
          registrationRequirement: 'No'
        });
        this.selectedProjectClassification = null;
        this.updateRequestRelationsInfo('earlyProjectClassification', 'projectClassificationDataForm', null);
        // this.updateRequestRelationsInfo('earlyProjectClassificationDesc', 'projectClassificationDataForm',null);
        this.updateRequestInfo('registrationClassificationAvailable', 'projectClassificationDataForm', false);
        this.updateRequestInfo('registrationRequirement', 'projectClassificationDataForm', false);
  
      })
    }
    


  }

  updateRequestRelationsInfo(formControl, formName, objValue?) {
    let relationName;
    let objectValue;
    let itemId;

    if (formName === 'projectClassificationDataForm') {
      relationName = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
      objectValue = objValue ? objValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl].value;
      itemId = this.requestDetails && this.requestDetails.requestItemId;

      if (formControl === ('earlyProjectClassification' || 'earlyProjectClassificationDesc')) {
        if (this.selectedProjectClassification?.ID) {
          objectValue = this.selectedProjectClassification && (environment.projectClassificationItemId + this.selectedProjectClassification.ID);
        }else{
          objectValue=null
        }

      }
    }


    this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
  }

  updateRequestRelationsIn(formControl, formName, objValue?) {
    let relationName;
    let objectValue;
    let itemId;

    if (formName === 'projectClassificationDataForm') {
      relationName = BusinessConstants.PROJECT_CLASSIFICATION_DATA_FORM[formControl];
      objectValue = objValue ? objValue : this.projectClassificationDataForm.controls[formControl] && this.projectClassificationDataForm.controls[formControl]?.value?.['Program_Tag-id']?.ItemId
      itemId = this.requestDetails && this.requestDetails.requestItemId;

      if (formControl === ('earlyProjectClassification' || 'earlyProjectClassificationDesc')) {
        objectValue = this.selectedProjectClassification && this.selectedProjectClassification.ID
      }
    }


    this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
  }

  getConditionValidators(formControl, condition): boolean {
    if (formControl === 'secondaryAWUser') {
      if (condition === false) {
        this.projectClassificationDataForm.get(formControl).clearValidators();
      }
      else {
        this.projectClassificationDataForm.get(formControl).setValidators([Validators.required]);
      }
      this.projectClassificationDataForm.get(formControl).updateValueAndValidity();
    }
    if (formControl === 'rtmUser') {
      if (condition === false) {
        this.projectClassificationDataForm.get(formControl).clearValidators();
      }
      else {
        this.projectClassificationDataForm.get(formControl).setValidators([Validators.required]);
      }
      this.projectClassificationDataForm.get(formControl).updateValueAndValidity();
    }

    return condition;
  }

  /**
   * @author Baba
   * @param onInItCall to check lead formula boolean field during opening of component
   * @returns based on the conditions to be applied to lead formula field returns with or without calling generateClassificationchange API
   */
  disableLeadFormulaField(onInItCall?){
    if(this.requestDetails?.requestSubjectType && this.requestDetails?.requestSubjectType=='E2E Review'){
      if(this.projectClassificationDataForm.get('newRnDFormula').value==true 
        && this.selectedProjectType.value==BusinessConstants.PROJECT_TYPES.COMMERCIAL_NEW_SKU){
          if(this.projectClassificationDataForm.get('newRnDFormula').enabled==true){
            this.projectClassificationDataForm.get('leadFormula').enable();
            this.projectClassificationDataForm.get('leadFormula').updateValueAndValidity()
          }
    }else{
      if(onInItCall){
        this.projectClassificationDataForm.get('leadFormula').disable();
        this.projectClassificationDataForm.get('leadFormula').updateValueAndValidity();
      }
      this.projectClassificationDataForm.get('leadFormula').setValue(false);
      this.projectClassificationDataForm.get('leadFormula').disable();
      this.projectClassificationDataForm.get('leadFormula').updateValueAndValidity();
      this.updateRequestInfo('leadFormula', 'projectClassificationDataForm', false);
    }
    if(onInItCall){
      return;
    }
    this.getClassificationChange(true)
    }
  }




}
