import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../constants/BusinessConstants';

@Component({
  selector: 'app-generic-form',
  templateUrl: './generic-form.component.html',
  styleUrls: ['./generic-form.component.scss']
})
export class GenericFormComponent implements OnInit {

 
  @Input() formConfig;
  @Input() request;
  @Input() ccopiedRequestDetails;
  genericForm : FormGroup;

  get formControls() { return this.genericForm.controls; };
  constructor(private formBuilder:FormBuilder,private monsterUtilService:MonsterUtilService) { }

  onValidate() {
    this.genericForm.markAllAsTouched();
    if(this.genericForm.invalid) {
      return true;
    } else {
      return false;
    }
  }
  
  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(this.genericForm,this.formConfig);
  }
  
  updateRequestInfo(formControl,formName,propertyValue?) {
    let property;
    let value;
    let itemId;

    if(formName === 'requestForm') {
      property = BusinessConstants.REQUEST_FORM[formControl];
      value = this.genericForm.controls[formControl] && this.genericForm.controls[formControl].value;
      itemId = this.request && this.request.requestItemId;
    }
    this.monsterUtilService.setFormProperty(itemId, property, value);
  }

  initializeForm() {

    this.genericForm = this.formBuilder.group({});

    Object.keys(this.formConfig).filter(formControlConfigName => {
      if(formControlConfigName && this.formConfig[formControlConfigName].ngIf && 'formControl' in this.formConfig[formControlConfigName] && this.formConfig[formControlConfigName]['formControl'] === true)  {
        let validators = [];
        if(this.formConfig[formControlConfigName].validators) {
          if(this.formConfig[formControlConfigName].validators.required) {
            validators.push(Validators.required);
          }
          if(this.formConfig[formControlConfigName].validators.length && this.formConfig[formControlConfigName].validators.length.max) {
             validators.push(Validators.maxLength(this.formConfig[formControlConfigName].validators.length.max));
          }
        }
        this.genericForm.addControl(formControlConfigName, new FormControl({ value: this.formConfig[formControlConfigName].value,
          disabled: this.formConfig[formControlConfigName].disabled },validators));
      }
    });
  }

 

  ngOnInit(): void {
    if(this.request && this.formConfig) {
      this.initializeForm();
    }
  }
  ngOnChanges(){
    if(this.ccopiedRequestDetails){
      this.genericForm.controls.businessCaseComments.setValue(this.ccopiedRequestDetails.result.items[0].Properties.BusinessCaseComments)
    }
  }

}
