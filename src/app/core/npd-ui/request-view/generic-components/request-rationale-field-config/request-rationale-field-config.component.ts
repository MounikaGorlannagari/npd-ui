import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { RequestService } from '../../../services/request.service';
import { BusinessConstants } from '../../constants/BusinessConstants';

@Component({
  selector: 'app-request-rationale-field-config',
  templateUrl: './request-rationale-field-config.component.html',
  styleUrls: ['./request-rationale-field-config.component.scss']
})
export class RequestRationaleFieldConfigComponent implements OnInit {

  @Input() rationaleConfig;
  @Input() request;
  @Input() requestDetails;
  @Input() ccopiedRequestDetails
  rationaleForm : FormGroup;

  get formControls() { return this.rationaleForm.controls; };
  constructor(private formBuilder:FormBuilder,private monsterUtilService:MonsterUtilService) { }

  onValidate() {
    this.rationaleForm.markAllAsTouched();
    if(this.rationaleForm.invalid) {
      return true;
    } else {
      return false;
    }
  }
  
  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(this.rationaleForm,this.rationaleConfig);
  }
  
  updateRequestInfo(formControl,formName,propertyValue?) {
    let property;
    let value;
    let itemId;

    if(formName === 'requestForm') {
      property = BusinessConstants.REQUEST_FORM[formControl];
      value = this.rationaleForm.controls[formControl] && this.rationaleForm.controls[formControl].value;
      itemId = this.request && this.request.requestItemId;
    }
    this.monsterUtilService.setFormProperty(itemId, property, value);
  }

  initializeForm() {

    this.rationaleForm = this.formBuilder.group({});

    Object.keys(this.rationaleConfig).filter(formControlConfigName => {
      if(formControlConfigName && this.rationaleConfig[formControlConfigName].ngIf && 'formControl' in this.rationaleConfig[formControlConfigName] && this.rationaleConfig[formControlConfigName]['formControl'] === true)  {
        let validators = [];
        if(this.rationaleConfig[formControlConfigName].validators) {
          if(this.rationaleConfig[formControlConfigName].validators.required) {
            validators.push(Validators.required);
          }
          if(this.rationaleConfig[formControlConfigName].validators.length && this.rationaleConfig[formControlConfigName].validators.length.max) {
             validators.push(Validators.maxLength(this.rationaleConfig[formControlConfigName].validators.length.max));
          }
        }
        this.rationaleForm.addControl(formControlConfigName, new FormControl({ value: this.rationaleConfig[formControlConfigName].value,
          disabled: this.rationaleConfig[formControlConfigName].disabled },validators));
      }
    });
  }

 

  ngOnInit(): void {
    if(this.request && this.rationaleConfig) {
      this.initializeForm();
    }
  }
  
  ngOnChanges(){
    if(this.ccopiedRequestDetails){
      this.rationaleForm.controls.requestRationale.setValue(this.ccopiedRequestDetails.result.items[0].Properties.Request_Rationale)
    }

  }

}
