import {
  Component,
  ViewChild,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
  OnInit,
} from "@angular/core";

import {
  NgbDate,
  NgbCalendar,
  NgbDatepicker
} from "@ng-bootstrap/ng-bootstrap";

import * as moment from 'moment';

@Component({
  selector: 'app-week-picker',
  templateUrl: './week-picker.component.html',
  styleUrls: ['./week-picker.component.scss'],
})
export class WeekPickerComponent implements OnInit, AfterViewInit {
  onTouched: any;
  model: any;
  onChangeValue: any;
  // fromDate: NgbDate //| null = null;
  // toDate: NgbDate //| null = null;

  //modal = { year: this.fromDate.year, week: value };
  week: any;
  @ViewChild(NgbDatepicker, { static: false }) datePicker:any;
  @Output() weekFormat = new EventEmitter<any>();
  @Input() weekYearValue;
  today = new Date();
  @Input() minDate;
  //@Input() isMaxDate;
  //@Input() maxDate;
  //minDate = {year: 2014, month: 5, day: 10};
  //maxDate = {year: 2018, month: 5, day: 10};

  @Input() fromDate: NgbDate;
  @Input() toDate: NgbDate;
  disabled: boolean = false;
  yearChangeFromNgbDate: NgbDate;
  yearChangeToNgbDate: NgbDate;
  currentDate: NgbDate;
  constructor(private ngbCalendar: NgbCalendar) {}
  ngOnInit(){
    
  }
  onDateSelection(date: NgbDate) {
    this.currentDate = date;
    //Changed this in NPD1-83 Safari issue
    let fromDate = new Date(`${date.year.toString()}-${date.month.toString().padStart(2,'0')}-${date.day.toString().padStart(2,'0')}`); 
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);

    let yearChangeFrom = new Date(`${date.year.toString()}-${date.month.toString().padStart(2,'0')}-${date.day.toString().padStart(2,'0')}`); 
    yearChangeFrom = new Date(yearChangeFrom.getTime() + 7 * 24 * 60 * 60 * 1000);
    yearChangeFrom = new Date(yearChangeFrom.getTime() - 7 * 24 * 60 * 60 * 1000);
    let Yeartime = yearChangeFrom.getDay() ? yearChangeFrom.getDay() - 1 : 6;
    yearChangeFrom = new Date(yearChangeFrom.getTime() - Yeartime * 24 * 60 * 60 * 1000);
    this.yearChangeFromNgbDate = new NgbDate(
      yearChangeFrom.getFullYear(),
      yearChangeFrom.getMonth() + 1,
      yearChangeFrom.getDate()
    );

    this.fromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );

    const earChangeToDate = new Date(yearChangeFrom.getTime() + 6 * 24 * 60 * 60 * 1000);

    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    this.yearChangeToNgbDate = new NgbDate(
      earChangeToDate.getFullYear(),
      earChangeToDate.getMonth() + 1,
      earChangeToDate.getDate()
    );

    this.toDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    if(this.onChangeValue){
      if (this.onChangeValue.day != date.day || this.onChangeValue.month == date.month || this.onChangeValue.year == date.year) {
        //this.week = this.calculateWeek(fromDate,date.day);
        let weekNo;
        if(date.year > 2021){
          weekNo = moment(earChangeToDate).isoWeek();
          // this.weekYearValue = ''+(this.calculateWeek(earChangeToDate) - 1)+'/'+date.year;//remove -1 
          this.weekYearValue = ''+ weekNo +'/'+date.year;//remove -1 
        } else {
          weekNo = moment(fromDate).isoWeek();
          // this.weekYearValue = ''+(this.calculateWeek(fromDate) - 1)+'/'+date.year;//remove -1 
           this.weekYearValue = ''+ weekNo +'/'+date.year;//remove -1 
        }
        this.weekFormat.emit(this.weekYearValue);
      } 
    }else{
      let weekNo;
      if(date.year > 2021){
        weekNo = moment(earChangeToDate).isoWeek();
        // this.weekYearValue = ''+(this.calculateWeek(earChangeToDate) - 1)+'/'+date.year;//remove -1 
        this.weekYearValue = ''+ weekNo +'/'+date.year;//remove -1 
      } else {
        weekNo = moment(fromDate).isoWeek();
        // this.weekYearValue = ''+(this.calculateWeek(fromDate) - 1)+'/'+date.year;//remove -1 
        this.weekYearValue = ''+ weekNo +'/'+date.year;//remove -1 
      }
      this.weekFormat.emit(this.weekYearValue);
    } 
  }
  isInside(date: NgbDate) {
    if(date.year > 2021){
      return date.after(this.yearChangeFromNgbDate) && date.before(this.yearChangeToNgbDate);
    } else {
      return date.after(this.fromDate) && date.before(this.toDate);
    }
  }

  isRange(date: NgbDate) {
    if(date.year > 2021){
      return (
        date.equals(this.yearChangeFromNgbDate) ||
        date.equals(this.yearChangeToNgbDate) ||
        this.isInside(date)
      );
    } else {
      return (
        date.equals(this.fromDate) ||
        date.equals(this.toDate) ||
        this.isInside(date)
      );
    }
  }

  calculateWeek(date: Date) {
    const time = date.getTime() + 4 * 24 * 60 * 60 * 1000;
    const firstDay = new Date(date.getFullYear() + "/1/1"); //NPD1-83 safari issue;
    return (
      Math.floor(Math.round((time - firstDay.getTime()) / 86400000) / 7) + 1
    );
  }

  // calculateWeek(date: any,day) {
  //   return moment(date).week();
  // }

  calculateDate(week: number, year: number) {
    const firstDay = new Date(year + "-1-4");
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    this.onDateSelection(selectDate);
  }

  ngAfterViewInit() {
    if (this.fromDate) {
      setTimeout(() => {
        this.datePicker.navigateTo(this.fromDate);
      });
    }
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  writeValue(value: any): void {
    if (value) {
      this.calculateDate(value.week, value.year);
    }
  }

}
