import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MONSTER_ROLES } from '../../../filter-configs/role.config';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { RequestService } from '../../../services/request.service';
import { BusinessConstants } from '../../constants/BusinessConstants';
@Component({
  selector: 'app-project-name-header',
  templateUrl: './project-name-header.component.html',
  styleUrls: ['./project-name-header.component.scss']
})
export class ProjectNameHeaderComponent implements OnInit {

  @Input() request;
  

  @Input() ccopiedRequestDetails;
  @Input() actionButtonsConfig;
  @Input() taskConfig

  @Output() preview = new EventEmitter<any>();
  @Output() submit = new EventEmitter<any>();
  @Output() discard = new EventEmitter<any>();
  @Output() back = new EventEmitter<any>();
  @Output() approve = new EventEmitter<any>();
  @Output() stop = new EventEmitter<any>();
  @Output() recycle = new EventEmitter<any>();
  @Output() complete = new EventEmitter<any>();
  @Output() convertProject = new EventEmitter<any>();
  @Output() deleteReq = new EventEmitter<any>();
  @Output() copyReq = new EventEmitter<any>();

  // userDetails: any
  filterType:any;
  requestGenerated:any
  
  E2EHeaderValue;
  dashboardMenuConfig: any;
  isAdmin: boolean;
  constructor(private requestService: RequestService,
    private activeRoute: ActivatedRoute,private monsterConfigApiService: MonsterConfigApiService) { 
    this.E2EHeaderValue = BusinessConstants.E2E_PM_HEADER; 
    this.requestService.goBackSubject.subscribe(res => {
      if(res){
        this.cancelRequestCreation(false);
      }
    });
  }

  ngOnInit(): void {
    this.readUrlParams((a,b)=>{
      this.filterType=b.menu      
    })
    this.monsterConfigApiService.getAllRolesForCurrentUser().subscribe(response =>{
      const admin = response.find(eachRole => eachRole['@id'].substring(3,eachRole['@id'].indexOf(',')) === MONSTER_ROLES.NPD_ADMIN);
      this.isAdmin = admin != null ? true : false;
  })
  }
  readUrlParams(callback) {
    this.activeRoute.queryParams.subscribe(queryParams => {
      if (this.activeRoute.parent.params && this.activeRoute.parent.params['value']
        && this.activeRoute.parent.params['value'].requestId) {
        this.activeRoute.parent.params.subscribe(parentRouteParams => {
          callback(parentRouteParams, queryParams);
        });
      } else {
        this.activeRoute.params.subscribe(routeParams => {
          callback(routeParams, queryParams);
        });
      }
    });
 }

  userDetails=JSON.parse(sessionStorage.getItem('USER_DETAILS_WORKFLOW')).User.ManagerFor.Target;
  isPM=this.userDetails.some(d=> {return d.Name ==="Program Manager"});
  

  // events
  PreviewForm() {
    this.preview.emit();
  }

  onSubmit(){
    this.submit.emit();
  }

  /*
    two events
  */
  cancelRequestCreation(bool){
    if(bool) {
      this.discard.emit(bool);
    } else {
      this.back.emit(bool);
    }
  }

  onApprove(){
    this.approve.emit();
  }

  onRejected(){
    this.stop.emit();
  }

  onResubmit(){
    this.recycle.emit();
  }

  completeTask(){
    this.complete.emit();
    
  }

  convertToProject(){
    this.convertProject.emit();
  }

  deleteRequest(){
    this.deleteReq.emit()
  }

  copyRequest(){
    this.copyReq.emit()
  }

}
