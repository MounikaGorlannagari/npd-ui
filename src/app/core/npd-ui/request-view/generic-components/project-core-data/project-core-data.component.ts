import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'mpm-library';
import { Observable } from 'rxjs';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../constants/BusinessConstants';

@Component({
  selector: 'app-project-core-data',
  templateUrl: './project-core-data.component.html',
  styleUrls: ['./project-core-data.component.scss']
})
export class ProjectCoreDataComponent implements OnInit {
  projectRequestorForm: FormGroup;
  @Input() config;
  @Input() requestDetails;
  @Input() projectCoreDataConfig;
  @Input() isNewRequest;
  @Input()ccopiedRequestDetails;
  


  selectedPlatformName;
  selectedVariantName;
  selectedBusinessUnit;
  readonly radioButtonsList = BusinessConstants.radioButtons;
  @Output() updateProjectName = new EventEmitter<any>();
  @Output() allThresholdValuesEvent = new EventEmitter<any>();
  @Output() allThresholdValuesForMonsterGreenEvent = new EventEmitter<any>();
  @Output() projectClassification = new EventEmitter<any>();
  selectedBrand = false;
  constructor(private formBuilder: FormBuilder,private monsterUtilService:MonsterUtilService,
    private monsterConfigApiService:MonsterConfigApiService,private loaderService: LoaderService) { }

  get formControls() { return this.projectRequestorForm.controls; };

  onValidate() {
    this.projectRequestorForm.markAllAsTouched();
    if(this.projectRequestorForm.invalid) {
      return true;
    } else{
      return false;
    }
  }

  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(this.projectRequestorForm,this.projectCoreDataConfig);
  }



  onBrandChange(brandFormControl,formName,brand) {
    this.updateRequestRelationsInfo(brandFormControl,formName);
    let brandRequestParams = {
      'Brands-id' : {
         Id : brand && brand.split('.')[1],
         ItemId : brand
      }
    }
    this.config.platforms = [];
    this.config.variants = [];
    this.projectRequestorForm.patchValue({
      platform : '',
      variant : ''
    });
    this.updateRequestRelationsInfo('platform',formName);
    this.updateRequestRelationsInfo('variant',formName);
    this.selectedBrand = true
    this.selectedPlatformName = null;
    this.selectedVariantName = null;
    this.updateProjectName.emit();

    if(brand) {
      this.loaderService.show();
      this.getPlatformsByBrand(brandRequestParams).subscribe(response=> {
        this.loaderService.hide();
      },(error) => {
        this.loaderService.hide();
      });
    }
  }


  onPlatformChange(platformFormControl,formName,platformItemId) {
    this.projectRequestorForm.patchValue({
      variant : ''
    });
    this.updateRequestRelationsInfo(platformFormControl,formName);
    this.updateRequestRelationsInfo('variant',formName);

    let selectedPlatForm:any = this.config.platforms.filter( platform => {
      return platform.ItemId === platformItemId ? platform.displayName : null
    });

    //NPD1-81
    if(selectedPlatForm && selectedPlatForm[0] ) {
      this.selectedPlatformName= selectedPlatForm[0].displayName;
    }
    this.selectedBrand = false

    this.selectedVariantName= null;
    this.updateProjectName.emit();
    this.config.variants = [];

    if(platformItemId) {
      this.loaderService.show();
      let platformRequestParams = {
        'Platforms-id' : {
           Id : platformItemId && platformItemId.split('.')[1],
           ItemId : platformItemId
        }
      }
      this.getVariantsByPlatform(platformRequestParams).subscribe(response=> {
        this.loaderService.hide();
      },(error) => {
        this.loaderService.hide();
      });
    }
  }

  onVariantChange(variantFormControl,formName,variant) {
     //changing project name based on platform & variant applied
     this.updateRequestRelationsInfo(variantFormControl,formName);
    this.selectedBrand = false

     let selectedVariant = this.config.variants.filter(v => { return v.ItemId === this.projectRequestorForm.controls.variant.value ? v.displayName : null} );
     if(selectedVariant && selectedVariant[0] ) {
      this.selectedVariantName = selectedVariant[0].displayName;
     }
     this.updateProjectName.emit();
  }


  updateRequestInfo(formControl,formName,propertyValue?,index?) {
    let property;
    let value;
    let itemId;


    if(formName === 'requestForm') {
      property = BusinessConstants.REQUEST_FORM[formControl];
      value = this.projectRequestorForm.controls[formControl] && this.projectRequestorForm.controls[formControl].value;
      itemId = this.requestDetails && this.requestDetails.requestItemId;
      /*  if(formControl === 'projectName') {
        value = this.projectName;
      } */
      if(formControl === 'status' && propertyValue) {
        value = propertyValue;
      }
    }
    this.monsterUtilService.setFormProperty(itemId, property, value);
  }

  updateRequestRelationsInfo(formControl,formName,index?) {
    let relationName;
    let objectValue;
    let itemId;

    if(formName === 'requestForm') {
      relationName = BusinessConstants.REQUEST_FORM[formControl];
      objectValue = this.projectRequestorForm.controls[formControl] && this.projectRequestorForm.controls[formControl].value;
      itemId = this.requestDetails && this.requestDetails.requestItemId;

      if(formControl === 'businessUnit') {
        objectValue = this.selectedBusinessUnit && this.selectedBusinessUnit.ItemId
      }
    }
    this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
    this.getLeadMarketRegion();
  }

  getPlatformsByBrand(parameters): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchPlatformsByBrand(parameters).subscribe(response => {
        this.config.platforms = this.monsterConfigApiService.transformResponse(response,'Platforms-id',true,true);
        if(this.ccopiedRequestDetails){
      this.projectRequestorForm.controls.platform.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_PLATFORMS$Identity.ItemId)
        this.onPlatformChange('platform','requestForm',this.ccopiedRequestDetails.result.items[0].R_PO_PLATFORMS$Identity.ItemId)
        }
        observer.next(true);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
  }

  getVariantsByPlatform(parameters): Observable<any> {
    return new Observable(observer => {
      this.monsterConfigApiService.fetchVariantsByPlatform(parameters).subscribe(response => {
        this.config.variants = this.monsterConfigApiService.transformResponse(response,'Variant_SKU-id',true,true);
        if(this.ccopiedRequestDetails){

        this.projectRequestorForm.controls.variant.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_VARIANT_SKU$Identity.ItemId)
        this.onVariantChange('variant','requestForm',this.ccopiedRequestDetails.result.items[0].R_PO_VARIANT_SKU$Identity.ItemId)
        }

        observer.next(true);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
  }

  getDependentDropDownValues() {
    if(this.projectRequestorForm.controls.brand.value) {
      let brandRequestParams = {
        'Brands-id' : {
           Id : this.projectRequestorForm.controls.brand.value.split('.')[1],
           ItemId : this.projectRequestorForm.controls.brand.value
        }
      }
      this.loaderService.show();
          this.getPlatformsByBrand(brandRequestParams).subscribe(response => {
            if(this.config.platforms.length > 0) {//&& this.requestForm.controls.variant.value
              let platformRequestParams = {
                'Platforms-id' : {
                  Id : this.projectRequestorForm.controls.platform.value && this.projectRequestorForm.controls.platform.value.split('.')[1],
                  ItemId : this.projectRequestorForm.controls.platform.value
                }
              }
              if(this.projectRequestorForm.controls.platform.value) {
                this.loaderService.show();
                this.getVariantsByPlatform(platformRequestParams).subscribe(response=> {
                  this.loaderService.hide();
                },(error) => {
                  this.loaderService.hide();
                });
              }
            }
        },(error) => {
          this.loaderService.hide();
        });
    }

  }

  initializeForm() {
    

    if(this.projectCoreDataConfig.businessUnit.value) {
      this.selectedBusinessUnit = this.config.businessUnits.find(b=>b.ItemId == this.projectCoreDataConfig.businessUnit.value)
    }
    this.projectRequestorForm = this.formBuilder.group({});

    Object.keys(this.projectCoreDataConfig).filter(formControlConfigName => {
      if(formControlConfigName && this.projectCoreDataConfig[formControlConfigName].ngIf && 'formControl' in this.projectCoreDataConfig[formControlConfigName] && this.projectCoreDataConfig[formControlConfigName]['formControl'] === true)  {
        let validators = [];
        if(this.projectCoreDataConfig[formControlConfigName].validators) {
          if(this.projectCoreDataConfig[formControlConfigName].validators.required) {
            validators.push(Validators.required);
          }
          if(this.projectCoreDataConfig[formControlConfigName].validators.length && this.projectCoreDataConfig[formControlConfigName].validators.length.max) {
             validators.push(Validators.maxLength(this.projectCoreDataConfig[formControlConfigName].validators.length.max));
          }
        }
        if(formControlConfigName != 'businessUnit') {
          this.projectRequestorForm.addControl(formControlConfigName, new FormControl({ value: this.projectCoreDataConfig[formControlConfigName].value,
            disabled: this.projectCoreDataConfig[formControlConfigName].disabled },validators));
        }
         else {
          this.projectRequestorForm.addControl(formControlConfigName, new FormControl({ value:  this.selectedBusinessUnit && this.selectedBusinessUnit.displayName,
            disabled: this.projectCoreDataConfig[formControlConfigName].disabled },validators));
         }
          //add validations too
      }
    });

  }
  updateDeliverablesThresholdValues(){

    this.setAllThresholdValues('brand', this.projectRequestorForm.controls.brand.value);
    this.setAllThresholdValues('platform', this.projectRequestorForm.controls.platform.value);
    this.setAllThresholdValues('variant', this.projectRequestorForm.controls.variant.value);
    this.setAllThresholdValues('bottler', this.projectRequestorForm.controls.bottler.value);
    this.setAllThresholdValuesForMonsterGreen('bottler', this.projectRequestorForm.controls.bottler.value);
  }

  ngOnInit(): void {
    if(this.config && this.projectCoreDataConfig) {

      this.initializeForm();
      if(!this.isNewRequest) {
        this.getDependentDropDownValues();
      }
    }

    

  }

  ngOnChanges(){
    if(this.ccopiedRequestDetails){

      this.projectRequestorForm.controls.brand.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_BRANDS$Identity.ItemId)
      this.onBrandChange('brand','requestForm',this.ccopiedRequestDetails.result.items[0].R_PO_BRANDS$Identity.ItemId)
      this.projectRequestorForm.controls.skuDetails.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_SKU_Details$Identity.ItemId)
      this.projectRequestorForm.controls.consumerUnitSize.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_CONSUMER_UNIT_SIZE$Identity.ItemId)
      this.projectRequestorForm.controls.casePackSize.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_CASE_PACK_SIZE$Identity.ItemId)
      this.projectRequestorForm.controls.primaryPackagingType.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_PACKAGING_TYPE$Identity.ItemId)
      this.projectRequestorForm.controls.secondaryPackagingType.setValue(this.ccopiedRequestDetails.result.items[0].Properties.SecondaryPackagingType)
      this.projectRequestorForm.controls.bottler.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_BOTTLER$Identity.ItemId)
      this.projectRequestorForm.controls.governanceApproach.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_GOVERNANCE_APPROACH$Identity.ItemId)
      if(this.ccopiedRequestDetails.result.items[0].R_PO_COMMERCIAL_CATEGORISATION$Identity.ItemId!=null){
      this.projectRequestorForm.controls.commercialCategory.setValue(this.ccopiedRequestDetails.result.items[0].R_PO_COMMERCIAL_CATEGORISATION$Identity.ItemId)
      this.projectRequestorForm.controls.svpAligned.setValue(this.ccopiedRequestDetails.result.items[0].Properties.Is_SVP_Aligned)
      this.projectRequestorForm.controls.postLaunchAnaylsis.setValue(this.ccopiedRequestDetails.result.items[0].Properties.PostLaunchAnalysis)
      this.updateRequestInfo('svpAligned','requestForm');
      this.updateRequestInfo('postLaunchAnaylsis','requestForm');
      }

    }
  }

  updateBusinessUnit(selectedBusinessUnit){
     this.projectRequestorForm.patchValue({
      businessUnit : selectedBusinessUnit.displayName
    });
    this.selectedBusinessUnit = selectedBusinessUnit;
    this.updateRequestRelationsInfo('businessUnit','requestForm');
  }

  setAllThresholdValues(property, value){
    this.allThresholdValuesEvent.emit({property: property, value: value && value.split('.')[1]});
  }

  setAllThresholdValuesForMonsterGreen(property, value){
    this.allThresholdValuesForMonsterGreenEvent.emit({property: property, value: value && value.split('.')[1]});
  }

  public getLeadMarketRegion(){
    this.projectRequestorForm.get('businessUnit').enable();
    let value= this.projectRequestorForm.get('businessUnit').value;
    this.projectRequestorForm.get('businessUnit').disable();
   
    return value;

  }

/*   setProjectClassification(property, value){
    this.projectClassification.emit({property: property, value: value && value.split('.')[1]});
  } */
}


/*     this.projectRequestorForm = this.formBuilder.group({
      businessUnit: [{ value: this.selectedBusinessUnit && this.selectedBusinessUnit.displayName,
                    disabled: this.projectCoreDataConfig.businessUnit.disabled }, Validators.required],
      bottler:  [{ value: this.projectCoreDataConfig.bottler.value,
                   disabled: this.projectCoreDataConfig.bottler.disabled }, Validators.required],
      projectType: [{ value: this.projectCoreDataConfig.projectType.value,
                      disabled: this.projectCoreDataConfig.projectType.disabled }],
      productionSite: [{ value: this.projectCoreDataConfig.productionSite.value,
                      disabled: this.projectCoreDataConfig.productionSite.disabled }],
      platform: [{ value: this.projectCoreDataConfig.platform.value,
                    disabled: this.projectCoreDataConfig.platform.disabled }, Validators.required],
      brand: [{ value: this.projectCoreDataConfig.brand.value,
                disabled: this.projectCoreDataConfig.brand.disabled }, Validators.required],
      primaryPackagingType: [{ value: this.projectCoreDataConfig.primaryPackagingType.value,
                            disabled: this.projectCoreDataConfig.primaryPackagingType.disabled }, Validators.required],
      secondaryPackagingType: [{ value: this.projectCoreDataConfig.secondaryPackagingType.value,
                              disabled: this.projectCoreDataConfig.secondaryPackagingType.disabled }, [Validators.required,Validators.maxLength(64)]],
      casePackSize: [{ value: this.projectCoreDataConfig.casePackSize.value,
                      disabled: this.projectCoreDataConfig.casePackSize.disabled }, Validators.required],
      consumerUnitSize: [{ value: this.projectCoreDataConfig.consumerUnitSize.value,
                    disabled: this.projectCoreDataConfig.consumerUnitSize.disabled }, Validators.required],
      variant: [{ value: this.projectCoreDataConfig.variant.value,
                  disabled: this.projectCoreDataConfig.variant.disabled }, Validators.required],
      skuDetails: [{ value: this.projectCoreDataConfig.skuDetails.value,
                    disabled: this.projectCoreDataConfig.skuDetails.disabled }, Validators.required],
      svpAligned: [{ value: this.projectCoreDataConfig.svpAligned.value,
                    disabled: this.projectCoreDataConfig.svpAligned.disabled }]
      //requestRationale: ['', [Validators.required,Validators.maxLength(500)]],
    });
    if(this.projectCoreDataConfig.commercialCategory.ngIf) {
      this.projectRequestorForm.addControl('commercialCategory', new FormControl({ value: this.projectCoreDataConfig.commercialCategory.value,
            disabled: this.projectCoreDataConfig.commercialCategory.disabled },[Validators.required]));
    }
    if(this.projectCoreDataConfig.governanceApproach.ngIf) {
      this.projectRequestorForm.addControl('governanceApproach', new FormControl({ value: this.projectCoreDataConfig.governanceApproach.value,
            disabled: this.projectCoreDataConfig.governanceApproach.disabled }));
    }
    if(this.projectCoreDataConfig.postLaunchAnaylsis.ngIf) {
      this.projectRequestorForm.addControl('postLaunchAnaylsis', new FormControl({ value: this.projectCoreDataConfig.postLaunchAnaylsis.value,
        disabled: this.projectCoreDataConfig.postLaunchAnaylsis.disabled },[Validators.required]));
    } */
