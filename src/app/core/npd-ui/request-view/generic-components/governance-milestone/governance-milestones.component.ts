import { Component, Input, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import {
  AppService,
  ConfirmationModalComponent,
  LoaderService,
  NotificationService,
  SharingService,
  UtilService,
} from 'mpm-library';
import { FilterConfigService } from '../../../filter-configs/filter-config.service';
import { NpdTaskService } from '../../../npd-task-view/services/npd-task.service';
import { EntityService } from '../../../services/entity.service';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../constants/BusinessConstants';
import { EntityListConstant } from '../../constants/EntityListConstant';

@Component({
  selector: 'app-governance-milestones',
  templateUrl: './governance-milestones.component.html',
  styleUrls: ['./governance-milestones.component.scss'],
})
export class GovernanceMilestonesComponent implements OnInit {
  showDecisionDate = [];
  isPM: boolean = false;
  isCorpProjectLeader: boolean = false;
  // isAdmin: boolean = false;
  disableFields: boolean = false;
  disableButtons = [];
  // showWeekPickerObj: {};

  constructor(
    private loaderService: LoaderService,
    private entityService: EntityService,
    private npdTaskService: NpdTaskService,
    public dialog: MatDialog,
    private monsterUtilService: MonsterUtilService,
    private formBuilder: FormBuilder,
    private adapter: DateAdapter<any>,
    private monsterConfigApiService: MonsterConfigApiService,
    private filterConfigService: FilterConfigService,
    private notificationService: NotificationService
  ) {}

  @Input() technicalRequestId;
  @Input() governanceMilestoneConfig;
  @Input() projectConfig;
  @Input() MPMProjectId;
  @Input() requestItemId;
  @Input() isRequest;
  @Input() governanceApproach;
  @Input() isAdmin;
  @Input() isFinalStatus;
  @Input() classificationNumber;
  @Input() isCP1Completed;

  @Output() refreshEmitter = new EventEmitter();
  readonly decisions = BusinessConstants.stageDecisions;

  /*
   * Market Scope Grid Variables
   */
  @ViewChild('stagesScopeTable') stagesScopeTable: MatTable<any>;

  dynamicGovernanceMilestoneForm: FormGroup;
  gmGridColumns = [];
  showGovernanceMilestoneForm;
  loadingGovernanceMilestone;
  isGovernanceMilestoneUpdate = false;

  toTargetDate: NgbDate[] = [];
  fromTargetDate: NgbDate[] = [];
  targetDPDate = [];
  revisedTargetDate = [];
  toDecisionDate: NgbDate[] = [];
  fromDecisionDate: NgbDate[] = [];
  DecisionDate = [];
  show = [];
  fromDate: NgbDate;
  toDate: NgbDate;
  weekPickerDate = new Date();
  disableSaveButton: boolean = true;
  governanceMilestoneData = [];
  revisedDateChanged: boolean = false;
  disableControlArray = [];
  disableRevisedButton = [];
  revisedTargetToDecisionDate = new Date();

  // convenience getters for easy access to form fields
  get dynamicGovernanceMilestoneFormControl() {
    return this.dynamicGovernanceMilestoneForm.controls;
  }
  get stagesScopeFormArray() {
    return this.dynamicGovernanceMilestoneFormControl.stagesScope as FormArray;
  }

  updateFormControlsDisability() {
    for (let i = 0; i < this.stagesScopeFormArray.length; i++) {
      this.monsterUtilService.disableFormControls(
        this.stagesScopeFormArray.controls[i],
        this.governanceMilestoneConfig
      );
    }
  }

  getIsoFormatDateString(datetime) {
    const newDate = datetime;
    const year = newDate.year;
    const actualMonth = newDate.month;
    const month =
      actualMonth.toString().length <= 1 ? '0' + actualMonth : actualMonth;
    const date = newDate.day;
    return year + '-' + month + '-' + date + 'T00:00:00.000Z';
  }

  saveGovernanceMileStone(element, index) {
    // console.log(this.governanceMilestoneData);
    let decisionWeek =
      this.dynamicGovernanceMilestoneForm.controls.stagesScope['controls'][
        index
      ].controls.decisionDate.value;
    let decisionDate;
    if (
      (decisionWeek && decisionWeek != 'NA') ||
      (decisionWeek && decisionWeek != 'NA')
    ) {
      decisionDate = this.monsterUtilService.calculateDate(
        decisionWeek?.split('/')[0],
        decisionWeek?.split('/')[1]
      )[1];
    }

    if (
      decisionDate &&
      decisionDate != '' &&
      element.decision &&
      element.decision == 'NA'
    ) {
      this.notificationService.error(
        'You are trying to add Decision Date without Decision, Kindly add Decision to Save'
      );
      return true;
    }
    let revisedDateWeek =
      this.dynamicGovernanceMilestoneForm.controls.stagesScope['controls'][
        index
      ].controls.revisedDate.value;
    // console.log(revisedDateWeek);
    let revisedDate;
    let savedRevisedWeek;
    if (this.governanceMilestoneData[2].Properties.RevisedTargetDate) {
      savedRevisedWeek = this.monsterUtilService.calculateTaskWeek(
        this.governanceMilestoneData[2].Properties.RevisedTargetDate
      );
      savedRevisedWeek == revisedDateWeek
        ? (revisedDateWeek = '')
        : (revisedDateWeek = revisedDateWeek);
    }
    // console.log(savedRevisedWeek);

    if (
      (revisedDateWeek && revisedDateWeek != 'NA') ||
      (revisedDateWeek && revisedDateWeek != '')
    ) {
      revisedDate = this.monsterUtilService.calculateDate(
        revisedDateWeek?.split('/')[0],
        revisedDateWeek?.split('/')[1]
      )[1];
    }

    if (
      (decisionDate == null && element.decision && element.decision != 'NA') ||
      (decisionDate == '' && element.decision && element.decision != 'NA')
    ) {
      let currDate = this.monsterUtilService.calculateTaskWeek(new Date());
      decisionDate = this.monsterUtilService.calculateDate(
        currDate?.split('/')[0],
        currDate?.split('/')[1]
      )[1];
    }
    let msg;
    if (this.revisedDateChanged) {
      msg =
        'Do you want to change the CP2 Revised Target Date and reset target date for CP2 and all subsequent milestones to live date?';
    } else {
      msg = 'Do you want to save the Governance milestone?';
    }
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '40%',
      disableClose: true,
      data: {
        message: msg,
        name: '',
        submitButton: 'yes',
        cancelButton: 'No',
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const milestoneObj = {
          UpdateGovernanceMilestone: {
            GovernanceMilestone: {
              Stage: element.Stage,
              Decision:
                element.decision && element.decision != 'NA'
                  ? element.decision
                  : '',
              DecisionDate: decisionDate, //element.decisionDate,
              RevisedTargetDate: revisedDate,
              Comments: element.comments,
              Id: element.itemId.split('.')[1],
            },
            RequestId: this.requestItemId.split('.')[1],
            MPMProjectId: this.MPMProjectId.split('.')[1],
            GovernanceApproach: this.governanceApproach,
            ItemId: this.MPMProjectId,
            // classificationNumber: this.classificationNumber
          },
        };
        this.monsterConfigApiService
          .updateGovernanceMilestone(milestoneObj)
          .subscribe((res) => {
            this.refreshEmitter.emit(true);
            this.revisedDateChanged = false;
          });
        // console.log(milestoneObj);
      } else {
        this.revisedDateChanged = false;
        let previousRevisedDate =
          this.monsterUtilService.calculateTaskWeek(
            this.governanceMilestoneData[index].Properties.RevisedTargetDate
          ) == '1/1970'
            ? ''
            : this.monsterUtilService.calculateTaskWeek(
                this.governanceMilestoneData[index].Properties.RevisedTargetDate
              );
        let previousDecisionDate =
          this.monsterUtilService.calculateTaskWeek(
            this.governanceMilestoneData[index].Properties.Decision_Date
          ) == '1/1970'
            ? ''
            : this.monsterUtilService.calculateTaskWeek(
                this.governanceMilestoneData[index].Properties.Decision_Date
              );

        this.dynamicGovernanceMilestoneForm.controls.stagesScope['controls'][
          index
        ].controls.revisedDate.setValue(previousRevisedDate);
        this.dynamicGovernanceMilestoneForm.controls.stagesScope['controls'][
          index
        ].controls.decisionDate.setValue(previousDecisionDate);
        this.dynamicGovernanceMilestoneForm.controls.stagesScope['controls'][
          index
        ].controls.decision.setValue(
          this.governanceMilestoneData[index].Properties.Decision
        );
        this.dynamicGovernanceMilestoneForm.controls.stagesScope['controls'][
          index
        ].controls.comments.setValue(
          this.governanceMilestoneData[index].Properties.Comments
        );
      }
    });
  }

  mapStagesScopeContentToForm(stagesScope) {
    this.dynamicGovernanceMilestoneForm = this.formBuilder.group({
      stagesScope: new FormArray([]),
    });
    if (this.isRequest) {
      this.gmGridColumns = [
        'stages',
        'targetStartDate',
        'decisionDate',
        'decision',
        'comments',
      ];
    } else {
      this.gmGridColumns = [
        'stages',
        'targetStartDate',
        'revisedDate',
        'liveDate',
        'decisionDate',
        'decision',
        'comments',
      ];
    }
    this.showGovernanceMilestoneForm = true;
    let disableCP1Fields = true;
    let disableCP2Fields = true;
    let disableCP3Fields = true;
    let disableCP4Fields = true;
    let cp1Decision;
    for (let i = 0; i < stagesScope.length; i++) {
      let targetStartDateValue;
      let decisionDateValue;
      let liveDateValue;
      let revisedDateValue;
      if (
        this.npdTaskService.checkValueIsNullOrEmpty(
          stagesScope[i].Properties.Target_Start_Date,
          ''
        ) != ''
      ) {
        targetStartDateValue = this.monsterUtilService.calculateTaskWeek(
          stagesScope[i].Properties.Target_Start_Date
        );
        //''+(this.calculateWeek(stagesScope[i].Properties.Target_Start_Date)-1)+'/'+ new Date(stagesScope[i].Properties.Target_Start_Date).getFullYear();
        let week = targetStartDateValue?.split('/')[0];
        let year = targetStartDateValue?.split('/')[1];
        let weekdates: NgbDate[] = this.calculateDate(week, year);
        this.fromTargetDate[i] = weekdates[0];
        this.toTargetDate[i] = weekdates[1];
      }
      if (
        this.npdTaskService.checkValueIsNullOrEmpty(
          stagesScope[i].Properties.LiveDate,
          ''
        ) != ''
      ) {
        liveDateValue = this.monsterUtilService.calculateTaskWeek(
          stagesScope[i].Properties.LiveDate
        );
        //''+(this.calculateWeek(stagesScope[i].Properties.Target_Start_Date)-1)+'/'+ new Date(stagesScope[i].Properties.Target_Start_Date).getFullYear();
        let week = liveDateValue?.split('/')[0];
        let year = liveDateValue?.split('/')[1];
        let weekdates: NgbDate[] = this.calculateDate(week, year);
        this.fromTargetDate[i] = weekdates[0];
        this.toTargetDate[i] = weekdates[1];
      }
      if (
        this.npdTaskService.checkValueIsNullOrEmpty(
          stagesScope[i].Properties.RevisedTargetDate,
          ''
        ) != ''
      ) {
        revisedDateValue = this.monsterUtilService.calculateTaskWeek(
          stagesScope[i].Properties.RevisedTargetDate
        );
        //''+(this.calculateWeek(stagesScope[i].Properties.Target_Start_Date)-1)+'/'+ new Date(stagesScope[i].Properties.Target_Start_Date).getFullYear();
        let week = revisedDateValue?.split('/')[0];
        let year = revisedDateValue?.split('/')[1];
        let weekdates: NgbDate[] = this.calculateDate(week, year);
        this.fromTargetDate[i] = weekdates[0];
        this.toTargetDate[i] = weekdates[1];
      }
      if (
        this.npdTaskService.checkValueIsNullOrEmpty(
          stagesScope[i].Properties.Decision_Date,
          ''
        ) != ''
      ) {
        decisionDateValue = this.monsterUtilService.calculateTaskWeek(
          stagesScope[i].Properties.Decision_Date
        );
        //''+(this.calculateWeek(stagesScope[i].Properties.Decision_Date)-1)+'/'+ new Date(stagesScope[i].Properties.Decision_Date).getFullYear();
        let week = decisionDateValue?.split('/')[0];
        let year = decisionDateValue?.split('/')[1];
        let weekdates: NgbDate[] = this.calculateDate(week, year);
        this.fromTargetDate[i] = weekdates[0];
        this.toTargetDate[i] = weekdates[1];
      }
      let disableComments;

      if (stagesScope[i].Properties.Stage == 'CP1') {
        cp1Decision = stagesScope[i].Properties.Decision;
      }
      if (
        this.projectConfig?.isProject &&
        this.projectConfig?.editGovernanceMilestone
      ) {
        disableComments =
          stagesScope[i]?.Properties?.Stage == 'CP0' ? true : false;
      } else {
        disableComments = this.governanceMilestoneConfig.comments.disabled;
      }
      if (
        stagesScope[i].Properties.Stage == 'CP1' &&
        this.isCP1Completed && (this.isCP1Completed == 'true' || this.isCP1Completed == true)
      ) {
        disableCP1Fields = false;
      }
      if (
        stagesScope[i].Properties.Stage == 'CP1' &&
        stagesScope[i].Properties.Decision == 'Approved' &&
        this.governanceApproach == "All CP's"
      ) {
        disableCP2Fields = false;
      } else if (
        stagesScope[i].Properties.Stage == 'CP2' &&
        stagesScope[i].Properties.Decision == 'Approved' &&
        this.governanceApproach == "All CP's"
      ) {
        disableCP2Fields = true;
        disableCP3Fields = false;
      } else if (
        stagesScope[i].Properties.Stage == 'CP3' &&
        stagesScope[i].Properties.Decision == 'Approved' &&
        this.governanceApproach == "All CP's"
      ) {
        disableCP2Fields = true;
        disableCP3Fields = true;
        disableCP4Fields = false;
      }
      let fieldAccess = !(this.isPM || this.isAdmin);
      let revisedDateFieldAccess = !(this.isPM || this.isCorpProjectLeader);
      this.disableFields =
        stagesScope[i].Properties.Stage == 'CP0' ||
        (stagesScope[i].Properties.Stage == 'CP1' &&
          (stagesScope[i].Properties.Decision == 'Approved' ||
            disableCP1Fields)) ||
        this.governanceApproach == 'No Governance' ||
        (stagesScope[i].Properties.Stage == 'CP2' && disableCP2Fields) ||
        (stagesScope[i].Properties.Stage == 'CP3' && disableCP3Fields) ||
        (stagesScope[i].Properties.Stage == 'CP4' && disableCP4Fields);
      this.disableButtons[i] = fieldAccess
        ? true
        : this.disableFields
        ? true
        : this.governanceMilestoneConfig.decisionDate.disabled ||
          stagesScope[i].Properties.Decision == 'Approved' ||
          stagesScope[i].Properties.Decision == 'Stop';
      this.disableRevisedButton[i] = revisedDateFieldAccess
        ? true
        : this.disableFields &&
          !(
            this.governanceApproach == 'CP1 Only' &&
            cp1Decision == 'Approved' &&
            stagesScope[i].Properties.Decision != 'Approved'
          )
        ? true
        : this.governanceMilestoneConfig.decisionDate.disabled ||
          stagesScope[i].Properties.Decision == 'Approved' ||
          stagesScope[i].Properties.Decision == 'Stop' ||
          this.isFinalStatus;

      this.stagesScopeFormArray.push(
        this.formBuilder.group({
          Stage: [stagesScope[i].Properties.Stage],
          //targetStartDate:  [{value:stagesScope[i].Properties.Target_Start_Date && new Date(stagesScope[i].Properties.Target_Start_Date).toISOString(), disabled: this.governanceMilestoneConfig.targetStartDate.disabled}],
          //decisionDate: [{value:stagesScope[i].Properties.Decision_Date && new Date(stagesScope[i].Properties.Decision_Date).toISOString(), disabled: this.governanceMilestoneConfig.decisionDate.disabled}],
          targetStartDate: [
            {
              value: targetStartDateValue,
              disabled: this.governanceMilestoneConfig.targetStartDate.disabled,
            },
          ],
          decisionDate: [
            {
              value: decisionDateValue,
              disabled: fieldAccess
                ? true
                : (this.disableFields &&
                    !(
                      this.governanceApproach == 'CP1 Only' &&
                      stagesScope[i].Properties.Stage == 'CP1' &&
                      stagesScope[i].Properties.Decision != 'Approved'
                    )) ||
                  this.governanceApproach == 'No Governance' ||
                  this.governanceMilestoneConfig.decisionDate.disabled ||
                  stagesScope[i]?.Properties?.Decision == 'Approved' ||
                  this.isFinalStatus ||
                  stagesScope[i]?.Properties?.Decision == 'Stop',
            },
          ],
          liveDate: [{ value: liveDateValue, disabled: true }],
          revisedDate: [
            {
              value: revisedDateValue,
              disabled: revisedDateFieldAccess
                ? true
                : (this.disableFields &&
                    !(
                      this.governanceApproach == 'CP1 Only' &&
                      cp1Decision == 'Approved' &&
                      stagesScope[i].Properties.Decision != 'Approved'
                    )) ||
                  cp1Decision != 'Approved' ||
                  this.governanceMilestoneConfig?.revisedDate?.disabled ||
                  stagesScope[i]?.Properties?.Decision == 'Approved' ||
                  this.isFinalStatus ||
                  stagesScope[i]?.Properties?.Decision == 'Stop',
            },
          ],
          decision: [
            {
              value: stagesScope[i].Properties.Decision,
              disabled: fieldAccess
                ? true
                : (this.disableFields &&
                    !(
                      this.governanceApproach == 'CP1 Only' &&
                      stagesScope[i].Properties.Stage == 'CP1' &&
                      stagesScope[i].Properties.Decision != 'Approved'
                    )) ||
                  this.governanceApproach == 'No Governance' ||
                  this.isFinalStatus ||
                  stagesScope[i]?.Properties?.Decision == 'Approved' ||
                  stagesScope[i]?.Properties?.Decision == 'Stop',
            },
            Validators.required,
          ],
          comments: [
            {
              value: stagesScope[i].Properties.Comments,
              disabled: fieldAccess
                ? true
                : (this.disableFields &&
                    !(
                      this.governanceApproach == 'CP1 Only' &&
                      stagesScope[i].Properties.Stage == 'CP1' &&
                      stagesScope[i].Properties.Decision != 'Approved'
                    )) ||
                  disableComments ||
                  this.governanceApproach == 'No Governance' ||
                  stagesScope[i]?.Properties?.Decision == 'Approved' ||
                  this.isFinalStatus ||
                  stagesScope[i]?.Properties?.Decision == 'Stop',
            },
            [Validators.maxLength(2000)],
          ],
          itemId: [stagesScope[i].Identity.ItemId],
        })
      );
      this.renderGMTableRows();
    }
  }

  /**
   * To re-render material table
   */
  renderGMTableRows() {
    this.stagesScopeTable && this.stagesScopeTable.renderRows
      ? this.stagesScopeTable.renderRows()
      : console.log('No rows in Governance Milestone.');
  }

  /**
   * To load Governance Milestones and set form groups
   */
  getAllGovernanceMilestones() {
    // this.loaderService.show();
    this.loadingGovernanceMilestone = true;
    this.entityService
      .getEntityItemDetails(
        this.technicalRequestId,
        EntityListConstant.TECHNICAL_REQUEST_GOVERNANCE_MILESTONE_RELATION
      )
      .subscribe(
        (governanceMilestoneResponse) => {
          // create form groups for GovernanceMilestones
          if (
            governanceMilestoneResponse &&
            governanceMilestoneResponse.items
          ) {
            this.governanceMilestoneData = governanceMilestoneResponse.items;
            this.governanceMilestoneData.sort((a, b) =>
              a.Properties.Stage.localeCompare(b.Properties.Stage)
            );
            // console.log(this.governanceMilestoneData);
            this.mapStagesScopeContentToForm(governanceMilestoneResponse.items);
            // this.loaderService.hide();
            // this.mapStagesScopeContentToForm(governanceMilestoneResponse.items);
            // this.governanceMilestoneData = governanceMilestoneResponse.items;
            // this.loaderService.hide();
          }
          this.loadingGovernanceMilestone = false;
        },
        (error) => {
          // this.loaderService.hide();
          this.loadingGovernanceMilestone = false;
        }
      );
  }

  updateGMInfo(formControl, index, property) {
    //this.isGovernanceMilestoneUpdate = true;//dynamicGovernanceMilestoneForm.value.stagesScope[i].decisionDate
    //if from project then only update comments
    if (this.isRequest) {
      if (this.projectConfig?.isProject) {
        this.monsterUtilService.setFormProperty(
          this.dynamicGovernanceMilestoneForm.value.stagesScope[index].itemId,
          property,
          this.dynamicGovernanceMilestoneForm.value.stagesScope[index][
            formControl
          ]
        );
      } else {
        this.monsterUtilService.setFormProperty(
          this.dynamicGovernanceMilestoneForm.value.stagesScope[index].itemId,
          property,
          this.dynamicGovernanceMilestoneForm.value.stagesScope[index][
            formControl
          ]
        );
        //remove this at request leel-update comments manually not required
      }
    }
  }

  openWeekPicker(i) {
    this.getAllBool();
    this.show[i] = !this.show[i];
  }

  openDecisionWeekPicker(event, i) {
    //event.stopPropagation();
    this.getAllBool();
    this.showDecisionDate[i] = !this.showDecisionDate[i];
  }

  getBool(i) {
    this.showDecisionDate[i] = !this.showDecisionDate[i];
  }

  getBoolRevisedDate(i) {
    this.show[i] = !this.show[i];
  }

  getStopPropogation(event, index) {
    if (this.show[index]) {
      return event.stopPropagation();
    }
    if (this.showDecisionDate[index]) {
      return event.stopPropagation();
    }
  }
  getAllBool() {
    this.show = this.show.map((ele) => {
      ele = false;
    });
    this.showDecisionDate = this.show.map((ele) => {
      ele = false;
    });
  }

  calculateWeek(date: Date) {
    const time = new Date(date).getTime() + 4 * 24 * 60 * 60 * 1000;
    const firstDay = new Date(new Date(date).getFullYear() + '/1/1'); //NPD1-83 safari issue;
    return (
      Math.floor(Math.round((time - firstDay.getTime()) / 86400000) / 7) + 1
    );
  }

  changeTargetStartDate(weekFormat, i) {
    this.targetDPDate[i] = weekFormat;
    this.dynamicGovernanceMilestoneForm.controls[i].patchValue({
      targetStartDate: weekFormat,
    });
    //this.updateRequestInfo('targetDP','dynamicstagesScopeForm',weekFormat,i);
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates: NgbDate[] = this.calculateDate(week, year);

      this.fromTargetDate[i] = weekdates[0];
      this.toTargetDate[i] = weekdates[1];
    }
  }

  changeRevisedTargetDate(weekFormat, i) {
    //this.disableSaveButton = this.dynamicGovernanceMilestoneForm.touched ? false : this.disableSaveButton;
    this.revisedDateChanged = true;
    this.revisedTargetDate[i] = weekFormat;
    this.dynamicGovernanceMilestoneForm.controls.stagesScope['controls'][
      i
    ].patchValue({ revisedDate: weekFormat });
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates: NgbDate[] = this.calculateDate(week, year);

      this.fromDecisionDate[i] = weekdates[0];
      this.toDecisionDate[i] = weekdates[1];
    }
    this.show[i] = !this.show[i];
  }

  // closeAllDatePicker(){
  //   this.showDecisionDate.forEach((ele, i) => {
  //     if(ele){
  //       this.showDecisionDate[i] = !this.showDecisionDate[i];
  //     }
  //   })
  //   this.show.forEach((ele,i) => {
  //     if(ele){
  //       this.show[i] = !this.show[i];
  //     }
  //   })
  // }

  changeDecisionDate(weekFormat, i) {
    
    //this.disableSaveButton = this.dynamicGovernanceMilestoneForm.touched ? false : this.disableSaveButton;
    this.DecisionDate[i] = weekFormat;
    this.dynamicGovernanceMilestoneForm.controls.stagesScope['controls'][
      i
    ].patchValue({ decisionDate: weekFormat });
    if (weekFormat && weekFormat !== undefined) {
      let week = weekFormat.split('/')[0];
      let year = weekFormat.split('/')[1];

      let weekdates: NgbDate[] = this.calculateDate(week, year);

      this.fromDecisionDate[i] = weekdates[0];
      this.toDecisionDate[i] = weekdates[1];
    }
    this.showDecisionDate[i] = !this.showDecisionDate[i];
  }

  calculateDate(week: number, year: number): NgbDate[] {
    let ngbFromDate, ngbToDate;
  
    const firstDay = new Date(year + '/1/4'); //NPD1-83 Safari Browser issue
    const date = new Date(
      firstDay.getTime() + (week - 1) * 7 * 24 * 60 * 60 * 1000
    );
    const selectDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    let fromDate = new Date(
      `${selectDate.year.toString()}-${selectDate.month
        .toString()
        .padStart(2, '0')}-${selectDate.day.toString().padStart(2, '0')}`
    );
    let time = fromDate.getDay() ? fromDate.getDay() - 1 : 6;
    fromDate = new Date(fromDate.getTime() - time * 24 * 60 * 60 * 1000);
    ngbFromDate = new NgbDate(
      fromDate.getFullYear(),
      fromDate.getMonth() + 1,
      fromDate.getDate()
    );
    const toDate = new Date(fromDate.getTime() + 6 * 24 * 60 * 60 * 1000);
    ngbToDate = new NgbDate(
      toDate.getFullYear(),
      toDate.getMonth() + 1,
      toDate.getDate()
    );
    return [ngbFromDate, ngbToDate];
  }

  ngOnInit() {
    //this.loaderService.show();
    this.isPM = this.filterConfigService.isProgramManagerRole();
    this.isCorpProjectLeader = this.filterConfigService.isCorpProjectLeader();
    // this.isAdmin = this.filterConfigService.isAdminRole();
    if (this.technicalRequestId && this.governanceMilestoneConfig) {
      this.getAllGovernanceMilestones();
    }
    if (navigator.language !== undefined) {
      //for mat date picker
      this.adapter.setLocale(navigator.language);
    }
  }
}
