import { Component, Input, OnInit } from '@angular/core';
import { MonsterUtilService } from '../../../services/monster-util.service';

@Component({
  selector: 'app-status-header',
  templateUrl: './status-header.component.html',
  styleUrls: ['./status-header.component.scss']
})
export class StatusHeaderComponent implements OnInit {

  @Input() request;
  @Input() taskConfig;
  @Input() ccopiedRequestDetails;
  constructor(private monsterUtilService:MonsterUtilService) { }

  getStatusDisplayName(statusValue) {
    let status = this.monsterUtilService.getStatusDisplayName(statusValue);
    return status && status.DISPLAY_NAME;
  }

  ngOnInit(): void {
  }

}
