import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { BusinessConstants } from '../../constants/BusinessConstants';

@Component({
  selector: 'app-portfolio-strategy',
  templateUrl: './portfolio-strategy.component.html',
  styleUrls: ['./portfolio-strategy.component.scss']
})
export class PortfolioStrategyComponent implements OnInit {

  portfolioStrategyForm: FormGroup;
  @Input() requestDetails;
  @Input() portfolioStrategyDataConfig;
  @Input() ccopiedRequestDetails;
  readonly incrementalReplacementSKUList = BusinessConstants.incrementalReplacementSKU;
  readonly specificCutOffSKUList = BusinessConstants.specificCutOffSKU;

  
  constructor(private formBuilder: FormBuilder,private monsterUtilService:MonsterUtilService) { }

  get formControls(){ return this.portfolioStrategyForm.controls; };

  onValidate() {
    this.portfolioStrategyForm.markAllAsTouched();
    if(this.portfolioStrategyForm.invalid) {
      return true;
    } else{
      return false;
    }
  }

  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(this.portfolioStrategyForm,this.portfolioStrategyDataConfig);
  }
  
  updateRequestInfo(formControl,formName,propertyValue?,index?) {
    let property;
    let value;
    let itemId;

    if(formName === 'portfolioStrategyForm') {
      property = BusinessConstants.PORTFOLIO_STRATEGY_FORM[formControl];
      value = this.portfolioStrategyForm.controls[formControl] && this.portfolioStrategyForm.controls[formControl].value;
      itemId = this.requestDetails && this.requestDetails.requestItemId;
      this.monsterUtilService.setFormProperty(itemId, property, value);
    }
  }

  initializeForm() {
    this.portfolioStrategyForm = this.formBuilder.group({});
    Object.keys(this.portfolioStrategyDataConfig).filter(formControlConfigName => {
      if(formControlConfigName && this.portfolioStrategyDataConfig[formControlConfigName].ngIf && 'formControl' in this.portfolioStrategyDataConfig[formControlConfigName] && this.portfolioStrategyDataConfig[formControlConfigName]['formControl'] === true)  {
        let validators = [];
        if(this.portfolioStrategyDataConfig[formControlConfigName].validators) {
          if(this.portfolioStrategyDataConfig[formControlConfigName].validators.required) {
            validators.push(Validators.required);
          }
          if(this.portfolioStrategyDataConfig[formControlConfigName].validators.length && this.portfolioStrategyDataConfig[formControlConfigName].validators.length.max) {
             validators.push(Validators.maxLength(this.portfolioStrategyDataConfig[formControlConfigName].validators.length.max));
          }
        }
        this.portfolioStrategyForm.addControl(formControlConfigName, new FormControl({ value: this.portfolioStrategyDataConfig[formControlConfigName].value,
          disabled: this.portfolioStrategyDataConfig[formControlConfigName].disabled },validators));
      }
    });
  }


  ngOnInit(): void {
    if(this.portfolioStrategyDataConfig) {
      this.initializeForm();
    }
  }

  ngOnChanges(){
    if(this.ccopiedRequestDetails){
      this.portfolioStrategyForm.controls.incrementalReplacementSku.setValue(this.ccopiedRequestDetails.result.items[0].Properties.IncrementalReplacemntalSKU)
      this.portfolioStrategyForm.controls.portfolioDelistStrategy.setValue(this.ccopiedRequestDetails.result.items[0].Properties.PortfolioDelistStrategy)
      this.portfolioStrategyForm.controls.specificCutOffDate.setValue(this.ccopiedRequestDetails.result.items[0].Properties.SpecificSKUCutOffIntro)
      this.portfolioStrategyForm.controls.switchDateAndDrivingDateReason.setValue(this.ccopiedRequestDetails.result.items[0].Properties.SwitchDateAndDrivingDateReason)
      this.portfolioStrategyForm.controls.commercialStrategy.setValue(this.ccopiedRequestDetails.result.items[0].Properties.CommercialStrategy)
    }
  }

}


/*  this.portfolioStrategyForm = this.formBuilder.group({
      incrementalReplacementSku: [{ value: this.portfolioStrategyDataConfig.incrementalReplacementSku.value,
            disabled: this.portfolioStrategyDataConfig.incrementalReplacementSku.disabled }],
      specificCutOffDate: [{ value: this.portfolioStrategyDataConfig.specificCutOffDate.value,
        disabled: this.portfolioStrategyDataConfig.specificCutOffDate.disabled }],
      commercialStrategy: [{ value: this.portfolioStrategyDataConfig.commercialStrategy.value,
        disabled: this.portfolioStrategyDataConfig.commercialStrategy.disabled },Validators.maxLength(64)],
    });
    if(this.portfolioStrategyDataConfig.portfolioDelistStrategy.ngIf) {
      this.portfolioStrategyForm.addControl('portfolioDelistStrategy', new FormControl({ value:  this.portfolioStrategyDataConfig.portfolioDelistStrategy.value,
        disabled: this.portfolioStrategyDataConfig.portfolioDelistStrategy.disabled },Validators.maxLength(2000)));
    }
    if(this.portfolioStrategyDataConfig.switchDateAndDrivingDateReason.ngIf) {
      this.portfolioStrategyForm.addControl('switchDateAndDrivingDateReason', new FormControl({ value:  this.portfolioStrategyDataConfig.switchDateAndDrivingDateReason.value,
        disabled: this.portfolioStrategyDataConfig.switchDateAndDrivingDateReason.disabled },[Validators.required,Validators.maxLength(2000)]));
    } */