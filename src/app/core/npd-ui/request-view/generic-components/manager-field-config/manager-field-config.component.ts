import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MonsterUtilService } from '../../../services/monster-util.service';
import { RequestService } from '../../../services/request.service';
import { BusinessConstants } from '../../constants/BusinessConstants';

@Component({
  selector: 'app-manager-field-config',
  templateUrl: './manager-field-config.component.html',
  styleUrls: ['./manager-field-config.component.scss']
})
export class ManagerFieldConfigComponent implements OnInit {

  @Input() managerConfig;
  @Input() listConfig;
  @Input() request;
  @Input() requestDetails;
  managerForm : FormGroup;
  selectede2EPM;
  requestStatus;
  selectedValue;

  get formControls() { return this.managerForm.controls; };
  constructor(private formBuilder:FormBuilder,private requestService: RequestService,private monsterUtilService:MonsterUtilService) { }

  onValidate() {
    this.managerForm.markAllAsTouched();
    if(this.managerForm.invalid) {
      return true;
    } else{
      return false;
    }
  }
  
  updateFormControlsDisability() {
    this.monsterUtilService.disableFormControls(this.managerForm,this.managerConfig);
  }

  one2EPMSelection(formControl,formName) {
    if(this.selectede2EPM) {
      this.requestService.getUserByID(this.selectede2EPM.name).subscribe(response => {
        this.selectede2EPM.userIdentity = response.result.items[0];
        this.updateRequestRelationsInfo(formControl,formName);
      }, error => {
      });
    }
  }
  
  updateRequestRelationsInfo(formControl,formName,index?) {
    let relationName;
    let objectValue;
    let itemId;

    if(formName === 'requestForm') {
      relationName = BusinessConstants.REQUEST_FORM[formControl];
      itemId = this.request.requestItemId;

      if(formControl === 'e2ePM') {
        objectValue =  this.selectede2EPM && this.selectede2EPM.userIdentity && this.selectede2EPM.userIdentity.Identity && this.selectede2EPM.userIdentity.Identity.ItemId
      } 
    }

    this.monsterUtilService.setFormRelations(itemId, relationName, objectValue);
  }

  initializeForm() {
   
    this.requestStatus = this.requestDetails.Properties.Request_Status
    this.selectedValue= this.requestDetails.R_PO_E2E_PM$Properties.UserId
   
    if(this.managerConfig.e2ePM.ngIf) {
      this.managerForm = this.formBuilder.group({
        e2ePM:  [{ value: '',
                     disabled: this.managerConfig.e2ePM.disabled }, Validators.required],
       
      });
      if(this.managerConfig.e2ePM.value && this.requestDetails) {
        this.selectede2EPM = this.formE2EPMValue(this.requestDetails);
      }
      //mape2epm
    }
    this.listConfig.e2ePMUsers = this.listConfig.e2ePMUsers.filter(e2e=>{
       return e2e.isActive === 'true'
    })
    

  }

  formE2EPMValue(request) {
    
    if (request && request.R_PO_E2E_PM$Identity && request.R_PO_E2E_PM$Identity.ItemId && request.R_PO_E2E_PM$Properties &&
      request.R_PO_E2E_PM$Properties.UserId) {
      const userIdentity = {
        Identity: {
          ItemId: request.R_PO_E2E_PM$Identity.ItemId,
        }
      };
      const selectedE2EPM = this.listConfig.e2ePMUsers.find(element => element.name === request.R_PO_E2E_PM$Properties.UserId);
      if (selectedE2EPM) {
        selectedE2EPM.userIdentity = userIdentity;
      }
      return (selectedE2EPM) ? selectedE2EPM : { userIdentity: userIdentity };
    } 
    return {};
  }

  ngOnInit(): void {
    if(this.request && this.listConfig && this.managerConfig) {
      this.initializeForm();
    }
  }

}
