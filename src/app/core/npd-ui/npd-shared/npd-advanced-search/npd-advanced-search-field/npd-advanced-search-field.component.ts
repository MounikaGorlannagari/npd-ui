import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { DateAdapter } from '@angular/material/core';
import {
  UtilService,
  NotificationService,
  SharingService,
  StatusService,
  EntityAppDefService,
  CategoryService,
  OTMMService,
  AppService,
  ProjectService,
  IndexerDataTypes,
  MPMField,
  AdvancedSearchStandardValue,
  CategoryLevel,
  LoaderService,
  Priority,
  ProjectConstant,
  SessionStorageConstants,
} from 'mpm-library';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { startWith } from 'rxjs/internal/operators/startWith';
import { MPMAdvancedSearchFieldComponent } from 'src/app/core/mpm-lib/search/advanced-search-field/advanced-search-field.component';
import { ProjectBulkEditConstants } from '../../../npd-project-view/constants/ProjectBulkEditConstants';
import { OverViewConstants } from '../../../npd-project-view/constants/projectOverviewConstants';
// import { NpdLookupDomainService } from '../../../npd-project-view/services/npd-lookup-domain.service';
import { NpdProjectService } from '../../../npd-project-view/services/npd-project.service';
import { NpdSessionStorageConstants } from '../../../request-view/constants/sessionStorageConstants';
import { MonsterConfigApiService } from '../../../services/monster-config-api.service';

@Component({
  selector: 'app-npd-advanced-search-field',
  templateUrl: './npd-advanced-search-field.component.html',
  styleUrls: ['./npd-advanced-search-field.component.scss'],
})
export class NpdAdvancedSearchFieldComponent
  extends MPMAdvancedSearchFieldComponent
  implements OnInit
{
  @Input() comboFieldsConfig;
  selectedValue = [];
  constructor(
    public adapter: DateAdapter<any>,
    public utilService: UtilService,
    public notificationService: NotificationService,
    public sharingService: SharingService,
    public statusService: StatusService,
    public entityAppDefService: EntityAppDefService,
    public categoryService: CategoryService,
    public otmmService: OTMMService,
    public appService: AppService,
    public projectService: ProjectService,
    // public npdLookupDomainService: NpdLookupDomainService,
    public loaderService: LoaderService,

    public npdProjectService: NpdProjectService,
    public monsterConfigApiService: MonsterConfigApiService
  ) {
    super(
      adapter,
      utilService,
      notificationService,
      sharingService,
      statusService,
      entityAppDefService,
      categoryService,
      otmmService,
      appService,
      projectService
    );
  }

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  inputs = [];
  values = [];
  earlyManufacturingSites = [];
  reportingProjectSubType = [];
  reportingProjectDetail = [];
  allListConfig = {
    projectTypes: [],
  };

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.inputs.push({ name: value.trim() });
      this.values.push(value.trim());
    }

    input.value = '';
    this.filterRowData.searchValue = this.values.toString();

    // const selectedFieldId = event.value;
    // this.filterRowData.searchField = selectedFieldId;
    // this.advancedSearchFieldForm.patchValue({
    //   fieldValue: this.values.toString(),
    // });
    // this.selectedValue = this.values;
    // this.advancedSearchFieldForm.controls.fieldValue.valueChanges.subscribe(
    //   (selectedValue) => {
    //     this.filterRowData.searchValue = selectedValue;
    //   }
    // )
    // Reset the input value
    // if (input) {

    // }
  }

  remove(fruit): void {
    const index = this.inputs.indexOf(fruit);

    if (index >= 0) {
      this.inputs.splice(index, 1);
      this.values = [];
      this.inputs.forEach((input) => {
        this.values.push(input.name);
      });
      this.filterRowData.searchValue = this.values.toString();

      // this.advancedSearchFieldForm.patchValue({
      //   fieldValue: this.inputs.toString(),
      // });
      this.filterSelection(true);
    }
  }
  loadFieldValueType(selectedOperatorId, isInitial) {
    this.filterRowData.searchOperatorId = selectedOperatorId;
    this.filterRowData.searchOperatorName = this.availableFieldOperators.find(
      (element) => element.OPERATOR_ID === selectedOperatorId
    )?.NAME;
    const selectedField: MPMField =
      this.filterRowData.availableSearchFields.find((field) => {
        return field.MPM_FIELD_CONFIG_ID === this.filterRowData.searchField;
      });
    if (!isInitial) {
      this.filterRowData.searchSecondValue = '';
      this.filterRowData.searchValue = '';
      if (
        this.advancedSearchFieldForm &&
        this.advancedSearchFieldForm.controls &&
        this.advancedSearchFieldForm.controls.fieldValue
      ) {
        this.advancedSearchFieldForm.controls.fieldValue.patchValue('');
      }
      if (
        this.advancedSearchFieldForm &&
        this.advancedSearchFieldForm.controls &&
        this.advancedSearchFieldForm.controls.fieldSecondValue
      ) {
        this.advancedSearchFieldForm.controls.fieldSecondValue.patchValue('');
      }
    }
    const selectedOperator =
      this.advancedSearchConfigData.searchOperatorList.filter((operator) => {
        return (
          operator.OPERATOR_ID === selectedOperatorId &&
          operator.DATA_TYPE === selectedField.DATA_TYPE
        );
      });
    if (selectedOperator[0] && selectedOperator[0].VALUE_COUNT) {
      this.requireFieldValue = true;
      this.filterRowData.issearchValueRequired = true;
    } else {
      this.requireFieldValue = false;
      this.filterRowData.issearchValueRequired = false;
    }
    if (selectedOperator[0] && selectedOperator[0].VALUE_COUNT === 2) {
      this.filterRowData.hasTwoValues = true;
      if (
        typeof this.filterRowData.searchValue === 'string' &&
        this.filterRowData.searchValue.includes('and') &&
        this.filterRowData.searchValue.split(' and ').length > 1
      ) {
        const searchValues = this.filterRowData.searchValue.split(' and ');
        this.filterRowData.searchValue = searchValues[0];
        this.filterRowData.searchSecondValue = searchValues[1];
      }
    } else {
      this.filterRowData.hasTwoValues = false;
    }
    if (!this.comboType) {
      if (
        (selectedOperator[0] &&
          selectedOperator[0].DATA_TYPE === IndexerDataTypes.STRING) ||
        (selectedOperator[0] &&
          selectedOperator[0].DATA_TYPE === IndexerDataTypes.BOOLEAN)
      ) {
        this.charDataType = true;
        this.dateDataType = false;
        this.numberDataType = false;
      } else if (
        (selectedOperator[0] &&
          selectedOperator[0].DATA_TYPE === IndexerDataTypes.DECIMAL) ||
        (selectedOperator[0] &&
          selectedOperator[0].DATA_TYPE === IndexerDataTypes.NUMBER)
      ) {
        this.charDataType = false;
        this.dateDataType = false;
        this.numberDataType = true;
      } else if (
        selectedOperator[0] &&
        selectedOperator[0].DATA_TYPE === IndexerDataTypes.DATETIME
      ) {
        this.charDataType = false;
        this.dateDataType = true;
        this.numberDataType = false;
        if (isInitial) {
          if (typeof this.filterRowData.searchValue === 'string') {
            // this.filterRowData.searchValue = new Date(this.filterRowData.searchValue.split('/').reverse().join('/'));
            this.filterRowData.searchValue = new Date(
              this.filterRowData.searchValue
            );
          }
          if (
            this.filterRowData.hasTwoValues &&
            typeof this.filterRowData.searchSecondValue === 'string'
          ) {
            // this.filterRowData.searchSecondValue = new Date(this.filterRowData.searchSecondValue.split('/').reverse().join('/'));
            this.filterRowData.searchSecondValue = new Date(
              this.filterRowData.searchSecondValue
            );
          }
        }
      } else if (selectedOperator[0].DATA_TYPE === IndexerDataTypes.NUMBER) {
        this.charDataType = false;
        this.dateDataType = false;
        this.numberDataType = true;
      }
    } else {
      this.requireFieldValue = true;
    }
  }

  validateSelectedField() {
    // to load default field
    if (!this.filterRowData.searchField) {
      this.filterRowData.searchField =
        this.filterRowData.availableSearchFields[0].MPM_FIELD_CONFIG_ID;
    }
    // to load selected field
    if (this.filterRowData.searchField) {
      const selectedField: MPMField[] =
        this.filterRowData.availableSearchFields.filter((field) => {
          return field.MPM_FIELD_CONFIG_ID === this.filterRowData.searchField;
        });
      // to load selected field operators
      if (selectedField && selectedField[0] && selectedField[0].DATA_TYPE) {
        this.availableFieldOperators = [];
        this.availableFieldOperators =
          this.advancedSearchConfigData.searchOperatorList.filter(
            (operator) => {
              return selectedField[0].DATA_TYPE === operator.DATA_TYPE;
            }
          );
        this.availableFieldOperators.sort((a,b) => a.NAME.localeCompare(b.NAME));
      }

      let isEditType;
      let comboConfig;
      this.comboFieldsConfig.forEach((comboField) => {
        if (comboField.MPMMapperName === selectedField[0].MAPPER_NAME) {
          isEditType = true;
          comboConfig = comboField;
        }
      });
      if (isEditType) {
        this.comboType = true;
        // this.comboValues = this.utilService.getLookupDomainValuesById(
        //   selectedField[0].MPM_FIELD_CONFIG_ID
        // );
        // get combo values;
        this.comboFieldSelection('COMBO', comboConfig);

        // let defaultField = this.comboValues.filter()
      }

      if (
        selectedField &&
        selectedField[0] &&
        selectedField[0].IS_DROP_DOWN === true
      ) {
        this.isDropDown = true;
        this.formAutoSuggestValues(selectedField[0].MAPPER_NAME);
      }

      if (this.filterRowData.searchOperatorId) {
        this.loadFieldValueType(this.filterRowData.searchOperatorId, true);
      }
    }
  }

  onFieldValueChange(event) {
    this.isDropDown = false;
    this.comboType = false;
    this.comboValues = [];
    this.options = [];
    const selectedFieldId = event.value;
    this.filterRowData.searchField = selectedFieldId;
    const selectedField: MPMField[] =
      this.filterRowData.availableSearchFields.filter((field) => {
        return field.MPM_FIELD_CONFIG_ID === selectedFieldId;
      });
    if (
      selectedField &&
      selectedField[0] &&
      selectedField[0].DATA_TYPE === IndexerDataTypes.NUMBER
    ) {
      selectedField[0].DATA_TYPE = IndexerDataTypes.NUMBER;
    }
    let isEditType;
    let comboConfig;
    this.comboFieldsConfig.forEach((comboField) => {
      if (comboField.MPMMapperName === selectedField[0].MAPPER_NAME) {
        isEditType = true;
        comboConfig = comboField;
      }
    });
    if (selectedField && selectedField[0] && isEditType) {
      this.comboType = true;
      // this.comboValues = this.utilService.getLookupDomainValuesById(
      //   selectedField[0].MPM_FIELD_CONFIG_ID
      // );
      // get combo values;
      this.comboFieldSelection('COMBO', comboConfig);
    }
    if (!this.comboType) {
      if (
        selectedField &&
        selectedField[0] &&
        selectedField[0].DATA_TYPE === IndexerDataTypes.STRING
      ) {
        this.charDataType = true;
        this.dateDataType = false;
        this.numberDataType = false;
      } else if (
        selectedField &&
        selectedField[0] &&
        selectedField[0].DATA_TYPE === IndexerDataTypes.DATETIME
      ) {
        this.charDataType = false;
        this.dateDataType = true;
        this.numberDataType = false;
      } else if (
        selectedField &&
        selectedField[0] &&
        selectedField[0].DATA_TYPE === IndexerDataTypes.NUMBER
      ) {
        this.charDataType = false;
        this.dateDataType = false;
        this.numberDataType = true;
      }
    } else {
      this.charDataType = false;
      this.dateDataType = false;
      this.numberDataType = false;
    }
    // this.filterRowData.issearchValueRequired = false;
    this.filterRowData.hasTwoValues = false;
    if (selectedField && selectedField[0] && selectedField[0].DATA_TYPE) {
      this.availableFieldOperators = [];
      this.availableFieldOperators =
        this.advancedSearchConfigData.searchOperatorList.filter((operator) => {
          return selectedField[0].DATA_TYPE === operator.DATA_TYPE;
        });
      this.availableFieldOperators.sort((a,b) => a.NAME.localeCompare(b.NAME));
    }
    /*   this.advancedSearchConfigData.availableSearchFields.forEach(searchFields => {
        if (searchFields.MAPPER_NAME === event.value || searchFields.MPM_FIELD_CONFIG_ID === event.value) {
          this.level = searchFields.MAPPER_NAME.includes(MPM_LEVELS.PROJECT) ? MPM_LEVELS.PROJECT : searchFields.MAPPER_NAME.includes(MPM_LEVELS.TASK) ? MPM_LEVELS.TASK : searchFields.MAPPER_NAME.includes(MPM_LEVELS.DELIVERABLE) ? MPM_LEVELS.DELIVERABLE : searchFields.MAPPER_NAME.includes(MPM_LEVELS.CAMPAIGN) ? MPM_LEVELS.CAMPAIGN : '';
          searchFields.IS_DROP_DOWN = true;
        }
        if (searchFields.MPM_FIELD_CONFIG_ID === event.value && searchFields.IS_CUSTOM_METADATA === 'true') {
          // this.isDropDown = true;
          const categoryLevelDetails = this.categoryService.getCategoryLevelDetailsByType(this.level);
          this.otmmService.getMetadatModelById(categoryLevelDetails.METADATA_MODEL_ID)
            .subscribe(metaDataModelDetails => {
              console.log(metaDataModelDetails);
              metaDataModelDetails.metadata_element_list.forEach(metadata => {
                if (metadata.id === ProjectConstant.MPM_PROJECT_CUSTOM_METADATA_GROUP || metadata.id === ProjectConstant.MPM_CAMPAIGN_CUSTOM_METADATA_GROUP) {
                  console.log("inside if")
                  metadata.metadata_element_list.forEach(data => {
                    if (data.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO && data.id === searchFields.OTMM_FIELD_ID) {
                      this.isDropDown = true;
                      searchFields.IS_DROP_DOWN = true;
                      const customDatas = this.utilService.getLookupDomainValuesById(data.domain_id);
                      customDatas.forEach(customData => {
                        this.options.push(customData.display_value);
                      });
                    }
                  });
                }
              });
            });
        }
      }); */

    AdvancedSearchStandardValue.ADVANCED_SEARCH_STANDARD_VALUE.forEach(
      (standardField) => {
        if (standardField === event.value) {
          this.isDropDown = true;
        }
      }
    );

    this.formAutoSuggestValues(event.value);
    /*  if (event.value === StandardProjectValue.PROJECT_TEAM || event.value === StandardTaskValue.TASK_TEAM || event.value === StandardDeliverableValue.DELIVERABLE_TEAM) {
       console.log(this.sharingService.getAllTeams());
       const allTeams = this.sharingService.getAllTeams();
       allTeams.forEach(teams => {
         this.options.push(teams.NAME);

       });
       if (this.options && this.options.length > 0) {
         this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
           startWith(''),
           map(value =>
             this._filter(value)

           )
         );
       }
       console.log(this.options)
     } else if (event.value === StandardProjectValue.PROJECT_PRIORITY || event.value === StandardProjectValue.PROJECT_PRIORITY_ID || event.value === StandardTaskValue.TASK_PRIORITY_ID || event.value === StandardTaskValue.TASK_PRIORITY || event.value === StandardDeliverableValue.DELIVERABLE_PRIORITY_ID || event.value === StandardDeliverableValue.DELIVERABLE_PRIORITY || event.value === StandardCampaignValue.CAMPAIGN_PRIORITY_ID || event.value === StandardCampaignValue.CAMPAIGN_PRIORITY) {
       this.entityAppDefService.getPriorities(this.level).subscribe(priorities => {
         console.log(priorities);
         priorities.forEach(priority => {
           if (event.value === StandardProjectValue.PROJECT_PRIORITY_ID || event.value === StandardTaskValue.TASK_PRIORITY_ID || event.value === StandardDeliverableValue.DELIVERABLE_PRIORITY_ID || event.value === StandardCampaignValue.CAMPAIGN_PRIORITY_ID) {
             this.options.push(priority["MPM_Priority-id"].Id);
           } else {
             this.options.push(priority.DESCRIPTION);
           }
         });
         if (this.options && this.options.length > 0) {
           this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
             startWith(''),
             map(value =>
               this._filter(value)

             )
           );
         }
         console.log(this.options)
       });

     } else if (event.value === StandardProjectValue.PROJECT_STATUS || event.value === StandardTaskValue.TASK_STATUS || event.value === StandardDeliverableValue.DELIVERABLE_STATUS || event.value === StandardCampaignValue.CAMPAIGN_STATUS) {
       this.statusService.getStatusByCategoryLevelName(this.level).subscribe(statuses => {
         console.log(statuses);
         statuses.forEach(status => {
           this.options.push(status.NAME);
         })
         console.log(this.options)
         if (this.options && this.options.length > 0) {
           this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
             startWith(''),
             map(value =>
               this._filter(value)

             )
           );
         }
       });

     } else if (event.value === StandardProjectValue.PROJECT_OWNER_NAME || event.value === StandardTaskValue.TASK_OWNER_NAME || event.value === StandardDeliverableValue.DELIVERABLE_OWNER_NAME || event.value === StandardCampaignValue.CAMPAIGN_OWNER_NAME) {
       const allPersons = this.sharingService.getAllPersons();
       allPersons.forEach(persons => {
         if (persons && persons.DisplayName && persons.DisplayName.__text) {
           this.options.push(persons.DisplayName.__text);
         }

       });
       if (this.options && this.options.length > 0) {
         this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
           startWith(''),
           map(value =>
             this._filter(value)

           )
         );
       }
       console.log(this.options)
     } */
  }
  onFieldValueSelection(event) {
    this.selectedValue = event.value;
    
  }

  filterSelection(selectionValue) {
    if (selectionValue) {
      let isValid = false;
      if (this.filterRowData.searchField) {
        isValid = true;
      }
      if (isValid) {
        if (this.comboType) {
          this.filterRowData.searchOperatorId = 'MPM.OPERATOR.CHAR.CONTAINS';
          this.filterRowData.searchOperatorName = 'contains';
          this.filterRowData.isComboType = true;
        }
        this.filterRowData.isFilterSelected = true;
        this.advancedSearchFieldForm.controls.field.disable();
        this.filterSelected.next(this.filterRowData);
      } else {
        this.notificationService.info('Kindly select all values first');
      }
    }
  }
  initializeForm() {
    this.validateSelectedField();
    this.advancedSearchFieldForm = new FormGroup({
      field: new FormControl(this.filterRowData.searchField),
      fieldType: new FormControl(this.filterRowData.searchOperatorId),
      fieldValue: new FormControl(),
      fieldSecondValue: new FormControl(this.filterRowData.searchSecondValue),
    });
    this.selectedValue = [];
    let isCombo = false;
    // this.validateSelectedField();
    if (typeof this.filterRowData.searchValue === 'string') {
      this.advancedSearchFieldForm.patchValue({
        fieldValue: this.filterRowData.searchValue,
      });
    } else {
      this.comboValues.forEach((combo) => {
        this.filterRowData.searchValue.forEach((searchValue) => {
          if (combo.displayName === searchValue.displayName) {
            this.selectedValue.push(combo);
            isCombo = true;
          }
        });
      });
    }
    if (!isCombo) {
      this.advancedSearchFieldForm.patchValue({
        fieldValue: this.filterRowData.searchValue,
      });
      if (
        this.filterRowData.searchValue &&
        this.filterRowData.searchValue != ''
      ) {
        this.filterRowData.searchValue
          .toString()
          .split(',')
          .forEach((value) => {
            this.inputs.push({ name: value });
          });
      }
    }
    this.advancedSearchFieldForm.controls.fieldType.valueChanges.subscribe(
      (selectedOperatorId) => {
        this.loadFieldValueType(selectedOperatorId, false);
      }
    );

    this.advancedSearchFieldForm.controls.fieldValue.valueChanges.subscribe(
      (selectedValue) => {
        if(typeof selectedValue=='string' ){
          if(selectedValue){
            this.filterRowData.searchValue = selectedValue;
          }
        }
        else if(selectedValue?.length && selectedValue?.length>0){
          this.filterRowData.searchValue = selectedValue;
        }
        // else if(isCombo && selectedValue.length==0){
        //   this.filterRowData.searchValue=selectedValue;
        // }
        }
    );

    this.advancedSearchFieldForm.controls.fieldSecondValue.valueChanges.subscribe(
      (selectedValue) => {
        this.filterRowData.searchSecondValue = selectedValue;
      }
    );
    //if (this.options && this.options.length > 0) {
    this.filteredOptions = this.advancedSearchFieldForm
      .get('fieldValue')
      .valueChanges.pipe(
        startWith(''),
        map((value) => this._filter(value))
      );
    // }
  }
  onOperatorChange(event) {
    this.inputs = [];
    this.values = [];
    // this.filterRowData.isFilterSelected=false;
    this.advancedSearchFieldForm.patchValue({
      fieldValue: this.values.toString(),
    });
  }

  getPriorities(categoryLevelName) {
    this.loaderService.show();
    this.comboValues = [];
    const currCategoryObj: CategoryLevel = this.sharingService
      .getCategoryLevels()
      .find((data) => {
        return data.CATEGORY_LEVEL_TYPE === categoryLevelName;
      });
    if (currCategoryObj) {
      if (
        sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES) !== null
      ) {
        let allPriorities: Array<Priority> = JSON.parse(
          sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES)
        );
        let allPriority: Array<Priority> = allPriorities.filter(
          (priority: Priority) => {
            return (
              priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id ===
              currCategoryObj['MPM_Category_Level-id'].Id
            );
          }
        );
        // let priority = [];
        // priority.push(allPriority);
        this.comboValues = allPriority;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
        this.loaderService.hide();
      } else {
        let allPriority: Array<Priority> = this.sharingService
          .getAllPriorities()
          .filter((priority: Priority) => {
            return (
              priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id ===
              currCategoryObj['MPM_Category_Level-id'].Id
            );
          });
        // let priority = [];
        // priority.push(allPriority);
        this.comboValues = allPriority;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
        this.loaderService.hide();
      }
    }
  }

  getCanCompanies() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllCanCompanies().subscribe(
      (response) => {
        let canCompanies = [];
        canCompanies = response;
        if (canCompanies?.length > 0) {
          canCompanies.forEach((canCompany) => {
            let canCompanyValue = {
              displayName: canCompany?.CanCompany,
              value: canCompany?.['Can_Supplier_Data-id']?.['Id'],
            };
            this.comboValues.push(canCompanyValue);
          });
          this.comboValues = this.sortComboValues(this.comboValues);
        }
        this.sessionInitialization();

        this.loaderService.hide();
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Can companies'
        );
      }
    );
  }
  getProgramTags() {
    this.loaderService.show();
    this.comboValues = [];
    this.monsterConfigApiService.getAllProgramTags().subscribe((response) => {
      let programTag = [];
      programTag = response;
      if (programTag?.length > 0) {
        programTag.forEach((canCompany) => {
          let programTagValue = {
            displayName: canCompany?.DisplayName,
            value: canCompany?.['Program_Tag-id'].Id,
          };
          this.comboValues.push(programTagValue);
        });
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
        this.loaderService.hide();
      }
    });
  }
  getTastingRequirements() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTastingRequirements().subscribe(
      (response) => {
        let printSuppliers = [];
        printSuppliers = response;
        if (printSuppliers?.length > 0) {
          printSuppliers.forEach((trialSupervisionQuality) => {
            let trialSupervisionQualityValue = {
              displayName: trialSupervisionQuality?.TastingRequirement,
              value:
                trialSupervisionQuality?.['Tasting_Requirement-id']?.['ItemId'],
            };
            this.comboValues.push(trialSupervisionQualityValue);
          });
          this.comboValues = this.sortComboValues(this.comboValues);
          this.sessionInitialization();
          this.loaderService.hide();
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Tasting Requirements'
        );
      }
    );
  }
  getTrialProtocolWrittenByUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTrialProtocolWrittenByUsers().subscribe(
      (response) => {
        let printSuppliers = [];
        printSuppliers = response;
        if (printSuppliers?.length > 0) {
          printSuppliers.forEach((trialSupervisionQuality) => {
            let trialSupervisionQualityValue = {
              displayName: trialSupervisionQuality?.Users,
              value:
                trialSupervisionQuality?.['Trial_Protocol_Written_By-id']?.[
                  'ItemId'
                ],
            };
            this.comboValues.push(trialSupervisionQualityValue);
          });
          this.comboValues = this.sortComboValues(this.comboValues);
          this.sessionInitialization();
          this.loaderService.hide();
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Can companies'
        );
      }
    );
  }

  sortComboValues(values){
    return values.sort((a,b) => {
      if(a.value){
        return a.value.localeCompare(b.value);
      }
      else if(a.Value){
        return a.Value.localeCompare(b.Value);
      }else if(a.VALUE){
        return a.VALUE.localeCompare(b.VALUE);
      }else if(a.displayName){
        return a.displayName.localeCompare(b.displayName);
      }else{
        return values;
      }
    });
  }

  getReportingProjectType() {
    this.loaderService.show();
    this.comboValues = [];
    this.monsterConfigApiService
      .getAllReportingProjectType()
      .subscribe((response) => {
        this.comboValues =
          this.monsterConfigApiService.transformResponseForLookup(
            response,
            'CP_Project_Type-id',
            true
          );
          this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
        this.loaderService.hide();
      });
  }
  getOPSPMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllOPSPMUsers().subscribe((response) => {
      this.comboValues = response;
      this.comboValues = this.sortComboValues(this.comboValues);
      this.sessionInitialization();
      this.loaderService.hide();
    });
  }
  getE2EPMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllE2EPMUsers().subscribe((response) => {
      this.comboValues = response;
      this.comboValues = this.sortComboValues(this.comboValues);
      this.sessionInitialization();
      this.loaderService.hide();
    });
  }
  getCorpPMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllCorpPMUsers().subscribe((response) => {
      this.comboValues = response;
      this.comboValues = this.sortComboValues(this.comboValues);
      this.sessionInitialization();
      this.loaderService.hide();
    });
  }
  getRTMUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllRTMUsers().subscribe((response) => {
      this.comboValues = response;
      this.comboValues = this.sortComboValues(this.comboValues);
      this.sessionInitialization();
      this.loaderService.hide();
    });
  }
  getOpsPlannerUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllOpsPlannerUsers().subscribe((response) => {
      this.comboValues = response;
      this.comboValues = this.sortComboValues(this.comboValues);
      this.sessionInitialization();
      this.loaderService.hide();
    });
  }

  getGFGUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllGFGUsers().subscribe((response) => {
      this.comboValues = response;
      this.comboValues = this.sortComboValues(this.comboValues);
      this.sessionInitialization();
      this.loaderService.hide();
    });
  }
  getProjectSpecialistUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService
      .getAllProjectSpecialistUsers()
      .subscribe((response) => {
        this.comboValues = response;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
        this.loaderService.hide();
      });
  }
  getRegulatoryLeadUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllRegulatoryLeadUsers().subscribe((response) => {
      this.comboValues = response;
      this.sessionInitialization();
      this.loaderService.hide();
    });
  }
  getCommercialLeadUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllCommercialLeadUsers().subscribe((response) => {
      this.comboValues = response;
      this.comboValues = this.sortComboValues(this.comboValues);
      this.sessionInitialization();
      this.loaderService.hide();
    });
  }
  getSecondaryArtworkSpecialists() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService
      .getAllSecondaryArtworkSpecialists()
      .subscribe((response) => {
        this.comboValues = response;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
        this.loaderService.hide();
      });
  }
  getDraftManufacturingLocation() {
    this.loaderService.show();
    this.comboValues = [];
    this.monsterConfigApiService
      .getAllDraftManufacturingLocations()
      .subscribe((response) => {
        let draftManufacturingLocationOptions = [];
        draftManufacturingLocationOptions = response;
        draftManufacturingLocationOptions.forEach(
          (draftManufacturingLocation) => {
            let location = {
              ItemId: draftManufacturingLocation?.Identity?.ItemId,
              Id: draftManufacturingLocation?.Identity?.Id,
              displayName:
                draftManufacturingLocation?.['R_PO_MARKET$Properties']
                  ?.DisplayName,
              regulatoryClassification:
                draftManufacturingLocation?.['R_PO_MARKET$Properties']
                  ?.RegulatoryClassification,
            };
            this.comboValues.push(location);
            this.sessionInitialization();
            this.loaderService.hide();
          }

        );
        this.comboValues = this.sortComboValues(this.comboValues);
      }),
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Country Of Manufacture'
        );
      };
  }
  getTrialSupervisionQuality() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTrialSupervisionQuality().subscribe(
      (response) => {
        let printSuppliers = [];
        printSuppliers = response;
        if (printSuppliers?.length > 0) {
          printSuppliers.forEach((trialSupervisionQuality) => {
            let trialSupervisionQualityValue = {
              displayName: trialSupervisionQuality?.Users,
              value:
                trialSupervisionQuality?.['Trial_Supervision_Quality-id']?.[
                  'ItemId'
                ],
            };
            this.comboValues.push(trialSupervisionQualityValue);
          });
          this.comboValues = this.sortComboValues(this.comboValues);
          this.sessionInitialization();
          this.loaderService.hide();
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Quality'
        );
      }
    );
  }

  getTrialSupervisionNPD() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllTrialSupervisionNPD().subscribe(
      (response) => {
        let trialSupervisionNPDResponse = [];
        trialSupervisionNPDResponse = response;
        if (trialSupervisionNPDResponse?.length > 0) {
          trialSupervisionNPDResponse.forEach((trialSupervisionNPD) => {
            let trialSupervisionNPDValue = {
              displayName: trialSupervisionNPD?.Users,
              value:
                trialSupervisionNPD?.['Trial_Supervision_NPD-id']?.['ItemId'],
            };
            this.comboValues.push(trialSupervisionNPDValue);
            this.comboValues = this.sortComboValues(this.comboValues);
            this.sessionInitialization();
            this.loaderService.hide();
          });
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Trial supervision NPD'
        );
      }
    );
  }
  getWhoWillCompleteUsers() {
    this.loaderService.show();
    this.comboValues = [];
    this.npdProjectService.getAllWhoWillCompleteUsers().subscribe(
      (response) => {
        let users = [];
        users = response;
        if (users?.length > 0) {
          users.forEach((user) => {
            let userValue = {
              displayName: user?.Users,
              value: user?.['Tech_Qual_Users-id']?.['ItemId'],
            };
            this.comboValues.push(userValue);
            this.sessionInitialization();
            this.loaderService.hide();
          });
          this.comboValues = this.sortComboValues(this.comboValues);
        }
      },
      (error) => {
        this.loaderService.hide();
        this.notificationService.error(
          'Something went wrong while fetching Who Will Complete users'
        );
      }
    );
  }
  getEarlyManufacturingSites(draftManufacturingLocation): Observable<any> {
    let draftManufacturingRequestParams = {
      'Draft_Manufacturing_Location-id': {
        Id:
          draftManufacturingLocation &&
          draftManufacturingLocation.split('.')[1],
        ItemId: draftManufacturingLocation,
      },
    };
    return new Observable((observer) => {
      this.monsterConfigApiService
        .fetchEarlyManufacturingSitesByDraftManufacturingLocation(
          draftManufacturingRequestParams
        )
        .subscribe(
          (response) => {
            this.earlyManufacturingSites =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Early_Manufacturing_Site-id',
                true
              );
            observer.next(true);
            observer.complete();
          },
          (error) => {
            observer.error(error);
          }
        );
    });
  }
  getAllDeliveryQuarters() {
    this.loaderService.show();
    this.comboValues = [];
    let deliveryQuarters = [];
    this.monsterConfigApiService
      .getAllDeliveryQuarters()
      .subscribe((response) => {
        deliveryQuarters =
          this.monsterConfigApiService.transformResponseForLookup(
            response,
            'PM_Delivery_Quarter-id',
            true
          );
        if (deliveryQuarters?.length > 0) {
          deliveryQuarters.forEach((deliveryQuarter) => {
            let deliveryQuarterValue = {
              displayName: deliveryQuarter.displayName,
              value: deliveryQuarter.ItemId,
            };
            this.comboValues.push(deliveryQuarterValue);
            this.sessionInitialization();
            this.loaderService.hide();
          });
          this.comboValues = this.sortComboValues(this.comboValues);
        }
      });
  }
  getProjectTypes(): Observable<any> {
    return new Observable((observer) => {
      this.monsterConfigApiService.getAllProjectTypes().subscribe(
        (response) => {
          this.allListConfig.projectTypes =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Project_Type-id',
              true
            );
          this.sessionInitialization();
          if (this.allListConfig.projectTypes?.length > 0) {
            this.allListConfig.projectTypes.forEach((projectType) => {
              let projectTypeValue = {
                displayName: projectType.displayName,
                value: projectType.ItemId,
              };
              this.comboValues.push(projectTypeValue);
            });
            this.comboValues = this.sortComboValues(this.comboValues);
          }
          observer.next(true);
          observer.complete();
        },
        (error) => {
          observer.error(error);
        }
      );
    });
  }
  comboFieldSelection(dataType, selectedField) {
    if (dataType === 'COMBO' || dataType === 'MULTISELECT') {
      let SessionStorageConstant = selectedField[0]
        ? selectedField[0].SessionStorageConstant
        : selectedField.SessionStorageConstant;

      if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_OPSPM_USERS
      ) {
        this.getOPSPMUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_E2EPM_USERS
      ) {
        this.getE2EPMUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_CORPPM_USERS
      ) {
        this.getCorpPMUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_RTM_USERS
      ) {
        this.getRTMUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_OPS_PLANNER_USERS
      ) {
        this.getOpsPlannerUsers();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_GFG_USERS
      ) {
        this.getGFGUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_PROJECT_SPECIALIST_USERS
      ) {
        this.getProjectSpecialistUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_REGULATORY_LEAD_USERS
      ) {
        this.getRegulatoryLeadUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_COMMERCIAL_LEAD_USERS
      ) {
        this.getCommercialLeadUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_SECONDARY_ARTWORK_USERS
      ) {
        this.getSecondaryArtworkSpecialists();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_REPORTING_TYPES
      ) {
        this.getReportingProjectType();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_CAN_COMPANIES
      ) {
        this.getCanCompanies();
      } else if (
        SessionStorageConstant === SessionStorageConstants.ALL_PRIORITIES
      ) {
        this.comboValues = [];
        this.comboValues = OverViewConstants.ProjectPriority;
        this.comboValues = this.sortComboValues(this.comboValues);
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_PROGRAM_TAG
      ) {
        this.getProgramTags();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TASTING_REQUIREMENT
      ) {
        this.getTastingRequirements();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TRIAL_PROTOCOL_WRITTEN_BY_USERS
      ) {
        this.getTrialProtocolWrittenByUsers();
      } else if (SessionStorageConstant === 'onSite') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.onSite;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
      } else if (
        SessionStorageConstant === 'BottlerCommunicationStatusConfig'
      ) {
        this.comboValues =
          ProjectBulkEditConstants.BottlerCommunicationStatusConfig;
          this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
      } else if (SessionStorageConstant === 'RAGConfig') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.RAGConfig;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_DRAFT_MANUFACTURING_LOCATIONS
      ) {
        this.getDraftManufacturingLocation();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_QUALITY
      ) {
        this.getTrialSupervisionQuality();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_WHO_WILL_COMPLETE_USERS
      ) {
        this.getWhoWillCompleteUsers();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_TRIAL_SUPERVISION_NPD
      ) {
        this.getTrialSupervisionNPD();
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_DELIVERY_QUARTERS
      ) {
        this.getAllDeliveryQuarters();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_BOTTLERS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService.getAllBottlers().subscribe((response) => {
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Bottler-id',
              true
            );
            this.comboValues = this.sortComboValues(this.comboValues);
          this.sessionInitialization();
          this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_CASE_PACK_SIZES
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllCasePackSizes()
          .subscribe((response) => {
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Case_Pack_Size-id',
                true
              );
              this.comboValues = this.sortComboValues(this.comboValues);
            this.sessionInitialization();
            this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_SKU_DETAILS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllSKUDetails()
          .subscribe((response) => {
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Sku_Details-id',
                true
              );
              this.comboValues = this.sortComboValues(this.comboValues);
            this.sessionInitialization();
            this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_MARKETS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService.getAllMarkets().subscribe((response) => {
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Markets-id',
              true,
              false,
              true
            );
            this.comboValues = this.sortComboValues(this.comboValues);
          this.sessionInitialization();
          this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_CONSUMER_UNITS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllConsumerUnits()
          .subscribe((response) => {
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Consumer_Unit_Size-id',
                true
              );
              this.comboValues = this.sortComboValues(this.comboValues);
            this.sessionInitialization();
            this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_BRANDS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService.getAllBrands().subscribe((response) => {
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Brands-id',
              true
            );
            this.comboValues = this.sortComboValues(this.comboValues);
          this.sessionInitialization();
          this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_PACKAGING_TYPES
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllPackagingTypes()
          .subscribe((response) => {
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Packaging_Type-id',
                true
              );
              this.comboValues = this.sortComboValues(this.comboValues);
            this.sessionInitialization();
            this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_PLATFORMS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService.getAllPlatforms().subscribe((response) => {
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Platforms-id',
              true
            );
            this.comboValues = this.sortComboValues(this.comboValues);
          this.sessionInitialization();
          this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_COMMERCIAL_CATEGORIES
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllCommercialCategorization()
          .subscribe((response) => {
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Commercial_Categorisation',
                true
              );
              this.comboValues = this.sortComboValues(this.comboValues);
            this.sessionInitialization();
            this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_EARLY_MANUFACTURING_SITES
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllEarlyManufacturingSites()
          .subscribe((response) => {
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Early_Manufacturing_Site-id',
                true
              );
              this.comboValues = this.sortComboValues(this.comboValues);
            this.sessionInitialization();
            this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_PROJECT_SUBTYPE
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllProjectSubtype()
          .subscribe((response) => {
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Project_Sub_Type-id',
                true
              );
              this.comboValues = this.sortComboValues(this.comboValues);
            this.sessionInitialization();
            this.loaderService.hide();
          });
      } else if (
        SessionStorageConstant ===
        NpdSessionStorageConstants.ALL_PROJECT_DETAILS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        this.monsterConfigApiService
          .getAllProjectDetail()
          .subscribe((response) => {
            this.comboValues =
              this.monsterConfigApiService.transformResponseForLookup(
                response,
                'Project_Detail-id',
                true
              );
              this.comboValues = this.sortComboValues(this.comboValues);
            this.sessionInitialization();
            this.loaderService.hide();
          });
      } else if (SessionStorageConstant === 'RequestStatus') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.RequestStatus;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
      } else if (SessionStorageConstant === 'ProjectStatus') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.ProjectStatus;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
      } else if (SessionStorageConstant === 'TaskStatus') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.TaskStatus;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
      } else if (SessionStorageConstant === 'TaskSubject') {
        this.comboValues = [];
        this.comboValues = OverViewConstants.TaskSubject;
        this.comboValues = this.sortComboValues(this.comboValues);
        this.sessionInitialization();
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_USERS
      ) {
        this.loaderService.show();
        this.comboValues = [];
        let userDetails = JSON.parse(
          sessionStorage.getItem('USER_DETAILS_WORKFLOW')
        ).User.ManagerFor.Target;
        let parameters;
        userDetails.forEach((role) => {
          if (role.Name === 'NPD Access') {
            parameters = {
              role: role.Id,
              depth: 1,
              sort: 'ascending',
            };
          }
        });
        if (!parameters) {
          this.sessionInitialization();
          this.loaderService.hide();
        } else {
          if (sessionStorage.getItem('ALL_NPD_USERS') == null) {
            this.appService
              .getUsersForRole(parameters)
              .subscribe((response) => {
                response.tuple.forEach((user) => {
                  this.comboValues.push({
                    displayName: user.old.entry.description.string,
                  });
                });
                this.comboValues = this.sortComboValues(this.comboValues);
                this.sessionInitialization();
                this.loaderService.hide();
                sessionStorage.setItem(
                  'ALL_NPD_USERS',
                  JSON.stringify(this.comboValues)
                );
              });
          } else {
            this.comboValues = JSON.parse(
              sessionStorage.getItem('ALL_NPD_USERS')
            );
            this.sessionInitialization();
            this.loaderService.hide();
          }
        }
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_PROJECT_TYPES
      ) {
        this.comboValues = [];
        this.loaderService.show();
        this.getProjectTypes().subscribe((response) => {
          this.sessionInitialization();
          this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_VARIANTS
      ) {
        this.comboValues = [];
        this.loaderService.show();
        this.monsterConfigApiService.getAllVariants().subscribe((response) => {
          this.comboValues =
            this.monsterConfigApiService.transformResponseForLookup(
              response,
              'Variant_SKU-id',
              true
            );
            this.comboValues = this.sortComboValues(this.comboValues);
          this.sessionInitialization();
          this.loaderService.hide();
        });
      } else if (
        SessionStorageConstant === NpdSessionStorageConstants.ALL_AUDIT_USERS
      ) {
        this.comboValues = [];
        this.loaderService.show();
        this.npdProjectService.getAllAuditUsers().subscribe((response) => {
          response.forEach((user) => {
            let auditUser = {
              displayName: user?.Users,
              value: user?.Users,
            };
            this.comboValues.push(auditUser);
          });
          this.comboValues = this.sortComboValues(this.comboValues);
          this.sessionInitialization();
          this.loaderService.hide();
        });
      }
      // else if (SessionStorageConstant == NpdSessionStorageConstants.ALL_NPD_ROLES) {
      //   this.loaderService.show();
      //   this.comboValues=[]
      //   if (
      //     sessionStorage.getItem(
      //       NpdSessionStorageConstants.ALL_NPD_ROLES
      //     ) == null
      //   ){
      //      this.appService.getAllTeamRoles().subscribe((response) => {
      //        response?.GetTeamRolesResponse?.Teams.MPM_Teams.MPM_Team_Role_Mapping?.forEach(
      //          (role) => {
      //            this.comboValues.push({
      //              displayName: role.NAME,
      //              value: role.NAME,
      //            });
      //          }
      //        );

      //        this.loaderService.hide();
      //      });
      //   }

      // }

      // return this.comboValues;
    }
  }
  ngOnInit() {
    this.initializeForm();
    this.filterRowData.availableSearchFields.sort((a:any,b:any) => a.DISPLAY_NAME.localeCompare(b.DISPLAY_NAME));
    if (navigator.language !== undefined) {
      this.adapter.setLocale(navigator.language);
    }
    if (this.filterRowData.isFilterSelected) {
      this.advancedSearchFieldForm.controls.field.disable();
    }
  }

  sessionInitialization() {
    if (this.filterRowData.searchValue) {
      if(typeof this.filterRowData.searchValue=='object'){
        this.comboValues.forEach((combo) => {
          this.filterRowData.searchValue.forEach((searchValue) => {
            if (combo.displayName === searchValue.displayName) {
              this.selectedValue.push(combo);
            }
          });
        });
      }
      else{
        this.comboValues.forEach((combo) => {
          if(combo.displayName==this.filterRowData.searchValue){
            this.selectedValue.push(combo);
            this.filterRowData.searchValue=this.selectedValue
          }
        }
        );
      }

    }
  }

  // getIsFilterSelected(){
  //   return this.filterRowData.isFilterSelected;
  // }
  // ngOnDestroy(){
  //   this.advancedSearchFieldForm.reset();
  // }
}
