import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdvancedSearchComponent, AdvancedSearchData, LoaderService, NotificationService, SearchSaveService, SharingService, SearchConfigService, FieldConfigService, ConditionList, MPMField, MPMFieldKeys, MPMSearchOperatorNames, MPMSearchOperators } from 'mpm-library';
import { MPMAdvancedSearchComponent } from 'src/app/core/mpm-lib/search/advanced-search/advanced-search.component';

@Component({
  selector: 'app-npd-advanced-search',
  templateUrl: './npd-advanced-search.component.html',
  styleUrls: ['./npd-advanced-search.component.scss'],
})
export class NpdAdvancedSearchComponent
  extends MPMAdvancedSearchComponent
  implements OnInit
{
  constructor(
    public dialogRef: MatDialogRef<AdvancedSearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public loaderService: LoaderService,
    public notificationService: NotificationService,
    public searchSaveService: SearchSaveService,
    public formatToLocalePipe: DatePipe,
    public sharingService: SharingService,
    public searchConfigService: SearchConfigService,
    public fieldConfigService: FieldConfigService,
    public datePipe: DatePipe
  ) {
    super(
      dialogRef,
      data,
      loaderService,
      notificationService,
      searchSaveService,
      formatToLocalePipe,
      sharingService,
      searchConfigService,
      fieldConfigService,
      datePipe
    );
  }

  search(isSavedSearch) {
    if (this.validateForSearch()) {
      const searchConditions: ConditionList[] = [];
      this.filterRowList.forEach((rowData) => {
        if (rowData.searchValue && rowData.searchValue instanceof Date) {
          rowData.searchValue = this.datePipe
            .transform(new Date(rowData.searchValue), 'yyyy-MM-dd')
            .concat('T00:00:00Z');
          if (
            rowData.searchSecondValue &&
            rowData.searchSecondValue instanceof Date
          ) {
            rowData.searchSecondValue = this.datePipe
              .transform(new Date(rowData.searchSecondValue), 'yyyy-MM-dd')
              .concat('T00:00:00Z');
          }
        }
        // rowData.availableSearchFields = [];
        const currMPMField: MPMField =
          this.fieldConfigService.getFieldByKeyValue(
            MPMFieldKeys.MPM_FIELD_CONFIG_ID,
            rowData.searchField
          );
        const fieldId = currMPMField
          ? this.fieldConfigService.formIndexerIdFromField(currMPMField)
          : rowData.searchField;
        const currentField: [] = this.data?.fieldConfigs;
        let isComboType;
        currentField.forEach((comboField) => {
          if (comboField['MPMMapperName'] === currMPMField.MAPPER_NAME) {
            isComboType = true;
          }
        });
        if (isComboType) {
          const values = rowData.searchValue;
          const condidtion: ConditionList = {
            isAdvancedSearch: true,
            searchIdentifier:
              this.data.advancedSearchConfigData.searchIdentifier
                .R_PO_ADVANCED_SEARCH_CONFIG,
            field_id: fieldId,
            relational_operator: 'AND',
            relational_operator_id: MPMSearchOperators.IS,
            relational_operator_name: MPMSearchOperatorNames.IS,
            type: currMPMField ? currMPMField.DATA_TYPE : 'string',
            mapper_field_id: rowData.searchField,
            mapper_name: currMPMField.MAPPER_NAME,
            view: this.data.advancedSearchConfigData.searchIdentifier.VIEW,
          };
          if (values.length === 1) {
            const conditionCopy = Object.assign({}, condidtion);
            conditionCopy.filterRowData = rowData;
            conditionCopy.filterRowData.isComboType = true;
            conditionCopy.left_paren = '(';
            conditionCopy.right_paren = ')';
            conditionCopy.display_value = values[0].displayName;
            conditionCopy.value = values[0].displayName;
            conditionCopy.relational_operator = 'AND';
            searchConditions.push(conditionCopy);
          } else {
            const lastValueIndex = values.length - 1;
            let conditionCopy = Object.assign({}, condidtion);
            for (let i = 0; i < values.length; i++) {
              const conditionCopy = Object.assign({}, condidtion);
              conditionCopy.display_value = values[i].displayName;
              conditionCopy.value = values[i].displayName;
              conditionCopy.filterRowData = rowData;

              if (i === 0) {
                conditionCopy.left_paren = '(';
              } else if (i === lastValueIndex) {
                conditionCopy.right_paren = ')';
                conditionCopy.relational_operator = 'OR';
              } else {
                conditionCopy.relational_operator = 'OR';
              }
              searchConditions.push(conditionCopy);
            }
          }
        } else {
          let value = rowData.searchValue;
          if (rowData.searchSecondValue) {
            value = value + ' and ' + rowData.searchSecondValue;
          }
          if (typeof value === 'string') {
            let values = value.split(',');
            const condidtion: ConditionList = {
              isAdvancedSearch: true,
              searchIdentifier:
                this.data.advancedSearchConfigData.searchIdentifier
                  .R_PO_ADVANCED_SEARCH_CONFIG,
              filterRowData: rowData,
              field_id: fieldId,
              type: currMPMField ? currMPMField.DATA_TYPE : 'string',
              value: values,
              relational_operator_id: rowData.searchOperatorId
                ? rowData.searchOperatorId
                : MPMSearchOperators.IS,
              relational_operator_name: rowData.searchOperatorName
                ? rowData.searchOperatorName
                : MPMSearchOperatorNames.IS,
              relational_operator: 'AND',
              mapper_field_id: rowData.searchField,
              mapper_name: currMPMField.MAPPER_NAME,
              view: this.data.advancedSearchConfigData.searchIdentifier.VIEW,
            };
            if (values.length === 1) {
              const conditionCopy = Object.assign({}, condidtion);
              conditionCopy.filterRowData = rowData;
              conditionCopy.left_paren = '(';
              conditionCopy.right_paren = ')';
              conditionCopy.display_value = values[0];
              conditionCopy.value = values[0];
              conditionCopy.relational_operator = 'AND';
              searchConditions.push(conditionCopy);
            } else {
              const lastValueIndex = values.length - 1;
              let conditionCopy = Object.assign({}, condidtion);
              for (let i = 0; i < values.length; i++) {
                const conditionCopy = Object.assign({}, condidtion);
                conditionCopy.display_value = values[i];
                conditionCopy.value = values[i];
                conditionCopy.filterRowData = rowData;
                if (i === 0) {
                  conditionCopy.left_paren = '(';
                } else if (i === lastValueIndex) {
                  conditionCopy.right_paren = ')';
                  conditionCopy.relational_operator = 'OR';
                } else {
                  conditionCopy.relational_operator = 'OR';
                }
                searchConditions.push(conditionCopy);
              }
            }
          }
          // if (rowData.searchSecondValue) {

          //   value = value + ' and ' + rowData.searchSecondValue;

          // }
          else {
            const condition: ConditionList = {
              isAdvancedSearch: true,
              searchIdentifier:
                this.data.advancedSearchConfigData.searchIdentifier
                  .R_PO_ADVANCED_SEARCH_CONFIG,
              filterRowData: rowData,
              field_id: fieldId,
              type: currMPMField ? currMPMField.DATA_TYPE : 'string',
              value: value,
              relational_operator_id: rowData.searchOperatorId
                ? rowData.searchOperatorId
                : MPMSearchOperators.IS,
              relational_operator_name: rowData.searchOperatorName
                ? rowData.searchOperatorName
                : MPMSearchOperatorNames.IS,
              relational_operator: 'and',
              mapper_field_id: rowData.searchField,
              mapper_name: currMPMField.MAPPER_NAME,
              view: this.data.advancedSearchConfigData.searchIdentifier.VIEW,
            };
            searchConditions.push(condition);
          }
        }
      });
      if (isSavedSearch) {
        if (this.data && this.data.recentSearch) {
          this.data.recentSearch.map((rowData) => {
            if (rowData.isEdit) {
              rowData.isEdit = false;
            }
          });
        }
        this.saveSearch(
          searchConditions,
          this.data.advancedSearchConfigData.searchIdentifier.VIEW
        );
      } else {
        this.close(searchConditions);
      }
    }
  }
  mapSeachConfig() {
    if (this.data.savedSearch) {
      this.searchTobeSavedName = this.data.savedSearch.NAME;
      let conditions: ConditionList[] = [];
      if (
        this.data.savedSearch.SEARCH_DATA &&
        this.data.savedSearch.SEARCH_DATA.search_condition_list &&
        this.data.savedSearch.SEARCH_DATA.search_condition_list.search_condition
      ) {
        conditions =
          this.data.savedSearch.SEARCH_DATA.search_condition_list
            .search_condition;
      }
      if (conditions[0] && conditions[0].searchIdentifier) {
        const advConfig = this.searchConfigService.getAdvancedSearchConfigById(
          conditions[0].searchIdentifier
        );
        this.allSearchFields = advConfig.R_PM_Search_Fields;
        conditions.forEach((condition) => {
          const count = this.filterRowList.length + 1;
          this.setAvialbleSearchFiels();
          const value = condition.value != null ? condition.value : '';
          this.filterRowList.push({
            rowId: count,
            searchField: condition.mapper_field_id,
            searchOperatorId: condition.relational_operator_id,
            searchOperatorName: condition.relational_operator_name,
            searchValue: value,
            searchSecondValue: value.includes('and')
              ? value.split('and ').length > 0
                ? value.split('and ')[1] !== undefined
                  ? value.split('and ')[1]
                  : ''
                : ''
              : '',
            issearchValueRequired: value ? true : false,
            isFilterSelected: true,
            hasTwoValues: '',
            availableSearchFields: this.allSearchFields.filter(
              (searchFieldItem) => {
                return (
                  searchFieldItem.MPM_FIELD_CONFIG_ID ===
                  condition.mapper_field_id
                );
              }
            ),
          });
          this.setAvialbleSearchFiels();
        });
        this.advancedSearchConfigData.availableSearchFields =
          this.allSearchFields;
        this.advancedSearchConfigData.searchOperatorList =
          this.data.advancedSearchConfigData.searchOperatorList;
      }
    } else {
      if (
        this.data.advancedSearchConfigData &&
        this.data.advancedSearchConfigData.searchIdentifier
      ) {
        const searchConfig =
          this.data.advancedSearchConfigData.advancedSearchConfiguration.filter(
            (serachConfig) => {
              return (
                serachConfig['MPM_Advanced_Search_Config-id'].Id ===
                this.data.advancedSearchConfigData.searchIdentifier
                  .R_PO_ADVANCED_SEARCH_CONFIG
              );
            }
          );

        // this.data.advancedSearchConfigData.searchIdentifier = searchConfig[0]['MPM_Advanced_Search_Config-id'].Id;

        this.allSearchFields = searchConfig[0].R_PM_Search_Fields;
        this.advancedSearchConfigData.availableSearchFields =
          searchConfig[0].R_PM_Search_Fields;
        this.advancedSearchConfigData.searchOperatorList =
          this.data.advancedSearchConfigData.searchOperatorList;
        this.setAvialbleSearchFiels();
        // this.addFilter();
      }
    }
  }
  close(data?: any) {
    if (data && data !== true) {
      this.sharingService.setRecentSearch(data);
    }
    if (data === true && this.data && this.data.recentSearch) {
      this.data.recentSearch.map((rowData) => {
        if (rowData.isEdit) {
          rowData.isEdit = false;
        }
      });
    }
    if(data == false){
      return
    }
    this.dialogRef.close(data);
  }
 
  ngOnInit() {
    this.advancedSearchConfigData = {
      availableSearchFields: [],
      searchOperatorList: [],
    };
    this.mapSeachConfig();
    if (this.data.recentSearch) {
      this.filterRowList = [];
      this.data.recentSearch.map((rowData) => {
        if (rowData.filterRowData) {
          rowData.filterRowData.availableSearchFields =
            this.allSearchFields.filter((searchFieldItem) => {
              return (
                searchFieldItem.MPM_FIELD_CONFIG_ID ===
                rowData.filterRowData.searchField
              );
            });
          this.filterRowList.push(rowData.filterRowData);
          this.setAvialbleSearchFiels();
        }
      });
    }
  }
}
