import { I, L, X } from '@angular/cdk/keycodes';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {
  StatusService,
  SharingService,
  TaskService,
  NotificationService,
  ConfirmationModalComponent,
  StatusTypes,
  MPM_LEVELS,
  LoaderService,
  SearchRequest,
  SearchResponse,
  IndexerService,
  IndexerDataTypes,
  MPMSearchOperators,
  CustomWorkflowFieldComponent,
  ProjectUtilService,
} from 'mpm-library';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import {
  tap,
  startWith,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  map,
} from 'rxjs/operators';
import { RequestConstant } from '../../request-management/constants/request-constant';
import { WorkFlowConstants } from '../constants/workflow-customconfig';

@Component({
  selector: 'app-npd-custom-workflow-field',
  templateUrl: './npd-custom-workflow-field.component.html',
  styleUrls: ['./npd-custom-workflow-field.component.scss'],
})
export class NpdCustomWorkflowFieldComponent extends CustomWorkflowFieldComponent implements OnInit {
  @Input() ruleData;
  @Input() ruleList;
  @Input() projectRuleList;
  @Input() projectBussinessId;
  @Input() ruleListGroup;
  @Input() taskData: any[];
  @Input() sourceTaskData: any[];
  @Input() isRulesInitiated;
  @Input() isCurrentTaskRule;
  @Output() closeCallbackHandler = new EventEmitter<any>();
  @Output() removeWorkflowRuleHandler = new EventEmitter<any>();
  @Output() enableInitiateWorkflowHandler = new EventEmitter<any>();
  // @Output() ruleChangeDetect = new EventEmitter<any>();

  @Output() isRuleChangedValue = new EventEmitter<boolean>();

  otherProjectWorkflowId;


  customWorkflowFieldForm: FormGroup;
  triggerTypes = [
    {
      name: 'Status',
      value: 'STATUS',
    },
    {
      name: 'Action',
      value: 'ACTION',
    },
  ];
  targetTypes = [
    {
      name: 'Task',
      value: 'TASK',
    },
    {
      name: 'Event',
      value: 'EVENT',
    },
  ];
  triggerData = [];
  targetData = [];
  taskList = [];
  projectId;
  isRuleChanged;
  taskStatuses;
  currentRuleList;
  allRuleList = [];

  options = [];
  allProjectRequestTasks = [];
  //filteredOptions: Observable<any>;
  filteredOptions = [];
  allRequests = [];
  currentSourceTasks
  constructor(
    public statusService: StatusService,
    public sharingService: SharingService,
    public taskService: TaskService,
    public notificationService: NotificationService,
    public loaderService: LoaderService,
    public dialog: MatDialog,
    private indexerService: IndexerService,
    public projectUtilService: ProjectUtilService
  ) {
    /* this.filteredOptions = this.customWorkflowFieldForm?.controls.requestId.valueChanges.pipe(
    startWith(''),
    debounceTime(400),
    distinctUntilChanged(),
    switchMap(val => {
          return '';//this.filter(val || '')
     }))  */
    super(statusService, sharingService, taskService, notificationService, projectUtilService, dialog);
  }

  displayFn(project?: any): string | undefined {
    return project ? project?.requestBusinessId : undefined;
  }

  /*   filter(val: string): Observable<any[]> {
    // call the service which makes the http-request
    return this.service.getData().pipe(
      map((response) =>
        response.filter((option) => {
          return option.name.toLowerCase().indexOf(val.toLowerCase()) === 0;
        })
      )
    );
  } */

  /* npd logic */
  // Forming search field for mpm-indexer input
  getSearchField(
    type,
    field_id,
    operator_id,
    operator_name,
    value?,
    field_operator?
  ) {
    let searchField = {
      type: type,
      field_id: field_id,
      relational_operator_id: operator_id,
      relational_operator_name: operator_name,
      value: value,
      relational_operator: field_operator,
    };
    return searchField;
  }

  getFilteredRequests(requestId, flag?: boolean): Observable<any> {
    //let baseSearchConditionList = RequestConstant.GET_REQUEST_SEARCH_CONDITION_LIST
    let searchConditionList = [];
    this.allRequests = [];
    //searchConditionList.push(baseSearchConditionList);
    let projectRequestConditionList = [];
    if (requestId) {
      projectRequestConditionList = [
        // this.getSearchField("string", "RQ_REQUEST_STATUS", "MPM.OPERATOR.CHAR.IS", "is", "Completed","AND"),
        // this.getSearchField(IndexerDataTypes.STRING, 'RQ_REQUEST_BUSINESS_ID', 'MPM.OPERATOR.CHAR.CONTAINS', 'contains', requestId , MPMSearchOperators.AND)
        //PROJECT_STATUS_ID: 16393//PROJECT_STATUS_TYPE: "INTERMEDIATE"PROJECT_STATUS_VALUE: "In Progress"
        this.getSearchField(
          'string',
          'CONTENT_TYPE',
          'MPM.OPERATOR.CHAR.IS',
          'is',
          'MPM_PROJECT'
        ),
        //this.getSearchField("string", "CONTENT_TYPE", "MPM.OPERATOR.CHAR.IS", "is", "MPM_PROJECT" ,"AND"),
        this.getSearchField(
          'string',
          WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID,
          'MPM.OPERATOR.CHAR.CONTAINS',
          'contains',
          requestId,
          'AND'
        ), //_string
      ]; //NPD_PROJECT_BUSINESS_ID: "EM1982468"//PR_REQUEST_ID_string
    }

    searchConditionList = searchConditionList.concat(
      projectRequestConditionList
    );

    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: '',
      search_condition_list: {
        search_condition: searchConditionList,
      },
      facet_condition_list: {
        facet_condition: [],
      },
      sorting_list: {
        sort: [],
      },
      cursor: {
        page_index: 0,
        page_size: 100,
      },
    };
    return new Observable((observer) => {
      this.loaderService.show();
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          if (response.data.length > 0) {
            let requests = response.data;
            requests.forEach((data) => {
              this.allRequests.push({
                requestBusinessId:
                  data?.[
                  WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID
                  ], //PR_REQUEST_ID_string,
                // projectData: data,
              });
            });
          }
          this.loaderService.hide();
          observer.next();
          observer.complete();
          if (!this.ruleData.isExist && !flag) {
            let obj = this.allRequests.find(x => x.requestBusinessId == this.ruleData.requestId)
            this.customWorkflowFieldForm.get('requestId').setValue(obj);
            this.getTaskDetailsByRequestBusinessId(obj?.requestBusinessId).subscribe();
            this.customWorkflowFieldForm.get('triggerData').setValue(this.ruleData.triggerData);


          }


        },
        () => {
          this.loaderService.hide();
          observer.error();
        }
      );
    });
  }

  changeAutoComplete(event) {
    this.getTaskDetailsByRequestBusinessId(
      event?.option?.value?.requestBusinessId
    ).subscribe(); //.requestBusinessId
  }

  getTaskDetailsByRequestBusinessId(requestBusinessId, flag?: boolean) {
    let searchConditionList = [
      this.getSearchField(
        'string',
        'CONTENT_TYPE',
        'MPM.OPERATOR.CHAR.IS',
        'is',
        'MPM_TASK'
      ),
      this.getSearchField(
        'string',
        WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID,
        'MPM.OPERATOR.CHAR.IS',
        'is',
        requestBusinessId,
        'AND'
      ),
    ];

    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: '',
      search_condition_list: {
        search_condition: searchConditionList,
      },
      facet_condition_list: {
        facet_condition: [],
      },
      sorting_list: {
        sort: [
          {
            field_id: 'TASK_START_DATE', //ID
            order: 'ASC',
          },
        ],
      },
      cursor: {
        page_index: 0,
        page_size: 200,
      },
    };
    //this.allProjectRequestTasks = []
    if (!flag) {
      this.updateFormValues();
    }
    return new Observable((observer) => {
      this.loaderService.show();
      this.indexerService.search(parameters).subscribe(
        (response: SearchResponse) => {
          this.taskList = [];
          if (response.data.length > 0) {
            let allTasks = response.data;
            allTasks.forEach((task) => {
              if (
                task.TASK_STATUS_TYPE === StatusTypes.INITIAL ||
                task.TASK_STATUS_TYPE === StatusTypes.INTERMEDIATE ||
                (this.isRulesInitiated)
              ) {
                this.taskList.push({
                  name: task.TASK_NAME,
                  value: task.ID,
                });
              }
            });
          }
          observer.next();
          observer.complete();
          if (!this.ruleData.isExist) {
            this.customWorkflowFieldForm.get('triggerData').setValue(this.ruleData.triggerData);
          }
          this.loaderService.hide();

        },
        () => {
          observer.error();
          this.loaderService.hide();
        }
      );
    });
  }


  updateFormValues() {
    this.customWorkflowFieldForm.patchValue({
      currentTask: '',
      triggerData: '',
      targetData: '',
    });
  }

  addOtherProjectWorkflowRule(successCallback?, errorCallback?) {
    let projectId;
    projectId =
      this.customWorkflowFieldForm.getRawValue()?.requestId.projectData?.ID;
    let customWorkflow = this.customWorkflowFieldForm.getRawValue();
    this.taskService
      .createWorkflowRule(
        this.customWorkflowFieldForm.getRawValue(),
        projectId,
        this.otherProjectWorkflowId
      )
      .subscribe((response) => {
        if (response) {
          //this.isRuleChanged = false;
          this.notificationService.info('Workflow Rule has been saved');
          this.otherProjectWorkflowId =
            response['Workflow_Rules-id'] && response['Workflow_Rules-id'].Id
              ? response['Workflow_Rules-id'].Id
              : null;
          this.enableInitiateWorkflowHandler.next(projectId);
          /*  this.loaderService.show();
        this.taskService.triggerWorkflow(projectId, true).subscribe(response => {
          this.loaderService.hide();
          if (response) {
            this.notificationService.info('Workflow Rule has been saved and Custom Workflow rules initiated successfully, dates will be aligned as per rule configured');
          }
        }); */
          if (successCallback) {
            successCallback();
          }
        } else {
          //this.notificationService.error('Something went wrong while creating workflow rule');
          if (errorCallback) {
            errorCallback();
          }
        }
      });
  }

  addRules() {
    this.addWorkflowRule(
      () => {
        if (
          this.customWorkflowFieldForm.getRawValue().requestId &&
          this.customWorkflowFieldForm.getRawValue()?.requestId.projectData
            ?.ID != this.projectId &&
          this.customWorkflowFieldForm.getRawValue().triggerType === 'STATUS' &&
          this.customWorkflowFieldForm.getRawValue().targetType === 'TASK'
        ) {
          this.addOtherProjectWorkflowRule(
            () => {
              this.notificationService.info('Workflow Rule has been saved');
            },
            (error) => {
              this.notificationService.error(
                'Something went wrong while creating workflow rule'
              );
            }
          );
        } else {
          this.notificationService.info('Workflow Rule has been saved');
        }
      },
      (error) => {
        this.notificationService.error(
          'Something went wrong while creating workflow rule'
        );
      }
    );
  }



  addWorkflowRule(successCallback?, errorCallback?) {
    this.ruleListGroup;
    // console.log(this.ruleListGroup)
    if (
      this.customWorkflowFieldForm.getRawValue()?.requestId.projectData?.ID !=
      this.projectId &&
      (this.customWorkflowFieldForm.getRawValue().triggerType != 'STATUS' ||
        this.customWorkflowFieldForm.getRawValue().targetType != 'TASK')
    ) {
      //
      this.notificationService.warn('Rule cannot be saved.');
      //if (errorCallback) { errorCallback(); }
    } else {
      this.taskService
        .createWorkflowRule(
          this.customWorkflowFieldForm.getRawValue(),
          this.projectId,
          this.ruleData.workflowRuleId
        )
        .subscribe((response) => {
          if (response) {
            this.isRuleChanged = false;
            this.ruleData.currentTask =
              this.customWorkflowFieldForm.getRawValue().currentTask;
            this.ruleData.triggerType =
              this.customWorkflowFieldForm.getRawValue().triggerType;
            this.ruleData.triggerData =
              this.customWorkflowFieldForm.getRawValue().triggerData;
            this.ruleData.targetType =
              this.customWorkflowFieldForm.getRawValue().targetType;
            this.ruleData.targetData =
              this.customWorkflowFieldForm.getRawValue().targetData;
            this.ruleData.isInitialRule =
              this.customWorkflowFieldForm.getRawValue().isInitialRule;
            this.ruleData.isRuleAdded = true;
            this.ruleData.workflowRuleId =
              response['Workflow_Rules-id'] && response['Workflow_Rules-id'].Id
                ? response['Workflow_Rules-id'].Id
                : null;
            this.enableInitiateWorkflowHandler.next();
            if (successCallback) {
              successCallback();
            }
          } else {
            this.notificationService.error('Something went wrong while creating workflow rule');
            if (errorCallback) {
              errorCallback();
            }
          }
        });
    }
  }

  validateRemoveWorkflowRule() {
    if (this.customWorkflowFieldForm && this.customWorkflowFieldForm.pristine) {
      this.removeRules();
    } else {
      const dialogRef = this.dialog.open(ConfirmationModalComponent, {
        width: '40%',
        disableClose: true,
        data: {
          message: 'Are you sure you want to close?',
          submitButton: 'Yes',
          cancelButton: 'No',
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result && result.isTrue) {
          this.removeRules();
        }
      });
    }
  }



  removeRules() {
    this.removeWorkflowRule(
      () => {
        if (
          this.customWorkflowFieldForm.getRawValue().requestId &&
          this.otherProjectWorkflowId
        ) {
          this.removeOtherProjectWorkflowRule(
            () => {
              this.notificationService.info('Workflow Rule has been removed');
              this.isRuleChangedValue.emit(false);
            },
            (error) => {
              this.notificationService.error(
                'Something went wrong while removing workflow rule'
              );
            }
          );
        } else {
          this.notificationService.info('Workflow Rule has been removed');
          this.isRuleChangedValue.emit(false);
        }
      },
      (error) => {
        this.notificationService.error(
          'Something went wrong while removing workflow rule'
        );
      }
    );
  }

  removeWorkflowRule(successCallback?, errorCallback?) {
    if (this.ruleData.workflowRuleId) {
      this.taskService
        .removeWorkflowRule(this.ruleData.workflowRuleId, this.ruleData)
        .subscribe((response) => {
          if (response) {
            this.removeWorkflowRuleHandler.next(this.ruleData.workflowRuleId);
            //this.notificationService.info('Workflow Rule has been removed');
            if (successCallback) {
              successCallback();
            }
          } else {
            if (errorCallback) {
              errorCallback();
            }
            //this.notificationService.error('Something went wrong while removing workflow rule');
          }
        });
    } else {
      this.removeWorkflowRuleHandler.next(null);
    }
  }


  removeOtherProjectWorkflowRule(successCallback?, errorCallback?) {
    if (this.otherProjectWorkflowId) {
      this.taskService
        .removeWorkflowRule(this.otherProjectWorkflowId, this.ruleData)
        .subscribe((response) => {
          if (response) {
            this.removeWorkflowRuleHandler.next(this.otherProjectWorkflowId);
            if (successCallback) {
              successCallback();
            }

          } else {
            if (errorCallback) {
              errorCallback();
            }
          }
        });
    } else {
      this.removeWorkflowRuleHandler.next(null);
      if (errorCallback) {
        errorCallback();
      }
    }
  }

  closeDialog() {
    if (this.customWorkflowFieldForm && this.customWorkflowFieldForm.pristine) {
      this.closeCallbackHandler.next();
    } else {
      const dialogRef = this.dialog.open(ConfirmationModalComponent, {
        width: '40%',
        disableClose: true,
        data: {
          message: 'Are you sure you want to close?',
          submitButton: 'Yes',
          cancelButton: 'No',
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result && result.isTrue) {
          this.closeCallbackHandler.next();
        }
      });
    }
  }

  initializeForm() {
    this.ruleList.forEach((rule) => {
      const existingRule = this.allRuleList.find(
        (selectedRule) => this.compareObjects(selectedRule, rule)
      );
      if (!existingRule) {
        this.allRuleList.push(rule);
      }
    });
    this.projectRuleList.forEach((rule) => {
      const existingRule = this.allRuleList.find(
        (selectedRule) => this.compareObjects(selectedRule, rule)
      );
      if (!existingRule) {
        this.allRuleList.push(rule);
      }
    });
    this.currentRuleList = [];
    this.allRuleList.forEach((rule) => {
      if (this.compareObjects(this.ruleData, rule)) {
        this.currentRuleList.push(rule);
      }
    });
    if (this.taskData?.[0]?.PROJECT_ITEM_ID) {
      const projectItemId = this.taskData[0].PROJECT_ITEM_ID;
      this.projectId = projectItemId.split('.')[1];
    }
    if (this.ruleData.isExist) {
      this.sourceTaskData?.forEach((task) => {
        if (
          task.TASK_STATUS_TYPE === StatusTypes.INITIAL ||
          task.TASK_STATUS_TYPE === StatusTypes.INTERMEDIATE ||
          (this.ruleData.isExist && this.isRulesInitiated)
        ) {
          if (task?.IS_APPROVAL_TASK === 'true') {
            /* if (this.currentRuleList) {
              const hasParentTaskRule = this.currentRuleList.find(rule => rule.currentTask === String(task.TASK_PARENT_ID));
              if (hasParentTaskRule) {
                this.taskList.push({
                  name: task.TASK_NAME,
                  value: task.ID
                });
              }
            } */
            if (this.currentRuleList && this.currentRuleList.length > 0) {
              const hasRule = this.currentRuleList.find(
                (rule) => rule.targetData === task.ID
              );
              if (hasRule) {
                this.taskList.push({
                  name: task.TASK_NAME,
                  value: task.ID,
                });
              }
            }
          } else {
            this.taskList.push({
              name: task.TASK_NAME,
              value: task.ID,
            });
          }
        }
      });
    }

    this.taskData?.forEach((task) => {
      if (
        task.TASK_STATUS_TYPE === StatusTypes.INITIAL ||
        task.TASK_STATUS_TYPE === StatusTypes.INTERMEDIATE ||
        (this.ruleData.isExist && this.isRulesInitiated)
      ) {
        /*  if (task.IS_APPROVAL_TASK === 'true') {
          if (this.currentRuleList && this.currentRuleList.length > 0) {
            const hasRule = this.currentRuleList.find(rule => rule.targetData === task.ID);
            if (hasRule) {
              this.taskList.push({
                name: task.TASK_NAME,
                value: task.ID
              });
            }
          }
        } else {
          this.taskList.push({
            name: task.TASK_NAME,
            value: task.ID
          });
        }*/
      }
    });

    this.targetTask = this.taskData.find(task => task.ID === this.ruleData.targetData);
    this.customWorkflowFieldForm = new FormGroup({
      requestId: new FormControl({
        value: this.ruleData.requestId,
        disabled: (this.isCurrentTaskRule ? true : (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (this.targetTask?.IS_ACTIVE_TASK === 'true' || this.targetTask?.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask?.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false))
      }),
      currentTask: new FormControl({
        value: this.ruleData.currentTask,
        disabled: (this.isCurrentTaskRule ? true : (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (this.targetTask?.IS_ACTIVE_TASK === 'true' || this.targetTask?.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask?.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false))
      }, [Validators.required]),
      triggerType: new FormControl({ value: this.ruleData.triggerType, disabled: (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (this.targetTask?.IS_ACTIVE_TASK === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) }, [Validators.required]),
      triggerData: new FormControl({ value: '', disabled: this.ruleData.triggerType ? (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (this.targetTask?.IS_ACTIVE_TASK === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) : true }, [Validators.required]),
      targetType: new FormControl({ value: this.ruleData.targetType, disabled: (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (this.targetTask?.IS_ACTIVE_TASK === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) }, [Validators.required]),
      targetData: new FormControl({ value: '', disabled: this.ruleData.targetType ? (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (this.targetTask?.IS_ACTIVE_TASK === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) : true }, [Validators.required]),
      isInitialRule: new FormControl({ value: this.ruleData.isInitialRule, disabled: (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (this.targetTask?.IS_ACTIVE_TASK === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) }),
    });

    // Manually trigger valueChanges event with the default value
    if (!this.ruleData.isExist) {
      // this.getFilteredRequests(this.ruleData.requestId).subscribe();
      this.allRequests = [{ requestBusinessId: this.ruleData.requestId }]
      this.customWorkflowFieldForm.get('requestId').setValue({ requestBusinessId: this.ruleData.requestId });
      this.getTaskDetailsByRequestBusinessId(this.ruleData.requestId).subscribe();
      this.customWorkflowFieldForm.get('triggerData').setValue(this.ruleData.triggerData);


    }

    if (!this.ruleData.isExist) {

      this.customWorkflowFieldForm?.controls.requestId.valueChanges.subscribe(
        (requestId) => {
          if (requestId && requestId.length && requestId.length >= 3) {
            this.getFilteredRequests(requestId, true).subscribe();
          } else {
            this.filteredOptions = [];
          }
        }
      );
    }
    /* this.customWorkflowFieldForm = new FormGroup({
      currentTask: new FormControl({
        value: this.ruleData.currentTask,
        disabled: ((this.ruleData.isActiveTask || this.ruleData.isFinalTask) ? true : (this.ruleData.isTaskView ? true : false))
      }, [Validators.required]),
      triggerType: new FormControl({ value: this.ruleData.triggerType, disabled: this.ruleData.isFinalTask }, [Validators.required]),
      triggerData: new FormControl({ value: '', disabled: this.ruleData.triggerType ? this.ruleData.isFinalTask : true }, [Validators.required]),
      targetType: new FormControl({ value: this.ruleData.targetType, disabled: this.ruleData.isFinalTask }, [Validators.required]),
      targetData: new FormControl({ value: '', disabled: this.ruleData.targetType ? this.ruleData.isFinalTask : true }, [Validators.required]),
      isInitialRule: new FormControl({ value: this.ruleData.isInitialRule, disabled: (this.isRulesInitiated ? (this.ruleData.isExist ? true : false) : false) })
    }); */
    const taskStatus = this.statusService.getAllStatusBycategoryName(
      MPM_LEVELS.TASK
    );
    // this.taskStatuses = this.statusService.getAllStatusBycategoryName(MPM_LEVELS.TASK);
    this.taskStatuses =
      taskStatus && taskStatus.length > 0 && Array.isArray(taskStatus)
        ? taskStatus[0]
        : taskStatus;
    if (this.customWorkflowFieldForm.getRawValue().currentTask) {
      const currentTask = this.taskData?.find(
        (task) =>
          task.ID === this.customWorkflowFieldForm.getRawValue().currentTask
      );
      let isTargetTask = false;
      this.allRuleList.forEach((rule) => {
        if (
          rule.targetData ===
          this.customWorkflowFieldForm.getRawValue().currentTask
        ) {
          const status =
            this.taskStatuses.find(
              (taskStatus) =>
                taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED
            ) == undefined
              ? []
              : this.taskStatuses.find(
                (taskStatus) =>
                  taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED
              );
          if (
            status.length != 0 &&
            !(
              rule.triggerType === 'STATUS' &&
              status['MPM_Status-id'].Id === rule.triggerData
            )
          ) {
            isTargetTask = true;
          }
        }
      });
      if (isTargetTask || currentTask?.IS_APPROVAL_TASK === 'true') {
        this.customWorkflowFieldForm.controls.isInitialRule.disable();
      }
    }
    if (this.customWorkflowFieldForm.getRawValue().triggerType) {
      if (this.customWorkflowFieldForm.getRawValue().triggerType === 'STATUS') {
        const currentTask = this.taskData?.find(
          (task) =>
            task.ID === this.customWorkflowFieldForm.controls.currentTask.value
        );
        this.taskStatuses.forEach((status) => {
          if (
            status.STATUS_TYPE !== StatusTypes.INITIAL &&
            status.STATUS_TYPE !== StatusTypes.FINAL_CANCELLED &&
            !(
              status.STATUS_TYPE === StatusTypes.FINAL_REJECTED &&
              currentTask?.IS_APPROVAL_TASK === 'false'
            )
          ) {
            this.triggerData.push({
              name: status.NAME,
              value: status['MPM_Status-id'].Id,
            });
          }
        });
        this.customWorkflowFieldForm.controls.triggerData.setValue(
          this.ruleData.triggerData
        );
      } else {
        const taskWorkflowActions =
          this.sharingService.getTaskWorkflowActions();
        taskWorkflowActions.forEach((action) => {
          this.triggerData.push({
            name: action.NAME,
            value: action['MPM_Workflow_Actions-id'].Id,
          });
        });
        this.customWorkflowFieldForm.controls.triggerData.setValue(
          this.ruleData.triggerData
        );
      }
    }
    if (this.customWorkflowFieldForm.getRawValue().targetType) {
      if (this.customWorkflowFieldForm.getRawValue().targetType === 'TASK') {
        this.taskData?.forEach((task) => {
          let isSelectedTask = false;
          let isParentTaskCompleted = true;
          let isCycleTask = false;
          if (this.currentRuleList && this.currentRuleList.length > 0) {
            const currentTaskRuleList = this.currentRuleList.filter(
              (rule) =>
                rule.currentTask ===
                this.customWorkflowFieldForm.controls.currentTask.value
            );
            currentTaskRuleList.forEach((rule) => {
              if (
                rule.targetData === task.ID &&
                rule.triggerData ===
                this.customWorkflowFieldForm.controls.triggerData.value
              ) {
                isSelectedTask = true;
              }
            });
            if (
              this.customWorkflowFieldForm.controls.triggerType.value ===
              'STATUS' &&
              this.customWorkflowFieldForm.controls.triggerData.value
            ) {
              const targetTaskRuleList = this.currentRuleList.filter(
                (rule) =>
                  rule.targetData ===
                  this.customWorkflowFieldForm.controls.currentTask.value
              );
              targetTaskRuleList.forEach((rule) => {
                if (
                  rule.currentTask === task.ID &&
                  rule.triggerType === 'STATUS' &&
                  rule.triggerData ===
                  this.customWorkflowFieldForm.controls.triggerData.value
                ) {
                  isCycleTask = true;
                }
              });
            }
          }
          const status = this.taskStatuses.find(
            (taskStatus) =>
              taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED
          );

          if (
            task?.IS_APPROVAL_TASK === 'true' &&
            !(
              this.customWorkflowFieldForm.controls.currentTask.value ===
              String(task.TASK_PARENT_ID) &&
              ((this.customWorkflowFieldForm.controls.triggerType.value ===
                'STATUS' &&
                this.customWorkflowFieldForm.controls.triggerData.value ===
                status['MPM_Status-id'].Id) ||
                this.customWorkflowFieldForm.controls.triggerType.value ===
                'ACTION')
            )
          ) {
            const parentTaskRuleList = this.allRuleList.filter(
              (rule) => rule.currentTask === String(task.TASK_PARENT_ID)
            );
            if (parentTaskRuleList && parentTaskRuleList.length > 0) {
              parentTaskRuleList.forEach((parentTaskRule) => {
                if (
                  status.length != 0 &&
                  !(
                    (parentTaskRule &&
                      parentTaskRule.triggerType === 'STATUS' &&
                      parentTaskRule.triggerData ===
                      status['MPM_Status-id'].Id) ||
                    parentTaskRule.triggerType === 'ACTION'
                  )
                ) {
                  isParentTaskCompleted = false;
                }
              });
            } else {
              isParentTaskCompleted = false;
            }
          }
          if (
            (!isSelectedTask && !isCycleTask && isParentTaskCompleted) ||
            (this.ruleData.isExist && this.isRulesInitiated)
          ) {
            if (
              ((task.TASK_STATUS_TYPE === StatusTypes.INITIAL && task.ID !== this.customWorkflowFieldForm.controls.currentTask.value) ||
                (this.ruleData.isExist && this.isRulesInitiated)

              )) {
              this.targetData.push({
                name: task.TASK_NAME,
                value: task.ID,
                sequence: task.sequence,
                taskStatusValue: task.TASK_STATUS_VALUE
              });
            }
          }
        });
        this.customWorkflowFieldForm.controls.targetData.setValue(
          this.ruleData.targetData
        );
      } else {
        const taskEvents = this.sharingService.getTaskEvents();
        taskEvents.forEach((event) => {
          this.targetData.push({
            name: event.DISPLAY_NAME,
            value: event['MPM_Events-id'].Id,
          });
        });
        this.customWorkflowFieldForm.controls.targetData.setValue(
          this.ruleData.targetData
        );
      }
    }
    this.customWorkflowFieldForm.controls.currentTask.valueChanges.subscribe(
      (currentTask) => {
        const currentSelectedTask = this.taskData?.find(
          (task) => task.ID === currentTask
        );
        if (
          currentTask &&
          this.customWorkflowFieldForm.controls.targetType &&
          this.customWorkflowFieldForm.controls.targetType.value === 'TASK'
        ) {
          this.targetData = [];
          // this.customWorkflowFieldForm.controls.targetData.setValue('');
          this.taskData?.forEach((task) => {
            let isSelectedTask = false;
            let isParentTaskCompleted = true;
            let isCycleTask = false;
            if (this.currentRuleList && this.currentRuleList.length > 0) {
              const currentTaskRuleList = this.currentRuleList.filter(
                (rule) => rule.currentTask === currentTask
              );
              currentTaskRuleList.forEach((rule) => {
                if (
                  rule.targetData === task.ID &&
                  rule.triggerData ===
                  this.customWorkflowFieldForm.controls.triggerData.value
                ) {
                  isSelectedTask = true;
                }
              });
              if (
                this.customWorkflowFieldForm.controls.triggerType.value ===
                'STATUS' &&
                this.customWorkflowFieldForm.controls.triggerData.value
              ) {
                const targetTaskRuleList = this.currentRuleList.filter(
                  (rule) => rule.targetData === currentTask
                );
                targetTaskRuleList.forEach((rule) => {
                  if (
                    rule.currentTask === task.ID &&
                    rule.triggerType === 'STATUS' &&
                    rule.triggerData ===
                    this.customWorkflowFieldForm.controls.triggerData.value
                  ) {
                    isCycleTask = true;
                  }
                });
              }
            }
            const status = this.taskStatuses.find(
              (taskStatus) =>
                taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED
            );
            if (
              status.length != 0 &&
              task?.IS_APPROVAL_TASK === 'true' &&
              !(
                currentTask === String(task.TASK_PARENT_ID) &&
                ((this.customWorkflowFieldForm.controls.triggerType.value ===
                  'STATUS' &&
                  this.customWorkflowFieldForm.controls.triggerData.value ===
                  status['MPM_Status-id'].Id) ||
                  this.customWorkflowFieldForm.controls.triggerType.value ===
                  'ACTION')
              )
            ) {
              if (this.currentRuleList && this.currentRuleList.length > 0) {
                const parentTaskRuleList = this.currentRuleList.filter(
                  (rule) => rule.currentTask === String(task.TASK_PARENT_ID)
                );
                if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                  parentTaskRuleList.forEach((parentTaskRule) => {
                    if (
                      !(
                        (parentTaskRule &&
                          parentTaskRule.triggerType === 'STATUS' &&
                          parentTaskRule.triggerData ===
                          status['MPM_Status-id'].Id) ||
                        parentTaskRule.triggerType === 'ACTION'
                      )
                    ) {
                      isParentTaskCompleted = false;
                    }
                  });
                } else {
                  isParentTaskCompleted = false;
                }
              } else {
                isParentTaskCompleted = false;
              }
            }
            if (!isSelectedTask && !isCycleTask && isParentTaskCompleted) {
              if (
                task.TASK_STATUS_TYPE === StatusTypes.INITIAL &&
                task.ID !== currentTask

              ) {
                //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                this.targetData.push({
                  name: task.TASK_NAME,
                  value: task.ID,

                });
              }
            }
          });
        }
        if (currentTask && this.customWorkflowFieldForm.controls.triggerType) {
          if (
            this.customWorkflowFieldForm.getRawValue().triggerType === 'STATUS'
          ) {
            this.triggerData = [];
            this.isRuleSaved = false;
            this.taskStatuses.forEach((status) => {
              if (
                status.STATUS_TYPE !== StatusTypes.INITIAL &&
                status.STATUS_TYPE !== StatusTypes.FINAL_CANCELLED &&
                !(
                  status.STATUS_TYPE === StatusTypes.FINAL_REJECTED &&
                  currentSelectedTask?.IS_APPROVAL_TASK === 'false'
                )
              ) {
                this.triggerData.push({
                  name: status.NAME,
                  value: status['MPM_Status-id'].Id,
                });
              }
            });
          }
        }
        let isTargetTask = false;
        if (this.currentRuleList && this.currentRuleList.length > 0) {
          this.currentRuleList.forEach((rule) => {
            if (rule.targetData === currentTask) {
              const status = this.taskStatuses.find(
                (taskStatus) =>
                  taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED
              );
              if (
                status.length != 0 &&
                !(
                  rule.triggerType === 'STATUS' &&
                  status['MPM_Status-id'].Id === rule.triggerData
                )
              ) {
                isTargetTask = true;
              }
            }
          });
        }
        if (isTargetTask || currentSelectedTask?.IS_APPROVAL_TASK === 'true') {
          this.customWorkflowFieldForm.controls.isInitialRule.disable();
        } else {
          this.customWorkflowFieldForm.controls.isInitialRule.enable();
        }
      }
    );
    this.customWorkflowFieldForm.controls.triggerType.valueChanges.subscribe(
      (triggerType) => {
        if (triggerType) {
          this.triggerData = [];
          this.customWorkflowFieldForm.controls.triggerData.setValue('');
          this.customWorkflowFieldForm.controls.triggerData.enable();
          if (triggerType === 'STATUS') {
            const currentTask = this.taskData?.find(
              (task) =>
                task.ID ===
                this.customWorkflowFieldForm.controls.currentTask.value
            );
            this.taskStatuses.forEach((status) => {
              if (
                status.STATUS_TYPE !== StatusTypes.INITIAL &&
                status.STATUS_TYPE !== StatusTypes.FINAL_CANCELLED &&
                !(
                  status.STATUS_TYPE === StatusTypes.FINAL_REJECTED &&
                  currentTask?.IS_APPROVAL_TASK === 'false'
                )
              ) {
                this.triggerData.push({
                  name: status.NAME,
                  value: status['MPM_Status-id'].Id,
                });
              }
            });
          } else if (triggerType === 'ACTION') {
            const taskWorkflowActions =
              this.sharingService.getTaskWorkflowActions();
            taskWorkflowActions.forEach((action) => {
              this.triggerData.push({
                name: action.NAME,
                value: action['MPM_Workflow_Actions-id'].Id,
              });
            });
          }
          if (
            this.customWorkflowFieldForm.controls.targetType.value === 'TASK'
          ) {
            this.targetData = [];
            this.customWorkflowFieldForm.controls.targetData.setValue('');
            this.taskData?.forEach((task) => {
              let isParentTaskCompleted = true;
              const status = this.taskStatuses.find(
                (taskStatus) =>
                  taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED
              );
              if (
                status.length != 0 &&
                task?.IS_APPROVAL_TASK === 'true' &&
                !(
                  this.customWorkflowFieldForm.controls.currentTask.value ===
                  String(task.TASK_PARENT_ID) &&
                  ((triggerType === 'STATUS' &&
                    this.customWorkflowFieldForm.controls.triggerData.value ===
                    status['MPM_Status-id'].Id) ||
                    triggerType === 'ACTION')
                )
              ) {
                if (this.currentRuleList && this.currentRuleList.length > 0) {
                  const parentTaskRuleList = this.currentRuleList.filter(
                    (rule) => rule.currentTask === String(task.TASK_PARENT_ID)
                  );
                  if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                    parentTaskRuleList.forEach((parentTaskRule) => {
                      if (
                        !(
                          parentTaskRule &&
                          parentTaskRule.triggerType === 'STATUS' &&
                          parentTaskRule.triggerData ===
                          status['MPM_Status-id'].Id
                        ) ||
                        parentTaskRule.triggerType === 'ACTION'
                      ) {
                        isParentTaskCompleted = false;
                      }
                    });
                  } else {
                    isParentTaskCompleted = false;
                  }
                } else {
                  isParentTaskCompleted = false;
                }
              }
              if (isParentTaskCompleted) {
                if (
                  task.TASK_STATUS_TYPE === StatusTypes.INITIAL &&
                  task.ID !==
                  this.customWorkflowFieldForm.controls.currentTask.value
                ) {
                  //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                  this.targetData.push({
                    name: task.TASK_NAME,
                    value: task.ID,
                  });
                }
              }
            });
          }
          //this.customWorkflowFieldForm.controls.triggerData.enable();
        }
      }
    );
    this.customWorkflowFieldForm.controls.triggerData.valueChanges.subscribe(
      (triggerData) => {
        if (
          triggerData &&
          this.customWorkflowFieldForm.controls.targetType.value === 'TASK'
        ) {
          this.targetData = [];
          // this.customWorkflowFieldForm.controls.targetData.setValue('');
          this.isRuleSaved = false;
          this.taskData?.forEach((task) => {
            let isSelectedTask = false;
            let isCycleTask = false;
            let isParentTaskCompleted = true;
            if (this.currentRuleList && this.currentRuleList.length > 0) {
              const currentTaskRuleList = this.currentRuleList.filter(
                (rule) =>
                  rule.currentTask ===
                  this.customWorkflowFieldForm.controls.currentTask.value
              );
              currentTaskRuleList.forEach((rule) => {
                if (
                  rule.targetData === task.ID &&
                  rule.triggerData === triggerData
                ) {
                  isSelectedTask = true;
                }
              });
              if (
                this.customWorkflowFieldForm.controls.triggerType.value ===
                'STATUS'
              ) {
                const targetTaskRuleList = this.currentRuleList.filter(
                  (rule) =>
                    rule.targetData ===
                    this.customWorkflowFieldForm.controls.currentTask.value
                );
                targetTaskRuleList.forEach((rule) => {
                  if (
                    rule.currentTask === task.ID &&
                    rule.triggerType === 'STATUS' &&
                    rule.triggerData === triggerData
                  ) {
                    isCycleTask = true;
                  }
                });
              }
            }
            const status =
              this.taskStatuses.find(
                (taskStatus) =>
                  taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED
              ) == undefined
                ? []
                : this.taskStatuses.find(
                  (taskStatus) =>
                    taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED
                );
            if (
              status.length != 0 &&
              task?.IS_APPROVAL_TASK === 'true' &&
              !(
                this.customWorkflowFieldForm.controls.currentTask.value ===
                String(task.TASK_PARENT_ID) &&
                ((this.customWorkflowFieldForm.controls.triggerType.value ===
                  'STATUS' &&
                  triggerData === status['MPM_Status-id'].Id) ||
                  this.customWorkflowFieldForm.controls.triggerType.value ===
                  'ACTION')
              )
            ) {
              if (this.currentRuleList && this.currentRuleList.length > 0) {
                const parentTaskRuleList = this.currentRuleList.filter(
                  (rule) => rule.currentTask === String(task.TASK_PARENT_ID)
                );
                if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                  parentTaskRuleList.forEach((parentTaskRule) => {
                    if (
                      !(
                        (parentTaskRule &&
                          parentTaskRule.triggerType === 'STATUS' &&
                          parentTaskRule.triggerData ===
                          status['MPM_Status-id'].Id) ||
                        parentTaskRule.triggerType === 'ACTION'
                      )
                    ) {
                      isParentTaskCompleted = false;
                    }
                  });
                } else {
                  isParentTaskCompleted = false;
                }
              } else {
                isParentTaskCompleted = false;
              }
            }
            if (!isSelectedTask && !isCycleTask && isParentTaskCompleted) {
              if (
                task.TASK_STATUS_TYPE === StatusTypes.INITIAL &&
                task.ID !==
                this.customWorkflowFieldForm.controls.currentTask.value
              ) {
                //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                this.targetData.push({
                  name: task.TASK_NAME,
                  value: task.ID,
                });
              }
            }
          });
        }
      }
    );
    this.customWorkflowFieldForm.controls.targetType.valueChanges.subscribe(
      (targetType) => {
        if (targetType) {
          this.targetData = [];
          this.customWorkflowFieldForm.controls.targetData.setValue('');
          if (targetType === 'EVENT') {
            const taskEvents = this.sharingService.getTaskEvents();
            taskEvents.forEach((event) => {
              this.targetData.push({
                name: event.DISPLAY_NAME,
                value: event['MPM_Events-id'].Id,
              });
            });
          } else if (targetType === 'TASK') {
            this.taskData?.forEach((task) => {
              let isSelectedTask = false;
              let isParentTaskCompleted = true;
              let isCycleTask = false;
              if (this.currentRuleList && this.currentRuleList.length > 0) {
                const currentTaskRuleList = this.currentRuleList.filter(
                  (rule) =>
                    rule.currentTask ===
                    this.customWorkflowFieldForm.controls.currentTask.value
                );
                currentTaskRuleList.forEach((rule) => {
                  if (
                    rule.targetData === task.ID &&
                    rule.triggerData ===
                    this.customWorkflowFieldForm.controls.triggerData.value
                  ) {
                    isSelectedTask = true;
                  }
                });
                if (
                  this.customWorkflowFieldForm.controls.triggerType.value ===
                  'STATUS' &&
                  this.customWorkflowFieldForm.controls.triggerData.value
                ) {
                  const targetTaskRuleList = this.currentRuleList.filter(
                    (rule) =>
                      rule.targetData ===
                      this.customWorkflowFieldForm.controls.currentTask.value
                  );
                  targetTaskRuleList.forEach((rule) => {
                    if (
                      rule.currentTask === task.ID &&
                      rule.triggerType === 'STATUS' &&
                      rule.triggerData ===
                      this.customWorkflowFieldForm.controls.triggerData.value
                    ) {
                      isCycleTask = true;
                    }
                  });
                }
              }
              const status = this.taskStatuses.find(
                (taskStatus) =>
                  taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED
              );
              if (
                task?.IS_APPROVAL_TASK === 'true' &&
                !(
                  this.customWorkflowFieldForm.controls.currentTask.value ===
                  String(task.TASK_PARENT_ID) &&
                  ((this.customWorkflowFieldForm.controls.triggerType.value ===
                    'STATUS' &&
                    this.customWorkflowFieldForm.controls.triggerData.value ===
                    status['MPM_Status-id'].Id) ||
                    this.customWorkflowFieldForm.controls.triggerType.value ===
                    'ACTION')
                )
              ) {
                if (this.currentRuleList && this.currentRuleList.length > 0) {
                  const parentTaskRuleList = this.currentRuleList.filter(
                    (rule) => rule.currentTask === String(task.TASK_PARENT_ID)
                  );
                  if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                    parentTaskRuleList.forEach((parentTaskRule) => {
                      if (
                        !(
                          (parentTaskRule &&
                            parentTaskRule.triggerType === 'STATUS' &&
                            parentTaskRule.triggerData ===
                            status['MPM_Status-id'].Id) ||
                          parentTaskRule.triggerType === 'ACTION'
                        )
                      ) {
                        isParentTaskCompleted = false;
                      }
                    });
                  } else {
                    isParentTaskCompleted = false;
                  }
                } else {
                  isParentTaskCompleted = false;
                }
              }
              if (!isSelectedTask && !isCycleTask && isParentTaskCompleted) {
                if (
                  task.TASK_STATUS_TYPE === StatusTypes.INITIAL &&
                  task.ID !==
                  this.customWorkflowFieldForm.controls.currentTask.value
                ) {
                  //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                  //To Set Sequence Obj and TaskStatus Value
                  this.targetData.push({
                    name: task.TASK_NAME,
                    value: task.ID,
                    equence: task.sequence,
                    taskStatusValue: task.TASK_STATUS_VALUE
                  });
                }
              }
            });
          }
          this.customWorkflowFieldForm.controls.targetData.enable();
        }
      }
    );
    this.customWorkflowFieldForm.controls.targetData.valueChanges.subscribe(response => {
      this.isRuleSaved = false;
    });
    this.customWorkflowFieldForm.valueChanges.subscribe((response) => {
      if (!this.isRuleChanged) {
        this.isRuleChangedValue.emit(true);

      }
      this.isRuleChanged = true;

    });
  }



  confirmRemoval(callback) {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      width: '40%',
      disableClose: true,
      data: {
        message: 'Are you sure you want to Remove?',
        submitButton: 'Yes',
        cancelButton: 'No'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (callback) { callback(result ? result.isTrue : null) };
    });
  }
  addWorkflowRuleNew(successCallback?, errorCallback?) {

    // Checking if the rule has workflowRule Id
    // console.log(this.ruleListGroup)
    if (JSON.stringify(this.ruleData) !== JSON.stringify(this.customWorkflowFieldForm.getRawValue()) && this.ruleData.isExist
      && this.ruleData.currentTask.length != this.customWorkflowFieldForm.getRawValue().currentTask.length) {
      this.updateWorkflowRule();
    } else {
      this.httpRequestArray = [];
      const customWorkflowFieldFormValue = this.customWorkflowFieldForm.getRawValue();
      let selectedTask = customWorkflowFieldFormValue.currentTask;
      let selectedTargetData = customWorkflowFieldFormValue.targetData;

      let acyclicisMatching = false;
      for (const element of this.ruleListGroup) {
        let currentTask = element.currentTask;
        let currentTargetData = element.targetData;


        if ((currentTask.includes(selectedTargetData) && selectedTask.includes(currentTargetData))) {
          acyclicisMatching = true;
          break
        }


      }

      if (acyclicisMatching) {
        this.notificationService.error('Acyclic Method Cant be Allowed');
      } else if (selectedTask.includes(selectedTargetData)) {
        this.notificationService.error('Same Task Dependency Not Allowed');


      }
      else {
        for (let i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
          const workflowRule = this.customWorkflowFieldForm.getRawValue();
          workflowRule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
          this.httpRequestArray.push(this.taskService.createWorkflowRule(workflowRule, this.projectId, this.ruleData.workflowRuleId[i]));
        }

      }

    }



    forkJoin(this.httpRequestArray).subscribe(response => {
      if (response) {
        this.ruleData.currentTask = [this.customWorkflowFieldForm.getRawValue().currentTask];
        this.ruleData.triggerType = this.customWorkflowFieldForm.getRawValue().triggerType;
        this.ruleData.triggerData = this.customWorkflowFieldForm.getRawValue().triggerData;
        this.ruleData.targetType = this.customWorkflowFieldForm.getRawValue().targetType;
        this.ruleData.targetData = this.customWorkflowFieldForm.getRawValue().targetData;
        this.ruleData.isInitialRule = this.customWorkflowFieldForm.getRawValue().isInitialRule;
        this.ruleData.isRuleAdded = true;
        this.ruleData.workflowRuleId = [];
        for (let i = 0; i < response.length; i++) {
          const workflowRuleId = response[i]['Workflow_Rules-id'] && response[i]['Workflow_Rules-id'].Id ? response[i]['Workflow_Rules-id'].Id : null;
          this.ruleData.workflowRuleId.push(workflowRuleId);
        }
        this.ruleGroupHandler.next();
        this.isRuleSaved = true;
        this.enableInitiateWorkflowHandler.next();
        this.notificationService.info('Workflow Rule has been saved');
        if (successCallback) {
          successCallback();
        }
      } else {
        this.notificationService.error('Something went wrong while creating workflow rule');
        if (errorCallback) {
          errorCallback();
        }
      }
    });

  }

  validateRemoveWorkflowRuleNew() {
    this.checkRuleDependencies1();
    if (this.noPredecessorRuleList.length <= 0) {
      if (this.customWorkflowFieldForm) {
        this.removeWorkflowRuleNew();
      }
    } else {
      this.notificationService.error("Please Delete Highlighted Child Rules First");
    }
  }


  removeWorkflowRuleNew() {
    const isInitialRule1 = this.ruleData.isInitialRule ? 'true' : 'false';
    if (this.ruleData.workflowRuleId) {
      this.ruleData.workflowRuleId = Array.isArray(this.ruleData.workflowRuleId) ? this.ruleData.workflowRuleId : [this.ruleData.workflowRuleId];
      //let httpRequestArray = []
      let ruleIds = []
      this.ruleData.workflowRuleId.forEach(workflowId => {
        const Rule = {
          Id: workflowId
        }
        ruleIds.push(Rule);
        //httpRequestArray.push(this.taskService.removeWorkflowRule(workflowId));
      });
      let currentTask = [];
      this.ruleData.currentTask.forEach(eachTask => {
        const sourceTask = {
          Id: eachTask
        }
        currentTask.push(sourceTask);
      })
      let rData = {
        currentTask,
        isInitialRule: isInitialRule1
      }
      this.confirmRemoval(result => {
        if (result) {
          this.taskService.removeWorkflowRule(ruleIds, rData).subscribe(response => {
            if (response) {
              this.removeWorkflowRuleHandler.next(this.ruleData);
              this.enableInitiateWorkflowHandler.next();
              this.notificationService.info('Workflow Rule has been removed');
              this.isRuleChangedValue.emit(false);

            } else {
              this.notificationService.error('Something went wrong while removing workflow rule');
            }
          })
        }
      })
    }
    else {
      this.confirmRemoval(result => {
        if (result) {
          this.removeWorkflowRuleHandler.next(null);
          this.notificationService.info('Workflow Rule has been removed');
          this.isRuleChangedValue.emit(false);


        }

      })


    }

  }











  ngOnInit(): void {
    const sourceTasksSet = new Set();
    this.ruleListGroup.forEach(rule => {
      rule.hasPredecessor = true;
    })
    this.taskData.forEach((task) => {
      this.ruleData.currentTask.forEach(eachTask => {
        if (task.ID === eachTask) {
          sourceTasksSet.add(task.TASK_NAME);
        }
      })
    })
    sourceTasksSet.forEach(task => this.currentSourceTasks.push(task));
    this.ruleData.currentTask = this.ruleData.currentTask.length > 0 && Array.isArray(this.ruleData.currentTask) ? this.ruleData.currentTask : [this.ruleData.currentTask];
    this.ruleList.forEach((rule) => {
      rule.currentTask = rule.currentTask.length > 0 && Array.isArray(rule.currentTask) ? rule.currentTask : [rule.currentTask];
    })

    this.initializeForm();

  }

  updateWorkflowRule() {
    if (this.ruleData.currentTask.length > this.customWorkflowFieldForm.getRawValue().currentTask.length) {
      //Removing a rule from the source tasks list
      const newTasks = [];
      this.ruleData.currentTask.forEach(task => {
        const index = this.customWorkflowFieldForm.getRawValue().currentTask.indexOf(task);
        if (index < 0) {
          newTasks.push(task);
        }
      })
      let deletedRules = []
      newTasks.forEach(task => {
        const rule = {
          currentTask: task,
          isInitialRule: this.ruleData.isInitialRule,
          targetData: this.ruleData.targetData,
          targetType: this.ruleData.targetType,
          triggerData: this.ruleData.triggerData,
          triggerType: this.ruleData.triggerType
        }
        deletedRules.push(rule);
      })
      let originalRules = [];
      for (let i = 0; i < this.ruleData.currentTask.length; i++) {
        const rule = {
          currentTask: this.ruleData.currentTask[i],
          isInitialRule: this.ruleData.isInitialRule,
          targetData: this.ruleData.targetData,
          targetType: this.ruleData.targetType,
          triggerData: this.ruleData.triggerData,
          triggerType: this.ruleData.triggerType,
          workflowRuleId: this.ruleData.workflowRuleId[i]
        }
        originalRules.push(rule);
      }
      let deletedRuleIds = [];
      deletedRules.forEach(rule => {
        originalRules.forEach(eachRule => {
          if (rule.currentTask == eachRule.currentTask) {
            deletedRuleIds.push(eachRule.workflowRuleId);
          }
        })
      })
      const otherParamsChanged = ((this.customWorkflowFieldForm.getRawValue().triggerType !== this.ruleData.triggerType)
        || (this.customWorkflowFieldForm.getRawValue().triggerData !== this.ruleData.triggerData)
        || (this.customWorkflowFieldForm.getRawValue().targetType !== this.ruleData.targetType)
        || (this.customWorkflowFieldForm.getRawValue().targetData !== this.ruleData.targetData)
        || (this.customWorkflowFieldForm.getRawValue().isInitialRule !== this.ruleData.isInitialRule));
      if (otherParamsChanged) {
        for (let i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
          const rule = this.customWorkflowFieldForm.getRawValue();
          rule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
          this.httpRequestArray.push(this.taskService.createWorkflowRule(rule, this.projectId, this.ruleData.workflowRuleId[i]));
        }
      }
      let approvalTasks = this.taskData.filter(task => task.IS_APPROVAL_TASK === 'true');
      let removedParentTasks = [];
      approvalTasks.forEach(approvalTask => {
        const index = newTasks.indexOf(String(approvalTask.TASK_PARENT_ID));
        if (index >= 0) {
          removedParentTasks.push(newTasks[index]);
        }
      })
      //checking if the removed task is a parent task hence checking dependencies...
      /************************************************************************************************* */
      let approvalTasksIdList = [];
      removedParentTasks.forEach(eachTask => {
        const approvalTaskIds = this.taskData.filter(task => String(task.TASK_PARENT_ID) == eachTask);
        approvalTasksIdList.push(...approvalTaskIds);
      });
      let allTasksinRules = [];
      this.ruleListGroup.forEach(rule => {
        allTasksinRules.push(...rule.currentTask);
        allTasksinRules.push(rule.targetData);
      });

      let affectedApprovalTasks = [];
      approvalTasksIdList.forEach(eachTask => {
        const index = allTasksinRules.indexOf(eachTask.ID);
        if (index >= 0) {
          affectedApprovalTasks.push(eachTask);
        }
      });

      this.ruleListGroup.forEach(rule => rule.hasPredecessor = true);
      this.ruleListGroup.forEach(rule => {
        let ruleTasks = [];
        ruleTasks.push(...rule.currentTask);
        ruleTasks.push(rule.targetData);
        affectedApprovalTasks.forEach(eachTask => {
          const index = ruleTasks.indexOf(eachTask.ID);
          if (index >= 0) {
            rule.hasPredecessor = false;
          }
        })
      })
      /**************************************************************************************************************** */
      if (!(affectedApprovalTasks && affectedApprovalTasks.length > 0)) {
        let ruleIds = []
        deletedRuleIds.forEach(workflowId => {
          const Rule = {
            Id: workflowId
          }
          ruleIds.push(Rule);
        })
        const customWorkflowFieldFormValue = this.customWorkflowFieldForm.getRawValue();
        let selectedTask = customWorkflowFieldFormValue.currentTask;
        let selectedTargetData = customWorkflowFieldFormValue.targetData;

        let acyclicisMatching = false;
        for (const element of this.ruleListGroup) {
          let currentTask = element.currentTask;
          let currentTargetData = element.targetData;
          if ((currentTask.includes(selectedTargetData) && selectedTask.includes(currentTargetData))) {
            acyclicisMatching = true;
            break
          }
        }
        if (acyclicisMatching) {
          this.notificationService.error('Acyclic Method Cant be Allowed');
        } else if (selectedTask.includes(selectedTargetData)) {
          this.notificationService.error('Same Task Dependency Not Allowed');
        } else {
          this.taskService.removeWorkflowRule(ruleIds, {}).subscribe(response => {
            if (response) {
              // console.log(this.customWorkflowFieldForm.controls.currentTask.value);
              // this.removeWorkflowRuleHandler.next(this.ruleData);
              this.customWorkflowFieldForm.controls.currentTask.value.forEach(task => this.currentSourceTasks.push(task));
              this.ruleData.currentTask = this.customWorkflowFieldForm.controls.currentTask.value;
              this.isRuleSaved = true;
              this.enableInitiateWorkflowHandler.next();
              this.notificationService.info('Workflow Rule has been removed');
              this.isRuleChangedValue.emit(false);
            } else {
              this.notificationService.error('Something went wrong while removing workflow rule');
            }
          });
        }
      } else {
        this.notificationService.error('Edit Operation could not be Performed because It is a Parent Task and it Will affect the highlighted rules');
      }

    }
    else if (this.ruleData.currentTask.length < this.customWorkflowFieldForm.getRawValue().currentTask.length) {
      // Adding a rule to the source tasks list
      const newTasks = [];
      let existingTasks = this.customWorkflowFieldForm.getRawValue().currentTask;
      existingTasks.forEach(task => {
        const index = this.ruleData.currentTask.indexOf(task);
        if (index < 0) {
          newTasks.push(task);
        }
      });
      this.httpRequestArray = [];
      if (
        this.customWorkflowFieldForm.getRawValue().triggerType !== this.ruleData.triggerType ||
        this.customWorkflowFieldForm.getRawValue().triggerData !== this.ruleData.triggerData ||
        this.customWorkflowFieldForm.getRawValue().targetType !== this.ruleData.targetType ||
        this.customWorkflowFieldForm.getRawValue().targetData !== this.ruleData.targetData ||
        this.customWorkflowFieldForm.getRawValue().isInitialRule !== this.ruleData.isInitialRule
      ) {
        for (let i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
          const rule = this.customWorkflowFieldForm.getRawValue();
          rule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
          this.httpRequestArray.push(
            this.taskService.createWorkflowRule(rule, this.projectId, this.ruleData.workflowRuleId[i])
          );
        }
      } else {
        const customWorkflowFieldFormValue = this.customWorkflowFieldForm.getRawValue();
        let selectedTask = customWorkflowFieldFormValue.currentTask;
        let selectedTargetData = customWorkflowFieldFormValue.targetData;

        let acyclicisMatching = false;
        for (const element of this.ruleListGroup) {
          let currentTask = element.currentTask;
          let currentTargetData = element.targetData;
          if ((currentTask.includes(selectedTargetData) && selectedTask.includes(currentTargetData))) {
            acyclicisMatching = true;
            break
          }
        }
        if (acyclicisMatching) {
          this.notificationService.error('Acyclic Method Not Allowed');
        } else if (selectedTask.includes(selectedTargetData)) {
          this.notificationService.error('Same Task Dependency Not Allowed');
        } else {
          newTasks.forEach(task => {
            const rule = this.customWorkflowFieldForm.getRawValue();
            rule.currentTask = task;
            this.httpRequestArray.push(this.taskService.createWorkflowRule(rule, this.projectId, ''));
          })
        }
      }
    }

  }


  //Below compareObjects method is Used to Compare Two objects instead of JSON.STRINGFY to Improve
  public compareObjects(obj1: any, obj2: any): boolean {
    if (Object.keys(obj1).length !== Object.keys(obj2).length) {
      return false;
    }

    for (const key in obj1) {
      if (obj1.hasOwnProperty(key)) {
        if (obj1[key] !== obj2[key]) {
          return false;
        }
      }
    }
    return true;
  }
  public taskListUpdated = false;
  public onTaskOpen() {
    if (!this.taskListUpdated && this.ruleData.isExist)
      this.getTaskDetailsByRequestBusinessId(this.ruleData.requestId, true).subscribe(res => {
        this.taskListUpdated = true;
      });


  }
}
