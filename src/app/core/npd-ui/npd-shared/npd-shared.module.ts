import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NpdCustomWorkflowModalComponent } from './npd-custom-workflow-modal/npd-custom-workflow-modal.component';
import { MaterialModule } from 'src/app/material.module';
import { MpmLibModule } from '../../mpm-lib/mpm-lib.module';
import { NpdCustomWorkflowFieldComponent } from './npd-custom-workflow-field/npd-custom-workflow-field.component';
import { PaginationModule } from 'mpm-library';
import { WorkflowRuleService } from './services/workflow-rule.service';
import { NpdAdvancedSearchComponent } from './npd-advanced-search/npd-advanced-search/npd-advanced-search.component';
import { NpdAdvancedSearchFieldComponent } from './npd-advanced-search/npd-advanced-search-field/npd-advanced-search-field.component';
import { RequestViewModule } from '../request-view/request-view.module';

@NgModule({
  declarations: [
    NpdCustomWorkflowModalComponent,
    NpdCustomWorkflowFieldComponent,
    NpdAdvancedSearchComponent,
    NpdAdvancedSearchFieldComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MpmLibModule,
    RequestViewModule,
    PaginationModule,
  ],
  exports: [NpdCustomWorkflowModalComponent],
  providers: [WorkflowRuleService],
  /*   entryComponents: [
    NpdCustomWorkflowModalComponent
  ] */
})
export class NpdSharedModule {}
