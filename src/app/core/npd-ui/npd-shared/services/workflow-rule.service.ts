import { Injectable } from '@angular/core';
import { AppService, IndexerService, SearchRequest, SearchResponse, UtilService } from 'mpm-library';
import { Observable } from 'rxjs';
import { EntityListConstant } from '../../request-view/constants/EntityListConstant';
import { EntityService } from '../../services/entity.service';

@Injectable({
  providedIn: 'root'
})
export class WorkflowRuleService {

  constructor(private entityService: EntityService, private utilService: UtilService, private indexerService: IndexerService, private appService: AppService) { }

  HANDLE_WORKFLOW_RULES_INITIATION_METHOD_NS = 'http://schemas.monster.npd.com/jobrequest/bpm/1.0';
  HANDLE_WORKFLOW_RULES_INITIATION_METHOD_WS = 'HandleCustomWorkflowRules';


  getWorkflowRulesByProjectAndInitialRule(projectId, isInitialRule, skip, top): Observable<any> {
    const filters = [];
    filters.push({
      'name': EntityListConstant.TASK_WORKFLOW_RULES_PARAMS.WORKFLOW_PROJECT_ID,
      'operator': 'eq',
      'value': projectId
    });
    filters.push({
      'name': EntityListConstant.TASK_WORKFLOW_RULES_PARAMS.WORKFLOW_IS_INITIAL_RULE,
      'operator': 'eq',
      'value': isInitialRule
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.TASK_WORKFLOW_RULES, filters, '', this.utilService.APP_ID);
    return new Observable(observer => {
      this.entityService.getEntityObjectsWithCount(EntityListConstant.TASK_WORKFLOW_RULES, filterParam, null, null, skip, top, this.utilService.APP_ID)
        .subscribe((workFlowResponse) => {
          if (workFlowResponse.result?.count > 0) {
            observer.next(true);
            observer.complete();
          } else {
            observer.next(false);
            observer.complete();
          }
        }, (error) => {
          observer.error();
        });
    });
  }

  getWorkflowRulesByProject(projectId, skip, top): Observable<any> {
    const filters = [];
    filters.push({
      'name': EntityListConstant.TASK_WORKFLOW_RULES_PARAMS.WORKFLOW_PROJECT_ID,
      'operator': 'eq',
      'value': projectId
    });
    const filterParam = this.entityService.perpareFilterParametersone(EntityListConstant.TASK_WORKFLOW_RULES, filters, '', this.utilService.APP_ID);
    return new Observable(observer => {
      this.entityService.getEntityObjectsWithCount(EntityListConstant.TASK_WORKFLOW_RULES, filterParam, null, null, 0, 999, this.utilService.APP_ID)
        .subscribe((workFlowResponse) => {
          let taskObject = {
            workFlowRules: workFlowResponse.result,
            taskData: []
          }
          if (workFlowResponse.result?.items?.length > 0) {
            let taskIdList = [];
            workFlowResponse.result.items.forEach(workFlowRule => {
              /* if(rule && rule.TRIGGER_TYPE === 'STATUS' && rule.TARGET_TYPE === 'TASK') {
              } */
              taskIdList.push(workFlowRule?.R_PO_TASK$Identity?.Id);
            });
            this.getTaskDetailsByProjectId(taskIdList).subscribe(taskResponse => {
              taskObject.taskData = taskResponse;
              observer.next(taskObject);
              observer.complete();
            }, (error) => {
              observer.error();
            });
          }
        }, (error) => {
          observer.error();
        });
    });
  }

  // Forming search field for mpm-indexer input
  getSearchField(type, field_id, operator_id, operator_name, value?, field_operator?) {

    let searchField = {
      "type": type,
      "field_id": field_id,
      "relational_operator_id": operator_id,
      "relational_operator_name": operator_name,
      "value": value,
      "relational_operator": field_operator
    }
    return searchField;
  }

  getUniqueArrayValues(arrayList) {
    let uniqueValueList = arrayList.filter((c, index) => {
      return arrayList.indexOf(c) === index;
    });
    return uniqueValueList;
  }

  getTaskDetailsByProjectId(taskIdList): Observable<any> {
    let searchConditionList = [
      this.getSearchField("string", "CONTENT_TYPE", "MPM.OPERATOR.CHAR.IS", "is", "MPM_TASK"),
    ];
    taskIdList = this.getUniqueArrayValues(taskIdList);
    taskIdList.forEach((task, index) => {
      if (index == 0) {
        searchConditionList.push(this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", task, "AND"))
      } else {
        searchConditionList.push(this.getSearchField("string", "ID", "MPM.OPERATOR.CHAR.IS", "is", task, "OR"))
      }
    });
    let operation = 'OR';
    (taskIdList?.length) > 0 ? (operation = 'OR') : (operation = 'AND');
    const parameters: SearchRequest = {
      search_config_id: null,
      keyword: "",
      search_condition_list: {
        search_condition: searchConditionList
      },
      facet_condition_list: {
        facet_condition: []
      },
      sorting_list: {
        sort: [
          {
            field_id: "TASK_START_DATE",//ID
            order: "ASC"
          }
        ]
      },
      cursor: {
        page_index: 0,
        page_size: 200
      }
    };
    return new Observable(observer => {
      this.indexerService.search(parameters).subscribe((response: SearchResponse) => {
        if (response.data.length > 0) {
        }
        observer.next(response.data);
        observer.complete();
      }, () => {
        observer.error();
      });
    });
  }

  triggerNPDWorkflow(projectId) {
    let projectUpdateObj = {
      ProjectId: projectId
    }
    return new Observable(observer => {
      this.appService.invokeRequest(this.HANDLE_WORKFLOW_RULES_INITIATION_METHOD_NS, this.HANDLE_WORKFLOW_RULES_INITIATION_METHOD_WS, projectUpdateObj).subscribe(response => {
        observer.next(response);
        observer.complete();
      }, error => {
        observer.error(error);
      });
    });
  }


}
