import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import {
  NotificationService,
  TaskService,
  LoaderService,
  UtilService,
  ConfirmationModalComponent,
  StatusTypes,
  ApplicationConfigConstants,
  SharingService,
} from 'mpm-library';
import { Observable } from 'rxjs';
import { NpdResourceManagementService } from '../../npd-resource-management/services/npd-resource-management.service';
import { WorkFlowConstants } from '../constants/workflow-customconfig';
import { WorkflowRuleService } from '../services/workflow-rule.service';

@Component({
  selector: 'app-npd-custom-workflow-modal',
  templateUrl: './npd-custom-workflow-modal.component.html',
  styleUrls: ['./npd-custom-workflow-modal.component.scss'],
})
export class NpdCustomWorkflowModalComponent implements OnInit {
  ruleList = [];
  taskData = [];
  sourceTaskData = [];
  projectId;
  enableInitiate;
  isRulesInitiated;
  existingRuleList = [];
  existingRuleListGroup = [];
  isTemplate;
  isCurrentTaskRule = false;
  projectRuleList = [];
  ruleListGroup = new Array();
  noPredecessorRuleList = [];
  otherProjectWorkFlows = [];
  counter = 0;
  projectBussinessId = null;



  paginator = {
    page: 1,
    skip: 0,
    top: 0,
    pageSize: 0,
    pageNumber: 0,
  };
  totalListDataCount;
  appConfig: any;

  constructor(
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private taskService: TaskService,
    private loaderService: LoaderService,
    private utilService: UtilService,
    private dialogRef: MatDialogRef<NpdCustomWorkflowModalComponent>,
    private workflowRuleService: WorkflowRuleService,
    private sharingService: SharingService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  close() {
    if (!this.isTemplate) {
      if (
        JSON.stringify(this.existingRuleListGroup) !==
        JSON.stringify(this.ruleListGroup)
      ) {
        const ruleIds = [];
        this.ruleList.forEach((rule) => {
          if (
            rule.isExist === false &&
            rule.isInitialRule === true &&
            rule.workflowRuleId &&
            this.isRulesInitiated
          ) {
            ruleIds.push({
              Id: rule.workflowRuleId,
            });
          }
        });
        let message;
        if (ruleIds && ruleIds.length > 0) {
          message =
            'Provided initial rules will be removed, click yes to proceed';
        } else {
          message = 'Are you sure you want to close?';
        }
        const dialogRef = this.dialog.open(ConfirmationModalComponent, {
          width: '40%',
          disableClose: true,
          data: {
            message: message,
            submitButton: 'Yes',
            cancelButton: 'No',
          },
        });
        dialogRef.afterClosed().subscribe((result) => {
          if (result && result.isTrue) {
            const isStatusRules = this.ruleList.find(
              (rule) => rule.isExist === false && rule.triggerType === 'STATUS'
            )
              ? true
              : false;
            if (isStatusRules) {
              this.taskService
                .UpdateDateOnRuleInitiate(this.projectId)
                .subscribe();
            }
            if (ruleIds) {
              this.taskService.deleteWorkflowRules(ruleIds).subscribe();
            }
            let triggerWorkflowAction = false;
            const actionRules = this.ruleList.filter(
              (rule) => rule.triggerType === 'ACTION'
            );
            actionRules.forEach((actionRule) => {
              const existingRule = this.existingRuleList.find(
                (existingActionRule) =>
                  existingActionRule.workflowRuleId ===
                  actionRule.workflowRuleId
              );
              if (existingRule) {
                if (existingRule.triggerData !== actionRule.triggerData) {
                  triggerWorkflowAction = true;
                }
              } else {
                triggerWorkflowAction = true;
              }
            });
            this.dialogRef.close(triggerWorkflowAction);
          }
        });
      } else {
        this.dialogRef.close(false);
      }
    } else {
      this.dialogRef.close(false);
    }
  }

  addRule() {

    let canAddRule = true;

    this.ruleListGroup.forEach((rule) => {
      if (rule.isExist === false) {
        if (!rule.isRuleAdded) {
          canAddRule = false;
        }
      }
    });

    if (canAddRule) {
      if (this.data.currentTaskData) {
        this.ruleListGroup.push({
          currentTask: [this.data.currentTaskData.ID],
          triggerType: '',
          triggerData: '',
          targetType: '',
          targetData: '',
          workflowRuleId: '',
          isExist: false,
          isInitialRule: false,
          isTaskView: true,
          requestId: '',
          hasPredecessor: true
        });
      } else {
        this.ruleListGroup.push({
          currentTask: [],
          triggerType: 'STATUS',
          triggerData: '16402',
          targetType: 'TASK',
          targetData: '',
          workflowRuleId: '',
          isExist: false,
          isInitialRule: false,
          isTaskView: false,
          requestId: this.projectBussinessId,
          hasPredecessor: true
        });
      }
    } else {
      this.notificationService.error('Kindly add the created rule first');
    }


  }

  removeRule(ruleData) {
    if (ruleData) {
      for (let i = 0; i < this.ruleListGroup.length; i++) {
        if (
          JSON.stringify(this.ruleListGroup[i].currentTask.sort()) ==
          JSON.stringify(ruleData.currentTask.sort()) &&
          JSON.stringify(this.ruleListGroup[i].workflowRuleId.sort()) ==
          JSON.stringify(ruleData.workflowRuleId.sort()) &&
          this.ruleListGroup[i].triggerData == ruleData.triggerData &&
          this.ruleListGroup[i].triggerData == ruleData.triggerData &&
          this.ruleListGroup[i].targetData == ruleData.targetData &&
          this.ruleListGroup[i].targetType == ruleData.targetType &&
          this.ruleListGroup[i].isInitialRule == ruleData.isInitialRule
        ) {
          this.ruleListGroup.splice(i, 1);
          break;
        }
      }
    } else {
      this.ruleListGroup.pop();
    }
    if (this.ruleList && this.ruleList.length === 0) {
      this.enableInitiate = false;
    }
  }

  checkRuleDependencies(ruleListGroup) {
    this.ruleListGroup = [];
    this.ruleListGroup = ruleListGroup;
  }

  getTaskWorkflowRules(taskId) {
    this.taskService.getWorkflowRulesByTask(taskId).subscribe((response) => {
      this.loaderService.hide();
      if (response) {
        let workflowRules;
        if (response && response.length > 0) {
          workflowRules = response;
        } else {
          workflowRules = [response];

        }
        workflowRules.forEach((rule) => {
          //const currentTask = this.taskData.find(task => task.ID === rule.R_PO_TASK['Task-id'].Id);
          const currentTask = this.sourceTaskData.find(
            (task) => task.ID === rule.R_PO_TASK['Task-id'].Id
          );

          this.ruleList.push({
            currentTask: rule.R_PO_TASK['Task-id'].Id,
            triggerType: rule.TRIGGER_TYPE,
            triggerData:
              rule.TRIGGER_TYPE === 'STATUS'
                ? rule.R_PO_STATUS['MPM_Status-id'].Id
                : rule.R_PO_ACTIONS['MPM_Workflow_Actions-id'].Id,
            targetType: rule.TARGET_TYPE,
            targetData:
              rule.TARGET_TYPE === 'TASK'
                ? rule.R_PO_TARGET_TASK['Task-id'].Id
                : rule.R_PO_EVENT['MPM_Events-id'].Id,
            isExist: true,
            isTaskView: true,
            isInitialRule: this.utilService.getBooleanValue(
              rule.IS_INITIAL_RULE
            ),
            workflowRuleId: rule['Workflow_Rules-id'].Id,
            isFinalTask:
              currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED ||
              currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED ||
              currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
            isActiveTask: this.utilService.getBooleanValue(
              currentTask.IS_ACTIVE_TASK
            ),
            requestId:
              currentTask?.[
              WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID
              ],
          });
          this.existingRuleList.push({
            currentTask: rule.R_PO_TASK['Task-id'].Id,
            triggerType: rule.TRIGGER_TYPE,
            triggerData:
              rule.TRIGGER_TYPE === 'STATUS'
                ? rule.R_PO_STATUS['MPM_Status-id'].Id
                : rule.R_PO_ACTIONS['MPM_Workflow_Actions-id'].Id,
            targetType: rule.TARGET_TYPE,
            targetData:
              rule.TARGET_TYPE === 'TASK'
                ? rule.R_PO_TARGET_TASK['Task-id'].Id
                : rule.R_PO_EVENT['MPM_Events-id'].Id,
            isExist: true,
            isTaskView: true,
            isInitialRule: this.utilService.getBooleanValue(
              rule.IS_INITIAL_RULE
            ),
            workflowRuleId: rule['Workflow_Rules-id'].Id,
            isFinalTask:
              currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED ||
              currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED ||
              currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
            isActiveTask: this.utilService.getBooleanValue(
              currentTask.IS_ACTIVE_TASK
            ),
            requestId:
              currentTask?.[
              WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID
              ],
          });
        });
        this.enableInitiate = this.ruleList.find(
          (rule) => rule.isInitialRule === true
        )
          ? this.enableInitiate
          : false;
      } else {
        this.enableInitiate = false;
      }
    });
  }

  groupRulesOnSave() {
    this.ruleList = [];
    this.ngOnInit();
  }
  isRuleChanged = false
  enableInitiateWorkflow(event) {
    if (event) {
      this.otherProjectWorkFlows.push(event);
    }
  }
  ruleChangeCounter = 0;
  ruleChanged = false;
  public onIsRuleChangedValueChange(event) {
    if (event) {
      this.ruleChangeCounter++;
    } else {
      this.ruleChangeCounter ? this.ruleChangeCounter-- : null;
    }
    // Do something with the emitted value of this.isRuleChanged from the child component.
    //this.triggerWorkflow()
    this.ruleChanged = true;
  }

  triggerWorkflow(value?: any) {

    if (this.ruleChangeCounter > 0) {
      this.notificationService.error('Kindly Save Rule Which is Altered');
    }
    else if (!(this.ruleChanged)) {
      this.notificationService.error('Save Changes before Clicking on Initiate Button');

    }
    else {
      const newrules = this.ruleListGroup.find(
        (rule) => rule.workflowRuleId === ''
      );

      const unsavedRules = this.ruleList.find(
        (rule) => rule.workflowRuleId === ''
      );
      if (unsavedRules) {
        this.notificationService.warn(
          'Unsaved rules will not be part of workflow'
        );
      }

      let isStatusRules;
      if (this.isRulesInitiated) {
        isStatusRules = this.ruleList.find(
          (rule) => rule.isExist === false && rule.triggerType === 'STATUS'
        )
          ? true
          : false;
      } else {
        isStatusRules = this.ruleList.find(
          (rule) => rule.triggerType === 'STATUS'
        )
          ? true
          : false;
      }
      this.loaderService.show();
      this.workflowRuleService.triggerNPDWorkflow(this.projectId).subscribe(
        (response) => {
          this.loaderService.hide();
          if (response) {
            let triggerWorkflowAction = false;
            const actionRules = this.ruleList.filter(
              (rule) => rule.triggerType === 'ACTION'
            );
            actionRules.forEach((actionRule) => {
              const existingRule = this.existingRuleList.find(
                (existingActionRule) =>
                  existingActionRule.workflowRuleId === actionRule.workflowRuleId
              );
              if (existingRule) {
                if (existingRule.triggerData !== actionRule.triggerData) {
                  triggerWorkflowAction = true;
                }
              } else {
                triggerWorkflowAction = true;
              }
            });
            //this.dialogRef.close(triggerWorkflowAction);
            if (newrules == null || newrules.workflowRuleId != '') {
              if (this.otherProjectWorkFlows.length > 0) {
                this.triggerMulipleWorkFlows(
                  () => {
                    this.loaderService.hide();
                    this.dialogRef.close(true);
                    this.notificationService.info(
                      'Workflow rules initiated successfully, dates will be aligned as per rule configured'
                    );
                  },
                  () => {
                    this.loaderService.hide();
                    this.dialogRef.close(true);
                    this.notificationService.info(
                      'Something went wrong while initiating workflow rules.'
                    );
                  }
                );
              } else {
                this.dialogRef.close(true);
                this.notificationService.info(
                  'Workflow rules initiated successfully, dates will be aligned as per rule configured'
                );
              }

            }


            else {
              this.notificationService.error(
                'Please Save or Remove Workflow rule which has been Created Recently'
              );
            }


          }
        },
        (error) => {
          this.loaderService.hide();
          this.dialogRef.close(true);
          this.notificationService.info(
            'Something went wrong while initiating workflow rules.'
          );
        }
      );
    }
  }

  triggerMulipleWorkFlows(successCallback?, errorCallback?) {
    this.loaderService.show();
    let allProjectWorkFlows = this.workflowRuleService.getUniqueArrayValues(
      this.otherProjectWorkFlows
    );
    allProjectWorkFlows.forEach((projectId, index) => {
      //this.taskService.triggerWorkflow(projectId, true).subscribe(response => {
      this.workflowRuleService.triggerNPDWorkflow(projectId).subscribe(
        (response) => {
          if (index == allProjectWorkFlows.length - 1) {
            if (successCallback) {
              successCallback();
            }
          }
        },
        (error) => {
          if (errorCallback) {
            errorCallback();
          }
        }
      );
    });
  }

  ngOnInit(): void {

    this.taskData = this.data.taskData;
    this.isTemplate = this.data.isTemplate;
    this.projectBussinessId = this.taskData[0]?.NPD_PROJECT_BUSINESS_ID;

    //this.sourceTaskData = this.data.sourceTaskData;
    this.sourceTaskData = [];
    const activeTasks = this.taskData.find(
      (task) => task.IS_ACTIVE_TASK === 'true'
    );
    const completedTasks = this.taskData.find(
      (task) =>
        task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED ||
        task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED
    );
    if (activeTasks || completedTasks) {
      this.isRulesInitiated = true;
      this.enableInitiate = false;
    } else {
      this.isRulesInitiated = false;
      this.enableInitiate = true;
    }
    const projectItemId =
      this.taskData && this.taskData[0] && this.taskData[0].PROJECT_ITEM_ID
        ? this.taskData[0].PROJECT_ITEM_ID
        : null;
    this.projectId = projectItemId ? projectItemId.split('.')[1] : null;
    this.appConfig = this.sharingService.getAppConfig();
    this.paginator.pageSize = this.utilService.convertStringToNumber(
      this.appConfig[
      ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION
      ]
    );
    this.paginator.top = this.paginator.pageSize;
    if (this.data && this.data.currentTaskData) {
      this.isCurrentTaskRule = true;
      const taskId = this.data.currentTaskData.ID;
      if (taskId) {
        //this.loaderService.show();
        //this.refreshByTaskId();
      }
    } else {

      if (this.projectId) {
        this.hasInitialRule();
        this.refreshByProjectId();
      } else {
        this.enableInitiate = false;
      }
    }


  }


  hasInitialRule() {


    this.workflowRuleService
      .getWorkflowRulesByProjectAndInitialRule(this.projectId, 'true', 0, 5)
      .subscribe((response) => {
        this.enableInitiate = response ? true : false;
      });
  }




  refreshByTaskId() {

    this.loaderService.show();
    const isApprovalTask = this.utilService.getBooleanValue(
      this.data.currentTaskData.IS_APPROVAL_TASK
    );
    const taskId = this.data.currentTaskData.ID;
    this.workflowRuleService
      .getWorkflowRulesByProject(
        this.projectId,
        this.paginator.skip,
        this.paginator.top
      )
      .subscribe((response) => {
        let hasRuleToActivate = !isApprovalTask;
        if (response) {
          this.loaderService.hide();
          let taskResponse = response;
          this.totalListDataCount = taskResponse?.workFlowRules?.totalCount;
          this.sourceTaskData = taskResponse?.taskData;
          this.ruleList = [];
          this.existingRuleList = [];
          if (taskResponse?.workFlowRules) {
            let workflowRules;
            if (taskResponse?.workFlowRules?.items?.length > 0) {
              workflowRules = taskResponse.workFlowRules.items;
            } else {
              workflowRules = [taskResponse?.workFlowRules?.items];
            }
            if (isApprovalTask) {
              hasRuleToActivate = workflowRules.find(
                (workflowRule) =>
                  workflowRule.Properties?.TARGET_TYPE === 'TASK' &&
                  workflowRule.R_PO_TARGET_TASK$Identity?.Id === taskId
              )
                ? true
                : false;
            }
            workflowRules.forEach((rule) => {
              const currentTask = this.taskData.find(
                (task) => task.ID === rule.R_PO_TASK['Task-id'].Id
              );
              this.projectRuleList.push({
                currentTask: rule.R_PO_TASK$Identity?.Id,
                triggerType: rule.Properties?.TRIGGER_TYPE,
                triggerData:
                  rule.Properties?.TRIGGER_TYPE === 'STATUS'
                    ? rule.R_PO_STATUS$Identity?.Id
                    : rule.R_PO_ACTIONS$Identity?.Id,
                targetType: rule.Properties?.TARGET_TYPE,
                targetData:
                  rule.Properties?.TARGET_TYPE === 'TASK'
                    ? rule.R_PO_TARGET_TASK$Identity?.Id
                    : rule.R_PO_EVENT$Identity?.Id,
                isExist: true,
                isTaskView: false,
                isInitialRule: this.utilService.getBooleanValue(
                  rule.Properties.IS_INITIAL_RULE
                ),
                workflowRuleId: rule.Identity?.Id,
                isFinalTask:
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED ||
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED ||
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
                isActiveTask: this.utilService.getBooleanValue(
                  currentTask.IS_ACTIVE_TASK
                ),
                requestId:
                  currentTask?.[
                  WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID
                  ],
              });
            });
          }
          if (hasRuleToActivate) {
            this.getTaskWorkflowRules(taskId);
          } else {
            this.loaderService.hide();
            this.notificationService.error(
              'As there is no rule to activate approval task, cannot configure rule for this task'
            );
            this.dialogRef.close(false);
          }
        }
      });
  }

  refreshByProjectId() {


    this.loaderService.show();

    this.workflowRuleService
      .getWorkflowRulesByProject(
        this.projectId,
        null,
        null
      )
      .subscribe(
        (response) => {
          this.loaderService.hide();
          let taskResponse = response;
          this.totalListDataCount = taskResponse?.workFlowRules?.totalCount;
          this.sourceTaskData = taskResponse?.taskData;
          this.ruleList = [];
          this.existingRuleList = [];
          if (taskResponse?.workFlowRules) {
            let workflowRules;
            if (taskResponse?.workFlowRules?.items?.length > 0) {
              workflowRules = taskResponse.workFlowRules.items;
            } else {
              workflowRules = [taskResponse?.workFlowRules?.items];
            }

            workflowRules.forEach((rule) => {
              const currentTask = this.sourceTaskData.find(
                (task) => task.ID === rule.R_PO_TASK$Identity?.Id
              );
              let obj = {
                taskName: rule.R_PO_TASK$Properties.NAME,
                currentTask: rule.R_PO_TASK$Identity?.Id,
                triggerType: rule.Properties?.TRIGGER_TYPE,
                triggerData:
                  rule.Properties?.TRIGGER_TYPE === 'STATUS'
                    ? rule.R_PO_STATUS$Identity?.Id
                    : rule.R_PO_ACTIONS$Identity?.Id,
                targetType: rule.Properties?.TARGET_TYPE,
                targetData:
                  rule.Properties?.TARGET_TYPE === 'TASK'
                    ? rule.R_PO_TARGET_TASK$Identity?.Id
                    : rule.R_PO_EVENT$Identity?.Id,
                isExist: true,
                isTaskView: false,
                isInitialRule: this.utilService.getBooleanValue(
                  rule.Properties.IS_INITIAL_RULE
                ),
                workflowRuleId: rule.Identity?.Id,
                isFinalTask:
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED ||
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED ||
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
                isActiveTask: this.utilService.getBooleanValue(
                  currentTask.IS_ACTIVE_TASK
                ),
                requestId:
                  currentTask?.[
                  WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID
                  ],
              }

              this.ruleList.push({
                currentTask: rule.R_PO_TASK$Identity?.Id,
                triggerType: rule.Properties?.TRIGGER_TYPE,
                triggerData:
                  rule.Properties?.TRIGGER_TYPE === 'STATUS'
                    ? rule.R_PO_STATUS$Identity?.Id
                    : rule.R_PO_ACTIONS$Identity?.Id,
                targetType: rule.Properties?.TARGET_TYPE,
                targetData:
                  rule.Properties?.TARGET_TYPE === 'TASK'
                    ? rule.R_PO_TARGET_TASK$Identity?.Id
                    : rule.R_PO_EVENT$Identity?.Id,
                isExist: true,
                isTaskView: false,
                isInitialRule: this.utilService.getBooleanValue(
                  rule.Properties.IS_INITIAL_RULE
                ),
                workflowRuleId: rule.Identity?.Id,
                isFinalTask:
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED ||
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED ||
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
                isActiveTask: this.utilService.getBooleanValue(
                  currentTask.IS_ACTIVE_TASK
                ),
                requestId:
                  currentTask?.[
                  WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID
                  ],



              });
              this.existingRuleList.push({
                currentTask: rule.R_PO_TASK$Identity?.Id,
                triggerType: rule.Properties?.TRIGGER_TYPE,
                triggerData:
                  rule.Properties?.TRIGGER_TYPE === 'STATUS'
                    ? rule.R_PO_STATUS$Identity?.Id
                    : rule.R_PO_ACTIONS$Identity?.Id,
                targetType: rule.Properties?.TARGET_TYPE,
                targetData:
                  rule.Properties?.TARGET_TYPE === 'TASK'
                    ? rule.R_PO_TARGET_TASK$Identity?.Id
                    : rule.R_PO_EVENT$Identity?.Id,
                isExist: true,
                isTaskView: false,
                isInitialRule: this.utilService.getBooleanValue(
                  rule.Properties.IS_INITIAL_RULE
                ),
                workflowRuleId: rule.Identity?.Id,
                isFinalTask:
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED ||
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED ||
                  currentTask.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED,
                isActiveTask: this.utilService.getBooleanValue(
                  currentTask.IS_ACTIVE_TASK
                ),
                requestId:
                  currentTask?.[
                  WorkFlowConstants.ProjectLevelIndexerConfigs.REQUEST_ID
                  ],


              });
            });


            this.projectRuleList = this.ruleList;

            this.ruleList.forEach((rule) => {
              let taskobj = this.data.sequence.find(x => x.taskId == rule.targetData);
              rule.currentTask = Array.isArray(rule.currentTask)
                ? rule.currentTask
                : [rule.currentTask];
              rule.workflowRuleId = Array.isArray(rule.workflowRuleId)
                ? rule.workflowRuleId
                : [rule.workflowRuleId];
              rule.sequence = taskobj?.sequence,
                rule.taskStatusValue = taskobj?.taskStatusValue

            });

            //GROUPING THE RULES...
            this.ruleListGroup = [];
            //To set Sorting for RuleListGroup according To TargetTask
            this.ruleListGroup = [...this.ruleList].sort((a, b) => { return a.sequence - b.sequence });
            this.ruleChangeCounter = 0;

            for (let i = 0; i < this.ruleListGroup.length; i++) {
              for (let j = i + 1; j < this.ruleListGroup.length; j++) {
                if (
                  this.ruleListGroup[i].targetData ===
                  this.ruleListGroup[j].targetData &&
                  this.ruleListGroup[i].isInitialRule ==
                  this.ruleListGroup[j].isInitialRule &&
                  this.ruleListGroup[i].targetType ==
                  this.ruleListGroup[j].targetType &&
                  this.ruleListGroup[i].triggerType ==
                  this.ruleListGroup[j].triggerType &&
                  this.ruleListGroup[i].triggerData ==
                  this.ruleListGroup[j].triggerData &&
                  this.ruleListGroup[i].targetType ==
                  this.ruleListGroup[j].targetType &&
                  this.ruleListGroup[i].requestId === this.ruleListGroup[j].requestId

                ) {
                  this.ruleListGroup[i].currentTask = this.ruleListGroup[
                    i
                  ].currentTask.concat(this.ruleListGroup[j].currentTask);
                  this.ruleListGroup[i].workflowRuleId = this.ruleListGroup[
                    i
                  ].workflowRuleId.concat(this.ruleListGroup[j].workflowRuleId);
                  this.ruleListGroup.splice(j, 1);
                  j -= 1;
                }

              }
            }
            this.ruleListGroup.forEach((eachRule) =>
              this.existingRuleListGroup.push(eachRule)
            );
            this.ruleListGroup.forEach((rule) => {
              rule.hasPredecessor = true;
            });

          }
        },
        (error) => {
          this.loaderService.hide();
          this.notificationService.error(
            'Something Went wrong while fetching Workflow Rules.'
          );
        }
      );




  }

  pagination(pagination) {
    this.paginator.pageSize = pagination.pageSize;
    this.paginator.top = pagination.pageSize;
    this.paginator.skip = pagination.pageIndex * pagination.pageSize;
    this.paginator.page = 1 + pagination.pageIndex;
    if (this.data?.currentTaskData?.ID) {
      //this.refreshByTaskId();
    } else {
      this.refreshByProjectId();
    }
  }
}

/* without custom service-mpm service logic
    this.taskService.triggerWorkflow(this.projectId, isStatusRules).subscribe(response => {
      this.loaderService.hide();
      if (response) {
        let triggerWorkflowAction = false;
        const actionRules = this.ruleList.filter(rule => rule.triggerType === 'ACTION');
        actionRules.forEach(actionRule => {
          const existingRule = this.existingRuleList.find(existingActionRule => existingActionRule.workflowRuleId === actionRule.workflowRuleId);
          if (existingRule) {
            if (existingRule.triggerData !== actionRule.triggerData) {
              triggerWorkflowAction = true;
            }
          } else {
            triggerWorkflowAction = true;
          }
        });
        //this.dialogRef.close(triggerWorkflowAction);
        if(this.otherProjectWorkFlows.length > 0) {
          this.triggerMulipleWorkFlows(()=> {
            this.loaderService.hide();
            this.dialogRef.close(true);
            this.notificationService.info('Workflow rules initiated successfully, dates will be aligned as per rule configured');
          },() => {
            this.loaderService.hide();
            this.dialogRef.close(true);
            this.notificationService.info('Something went wrong while initiating workflow rules.');
          })
        } else {
          this.dialogRef.close(true);
          this.notificationService.info('Workflow rules initiated successfully, dates will be aligned as per rule configured');
        }
      }
    });
*/

