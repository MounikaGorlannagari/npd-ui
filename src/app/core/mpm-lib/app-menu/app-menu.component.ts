import { Component } from '@angular/core';
import { AppMenuComponent } from 'mpm-library';

@Component({
    selector: 'app-menu',
    templateUrl: './app-menu.component.html',
    styleUrls: ['./app-menu.component.scss']
})

export class MPMAppMenuComponent extends AppMenuComponent {
}
