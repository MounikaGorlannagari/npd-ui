export const CalendarConstants = {
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    evenMonth: ['January', 'March', 'May', 'July', 'August', 'October', 'December'],
    oddMonth: ['April', 'June', 'September', 'November'],
    leapMonth: ['February']
};
