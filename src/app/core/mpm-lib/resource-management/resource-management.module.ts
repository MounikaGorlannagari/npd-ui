import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResourceManagementComponent } from './resource-management.component';
import { ResourceManagementRoutingModule } from './resource-management.routing.module';
import { MpmLibModule } from '../../mpm-lib/mpm-lib.module';
import { CustomSelectModule, FacetModule } from 'mpm-library';
import { MaterialModule } from 'src/app/material.module';
import { MPMResourceManagementToolbarComponent } from './resource-management-toolbar/resource-management-toolbar.component';
import { MPMSynchronizeTaskFiltersComponent } from './synchronize-task-filters/synchronize-task-filters.component';
import { MpmLibraryModule } from 'mpm-library';
import { MPMResourceManagementResourceAllocationComponent } from './resource-management-resource-allocation/resource-management-resource-allocation.component';

@NgModule({
    declarations: [
        ResourceManagementComponent,
        MPMResourceManagementToolbarComponent,
        MPMSynchronizeTaskFiltersComponent,
        MPMResourceManagementResourceAllocationComponent
    ],
    imports: [
        CommonModule,
        ResourceManagementRoutingModule,
        MaterialModule,
        FacetModule,
        MpmLibModule,
        CustomSelectModule,
        MpmLibraryModule
    ],
    exports: [
        MPMResourceManagementToolbarComponent
    ]
})
export class ResourceManagementModule { }
