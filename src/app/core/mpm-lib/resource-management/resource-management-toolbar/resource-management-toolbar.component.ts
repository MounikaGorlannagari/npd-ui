import { Component, ViewChild } from '@angular/core';
import { ResourceManagementToolbarComponent } from 'mpm-library';
import { MPMSynchronizeTaskFiltersComponent } from '../synchronize-task-filters/synchronize-task-filters.component';


@Component({
  selector: 'app-resource-management-toolbar',
  templateUrl: './resource-management-toolbar.component.html',
  styleUrls: ['./resource-management-toolbar.component.scss']
})
export class MPMResourceManagementToolbarComponent /* implements OnInit, OnDestroy {  */ extends ResourceManagementToolbarComponent {

  @ViewChild(MPMSynchronizeTaskFiltersComponent) synchronizeTaskFiltersComponent: MPMSynchronizeTaskFiltersComponent;

  // MPM-V3-2217
  updateTaskDeliverable(data, userId) {
    this.taskService.assignDeliverable(data.ID, userId).subscribe(response => {
      if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
        this.reload(true);
        this.notificationService.success('Deliverable updated successfully');
        //  this.addedResourceHandler.next(userId);
        this.synchronizeTaskFiltersComponent.getDeliverable(userId, false, true);
      } else if (response && response.APIResponse && response.APIResponse.statusCode === '500') {
        this.loaderService.hide();
        this.reload(true);
        this.notificationService.error('Something went wrong on while updating deliverable');
      } else {
        this.loaderService.hide();
        this.reload(true);
        this.notificationService.error('Something went wrong on while updating deliverable');
      }
    }, error => {
      this.loaderService.hide();
      this.reload(true);
      this.notificationService.error('Something went wrong on while updating deliverable');
    });
  }

  updateDeliverable(data, userId) {
    let deliverableObject;
    deliverableObject = {
      'BulkOperation': false,
      'RPODeliverableOwnerID': {
        'IdentityID': {
          'userID': userId
        }
      },
      'DeliverableId': {
        'Id': data.ID
      }
    }
    const requestObj = {
      DeliverableObject: deliverableObject,
      DeliverableReviewers: {
        DeliverableReviewer: ''
      }
    };
    this.deliverableService.updateDeliverable(requestObj)
      .subscribe(response => {
        if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
          && response.APIResponse.data.Deliverable && response.APIResponse.data.Deliverable['Deliverable-id'] && response.APIResponse.data.Deliverable['Deliverable-id'].Id) {
          this.reload(true);
          this.notificationService.success('Deliverable updated successfully');
          //  this.addedResourceHandler.next(userId);
          this.synchronizeTaskFiltersComponent.getDeliverable(userId, false, true);
        } else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
          && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
          this.loaderService.hide();
          this.reload(true);
          this.notificationService.error(response.APIResponse.error.errorMessage);
        } else {
          this.loaderService.hide();
          this.reload(true);
          this.notificationService.error('Something went wrong on while updating deliverable');
        }
      }, error => {
        this.loaderService.hide();
        this.reload(true);
        this.notificationService.error('Something went wrong on while updating deliverable');
      });
  }

  refresh() {
    this.reload(true);
    this.synchronizeTaskFiltersComponent.getDeliverable('', false, true);
  }

}
