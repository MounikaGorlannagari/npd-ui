import { Component, OnInit, SimpleChanges } from '@angular/core';
import { DeliverableTaskListComponent } from 'mpm-library';

@Component({
  selector: 'app-deliverable-task-list',
  templateUrl: './deliverable-task-list.component.html',
  styleUrls: ['./deliverable-task-list.component.scss']
})
export class MPMDeliverableTaskListComponent extends DeliverableTaskListComponent {

}
