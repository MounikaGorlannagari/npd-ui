import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MPMShareAssetsComponent } from './share-assets.component';
import { MaterialModule } from '../../../material.module';
import { CustomEmailFieldModule } from 'mpm-library';

@NgModule({
    declarations: [
        MPMShareAssetsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        CustomEmailFieldModule
    ],
    exports: [
        MPMShareAssetsComponent
    ]
})

export class MPMShareAssetsModule { }
