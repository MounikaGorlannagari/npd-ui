import { Component } from '@angular/core';

import { ShareAssetsComponent } from 'mpm-library';

@Component({
    selector: 'app-share-assets',
    templateUrl: './share-assets.component.html',
    styleUrls: ['./share-assets.component.scss']
})

export class MPMShareAssetsComponent extends ShareAssetsComponent {

    super() { }
}
