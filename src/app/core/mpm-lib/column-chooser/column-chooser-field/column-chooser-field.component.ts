import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ColumnChooserFieldComponent } from 'mpm-library';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NotificationService } from 'mpm-library';


@Component({
  selector: 'app-column-chooser-field',
  templateUrl: './column-chooser-field.component.html',
  styleUrls: ['./column-chooser-field.component.scss']
})
export class MPMColumnChooserFieldComponent extends ColumnChooserFieldComponent implements OnInit, OnChanges {


  myControl = new FormControl();
  maxFreezeColumnCount: Observable<number[]>;
  enableIndexerFieldRestriction
  selectedFields = []
  maxNum;
  noOfSelectedFields
  abc: any;

  constructor(
    private notificationService: NotificationService,
  ) {

    super()
  }

  ngOnInit() {
    this.setFreezeCountOptions();
  }

  private _filter(value: string): number[] {
    const filterValue = value;

    return this.maxNum.filter(option => option.toString().includes(filterValue));
  }

  setFreezeCountOptions() {
    this.selectedFields = this.columnChooserFields?.filter((request: any) => request.selected);
    this.noOfSelectedFields = this.selectedFields ? this.selectedFields?.length : 0;
    this.maxNum = Array(this.noOfSelectedFields + 1).fill(0).map((x, i) => i);
    this.maxFreezeColumnCount = this.myControl.valueChanges.pipe(startWith(''), map(value => this._filter(value)));
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.freezeColumnCountNumber = this.userPreferenceFreezeCount
    if (changes.columnChooserFields) {
      this.setFreezeCountOptions();
    }
  }


  checkboxFunction(variable) {
    if (variable.selected || variable.checked) {
      this.noOfSelectedFields++
    } else {
      this.noOfSelectedFields--
    }
    this.maxNum = Array(this.noOfSelectedFields + 1).fill(1).map((x, i) => i);
    this.maxFreezeColumnCount = this.myControl.valueChanges.pipe(startWith(''), map(value => this._filter(value)));
  }

  onValueChange(i) {
    this.freezeColumnCountNumber = i
  }

  updateColumnChooser(isPreference) {
    if (isPreference) {
      if (!(this.freezeColumnCountNumber <= this.noOfSelectedFields)) {
        this.notificationService.error('Invalid Freeze column count');
        this.saveColumnChooserHandler.next({ 'column': this.columnChooserFields, 'frezeCount': this.userPreferenceFreezeCount });
        this.freezeColumnCountNumber = this.userPreferenceFreezeCount
      } else {
        this.saveColumnChooserHandler.next({ 'column': this.columnChooserFields, 'frezeCount': this.freezeColumnCountNumber });
      }
    } else {
      if (!(this.freezeColumnCountNumber <= this.noOfSelectedFields)) {
        this.notificationService.error('Invalid Freeze column count');
        this.columnChooserHandler.next({ 'column': this.columnChooserFields, 'frezeCount': this.userPreferenceFreezeCount });
        this.freezeColumnCountNumber = this.userPreferenceFreezeCount
      } else {
        this.columnChooserHandler.next({ 'column': this.columnChooserFields, 'frezeCount': this.freezeColumnCountNumber });
      }
    }
  }

  numericPattern = /^[0-9]*$/;
  numericOnly(event) {
    return this.numericPattern.test(event.key);
  }

  resetting() {
    // this.columnChooserHandler.next({'column':this.columnChooserFields, 'frezeCount':this.userPreferenceFreezeCount});
    this.freezeColumnCountNumber = this.userPreferenceFreezeCount
    // this.columnChooserFields[0].selected=this.abc[0].selected;
  }

}
