import { Component, OnInit ,ViewChild} from '@angular/core';
import { ColumnChooserComponent, ColumnChooserFieldComponent } from 'mpm-library';
import { MPMColumnChooserFieldComponent } from '../column-chooser-field/column-chooser-field.component';

@Component({
  selector: 'app-column-chooser',
  templateUrl: './column-chooser.component.html',
  styleUrls: ['./column-chooser.component.scss']
})
export class MPMColumnChooserComponent extends ColumnChooserComponent{

  @ViewChild('columnChooser') columnChooser : MPMColumnChooserFieldComponent
  
  close(e){
    this.columnChooser.resetting()
    
  }
}
