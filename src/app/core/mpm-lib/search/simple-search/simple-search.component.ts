import { Component, Inject } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from '@angular/material/dialog';
import {
  AdvancedSearchData,
  AppService,
  LoaderService,
  NotificationService,
  SearchConfigService,
  SearchDataService,
  SearchSaveService,
  SessionStorageConstants,
  SharingService,
  SimpleSearchComponent,
  SimpleSearchData,
} from 'mpm-library';
import { forkJoin } from 'rxjs';
import { NpdAdvancedSearchComponent } from 'src/app/core/npd-ui/npd-shared/npd-advanced-search/npd-advanced-search/npd-advanced-search.component';
import { NpdSessionStorageConstants } from 'src/app/core/npd-ui/request-view/constants/sessionStorageConstants';
import { MPMAdvancedSearchComponent } from '../advanced-search/advanced-search.component';
import { IndexerConfigService } from 'mpm-library/lib/shared/services/indexer/mpm-indexer.config.service';
import { NpdLookupDomainService } from 'src/app/core/npd-ui/npd-project-view/services/npd-lookup-domain.service';

@Component({
  selector: 'app-simple-search',
  templateUrl: './simple-search.component.html',
  styleUrls: ['./simple-search.component.scss'],
})
export class MPMSimpleSearchComponent extends SimpleSearchComponent {
  GET_ALL_COMBO_FIELDS_FOR_ADVANCED_SEARCH_FIELD_CONFIG_NS =
    'http://schemas/NPDLookUpDomainModel/Overview_Fields_Config/operations';
  GET_ALL_COMBO_FIELDS_FOR_ADVANCED_SEARCH_FIELD_CONFIG_WS =
    'GetAllLookupDomainField';
  fieldConfigs = [];
  constructor(
    public dialogRef: MatDialogRef<SimpleSearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SimpleSearchData,
    public sharingService: SharingService,
    public dialog: MatDialog,
    public loaderService: LoaderService,
    public notificationService: NotificationService,
    public searchConfigService: SearchConfigService,
    public searchDataService: SearchDataService,
    public searchSaveService: SearchSaveService,
    public appService: AppService,
    public lookUpDomainService:NpdLookupDomainService
  ) {
    super(
      dialogRef,
      data,
      sharingService,
      dialog,
      loaderService,
      notificationService,
      searchConfigService,
      searchDataService,
      searchSaveService
    );
  }
  openAdvancedSearchModal(advData) {
    advData.fieldConfigs = this.fieldConfigs;
    if (
      this.data.searchData &&
      this.data.searchData.advancedSearchConfig &&
      (this.data.searchData.advancedSearchConfig.NAME !== '' ||
        this.data.searchData.advancedSearchConfig[0].NAME !== '')
    ) {
      this.savedSearches.forEach((data) => {
        if (
          data.NAME ===
          (this.data.searchData.advancedSearchConfig.NAME
            ? this.data.searchData.advancedSearchConfig.NAME
            : this.data.searchData.advancedSearchConfig[0].NAME)
        ) {
          advData.savedSearch = data;
        }
      });
    } else if (
      advData.recentSearch &&
      advData.recentSearch[0] &&
      advData.recentSearch[0].NAME !== ''
    ) {
      this.savedSearches.forEach((data) => {
        if (data.NAME === advData.recentSearch[0].NAME) {
          advData.savedSearch = data;
        }
      });
    }
   
    if(advData.savedSearch &&  !(advData.recentSearch &&
      advData.recentSearch[0] &&
      advData.recentSearch[0].NAME !== '') ){
      let savedSearchArray = [];
      let _savedSearch =
        advData.savedSearch.SEARCH_DATA.search_condition_list.search_condition;
      if (_savedSearch && Object.keys(_savedSearch).length > 1) {
      for (var i = 0; i < _savedSearch.length; i++) {
        let array = [];
        let searchValue :any;
         let isCombo = false;
         this.fieldConfigs.forEach((field) => {
           if (_savedSearch[i] && field.MPMMapperName === _savedSearch[i].mapper_name) {
             isCombo = true;
           }
         });
        if (isCombo) {
          searchValue = [];
        }
          for (var k = i + 1; k < _savedSearch.length; k++) {
            if (
              _savedSearch[i] &&
              _savedSearch[k] &&
              _savedSearch[i].mapper_name == _savedSearch[k].mapper_name
            ) {
              if (array.length == 0) {
                array.push(_savedSearch[i]);
                if (isCombo) {
                  searchValue.push({
                    displayName: _savedSearch[i].display_value,
                    value: _savedSearch[i].display_value,
                  });
                  searchValue.push({
                    displayName: _savedSearch[k].display_value,
                    value: _savedSearch[k].display_value,
                  });
                } else {
                  searchValue = _savedSearch[i].display_value + ',' + _savedSearch[k].display_value;
                }
              } else {
                if (isCombo) {
                  searchValue.push({
                    displayName: _savedSearch[k].display_value,
                    value: _savedSearch[k].display_value,
                  });
                } else {
                  searchValue = searchValue + ',' + _savedSearch[k].display_value;
                }
              }
              delete _savedSearch[k];
            }
          }
        if (array.length > 0) {
          array[0].value = searchValue;
          array[0].filterRowData.searchValue = searchValue;

          savedSearchArray.push(array[0]);
        } else {
          if(isCombo){
            let value=_savedSearch[i].value;
            if(typeof value=='string'){
              searchValue.push({
                displayName:_savedSearch[i].value,
                  value:_savedSearch[i].value
              })
            }
            else{
              let length=_savedSearch[i].value.length;
            if(length>0){
              for(let index=0;index<length;index++){
                searchValue.push({
                  displayName:_savedSearch[i].value[index].displayName,
                  value:_savedSearch[i].value[index].displayName
                })
              }
            }
            else{
              searchValue.push({
                displayName: _savedSearch[i].display_value,
                value: _savedSearch[i].display_value,
              });
            }
            }
            
            
            // _savedSearch[i].value = searchValue;
            _savedSearch[i].filterRowData.searchValue = searchValue;
          }
          // else{
          //   let length=_savedSearch[i].value.split(',').length;
          //   if(length>0){

          //   }
          // }
          
        
            savedSearchArray.push(_savedSearch[i]);
          
          
        }
      }
      }
      savedSearchArray = savedSearchArray.filter((recent) => recent);
    
      if(savedSearchArray.length>0){
        advData.savedSearch.SEARCH_DATA.search_condition_list.search_condition =
        savedSearchArray;
      }
      }
      
     
    this.advDialogRef = this.dialog.open(NpdAdvancedSearchComponent, {
      position: { top: '0' },
      width: '1100px',
      // height: '65%',
      disableClose: true,
      data: advData,
    });
    this.advDialogRef.afterClosed().subscribe((result) => {
      if (result === true) {
        //  this.dialogRef.close(result);
      } else {
        this.dialogRef.close(result);
      }
    });
  }
  openAdvancedSearch(savedSearchId, recentSearch) {
    let recentSearchArray = [];
    let _recentSearch = recentSearch;
    if (_recentSearch && Object.keys(_recentSearch).length > 1) {
      for (var i = 0; i < _recentSearch.length; i++) {
        let array = [];
        let searchValue: any;
        let isCombo= false;
        this.fieldConfigs.forEach(field=>{
          if(_recentSearch[i] && field.MPMMapperName === _recentSearch[i].mapper_name){
            isCombo = true
            this.lookUpDomainService.comboFieldSelection(
              'COMBO',
              field
            );
          }
        })
        if (isCombo) {
          searchValue = [];
        }
        for (var k = i + 1; k < _recentSearch.length; k++) {
          if (
            _recentSearch[i] &&
            _recentSearch[k] &&
            _recentSearch[i].mapper_name == _recentSearch[k].mapper_name
          ) {
            if (array.length == 0) {
              array.push(_recentSearch[i]);
              if (isCombo) {
                searchValue.push({
                  displayName: _recentSearch[i].display_value,
                  value: _recentSearch[i].display_value,
                });
                searchValue.push({
                  displayName: _recentSearch[k].display_value,
                  value: _recentSearch[k].display_value,
                });
              } else {
                searchValue =
                  _recentSearch[i].display_value + ',' + _recentSearch[k].display_value;
              }
             
            } else {
               if (isCombo) {
                 searchValue.push({
                   displayName: _recentSearch[k].display_value,
                   value: _recentSearch[k].display_value,
                 });
               } else {
                 searchValue = searchValue + ',' + _recentSearch[k].display_value;
               }
            }
            delete _recentSearch[k];
          }
        }
        if (array.length > 0) {
          array[0].value = searchValue;
          array[0].filterRowData.searchValue = searchValue;

          recentSearchArray.push(array[0]);
        } else {
          recentSearchArray.push(_recentSearch[i]);
        }
      }
    }
    recentSearchArray = recentSearchArray.filter((recent) => recent);
    if (
      this.data &&
      this.data.searchData &&
      this.data.searchData.searchConfig &&
      this.data.searchData.searchConfig.advancedSearchConfiguration
    ) {
      const advData: any = {
        advancedSearchConfigData: this.data.searchData.searchConfig,
        availableSavedSearch: this.savedSearches,
        savedSearch: savedSearchId,
        recentSearch: recentSearchArray.length
          ? recentSearchArray
          : recentSearch,
      };

      if (savedSearchId) {
        if (typeof savedSearchId === 'string') {
          this.searchConfigService
            .getSavedSearcheById(savedSearchId)
            .subscribe((savedSearchDetails) => {
              if (savedSearchDetails) {
                advData.savedSearch = savedSearchDetails;
                this.openAdvancedSearchModal(advData);
              }
            });
        } else {
          this.openAdvancedSearchModal(advData);
        }
      } else {
        this.openAdvancedSearchModal(advData);
      }
    }
  }
  setNPDFieldConfig() {

    this.loaderService.show();
    this.appService
      .invokeRequest(
        this.GET_ALL_COMBO_FIELDS_FOR_ADVANCED_SEARCH_FIELD_CONFIG_NS,
        this.GET_ALL_COMBO_FIELDS_FOR_ADVANCED_SEARCH_FIELD_CONFIG_WS,
        null
      )
      .subscribe((response) => {
        this.loaderService.hide();
        let allViewSpecificAdvancedSearchFieldConfig = [];
        allViewSpecificAdvancedSearchFieldConfig =
          response.Overview_Fields_Config;
        this.fieldConfigs=[]
        if (response.Overview_Fields_Config.length > 1) {
          for (var j = 0;j < allViewSpecificAdvancedSearchFieldConfig.length;j++) {
            let field: any = {};
            let data = allViewSpecificAdvancedSearchFieldConfig[j];
            field.DataType = data?.DataType;
            field.FieldName = data?.FieldName;
            field.FieldType = data?.FieldType;
            field.FieldID = data['Overview_Fields_Config-id'].Id;
            field.ProjectMapperId = data?.ProjectMapperId;
            field.ProjectMapperName = data?.ProjectMapperName;
            field.RequestMapper = data?.RequestMapper;
            field.IndexerMapperId = data?.IndexerMapperId;
            field.IndexerMapperName = data?.IndexerMapperName;
            field.IsRequest = data?.IsRequest;
            field.IsProject = data?.IsProject;
            field.IsCalculationRequired = data?.IsCalculationRequired;
            field.IsDynamic = data?.IsDynamic;
            field.IsUser = data?.IsUser;
            field.MPMMapperName = data?.MPMMapperName;
            field.SessionStorageConstant = data?.SessionStorageConstant;
            this.fieldConfigs.push(field);
          }
        } else {
          let field: any = {};
          let data = response.Overview_Fields_Config;
          field.DataType = data?.DataType;
          field.FieldName = data?.FieldName;
          field.FieldType = data?.FieldType;
          field.FieldID = data['Overview_Fields_Config-id'].Id;
          field.ProjectMapperId = data?.ProjectMapperId;
          field.ProjectMapperName = data?.ProjectMapperName;
          field.ProjectfieldID = data?.ProjectfieldID;
          field.RequestMapper = data?.RequestMapper;
          field.IndexerMapperId = data?.IndexerMapperId;
          field.IndexerMapperName = data?.IndexerMapperName;
          field.IsRequest = data?.IsRequest;
          field.IsProject = data?.IsProject;
          field.IsCalculationRequired = data?.IsCalculationRequired;
          field.IsDynamic = data?.IsDynamic;
          field.IsUser = data?.IsUser;
          field.MPMMapperName = data?.MPMMapperName;
          field.SessionStorageConstant = data?.SessionStorageConstant;
          this.fieldConfigs.push(field);
        }
        sessionStorage.setItem(
          NpdSessionStorageConstants['ALL_ADVANCED_SEARCH_FIELD_CONFIGs'],
          JSON.stringify(this.fieldConfigs)
        );
       
        this.fieldConfigs = JSON.parse(
          sessionStorage.getItem(
            NpdSessionStorageConstants.ALL_ADVANCED_SEARCH_FIELD_CONFIGs
          )
        );
      });
  }

  ngOnInit() {
    if (
      sessionStorage.getItem(
        NpdSessionStorageConstants.ALL_ADVANCED_SEARCH_FIELD_CONFIGs
      ) == null
    ) {
      this.setNPDFieldConfig();
    }
    this.fieldConfigs = JSON.parse(
      sessionStorage.getItem(
        NpdSessionStorageConstants.ALL_ADVANCED_SEARCH_FIELD_CONFIGs
      )
    );
  
    this.searchKeyword = this.data.searchData.searchValue;
    this.getAllSavedSearches().subscribe((response) => {
      if (this.data.searchData && this.data.searchData.advancedSearchConfig) {
        if (
          this.data.searchData.advancedSearchConfig['MPM_Saved_Searches-id'] &&
          this.data.searchData.advancedSearchConfig['MPM_Saved_Searches-id'].Id
        ) {
          this.editSavedSearch(this.data.searchData.advancedSearchConfig);
        } else {
          this.openAdvancedSearch(
            false,
            this.data.searchData.advancedSearchConfig
          );
        }
      }
    });
    this.getAllRecentSearches();
  }
}
