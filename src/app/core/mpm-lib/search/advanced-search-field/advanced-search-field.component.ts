import { Component } from "@angular/core";
import { AdvancedSearchFieldComponent } from "mpm-library";


@Component({
  selector: 'app-advanced-search-field',
  templateUrl: './advanced-search-field.component.html',
  styleUrls: ['./advanced-search-field.component.scss']
})
export class MPMAdvancedSearchFieldComponent extends AdvancedSearchFieldComponent {

}
