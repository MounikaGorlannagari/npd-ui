import { Component } from '@angular/core';
import { SearchIconComponent, MPMAdvancedSearchConfig } from 'mpm-library';
import { MPMSimpleSearchComponent } from '../simple-search/simple-search.component';


@Component({
    selector: 'app-search-icon',
    templateUrl: './search-icon.component.html',
    styleUrls: ['./search-icon.component.scss']
})

export class MPMSearchIconComponent extends SearchIconComponent { 
   /*  openSimpleSearchPopup(advancedSearchConfig: MPMAdvancedSearchConfig) {
        if (advancedSearchConfig && advancedSearchConfig[0]) {
            advancedSearchConfig[0].isEdit = true;
        }
        const dialogRef = this.dialog.open(MPMSimpleSearchComponent, {
            position: {
                top: '0'
            },
            width: '575px',
            data: {
                searchData: {
                    searchValue: this.searchValue,
                    searchConfig: this.searchConfig,
                    advancedSearchConfig: advancedSearchConfig
                }
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result && result.length > 0) {
                if (result[0].isAdvancedSearch) {
                    this.savedSearchName = result[0].NAME ? result[0].NAME : null;
                    this.advancedSearchConfig = result;
                    this.isAdvancedSearch = true;
                } else {
                    this.isAdvancedSearch = false;
                    this.searchValue = result[0].keyword || result[0].value;
                    this.searchName = this.searchValue;
                }
                this.clearSearch = true;
                this.search.emit(result);
            } else if (result && result.SEARCH_DATA && result.SEARCH_DATA.search_condition_list &&
                result.SEARCH_DATA.search_condition_list.search_condition &&
                result.SEARCH_DATA.search_condition_list.search_condition.length) {
                this.savedSearchName = result.NAME;
                if (result.SEARCH_DATA.search_condition_list.search_condition[0].isAdvancedSearch) {
                    this.advancedSearchConfig = result;
                    this.isAdvancedSearch = true;
                } else {
                    this.isAdvancedSearch = false;
                    this.searchValue = result[0].keyword || result[0].value;
                }
                this.clearSearch = true;
                this.search.emit(result.SEARCH_DATA.search_condition_list.search_condition);
            }
        });
    } */


    openSimpleSearchPopup(advancedSearchConfig) {
        if (advancedSearchConfig && advancedSearchConfig[0]) {
            advancedSearchConfig[0].isEdit = true;
        }
        const dialogRef = this.dialog.open(MPMSimpleSearchComponent, {
            position: {
                top: '0'
            },
            width: '575px',
            data: {
                searchData: {
                    searchValue: this.searchValue,
                    searchConfig: this.searchConfig,
                    advancedSearchConfig: advancedSearchConfig
                }
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result && result.length > 0) {
                if (result[0].isAdvancedSearch) {
                    this.savedSearchName = result[0].NAME ? result[0].NAME : null;
                    this.advancedSearchConfig = result;
                    this.isAdvancedSearch = true;
                }
                else {
                    this.isAdvancedSearch = false;
                    this.searchValue = result[0].keyword || result[0].value;
                    this.searchName = this.searchValue;
                }
                this.clearSearch = true;
                this.search.emit(result);
            }
            else if (result && result.SEARCH_DATA && result.SEARCH_DATA.search_condition_list &&
                result.SEARCH_DATA.search_condition_list.search_condition &&
                result.SEARCH_DATA.search_condition_list.search_condition.length) {
                this.savedSearchName = result.NAME;
                if (result.SEARCH_DATA.search_condition_list.search_condition[0].isAdvancedSearch) {
                    this.advancedSearchConfig = result;
                    this.isAdvancedSearch = true;
                }
                else {
                    this.isAdvancedSearch = false;
                    this.searchValue = result[0].keyword || result[0].value;
                }
                this.clearSearch = true;
                this.search.emit(result.SEARCH_DATA.search_condition_list.search_condition);
            }
        });
    }
}
