import { Component } from "@angular/core";
import { AdvancedSearchComponent } from "mpm-library";

export interface Keyword {
  name: string;
}

@Component({
  selector: 'app-advanced-search',
  templateUrl: './advanced-search.component.html',
  styleUrls: ['./advanced-search.component.scss']
})
export class MPMAdvancedSearchComponent extends AdvancedSearchComponent {

}
