import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MPM_LEVELS, ProjectConstant, TaskConstants, TaskCreationComponent } from 'mpm-library';
import { startWith, map, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-task-creation',
  templateUrl: './task-creation.component.html',
  styleUrls: ['./task-creation.component.scss']
})
export class MPMTaskCreationComponent extends TaskCreationComponent {

  initialiseTaskForm() {
    const disableField = false;
    this.taskForm = null;
    this.taskModalConfig.minDate = this.taskModalConfig.selectedProject.START_DATE;
    this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
    const pattern = ProjectConstant.NAME_STRING_PATTERN;
    if (!this.taskModalConfig.isTemplate) {
        if (this.taskModalConfig.isNewTask) {
            this.taskStatus = this.taskModalConfig.taskInitialStatusList;
            this.taskForm = new FormGroup({
                taskName: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                duration: new FormControl({ value: '', disabled: (disableField || !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || !this.enableDuration) }),
                status: new FormControl({
                    value: this.taskModalConfig.taskInitialStatusList && this.taskModalConfig.taskInitialStatusList[0] &&
                        this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'] ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'].Id : '', disabled: true
                }
                ),
                owner: new FormControl({ value: '', disabled: disableField }),
                role: new FormControl({ value: '', disabled: disableField }),
                priority: new FormControl({
                    value: this.taskModalConfig.taskPriorityList && this.taskModalConfig.taskPriorityList[0] &&
                        this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'] ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                }, [Validators.required]),
                startDate: new FormControl({
                    value: this.taskModalConfig.selectedProject.START_DATE, disabled: disableField
                }, [Validators.required]),
                endDate: new FormControl({
                    value: this.taskModalConfig.selectedProject.DUE_DATE, disabled: (disableField || this.enableDuration)
                }, [Validators.required]),
                description: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.required, Validators.maxLength(1000)]),
                revisionReviewRequired: new FormControl({ value: this.taskConfig.isApprovalTask ? this.defaultRevisionRequiredStatus : '', disabled: disableField }),
                isMilestone: new FormControl({ value: false, disabled: disableField }),
                viewOtherComments: new FormControl({ value: '', disabled: disableField }),
                predecessor: new FormControl({ value: '', disabled: disableField }),
                deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                allocation: new FormControl({ value: '', disabled: disableField })
            });
            this.taskForm.updateValueAndValidity();
        } else if (this.taskModalConfig.selectedTask) {
            this.taskStatus = this.projectUtilService.filterStatusOptions(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList, MPM_LEVELS.TASK);
            this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList);
            // if (selectedStatusInfo && (selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.FINAL)) {
            // this.overViewConfig.isReadOnly = true;
            // disableFields = true;
            // this.disableStatusOptions = true;
            // }
            const task = this.taskModalConfig.selectedTask;
            const taskField = this.getTaskFields(task);
            const taskRoleId = task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'] ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
            // const taskOwnerId = task.R_PO_OWNER_ID && task.R_PO_OWNER_ID['Identity-id'] ? task.R_PO_OWNER_ID['Identity-id'].Id : '';
            task.predecessor = (task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object') ? task.PARENT_TASK_ID : null;
            this.taskForm = new FormGroup({
                taskName: new FormControl({ value: task.NAME, disabled: disableField },
                    [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                duration: new FormControl({
                    value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                    disabled: (disableField || !this.enableDuration)
                }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({
                    value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                    disabled: (disableField || !this.enableDuration)
                }),
                status: new FormControl({
                    value: task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id'] ? task.R_PO_STATUS['MPM_Status-id'].Id : '',
                    disabled: this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL
                }),
                owner: new FormControl({
                    value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
                    disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                role: new FormControl({
                    value: this.formRoleValue(taskRoleId), disabled: disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                priority: new FormControl({
                    value: (task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id'] ? task.R_PO_PRIORITY['MPM_Priority-id'].Id : ''),
                    disabled: disableField
                }, [Validators.required]),
                startDate: new FormControl({
                    value: this.commentsUtilService.addUnitsToDate(new Date(task.START_DATE), 0, 'days', this.enableWorkWeek), disabled: (disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL))
                }, [Validators.required]),
                endDate: new FormControl({ value: task.DUE_DATE, disabled: (disableField || this.enableDuration) }, [Validators.required]),
                description: new FormControl({ value: this.utilService.isNullOrEmpty(task.DESCRIPTION) ? '' : task.DESCRIPTION, disabled: disableField }, [Validators.maxLength(1000)]),
                revisionReviewRequired: new FormControl({ value: this.utilService.getBooleanValue(task.REVISION_REVIEW_REQUIRED), disabled: true }),
                isMilestone: new FormControl({ value: this.utilService.getBooleanValue(task.IS_MILESTONE), disabled: disableField }),
                viewOtherComments: new FormControl({ value: taskField.showOtherComments, disabled: disableField }),
                predecessor: new FormControl(this.formPredecessor(task.predecessor)),
                // disabled: taskField.isApprovalTaskSelected || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL
                deliverableApprover: new FormControl({
                    value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
                    disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                deliverableReviewers: new FormControl({ value: this.savedReviewers }),
                allocation: new FormControl({
                    value: task.ASSIGNMENT_TYPE, disabled: disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                })
            });
            this.taskModalConfig.selectedpredecessor = this.getPredecessortask(task.predecessor);
            this.taskForm.controls.predecessor.disable();
            if (this.taskModalConfig.selectedpredecessor) {
                const minDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek);
                this.taskModalConfig.minDate = minDateObject;
            }
            this.getTaskOwnerAssignmentCount();
            // this.onTaskSelection();
            this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
            this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject
                (this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers);
            this.taskForm.updateValueAndValidity();
        }
    } else {
        if (this.taskModalConfig.isNewTask) {
            this.taskStatus = this.taskModalConfig.taskInitialStatusList;
            this.taskForm = new FormGroup({
                taskName: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                // 205
                /*   duration: new FormControl({ value: '', disabled: (disableField || !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                  durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || !this.enableDuration) }),
                  */
                duration: new FormControl({ value: '', disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration) }),
                status: new FormControl(this.taskModalConfig.taskInitialStatusList && this.taskModalConfig.taskInitialStatusList[0] &&
                    this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'] ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'].Id : ''),
                owner: new FormControl({ value: '', disabled: disableField }),
                role: new FormControl({ value: '', disabled: disableField }),
                priority: new FormControl({
                    value: this.taskModalConfig.taskPriorityList && this.taskModalConfig.taskPriorityList[0] &&
                        this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'] ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                }, [Validators.required]),
                description: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(1000)]),
                revisionReviewRequired: new FormControl({ value: this.taskConfig.isApprovalTask ? this.defaultRevisionRequiredStatus : '', disabled: disableField }),
                isMilestone: new FormControl({ value: false, disabled: disableField }),
                viewOtherComments: new FormControl({ value: '', disabled: disableField }),
                predecessor: new FormControl({ value: '', disabled: disableField }),
                deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                allocation: new FormControl({ value: '', disabled: disableField })
            });
        } else if (this.taskModalConfig.selectedTask) {
            this.taskStatus = this.projectUtilService.filterStatusOptions(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList, MPM_LEVELS.TASK);
            this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList);

            const task = this.taskModalConfig.selectedTask;
            const taskField = this.getTaskFields(task);
            const taskRoleId = task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'] ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
            task.predecessor = (task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object') ? task.PARENT_TASK_ID : null;
            this.taskForm = new FormGroup({
                taskName: new FormControl({ value: task.NAME, disabled: disableField },
                    [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                // 205
                /* duration: new FormControl({
                    value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                    disabled: (disableField || !this.enableDuration)
                }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({
                    value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                    disabled: (disableField || !this.enableDuration)
                }), */
                duration: new FormControl({
                    value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                    disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration)
                }, [Validators.required, Validators.max(999), Validators.min(1)]),
                durationType: new FormControl({
                    value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                    disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration)
                }),
                status: new FormControl({
                    value: task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id'] ? task.R_PO_STATUS['MPM_Status-id'].Id : '',
                    disabled: true
                }),
                owner: new FormControl({
                    value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
                    disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                role: new FormControl({
                    value: this.formRoleValue(taskRoleId), disabled: disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                priority: new FormControl({
                    value: (task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id'] ? task.R_PO_PRIORITY['MPM_Priority-id'].Id : ''),
                    disabled: disableField
                }, [Validators.required]),
                description: new FormControl({ value: this.utilService.isNullOrEmpty(task.DESCRIPTION) ? '' : task.DESCRIPTION, disabled: disableField }, [Validators.maxLength(1000)]),
                revisionReviewRequired: new FormControl({ value: this.utilService.getBooleanValue(task.REVISION_REVIEW_REQUIRED), disabled: true }),
                isMilestone: new FormControl({ value: this.utilService.getBooleanValue(task.IS_MILESTONE), disabled: disableField }),
                viewOtherComments: new FormControl({ value: taskField.showOtherComments, disabled: disableField }),
                predecessor: new FormControl(this.formPredecessor(task.predecessor)),
                deliverableApprover: new FormControl({
                    value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
                    disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                }),
                deliverableReviewers: new FormControl({ value: this.savedReviewers }),
                allocation: new FormControl({
                    value: task.ASSIGNMENT_TYPE, disabled: disableField ||
                        (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                })

            });
            this.taskModalConfig.selectedpredecessor = this.getPredecessortask(task.predecessor);
            this.taskForm.controls.predecessor.disable();
            if (this.taskModalConfig.selectedpredecessor) {
                const minDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek);
                this.taskModalConfig.minDate = minDateObject;
            }
            this.getTaskOwnerAssignmentCount();
            // this.onTaskSelection();
            this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
            this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject
                (this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers);
        }
    }

    this.filteredReviewers = this.taskForm.get('deliverableReviewers').valueChanges.pipe(
        startWith(''),
        map((reviewer: string | null) => reviewer ? this.filterReviewers(reviewer) : this.allReviewers.slice()));

    this.taskModalConfig.selectedStatusItemId = this.taskForm.value.status;
    // this.taskForm.controls.status.disable();
    this.taskModalConfig.predecessorConfig.formControl = this.taskForm.controls.predecessor;
    this.taskModalConfig.taskOwnerConfig.formControl = this.taskForm.controls.owner;
    this.taskModalConfig.taskRoleConfig.formControl = this.taskForm.controls.role;
    this.taskModalConfig.deliverableApprover.formControl = this.taskForm.controls.deliverableApprover;
    this.taskModalConfig.deliverableReviewer.formControl = this.taskForm.controls.deliverableReviewer;
    this.taskModalConfig.deliveryPackageConfig.formControl = this.taskForm.controls.predecessor;
    if (this.taskForm.controls.allocation.value === TaskConstants.ALLOCATION_TYPE_ROLE) {
        if (this.taskForm.controls.owner && !this.taskModalConfig.selectedTask) {
            this.taskForm.controls.owner.disable();
        }
        if (this.taskForm.controls.deliverableApprover && !this.taskModalConfig.selectedTask) {
            this.taskForm.controls.deliverableApprover.disable();
        }
        if (this.taskForm.controls.deliverableReviewer && !this.taskModalConfig.selectedTask) {
            this.taskForm.controls.deliverableReviewer.disable();
        }
        /* if (this.taskForm.controls.role.value !== '') {
            this.getUsersByRole().subscribe(() => {
                this.taskModalConfig.taskOwnerConfig.filterOptions = this.teamRoleUserOptions;
                this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
            });
        } */
    }

    this.taskForm.controls.predecessor.valueChanges.subscribe(predecessor => {
        // TODO predecessor change method
        if (!this.taskModalConfig.selectedTask && typeof predecessor === 'object' && predecessor && predecessor.value) {
            this.taskModalConfig.taskList.map(task => {
                if (task['Task-id'].Id === predecessor.value) {
                    /*if (task.REVISION_REVIEW_REQUIRED) {
                        this.taskForm.controls.revisionReviewRequired.patchValue(true);
                    }*/
                    const minDateObject = this.commentsUtilService.addUnitsToDate(new Date(task.DUE_DATE), 1, 'days', this.enableWorkWeek);
                    this.taskModalConfig.minDate = minDateObject;
                    this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
                    if (!this.taskModalConfig.isTemplate) {
                        this.taskForm.controls.startDate.setValue(minDateObject);
                        this.taskForm.controls.endDate.setValue(this.taskModalConfig.selectedProject.DUE_DATE);
                    }
                }
            });
        } else {
            this.taskModalConfig.minDate = this.taskModalConfig.selectedpredecessor ?
                this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek) : this.taskModalConfig.selectedProject.START_DATE;
            this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
            let selectedPredecessor;
            if (this.taskConfig.isApprovalTask) {
                selectedPredecessor = this.taskModalConfig.deliveryPackageConfig.filterOptions.find(predecessorValue => predecessorValue.displayName === predecessor);
            } else {
                selectedPredecessor = this.taskModalConfig.predecessorConfig.filterOptions.find(predecessorValue => predecessorValue.displayName === predecessor);
            }
            if (selectedPredecessor) {
                this.taskForm.controls.predecessor.setValue(selectedPredecessor);
            }
            // if (!this.taskModalConfig.isTemplate) {
            //     this.taskForm.controls.startDate.setValue(this.taskModalConfig.selectedProject.START_DATE);
            //     this.taskForm.controls.endDate.setValue(this.taskModalConfig.selectedProject.DUE_DATE);
            // }
        }
    });

    if (this.taskForm.controls.owner) {
        this.taskForm.controls.owner.valueChanges.subscribe(ownerValue => {
            if (ownerValue && typeof ownerValue === 'object' && !this.taskModalConfig.isTemplate) {
                this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                    if (taskOwnerName.value === ownerValue.value) {
                        this.getTaskOwnerAssignmentCount();
                    }
                });
            } else {
                this.ownerProjectCountMessage = '';
                const selectedOwner = this.taskModalConfig.taskOwnerConfig.filterOptions.find(owner => owner.displayName === ownerValue);
                if (selectedOwner) {
                    this.taskForm.controls.owner.setValue(selectedOwner);
                }
            }
        });
    }
    this.taskForm.controls.allocation.valueChanges.subscribe(allocationTypeValue => {
        if (this.taskForm.controls.role) {
            this.taskForm.controls.role.setValue('');
        }
        if (this.taskForm.controls.owner) {
            this.taskForm.controls.owner.setValue('');
        }
        if (this.taskForm.controls.deliverableApprover) {
            this.taskForm.controls.deliverableApprover.setValue('');
        }
        if (this.taskForm.controls.deliverableReviewers) {
            this.taskForm.controls.deliverableReviewers.setValue('');
        }
        this.onChangeofAllocationType(allocationTypeValue, null, null, true);
    });

    /* this.taskForm.controls.deliverableReviewers.valueChanges.subscribe(deliverableReviewers => {
        console.log(deliverableReviewers);
    }); */

    this.taskForm.controls.role.valueChanges.subscribe(role => {
        if (role && typeof role === 'object') {
            this.onChangeofRole(role);
        } else {
            const selectedRole = this.taskModalConfig.taskRoleConfig.filterOptions.find(taskRole => taskRole.displayName === role);
            if (selectedRole) {
                this.taskForm.controls.role.setValue(selectedRole);
            }
        }
    });

    if (this.taskForm.controls.startDate) {
        this.taskForm.controls.startDate.valueChanges.subscribe(() => {
            if (this.taskForm.value.owner && this.taskForm.value.owner.value) {
                this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                    if (taskOwnerName.value === this.taskForm.value.owner.value) {
                        this.getTaskOwnerAssignmentCount();
                    }
                });
            } else {
                this.ownerProjectCountMessage = '';
            }
            if (this.dateValidation && this.dateValidation.startMaxDate && new Date(this.taskForm.controls.startDate.value) > new Date(this.dateValidation.startMaxDate)) {
                this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            } else {
                this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
            }

            if (new Date(this.taskForm.controls.endDate.value) > new Date(this.taskModalConfig.selectedProject.DUE_DATE)) {
                this.durationErrorMessage = this.taskConstants.DURATION_ERROR_MESSAGE;
            }

            if (this.enableDuration) {
                this.validateDuration();
            }

            if (this.dateValidation && this.dateValidation.startMaxDate && new Date(this.taskForm.controls.startDate.value) > new Date(this.dateValidation.startMaxDate)) {
                this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            } else {
                this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
            }


        });




        this.taskForm.controls.endDate.valueChanges.subscribe(() => {
            if (this.taskForm.value.owner && this.taskForm.value.owner.value) {
                this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                    if (taskOwnerName.value === this.taskForm.value.owner.value) {
                        this.getTaskOwnerAssignmentCount();
                    }
                });
            } else {
                this.ownerProjectCountMessage = '';
            }
            if (this.dateValidation && this.dateValidation.endMinDate && new Date(this.taskForm.controls.endDate.value) < new Date(this.dateValidation.endMinDate)) {
                this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
            } else if (new Date(this.taskForm.controls.endDate.value) < new Date(this.taskForm.controls.startDate.value)) {
                this.dateErrorMessage = this.taskConstants.START_DATE_ERROR_MESSAGE;
            } else {
                this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
            }



        });
    }
    if (this.enableDuration) {
        this.taskForm.controls.duration.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
            if (this.taskForm.controls.startDate && this.taskForm.controls.startDate.value && this.taskForm.controls.duration && this.taskForm.controls.duration.value) {

                this.validateDuration();
            }
        });
        this.taskForm.controls.durationType.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
            if (this.taskForm.controls.startDate && this.taskForm.controls.startDate.value && this.taskForm.controls.duration && this.taskForm.controls.duration.value) {

                this.validateDuration();
            }
        });
    }


    this.taskModalConfig.hasAllConfig = true;
}
}

