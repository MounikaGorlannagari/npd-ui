import { Component, OnInit } from '@angular/core';
import { TaskToolbarComponent } from 'mpm-library';

@Component({
  selector: 'app-task-toolbar',
  templateUrl: './task-toolbar.component.html',
  styleUrls: ['./task-toolbar.component.scss']
})
export class MPMTaskToolbarComponent extends TaskToolbarComponent {

}
