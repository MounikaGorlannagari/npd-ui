import { Component, OnInit } from '@angular/core';
import { SwimLaneComponent } from 'mpm-library';

@Component({
  selector: 'app-swim-lane',
  templateUrl: './swim-lane.component.html',
  styleUrls: ['./swim-lane.component.scss']
})
export class MPMSwimLaneComponent extends SwimLaneComponent {

}
