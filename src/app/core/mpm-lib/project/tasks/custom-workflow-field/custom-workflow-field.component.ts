import { Component } from '@angular/core';
import { CustomWorkflowFieldComponent } from 'mpm-library';

@Component({
  selector: 'app-custom-workflow-field',
  templateUrl: './custom-workflow-field.component.html',
  styleUrls: ['./custom-workflow-field.component.scss']
})
export class MPMCustomWorkflowFieldComponent extends CustomWorkflowFieldComponent {
}
