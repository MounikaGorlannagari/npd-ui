import { Component, OnInit } from '@angular/core';
import { BulkActionsComponent } from 'mpm-library';

@Component({
  selector: 'app-bulk-actions',
  templateUrl: './bulk-actions.component.html',
  styleUrls: ['./bulk-actions.component.scss']
})
export class MPMBulkActionsComponent extends BulkActionsComponent {

}
