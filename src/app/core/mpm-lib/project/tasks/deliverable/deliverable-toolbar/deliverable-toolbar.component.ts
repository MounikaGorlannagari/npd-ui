import { Component, OnInit } from '@angular/core';
import { DeliverableToolbarComponent } from 'mpm-library';

@Component({
  selector: 'app-deliverable-toolbar',
  templateUrl: './deliverable-toolbar.component.html',
  styleUrls: ['./deliverable-toolbar.component.scss']
})
export class MPMDeliverableToolbarComponent extends DeliverableToolbarComponent {

}
