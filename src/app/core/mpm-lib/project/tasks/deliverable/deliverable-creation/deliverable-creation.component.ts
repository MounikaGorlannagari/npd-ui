import { Component, OnInit } from '@angular/core';
import { DeliverableCreationComponent } from 'mpm-library';

@Component({
  selector: 'app-deliverable-creation',
  templateUrl: './deliverable-creation.component.html',
  styleUrls: ['./deliverable-creation.component.scss']
})
export class MPMDeliverableCreationComponent extends DeliverableCreationComponent {

}
