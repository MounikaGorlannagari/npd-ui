import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  AppService,
  AssetService,
  DeliverableCardComponent,
  DeliverableService,
  EntityAppDefService,
  FieldConfigService,
  FormatToLocalePipe,
  IndexerDataTypes,
  LoaderService,
  NotificationService,
  OtmmMetadataService,
  OTMMService,
  ProjectService,
  QdsService,
  SharingService,
  StatusService,
  TaskService,
  UtilService,
  ViewConfigService,
} from 'mpm-library';
import { MonsterUtilService } from 'src/app/core/npd-ui/services/monster-util.service';

@Component({
  selector: 'app-deliverable-card',
  templateUrl: './deliverable-card.component.html',
  styleUrls: ['./deliverable-card.component.scss'],
})
export class MPMDeliverableCardComponent extends DeliverableCardComponent {
  constructor(
    public dialog: MatDialog,
    public taskService: TaskService,
    public sharingService: SharingService,
    public formatToLocalePipe: FormatToLocalePipe,
    public notificationService: NotificationService,
    public loaderService: LoaderService,
    public appService: AppService,
    public entityAppdefService: EntityAppDefService,
    public otmmMetadataService: OtmmMetadataService,
    public otmmService: OTMMService,
    public assetService: AssetService,
    public utilService: UtilService,
    public projectService: ProjectService,
    public deliverableService: DeliverableService,
    public statusService: StatusService,
    public viewConfigService: ViewConfigService,
    public fieldConfigService: FieldConfigService,
    public qdsService: QdsService,
    public monsterUtilService: MonsterUtilService
  ) {
    super(
      dialog,
      taskService,
      sharingService,
      formatToLocalePipe,
      notificationService,
      loaderService,
      appService,
      entityAppdefService,
      otmmMetadataService,
      otmmService,
      assetService,
      utilService,
      projectService,
      deliverableService,
      statusService,
      viewConfigService,
      fieldConfigService,
      qdsService
    );
  }

  getWeek(column, dateValue) {
    //let date = this.monsterUtilService.getFieldValueByDisplayColumn(column,row);
    if (
      dateValue &&
      dateValue != 'NA' &&
      column?.DATA_TYPE.toLowerCase() ===
        IndexerDataTypes.DATETIME.toLowerCase()
    ) {
      return this.monsterUtilService.calculateWeek(dateValue);
    }
  }

  getWeekValue(taskData, columnName) {
    if (taskData?.[columnName] && taskData?.[columnName] != 'NA') {
      return this.monsterUtilService.calculateWeek(taskData[columnName]);
    }
  }
}
