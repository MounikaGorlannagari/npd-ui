import { Component, OnInit } from '@angular/core';
import { DeliverableSwinLaneComponent } from 'mpm-library';

@Component({
  selector: 'app-deliverable-swim-lane',
  templateUrl: './deliverable-swim-lane.component.html',
  styleUrls: ['./deliverable-swim-lane.component.scss']
})
export class MPMDeliverableSwimLaneComponent extends DeliverableSwinLaneComponent {

}
