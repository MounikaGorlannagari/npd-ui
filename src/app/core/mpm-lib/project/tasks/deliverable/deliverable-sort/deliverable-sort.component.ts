import { Component, OnInit } from '@angular/core';
import { DeliverableSortComponent } from 'mpm-library';

@Component({
  selector: 'app-deliverable-sort',
  templateUrl: './deliverable-sort.component.html',
  styleUrls: ['./deliverable-sort.component.scss']
})
export class MPMDeliverableSortComponent extends DeliverableSortComponent {

}
