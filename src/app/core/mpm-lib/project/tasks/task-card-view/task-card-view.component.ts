import { Component, OnInit } from '@angular/core';
import { TaskCardViewComponent } from 'mpm-library';

@Component({
  selector: 'app-task-card-view',
  templateUrl: './task-card-view.component.html',
  styleUrls: ['./task-card-view.component.scss']
})
export class MPMTaskCardViewComponent extends TaskCardViewComponent {

}
