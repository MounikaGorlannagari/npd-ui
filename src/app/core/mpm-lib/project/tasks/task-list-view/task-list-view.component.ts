import { Component, OnInit } from '@angular/core';
import { TaskListViewComponent } from 'mpm-library';

@Component({
  selector: 'app-task-list-view',
  templateUrl: './task-list-view.component.html',
  styleUrls: ['./task-list-view.component.scss']
})
export class MPMTaskListViewComponent extends TaskListViewComponent {

}
