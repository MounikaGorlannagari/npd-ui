import { Component, OnInit } from '@angular/core';
import { AssetDetailsComponent } from 'mpm-library';

@Component({
  selector: 'app-asset-details-view',
  templateUrl: './asset-details-view.component.html',
  styleUrls: ['./asset-details-view.component.scss']
})
export class MPMAssetDetailsViewComponent extends AssetDetailsComponent {

}
