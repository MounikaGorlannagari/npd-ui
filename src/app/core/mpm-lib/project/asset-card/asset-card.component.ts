import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatMenu } from '@angular/material/menu';
import { AssetCardComponent } from 'mpm-library';

@Component({
  selector: 'app-asset-card',
  templateUrl: './asset-card.component.html',
  styleUrls: ['./asset-card.component.scss']
})
export class MPMAssetCardComponent extends AssetCardComponent {
  @Output() onAssetDelete = new EventEmitter<string>();
  @Input()showDeleteIcon;
  


  public deleteAsset(asset){
   
    this.onAssetDelete.emit(asset);
  }

  getColumnValue(column){
    const data = this.asset?.allMetadata;
    const assetKeys=Object.keys(this.asset);
    if(assetKeys.includes(column.OTMM_FIELD_ID)){
      return this.asset[column.OTMM_FIELD_ID];
    }
    else{
      for(let row = 0; row < data.length; row++){
        if(column.OTMM_FIELD_ID === data[row].id){
          if(data[row].value && data[row].value.value && data[row].value.value.value){
            return data[row].value.value.value;
          }
          else if(data[row].metadata_element && data[row].metadata_element.value && data[row].metadata_element.value.value && data[row].metadata_element.value.value.value){
            return data[row].metadata_element.value.value.value;
          }
        }
      }
      for(let metadatalist=0;metadatalist<this.asset.metadata.metadata_element_list.length;metadatalist++){
        const metadatarow=this.asset?.metadata?.metadata_element_list[metadatalist]?.metadata_element_list;
        for(let row=0;row<metadatarow.length;row++){
          if(column.OTMM_FIELD_ID === metadatarow[row].id){
            if(metadatarow[row].value && metadatarow[row].value.value && metadatarow[row].value.value.value){
              return metadatarow[row].value.value.value;
            }
            else if(metadatarow[row].metadata_element && metadatarow[row].metadata_element.value && metadatarow[row].metadata_element.value.value && metadatarow[row].metadata_element.value.value.value){
              return metadatarow[row].metadata_element.value.value.value;
            }
          }
        }
      }
    return "NA";
    }
    
  }

  updateDescription(event?) {
    if(this.assetDescription===''){
      event?.stopPropagation();
      this.notificationService.error("Please enter the description");
      
      
    }else{
      this.loadingHandler(true);
    let assetId = this.asset.asset_id;
    let body = {
      "edited_asset":{
         "data":{
            "asset_identifier":"string",
            "metadata":[
               {
                  "id":"ARTESIA.FIELD.ASSET DESCRIPTION",
                  "name":"Description",
                  "type":"com.artesia.metadata.MetadataField",
                  "value":{
                     "value":{
                        "type":"string",
                        "value": this.assetDescription
                     }
                  }
               }
            ]
         }
      }
   }
   this.otmmService.updateMetadata(assetId,'Asset',body).subscribe((res) => {
    this.notificationService.success('metadata Successfully updated');
   });
      this.loadingHandler(false);
    }
  }

  

}
