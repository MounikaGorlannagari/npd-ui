import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpmAssetListComponent } from './asset-list.component';

describe('MpmAssetListComponent', () => {
  let component: MpmAssetListComponent;
  let fixture: ComponentFixture<MpmAssetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpmAssetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpmAssetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
