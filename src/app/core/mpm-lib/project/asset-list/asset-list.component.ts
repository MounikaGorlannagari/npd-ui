import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, Renderer2, isDevMode } from '@angular/core';
import { AppService, AssetListComponent, AssetService, DeliverableService, EntityAppDefService, FieldConfigService, LoaderService, MPMField, NotificationService, OTMMService, QdsService, SharingService, TaskService, UtilService } from 'mpm-library';

@Component({
  selector: 'app-asset-list',
  templateUrl: './asset-list.component.html',
  styleUrls: ['./asset-list.component.scss']
})
export class MpmAssetListComponent extends AssetListComponent implements OnInit{

  @Output() onAssetDelete = new EventEmitter<string>();
  @Input()showDeleteIcon;
  
  constructor(
     utilService: UtilService,
     renderer: Renderer2,
     fieldConfigService: FieldConfigService,
     public sharingservice: SharingService,
     public otmmservice: OTMMService,
     public notificationservice: NotificationService,
     loaderService: LoaderService,
     qdsService: QdsService,
     entityAppdefService: EntityAppDefService,
     taskService: TaskService,
     assetService: AssetService,
     deliverableService: DeliverableService,
     appService: AppService
  ){
    super(utilService,renderer,fieldConfigService,sharingservice,otmmservice,notificationservice,loaderService,qdsService,entityAppdefService,taskService,assetService,deliverableService,appService);
  }
  public deleteAsset(asset){
    
    this.onAssetDelete.emit(asset);
  }


  getColumnProperty(column,data,row?){
    const assetKeys=Object.keys(row);
    if(assetKeys.includes(column.OTMM_FIELD_ID)){
      return row[column.OTMM_FIELD_ID];
    }
    else{
      for(let row = 0; row < data.length; row++){
        if(column.OTMM_FIELD_ID === data[row].id){
          if(data[row].value && data[row].value.value && data[row].value.value.value){
            return data[row].value.value.value;
          }
          else if(data[row].metadata_element && data[row].metadata_element.value && data[row].metadata_element.value.value && data[row].metadata_element.value.value.value){
            return data[row].metadata_element.value.value.value;
          }
          else if(data[row].metadata_element && data[row].metadata_element.value && data[row].metadata_element.value.value && data[row].metadata_element.value.value.field_value){
            return data[row].metadata_element.value.value.field_value.value;
          }
        }
      }
      for(let metadatalist=0;metadatalist<row.metadata.metadata_element_list.length;metadatalist++){
        const metadatarow=row?.metadata.metadata_element_list[metadatalist]?.metadata_element_list;
        for(let row=0;row<metadatarow.length;row++){
          if(column.OTMM_FIELD_ID === metadatarow[row].id){
            if(metadatarow[row].value && metadatarow[row].value.value && metadatarow[row].value.value.value){
              return metadatarow[row].value.value.value;
            }
            else if(metadatarow[row].metadata_element && metadatarow[row].metadata_element.value && metadatarow[row].metadata_element.value.value && metadatarow[row].metadata_element.value.value.value){
              return metadatarow[row].metadata_element.value.value.value;
            }
          }
        }
      }
      return 'NA';
    }
    }

    updateDescription(event?) {
      
      if(this.assetDescription===''){
        event?.stopPropagation();
        this.notificationservice.error("Please enter the description");
      }else{
        this.loadingHandler(true);
      let assetId = this.currentAsset.asset_id;
      let body = {
        "edited_asset":{
           "data":{
              "asset_identifier":"string",
              "metadata":[
                 {
                    "id":"ARTESIA.FIELD.ASSET DESCRIPTION",
                    "name":"Description",
                    "type":"com.artesia.metadata.MetadataField",
                    "value":{
                       "value":{
                          "type":"string",
                          "value": this.assetDescription
                       }
                    }
                 }
              ]
           }
        }
     }
      this.otmmservice.updateMetadata(assetId,'Asset',body).subscribe((res) => {
        this.notificationservice.success('metadata Successfully updated');
      }, (error) => {
         this.notificationservice.error('Something Went Wrong While Updating the Metadata');
      });
        this.loadingHandler(false);
      }
      
    }

    ngOnInit(){
      this.assetList = this.assets;
      this.otmmBaseUrl = isDevMode() ? './' : this.sharingservice.getMediaManagerConfig().url;
      this.displayColumns = this.assetDisplayableFields.map((column: MPMField) => column.INDEXER_FIELD_ID);
      this.crActionData = this.sharingservice.getCRActions();
      this.refreshData();
      this.uncheckAllProjects();
    }
    
}
