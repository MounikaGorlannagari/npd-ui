import { Component } from '@angular/core';
import { ProjectOverviewComponent } from 'mpm-library';

@Component({
  selector: 'app-project-overview',
  templateUrl: './project-overview.component.html',
  styleUrls: ['./project-overview.component.scss']
})
export class MPMProjectOverviewComponent extends ProjectOverviewComponent {

}
