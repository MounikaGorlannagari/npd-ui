import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren, isDevMode } from '@angular/core';
import { AssetsComponent } from 'mpm-library';
import { MPMAssetCardComponent } from '../asset-card/asset-card.component'
@Component({
  selector: 'app-assets-view',
  templateUrl: './assets-view.component.html',
  styleUrls: ['./assets-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MPMAssetsViewComponent extends AssetsComponent{
  @ViewChildren(MPMAssetCardComponent) assetCardComponent: QueryList<ElementRef>;
  @Output() onAssetDelete = new EventEmitter<string>();
  @Input()showDeleteIcon;


  public unChekedAllAsets() {
    

    let components: any;
    components = this.assetCardComponent;
    components?._results?.forEach(x => {
      x.checked = false;
    })
  }
  public deleteAsset(asset) {
   
    this.onAssetDelete.emit(asset);
  }

}
