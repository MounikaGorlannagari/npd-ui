import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NPDFacetService {

  constructor() { }

  // public dashboardPreference = new BehaviorSubject([]);

  public orderedDisplayableFields = new BehaviorSubject([]);

  // onOpenFacets = this.dashboardPreference.asObservable(); 

  onChangeofDisplayOrder = this.orderedDisplayableFields.asObservable();

  // updateDashboardPreference(preference) {
  //   this.dashboardPreference.next(preference);
  // }

  updateOrderdDisplayableFields(fields){
    this.orderedDisplayableFields.next(fields);
  }
}
