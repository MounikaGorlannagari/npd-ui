import { Component, OnInit, OnDestroy, ViewChild, Input, ElementRef } from '@angular/core';
import { FacetChangeEventService, Facet } from 'mpm-library';
import { MatAccordion } from '@angular/material/expansion';
import { FacetChickletEventService } from 'mpm-library';
import { FacetConditionList, FacetFieldCondition } from 'mpm-library';
import { SearchChangeEventService } from 'mpm-library';
import { FormatToLocalePipe } from 'mpm-library';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { SharingService } from 'mpm-library';
import { MPMField } from 'mpm-library';
import { MPMFacetConfig } from 'mpm-library';
import { FacetService } from 'mpm-library';
import { LoaderService } from 'mpm-library';
import { RouteService } from '../../mpm/route.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { StateService } from 'mpm-library';
import { MenuConstants } from '../../mpm/shared/constants/menu.constants';
import { WeekPickerService } from '../../npd-ui/services/week-picker.service';
import { WeekPickerComponent } from '../../npd-ui/npd-task-view/week-picker/week-picker.component';
import { NPDFacetService } from './facet.service';
import { SPField } from '../../npd-ui/request-view/constants/BusinessConstants';

@Component({
  selector: 'app-facet',
  templateUrl: './facet.component.html',
  styleUrls: ['./facet.component.scss']
})

export class FacetComponent implements OnInit, OnDestroy {
  startDateFlag = false
  endDateFlag = false;

  chickletData: any[] = [];
  facetFilters: Facet[] = [];
  allMPMFields: MPMField[] = [];
  selectedFacetFilters: FacetFieldCondition[] = [];
  facetConfigurations: MPMFacetConfig[];
  activeFacetConfiguration: MPMFacetConfig;
  allExpandState = true;
  hasSP1False = false;
  hasSP2False = false;
  hasSP3False = false;
  hasSP4False = false;
  hasSP1ProofFalse = false;
  hasSP2ProofFalse = false;
  hasSP3ProofFalse = false;
  hasSP4ProofFalse = false;
  hasProjectFalse = false;

  hasMatch = false;
  hasDateField = false;

  viewName;
  isListView;
  sortBy;
  sortOrder;
  pageNumber;
  pageSize;
  searchName;
  savedSearchName;
  advSearchData;
  facetData;

  queryParams;
  groupByFilter;
  deliverableTaskFilter;
  listDependentFilter;

  facetGenBehaviour = 'EXCLUDE';
  isShowAll = false;

  tmpFacetResetEventSubscription$;
  tmpFacetChangeEventSubscription$;

  show = {
    startDate: false,
    endDate: false,
  };

  weekYear = {
    startDate: null,
    endDate: null,
  };

  fromDate = {
    startDate: null,
    endDate: null,
  };

  toDate = {
    startDate: null,
    endDate: null,
  };

  @ViewChild('facetAccordion') facetAccordion: MatAccordion;

  @ViewChild('weekPicker') weekPicker: WeekPickerComponent;
  @ViewChild('searchInput') searchInput: ElementRef<HTMLInputElement>;
  removeSearchInput: string;


  @Input() dataCount;

  isSelected;

  constructor(
    private adapter: DateAdapter<any>,
    private formatToLocalePipe: FormatToLocalePipe,
    private sharingService: SharingService,
    private facetService: FacetService,
    private searchChangeEventService: SearchChangeEventService,
    private facetChangeEventService: FacetChangeEventService,
    private facetChickletEventService: FacetChickletEventService,
    private loaderService: LoaderService,
    private activatedRoute: ActivatedRoute,
    private routeService: RouteService,
    private stateService: StateService,
    private weekPickerService: WeekPickerService,
    private npdFacetService: NPDFacetService,
    private router: Router
  ) { }

  toggleExpandState() {
    this.allExpandState = !this.allExpandState;
    if (this.allExpandState) {
      this.facetAccordion.openAll();
    } else {
      this.facetAccordion.closeAll();
    }
  }

  toggleShowMore(facet) {
    facet.isShowAll = !facet.isShowAll;
    if (!facet.isShowAll) {
      facet.height = (this.activeFacetConfiguration.defaultFacetValueDisplayed * 33) + 'px'; //23
    } else {
      facet.height = 'auto';
    }
  }

  toggleShowAll() {
    this.isShowAll = !this.isShowAll;
  }

  getFieldDetails(fieldId: string) {
    return this.allMPMFields.find(field => {
      return field.INDEXER_FIELD_ID === fieldId;
    });
  }

  publishFacetFieldRestriction() {
    //https://acheron-tech.atlassian.net/browse/MPMV3-1695
    this.selectedFacetFilters.forEach(filter => {
      filter.values.forEach(fill => {
        if (fill && fill.interval_label && (fill.interval_label.includes('%') ||
          fill.interval_label.includes('months') ||
          fill.interval_label.includes('day') ||
          fill.interval_label.includes('hours') ||
          fill.interval_label.includes('today') ||
          fill.interval_label.toLowerCase() === 'tomorrow')) {
          delete fill.displayName;
        }
      });
    });

    const facetRestrictionList: FacetConditionList = {
      facet_condition: this.selectedFacetFilters
    };
    this.facetChangeEventService.updateFacetSelectFacetCondition(facetRestrictionList);
    const facetFilters = [];
    this.selectedFacetFilters.forEach(filter => {
      const facetFilter = {
        fieldId: filter.field_id,
        type: filter.type,
        values: JSON.stringify(filter.values)
      };
      facetFilters.push(facetFilter);
    });
    this.allExpandState = true;

    this.facetData = facetFilters && facetFilters.length > 0 ? JSON.stringify(facetFilters) : null;
    if (this.queryParams.menuName === MenuConstants.DELIVERABLE_TASK_VIEW) {
      this.stateService.setDeliverableTaskViewFacetSearch(this.selectedFacetFilters);
      this.routeService.goToDeliverableTaskView(this.queryParams, this.searchName, this.savedSearchName, this.advSearchData, this.facetData ? this.facetData : null);
    } else if (this.queryParams.menuName === MenuConstants.RESOURCE_MANAGEMENT) {
      this.stateService.setResourceManagementFacetSearch(this.selectedFacetFilters);
      this.routeService.goToResourceManagement(this.queryParams, this.searchName, this.savedSearchName, this.advSearchData, this.facetData ? this.facetData : null);
    } else if (this.queryParams.menuName === MenuConstants.PROJECT_MANAGEMENT) {
      this.stateService.setFacetSearch(this.selectedFacetFilters);
      this.routeService.goToDashboard(this.viewName, this.isListView, this.sortBy, this.sortOrder, this.pageNumber, this.pageSize,
        this.searchName, this.savedSearchName, this.advSearchData, this.facetData ? this.facetData : null);
    } else {
      this.routeService.goToRequestDashboard();
    }
  }

  getValue(value) {
    if (!value) {
      return;
    }
    if (typeof value === 'string') {
      return value;
    } else if (typeof value === 'object' && value !== null) {
      if (value.value) {
        return value.value;
      } else if (value.range_label) {
        return value.range_label;
      }
      else if (value.displayName) {
        return value.displayName;
      }
      else if (value.interval_label) {
        return value.interval_label;
      }

    }
    return;
  }

  updateChicklet() {

    //https://acheron-tech.atlassian.net/browse/MPMV3-1695
    //https://acheron-tech.atlassian.net/browse/MPMV3-2197
    //https://acheron-tech.atlassian.net/browse/MPMV3-2196 - Reopened ticket solution

    if (this.selectedFacetFilters.length > 0) {
      this.selectedFacetFilters.forEach(selectedFilter => {
        if (selectedFilter.type.indexOf('Interval') !== -1) {
          selectedFilter.values.forEach(fill => {
            let facetFilter = this.facetFilters.find(item => { return item.facet_field_request.field_id === selectedFilter.field_id; });
            let dateInterval = facetFilter.facet_value_list.find(item => { return fill.interval_label === item.date_interval.interval_label });
            if (dateInterval && dateInterval.date_interval) {
              fill.displayName = dateInterval.date_interval.displayName;
            }
          });
        }
      });
    }

    const chicklets = [];
    this.selectedFacetFilters.forEach(item => {
      item.values.forEach(value => {
        const data = {
          fieldId: item.field_id,
          value: this.getValue(value),
          originalValue: value
        };
        chicklets.push(data);
      });
    });
    this.facetChickletEventService.updateChickletFilters(chicklets);
    this.setDynamicHeightForFacetContainer();

  }

  clearAllFacets() {
    this.loaderService.show();
    this.selectedFacetFilters = Object.assign([]);


    this.updateChicklet();
    this.publishFacetFieldRestriction();
  }

  getFactFromSelectedFacets(fieldId) {
    return this.selectedFacetFilters.find(facetFilterItem => {
      return facetFilterItem.field_id === fieldId;
    });
  }

  isValueSelected(fieldId, facetValue) {
    if (!fieldId || !facetValue) {
      return;
    }
    const fieldRestrictionItem = this.getFactFromSelectedFacets(fieldId);
    const facetObj = this.facetFilters.find(filterItrItem => {
      return filterItrItem.facet_field_request.field_id === fieldId;
    });
    if (!fieldRestrictionItem || !facetObj) {
      return;
    }
    if (typeof facetValue === 'string') {
      if (fieldRestrictionItem.values.indexOf(facetValue) !== -1) {
        return true;
      }
    } else if (typeof facetValue === 'object' && facetValue !== null) {
      let value = null;
      if (facetObj.isDate && facetObj.isRange) {
        value = facetValue.range_label || facetValue.date_range.range_label;
      } else if (facetObj.isDate && facetObj.isInterval) {
        value = facetValue.interval_label || facetValue.date_interval.interval_label;
      } if (facetObj.isNumeric && facetObj.isRange) {
        value = facetValue.range_label || facetValue.numeric_range.range_label;
      } else if (facetObj.isNumeric && facetObj.isInterval) {
        value = facetValue.interval_label || facetValue.numeric_interval.interval_label;
      } else if (facetValue.value) {
        value = facetValue.value;
      }
      if (!value) {
        return;
      }
      return fieldRestrictionItem.values.find(restrictionItemValue => {
        if (typeof restrictionItemValue === 'string') {
          if (fieldRestrictionItem.values.indexOf(value) !== -1) {
            return true;
          }
        } else if (typeof facetValue === 'object' && facetValue !== null) {
          return (
            restrictionItemValue.range_label === value ||
            restrictionItemValue.interval_label === value ||
            restrictionItemValue.value === value
          );
        }
      });
    }
  }

  removeFilterSelection(fieldId, facetValue) {
    if (!fieldId || !facetValue) {
      return;
    }
    const fieldSelected = this.getFactFromSelectedFacets(fieldId);
    if (!fieldSelected) {
      return;
    }
    const selectedFacetFiltersCopy = Object.assign([], this.selectedFacetFilters);
    if (fieldSelected.values.length > 1) {
      const filteredValues = fieldSelected.values.filter(valueItem => {
        if (!facetValue) {
          return true;
        }
        if (facetValue === valueItem) {
          return false;
        } if (facetValue && facetValue.range_label && facetValue.range_label === valueItem.range_label) {
          return false;
        } else if (facetValue && facetValue.interval_label && facetValue.interval_label === valueItem.interval_label) {
          return false;
        } if (facetValue.value && valueItem.value && (facetValue.value === valueItem.value)) {
          return false;
        } else {
          return true;
        }
      });
      fieldSelected.values = filteredValues;
    } else {
      this.selectedFacetFilters = selectedFacetFiltersCopy.filter(selectionItem => {
        return selectionItem.field_id !== fieldId;
      });
    }
  }

  handleFacetSelection(fieldId, facetValue) {
    const facetObj = this.facetFilters.find(filterItrItem => {
      return filterItrItem.facet_field_request.field_id === fieldId;
    });
    if (this.selectedFacetFilters.length === 0) {
      this.selectedFacetFilters.push({
        type: facetObj.type,
        behavior: this.facetGenBehaviour,
        field_id: fieldId,
        values: [facetValue]
      });
      this.isSelected = true;
    } else {
      const fieldSelected = this.getFactFromSelectedFacets(fieldId);
      if (fieldSelected) {
        const isValueSelected = this.isValueSelected(fieldId, facetValue);
        if (isValueSelected) {
          this.removeFilterSelection(fieldId, facetValue);
          this.isSelected = false;
        } else {
          if (facetObj) {
            if (facetObj.facet_field_request.multi_select) {
              fieldSelected.values.push(facetValue);
            } else {
              fieldSelected.values = [facetValue];
            }
          } else {
            this.removeFilterSelection(fieldId, facetValue);
            this.isSelected = false;
          }
        }
      } else {
        this.selectedFacetFilters.push({
          type: facetObj.type,
          behavior: this.facetGenBehaviour,
          field_id: fieldId,
          values: [facetValue]
        });
      }
    }

    this.updateChicklet();
    this.publishFacetFieldRestriction();
  }

  filterByCustomFacet(facet: Facet) {
    
    let customValue;
    if (facet.isDate) {
      if (facet.isInterval) {
        let [startDate] = this.weekPickerService.getStartEndDate(
          facet.formGroup.get('start_date').value, facet.facet_field_request.field_id
        );
        let [endDate] = this.weekPickerService.getStartEndDate(
          facet.formGroup.get('end_date').value, facet.facet_field_request.field_id
        );
        customValue = {
          custom_range: true,
          fixed_end_date: this.toDateString(endDate),
          fixed_start_date: this.toDateString(startDate),
          interval_label: facet.formGroup.get('start_date').value
            + ' to ' + facet.formGroup.get('end_date').value
        };
      }


      else if (facet.isRange) {
        if (facet['startDateFlag'] && facet['endDateFlag']) {
          customValue = {
            custom_range: true,
            end_date: this.toDateString(facet.formGroup.get('end_date').value),
            start_date: this.toDateString(facet.formGroup.get('start_date').value),
            range_label: this.converToLocalDate(facet.formGroup.get('start_date').value)
              + ' to ' + this.converToLocalDate(facet.formGroup.get('end_date').value)
          };
        } else {

          let [startDate] = this.weekPickerService.getStartEndDate(
            facet.formGroup.get('start_date').value, facet.facet_field_request.field_id
          );
          let [, endDate] = this.weekPickerService.getStartEndDate(
            facet.formGroup.get('end_date').value, facet.facet_field_request.field_id
          );
          customValue = {
            custom_range: true,
            end_date: this.toDateString(endDate),
            start_date: this.toDateString(startDate),
            range_label: facet.formGroup.get('start_date').value
              + ' to ' + facet.formGroup.get('end_date').value
          };
        }

      }


    } else if (facet.isNumeric) {
      if (facet.isInterval) {
        customValue = {
          custom_range: true,
          range_label: facet.formGroup.get('start_value').value + ' - ' + facet.formGroup.get('end_value').value,
          end_value: facet.formGroup.get('end_value').value,
          start_value: facet.formGroup.get('start_value').value
        };
      } else if (facet.isRange) {
        customValue = {
          custom_range: true,
          range_label: facet.formGroup.get('start_value').value + ' - ' + facet.formGroup.get('end_value').value,
          end_value: facet.formGroup.get('end_value').value,
          start_value: facet.formGroup.get('start_value').value
        };
      }
    }
    if (customValue) {
      this.handleFacetSelection(facet.facet_field_request.field_id, customValue);
    }
  }

  filterByFacets(filterId, filterValue) {
    this.loaderService.show();
    if (!filterId || !filterValue) {
      return;
    }
    this.handleFacetSelection(filterId, filterValue);
  }

  chickletRemoved(removedFilters) {
    this.loaderService.show();
    const removedFilter = removedFilters[0];
    this.handleFacetSelection(removedFilter.fieldId, removedFilter.originalValue);
  }

  setFilterSelectinAndViewType(filter) {
    let isFilterSelected = false;
    const fieldRestrictionItem = this.selectedFacetFilters.find(facetFilterItem => {
      return facetFilterItem.field_id === filter.facet_field_request.field_id;
    });
    if (fieldRestrictionItem && Array.isArray(fieldRestrictionItem.values) && fieldRestrictionItem.values.length > 0) {
      if (filter.facet_field_request.multi_select) {
        filter.facet_value_list.forEach(valueObj => {
          if (this.isValueSelected(fieldRestrictionItem.field_id, valueObj)) {
            valueObj.isSelected = true;
            isFilterSelected = true;
          }
        });
      } else {
        filter.facet_value_list.forEach(valueObj => {
          if (this.isValueSelected(fieldRestrictionItem.field_id, valueObj)) {
            if (typeof valueObj === 'string') {
              filter.selectedItem = valueObj;
            } else if (typeof valueObj === 'object' && valueObj !== null) {
              let valueToAssign;
              let dataType;
              if (filter.isDate) {
                dataType = 'date';
              } else if (filter.isNumeric) {
                dataType = 'numeric';
              } else {
                valueToAssign = this.getValue(valueObj);
              }
              if (dataType) {
                if (filter.isInterval) {
                  valueToAssign = this.getValue(valueObj[dataType + '_' + 'interval']);
                } else if (filter.isRange) {
                  valueToAssign = this.getValue(valueObj[dataType + '_' + 'range']);
                }
              }
              if (valueToAssign) {
                filter.selectedItem = valueToAssign;
                valueObj.selectedItem = valueToAssign;
              }
            }
            isFilterSelected = true;
          }
        });
      }

      //MPMV3-1169- Because of this ticket I am commenting the following snippet
      /*if (this.activeFacetConfiguration.valueOrder === 'COUNT') {
        filter.facet_value_list.sort((a, b) => (a.asset_count < b.asset_count) ? 1 : -1);
      }*/
    }
    if (this.activeFacetConfiguration.defaultFacetValueDisplayed &&
      Array.isArray(filter.facet_value_list) && filter.facet_value_list.length >
      this.activeFacetConfiguration.defaultFacetValueDisplayed) {
      if (!isFilterSelected) {
        filter.height = (this.activeFacetConfiguration.defaultFacetValueDisplayed * 33) + 'px';

      } else {
        filter.height = 'auto';
        filter.isShowAll = true;
      }
    } else {
      filter.height = 'auto';
      filter.isShowAll = true;
    }
  }

  converToLocalDate(date) {
    const utcDate = new Date(date);
    const formattedDate = utcDate.toLocaleDateString('en-GB');
    return formattedDate;
  }

  toDateString(date) {
    return date.toISOString();
  }

  formRangeLabel(filter) {
    const isDateType = filter.isDate;
    const isNumeric = filter.isNumeric;
    const datetype = filter.facet_field_request.field_id
    filter.facet_value_list.map(value => {
      if (isDateType) {
        if (filter.startDateFlag && filter.endDateFlag) {

          value.date_range.range_label = (
            (value.date_range.start_date ? this.converToLocalDate(value.date_range.start_date) : 'before')
            + ' - ' +
            (value.date_range.end_date ? this.converToLocalDate(value.date_range.end_date) : 'after')
          );
        }


        else {

          value.date_range.range_label = (
            (value.date_range.start_date ? this.weekPickerService.calculateTaskWeek(value.date_range.start_date, datetype) : 'before')
            + ' - ' +
            (value.date_range.end_date ? this.weekPickerService.calculateTaskWeek(value.date_range.end_date, datetype) : 'after')
          );

        }

      } else if (isNumeric) {
        if (value.numeric_range.start_value && value.numeric_range.end_value) {
          value.numeric_range.range_label = value.numeric_range.start_value + ' - ' + value.numeric_range.end_value;
        } else if (value.numeric_range.start_value) {
          value.numeric_range.range_label = ' > ' + value.numeric_range.start_value;
        } else if (value.numeric_range.end_value) {
          value.numeric_range.range_label = ' < ' + value.numeric_range.end_value;
        }
      }
    });
  }


  public setDynamicHeightForFacetContainer() {
    const dynamicHeightElement = document.getElementById('facet-list-container');
    let containerHeight = document.getElementById('facet-header-wrapper')?.clientHeight;
    if (dynamicHeightElement) {

      if (this.router.url.includes('ResourceManagement')) {
        dynamicHeightElement.style.height = `calc(63vh - ${containerHeight + 105}px`;
      } else {
        dynamicHeightElement.style.height = `calc(100vh - ${containerHeight + 105}px`;
      }
    }

  }
  initalizeFacets() {

    const activeFacetId = this.searchChangeEventService.getFacetConfigIdForActiveSearch();
    this.activeFacetConfiguration = this.facetConfigurations.find(config => {
      return config.id === activeFacetId;
    });
    if (this.activeFacetConfiguration) {
      this.facetFilters.forEach(filter => {

        if (filter && filter.type) {
          const typeSplitted = filter.type.split('Response');
          if (typeSplitted.length > 1) {
            filter.type = typeSplitted[0] + 'Restriction';
          }
        }
        if (filter.type.indexOf('Date') !== -1) {
          filter.isDate = true;
          filter.formGroup = new FormGroup({
            start_date: new FormControl('', Validators.required),
            end_date: new FormControl('', Validators.required)
          });
        }
        if (filter.type.indexOf('Numeric') !== -1) {
          filter.isNumeric = true;
          filter.formGroup = new FormGroup({
            start_value: new FormControl('', Validators.required),
            end_value: new FormControl('', Validators.required)
          });
        }

        if (filter.type.indexOf('Range') !== -1) {
          filter.isRange = true;
          this.formRangeLabel(filter);
        }
        if (filter.type.indexOf('Interval') !== -1) {
          this.facetService.facetIntervalDisplayValues(filter);
          filter.isInterval = true;
        }
        if (!filter.facet_field_request.name) {
          const fieldDetails: MPMField = this.getFieldDetails(filter.facet_field_request.field_id);
          filter.facet_field_request.name = fieldDetails ? fieldDetails.DISPLAY_NAME : '';
        }
        this.setFilterSelectinAndViewType(filter);
      });
    } else {
    }

    this.scrollToMatch(this.facetFilters[0]?.facet_field_request?.name)
    
  }

  scrollToMatch(param?) {


    const searchQuery = this.searchInput?.nativeElement.value.toLowerCase();
    let element;
    let match;


    // if seaechquery is empty then it  match should also be empty becuse startwith element takes empth string as first element and effects scroll

    if (!searchQuery) {
      match = ''

    } else if (searchQuery) {
      // Find the first matching facet
      match = this.facetFilters.find(facet =>
        facet.facet_field_request.name?.toLowerCase().startsWith(searchQuery)
      );

    }


    //if macth is not null scroll should go to user input
    if (match != null && match != '') {

      setTimeout(() => {
        if (param != null) {
          element = document.getElementById(`facet_${match.facet_field_request.name.replace(' ', '_')}`)
        } else {
          element = document.getElementById(`facet_${match.facet_field_request.name.replace(' ', '_')}`)
        }


       
        element?.scrollIntoView({ behavior: 'smooth' });
        this.searchInput?.nativeElement.focus();
      }, 0);

      // For automatically open show more facets while searching element under more facets
      if (!this.isShowAll && this.facetFilters?.length > this.activeFacetConfiguration?.defaultFacetDisplayed) {
        this.toggleShowAll();
      }


    } // if it is null scroll to top of facet container
    else if (match == '') {

    
      element?.scrollIntoView({ behavior: 'smooth' });
      this.searchInput?.nativeElement.focus();

    }
    // For automatically open show more facets while searching element under more facets

    // if (!this.isShowAll && this.facetFilters?.length > this.activeFacetConfiguration?.defaultFacetDisplayed) {
    //   this.toggleShowAll();
    // }

    else if (!element || typeof element === 'undefined') { // if element is null or undefined
      const facetContainer = document.querySelector('.scroll-container');
      facetContainer?.scrollIntoView({ behavior: 'smooth' });
    }

  }


  resetFacet() {
    // this.isCalledFromResetFacet = true; // Set the flag to true

    this.facetFilters = Object.assign([]);
    this.selectedFacetFilters = Object.assign([]);
    this.updateChicklet();
    // const facetRestrictionList: FacetConditionList = {
    //   facet_condition: this.selectedFacetFilters
    // };
    // this.facetChangeEventService.updateFacetSelectFacetCondition(facetRestrictionList);
  }

  getBool(date) {
    this.show[date] = !this.show[date];
  }

  getAllBool() {
    Object.keys(this.show).map((ele) => {
      this.show[ele] = false;
    });
  }

  changeTargetDP(weekFormat, date, facet) {
    this.weekYear[date] = weekFormat;
    if (date == 'startDate') {
      facet.formGroup.patchValue({ start_date: weekFormat });
    } else {
      facet.formGroup.patchValue({ end_date: weekFormat });
    }
    [this.fromDate[date], this.toDate[date]] = this.weekPickerService.getWeekFormat(weekFormat);
  }

  sortByOrder(array, order) {
    return array.sort((a, b) => order.indexOf(a.facet_field_request.field_id) - (order.indexOf(b.facet_field_request.field_id)));
  }
  ngOnInit(): void {

    if (navigator.language !== undefined) {
      this.adapter.setLocale(navigator.language);
    }
    let order;
    this.allMPMFields = this.sharingService.getAllApplicationFields();
    this.npdFacetService.onChangeofDisplayOrder.subscribe(res => {
      this.setDynamicHeightForFacetContainer();

      order = [];
      res.map(item => {
        if (item.INDEXER_FIELD_ID) {
          order.push(item.INDEXER_FIELD_ID)
        }
        else {
          order.push(item.field_id);
        }
      });
    })

    this.tmpFacetChangeEventSubscription$ = this.facetChangeEventService.onFacetFiltersChange.subscribe(facet => {
      this.facetFilters = [];
      this.removeSearchInput = "";


      this.facetFilters = facet.filter((eachFacet) => {
        const index = order.indexOf(eachFacet.facet_field_request.field_id);
        return index >= 0 ? true : false;
      });



      let arr = SPField;
      this.facetFilters.forEach(obj => {
        
        if (arr.includes(obj.facet_field_request.field_id)) {
          
          if (obj.type === "DateRangeFieldResponse") {
           
            obj['startDateFlag'] = true;
            obj['endDateFlag'] = true;
          } else {
            obj['startDateFlag'] = false;
            obj['endDateFlag'] = false;
          }

        } else {
          obj['startDateFlag'] = false;
          obj['endDateFlag'] = false;
        }


      })



      this.facetFilters.forEach(eachFilter => {
        // eachFilter.facet_value_list.sort((v1, v2) => v1?.value?.localeCompare(v2?.value));

        eachFilter.facet_value_list.sort(function (a, b) {
          for (let i = 0; i < Math.min(a?.value?.length, b?.value?.length); i++) {
            if (a?.value.charCodeAt(i) !== b?.value.charCodeAt(i)) {
              return a?.value?.charCodeAt(i) - b?.value?.charCodeAt(i);
            }
          }
          return a?.value?.length - b?.value?.length;
        });
      })
      // this.facetFilters = facet || Object.assign([]);
      this.facetFilters = this.sortByOrder(this.facetFilters, order);
      if (Array.isArray(this.facetConfigurations) && this.facetConfigurations.length > 0) {
        this.initalizeFacets();
      }
      else {
        this.facetService.setFacetConfigurations().subscribe(facetConfigurations => {
          this.facetConfigurations = facetConfigurations;
          this.initalizeFacets();
        }, error => {
          this.facetFilters = Object.assign([]);
          this.selectedFacetFilters = [];
          this.initalizeFacets();
        });
      }
    });

    this.activatedRoute.queryParams.subscribe(params => {
      this.queryParams = params;
      this.isListView = params.isListView;
      this.sortBy = params.sortBy;
      this.sortOrder = params.sortOrder;
      this.pageNumber = params.pageNumber;
      this.pageSize = params.pageSize;
      this.searchName = params.searchName;
      this.savedSearchName = params.savedSearchName;
      this.advSearchData = params.advSearchData;
      if (params.facetData) {
        if ((this.viewName && params.viewName && this.viewName !== params.viewName) || (this.groupByFilter && params.groupByFilter && this.groupByFilter !== params.groupByFilter) ||
          (this.deliverableTaskFilter && params.deliverableTaskFilter && this.deliverableTaskFilter !== params.deliverableTaskFilter) ||
          (this.listDependentFilter && params.listDependentFilter && this.listDependentFilter !== params.listDependentFilter)) {
          this.viewName = params.viewName;
          this.groupByFilter = params.groupByFilter;
          this.deliverableTaskFilter = params.deliverableTaskFilter;
          this.listDependentFilter = params.listDependentFilter;
          this.searchName = null;
          this.savedSearchName = null;
          this.advSearchData = null;
          this.clearAllFacets();
        } else {
          this.viewName = params.viewName;
          this.groupByFilter = params.groupByFilter;
          this.deliverableTaskFilter = params.deliverableTaskFilter;
          this.listDependentFilter = params.listDependentFilter;
          if (params.facetData !== this.facetData) {
            const selectedFacets = [];
            let facetSearchData;
            if (params.menuName === MenuConstants.DELIVERABLE_TASK_VIEW) {
              facetSearchData = this.stateService.getDeliverableTaskViewFacetSearch();
            } else {
              facetSearchData = this.stateService.getFacetSearch();
            }
            if (facetSearchData) {
              if (facetSearchData !== this.selectedFacetFilters) {
                this.selectedFacetFilters = facetSearchData;


                this.updateChicklet();
                this.publishFacetFieldRestriction();
              }
            } else {
              this.facetData = params.facetData;
              const facetFilters = JSON.parse(this.facetData);
              facetFilters.forEach(filter => {
                const facetFilter = {
                  behavior: this.facetGenBehaviour,
                  field_id: filter.fieldId,
                  type: filter.type,
                  values: JSON.parse(filter.values)
                };
                selectedFacets.push(facetFilter);
              });
              this.selectedFacetFilters = selectedFacets;
              this.publishFacetFieldRestriction();
            }
            setTimeout(() => {
              this.updateChicklet();
              this.publishFacetFieldRestriction();
            }, 2000);
          }
        }
      } else {
        this.viewName = params.viewName;
        this.groupByFilter = params.groupByFilter;
        this.deliverableTaskFilter = params.deliverableTaskFilter;
        this.listDependentFilter = params.listDependentFilter;
      }
    });

    this.tmpFacetResetEventSubscription$ = this.facetChangeEventService.resetFacet$.subscribe(data => {
      this.resetFacet();
    });

  }
  ngAfterViewInit() {
    this.setDynamicHeightForFacetContainer();
  }

  ngOnDestroy(): void {
    this.resetFacet();
    if (this.tmpFacetChangeEventSubscription$) {
      this.tmpFacetChangeEventSubscription$.unsubscribe();
    }
    if (this.tmpFacetResetEventSubscription$) {
      this.tmpFacetResetEventSubscription$.unsubscribe();
    }
  }

}
