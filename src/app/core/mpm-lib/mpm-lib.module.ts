import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MPMAppMenuComponent } from './app-menu/app-menu.component';
import { ColumnChooserModule, FacetModule, MpmLibraryModule, PMDashboardModule, TasksModule } from 'mpm-library';
import { MPMProjectListComponent } from './pm-dashboard/project-list/project-list.component';
import { MPMProjectCardComponent } from './pm-dashboard/project-card/project-card.component';
import { MPMProjectOverviewComponent } from './project/project-overview/project-overview.component';
import { MPMSwimLaneComponent } from './project/tasks/swim-lane/swim-lane.component';
import { MPMTaskListViewComponent } from './project/tasks/task-list-view/task-list-view.component';
import { MPMTaskCardViewComponent } from './project/tasks/task-card-view/task-card-view.component';
import { MPMTaskToolbarComponent } from './project/tasks/task-toolbar/task-toolbar.component';
import { MaterialModule } from '../../material.module';
import { MPMTaskCreationComponent } from './project/tasks/task-creation/task-creation.component';
import { MPMProjectSortComponent } from './pm-dashboard/project-sort/project-sort.component';
import { MPMDeliverableSwimLaneComponent } from './project/tasks/deliverable/deliverable-swim-lane/deliverable-swim-lane.component';
import { DeliverableModule } from 'mpm-library';
import { MPMShareAssetsModule } from './share-assets/share-assets.module';
import { MPMDeliverableCardComponent } from './project/tasks/deliverable/deliverable-card/deliverable-card.component';
import { MPMDeliverableToolbarComponent } from './project/tasks/deliverable/deliverable-toolbar/deliverable-toolbar.component';
import { MPMDeliverableCreationComponent } from './project/tasks/deliverable/deliverable-creation/deliverable-creation.component';
import { MPMDeliverableSortComponent } from './project/tasks/deliverable/deliverable-sort/deliverable-sort.component';
import { MPMBulkActionsComponent } from './project/tasks/deliverable/bulk-actions/bulk-actions.component';
import { MPMAssetsViewComponent } from './project/assets-view/assets-view.component';
import { MPMAssetCardComponent } from './project/asset-card/asset-card.component';
import { MPMAssetDetailsViewComponent } from './project/asset-details-view/asset-details-view.component';
import { MPMSearchIconComponent } from './search/search-icon/search-icon.component';
import { MPMSimpleSearchComponent } from './search/simple-search/simple-search.component';
import { MPMAdvancedSearchComponent } from './search/advanced-search/advanced-search.component';
import { MPMAdvancedSearchFieldComponent } from './search/advanced-search-field/advanced-search-field.component';
import { MPMDeliverableTaskListComponent } from './deliverable-task/deliverable-task-list/deliverable-task-list.component';
import { DeliverableTaskModule } from 'mpm-library';
import { FacetComponent } from './facet/facet.component';
import { MPMCustomWorkflowFieldComponent } from './project/tasks/custom-workflow-field/custom-workflow-field.component';
import { PaginationModule } from 'mpm-library';
import { MPMResourceManagementResourceAllocationComponent } from './resource-management/resource-management-resource-allocation/resource-management-resource-allocation.component';
import { MpmAssetListComponent } from './project/asset-list/asset-list.component';
import { NpdDeliverableTaskListComponent } from '../npd-ui/npd-task-view/npd-deliverable-task-list/npd-deliverable-task-list.component';
import { NpdDeliverableCardComponent } from '../npd-ui/npd-task-view/npd-deliverable-card/npd-deliverable-card.component';
import { NpdTaskModule } from '../npd-ui/npd-task-view/npd-task.module';
import { MPMColumnChooserComponent } from './column-chooser/column-chooser/column-chooser.component';
import { MPMColumnChooserFieldComponent } from './column-chooser/column-chooser-field/column-chooser-field.component';


@NgModule({
  declarations: [
    MPMAppMenuComponent,
    MPMProjectCardComponent,
    MPMProjectListComponent,
    MPMProjectOverviewComponent,
    MPMSwimLaneComponent,
    MPMTaskListViewComponent,
    MPMTaskCardViewComponent,
    MPMTaskToolbarComponent,
    MPMTaskCreationComponent,
    MPMProjectSortComponent,
    MPMDeliverableSwimLaneComponent,
    MPMDeliverableCardComponent,
    MPMDeliverableToolbarComponent,
    MPMDeliverableCreationComponent,
    MPMDeliverableSortComponent,
    MPMBulkActionsComponent,
    MPMAssetsViewComponent,
    MPMAssetCardComponent,
    MPMAssetDetailsViewComponent,
    MPMSearchIconComponent,
    MPMSimpleSearchComponent,
    MPMAdvancedSearchComponent,
    MPMAdvancedSearchFieldComponent,
    MPMDeliverableTaskListComponent,
    FacetComponent,
    MPMCustomWorkflowFieldComponent,
    MpmAssetListComponent,
    NpdDeliverableTaskListComponent,
    NpdDeliverableCardComponent,
    MPMColumnChooserComponent,
    MPMColumnChooserFieldComponent
  ],
  imports: [
    CommonModule,
    MpmLibraryModule,
    MaterialModule,
    PMDashboardModule,
    TasksModule,
    DeliverableModule,
    MPMShareAssetsModule,
    DeliverableTaskModule,
    FacetModule,
    PaginationModule,
    ColumnChooserModule,
    NpdTaskModule
  ],
  entryComponents: [],
  exports: [
    MPMAppMenuComponent,
    MPMProjectCardComponent,
    MPMProjectListComponent,
    MPMProjectOverviewComponent,
    MPMSwimLaneComponent,
    MPMTaskListViewComponent,
    MPMTaskCardViewComponent,
    MPMTaskToolbarComponent,
    MPMTaskCreationComponent,
    MPMProjectSortComponent,
    MPMDeliverableSwimLaneComponent,
    MPMDeliverableCardComponent,
    MPMDeliverableToolbarComponent,
    MPMDeliverableCreationComponent,
    MPMDeliverableSortComponent,
    MPMBulkActionsComponent,
    MPMAssetsViewComponent,
    MPMAssetCardComponent,
    MPMAssetDetailsViewComponent,
    MPMSearchIconComponent,
    MPMSimpleSearchComponent,
    MPMAdvancedSearchComponent,
    MPMAdvancedSearchFieldComponent,
    MPMDeliverableTaskListComponent,
    FacetComponent,
    MPMCustomWorkflowFieldComponent,
    NpdDeliverableTaskListComponent,
    NpdDeliverableCardComponent,
    MPMColumnChooserComponent,
    MPMColumnChooserFieldComponent
  ]
})
export class MpmLibModule { }
