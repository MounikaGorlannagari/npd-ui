import { Component, OnInit } from '@angular/core';
import { ProjectSortComponent } from 'mpm-library';

@Component({
  selector: 'app-project-sort',
  templateUrl: './project-sort.component.html',
  styleUrls: ['./project-sort.component.scss']
})
export class MPMProjectSortComponent extends ProjectSortComponent {

}
