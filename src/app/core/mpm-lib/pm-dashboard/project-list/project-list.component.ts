import { Component, OnInit, SimpleChanges } from '@angular/core';
import { IndexerDataTypes, MPMField, ProjectListComponent } from 'mpm-library';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
})
export class MPMProjectListComponent extends ProjectListComponent {
  getFieldValueByDisplayColumn(displayColumn: MPMField, dataSet: any): any {
    if (displayColumn && dataSet && displayColumn.INDEXER_FIELD_ID) {
      const indexerId = this.formIndexerIdFromField(displayColumn);
      let currValue = dataSet[indexerId];
      currValue =
        currValue &&
          displayColumn.DATA_TYPE.toLowerCase() ===
          IndexerDataTypes.DATETIME.toLowerCase()
          ? this.formatToLocalePipe.transform(currValue)
          : currValue
            ? currValue.toString()
            : currValue;
      return (
        currValue ||
        (displayColumn.DATA_TYPE.toLowerCase() ===
          IndexerDataTypes.NUMBER.toLowerCase() ||
          displayColumn.DATA_TYPE.toLowerCase() ===
          IndexerDataTypes.DECIMAL.toLowerCase()
          ? '0' : displayColumn.DATA_TYPE.toLowerCase() === IndexerDataTypes.BOOLEAN ? 'false' : 'NA')
      );
    }
    return 'NA';
  }
  formIndexerIdFromField(displayColumn: MPMField): string {
    let indexerId = displayColumn.INDEXER_FIELD_ID;
    return indexerId;
    if (
      displayColumn.IS_CUSTOM_METADATA &&
      displayColumn.IS_CUSTOM_METADATA === 'true'
    ) {
      indexerId +=
        displayColumn.SEARCHABLE === 'true'
          ? '_key_' + displayColumn.DATA_TYPE
          : '_' + displayColumn.DATA_TYPE;
    }
  }
  getProperty(displayColumn: MPMField, projectData: any): string {

    this.status.forEach((statusFilter) => {
      if (
        this.getFieldValueByDisplayColumn(
          displayColumn,
          projectData
        ) === statusFilter
      ) {
        this.enableBulkEdit = true;
      }
    });
    // if(displayColumn.MAPPER_NAME === this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS){
    //     this.getCompletedMilestonesByProjectId(projectData.ID);
    //     return (this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData).concat(this.milestoneTasks));
    // }
    // else{
    let value = this.getFieldValueByDisplayColumn(displayColumn,projectData)
    if(value == "true"){
      return "yes"
    }else if(value == "false"){
      return "No"
    }
    return value;
    // }
  }
}
