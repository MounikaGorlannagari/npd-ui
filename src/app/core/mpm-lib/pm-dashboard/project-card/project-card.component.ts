import { Component, OnInit } from '@angular/core';
import { ProjectCardComponent } from 'mpm-library';

@Component({
  selector: 'app-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss']
})
export class MPMProjectCardComponent extends ProjectCardComponent {

}
