import { Component } from '@angular/core';
import { UploadComponent } from 'mpm-library';


@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss']
})

export class MPMUploadComponent extends UploadComponent {

}
