import { Component } from '@angular/core';
import { FileUploadComponent } from 'mpm-library';

@Component({
    selector: 'app-file-upload',
    templateUrl: './file-upload.component.html',
    styleUrls: ['./file-upload.component.scss']
})

export class MPMFileUploadComponent extends FileUploadComponent {

    super() {
        this.htmlElement = this.element.nativeElement;
    }

}
