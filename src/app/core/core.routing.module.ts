import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoreComponent } from './core.component';

import { AuthGuard, AppGuard } from 'mpm-library';
import { RoutingConstants } from './mpm/routingConstants';
const routes: Routes = [
    {
        path: '',
        component: CoreComponent,
    }, {
        path: 'mpm',
        loadChildren: () => import('./mpm/mpm.module').then(m => m.MpmModule)
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class CoreRoutingModule { }
