import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { MpmLibraryModule, IndexerInterceptor } from 'mpm-library';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FooterComponent } from './core/mpm/footer/footer.component';
import { AppDynamicConfigService } from 'mpm-library';
import { MAT_HAMMER_OPTIONS } from '@angular/material/core';
@NgModule({
  declarations: [AppComponent, FooterComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MpmLibraryModule,
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    {
      provide: MAT_HAMMER_OPTIONS,
      useValue: { cssProps: { userSelect: true } },
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: IndexerInterceptor,
      multi: true,
    },
    AppDynamicConfigService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
